import pytest
from addons.testutils import setup
import os

TEST_DB = os.getenv('OERP_TEST_DB', 'prodotti4')
TEST_CONF = os.getenv('OERP_TEST_CONF', '/vagrant/test.conf')


setup.setup_basic(TEST_CONF, TEST_DB)

@pytest.fixture(scope='session', autouse=True)
def setup_openerp_tests(request):

    cn, pool = setup.setup(TEST_DB)
    cr = cn.cursor()

    def find_openerp_module(mod):
        p = os.path.dirname(mod.__file__)
        lp = None
        while p != lp:
            lp = p
            if '__openerp__.py' in os.listdir(p):
                return os.path.basename(p)
            p = os.path.dirname(p)

    modules = {find_openerp_module(i.module) for i in request.session.items}
    modules = [setup.Module(cr, m) for m in  modules]
    try:
        setup.reload_modules(cr, pool, modules)
    except:
        cr.rollback()
        raise
    cr.commit()
    cr.close()

