from sqlalchemy.engine import create_engine
from sqlalchemy import orm

import settings
from unicode_pyodbc import UnicodePyodbc


engine = create_engine(settings.CONNECTION_STRING, module=UnicodePyodbc(settings.ENCODING))
engine_pg = create_engine(settings.PG_CONNECTION_STRING)

class unicode_sessionmaker(orm.sessionmaker):
    pass

Session = orm.scoped_session(orm.sessionmaker(bind=engine))
PGSession = orm.scoped_session(orm.sessionmaker(bind=engine_pg))

class SessionGuard():
    def __init__(self, sessionmaker):
        self.sessionmaker = sessionmaker

    def __enter__(self):
        """
        :rtype: orm.Session
        """
        return self.sessionmaker()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.sessionmaker.remove()

session = SessionGuard(Session)
pg_session= SessionGuard(PGSession)


