import pyodbc

class Cursor():
    def __init__(self, cursor, encoding):
        self.cursor = cursor
        self.encoding = encoding

    def convert_string(self, s):
        if isinstance(s, str):
            s = s.decode(self.encoding)
        return s

    def convert_row(self, row):
        if row is None:
            return None
        return map(self.convert_string, row)

    def fetchall(self, *args, **kwargs):
        for r in self.cursor.fetchall(*args, **kwargs):
            yield self.convert_row(r)

    def fetchmany(self, *args, **kwargs):
        for r in self.cursor.fetchmany(*args, **kwargs):
            yield self.convert_row(r)

    def fetchone(self, *args, **kwargs):
        return self.convert_row(self.cursor.fetchone(*args, **kwargs))

    def __getattr__(self, item):
        return getattr(self.cursor, item)

class Connection():
    def __init__(self, connection, encoding):
        self.connection = connection
        self.encoding = encoding

    def cursor(self):
        return Cursor(self.connection.cursor(), self.encoding)

    def __getattr__(self, item):
        return getattr(self.connection, item)

class UnicodePyodbc():
    def __init__(self, encoding):
        self.encoding = encoding

    def connect(self, *args, **kwargs):
        connection = pyodbc.connect(*args, **kwargs)
        return Connection(connection, self.encoding)

    def __getattr__(self, item):
        return getattr(pyodbc, item)



