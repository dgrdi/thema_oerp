DB=thema_tests
export CLASSPATH=/home/federica/PycharmProjects/thema_oerp/addons/jasper_reports/java/lib:$CLASSPATH

if [ -z $1 ]; then
	echo "usage: run-tests.sh [module] [update]"
	echo "	module: module to test"
else
	CMD="run_openerp.py --config=openerp.conf -d $DB  --update=$1 --log-level=debug -i $1 --test-enable"
	echo "$CMD"
	python $CMD
fi
