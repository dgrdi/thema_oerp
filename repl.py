import sys
from openerp.pooler import get_db_and_pool
from openerp import SUPERUSER_ID as uid
from openerp.tools.config import config

def setup_repl(conf, db):
    config.parse_config(['-c', conf, '-d', db, '--log-level=debug'])
    global conn
    global pool
    conn, pool = get_db_and_pool(db)
    return conn, pool


if __name__ == '__main__':
    setup_repl(sys.argv[1], sys.argv[2])

