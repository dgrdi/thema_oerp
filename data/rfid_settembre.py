from db import session

def ordini():
    with session as s:
        res = s.execute("select ordine, prodotto, qta_marchio, qta_neutra from thema.ordini_settembre2")
        return res.fetchall()

def mappa_rfid():
    with session as s:
        res = s.execute("select COD_PRODOTTO, COD_DEPOSITO, EPC from rfid_epc")
        mp = {}
        for cp, cd, epc in res.fetchall():
            cp = cp.strip(); cd = cd.strip()
            mp[(cp, cd)] = epc
        return mp

def esistenti():
    with session as s:
        res = s.execute("select EPC, PROGRESSIVO from thema.rfid_esistenti where DATA_CREAZIONE = '2014-07-10'")
        rf = {}
        for epc, pg in res.fetchall():
            rf.setdefault(epc, []).append(pg)
        for v in rf.values():
            v.reverse()
        return rf


def assegna_ordini():
    ord = ordini()
    mp = mappa_rfid()
    ex = esistenti()

    res = {}

    for ordine, prod, qta_marchio, qta_neutra in ord:
        ord_rf = res.setdefault(ordine, [])
        epc_prod_marchio = mp[(prod, '001')]
        if qta_neutra > 0:
            epc_prod_neutro = mp[(prod, 'NEU')]
        for i in range(int(float(qta_marchio) * 1.05)):
            prog = ex[epc_prod_marchio].pop()
            ord_rf.append((epc_prod_marchio, prog))
        for i in range(int(float(qta_neutra) * 1.05)):
            prog = ex[epc_prod_neutro].pop()
            ord_rf.append((epc_prod_neutro, prog))
    return res


def inserisci(ord):
    with session as s:
        s.execute("INSERT INTO thema.rfid_ordini_settembre2(ordine, epc, progressivo) "
                  "VALUES (:ordine, :epc, :prog)",
            [{'ordine': ordine, 'epc': epc, 'prog': prog} for ordine, vals in ord.iteritems() for epc, prog in vals])
        s.commit()


def spezza_file():
    import sys
    files = {}
    with open(sys.argv[1]) as fo:
        head = ','.join(fo.readline().split(',')[4:])
        def get_file(ordine):
            if ordine not in files:
                files[ordine] = open(ordine.replace('/', '_') + '.csv', 'wb')
                files[ordine].write(head)
            return files[ordine]
        for l in fo:
            l = l.split(',')
            ord = l[0]
            l = ','.join(l[4:])
            get_file(ord).write(l)
    for fo in files.values():
        fo.close()


if __name__ == '__main__':
    spezza_file()