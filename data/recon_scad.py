from db import session
from extractor import client
from thwork import split_work
from itertools import combinations

def scadenze_v2(save=True, azienda='AZIENDA_TOP'):
    with session as s:
        s.execute('USE %s;' % azienda)
        res = s.execute("""
            select
              s.TIPO_SEZIONALE,
              s.CAMPO_RICERCA,
              s.TIPO_PARTITA,
              s.ANNO_PARTITA,
              s.NUMERO_PARTITA,
              s.TIPO_ORI,
              s.ANNO_ORI,
              s.NUMERO_ORI,
              s.DATA_SCADENZA,
              case pn.F_DARE_AVERE when 'D' then 1 else -1 END * s.IMPORTO_LIRE imp,
              s.ID_PRIMA_NOTA,
              s.ID_SCADENZA,
              f.TIPO_DOC_RIF,
              f.ANNO_DOC_RIF,
              f.NUMERO_DOC_RIF
              FROM MOV_SCADENZE s
              INNER JOIN PRIMA_NOTA pn ON pn.ID_PRIMA_NOTA = s.ID_PRIMA_NOTA
              LEFT JOIN T_FATT_VENDITA f ON f.ANNO_DOC = s.ANNO_ORI and f.NUMERO_DOC = s.NUMERO_ORI and s.TIPO_ORI = f.TIPO_DOC
              LEFT JOIN TAB_DOC_FATVEN tf ON tf.COD_DOC_FATVEN = f.COD_DOC_FATVEN AND tf.F_FATT_NC = 'N'
              WHERE
              s.TIPO_MOV_SC IN ('A', 'C')
            ORDER BY
              s.TIPO_SEZIONALE,
              s.CAMPO_RICERCA,
              s.TIPO_PARTITA,
              s.ANNO_PARTITA,
              s.NUMERO_PARTITA,
              s.DATA_SCADENZA,
              s.TIPO_ORI,
              s.ANNO_ORI,
              s.NUMERO_ORI,
              pn.DATA_REGISTRAZ,
              s.ID_SCADENZA

        """.format(az=azienda)).fetchall()
    recs = []
    partial_recs = []
    lastp = None
    lasts = None
    tot = 0
    part = []
    scad = []
    for row in res + [None]:
        if row is not None:
            p = (row.TIPO_SEZIONALE, row.CAMPO_RICERCA, row.TIPO_PARTITA, row.ANNO_PARTITA, row.NUMERO_PARTITA)
            s = p + (row.TIPO_ORI, row.ANNO_ORI, row.NUMERO_ORI, row.DATA_SCADENZA)
            nx = (row.ID_PRIMA_NOTA, row.ID_SCADENZA)
        else:
            s = None; p =None
        if s != lasts:
            if scad:
                part.append(scad)
            scad = []
        if p != lastp:
            nonzero = []
            for sc in part:
                if sum(el[1] for el in sc) == 0:
                    recs.append([el[0] for el in sc])
                else:
                    nonzero.append(sc)
            elems = [el for sc in nonzero for el in sc]
            if elems:
                spl = splitta_elementi(set(elems))
                print 'Divisione elementi rimasti in partita: da \n\t%s\n\ta\n\t%s' % (elems, spl)
                for g in spl:
                    if any(el[1] > 0 for el in g) and any(el[1] < 0 for el in g):
                        print 'riconciliando %s', g
                        sm = sum(el[1] for el in g)
                        if sm == 0:
                            recs.append([el[0] for el in g])
                        else:
                            partial_recs.append([el[0] for el in g])
                    else:
                        print 'non posso riconciliare, segni concordi: %s' % g
            part = []
        if row is not None:
            scad.append((nx, row.imp))
            lastp = p
            lasts = s
    if save:
        with session as s:
            s.execute('USE %s;' % azienda)
            s.execute("""
               DELETE FROM oerp.rec_h where typ = 'S';
               DELETE FROM oerp.rec_d where rec_typ = 'S';

            """)
            s.execute("""
              INSERT INTO oerp.rec_h (typ, id, saldo) VALUES ('S', :i, 0);
            """, [{'i': i + 1} for i in range(len(recs) + len(partial_recs))])
            s.execute("""
              INSERT INTO oerp.rec_d (rec_typ, rec_id, ID_PRIMA_NOTA, ID_SCADENZA)
                VALUES ('S', :i, :pn, :sc);
            """, [{'i': i + 1, 'pn': pn, 'sc': sc} for i, lines in enumerate(recs + partial_recs) for pn, sc in lines])
            s.commit()
    return recs, partial_recs

def splitta_elementi(elems):
    if not elems:
        return []
    for i in range(2, len(elems) -1):
        for c in combinations(elems, i):
            if sum(el[1] for el in c) == 0:
                return [c] + splitta_elementi(elems.difference(set(c)))
    return [elems]


def remove_rec():
    rc = client.get_model('account.move.reconcile')
    ids = rc.search([])
    print len(ids), 'to delete'
    def worker(data):
        print 'Unlinking %s reconciles' % len(data)
        rc.unlink(data)
    def chunkify():
        for i in range(0, len(ids), 20000):
            yield ids[i:i+20000]
    for c in chunkify():
        print 'Unlinking %s reconciles' % len(c)
        rc.unlink(c)