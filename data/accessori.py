from extractor import extract_data


class aste_tpl(extract_data):
    model = 'product.template'
    query = """
        select thema.xid('TPL' + rtrim(COD_PRODOTTO)) id,
          case
          when substring(DESCR_PRODOTTO, 1, 1) = '*' then substring(DESCR_PRODOTTO, 2, 100)
          else DESCR_PRODOTTO
          end name,
          thema.xid(COD_LINEA) [collection_id/id],
          thema.xid('PMD' + COD_MODELLO) [model_id/id],
          'thema_product.product_category_temple' [categ_id/id],
          'thema_product.material_plastic' [material_id/id],
          case when substring(DESCR_PRODOTTO, 1, 1) = '*' then 'obsolete' else 'sellable' end state,
        'thema_product.product_uom_pair' [uom_po_id/id],
        'thema_product.product_uom_pair' [uom_id/id]
        from MAGAZZINO_1
        where COD_CAT_MERC = 'ZZZ'
    """

class aste_prod(extract_data):
    model = 'product.product'
    query = """
        select thema.xid(rtrim(M.COD_PRODOTTO)) id,
        rtrim(M.COD_PRODOTTO) default_code,
        thema.xid('TPL' + rtrim(M.COD_PRODOTTO)) [product_tmpl_id/id],
        case
          when substring(DESCR_PRODOTTO, 1, 1) = '*' then substring(DESCR_PRODOTTO, 2, 100)
          else DESCR_PRODOTTO
          end name,
        thema.xid(rtrim(COD_COLORE) + '/' + isnull(C.COLORE, 'SC')) [color_id/id],
        COD_BARRE ean13,
        cast(LUNGHEZZA_ASTA as int) temple_length
        from MAGAZZINO_1 M
        left join thema.colori C on C.COLORE_DA_DESCR = thema.fn_colore_da_descr(M.DESCR_PRODOTTO)
        where isnull(COD_CAT_MERC, '')  = 'ZZZ'
        and COD_LINEA is not null and COD_MODELLO is not null and COD_COLORE is not null;
    """

class astucci_tpl(extract_data):
    model = 'product.template'
    query = """
        select 'TPL' + thema.xid(rtrim(COD_PRODOTTO)) id,
          'thema_product.product_category_case' [categ_id/id],
        case when substring(DESCR_PRODOTTO, 1, 1) = '*' then 'obsolete' else 'sellable' end state,
        case
          when substring(DESCR_PRODOTTO, 1, 1) = '*' then substring(DESCR_PRODOTTO, 2, 100)
          else DESCR_PRODOTTO
          end name,
        COD_BARRE ean13,
        rtrim('PMD' + M.COD_MODELLO) [model_id/id]
        from MAGAZZINO_1 M
        where M.COD_CAT_MERC = 'ASS'
    """

class astucci(extract_data):
    model = 'product.product'
    query ="""
        select thema.xid(rtrim(M.COD_PRODOTTO)) id,
        rtrim(M.COD_PRODOTTO) default_code,
        'TPL' + thema.xid(rtrim(COD_PRODOTTO)) [product_tmpl_id/id]
        from MAGAZZINO_1 M
        where isnull(COD_CAT_MERC, '')  = 'ASS'
    """

def go():
    m = aste_tpl()
    m.bulk_import(context={'ignore_methods': True, 'import': True})
    p = aste_prod()
    p.bulk_import(context={'ignore_methods': True, 'import': True})
    astm = astucci_tpl()
    astm.bulk_import(context={'ignore_methods': True, 'import': True})
    ast = astucci()
    ast.bulk_import(context={'ignore_methods': True, 'import': True})

if __name__ == '__main__':
    go()