from extractor import extract_data, client
from thwork import split_work

class order_status(extract_data):
    model = 'sale.order'
    query = """
        WITH s AS (
            SELECT
              t.ID_ORDINE,
              max(CASE
                  WHEN isnull(d.F_SALDO, '') <> '' AND QTA_EVASA + QTA_IN_SPED > 0 THEN 3
                  WHEN QTA_EVASA + QTA_IN_SPED = QTA_ORDINE THEN 3
                  WHEN isnull(d.F_SALDO, '') <> '' AND QTA_EVASA + QTA_IN_SPED = 0 THEN 2
                  WHEN isnull(t.F_BLOCCO, '') <> '' OR isnull(t.F_STAMPATO, 'N') = 'N' THEN 1
                  ELSE 0
                  END) [state]
            FROM T_ORDINI_CLI t
              INNER JOIN D_ORDINI_CLI d ON d.ID_ORDINE = t.ID_ORDINE
            GROUP BY t.ID_ORDINE
        )
        SELECT
          o.ID_ORDINE,
          thema.xid(dbo.fn_RifDocumento(o.TIPO_DOC, o.ANNO_DOC, o.NUMERO_DOC)) id,
          CASE WHEN isnull(o.F_BLOCCO, '') <> '' THEN 'blocked'
          WHEN isnull(o.F_STAMPATO, '') <> 'S' and s.state < 2 then 'draft'
          when s.state = 2 then 'cancel'
          when s.state = 3 then 'done'
          else 'progress'
          END state
        FROM s
          INNER JOIN T_ORDINI_CLI o ON o.ID_ORDINE = s.ID_ORDINE
    """

def fix_order_line_status():
    so = client.get_model('sale.order')
    sol = client.get_model('sale.order.line')
    mp = {
        'draft': 'draft',
        'blocked': 'draft',
        'cancel': 'cancel',
        'done': 'done',
        'progress': 'confirmed',
    }
    for st_o, st_l in mp.iteritems():
        ids = sol.search([('order_id.state', '=', st_o)])
        print 'writing state %s for %s order lines' % (st_l, len(ids))
        def worker(data):
            sol.bulk_import(['.id', 'state'], [(id, st_l) for id in data],
                            context={'import_skip_store': True, 'ignore_methods': True, 'nocreate': True})
        split_work(ids, worker)

if __name__ == '__main__':
    order_status().bulk_import(context={'ignore_methods': True, 'import_skip_store': True, 'nocreate': True})
    fix_order_line_status()


