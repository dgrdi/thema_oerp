from extractor import extract_csv, client

class aziende(extract_csv):
    csv = 'csv/res.company.csv'
    model = 'res.company'

class nomi_aziende(extract_csv):
    csv = 'csv/res.company.name.csv'
    model = 'res.partner'

def set_xid():
    """imposta xml id per partner collegati alle aziende."""
    map = {
        'thema_canaria': 'partner_canaria',
        'thema_spagna': 'partner_spagna',
    }
    md = client.get_model('ir.model.data')
    company = client.get_model('res.company')
    partner = client.get_model('res.partner')
    for c in company.search([]):
        xid = company.get_external_id([c])[unicode(c)].split('.')[-1]
        partner_id = company.read(c, fields=('partner_id',))['partner_id'][0]
        partner_xid = partner.get_external_id([partner_id])[unicode(partner_id)]
        if not partner_xid and xid in map:
            partner_xid = map[xid]
            md.create({
                'res_id': partner_id,
                'model': 'res.partner',
                'name': partner_xid,
            })

def greenvision():
    cp = client.get_model('res.company')
    md = client.get_model('ir.model.data')
    pid = md.search([('name', '=', 'CL011141')])[0]
    pid = md.read(pid, fields=['res_id'])['res_id']
    dt = {
        'id': 'greenvision',
        'partner_id/.id': pid,
        'name': 'Greenvision',
        'code': 'GV',
    }
    vl = dt.items()
    print zip(*vl)[0], zip(*vl)[1]
    rs = cp.load(list(zip(*vl)[0]), [list(zip(*vl)[1])])
    print rs['messages']


def go():
    a = aziende()
    a.load()
    a.print_messages()
    set_xid()
    an = nomi_aziende()
    an.load()
    an.print_messages()
    greenvision()


if __name__ == '__main__':
    go()