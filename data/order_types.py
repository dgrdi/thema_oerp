from extractor import extract_data


class order(extract_data):
    model = 'sale.order'
    query = """
        SELECT
          thema.xid(dbo.fn_RifDocumento(t.TIPO_DOC, t.ANNO_DOC, t.NUMERO_DOC))                           id,
          CASE WHEN ot.TIPO_FATTURAZ = 'I' THEN 'tsBC' ELSE thema.xid('ts' + ot.COD_DOC_GENERATO) END    [delivery_type_id/id],
          CASE WHEN ot.TIPO_FATTURAZ = 'I' THEN 'invoice_type_immediate' ELSE 'invoice_type_delayed' END [invoice_type_id/id],
          CASE WHEN pg.F_CONTRASSEGNO = 1 THEN 1 ELSE 0 END                                              cod,
          CASE
          WHEN t.COD_DOC_ORDCLI LIKE 'I%' THEN 'origin_agent'
          WHEN t.COD_DOC_ORDCLI LIKE '3 -%' THEN 'origin_internet'
          WHEN t.COD_AGENTE = '000201' THEN 'origin_direct'
          WHEN t.TIPO_DOC = '02' THEN 'origin_agent'
          WHEN t.TIPO_DOC = '06' THEN 'origin_phone'
          WHEN t.TIPO_DOC = '03' THEN 'origin_agent'
          WHEN t.TIPO_DOC = '04' THEN 'origin_agent'
          WHEN t.TIPO_DOC = '05' THEN 'origin_agent'
          WHEN t.TIPO_DOC = '08' THEN 'origin_internet'
          WHEN t.TIPO_DOC = '07' THEN 'origin_direct'
          ELSE 'origin_agent' END                                                                        [origin_id/id]
        FROM T_ORDINI_CLI t
          INNER JOIN TAB_DOC_ORDCLI ot ON ot.COD_DOC_ORDCLI = t.COD_DOC_ORDCLI
          LEFT JOIN TAB_PAGAMENTI pg ON pg.COD_PAGAMENTO = ot.COD_PAGAMENTO

    """

if __name__ == '__main__':
    order().bulk_import(context={'ignore_methods': True, 'import_skip_store': True, 'nocreate': True})
