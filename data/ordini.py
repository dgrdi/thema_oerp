from extractor import extract_data
from trigger_recompute import trigger_recompute

class testate(extract_data):
    model = 'sale.order'
    query = """
        SELECT
          thema.xid(dbo.fn_RifDocumento(t.TIPO_DOC, t.ANNO_DOC, t.NUMERO_DOC)) id,
          v.xid                                                                [currency_id/id],
          convert(VARCHAR(20), t.DATA_REGISTRAZ)                               [date_order],
        --TODO fiscal position
        -- TODO incoterm
          dbo.fn_RifDocumento(t.TIPO_DOC, t.ANNO_DOC, t.NUMERO_DOC)            name,
          oerp.commenti_docs_testata('OC', t.ID_ORDINE)                        note,
          'picking'                                                            [order_policy],
          NUM_ORDCLI                                                           ref,
          NUM_ORDCLI                                                           client_order_ref,
          rtrim(xp.xid + CASE
                         WHEN ds.CAMPO_RICERCA IS NULL THEN ''
                         ELSE isnull(t.DESTINAZIONE, '') END)                  [partner_id/id],
          rtrim(xpf.xid + CASE
                          WHEN dsf.CAMPO_RICERCA IS NULL THEN ''
                          ELSE isnull(t.DESTINAZIONE_FT, '') END)                 [partner_invoice_id/id],
          rtrim(xpd.xid + CASE
                          WHEN ds.CAMPO_RICERCA IS NULL THEN ''
                          ELSE isnull(t.DESTINAZIONE, '') END)                 [partner_shipping_id/id],
          oerp.x_pagamento(t.COD_PAGAMENTO)                                    [payment_term/id],
          isnull(isnull('C' + thema.xid(oerp.listino_per_cliente(t.CAMPO_RICERCA)),
                        l.xid),
                 'L03')                                                        [pricelist_id/id],
          'progress'                                                           state,
          oerp.fn_user(isnull(op.xid, t.USER_OWNER))                                           [user_id/id],
          thema.xid('AG' + ca.COD_AGENTE)                                      [agent_id/id],
          thema.xid('AG' + ca.COD_CAPOAREA)                                    [area_manager_id/id],
          case t.COD_DOC_ORDCLI
          when 'VG' then 'greenvision'
          else 'base.main_company' end                                                  [company_id/id],
          cast(SCONTO_PAGA as float) payment_discount,
          case when tpg.F_CONTRASSEGNO = 1 then 1 else 0 end cod


        FROM T_ORDINI_CLI t
          LEFT JOIN oerp.valute v ON t.COD_VALUTA = v.COD_VALUTA
          LEFT JOIN oerp.listini l ON l.COD_LISTINO = t.COD_LISTINO
          LEFT JOIN oerp.corr_age_cli ca ON t.COD_AGENTE = ca.COD_AGENTE
          LEFT JOIN oerp.x_partner xp ON xp.TIPO_ANAGRAFICO = t.TIPO_ANAGRAFICO AND xp.CAMPO_RICERCA = t.CAMPO_RICERCA
          LEFT JOIN oerp.x_partner xpf ON xpf.TIPO_ANAGRAFICO = t.TIPO_ANAGRAFICO AND xpf.CAMPO_RICERCA = t.CAMPO_RICERCA
          LEFT JOIN oerp.x_partner xpd ON xpd.TIPO_ANAGRAFICO = t.TIPO_ANAGRAFICO AND xpd.CAMPO_RICERCA = t.CAMPO_RICERCA
          LEFT JOIN DESTINAZ_CLI ds ON ds.CAMPO_RICERCA = t.CAMPO_RICERCA AND ds.DESTINAZIONE = t.DESTINAZIONE
          LEFT JOIN DESTINAZ_CLI dsf ON dsf.CAMPO_RICERCA = t.CAMPO_RICERCA AND dsf.DESTINAZIONE = t.DESTINAZIONE_FT
          LEFT JOIN oerp.x_operatori op ON op.COD_OPERATORE = t.COD_OPERATORE
          LEFT JOIN TAB_PAGAMENTI tpg ON tpg.COD_PAGAMENTO = t.COD_PAGAMENTO

    where t.DATA_REGISTRAZ >= '{datai}'
    """


class dettagli(extract_data):
    model = 'sale.order.line'
    chunk_size = 200000
    query = """
    select
        thema.xid(dbo.fn_RifDocumento(t.TIPO_DOC, t.ANNO_DOC, t.NUMERO_DOC) + cast(d.PROGRESSIVO_RIGA as varchar(10))) id,
        thema.xid(dbo.fn_RifDocumento(t.TIPO_DOC, t.ANNO_DOC, t.NUMERO_DOC)) [order_id/id],
        cast(d.PROVVIGIONE as float) [agent_commission],
        cast (
          d.SCONTO + t.SCONTO_CLIENTE - (d.SCONTO * t.SCONTO_CLIENTE / 100)
        as float) as discount_flat,
        CASE
        WHEN d.COD_TIPO_RIGA = 'SP' then 'service'
         WHEN d.COD_TIPO_RIGA = 'ST' then 'shipping'
         when d.COD_TIPO_RIGA = 'AR' then 'collect'
         WHEN R.CAUS_RIGA IN ('01', 'SC') THEN 'sale'
         ELSE
             CASE WHEN R.CAUS_RIGA IN ('SM') THEN  'discount'
                  WHEN R.CAUS_RIGA = 'OI' THEN 'gift'
                  ELSE 'sale'
             END
        END  AS [line_type_id/id],
        case d.UNITA_DI_MISURA
        when 'PZ' then 'product.product_uom_unit'
        when 'PA' then 'thema_product.product_uom_pair'
        end [product_uom/id],
        case d.UNITA_DI_MISURA
        when 'PZ' then 'product.product_uom_unit'
        when 'PA' then 'thema_product.product_uom_pair'
        end [product_uos/id],
        case when p.COD_PRODOTTO is not null then '[' + p.COD_PRODOTTO + '] ' else '' end + isnull(d.DESCR_PRODOTTO, '/') name,
        cast(d.PREZZO_VENDITA as float) price_unit,
        cast(d.QTA_ORDINE as float) product_uom_qty,
        CASE WHEN rtrim(d.COD_PRODOTTO)='SPESE TRASPORTO' THEN 'product_shipping' ELSE thema.xid(rtrim(p.COD_PRODOTTO)) END AS [product_id/id],
        d.PROGRESSIVO_RIGA sequence,
        'it' + i.codice [tax_id/id],
        'make_to_stock' type

    from T_ORDINI_CLI t
    inner join D_ORDINI_CLI d ON d.ID_ORDINE = t.ID_ORDINE
    inner join TAB_TIPO_RIGA R on R.COD_TIPO_RIGA = d.COD_TIPO_RIGA
    LEFT JOIN oerp.prodotti_esportabili p ON p.COD_PRODOTTO = d.COD_PRODOTTO
    LEFT JOIN oerp.iva i ON d.COD_IVA = i.COD_IVA AND debcred = 'D'
    where t.DATA_REGISTRAZ >= '{datai}'
    """


def go(datai):

    t = testate(datai=datai)
    t.bulk_import(context={'ignore_methods': True, 'import': True, 'import_skip_store': True})
    d = dettagli(datai=datai)
    d.bulk_import(context={'ignore_methods': True, 'import': True, 'import_skip_store': True})
    trigger_recompute('sale.order.line', ['value_amount'], domain=repr([('order_id.date_order', '>=', datai)]))
    trigger_recompute('sale.order', ['amount_total', 'amount_payment_discount',
                                     'amount_tax', 'amount_untaxed', 'amount_no_discount'],
                      domain=repr([('date_order', '>=', datai)]))



if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        datai = sys.argv[1]
    else:
        datai = '2014-06-01'
    go(datai)