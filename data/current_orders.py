from extractor import extract_data, client
from thwork import split_work


class cancel(extract_data):
    model = 'stock.picking'

    query = """
        SELECT
          thema.xid('PKOC' + cast(o.ID_ORDINE AS VARCHAR(20))) id,
          'out'                                                            type,
          dbo.fn_RifDocumento(o.TIPO_DOC, o.ANNO_DOC, o.NUMERO_DOC) + '/C' name,
          convert(VARCHAR(100), o.DATA_REGISTRAZ, 126)                     date,
          rtrim(xp.xid + isnull(o.DESTINAZIONE, ''))          [partner_id/id],
          '2binvoiced' invoice_state,
          ts.id [delivery_type_id/id],
          ts.[stock_journal_id/id],
          'cancel' state,
          ts.[location_id/id],
          ts.[location_dest_id/id]
        FROM T_ORDINI_CLI o
          INNER JOIN oerp.x_partner xp ON xp.TIPO_ANAGRAFICO = o.TIPO_ANAGRAFICO AND xp.CAMPO_RICERCA = o.CAMPO_RICERCA_DDT
          INNER JOIN TAB_DOC_ORDCLI toc ON toc.COD_DOC_ORDCLI = o.COD_DOC_ORDCLI
          INNER JOIN oerp.tipi_sped ts ON (substring(ts.id, 2, 100) = toc.COD_DOC_GENERATO AND toc.TIPO_FATTURAZ = 'D')
                                          OR (ts.id = 'tsBC' AND toc.TIPO_FATTURAZ = 'I')

        WHERE o.ID_ORDINE IN (
          SELECT DISTINCT
            ID_ORDINE
          FROM D_ORDINI_CLI do
          inner join oerp.prodotti_esportabili exp on exp.COD_PRODOTTO = do.COD_PRODOTTO
          WHERE isnull(F_SALDO, '') <> ''
        )
    """

    def bulk_import(self, model=None, start=None, end=None, context=None):
        pk = client.get_model('stock.picking')
        irmd = client.get_model('ir.model.data')
        ids = irmd.search([('model', '=', 'stock.picking'), ('name', 'like', 'PKOC%')])
        ids = irmd.read(ids, fields=['res_id'])
        ids = [el['res_id'] for el in ids]

        def worker(data):
            pk.write(data, {'state': 'draft'})
            pk.unlink(data)

        split_work(ids, worker)
        return super(cancel, self).bulk_import(model, start, end, context)


class cancel_moves(extract_data):
    model = 'stock.move'
    query = """
        SELECT
          thema.xid('PKOCD' + cast(d.ID_ORDINE AS VARCHAR(20)) + '/' + cast(d.PROGRESSIVO_RIGA AS VARCHAR(20))) id,
          thema.xid('PKOC' + cast(d.ID_ORDINE AS VARCHAR(20)))                                                  [picking_id/id],
          '[' + rtrim(d.COD_PRODOTTO) + '] ' + isnull(d.DESCR_PRODOTTO, '/')                                                                         name,
          thema.xid(d.COD_PRODOTTO)                                                                             [product_id/id],
          cast(d.QTA_ORDINE - d.QTA_EVASA - d.QTA_IN_SPED - d.QTA_ASS AS FLOAT)                                 [product_qty],
          CASE d.UNITA_DI_MISURA
          WHEN 'PZ' THEN 'product.product_uom_unit'
          WHEN 'PA' THEN 'thema_product.product_uom_pair' END                                                   [product_uom/id],
          isnull(ts.[location_id/id], isnull('dp' + rtrim(
              CASE d.COD_DEPOSITO
              WHEN '001' THEN NULL
              ELSE cast(d.COD_DEPOSITO AS VARCHAR(100))
              END
          ),
                                             'stock.stock_location_stock'))                                     [location_id/id],
          isnull(ts.[location_dest_id/id],
                 'stock.stock_location_customers')                                                              [location_dest_id/id],
          rtrim(xp.xid + isnull(t.DESTINAZIONE, ''))                                                            [partner_id/id],
          'cancel'                                                                                              state,
          CASE d.COD_DEPOSITO
          WHEN 'GV' THEN 'greenvision' ELSE 'main_company' END                                                  [company_id/id]
        FROM D_ORDINI_CLI d
          INNER JOIN oerp.prodotti_esportabili exp ON exp.COD_PRODOTTO = d.COD_PRODOTTO
          INNER JOIN T_ORDINI_CLI t ON t.ID_ORDINE = d.ID_ORDINE
          INNER JOIN TAB_DOC_ORDCLI toc ON toc.COD_DOC_ORDCLI = t.COD_DOC_ORDCLI
          INNER JOIN oerp.tipi_sped ts ON (substring(ts.id, 2, 100) = toc.COD_DOC_GENERATO AND toc.TIPO_FATTURAZ = 'D')
                                          OR (ts.id = 'tsBC' AND toc.TIPO_FATTURAZ = 'I')
          INNER JOIN oerp.x_partner xp ON xp.TIPO_ANAGRAFICO = t.TIPO_ANAGRAFICO AND t.CAMPO_RICERCA_DDT = xp.CAMPO_RICERCA
        WHERE isnull(d.F_SALDO, '') <> ''
        and QTA_ORDINE > QTA_EVASA + QTA_IN_SPED + QTA_ASS

    """


    def bulk_import(self, model=None, start=None, end=None, context=None):
        mv = client.get_model('stock.move')
        irmd = client.get_model('ir.model.data')
        ids = irmd.search([('model', '=', 'stock.move'), ('name', 'like', 'PKOCD%')])
        ids = irmd.read(ids, fields=['res_id'])
        ids = [el['res_id'] for el in ids]

        def worker(data):
            mv.write(data, {'state': 'draft'})
            mv.unlink(data)

        split_work(ids, worker)
        return super(cancel_moves, self).bulk_import(model, start, end, context)


class open(extract_data):
    model = 'stock.picking'
    query = """
        WITH nn AS (
            SELECT
              ID_ORDINE,
              n,
              row_number()
              OVER (PARTITION BY ID_ORDINE
                ORDER BY n DESC) nn
            FROM oerp.ordina_spedizioni
        )
        SELECT
          thema.xid('PKOO' + cast(o.ID_ORDINE AS VARCHAR(20)))                                                   id,
          'out'                                                                                                  type,
          '2binvoiced' invoice_state,
          dbo.fn_RifDocumento(o.TIPO_DOC, o.ANNO_DOC, o.NUMERO_DOC) + '/' + cast(isnull(nn.n, 0) + 1 AS VARCHAR(10)) name,          convert(VARCHAR(100), o.DATA_REGISTRAZ, 126)                                                           date,
          rtrim(xp.xid + isnull(o.DESTINAZIONE,
                                ''))                                                                             [partner_id/id],
          ts.id                                                                                                  [delivery_type_id/id],
          ts.[stock_journal_id/id],
          'confirmed'                                                                                               state,
          ts.[location_id/id],
          ts.[location_dest_id/id]
        FROM T_ORDINI_CLI o
          INNER JOIN oerp.x_partner xp ON xp.TIPO_ANAGRAFICO = o.TIPO_ANAGRAFICO AND xp.CAMPO_RICERCA = o.CAMPO_RICERCA_DDT
          INNER JOIN TAB_DOC_ORDCLI toc ON toc.COD_DOC_ORDCLI = o.COD_DOC_ORDCLI
          INNER JOIN oerp.tipi_sped ts ON (substring(ts.id, 2, 100) = toc.COD_DOC_GENERATO AND toc.TIPO_FATTURAZ = 'D')
                                          OR (ts.id = 'tsBC' AND toc.TIPO_FATTURAZ = 'I')
          LEFT JOIN nn ON nn.nn = 1 AND nn.ID_ORDINE = o.ID_ORDINE
        WHERE o.ID_ORDINE IN (
          SELECT DISTINCT
            do.ID_ORDINE
          FROM D_ORDINI_CLI do
            INNER JOIN oerp.prodotti_esportabili exp ON exp.COD_PRODOTTO = do.COD_PRODOTTO
            inner join T_ORDINI_CLI t ON t.ID_ORDINE = do.ID_ORDINE
          WHERE isnull(do.F_SALDO, '') = ''
                AND QTA_ORDINE > QTA_EVASA + QTA_IN_SPED + QTA_ASS
        and isnull(t.F_BLOCCO, '') = ''
        and isnull(t.F_STAMPATO, '') = 'S'
        )

    """

    def bulk_import(self, model=None, start=None, end=None, context=None):
        pk = client.get_model('stock.picking')
        irmd = client.get_model('ir.model.data')
        ids = irmd.search([('model', '=', 'stock.picking'), ('name', 'like', 'PKOO%')])
        ids = irmd.read(ids, fields=['res_id'])
        ids = [el['res_id'] for el in ids]

        def worker(data):
            pk.write(data, {'state': 'draft'})
            pk.unlink(data)

        split_work(ids, worker)
        return super(open, self).bulk_import(model, start, end, context)


class open_moves(extract_data):
    model = 'stock.move'
    query = """


        SELECT
          thema.xid('PKOOD' + cast(d.ID_ORDINE AS VARCHAR(20)) + '/' + cast(d.PROGRESSIVO_RIGA AS VARCHAR(20))) id,
          thema.xid('PKOO' + cast(d.ID_ORDINE AS VARCHAR(20)))                                                  [picking_id/id],
          '[' + rtrim(d.COD_PRODOTTO) + '] ' + isnull(d.DESCR_PRODOTTO, '/')                                                                         name,
          thema.xid(d.COD_PRODOTTO)                                                                             [product_id/id],
          cast(d.QTA_ORDINE - d.QTA_EVASA - d.QTA_IN_SPED - d.QTA_ASS AS FLOAT)                                 [product_qty],
          CASE d.UNITA_DI_MISURA
          WHEN 'PZ' THEN 'product.product_uom_unit'
          WHEN 'PA' THEN 'thema_product.product_uom_pair' END                                                   [product_uom/id],
          isnull(ts.[location_id/id], isnull('dp' + rtrim(
              CASE d.COD_DEPOSITO
              WHEN '001' THEN NULL
              ELSE cast(d.COD_DEPOSITO AS VARCHAR(100))
              END
          ),
                                             'stock.stock_location_stock'))                                     [location_id/id],
          isnull(ts.[location_dest_id/id],
                 'stock.stock_location_customers')                                                              [location_dest_id/id],
          rtrim(xp.xid + isnull(t.DESTINAZIONE, ''))                                                            [partner_id/id],
          'confirmed'                                                                                              state,
          CASE d.COD_DEPOSITO
          WHEN 'GV' THEN 'greenvision' ELSE 'main_company' END                                                  [company_id/id]
        FROM D_ORDINI_CLI d
          INNER JOIN oerp.prodotti_esportabili exp ON exp.COD_PRODOTTO = d.COD_PRODOTTO
          INNER JOIN T_ORDINI_CLI t ON t.ID_ORDINE = d.ID_ORDINE
          INNER JOIN TAB_DOC_ORDCLI toc ON toc.COD_DOC_ORDCLI = t.COD_DOC_ORDCLI
          INNER JOIN oerp.tipi_sped ts ON (substring(ts.id, 2, 100) = toc.COD_DOC_GENERATO AND toc.TIPO_FATTURAZ = 'D')
                                          OR (ts.id = 'tsBC' AND toc.TIPO_FATTURAZ = 'I')
          INNER JOIN oerp.x_partner xp ON xp.TIPO_ANAGRAFICO = t.TIPO_ANAGRAFICO AND t.CAMPO_RICERCA_DDT = xp.CAMPO_RICERCA
        WHERE isnull(d.F_SALDO, '') = ''
        and QTA_ORDINE > QTA_EVASA + QTA_IN_SPED + QTA_ASS
        and isnull(t.F_BLOCCO, '') = ''
        and isnull(t.F_STAMPATO, '') = 'S'

    """

    def bulk_import(self, model=None, start=None, end=None, context=None):
        mv = client.get_model('stock.move')
        irmd = client.get_model('ir.model.data')
        ids = irmd.search([('model', '=', 'stock.move'), ('name', 'like', 'PKOOD%')])
        ids = irmd.read(ids, fields=['res_id'])
        ids = [el['res_id'] for el in ids]

        def worker(data):
            mv.write(data, {'state': 'draft'})
            mv.unlink(data)

        split_work(ids, worker)
        return super(open_moves, self).bulk_import(model, start, end, context)


class backorders(extract_data):
    model = 'stock.picking'
    query = """
          SET NOCOUNT ON;


          SELECT
            ID_ORDINE,
            typ,
            ID_DOC,
            n,
            row_number()
            OVER (PARTITION BY ID_ORDINE
              ORDER BY n DESC) nn
          INTO #nn
          FROM oerp.ordina_spedizioni
          WHERE (
            typ = 'F' AND ID_DOC IN (SELECT
                                       ID_FATTURA
                                     FROM T_FATT_VENDITA tf
                                     WHERE exists(SELECT
                                                    1
                                                  FROM D_FATT_VENDITA df INNER JOIN oerp.prodotti_esportabili exp
                                                      ON exp.COD_PRODOTTO = df.COD_PRODOTTO
                                                  WHERE df.ID_FATTURA = tf.ID_FATTURA))

          );
          INSERT INTO #nn
            SELECT
              ID_ORDINE,
              typ,
              ID_DOC,
              n,
              row_number()
              OVER (PARTITION BY ID_ORDINE
                ORDER BY n DESC) nn
            FROM oerp.ordina_spedizioni
            WHERE
              (
                typ = 'B' AND ID_DOC IN (
                  SELECT
                    ID_BOLLA
                  FROM T_BOLLE_VENDITA tb
                  WHERE exists(SELECT
                                 1
                               FROM D_BOLLE_VENDITA db INNER JOIN oerp.prodotti_esportabili exp
                                   ON exp.COD_PRODOTTO = db.COD_PRODOTTO
                               WHERE db.ID_BOLLA = tb.ID_BOLLA
                  )
                ));


          SELECT
            thema.xid('PKOC' + cast(o.ID_ORDINE AS VARCHAR(20))) id,
            'PK' + nn.typ + cast(nn.ID_DOC AS VARCHAR(20)) + '/' +
            isnull(cast(nn.ID_ORDINE AS VARCHAR(10)), '')        [backorder_id/id]
          FROM T_ORDINI_CLI o
            INNER JOIN #nn nn ON nn.nn = 1 AND nn.ID_ORDINE = o.ID_ORDINE
          WHERE o.ID_ORDINE IN (
            SELECT DISTINCT
              ID_ORDINE
            FROM D_ORDINI_CLI do
              INNER JOIN oerp.prodotti_esportabili exp ON exp.COD_PRODOTTO = do.COD_PRODOTTO
            WHERE isnull(F_SALDO, '') <> ''
          )
          UNION ALL
          SELECT
            thema.xid('PKOO' + cast(o.ID_ORDINE AS VARCHAR(20))) id,
            'PK' + nn.typ + cast(nn.ID_DOC AS VARCHAR(20)) + '/' +
            isnull(cast(nn.ID_ORDINE AS VARCHAR(10)), '')        [backorder_id/id]
          FROM T_ORDINI_CLI o
            INNER JOIN #nn nn ON nn.nn = 1 AND nn.ID_ORDINE = o.ID_ORDINE
          WHERE o.ID_ORDINE IN (
            SELECT DISTINCT
              do.ID_ORDINE
            FROM D_ORDINI_CLI do
              INNER JOIN oerp.prodotti_esportabili exp ON exp.COD_PRODOTTO = do.COD_PRODOTTO
              INNER JOIN T_ORDINI_CLI t ON t.ID_ORDINE = do.ID_ORDINE
            WHERE isnull(do.F_SALDO, '') = ''
                  AND QTA_ORDINE > QTA_EVASA + QTA_IN_SPED + QTA_ASS
                  AND isnull(t.F_BLOCCO, '') = ''
                  AND isnull(t.F_STAMPATO, '') = 'S'
          )

    """

class link(extract_data):
    model = 'stock.picking'
    query = """
          SELECT
            thema.xid('PKOC' + cast(o.ID_ORDINE AS VARCHAR(20))) id,
            thema.xid(dbo.fn_RifDocumento(o.TIPO_DOC, o.ANNO_DOC, o.NUMERO_DOC)) [sale_id/id],
            thema.xid(dbo.fn_RifDocumento(o.TIPO_DOC, o.ANNO_DOC, o.NUMERO_DOC)) origin
          FROM T_ORDINI_CLI o
          WHERE o.ID_ORDINE IN (
            SELECT DISTINCT
              ID_ORDINE
            FROM D_ORDINI_CLI do
              INNER JOIN oerp.prodotti_esportabili exp ON exp.COD_PRODOTTO = do.COD_PRODOTTO
            WHERE isnull(F_SALDO, '') <> ''
          )
          UNION ALL
          SELECT
            thema.xid('PKOO' + cast(o.ID_ORDINE AS VARCHAR(20))) id,
            thema.xid(dbo.fn_RifDocumento(o.TIPO_DOC, o.ANNO_DOC, o.NUMERO_DOC)) [sale_id/id],
            thema.xid(dbo.fn_RifDocumento(o.TIPO_DOC, o.ANNO_DOC, o.NUMERO_DOC)) origin
          FROM T_ORDINI_CLI o
          WHERE o.ID_ORDINE IN (
            SELECT DISTINCT
              do.ID_ORDINE
            FROM D_ORDINI_CLI do
              INNER JOIN oerp.prodotti_esportabili exp ON exp.COD_PRODOTTO = do.COD_PRODOTTO
              INNER JOIN T_ORDINI_CLI t ON t.ID_ORDINE = do.ID_ORDINE
            WHERE isnull(do.F_SALDO, '') = ''
                  AND QTA_ORDINE > QTA_EVASA + QTA_IN_SPED + QTA_ASS
                  AND isnull(t.F_BLOCCO, '') = ''
                  AND isnull(t.F_STAMPATO, '') = 'S'
          )
    """

class link_line(extract_data):
    model = 'stock.move'
    query = """
        SELECT
          thema.xid('PKOOD' + cast(d.ID_ORDINE AS VARCHAR(20)) + '/' + cast(d.PROGRESSIVO_RIGA AS VARCHAR(20))) id,
          thema.xid(dbo.fn_RifDocumento(t.TIPO_DOC, t.ANNO_DOC, t.NUMERO_DOC) +
                    cast(d.PROGRESSIVO_RIGA AS VARCHAR(10)))                                                    [sale_line_id/id]
        FROM D_ORDINI_CLI d
          INNER JOIN oerp.prodotti_esportabili exp ON exp.COD_PRODOTTO = d.COD_PRODOTTO
          INNER JOIN T_ORDINI_CLI t ON t.ID_ORDINE = d.ID_ORDINE
        WHERE isnull(d.F_SALDO, '') = ''
              AND QTA_ORDINE > QTA_EVASA + QTA_IN_SPED + QTA_ASS
              AND isnull(t.F_BLOCCO, '') = ''
              AND isnull(t.F_STAMPATO, '') = 'S'
        UNION ALL
        SELECT
          thema.xid('PKOCD' + cast(d.ID_ORDINE AS VARCHAR(20)) + '/' + cast(d.PROGRESSIVO_RIGA AS VARCHAR(20))) id,
          thema.xid(dbo.fn_RifDocumento(t.TIPO_DOC, t.ANNO_DOC, t.NUMERO_DOC) +
                    cast(d.PROGRESSIVO_RIGA AS VARCHAR(10)))                                                    [sale_line_id/id]
        FROM D_ORDINI_CLI d
          INNER JOIN oerp.prodotti_esportabili exp ON exp.COD_PRODOTTO = d.COD_PRODOTTO
          INNER JOIN T_ORDINI_CLI t ON t.ID_ORDINE = d.ID_ORDINE
        WHERE isnull(d.F_SALDO, '') <> ''
        and QTA_ORDINE > QTA_EVASA + QTA_IN_SPED + QTA_ASS

    """


def go():
    cc = cancel()
    cc.bulk_import(context={'ignore_methods': True, 'import_skip_store': True, 'import': True})
    ccm = cancel_moves()
    ccm.bulk_import(context={'ignore_methods': True, 'import_skip_store': True, 'import': True})
    cc = open()
    cc.bulk_import(context={'ignore_methods': True, 'import_skip_store': True, 'import': True})
    ccm = open_moves()
    ccm.bulk_import(context={'ignore_methods': True, 'import_skip_store': True, 'import': True})
    bo = backorders()
    bo.bulk_import(context={'ignore_methods': True, 'import_skip_store': True, 'import': True, 'nocreate': True})
    lk = link()
    lk.bulk_import(context={'ignore_methods': True, 'import_skip_store': True, 'import': True, 'nocreate': True})
    lk = link_line()
    lk.bulk_import(context={'ignore_methods': True, 'import_skip_store': True, 'import': True, 'nocreate': True})


if __name__ == '__main__':
    go()