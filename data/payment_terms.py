import db

from extractor import extract_data, client



class payment_terms(extract_data):
    model = 'account.payment.term'
    query = """
        SELECT *
        FROM oerp.pagamenti
    """



def get_payment_term_details():
    with db.session as s:
        pg = s.execute("""
            SELECT
                p.id,
                pg.COD_PAGAMENTO,
                 pg.DES_PAGAMENTO,
                 pg.NUM_RATE_COMP,
                 pg.FLAG_PARTENZA,
                 pg.TIPO_DISTANZA,
                 pg.PERC_1_RATA,
                 pg.NUM_GG_1_RATA,
                 pg.F_IVA_1_RATA,
                 pg.NUM_GG_INTER,
                 pg.PERC_U_RATA,
                 pg.NUM_GG_U_RATA,
                 pg.DOC_CLI,
                 pg.DOC_FOR,
                 pg.SCONTO,
                 pg.COD_PERIODO,
                 pg.DATA_INATTIVO,
                 pg.F_CONTRASSEGNO,
                 pg.SCONTO_CONDIZIONATO,
                 pg.MODO_INCPAG

            FROM oerp.pagamenti p
            inner join TAB_PAGAMENTI pg ON pg.COD_PAGAMENTO = substring(p.id, 3, 100)
            """)
        return list(pg.fetchall())

def expand_payment_term(pg):
    res = []
    # numero rate prima dell'ultima
    nrat = pg.NUM_RATE_COMP - 1
    rat1 = float(pg.PERC_1_RATA) / 100 or 1.0 / (nrat + 1)
    ratz = float(pg.PERC_U_RATA) / 100 or 1.0 / (nrat + 1)
    #prima rata
    res.append({
        'value': 'procent' if nrat else 'balance',
        'value_amount': rat1,
        'days': pg.NUM_GG_1_RATA,
        'days2': -1 if pg.FLAG_PARTENZA == 'F' else 0
    })
    if nrat > 0:
        rem = 1.0 - (rat1 + ratz)
        if nrat > 1:
            rat = rem / (nrat - 1)
        else:
            rat = rem
        days = pg.NUM_GG_1_RATA
        for i in range(nrat -1):
            days += pg.NUM_GG_INTER
            res.append({
                'value': 'procent',
                'value_amount': rat,
                'days': days,
                'days2': 0 if pg.FLAG_PARTENZA == 'F' else -1
            })
        if pg.NUM_GG_U_RATA:
            days += pg.NUM_GG_U_RATA
        else:
            days += pg.NUM_GG_INTER
        res.append({
            'value': 'balance',
            'days': days,
            'days2': 0 if pg.FLAG_PARTENZA == 'F' else -1
        })
    return pg.id, res

def exclude():
    with db.session as s:
        res = s.execute("""
          SELECT per.*,
          thema.xid('PG' + pag.COD_PAGAMENTO) id
          FROM DETT_PERIODI per
          INNER JOIN TAB_PAGAMENTI pag ON pag.COD_PERIODO = per.COD_PERIODO
          """)
        per = list(res.fetchall())
    pagdata = {}
    for p in per:
        pdata = {
            'month_start': p.DATA_DA.month,
            'month_stop': p.DATA_A.month,
            'day_start': p.DATA_DA.day,
            'day_stop': p.DATA_A.day,
            'days': 0,
            'days2': p.DATA_NUOVA.day,
        }
        pagdata.setdefault(p.id, []).append(pdata)
    pgobj = client.get_model('account.payment.term')
    md = client.get_model('ir.model.data')
    for id, data in pagdata.items():
        clean = set()
        for d in data:
            clean.add(tuple(sorted(d.items())))
        data = map(dict, clean)
        pid = md.search([('model', '=', 'account.payment.term'), ('name', '=', id)])[0]
        pid = md.read(pid, fields=['res_id'])['res_id']
        current = pgobj.read(pid, fields=['exclude_ids'])['exclude_ids']
        try:
            pgobj.write(pid, {
                'exclude_ids': [(2, lid) for lid in current] + [(0, 0, v) for v in data]
            })
        except:
            print id, data




def go():
    p = payment_terms()
    p.bulk_import()
    pg = get_payment_term_details()
    pgobj = client.get_model('account.payment.term')
    md = client.get_model('ir.model.data')
    for p in pg:
        pxid, exp = expand_payment_term(p)
        pid = md.search([('model', '=', 'account.payment.term'), ('name', '=', pxid)])[0]
        pid = md.read(pid, fields=['res_id'])['res_id']
        current = pgobj.read(pid, fields=['line_ids'])['line_ids']
        try:
            pgobj.write(pid, {
                'line_ids': [(2, lid) for lid in current] + [(0, 0, v) for v in exp]
            })
        except:
            print pxid, exp
    exclude()


if __name__ == '__main__':
    go()