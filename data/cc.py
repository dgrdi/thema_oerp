from extractor import extract_data, client

class extract_cc(extract_data):
    model = 'thema.cc'
    query = """
    SELECT thema.xid(cc) id,
      cc name
    FROM thema.copie_commissione;
    """

class extract_ccver(extract_data):
    model = 'thema.cc.version'
    query ="""
        SELECT thema.xid(versione) id,
            thema.xid(cc) [cc_id/id],
            thema.xid('L' + listino) [pricelist_id/id],
            versione name
        FROM thema.copie_commissione;
    """

class extract_ccitem(extract_data):
    model = 'thema.cc.item'
    query = """
        SELECT thema.xid(cc.versione + l.COD_PRODOTTO) id,
        thema.xid(cc.versione) [version_id/id],
        thema.xid(l.COD_PRODOTTO) [product_id/id],
        'x' col_label,
        'x' row_label,
        0 price
        FROM thema.copie_commissione cc
        INNER JOIN thema.listini l ON l.LISTINO = cc.listino
        inner join oerp.prodotti_esportabili e on e.COD_PRODOTTO = l.COD_PRODOTTO
    """ #TODO prezzi di prodotti non-occhiale

def recompute_items():
    itm = client.get_model('thema.cc.item')
    itm_ids = itm.search([])
    itm.recompute(itm_ids)

def go():
    cc = extract_cc()
    cc.bulk_import()
    ccver = extract_ccver()
    ccver.bulk_import()
    ccitm = extract_ccitem()
    ccitm.bulk_import()
    recompute_items()

if __name__ == '__main__':
    go()

