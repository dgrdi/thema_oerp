import coll, models, oframes, accessori
import listini, clienti, agenti, invoice
import cc

def load_products():
    coll.go()
    models.go()
    oframes.go()
    accessori.go()

if __name__ == '__main__':
    load_products()
    listini.go()
    clienti.go()
    agenti.go();
    cc.go()
    invoice.go();

