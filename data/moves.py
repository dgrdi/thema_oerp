from extractor import extract_data, client
from thwork import split_work




class moves(extract_data):
    model = 'account.move'
    threads = 20
    query = """
      SELECT
        xa.xid                                                    id,
        dbo.fn_RifDocumento(a.TIPO_DOC, a.ANNO_DOC, a.NUMERO_DOC) [name],
        dbo.fn_RifDocumento(a.TIPO_DOC, a.ANNO_DOC, a.NUMERO_DOC) [ref],
        aj.journal_xid                                            [journal_id/id],
        left(convert(VARCHAR, a.DATA_REGISTRAZ, 20), 10)          date,
        oerp.fn_x_periodo(a.ESERCIZIO, a.DATA_REGISTRAZ)          [period_id/id],
        oz.DES_OPERAZIONE                                         narration,
        CASE a.TIPO_EXTRA
        WHEN 'AS' THEN 1 ELSE 0 END                               provisional,
        case a.TIPO_EXTRA
            when 'AS' then 'draft' else 'posted' end state,
        isnull(ef.bill_receivable, 0) bill_receivable,
        isnull(ef.bill_deposit, 0) bill_deposit,
        isnull(ef.bill_force_deposit, 0) bill_force_deposit,
        ef.bill_date_due,
        ef.clearance_delay
      FROM ARTICOLI a
        INNER JOIN oerp.x_articoli xa ON xa.ID_ARTICOLO = a.ID_ARTICOLO
        INNER JOIN TAB_OPERAZIONI oz ON oz.COD_OPERAZIONE = a.COD_OPERAZIONE
        LEFT JOIN oerp.art_journal aj ON aj.ID_ARTICOLO = a.ID_ARTICOLO
        LEFT JOIN oerp.journals j ON aj.journal_xid = j.id
        LEFT JOIN oerp.effetti ef ON ef.ID_ARTICOLO = a.ID_ARTICOLO
        WHERE a.DATA_REGISTRAZ >= '{datai}'

    """

    def bulk_import(self, model=None, start=None, end=None, context=None):
        mo = client.get_model('account.move')
        ids = mo.search([('provisional', '=', True)])
        def worker(data):
            mo.write(data, {'provisional': False})
        split_work(ids, worker, nthreads=1)
        return super(moves, self).bulk_import(model, start, end, context)

class move_refs(extract_data):
    model = 'account.move'
    query = """
      SELECT
        xid id,
        [bill_deposit_id/id],
        [bill_cancel_id/id],
        [bill_unpaid_id/id]
      FROM oerp.effetti ef
      inner join ARTICOLI a ON a.ID_ARTICOLO = ef.ID_ARTICOLO
      WHERE a.DATA_REGISTRAZ >= '{datai}'

    """
class move_lines(extract_data):
    model = 'account.move.line'

    query = """
        select
        xa.xid id,
        'valid' state,
        axa.xid [move_id/id],
        aj.journal_xid [journal_id/id],
        isnull(pn.DES_AGGIUNTA, '/') name,
        left(convert(varchar, a.DATA_REGISTRAZ, 20), 10) date,
        left(convert(varchar, pn.DATA_REGISTRAZ, 20), 10) date_created,
        left(convert(varchar, sc.DATA_SCADENZA, 20), 10) date_maturity,
        isnull(ef.bill_receivable, 0) bill_receivable,
        isnull(ef.bill_deposit, 0) bill_deposit,
        isnull(fx.xid, cx.xid) [partner_id/id],
        xc.xid [account_id/id],
        cast(case
                when pn.F_DARE_AVERE = 'D' and isnull(sc.IMPORTO_LIRE, pn.IMPORTO_LIRE) > 0
                or pn.F_DARE_AVERE = 'A' and isnull(sc.IMPORTO_LIRE, pn.IMPORTO_LIRE) < 0
                then  abs(isnull(sc.IMPORTO_LIRE, pn.IMPORTO_LIRE))
            else 0 end as float) debit,
        cast(case
                when pn.F_DARE_AVERE = 'A' and isnull(sc.IMPORTO_LIRE, pn.IMPORTO_LIRE) > 0
                or pn.F_DARE_AVERE = 'D' and isnull(sc.IMPORTO_LIRE, pn.IMPORTO_LIRE) < 0
                then  abs(isnull(sc.IMPORTO_LIRE, pn.IMPORTO_LIRE))
            else 0 end as float) credit,
        case
            when pn.F_DARE_AVERE = 'A' and isnull(sc.IMPORTO_LIRE, pn.IMPORTO_LIRE) > 0
                or pn.F_DARE_AVERE = 'D' and isnull(sc.IMPORTO_LIRE, pn.IMPORTO_LIRE) < 0
                then -1
                else 1
        end * cast(isnull(sc.IMPORTO_VALUTA, pn.IMPORTO_VALUTA) as float) amount_currency,
        v.xid [currency_id/id]
        from PRIMA_NOTA pn
        inner join ARTICOLI a on a.ESERCIZIO = pn.ESERCIZIO and a.NUM_ARTICOLO = pn.NUM_ARTICOLO
        inner join oerp.x_articoli axa on axa.ID_ARTICOLO = a.ID_ARTICOLO
        inner join oerp.x_prima_nota xa on xa.ID_PRIMA_NOTA = pn.ID_PRIMA_NOTA
        inner join oerp.x_conti xc on xc.KEY_CONTO = pn.KEY_CONTO
        left join oerp.righe_effetti ef ON ef.ID_PRIMA_NOTA = pn.ID_PRIMA_NOTA
            AND (ef.ID_SCADENZA = xa.ID_SCADENZA or ef.ID_SCADENZA IS NULL AND xa.ID_SCADENZA IS NULL)
        left join MOV_SCADENZE sc ON sc.ID_PRIMA_NOTA = pn.ID_PRIMA_NOTA and sc.ID_SCADENZA = xa.ID_SCADENZA
        left join oerp.x_clienti cx ON cx.CAMPO_RICERCA = pn.CAMPO_RICERCA and pn.TIPO_SEZIONALE = 'C'
        left join oerp.x_fornitori fx ON fx.COD_FORNITORE = pn.CAMPO_RICERCA and pn.TIPO_SEZIONALE = 'F'
        left join oerp.art_journal aj ON aj.ID_ARTICOLO = a.ID_ARTICOLO
        left join oerp.valute v on v.COD_VALUTA = a.COD_VALUTA
        /*
        left join oerp.origini_pn op ON op.ID_PRIMA_NOTA = sc.ID_PRIMA_NOTA and op.ID_SCADENZA = sc.ID_SCADENZA
        left join oerp.x_prima_nota xo ON xo.ID_PRIMA_NOTA = op.ID_PRIMA_NOTA_O and xo.ID_SCADENZA = op.ID_SCADENZA_O
        */
        WHERE a.DATA_REGISTRAZ >= '{datai}'
      """


class move_lines_eff(extract_data):
    model = 'account.move.line'
    chunk_size = 20000
    query = """
        select
        xa.xid id,
        isnull(ef.bill_deposit, 0) bill_deposit,
        isnull(ef.bill_receivable, 0) bill_receivable
        from PRIMA_NOTA pn
        inner join oerp.x_prima_nota xa on xa.ID_PRIMA_NOTA = pn.ID_PRIMA_NOTA
        inner join oerp.righe_effetti ef ON ef.ID_PRIMA_NOTA = pn.ID_PRIMA_NOTA
            and (xa.ID_SCADENZA = ef.ID_SCADENZA or xa.ID_SCADENZA IS NULL and ef.ID_SCADENZA is null)
        where ef.bill_deposit = 1 or ef.bill_receivable=1
          """


class move_line_tax(extract_data):
    model = 'account.move.line.tax'
    query = """
        SELECT "id", "move_line_id/id", "tax_code_id/id", "tax_amount"
        FROM oerp.move_line_tax mlt
        INNER JOIN PRIMA_NOTA pn ON pn.ID_PRIMA_NOTA = mlt.ID_PRIMA_NOTA
        --where pn.DATA_REGISTRAZ >= '2014-04-01'
    """


class riapertura_effetti(extract_data):
    model = 'account.move.line'
    query = """
        SELECT
            xid_i id,
            xid [bill_orig_id/id]
        FROM oerp.riapertura_insoluti;
    """

def do_azienda(azienda, datai):
    m = moves(azienda=azienda, datai=datai)
    m.bulk_import(context={'import': True, 'ignore_methods': True, 'novalidate': True, 'import_skip_store': True})
    m = move_refs(azienda=azienda, datai=datai)
    m.bulk_import(context={'import': True, 'ignore_methods': True, 'novalidate': True, 'import_skip_store': True, 'nocreate': True})
    del m
    ml = move_lines(azienda=azienda, datai=datai)
    ml.bulk_import(context={'import': True, 'ignore_methods': True, 'novalidate': True, 'import_skip_store': True,})
    ml = riapertura_effetti(azienda=azienda)
    ml.bulk_import(context={'import': True, 'ignore_methods': True, 'novalidate': True, 'import_skip_store': True, 'nocreate': True})
    #ml.load(start=0, end=1000, context={'import': True, 'ignore_methods': True})
    del ml
    mlt = move_line_tax(azienda=azienda)
    mlt.bulk_import(context={'ignore_methods': True, 'novalidate': True})


def go(datai='2014-04-01'):
    for az in ['AZIENDA_TOP',
               'AZIENDA_TES',
               'AZIENDA_TEC']:
        do_azienda(az, datai)

if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        datai = sys.argv[1]
    else:
        datai = '2014-04-01'

    go(datai)
