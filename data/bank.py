from extractor import extract_data, client

class bank(extract_data):
    model = 'res.bank'
    query = """
        SELECT thema.xid(rtrim(COD_BANCA)) id,
          DESCR_BANCA name,
          INDIRIZZO street,
          FRAZIONE street2,
          CAP zip,
          case when PROVINCIA in ('AT', 'DE', 'NL') then NULL else s.xid end [state/id],
          case when PROVINCIA in ('AT', 'DE', 'NL') then 'base.' + lower(PROVINCIA) else 'base.it' end [country/id],
          rtrim(CODICE_BIC) bic
        FROM ANAG_BANCHE b
        left join oerp.stati s on s.stato =  b.PROVINCIA and s.nazione = 'IT'

    """

class company_partner_bank(extract_data):
    model = 'res.partner.bank'
    query = """
        select
        thema.xid(rtrim(b.COD_BANCA) + rtrim(b.CONTO_CORRENTE)) id,
        'base.main_company' [company_id/id],
        'base.main_partner' [partner_id/id],
        rtrim(b.CODICE_IBAN) [acc_number],
        j.id [journal_id/id],
        thema.xid(rtrim(b.COD_BANCA)) [bank/id],
        ab.DESCR_BANCA bank_name,
        cast(ab.CODICE_ABI as varchar(20)) abi,
        cast(ab.CODICE_CAB as varchar(20)) cab,
        case TIPO_RAPPORTO
            when 'AF' then 'af'
            when 'CI' then 'ic'
            else 'bank'
        end state
        FROM ANAG_RAPP_BANCHE b
         left join oerp.journals j ON substring(j.[default_credit_account_id/id], 3, 100) = b.KEY_CONTO
         inner join ANAG_BANCHE ab ON ab.COD_BANCA = b.COD_BANCA
    """

class partner_bank(extract_data):
    model = 'res.partner.bank'
    query = """
    SELECT
        thema.xid('BN' + b.TIPO_ANAGRAFICO + b.CAMPO_RICERCA + rtrim(b.COD_BANCA_VS)) id,
        isnull(xf.xid, xc.xid) [partner_id/id],
        rtrim(b.CODICE_IBAN) acc_number,
        rtrim(b.DESCR_BANCA_VS) ban_name,
        cast(b.CODICE_ABI as varchar(20)) abi,
        cast(b.CODICE_CAB as varchar(20)) cab,
        case when b.CODICE_IBAN is null then 'ita' else 'bank' end state
    FROM ANAG_BANCHE_VS b
        left join oerp.x_clienti xc ON b.TIPO_ANAGRAFICO = 'C' and xc.CAMPO_RICERCA = b.CAMPO_RICERCA
        left join oerp.x_fornitori xf ON b.TIPO_ANAGRAFICO = 'F' and xf.COD_FORNITORE = b.CAMPO_RICERCA
    where isnull(xf.xid, xc.xid) is not null
    """

def update_details():
    client.get_model('res.partner.bank').update_bank_details()

def go():
    b = bank()
    b.bulk_import()
    b = company_partner_bank()
    b.bulk_import(context={'ignore_methods': True})
    b = partner_bank()
    b.bulk_import(context={'ignore_methods': True})



if __name__ == '__main__':
    go()