from extractor import extract_data


class extract_coll(extract_data):
    model = 'product.collection'
    query = """
        select thema.xid(rtrim(COD_LINEA)) id,
          DES_LINEA name
        from TAB_LINEA
    """

def go():
    l = extract_coll()
    l.bulk_import()

if __name__ == '__main__':
    go()