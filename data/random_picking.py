from extractor import client
import random

class RandId(object):
    def __init__(self, model):
        self.model = model
        m = client.get_model(self.model)
        self.ex = m.search([])
        self.names = m.read(self.ex, fields=['name'])
        self.names = {x['id']: x['name'] for x in self.names}

    def rand(self):
        r = random.randrange(0, len(self.ex))
        return self.ex[r]


class RandProduct(RandId):
    def __init__(self):
        super(RandProduct, self).__init__('product.product')
        m = client.get_model('product.product')
        self.uom = m.read(self.ex, fields=['uom_id'])
        self.uom = {x['id']: x['uom_id'][0] for x in self.uom}


rpartner = RandId('res.partner')
rproduct = RandProduct()

def generate_picking(out=False):
    pick = client.get_model('stock.picking.out')
    loc = client.get_model('stock.location')
    loc_id = loc.name_search('Stock')[0][0]
    loc_dest_id = loc.name_search('Customers')[0][0]
    loc_out_id = loc.name_search('Out')[0][0]
    assert loc_id and loc_dest_id
    moves = []
    part = rpartner.rand()
    data = {
        'partner_id': part,
        'move_lines': moves,
    }
    for i in range(0, random.randint(1, 1000)):
        p = rproduct.rand()
        line = {
            'product_id': p,
            'name': rproduct.names[p],
            'product_qty': random.randint(1, 100),
            'product_uom': rproduct.uom[p],
            'location_id': loc_id,
            'location_dest_id': loc_dest_id if not out else loc_out_id,
            'partner_id': part,
        }
        moves.append((0, 0, line))
    id = pick.create(data)
    pick.action_confirm([id])


if __name__ == '__main__':
    import sys
    if sys.argv[1] == 'confirm':
        print 'confirm'
        pick = client.get_model('stock.move')
        draft = pick.search([('state', '=', 'draft')])
        pick.write(draft, {'state': 'confirmed'})
    else:
        out = sys.argv[1] == 'out'
        print out
        for i in range(0, int(sys.argv[2])):
            generate_picking(out)
