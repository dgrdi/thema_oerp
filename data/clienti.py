from extractor import  extract_data

class extract_patner_customer_cat(extract_data):
    threads = 1
    model = 'res.partner.category'
    query = """
            SELECT  c.COD_CATEGORIA AS id,
                    c.DES_CATEGORIA AS name,
                    thema.concatenaCODCLICAT(c.COD_CATEGORIA) AS [partner_ids/id]
            FROM TAB_CATEGORIE c
            LEFT JOIN (SELECT * FROM (
                        SELECT
                            thema.xid('CL'+ANAG_CLI.CAMPO_RICERCA) AS id,
                            COD_CATEGORIA
                        FROM ANAG_CLI
                        UNION ALL
                        SELECT
                            thema.xid('CL'+d.CAMPO_RICERCA + d.DESTINAZIONE) AS id,
                            COD_CATEGORIA
                        FROM ANAG_CLI
                        INNER JOIN DESTINAZ_CLI d ON d.CAMPO_RICERCA=ANAG_CLI.CAMPO_RICERCA) t
                       ) a ON a.COD_CATEGORIA=c.COD_CATEGORIA
            GROUP BY c.COD_CATEGORIA, c.DES_CATEGORIA
            """


class extract_patner_customer_anag(extract_data):
    model = 'res.partner'
    threads = 1
    query = """
            SELECT thema.xid('CL'+CAMPO_RICERCA) AS id,
                CAMPO_RICERCA AS code,
                isnull(lang.lang, 'en_US') lang,
                0 tz,
                1 is_company,
                1 AS customer,
                CASE WHEN DATA_INATTIVO IS null then 1 else 0 end AS active,
                RAG_SOCIALE as name,
                RAG_SOCIALE as legal_name,
                '01-01-2014' as birthdate,
                E_MAIL AS email,
                NUMERI_TELEFONO AS phone,
                FAX AS fax,
                INDIRIZZO AS street,
                FRAZIONE AS street2,
                LOCALITA_NEW AS city,
                c.xid AS [country_id/id],
                CAP_NEW AS zip,
                tpc.postal_code AS zip_checked,
                oerp.x_pagamento(COD_PAGAMENTO) [property_payment_term/id],
            --STATE (provincia)
                isnull(tpc.xid, st.xid) [state_id/id],
            --VAT
                rtrim(CASE WHEN c.COD_NAZIONE in ('DE', 'ES','FR','NL','PT','PL')  THEN
                    --stati esteri con vat in CODICE_FISCALE
                    CASE WHEN LOWER(SUBSTRING(CODICE_FISCALE,1,2))=LOWER(VAT_CODE) THEN REPLACE(REPLACE(CODICE_FISCALE, ' ', ''), '-', '') ELSE
                    VAT_CODE+CODICE_FISCALE END
                ELSE
                    CASE WHEN PARTITA_IVA IS NULL OR len(rtrim(PARTITA_IVA))=0 THEN NULL ELSE
                        CASE WHEN c.COD_NAZIONE ='IT' THEN REPLACE('IT'+REPLACE(PARTITA_IVA,'O','0') , ' ', '') ELSE
                            CASE WHEN VAT_CODE IS NOT NULL THEN
                                CASE WHEN LOWER(SUBSTRING(PARTITA_IVA,1,2))=LOWER(VAT_CODE) THEN PARTITA_IVA ELSE
                                VAT_CODE+PARTITA_IVA END
                            ELSE PARTITA_IVA END
                        END
                    END
                END) AS vat,
                CASE WHEN VAT_CODE IS NULL THEN 't' ELSE 'f' END AS vat_subjected,
            -- FISCAL CODE
                CASE WHEN c.COD_NAZIONE in ('DE', 'ES','FR','NL','PT','PL')  THEN
                --stati esteri con vat in CODICE_FISCALE
                PARTITA_IVA ELSE CODICE_FISCALE END AS fiscal_code
            FROM (SELECT CASE WHEN ac.CAP IS NULL OR len(rtrim(ac.CAP))=0 THEN NULL ELSE
                            CASE WHEN ac.COD_NAZIONE ='PT' THEN thema.fn_cap_portogallo_NEW(ac.CAP,ac.LOCALITA) ELSE
                            ac.CAP END END AS CAP_NEW,
                         CASE WHEN ac.LOCALITA IS NULL OR len(rtrim(ac.LOCALITA))=0 THEN NULL ELSE
                            CASE WHEN ac.COD_NAZIONE ='PT' THEN thema.fn_localita_portogallo(ac.LOCALITA) ELSE
                            ac.LOCALITA END END AS LOCALITA_NEW,
                        ocn.country_code AS NAZIONE_NEW,
                        ocn.vat_code AS VAT_CODE, ac.*,
                        ocn.xid
                    FROM ANAG_CLI ac
            -- TABELLA CORRELAZIONE PER IDENTIFICAZIONE NAZIONE CORRETTA
                    LEFT JOIN ANAG_AGENTI ON ac.COD_AGENTE=ANAG_AGENTI.COD_AGENTE
                    LEFT JOIN ANAG_FOR f ON ANAG_AGENTI.COD_FORNITORE=f.CAMPO_RICERCA
                    LEFT JOIN oerp.x_nazioni ocn ON rtrim(ocn.COD_NAZIONE)=rtrim(CASE WHEN ac.COD_NAZIONE  IS NULL THEN f.COD_NAZIONE ELSE ac.COD_NAZIONE  END)
            ) c
            -- TABELLA CORRELAZIONE PER IDENTIFICAZIONE POSTAL_CODE/PROVINCE
            LEFT JOIN (
                SELECT row_number() over (partition by country_code, postal_code
                                          order by postal_code desc, admin_code2 desc, admin_code1 desc) n,
                       country_code,
                       postal_code,
                       s.xid
                FROM thema.postal_codes pc
                INNER JOIN oerp.stati s ON s.nazione = pc.country_code
                    AND (pc.admin_code2 = s.stato OR pc.admin_code1 = s.stato)
            ) tpc ON tpc.n = 1 and rtrim(c.NAZIONE_NEW)=rtrim(tpc.country_code)
                     AND rtrim(c.CAP_NEW)= rtrim(tpc.postal_code)
            LEFT JOIN oerp.stati st ON st.nazione = rtrim(c.NAZIONE_NEW) AND st.stato = upper(c.PROVINCIA)
            LEFT JOIN oerp.languages lang ON c.NAZIONE_NEW = lang.COD_NAZIONE
            """
    exclude =  ('zip_checked', )

class extract_patner_customer_capoconto(extract_data):
    model = 'res.partner'
    threads = 1
    query = """
        SELECT  thema.xid('CL'+CAMPO_RICERCA) AS id,
                thema.xid('CL'+CAPOCONTO) AS [invoice_partner/id]
        FROM ANAG_CLI a
    """

class extract_patner_customer_destinaz(extract_data):
    model = 'res.partner'
    threads = 1
    query = """
            SELECT thema.xid('CL'+CAMPO_RICERCA + DESTINAZIONE) AS id,
                thema.xid('CL'+CAMPO_RICERCA) AS [parent_id/id],
                isnull(lang.lang, 'en_US') lang,
                CAMPO_RICERCA+'/'+DESTINAZIONE AS code,
                CASE WHEN type<>'delivery' THEN
                    CASE WHEN TIPO_INDIRIZZO='T' OR TIPO_INDIRIZZO='I' THEN 'other'
                    ELSE 'contact' END
                ELSE type END type,
                --isnull(lingue.lingua, 'it_IT') lang,
                case COD_NAZIONE when 'TE' then 'thema_canaria' when 'ES' then 'thema_spagna' else 'base.main_company' end [company_id/id],
                0 tz,
                0 is_company,
                1 AS customer,
                CASE WHEN DATA_INATTIVO IS null then 1 else 0 end AS active,
                RAG_SOCIALE as name,
                RAG_SOCIALE as legal_name,
                '01-01-2014' as birthdate,
                E_MAIL AS email,
                NUMERI_TELEFONO AS phone,
                FAX AS fax,
                INDIRIZZO AS street,
                FRAZIONE AS street2,
                LOCALITA_NEW AS city,
                c.xid AS [country_id/id],
                CAP_NEW AS zip,
            --STATE (provincia)
                isnull(tpc.xid, st.xid) [state_id/id]
            FROM (SELECT CASE WHEN ac.CAP IS NULL OR len(rtrim(ac.CAP))=0 THEN NULL ELSE
                            CASE WHEN ac.COD_NAZIONE ='PT' THEN thema.fn_cap_portogallo_NEW(ac.CAP,ac.LOCALITA) ELSE
                            ac.CAP END END AS CAP_NEW,
                         CASE WHEN ac.LOCALITA IS NULL OR len(rtrim(ac.LOCALITA))=0 THEN NULL ELSE
                            CASE WHEN ac.COD_NAZIONE ='PT' THEN thema.fn_localita_portogallo(ac.LOCALITA) ELSE
                            ac.LOCALITA END END AS LOCALITA_NEW,
                        ocn.country_code AS NAZIONE_NEW,
                        ocn.vat_code AS VAT_CODE, ac.*,
                        ocn.xid
                    FROM
                        (SELECT
                            a.CAMPO_RICERCA,
                            d.DESTINAZIONE,
                            d.TIPO_INDIRIZZO,
                            CASE WHEN DESTINAZ_BASE IS NULL OR DESTINAZ_BASE<>d.DESTINAZIONE
                                THEN 'contact' ELSE 'delivery' END AS type,
                            d.RAG_SOCIALE AS RAG_SOCIALE,
                            d.NUMERI_TELEFONO AS NUMERI_TELEFONO,
                            d.FAX AS FAX,
                            d.INDIRIZZO AS INDIRIZZO,
                            d.FRAZIONE AS FRAZIONE,
                            d.LOCALITA AS LOCALITA,
                            d.CAP AS CAP,
                            d.PROVINCIA AS PROVINCIA,
                            ISNULL(d.COD_NAZIONE, a.COD_NAZIONE) AS COD_NAZIONE,
                            d.E_MAIL AS E_MAIL,
                            d.DATA_INATTIVO AS DATA_INATTIVO,
                            a.COD_CATEGORIA AS COD_CATEGORIA,
                            a.COD_AGENTE
                         FROM ANAG_CLI a
                         INNER JOIN DESTINAZ_CLI d ON d.CAMPO_RICERCA=a.CAMPO_RICERCA)ac
            -- TABELLA CORRELAZIONE PER IDENTIFICAZIONE NAZIONE CORRETTA
                    LEFT JOIN oerp.x_nazioni ocn ON rtrim(ocn.COD_NAZIONE)=rtrim(ac.COD_NAZIONE)
            ) c
            -- TABELLA CORRELAZIONE PER IDENTIFICAZIONE POSTAL_CODE/PROVINCE
            LEFT JOIN (
                SELECT row_number() over (partition by country_code, postal_code
                                          order by postal_code desc, admin_code2 desc, admin_code1 desc) n,
                       country_code,
                       postal_code,
                       s.xid
                FROM thema.postal_codes pc
                INNER JOIN oerp.stati s ON s.nazione = pc.country_code
                    AND (pc.admin_code2 = s.stato OR pc.admin_code1 = s.stato)
            ) tpc ON tpc.n = 1 and rtrim(c.NAZIONE_NEW)=rtrim(tpc.country_code)
                     AND rtrim(c.CAP_NEW)= rtrim(tpc.postal_code)
            LEFT JOIN oerp.stati st ON st.nazione = rtrim(c.NAZIONE_NEW) AND st.stato = upper(c.PROVINCIA)
            LEFT JOIN oerp.languages lang ON c.NAZIONE_NEW = lang.COD_NAZIONE
    """
##
class extract_patner_customer_aggr(extract_data):
    threads = 1
    model = 'res.partner'
    query = """
            SELECT  thema.xid('CL'+ CAMPO_RICERCA) AS id,
                    thema.xid('CL'+ COD_CLIENTE_PADRE) AS [parent_id/id],
                    0 AS is_company
            FROM ANAG_CLI
            JOIN (SELECT *
                  FROM oerp.corr_clienti
                  WHERE COD_CLIENTE<>COD_CLIENTE_PADRE
                  ) cc ON CAMPO_RICERCA=COD_CLIENTE
            """

class extract_patner_customer_agenti(extract_data):
    threads = 1
    model = 'res.partner'
    query = """
            SELECT thema.xid('FO'+CAMPO_RICERCA) AS id,
                CAMPO_RICERCA AS code,
                isnull(lang.lang, 'en_US') lang,
                --isnull(lingue.lingua, 'it_IT') lang,
                0 tz,
                0 is_company,
                1 AS customer,
                1 AS supplier,
                RAG_SOCIALE as name,
                RAG_SOCIALE as legal_name,
                '01-01-2014' as birthdate,
                E_MAIL AS email,
                NUMERI_TELEFONO AS phone,
                FAX AS fax,
                INDIRIZZO AS street,
                FRAZIONE AS street2,
                LOCALITA_NEW AS city,
                c.xid AS [country_id/id],
                CAP_NEW AS zip,
                tpc.postal_code AS zip_checked,
            --STATE (provincia)
                isnull(tpc.xid, st.xid) [state_id/id],
            --VAT
                CASE WHEN c.COD_NAZIONE ='DE' OR COD_NAZIONE='ES' OR COD_NAZIONE='FR' OR COD_NAZIONE='NL' OR COD_NAZIONE='PT'  OR COD_NAZIONE='PL'  THEN
                    --stati esteri con vat in CODICE_FISCALE
                    CASE WHEN LOWER(SUBSTRING(CODICE_FISCALE,1,2))=LOWER(VAT_CODE) THEN REPLACE(REPLACE(CODICE_FISCALE, ' ', ''), '-', '') ELSE
                    VAT_CODE+CODICE_FISCALE END
                ELSE
                    CASE WHEN PARTITA_IVA IS NULL OR len(rtrim(PARTITA_IVA))=0 THEN NULL ELSE
                        CASE WHEN c.COD_NAZIONE ='IT' THEN REPLACE('IT'+REPLACE(PARTITA_IVA,'O','0') , ' ', '') ELSE
                            CASE WHEN VAT_CODE IS NOT NULL THEN
                                CASE WHEN LOWER(SUBSTRING(PARTITA_IVA,1,2))=LOWER(VAT_CODE) THEN PARTITA_IVA ELSE
                                VAT_CODE+PARTITA_IVA END
                            ELSE PARTITA_IVA END
                        END
                    END
                END AS vat,
                CASE WHEN VAT_CODE IS NULL THEN 't' ELSE 'f' END AS vat_subjected,
            -- FISCAL CODE
                CASE WHEN c.COD_NAZIONE ='DE' OR COD_NAZIONE='ES' OR COD_NAZIONE='FR' OR COD_NAZIONE='NL' OR COD_NAZIONE='PT'  OR COD_NAZIONE='PL'  THEN
                --stati esteri con vat in CODICE_FISCALE
                PARTITA_IVA ELSE CODICE_FISCALE END AS fiscal_code
            FROM (SELECT CASE WHEN ac.CAP IS NULL OR len(rtrim(ac.CAP))=0 THEN NULL ELSE
                            CASE WHEN ac.COD_NAZIONE ='PT' THEN thema.fn_cap_portogallo_NEW(ac.CAP,ac.LOCALITA) ELSE
                            ac.CAP END END AS CAP_NEW,
                         CASE WHEN ac.LOCALITA IS NULL OR len(rtrim(ac.LOCALITA))=0 THEN NULL ELSE
                            CASE WHEN ac.COD_NAZIONE ='PT' THEN thema.fn_localita_portogallo(ac.LOCALITA) ELSE
                            ac.LOCALITA END END AS LOCALITA_NEW,
                        ocn.country_code AS NAZIONE_NEW,
                        ocn.vat_code AS VAT_CODE, ac.*,
                        ocn.xid
                    FROM ANAG_FOR ac
            -- TABELLA CORRELAZIONE PER IDENTIFICAZIONE NAZIONE CORRETTA
                    INNER JOIN (SELECT COD_FORNITORE FROM thema.CORR_AGE_CLI WHERE COD_CLIENTE IS NULL) corr ON corr.COD_FORNITORE=ac.CAMPO_RICERCA
                    LEFT JOIN oerp.x_nazioni ocn ON rtrim(ocn.COD_NAZIONE)=rtrim(ac.COD_NAZIONE)
            ) c
            -- TABELLA CORRELAZIONE PER IDENTIFICAZIONE POSTAL_CODE/PROVINCE
            LEFT JOIN (
                SELECT row_number() over (partition by country_code, postal_code
                                          order by postal_code desc, admin_code2 desc, admin_code1 desc) n,
                       country_code,
                       postal_code,
                       s.xid
                FROM thema.postal_codes pc
                INNER JOIN oerp.stati s ON s.nazione = pc.country_code
                    AND (pc.admin_code2 = s.stato OR pc.admin_code1 = s.stato)
            ) tpc ON tpc.n = 1 and rtrim(c.NAZIONE_NEW)=rtrim(tpc.country_code)
                     AND rtrim(c.CAP_NEW)= rtrim(tpc.postal_code)
            LEFT JOIN oerp.stati st ON st.nazione = rtrim(c.NAZIONE_NEW) AND st.stato = upper(c.PROVINCIA)
            LEFT JOIN oerp.languages lang ON c.NAZIONE_NEW = lang.COD_NAZIONE
    """

class extract_patner_supplier(extract_data):
    threads = 1
    model = 'res.partner'
    query = """
            SELECT  thema.xid('FO'+CAMPO_RICERCA) AS id,
                    CAMPO_RICERCA AS code,
                    isnull(lang.lang, 'en_US') lang,
                    --isnull(lingue.lingua, 'it_IT') lang,
                    0 tz,
                    CASE WHEN COD_CLIENTE IS NULL THEN 0 ELSE 1 END AS customer,
                    1 AS supplier,
                    CASE WHEN COD_CLIENTE IS NULL THEN 1 ELSE 0 END AS  is_company,
                    CASE WHEN import_dest=1 THEN thema.xid('CL'+COD_CLIENTE)  ELSE NULL END AS  [parent_id/id],
                    CASE WHEN DATA_INATTIVO IS null then 1 else 0 end AS active,
                    RAG_SOCIALE as name,
                    RAG_SOCIALE as legal_name,
                    '01-01-2014' as birthdate,
                    E_MAIL AS email,
                    NUMERI_TELEFONO AS phone,
                    FAX AS fax,
                    INDIRIZZO AS street,
                    FRAZIONE AS street2,
                    LOCALITA_NEW AS city,
                    c.xid AS [country_id/id],
                    CAP_NEW AS zip,
                    tpc.postal_code AS zip_checked,
                    --STATE (provincia)
                    isnull(tpc.xid, st.xid) [state_id/id],
                    --VAT
                    CASE WHEN c.COD_NAZIONE ='DE' OR COD_NAZIONE='ES' OR COD_NAZIONE='FR' OR COD_NAZIONE='NL' OR COD_NAZIONE='PT'  OR COD_NAZIONE='PL'  THEN
                      --stati esteri con vat in CODICE_FISCALE
                      CASE WHEN LOWER(SUBSTRING(CODICE_FISCALE,1,2))=LOWER(VAT_CODE) THEN REPLACE(REPLACE(CODICE_FISCALE, ' ', ''), '-', '') ELSE
                      VAT_CODE+CODICE_FISCALE END
                    ELSE
                      CASE WHEN PARTITA_IVA IS NULL OR len(rtrim(PARTITA_IVA))=0 THEN NULL ELSE
                        CASE WHEN c.COD_NAZIONE ='IT' THEN REPLACE('IT'+REPLACE(PARTITA_IVA,'O','0') , ' ', '') ELSE
                            CASE WHEN VAT_CODE IS NOT NULL THEN
                                CASE WHEN LOWER(SUBSTRING(PARTITA_IVA,1,2))=LOWER(VAT_CODE) THEN PARTITA_IVA ELSE
                                VAT_CODE+PARTITA_IVA END
                            ELSE PARTITA_IVA END
                        END
                      END
                    END AS vat,
                    CASE WHEN VAT_CODE IS NULL THEN 't' ELSE 'f' END AS vat_subjected,
                    -- FISCAL CODE
                    CASE WHEN c.COD_NAZIONE ='DE' OR COD_NAZIONE='ES' OR COD_NAZIONE='FR' OR COD_NAZIONE='NL' OR COD_NAZIONE='PT'  OR COD_NAZIONE='PL'  THEN
                    --stati esteri con vat in CODICE_FISCALE
                    PARTITA_IVA ELSE CODICE_FISCALE END AS fiscal_code
              FROM (SELECT CASE WHEN af.CAP IS NULL OR len(rtrim(af.CAP))=0 THEN NULL ELSE
                            CASE WHEN af.COD_NAZIONE ='PT' THEN thema.fn_cap_portogallo_NEW(af.CAP,af.LOCALITA) ELSE
                            af.CAP END END AS CAP_NEW,
                         CASE WHEN af.LOCALITA IS NULL OR len(rtrim(af.LOCALITA))=0 THEN NULL ELSE
                            CASE WHEN af.COD_NAZIONE ='PT' THEN thema.fn_localita_portogallo(af.LOCALITA) ELSE
                            af.LOCALITA END END AS LOCALITA_NEW,
                        ocn.country_code AS NAZIONE_NEW,
                        ocn.vat_code AS VAT_CODE, af.*,
                        ocn.xid,
                        COD_CLIENTE, cf.xid AS idPartner, import_dest
                    FROM ANAG_FOR af
                    JOIN (SELECT * FROM oerp.fornitori WHERE COD_CLIENTE IS NULL OR import_dest=1) cf ON cf.COD_FORNITORE=af.CAMPO_RICERCA
            -- TABELLA CORRELAZIONE PER IDENTIFICAZIONE NAZIONE CORRETTA
                    LEFT JOIN oerp.x_nazioni ocn ON rtrim(ocn.COD_NAZIONE)=rtrim(af.COD_NAZIONE)
            ) c
            -- TABELLA CORRELAZIONE PER IDENTIFICAZIONE POSTAL_CODE/PROVINCE
            LEFT JOIN (
                SELECT row_number() over (partition by country_code, postal_code
                                          order by postal_code desc, admin_code2 desc, admin_code1 desc) n,
                       country_code,
                       postal_code,
                       s.xid
                FROM thema.postal_codes pc
                INNER JOIN oerp.stati s ON s.nazione = pc.country_code
                    AND (pc.admin_code2 = s.stato OR pc.admin_code1 = s.stato)
            ) tpc ON tpc.n = 1 and rtrim(c.NAZIONE_NEW)=rtrim(tpc.country_code)
                     AND rtrim(c.CAP_NEW)= rtrim(tpc.postal_code)
            LEFT JOIN oerp.stati st ON st.nazione = rtrim(c.NAZIONE_NEW) AND st.stato = upper(c.PROVINCIA)
            LEFT JOIN oerp.languages lang ON c.NAZIONE_NEW = lang.COD_NAZIONE
    """
class extract_patner_supplier_existing(extract_data):
    threads = 1
    model = 'res.partner'
    query = """
            SELECT  thema.xid('CL'+CAMPO_RICERCA) AS id,
                    1 AS supplier
            FROM ANAG_CLI c
            JOIN (SELECT * FROM oerp.fornitori
                 WHERE COD_CLIENTE IS NOT NULL AND (import_dest is null OR import_dest!=1)) cf
                 ON c.CAMPO_RICERCA=cf.COD_CLIENTE
            """

class extract_patner_customer_pl(extract_data):
    threads = 1
    model = 'res.partner'
    query = """
        SELECT  thema.xid('CL'+ANAG_CLI.CAMPO_RICERCA) AS id,
                thema.xid( CASE WHEN LISTINI.CAMPO_RICERCA IS NOT NULL
                        THEN LISTINI.TIPO_LISTINO+LISTINI.CAMPO_RICERCA
                        ELSE 'L'+ANAG_CLI.COD_LISTINO END) AS [property_product_pricelist/id]
        FROM ANAG_CLI
        LEFT JOIN LISTINI ON  ANAG_CLI.CAMPO_RICERCA=LISTINI.CAMPO_RICERCA
        WHERE ANAG_CLI.CAMPO_RICERCA IN (SELECT CAMPO_RICERCA FROM LISTINI)
            OR ANAG_CLI.COD_LISTINO IN (SELECT CAMPO_RICERCA FROM LISTINI)
    """


class extract_patner_customer_comment(extract_data):
    threads = 1
    model = 'res.partner'
    query = """
        SELECT  thema.xid('CL'+ANAG_CLI.CAMPO_RICERCA) AS id,
                thema.concatenaTEXTDOCS(ID_ORIGINE)AS comment
        FROM ANAG_CLI
        JOIN (SELECT * FROM TEXTDOCS WHERE TABELLA_ORIGINE='ANAG_CLI') t ON ID_UNIQUE=ID_ORIGINE
        GROUP BY CAMPO_RICERCA, RAG_SOCIALE, ID_ORIGINE
    """

class extract_patner_customer_update(extract_data):
    model = 'res.partner'
    threads = 1
    query = """
        SELECT * FROM (
            SELECT
                thema.xid('CL'+ANAG_CLI.CAMPO_RICERCA) AS id
            FROM ANAG_CLI
            UNION ALL
            SELECT
                thema.xid('CL'+d.CAMPO_RICERCA + d.DESTINAZIONE) AS id
            FROM ANAG_CLI a
            INNER JOIN DESTINAZ_CLI d ON d.CAMPO_RICERCA=a.CAMPO_RICERCA
        ) t
    """


class extract_patner_customer_notes(extract_data):
    model = 'partner.note.default'
    threads = 1
    query = """
            SELECT  thema.xid('CL'+ CAMPO_RICERCA + '_'+RIGHT('00000'+ CONVERT(VARCHAR,ROW_NUMBER() OVER(PARTITION BY CAMPO_RICERCA ORDER BY o_i,o_h,o_f, d_i, d_h, d_f, i_i, i_h, i_f)),3) ) AS id,
                    thema.xid('CL'+ CAMPO_RICERCA) AS [partner_id/id],
                    o_i order_internal,
                    o_h order_header,
                    o_f order_footer,

                    d_i delivery_internal,
                    d_h delivery_header,
                    d_f delivery_footer,

                    i_i invoice_internal,
                    i_h invoice_header,
                    i_f invoice_footer,

                    thema.concatenaCOMMENTI(CAMPO_RICERCA)AS note
            FROM ( SELECT CAMPO_RICERCA,
                    RIGA,
                    MIN(PROG) PROG,
                    CASE WHEN SUM(o_i)>=1 THEN 1 ELSE 0 END o_i,
                    CASE WHEN SUM(o_h)>=1 THEN 1 ELSE 0 END o_h,
                    CASE WHEN SUM(o_f)>=1 THEN 1 ELSE 0 END o_f,
                    CASE WHEN SUM(d_i)>=1 THEN 1 ELSE 0 END d_i,
                    CASE WHEN SUM(d_h)>=1 THEN 1 ELSE 0 END d_h,
                    CASE WHEN SUM(d_f)>=1 THEN 1 ELSE 0 END d_f,
                    CASE WHEN SUM(i_i)>=1 THEN 1 ELSE 0 END i_i,
                    CASE WHEN SUM(i_h)>=1 THEN 1 ELSE 0 END i_h,
                    CASE WHEN SUM(i_f)>=1 THEN 1 ELSE 0 END i_f
            FROM
            -- TOGLIE RIGHE DUPLICATE
                (SELECT DISTINCT
                    C.CAMPO_RICERCA,
                    MIN(PROG_COMMENTO) PROG,
                    0 AS o_i,
                    CASE WHEN PROC_DEST='OC' AND TIPO_COMMENTO='T' THEN 1 ELSE 0 END AS o_h,
                    CASE WHEN PROC_DEST='OC' AND TIPO_COMMENTO='P' THEN 1 ELSE 0 END AS o_f,

                    0 AS d_i,
                    CASE WHEN PROC_DEST='BC' AND TIPO_COMMENTO='T' THEN 1 ELSE 0 END AS d_h,
                    CASE WHEN PROC_DEST='BC' AND TIPO_COMMENTO='P' THEN 1 ELSE 0 END AS d_f,

                    0 AS i_i,
                    CASE WHEN PROC_DEST='FC' AND TIPO_COMMENTO='T' THEN 1 ELSE 0 END AS i_h,
                    CASE WHEN PROC_DEST='FC' AND TIPO_COMMENTO='P' THEN 1 ELSE 0 END AS i_f,
                    RTRIM(RIGA_COMMENTO) AS RIGA
                    FROM COMMENTI_DOCS_CLIFOR C
                    INNER JOIN ANAG_CLI ON C.CAMPO_RICERCA=ANAG_CLI.CAMPO_RICERCA
                   GROUP BY C.CAMPO_RICERCA, PROC_DEST, TIPO_COMMENTO, RTRIM(RIGA_COMMENTO)
                   HAVING RTRIM(RIGA_COMMENTO) IS NOT NULL AND RTRIM(RIGA_COMMENTO)!=''
                    ) a
            -- TOGLIE RIGHE DUPLICATE PER DIVERSE DESTINAZIONI
                GROUP BY CAMPO_RICERCA, RIGA
                ) b
            -- CONCATENA I VALORI DI RIGA MULTIPLI
            GROUP BY CAMPO_RICERCA,o_i,o_h,o_f, d_i, d_h, d_f, i_i, i_h, i_f
    """


def go_anag():
    # ANAGRAFICA BASE
    anag = extract_patner_customer_anag()
    anag.bulk_import(context={'import': True, 'ignore_methods': True, 'no_store_compute': True})

    # AGGIUNTA RIFERIMENTO AL CAPOCONTO
    capoc = extract_patner_customer_capoconto()
    capoc.bulk_import(context={'import': True, 'ignore_methods': True, 'no_store_compute': True})

    # AGGIUNTA RIFERIMENTO AL LISTINO
    pl = extract_patner_customer_pl()
    pl.bulk_import(context={'import': True, 'ignore_methods': True, 'no_store_compute': True})

    # AGGIUNTA ANAGRAFICA CONTATTI (DA DESTINAZINI)
    dest = extract_patner_customer_destinaz()
    dest.bulk_import(context={'import': True, 'ignore_methods': True, 'no_store_compute': True})

    # AGGREGAZIONE CLIENTE CON DOWNGRADE DI PARTNER A CONTATTI SULLA BASE DELLA PARTITA IVA COMUNE
    agcli = extract_patner_customer_aggr()
    agcli.bulk_import(context={'import': True, 'ignore_methods': True, 'no_store_compute': True})

    # AGGIUNTA FORNITORI NON RICONDUCIBILI A CLIENTI
    supp= extract_patner_supplier()
    supp.bulk_import(context={'import': True, 'ignore_methods': True, 'no_store_compute': True})

    # AGGIUNTA QUALIFICA DI FORNITORI A CLIENTI GIA' IMPORTATI
    supp_ex= extract_patner_supplier_existing()
    supp_ex.bulk_import(context={'import': True, 'ignore_methods': True, 'no_store_compute': True})

    # AGGIUNTA DELLE NOTE INTERNE SUL PARTNER
    comm = extract_patner_customer_comment()
    comm.bulk_import(context={'import': True, 'ignore_methods': True, 'no_store_compute': True})

    # AGGIUNTA DEGLI AGENTI - TOLTA PERCHE' GIA' IMPORTATI CON I FORNITORI
    # agenti=extract_patner_customer_agenti();
    # agenti.bulk_import(context={'import': True, 'ignore_methods': True})

    # NIENTE - UPDATE
    update= extract_patner_customer_update()
    update.bulk_import(context={'import': True, 'ignore_methods': True, 'recalculate_tree':True})

def go_categ():
    # AGGIUNTA DELLE NOTE CATEGORIE PER I CLIENTI
    categ = extract_patner_customer_cat()
#    categ.bulk_import(context={'import': True, 'ignore_methods': True})
    categ.load()

def go_notes():
    # AGGIUNTA DELLE NOTE PREDEFINITE PER DOCUMENTI
    note= extract_patner_customer_notes()
    note.bulk_import(context={'import': True, 'ignore_methods': True})

def go():
    go_anag()
    go_categ()
    go_notes()


if __name__ == '__main__':
    go()
    #go_categ()
