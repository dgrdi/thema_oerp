from extractor import  extract_data
from trigger_recompute import trigger_recompute

class stock_picking(extract_data):
    model = 'stock.picking'
    query = """
        WITH dd AS (
            SELECT DISTINCT
              ID_ORDINE,
              ID_BOLLA
            FROM oerp.spedizioni_bolle2
        ), ff AS (
            SELECT DISTINCT
              ID_ORDINE,
              ID_FATTURA
            FROM oerp.spedizioni_fatture2
        ) SELECT
            'PKB' + cast(ID_BOLLA AS VARCHAR(20)) + '/' + isnull(cast(dd.ID_ORDINE AS VARCHAR(20)), '') id,
            dbo.fn_RifDocumento(t.TIPO_DOC, t.ANNO_DOC, t.NUMERO_DOC)                                   [sale_id/id],
            dbo.fn_RifDocumento(t.TIPO_DOC, t.ANNO_DOC, t.NUMERO_DOC)                                   [origin]
          FROM dd
            INNER JOIN T_ORDINI_CLI t ON t.ID_ORDINE = dd.ID_ORDINE
          UNION ALL
         SELECT
            'PKF' + cast(ID_FATTURA AS VARCHAR(20)) + '/' + isnull(cast(dd.ID_ORDINE AS VARCHAR(20)), '') id,
            dbo.fn_RifDocumento(t.TIPO_DOC, t.ANNO_DOC, t.NUMERO_DOC)                                   [sale_id/id],
            dbo.fn_RifDocumento(t.TIPO_DOC, t.ANNO_DOC, t.NUMERO_DOC)                                   [origin]
          FROM ff dd
            INNER JOIN T_ORDINI_CLI t ON t.ID_ORDINE = dd.ID_ORDINE
    """


class stock_move(extract_data):
    model = 'stock.move'
    chunk_size = 20000
    query = """
        SELECT
          'PKBD' + cast(b.ID_BOLLA AS VARCHAR(20)) + '/' + cast(b.PROGRESSIVO_RIGA AS VARCHAR(10))       id,
          dbo.fn_RifDocumento(o.TIPO_DOC, o.ANNO_DOC, o.NUMERO_DOC) + cast(b.RIGA_ORDINE AS VARCHAR(10)) [sale_line_id/id]
        FROM oerp.spedizioni_bolle2 b
          INNER JOIN T_ORDINI_CLI o ON o.ID_ORDINE = b.ID_ORDINE
        UNION ALL
        SELECT
          'PKFD' + cast(b.ID_FATTURA AS VARCHAR(20)) + '/' + cast(b.PROGRESSIVO_RIGA AS VARCHAR(10))     id,
          dbo.fn_RifDocumento(o.TIPO_DOC, o.ANNO_DOC, o.NUMERO_DOC) + cast(b.RIGA_ORDINE AS VARCHAR(10)) [sale_line_id/id]
        FROM oerp.spedizioni_fatture2 b
          INNER JOIN T_ORDINI_CLI o ON o.ID_ORDINE = b.ID_ORDINE
    """



def go():
    p = stock_picking()
    p.bulk_import(context={'ignore_methods': True, 'nocreate': True, 'import_skip_store': True})
    del p
    m = stock_move()
    m.bulk_import(context={'ignore_methods': True, 'nocreate': True, 'import_skip_store': True})
    trigger_recompute('stock.move', ['origin'])


if __name__ == '__main__':
    go()