from extractor import  extract_data


class extract_thema_agent_list(extract_data):
    model = 'thema.agent'
    query = """
        SELECT  thema.xid('AG' + a.COD_AGENTE) AS id,
                ltrim(a.RAG_SOCIALE) AS name,
                thema.xid(CASE WHEN ca.COD_CLIENTE IS NULL
                            THEN 'FO'+ca.COD_FORNITORE
                            ELSE 'CL'+ca.COD_CLIENTE  END) AS [partner_id/id]
        FROM ANAG_AGENTI a
        INNER JOIN thema.CORR_AGE_CLI ca ON a.COD_AGENTE=ca.COD_AGENTE
    """

class extract_thema_agent_capoarea(extract_data):
    model = 'thema.agent'
    query = """
        SELECT  thema.xid('AG' + a.COD_AGENTE) AS id,
        --	    '' AS [contract_id/id],
                thema.xid('AG' + ca.COD_CAPOAREA) AS [area_manager_id/id]
        FROM ANAG_AGENTI a
        INNER JOIN thema.CORR_AGE_CLI ca ON a.COD_AGENTE=ca.COD_AGENTE
    """
class extract_thema_agent_for_partner(extract_data):
    # collegamento agente per il cliente e aggiunta del flag anche su fornitore per il cliente
    model = 'res.partner'
    query = """
        SELECT
            thema.xid('CL'+CAMPO_RICERCA) AS id,
            CASE WHEN ca.COD_CLIENTE IS NULL THEN 0 ELSE 1 END AS supplier ,
            thema.xid('AG'+a.COD_AGENTE) AS [agent_id/id]
        FROM ANAG_CLI a
        LEFT JOIN thema.CORR_AGE_CLI ca ON a.CAMPO_RICERCA=ca.COD_CLIENTE
    """

def go():
    agent=extract_thema_agent_list();
    agent.bulk_import(context={'import': True, 'ignore_methods': True})

    agentca=extract_thema_agent_capoarea();
    agentca.bulk_import(context={'import': True, 'ignore_methods': True})

    partag=extract_thema_agent_for_partner();
    partag.bulk_import(context={'import': True, 'ignore_methods': True})


if __name__ == '__main__':
    go()
