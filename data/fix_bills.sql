begin;
update account_move_line
  from account_move m
  where m.id = account_move_line.move_id
  and m.bill_receivable
  and account_move_line.account_id in (select default_debit_account_id from account_journal j where j.bill_receivable);
commit;