from extractor import  extract_data

class extract_pricelist(extract_data):
    model = 'product.pricelist'
    query = """
        select thema.xid(TIPO_LISTINO +  rtrim(CAMPO_RICERCA)) id,
          DES_LISTINI name,
          'sale' type,
          case isnull(COD_VALUTA, '') when '' THEN 'EUR' else rtrim(COD_VALUTA) end [currency_id/id]
        from LISTINI
        """

class extract_pl_versions(extract_data):
    model = 'product.pricelist.version'
    query = """
        select thema.xid(TIPO_LISTINO +  rtrim(CAMPO_RICERCA)  + '2014') id,
        rtrim(CAMPO_RICERCA)  + ' 2014' name,
        thema.xid(TIPO_LISTINO +  rtrim(CAMPO_RICERCA)) [pricelist_id/id]
        from LISTINI
    """


class extract_price(extract_data):
    model = 'product.pricelist.item'
    query = """
        select *
        FROM (
          SELECT
            M.COD_PRODOTTO,
            thema.xid('P' + rtrim(M.COD_PRODOTTO) + TIPO_LISTINO +  rtrim(CAMPO_RICERCA)  + '2014') id,
            thema.xid(TIPO_LISTINO +  rtrim(CAMPO_RICERCA)  + '2014') [price_version_id/id],
            5 sequence,
            NULL [product_id/id],
            thema.xid('TPL' + rtrim(M.COD_PRODOTTO)) [product_tmpl_id/id],
            NULL [model_id/id],
            NULL [collection_id/id],
            --'Fixed price' base,
            -3 base,
            cast(L.PREZZO as float)  fixed_price
          FROM DETT_LISTINI L
          INNER JOIN MAGAZZINO_1 M on M.COD_PRODOTTO = L.COD_PRODOTTO AND L.F_TIPOLOGIA = 'P'
          
          UNION 
          SELECT
            M.COD_PRODOTTO,
            thema.xid('M' + rtrim(M.COD_MODELLO) + TIPO_LISTINO +  rtrim(CAMPO_RICERCA)  + '2014') id,
            thema.xid(TIPO_LISTINO +  rtrim(CAMPO_RICERCA)  + '2014') [price_version_id/id],
            10 sequence,
            NULL [product_id/id],
            NULL [product_tmpl_id/id],
            thema.xid('PMD' + rtrim(M.COD_MODELLO)) [model_id/id],
            NULL [collection_id/id],
            --'Fixed price' base,
            -3 base,
            cast(L.PREZZO as float) fixed_price
          FROM DETT_LISTINI L
          CROSS APPLY (
              select top 1 * from MAGAZZINO_1 where COD_MODELLO = L.COD_PRODOTTO
          ) M
          where L.F_TIPOLOGIA = 'M'


          UNION 
          SELECT
            M.COD_PRODOTTO,
            thema.xid('L' + rtrim(M.COD_LINEA) + TIPO_LISTINO +  rtrim(CAMPO_RICERCA)  + '2014') id,
            thema.xid(TIPO_LISTINO +  rtrim(CAMPO_RICERCA)  + '2014') [price_version_id/id],
            15 sequence,
            NULL [product_id/id],
            NULL [product_tmpl_id/id],
            NULL [model_id/id],
            thema.xid(rtrim(M.COD_LINEA)) [collection_id/id],
            --'Fixed price' base,
            -3 base,
            cast(L.PREZZO as float) fixed_price
          FROM DETT_LISTINI L
          CROSS APPLY (
              select top 1 * from MAGAZZINO_1 where COD_LINEA = L.COD_PRODOTTO
          ) M
          where L.F_TIPOLOGIA = 'L'

        ) an
        where COD_PRODOTTO in (
          select COD_PRODOTTO from oerp.prodotti_esportabili
        )

    """
    exclude =  ('COD_PRODOTTO', )

def go():
    pl = extract_pricelist()
    pl.bulk_import()

    v = extract_pl_versions()
    v.bulk_import()

    pr = extract_price()
    pr.bulk_import()

if __name__ == '__main__':
    go()
