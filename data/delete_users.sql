do language  plpgsql $t$
  declare
    cnn varchar;
    tbn varchar;
    cln varchar;
    cnt int;
    i int;
  begin
    delete from res_company_users_rel where user_id <> 1;
    delete from res_groups_users_rel where uid <> 1;
    create temp table cs as
    select
      c.constraint_name, c.table_name, ku.column_name
    from information_schema.table_constraints c
    inner join information_schema.key_column_usage ku on ku.constraint_name = c.constraint_name
    inner join information_schema.constraint_column_usage cu on cu.constraint_name = c.constraint_name
    where c.constraint_type = 'FOREIGN KEY'
    and cu.table_name = 'res_users';

    select count(*) from cs into cnt;

    for cnn, tbn, cln, i in select constraint_name, table_name, column_name, row_number() over (order by constraint_name) i
    from cs loop
      raise notice 'dropping constraint %s on table %s (%s/%s)', cnn, tbn, i, cnt;
      execute format($s$alter table %I drop constraint %I; update %I set %I=1 where %I <> 1;$s$, tbn, cnn, tbn, cln, cln);
    end loop;
    raise notice 'deleting from res_users...';
    delete from res_users where id <> 1;
    for cnn, tbn, cln, i in select constraint_name, table_name, column_name, row_number() over (order by constraint_name) i
      from cs loop
      raise notice 'creating constraint %s on table %s (%s/%s)', cnn, tbn, i, cnt;
      execute format($s$alter table %I add constraint %I FOREIGN KEY (%I) REFERENCES res_users(id);$s$, tbn, cnn, cln);
    end loop;
  end
$t$;



do language  plpgsql $t$
  declare
    cnn varchar;
    tbn varchar;
    cln varchar;
    cnt int;
    i int;
  begin
    --delete from res_company_users_rel where user_id <> 1;
    --delete from res_groups_users_rel where uid <> 1;
    create temp table cs as
    select
      c.constraint_name, c.table_name, ku.column_name
    from information_schema.table_constraints c
    inner join information_schema.key_column_usage ku on ku.constraint_name = c.constraint_name
    inner join information_schema.constraint_column_usage cu on cu.constraint_name = c.constraint_name
    where c.constraint_type = 'FOREIGN KEY'
    and cu.table_name = 'res_partner';

    select count(*) from cs into cnt;

    for cnn, tbn, cln, i in select constraint_name, table_name, column_name, row_number() over (order by constraint_name) i
    from cs loop
      raise notice 'dropping constraint %s on table %s (%s/%s)', cnn, tbn, i, cnt;
      execute format($s$alter table %I drop constraint %I;$s$, tbn, cnn, tbn, cln, cln);
    end loop;
    raise notice 'deleting from res_partner...';
    delete from res_partner where id in (select res_id from ir_model_data where name like '%_res_partner' and model = 'res.partner');

    for cnn, tbn, cln, i in select constraint_name, table_name, column_name, row_number() over (order by constraint_name) i
      from cs loop
      raise notice 'creating constraint %s on table %s (%s/%s)', cnn, tbn, i, cnt;
      execute format($s$alter table %I add constraint %I FOREIGN KEY (%I) REFERENCES res_partner(id);$s$, tbn, cnn, cln);
    end loop;
  end
$t$;



