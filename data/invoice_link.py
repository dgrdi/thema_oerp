from extractor import extract_data
from thwork import split_work
from db import session
from extractor import client


def build_sale_invoice_rel():
    with session as s:
        res = s.execute("""
            SELECT
              dbo.fn_RifDocumento(ot.TIPO_DOC, ot.ANNO_DOC, ot.NUMERO_DOC)                                       ordine,
              od.PROGRESSIVO_RIGA                                                                                riga_ordine,
              tf.TIPO_DOC + substring(cast(tf.ANNO_DOC AS VARCHAR), 3, 2) + '/' + cast(tf.NUMERO_DOC AS VARCHAR) fattura,
              df.PROGRESSIVO_RIGA                                                                                riga_fattura
            FROM CROSS_REF_DOCS x
              INNER JOIN D_ORDINI_CLI od ON od.ID_ORDINE = x.ID_DOC_ORIG AND od.PROGRESSIVO_RIGA = x.NR_RIGA_ORIG
              INNER JOIN T_ORDINI_CLI ot ON ot.ID_ORDINE = od.ID_ORDINE
              INNER JOIN D_FATT_VENDITA df ON df.ID_FATTURA = x.ID_DOC_DEST AND x.NR_RIGA_DEST = df.PROGRESSIVO_RIGA
              INNER JOIN T_FATT_VENDITA tf ON tf.ID_FATTURA = df.ID_FATTURA
            WHERE x.PROC_ORIG = 'OC' AND x.PROC_DEST = 'FC'
            UNION ALL
            SELECT
              dbo.fn_RifDocumento(ot.TIPO_DOC, ot.ANNO_DOC, ot.NUMERO_DOC)                                       ordine,
              od.PROGRESSIVO_RIGA                                                                                riga_ordine,
              tf.TIPO_DOC + substring(cast(tf.ANNO_DOC AS VARCHAR), 3, 2) + '/' + cast(tf.NUMERO_DOC AS VARCHAR) fattura,
              df.PROGRESSIVO_RIGA                                                                                riga_fattura
            FROM CROSS_REF_DOCS x
              INNER JOIN D_ORDINI_CLI od ON od.ID_ORDINE = x.ID_DOC_ORIG AND od.PROGRESSIVO_RIGA = x.NR_RIGA_ORIG
              INNER JOIN T_ORDINI_CLI ot ON ot.ID_ORDINE = od.ID_ORDINE
              INNER JOIN CROSS_REF_DOCS x2 ON x2.PROC_ORIG = x.PROC_DEST AND x2.PROC_DEST = 'FC'
                                              AND x2.ID_DOC_ORIG = x.ID_DOC_DEST AND x2.NR_RIGA_ORIG = x.NR_RIGA_DEST
              INNER JOIN D_FATT_VENDITA df ON df.ID_FATTURA = x2.ID_DOC_DEST AND x2.NR_RIGA_DEST = df.PROGRESSIVO_RIGA
              INNER JOIN T_FATT_VENDITA tf ON tf.ID_FATTURA = df.ID_FATTURA
            WHERE x.PROC_ORIG = 'OC' AND x.PROC_DEST = 'BC'
        """)
        ords = {}
        righe = {}
        for ordine, riga_ordine, fattura, riga_fattura in res.fetchall():
            riga_ordine = ordine + str(riga_ordine)
            riga_fattura = fattura + 'R' + str(riga_fattura)
            ords.setdefault(ordine, set()).add(fattura)
            righe.setdefault(riga_ordine, set()).add(riga_fattura)
    ords = [[id, ','.join(v)] for id, v in ords.iteritems()]

    def worker(data):
        client.get_model('sale.order').bulk_import(['id', 'invoice_ids'], data,
                                                   context={'ignore_methods': True, 'nocreate': True})

    split_work(ords, worker)
    righe = [[id, ','.join(v)] for id, v in righe.iteritems()]

    def worker(data):
        def chunkify():
            for i in range(0, len(data), 20000):
                yield data[i:i + 20000]

        for c in chunkify():
            client.get_model('sale.order.line').bulk_import(['id', 'invoice_lines'], c,
                                                            context={'ignore_methods': True, 'nocreate': True,
                                                                     'import_skip_store': True})

    split_work(righe, worker)


def build_picking_invoice_rel():
    with session as s:
        res = s.execute("""
            select
            'F', s.ID_FATTURA, s.ID_ORDINE, s.PROGRESSIVO_RIGA,
            thema.xid(TIPO_DOC+substring(cast(ANNO_DOC AS varchar),3,2)+'/'+cast(NUMERO_DOC AS varchar)),
            s.PROGRESSIVO_RIGA
            from oerp.spedizioni_fatture2 s
            inner join T_FATT_VENDITA f ON f.ID_FATTURA = s.ID_FATTURA
            UNION ALL
            SELECT
             'B', s.ID_BOLLA, s.ID_ORDINE, s.PROGRESSIVO_RIGA,
              thema.xid(TIPO_DOC+substring(cast(ANNO_DOC AS varchar),3,2)+'/'+cast(NUMERO_DOC AS varchar)),
              x.NR_RIGA_DEST
            from oerp.spedizioni_bolle2 s
            inner join CROSS_REF_DOCS x ON x.PROC_ORIG = 'BC' and x.PROC_DEST = 'FC' and x.NR_RIGA_ORIG = s.PROGRESSIVO_RIGA
            and x.ID_DOC_ORIG = s.ID_BOLLA
            INNER JOIN T_FATT_VENDITA f ON f.ID_FATTURA = x.ID_DOC_DEST
        """)
        testate = {}
        righe = {}
        for typ, idf, ido, prog, xid, nf in res.fetchall():
            ido = ido or ''
            riga_fattura = xid + 'R%s' % nf
            testata_sped = 'PK{typ}{idf}/{ido}'.format(**locals())
            riga_sped = 'PK{typ}D{idf}/{prog}'.format(**locals())
            testate[testata_sped] = xid
            righe[riga_sped] = riga_fattura

    def worker(data):
        client.get_model('stock.picking').bulk_import(['id', 'invoice_id/id'], data,
                                                      context={'ignore_methods': True, 'nocreate': True,
                                                               'import_skip_store': True})

    split_work(testate.items(), worker)

    def worker(data):
        def chunkify():
            for i in range(0, len(data), 20000):
                yield data[i:i + 20000]

        for c in chunkify():
            client.get_model('stock.move').bulk_import(['id', 'invoice_line_id/id'], c,
                                                       context={'ignore_methods': True, 'nocreate': True,
                                                                'import_skip_store': True})
    split_work(righe.items(), worker)


class invoice_ref_link(extract_data):
    model = 'account.invoice'
    query = """
    SELECT thema.xid(TIPO_DOC+substring(cast(ANNO_DOC AS varchar),3,2)+'/'+cast(NUMERO_DOC AS varchar)) id,
      thema.xid(TIPO_DOC_RIF+substring(cast(ANNO_DOC_RIF AS varchar),3,2)+'/'+cast(NUMERO_DOC_RIF AS varchar)) [ref_invoice_id/id]
    FROM T_FATT_VENDITA
    WHERE TIPO_DOC_RIF IS NOT NULL
    """

if __name__ == '__main__':
    build_picking_invoice_rel()
    build_sale_invoice_rel()
    invoice_ref_link().bulk_import(context={'ignore_methods': True, 'import_skip_store': True, 'nocreate': True})
