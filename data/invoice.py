from thwork import split_work
from extractor import extract_data
from extractor import client
from trigger_recompute import trigger_recompute


class extract_invoice_testata(extract_data):
    model = 'account.invoice'
    query = """
        SELECT  F.TIPO_DOC+substring(cast(F.ANNO_DOC AS varchar),3,2)+'/'+cast(F.NUMERO_DOC AS varchar) + '{azienda}' AS id,
                dbo.fn_RifDocumento(F.TIPO_DOC, F.ANNO_DOC, F.NUMERO_DOC) internal_number,
                dbo.fn_RifDocumento(F.TIPO_DOC, F.ANNO_DOC, F.NUMERO_DOC) number,
                --dbo.fn_RifDocumento(F.TIPO_DOC, F.ANNO_DOC, F.NUMERO_DOC) name,
                thema.xid('CL'+F.CAMPO_RICERCA) AS [partner_id/id],
                oerp.x_pagamento(F.COD_PAGAMENTO) [payment_term/id],
                ax.xid [move_id/id],
                oerp.x_valuta(F.COD_VALUTA) [currency_id/id],
                thema.xid('AG'+CA.COD_AGENTE)  AS [agent_id/id],
                --CASE WHEN CA.COD_CLIENTE IS NULL THEN thema.xid('FO'+CA.COD_FORNITORE)  ELSE thema.xid('CL'+CA.COD_CLIENTE) END AS [agent_id/id],
                oerp.fn_x_periodo(F.ANNO_DOC, F.DATA_DOC) [period_id/id],
                cast(F.SCONTO_PAGA as float) [payment_discount],

                thema.xid('AG'+CA.COD_CAPOAREA)  AS [area_manager_id/id],
                case when F_FATT_NC = 'N' then '{code}NC' else '{code}FV' end [journal_id/id],
                CONVERT(VARCHAR(24),F.DATA_DOC, 126)  AS date_invoice,
                0 AS check_total,
                CASE WHEN C.MASTRO IS NULL THEN thema.xid(rtrim(lower('{code}')) + '5052') ELSE thema.xid(rtrim(lower('{code}')) + C.MASTRO) END AS [account_id/id],
                '' AS comment,
                '{xid_azienda}' AS [company_id/id],
                '' origin,
                '' AS reference,
                1 AS sent,
                CASE WHEN F_TRATTAMENTO=0 THEN 'open' ELSE 'proforma2' END AS  state ,
                oerp.fn_user(isnull(op.xid, F.USER_OWNER)) [user_id/id]

        FROM T_FATT_VENDITA F
        LEFT JOIN TAB_DOC_FATVEN TF ON F.COD_DOC_FATVEN=TF.COD_DOC_FATVEN
        LEFT JOIN ANAG_CLI C ON F.CAMPO_RICERCA=C.CAMPO_RICERCA
        LEFT JOIN oerp.corr_age_cli CA ON C.COD_AGENTE=CA.COD_AGENTE
        LEFT JOIN ARTICOLI a ON a.TIPO_DOC = F.TIPO_DOC and a.ANNO_DOC = F.ANNO_DOC and a.NUMERO_DOC = F.NUMERO_DOC
        LEFT JOIN oerp.x_articoli ax ON ax.ID_ARTICOLO = a.ID_ARTICOLO
        left join AZIENDA_TOP.oerp.x_operatori op ON op.COD_OPERATORE = F.COD_OPERATORE
        where (F.F_TRATTAMENTO = 0 OR '{code}' = 'it')

        and F.DATA_DOC >= '{datai}'
    """


get_query_dettagli = """
         SELECT thema.xid(TIPO_DOC+substring(cast(ANNO_DOC AS varchar),3,2)+'/'+cast(NUMERO_DOC AS varchar) +'R'+ '{azienda}' + cast(PROGRESSIVO_RIGA AS varchar))AS id,
                thema.xid(TIPO_DOC+substring(cast(ANNO_DOC AS varchar),3,2)+'/'+cast(NUMERO_DOC AS varchar)) + '{azienda}' AS [invoice_id/id],
                D.PROGRESSIVO_RIGA sequence,
                CASE
                WHEN D.COD_TIPO_RIGA = 'SP' then 'service'
                 WHEN D.COD_TIPO_RIGA = 'ST' then 'shipping'
                 when D.COD_TIPO_RIGA = 'AR' then 'collect'
                 WHEN R.CAUS_RIGA IN ('01', 'SC') THEN 'sale'
                 ELSE
                     CASE WHEN R.CAUS_RIGA IN ('SM') THEN  'discount'
                          WHEN R.CAUS_RIGA = 'OI' THEN 'gift'
                          ELSE 'sale'
                     END
                END  AS [line_type_id/id],
                --CASE WHEN P.COD_PRODOTTO IS NULL THEN  ELSE thema.xid(rtrim(D.COD_PRODOTTO)) END [product_id/id],
                CASE WHEN rtrim(D.COD_PRODOTTO)='SPESE TRASPORTO' THEN 'product_shipping' ELSE thema.xid(rtrim(P.COD_PRODOTTO)) END AS [product_id/id],
                CASE WHEN P.COD_PRODOTTO IS NOT NULL THEN '[' + rtrim(P.COD_PRODOTTO) + '] ' else '' end +
                CASE WHEN substring(D.DESCR_PRODOTTO, 1, 1) = '*' THEN substring(D.DESCR_PRODOTTO, 2, 100) ELSE
                  CASE WHEN   D.DESCR_PRODOTTO IS NULL THEN
                    CASE WHEN rtrim(D.COD_PRODOTTO)='.'  THEN 'PRODOTTO GENERICO NON MOVIMENTABILE' ELSE rtrim(D.COD_PRODOTTO) END
                  ELSE D.DESCR_PRODOTTO END
                END AS name,
                CASE WHEN R.CAUS_RIGA IN ('SC') THEN  cast((D.PREZZO_VENDITA*(-1)) as float) ELSE cast(D.PREZZO_VENDITA as float) END AS price_unit,
                cast(D.QTA_FATTURATA as float)  as quantity,
                --CASE WHEN R.CAUS_RIGA IN ('SC') THEN  cast((D.PREZZO_VENDITA*(-1)) as float) ELSE cast(D.PREZZO_VENDITA as float) * cast(D.QTA_FATTURATA as float) END AS price_subtotal,
                cast(D.PROVVIGIONE as float) AS  agent_commission,
                cast(D.PROVVIGIONE_2 as float) AS  manager_commission,
                cast (
                  D.SCONTO + T.SCONTO_CLIENTE - (D.SCONTO * T.SCONTO_CLIENTE / 100)
                as float) as discount_flat,
                case D.UNITA_DI_MISURA

                  WHEN 'PZ' THEN 'product.product_uom_unit'
                  WHEN 'PA' THEN 'thema_product.product_uom_pair'
                  END                                                                                          [uos_id/id],

                --'intraproduct' + mg.COD_NOMENCLATURA [intra_code_id/id],
                --cast(SCONTO as float) AS discount_flat,
                thema.xid(rtrim(lower('{code}')) + D.KEY_CONTO) AS [account_id/id],
                '{code}'+I.codice AS [invoice_line_tax_id/id]
        FROM D_FATT_VENDITA D
        LEFT JOIN T_FATT_VENDITA T ON D.ID_FATTURA=T.ID_FATTURA
        LEFT JOIN TAB_TIPO_RIGA R ON D.COD_TIPO_RIGA=R.COD_TIPO_RIGA
        LEFT JOIN oerp.prodotti_esportabili P ON  D.COD_PRODOTTO=P.COD_PRODOTTO
        LEFT JOIN oerp.iva I ON D.COD_IVA=I.COD_IVA AND debcred='D'
        INNER JOIN MAGAZZINO_1 mg ON mg.COD_PRODOTTO = D.COD_PRODOTTO
        where (T.F_TRATTAMENTO = 0 OR '{code}' = 'it')
        and T.DATA_DOC >= '{datai}'
    """


class extract_invoice_dettagli1(extract_data):
    model = 'account.invoice.line'
    query = get_query_dettagli


class extract_invoice_tax(extract_data):
    model = 'account.invoice.line'
    query = """
         SELECT thema.xid(TIPO_DOC+substring(cast(ANNO_DOC AS varchar),3,2)+'/'+cast(NUMERO_DOC AS varchar) +'R'+ cast(PROGRESSIVO_RIGA AS varchar))AS id,
                'it'+I.codice AS [invoice_line_tax_id/id]
         FROM D_FATT_VENDITA D
         LEFT JOIN T_FATT_VENDITA T ON D.ID_FATTURA=T.ID_FATTURA
         LEFT JOIN oerp.iva I ON D.COD_IVA=I.COD_IVA AND debcred='D'
         WHERE T.DATA_DOC >= '2014-05-01'
    """


def get_context(update_all):
    if (update_all):
        c = {'import': True, 'ignore_methods': True, 'recompute': True, 'import_skip_store': True}
    else:
        c = {'import': True, 'ignore_methods': True, 'novalidate': True, 'import_skip_store': True}
    return c


def go_invoice_dettagli(update_all, azienda, datai):
    c = get_context(update_all)

    t_inv_dett = extract_invoice_dettagli1(azienda=azienda, datai=datai)
    t_inv_dett.bulk_import(context=c)


def go_invoice_tax():
    t_inv_tax = extract_invoice_tax()
    t_inv_tax.bulk_import(context={'import': True, 'ignore_methods': True, 'import_skip_store': True})
    del t_inv_tax


def go_update_tax():
    '''
    config.parse_config(['--db_host=localhost', '-d', 'thema_dati', '-r', 'postgres', '-w', 'postgres'])
    connection, pool = get_db_and_pool('thema_dati')
    cr = connection.cursor()
    '''
    # invoice = pool.get('account.invoice')
    invoice = client.get_model('account.invoice')

    ids = invoice.read(invoice.search([]), )
    #print ids
    invoice.button_reset_taxes(uid=1, ids=ids, context=None)


def do_azienda(azienda, datai):
    # Importazione delle testate di fatture
    t_inv = extract_invoice_testata(azienda=azienda, datai=datai)
    t_inv.bulk_import(context={'import': True, 'ignore_methods': True, 'import_skip_store': True})
    del t_inv

    #   Importazione dei dettagli delle fatture
    #go_invoice_dettagli(True)
    go_invoice_dettagli(False, azienda=azienda, datai=datai)

    #   Importazione dei dettagli fiscali delle fatture
    #go_invoice_tax() # compreso nell'importazione delle fatture
    #md = client.get_model('account.invoice')
    #button_reset_taxes(self, cr, uid, ids, context=None):


def compute_taxes(datai=None):
    print 'recomputing taxes & totals...'
    if datai is not None:
        domain = [('date_invoice', '>=', datai)]
    else:
        domain = []
    inv = client.get_model('account.invoice')
    ids = inv.search(domain)

    def chunkify(data):
        for i in range(0, len(data), 2000):
            yield data[i:i+2000]

    def worker(data):
        print 'recomputing %s records' % len(data)
        for c in chunkify(data):
            inv.button_compute(c, set_total=False, context={'no_store_compute': True})

    split_work(ids, worker)


if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        datai = sys.argv[1]
    else:
        datai = '2014-07-01'
    for az in [
        'AZIENDA_TOP',
        'AZIENDA_TES',
        'AZIENDA_TEC'
    ]:
        do_azienda(azienda=az, datai=datai)
    trigger_recompute('account.invoice.line', [
        'price_subtotal_flat',
        'value_amount',
    ], repr([('invoice_id.date_invoice', '>=', datai)]))
    trigger_recompute('account.invoice', None, repr([('date_invoice', '>=', datai)]))
    compute_taxes(datai)
    inv = client.get_model('account.invoice')
    inv.recompute_state(context={'mail_notrack': True})
    # go_update_tax()
    #go_invoice_dettagli(False)
    #go_invoice_tax()
