from extractor import client
from thwork import split_work, cth

def fix():
    ml = client.get_model('account.move.line')
    dat = client.dataset_search_read('account.move.line', domain=[('partner_id', '!=', False)], fields=['move_id', 'partner_id'])
    pmov = {}
    ban = set()
    for d in dat['records']:
        part = d['partner_id'][0]
        mid = d['move_id'][0]
        if mid in ban:
            continue
        if pmov.get(mid, part) != part:
            ban.add(mid)
            pmov.pop(mid, None)
        else:
            pmov[mid] = part

    dat  = client.dataset_search_read('account.move.line', domain=[('move_id', 'in', pmov.keys()), ('partner_id', '=', False)],
                                      fields=['move_id'])
    ppar = {}
    for d in dat['records']:
        part = pmov[d['move_id'][0]]
        ppar.setdefault(part, []).append(d['id'])

    vals = []
    for part, ids in ppar.iteritems():
        vals.append((ids, {'partner_id': part}))
        #ml.write(ids, {'partner_id': part})
    if vals:
        def worker(data):
            print cth().getName(), len(data)
            ml.bulk_write(data, context={'ignore_methods': True, 'import': True, 'novalidate': True})
        split_work(vals, worker)

def fix_recon():
    ml = client.get_model('account.move.line')
    res = client.dataset_search_read('account.move.line', domain=[
        '|',
        ('reconcile_id', '!=', False),
        ('reconcile_partial_id', '!=', False),
        ('partner_id', '!=', False),
    ], fields = ['reconcile_id', 'reconcile_partial_id', 'partner_id'])
    recs = {}
    for d in res['records']:
        rid = d['reconcile_id'] and d['reconcile_id'][0]
        if not rid:
            rid = d['reconcile_partial_id'] and d['reconcile_partial_id'][0]
        recs[rid] = d['partner_id'][0]
    res = client.dataset_search_read('account.move.line', domain=[
        '|',
        ('reconcile_id', 'in', recs.keys()),
        ('reconcile_partial_id', 'in', recs.keys()),
        ('partner_id', '=', False)
    ], fields = ['reconcile_id', 'reconcile_partial_id'])
    parts = {}
    for d in res['records']:
        rid = d['reconcile_id'] and d['reconcile_id'][0]
        if not rid:
            rid = d['reconcile_partial_id'] and d['reconcile_partial_id'][0]
        parts.setdefault(recs[rid], []).append(d['id'])

    parts = [(v, {'partner_id': k}) for k, v in parts.iteritems()]
    if parts:
        def worker(data):
            print cth().getName(), len(data)
            ml.bulk_write(data, context={'ignore_methods': True, 'import': True, 'novalidate': True})
        split_work(parts, worker)


if __name__ == '__main__':
    fix_recon()
    fix()
