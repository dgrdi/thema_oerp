from extractor import client, extract_data

def fix_tax():
    tx = client.get_model('account.tax')
    ids = tx.search(['|', ('description', 'ilike', '%cee%'), ('description', 'ilike', '%art41%')])
    tx.write(ids, {'intra': True})

class codes(extract_data):
    model = 'product.template'
    query = """
        select thema.xid('TPL' + rtrim(COD_PRODOTTO)) id,
        COD_NOMENCLATURA [intra_goods_code_id/.id]
        FROM MAGAZZINO_1
        WHERE COD_NOMENCLATURA IS NOT NULL
    """

    def process_set(self, rows):
        p = client.get_model('product.template')
        ex = p.read(p.search([]), fields=['res_intra_code_id'])
        ex = [el['id'] for el in ex if not el['res_intra_code_id']]
        ex = p.get_external_id(ex)
        ex = {el.split('.')[-1] for el in ex.values()}
        res = []
        codes = set()
        for row in rows:
            if row['id'] not in ex:
                res.append(row)
                codes.add(row['intra_goods_code_id/.id'])
        ic = client.get_model('intra.code')
        dom = [('code', '=', c) for c in codes]
        dom = ['|'] * ( len(dom) - 1) + dom
        codes = ic.read(ic.search(dom), fields=['code'])
        codes = {el['code']: el['id'] for el in codes}
        for row in res:
            row['intra_goods_code_id/.id'] = codes[row['intra_goods_code_id/.id']]
        return res

if __name__ == '__main__':
    fix_tax()
    codes().bulk_import(context={'ignore_methods': True, 'import_skip_store': True, 'nocreate': True})