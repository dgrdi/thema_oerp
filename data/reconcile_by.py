from db import session
from extractor import extract_data, client
import time
import hashlib
from collections import OrderedDict
from recon_scad import scadenze_v2


def rec_scadenze(azienda='AZIENDA_TOP'):
    with session as s:
        s.execute('USE %s;' % azienda)
        res = s.execute("""
            SELECT
                ID_SCADENZA,
                ID_PRIMA_NOTA,
                ID_SCADENZA_C,
                ID_PRIMA_NOTA_C
            FROM oerp.chiusura_scadenze_ancora
            ORDER BY ID_SCADENZA, ID_PRIMA_NOTA
        """.format(az=azienda)).fetchall()
    sc = OrderedDict()
    link = {}
    for scad, pn, scad_c, pn_c in res:
        sc.setdefault((scad, pn), set()).update([(scad, pn), (scad_c, pn_c)])
    return sc.values()


def save_scadenze(data, azienda='AZIENDA_TOP'):
    with session as s:
        s.execute('USE %s;' % azienda)
        s.execute("""
            DELETE from oerp.rec_d where rec_typ = 'S';
            DELETE FROM oerp.rec_h WHERE typ = 'S';
        """)
        s.execute("""
            INSERT INTO oerp.rec_h(typ, id)
            VALUES ('S', :id);
        """, [{'id': i + 1} for i in range(len(data))])
        s.execute("""
            INSERT INTO oerp.rec_d(rec_typ, rec_id, ID_PRIMA_NOTA, ID_SCADENZA)
            VALUES ('S', :id, :pn, :scad);
        """, [{'id': i + 1, 'pn': pn, 'scad': scad} for i, v in enumerate(data) for scad, pn in v])
        s.commit()


def update_balance(azienda='AZIENDA_TOP'):
    with session as s:
        s.execute('USE %s;' % azienda)
        s.execute("""
            UPDATE rh
                SET saldo = d.delta
            FROM oerp.rec_h rh
              INNER JOIN (
              SELECT
                r.rec_typ,
                r.rec_id,
                sum(case when pn.F_DARE_AVERE = 'D'
                      then  isnull(s.IMPORTO_LIRE, pn.IMPORTO_LIRE)
                      else -isnull(s.IMPORTO_LIRE, pn.IMPORTO_LIRE)
                    end
                 ) delta
              FROM oerp.rec_d r
              LEFT JOIN MOV_SCADENZE s ON s.ID_SCADENZA = r.ID_SCADENZA
              INNER JOIN PRIMA_NOTA pn ON pn.ID_PRIMA_NOTA = r.ID_PRIMA_NOTA
              GROUP BY r.rec_typ, r.rec_id
            ) d ON d.rec_typ = rh.typ AND d.rec_id = rh.id
        """.format(az=azienda))
        s.commit()


def calculate_reconcile(azienda='AZIENDA_TOP'):
    scadenze_v2(azienda=azienda)


class extract_rec(extract_data):
    model = 'account.move.reconcile'
    chunk_size = 20000
    query = """
    SELECT
        thema.xid('I' + h.typ + cast(h.id as varchar) ) + '{azienda}' id,
        thema.xid('I' + h.typ + cast(h.id as varchar) ) name,
        'auto' type
    FROM oerp.rec_h h
    """


class set_rec(extract_data):
    model = 'account.move.line'
    chunk_size = 20000
    query = """
        SELECT
          pn.xid id,
          case
            when h.saldo <> 0 then thema.xid('I' + h.typ + cast(h.id as varchar) ) + '{azienda}'
            else NULL
          end [reconcile_partial_id/id],
          case
            when h.saldo  = 0 then thema.xid('I' + h.typ + cast(h.id as varchar) ) + '{azienda}'
            else NULL
          end [reconcile_id/id]
         FROM oerp.rec_d d
        INNER JOIN oerp.rec_h h ON h.typ = d.rec_typ AND d.rec_id = h.id
        INNER JOIN oerp.x_prima_nota pn ON pn.ID_PRIMA_NOTA = d.ID_PRIMA_NOTA
         AND (pn.ID_SCADENZA = d.ID_SCADENZA OR d.ID_SCADENZA IS NULL or pn.ID_SCADENZA IS NULL)
        INNER JOIN PRIMA_NOTA ppn ON d.ID_PRIMA_NOTA = ppn.ID_PRIMA_NOTA
        INNER JOIN ARTICOLI a ON a.NUM_ARTICOLO = ppn.NUM_ARTICOLO AND a.ESERCIZIO = ppn.ESERCIZIO
        INNER JOIN oerp.art_journal aj ON aj.ID_ARTICOLO = a.ID_ARTICOLO

    """

    def bulk_import(self, *args, **kwargs):
        mv = client.get_model('account.move.line')
        move_ids = mv.search([
            '|',
            ('reconcile_id', '!=', False),
            ('reconcile_partial_id', '!=', False),
            ('company_id', '=', self.company_id)
        ])
        xids = mv.get_external_id(move_ids).values()
        xids = {x.split('.')[1] for x in xids if x}
        ex = {el['id'] for el in self.data}
        self.data.extend({'id': id, 'reconcile_id/id': None, 'reconcile_partial_id/id': None} for id in xids - ex)
        return super(set_rec, self).bulk_import(*args, **kwargs)


def rec_effetti(azienda='AZIENDA_TOP'):
    with session as s:
        s.execute('USE %s;' % azienda)
        eff = s.execute("""
            SELECT ID_DISTINTA, case F_DARE_AVERE when 'D' then IMPORTO_LIRE else -IMPORTO_LIRE end IMP, ID_PRIMA_NOTA
             FROM oerp.movimenti_effetti e
             ORDER BY ID_DISTINTA, IMPORTO_LIRE
        """).fetchall()
        dis = s.execute("""
            SELECT ID_DISTINTA, case F_DARE_AVERE when 'D' then IMPORTO_LIRE else -IMPORTO_LIRE end IMP, ID_PRIMA_NOTA
            FROM oerp.movimenti_distinte d
            ORDER BY ID_DISTINTA, IMPORTO_LIRE
        """).fetchall()
    effd = {}
    for idd, imp, xid in eff:
        effd.setdefault(idd, []).append((imp, xid))
    disd = {}
    for idd, imp, xid in dis:
        disd.setdefault(idd, []).append((imp, xid))
    rec = []
    cnt = [0]
    moves = []
    recdone = {}

    def do_rec(xids, partial):
        cnt[0] += 1
        rcid = cnt[0]
        vals = {
            'id': rcid,
            'xids': xids
        }
        rec.append(vals)
        for x in xids:
            recdone[x] = rcid

    norec = set()
    unbal = {}
    lastt = time.time()
    ln = len(effd)
    for i, idd in enumerate(effd.iterkeys()):
        effs, diss = effd[idd], disd.get(idd)
        if not diss:
            continue
        for imp, xid in effs:
            if xid in recdone:
                continue
            for impd, xidd in diss:
                if abs(impd) > abs(imp):
                    norec.add((imp, xid))
                    break
                if impd == -imp:
                    do_rec([xid, xidd], partial=False)
                    diss.remove((impd, xidd))
                    break
            else:
                norec.add((imp, xid))
        if norec:
            elems = set.union(norec, diss)
            totd = sum(imp for imp, xid in elems)
            xids = [xid for imp, xid in elems]
            if totd != 0:
                print 'Distinta %s non e bilanciata, differenza: %s' % (idd, totd)
                if abs(totd) <= 10 and len(xids) % 2 == 0:
                    print 'Riconciliando comunque.'
                    do_rec(xids, partial=True)
                else:
                    print 'Da chiarire.'
                    for x in norec:
                        unbal[x[1]] = idd

            else:
                do_rec(xids, partial=False)
            norec.clear()
        curt = time.time()
        if curt > lastt + 2:
            lastt = curt
            print '%f%%' % (float(i) / ln)

    if unbal:
        unbxid = ', '.join("'%s'" % e for e in unbal.iterkeys())
        with session as s:
            s.execute('USE %s;' % azienda)
            res = s.execute("""
                SELECT DISTINCT
                  e.pn_xid,
                  xpn.xid
                FROM oerp.movimenti_effetti e
                INNER JOIN PRIMA_NOTA pn ON pn.ID_PRIMA_NOTA = e.ID_PRIMA_NOTA
                INNER JOIN PRIMA_NOTA pn2 ON pn.F_DARE_AVERE <> pn2.F_DARE_AVERE and pn2.IMPORTO_LIRE = pn.IMPORTO_LIRE
                and pn.KEY_CONTO = pn2.KEY_CONTO and datediff(day, pn.DATA_REGISTRAZ, pn2.DATA_REGISTRAZ) <= 10
                  and datediff(day, pn.DATA_REGISTRAZ, pn2.DATA_REGISTRAZ) >= 0
                INNER JOIN oerp.x_prima_nota xpn ON pn2.ID_PRIMA_NOTA = xpn.ID_PRIMA_NOTA
                where e.pn_xid IN ({0})

            """.format(unbxid))
            for x1, x2 in res.fetchall():
                if x2 not in recdone:
                    unbal.pop(x1)
                    do_rec([x1, x2], partial=False)
                else:
                    print '%s -> %s: seconda gia riconciliata in %s' % (x1, x2, recdone[x2])
                    for m in moves:
                        if m['reconcile_id/id'] == recdone[x2] or m['reconcile_partial_id/id'] == recdone[x2]:
                            print m['id'],
                    print ''

    if unbal:
        print 'Non riconciliate:'
        print unbal

    return rec


def save_effetti(rec, azienda='AZIENDA_TOP'):
    with session as s:
        s.execute('USE %s;' % azienda)
        s.execute("""
          DELETE FROM oerp.rec_d WHERE rec_typ = 'E';
          DELETE FROM oerp.rec_h WHERE typ = 'E';
        """)
        s.execute("""
            INSERT INTO oerp.rec_h(typ, id)
            SELECT 'E', :id
        """, [{'id': v['id']} for v in rec])

        s.execute("""
            INSERT INTO oerp.rec_d (rec_typ, rec_id, ID_PRIMA_NOTA, ID_SCADENZA)
            SELECT 'E', :id, :pn, NULL
        """, [{'id': vs['id'], 'pn': v} for vs in rec for v in vs['xids']])

        s.commit()


def rec_apchiu(azienda='AZIENDA_TOP'):
    with session as s:
        s.execute('''
            USE {az};
            DELETE FROM oerp.rec_h WHERE typ = 'A';
            DELETE  FROM oerp.rec_d WHERE rec_typ = 'A';
            SELECT
             ROW_NUMBER() OVER (ORDER BY ID_PRIMA_NOTA) id,
            ac.ID_PRIMA_NOTA,
            ac.ID_PRIMA_NOTA_C
            INTO #ra
            FROM oerp.apertura_chiusura ac


            INSERT INTO oerp.rec_h(typ, id) SELECT 'A', id FROM #ra;
            INSERT INTO oerp.rec_d(rec_typ, rec_id, ID_PRIMA_NOTA)
              SELECT 'A', id, ID_PRIMA_NOTA FROM #ra
              UNION ALL
              SELECT 'A', id, ID_PRIMA_NOTA_C FROM #ra;
            DROP TABLE #ra;
        '''.format(az=azienda))
        s.commit()


def annulla_rec():
    from thwork import split_work, cth

    ml = client.get_model('account.move.line')
    rc = client.get_model('account.move.reconcile')
    lines = ml.search([
        '|',
        ('reconcile_id', '!=', False),
        ('reconcile_partial_id', '!=', False)
    ])

    def worker(data):
        print cth().getName(), 'Eliminando riferimenti ric. (%s movimenti)' % len(data)
        try:
            ml.write(data, {'reconcile_id/id': False, 'reconcile_partial_id/id': False}, context={
                'no_store_compute': True,
                'novalidate': True
            })
        except BaseException as e:
            print e.data['debug']

    for i in range(0, len(lines), 60000):
        ln2 = lines[i * 60000:(i + 1) * 60000]
        split_work(ln2, worker)
    recs = rc.search([])

    def worker(data):
        print cth().getName(), 'Eliminando ric. (%s record)' % len(data)
        rc.unlink(data)

    split_work(recs, worker)


def do_azienda(azienda, calc=True):

    if calc:
        calculate_reconcile(azienda=azienda)
        save_effetti(rec_effetti(azienda=azienda), azienda=azienda)
        rec_apchiu(azienda=azienda)
        update_balance(azienda=azienda)
    rec = extract_rec(azienda=azienda)
    rec.bulk_import(context={'novalidate': True, 'import': True, 'import_skip_store': True})
    srec = set_rec(azienda=azienda)
    srec.bulk_import(context={'ignore_methods': True, 'novalidate': True, 'nocreate': True, 'import_skip_store': True})


if __name__ == '__main__':
    import sys
    for az in [
        'AZIENDA_TOP',
        'AZIENDA_TES',
        'AZIENDA_TEC'
    ]:
        do_azienda(az, calc=(len(sys.argv) == 1))