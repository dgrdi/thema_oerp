from extractor import client

def set_accounts():
    res = client.dataset_search_read('account.journal', domain=[
        ('bill_receivable', '=', True),
    ], fields=['default_credit_account_id'])
    cred = client.get_model('account.account.type').search([('name', '=', 'Receivable')])[0]
    accts = [el['default_credit_account_id'][0] for el in res['records']]
    client.get_model('account.account').write(accts, {'type': 'receivable', 'user_type': cred})

if __name__ == '__main__':
    set_accounts()