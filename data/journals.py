from extractor import extract_data, client
from thwork import split_work

class journals(extract_data):
    model = 'account.journal'
    threads = 1
    query = """
        SELECT id,
              code,
              [name],
              [type],
              --centralization,
              [company_id/id],
              [default_credit_account_id/id],
              [default_debit_account_id/id],
              cast(provisional as varchar(10)) provisional,
              [provisional_post_journal_id/id]
              --[bills_receivable],
              --[bill_deposit]
        FROM oerp.journals j
        WHERE isnull(j.type, '') <> '';
    """
    def process_set(self, rows):
        ex = client.dataset_search_read('ir.model.data', domain=[('model', '=', 'account.journal')],
                                   fields=['name'])
        ex = {el['name'] for el in ex['records']}
        res = []
        for r in rows:
            if r['id'] not in ex:
                res.append(r)
        return res

class fix_journals(journals):
    threads = 1
    query = """
        SELECT id,
              provisional,
              [type],
              [provisional_post_journal_id/id],
              [bills_receivable] bill_receivable,
              [bill_deposit] bill_deposit,
              clearance_delay
        FROM oerp.journals j
        WHERE isnull(j.type, '') <> '';
    """
    def process_set(self, rows):
        ex = client.dataset_search_read('ir.model.data', domain=[('model', '=', 'account.journal')],
                                   fields=['name'])
        ex = {el['name'] for el in ex['records']}
        res = []
        for r in rows:
            if r['id'] in ex:
                res.append(r)
        return res

def do_azienda(azienda):
    j = journals(azienda=azienda)
    if j.data:
        j.load()
        j.print_messages()
    j = fix_journals(azienda=azienda)
    j.bulk_import(context={'ignore_methods': True})

def go():
    for az in ['AZIENDA_TOP',
               'AZIENDA_TES',
               'AZIENDA_TEC']:
        do_azienda(az)

if __name__ == '__main__':
    go()
