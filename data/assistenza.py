from extractor import client
from db import pg_session
from thwork import split_work, cth
from functools import partial

def create_pieces():
    with pg_session as s:
        res = s.execute("""
            select  distinct 'PMD' || rtrim(p.modello) id
            from sostituzioni_sostituzione s
              inner join prodotti_prodotto p on rtrim(p.codice) = rtrim(s.codice_prodotto)
              where s.tipo = 'P'
        """).fetchall()
    glasant = [el.id for el in res]

    imd = client.get_model('ir.model.data')
    glasant = imd.search([('model', '=', 'product.model'), ('name', 'in', glasant)])
    glasant = imd.read(glasant, fields=['res_id'])
    glasant = [el['res_id'] for el in glasant]

    asx = imd.get_object_reference('thema_post_sale', 'part_temple_left')[-1]
    adx = imd.get_object_reference('thema_post_sale', 'part_temple_right')[-1]
    p = imd.get_object_reference('thema_post_sale', 'part_nose')[-1]

    mod = client.get_model('product.model')
    noglasant = mod.search([('id', 'not in', glasant)])

    def worker(parts, ids):
        mod.generate_parts(ids, parts)

    split_work(glasant, partial(worker, [asx, adx, p]))
    split_work(noglasant, partial(worker, [asx, adx]))


def requests():
    with pg_session as s:
        res = s.execute("""
            WITH ev AS (
                SELECT
                  *,
                  row_number()
                  OVER (PARTITION BY sostituzione_id
                    ORDER BY dataora) n
                FROM sostituzioni_evasione
            )
            SELECT
              'OASS' || s.id :: VARCHAR               id,
              rtrim(s.codice_cliente) || CASE WHEN coalesce(s.destinazione, '') <> '' THEN '/' || rtrim(s.destinazione)
                                         ELSE '' END  partner_code,
              rtrim(s.codice_prodotto)                product_code,
              s.commento                              note,
              s.data :: VARCHAR                       date,
              CASE s.tipo
              WHEN 'M' THEN 'frame'
              WHEN 'A' THEN 'temple'
              WHEN 'ASX' THEN 'temple_left'
              WHEN 'ADX' THEN 'temple_right'
              WHEN 'P' THEN 'nose'
              ELSE 'frame'
              END                                     part_type,
              CASE
              WHEN e.id IS NOT NULL THEN 'process'
              WHEN s.stato = -1 THEN 'cancel'
              ELSE 'none' END                         operation,
              CASE WHEN codice_prodotto = 'ALTROPRODOTTO' THEN commento
              ELSE codice_prodotto || ' ' || tipo END "name",
              e.montatura                             frame,
              CASE rtrim(s.operatore)
              WHEN 'cliente' THEN NULL
              WHEN 'themaoptical' THEN 'admin'
              ELSE s.operatore END                    "user",
              CASE rtrim(s.operatore)
              WHEN 'cliente' THEN 'online'
              ELSE 'phone' END                        origin
            FROM sostituzioni_sostituzione s
              LEFT JOIN ev e ON e.sostituzione_id = s.id AND e.n = 1
            WHERE s.operatore != 'magazzino'
        """).fetchall()
    data = []
    for d in res:
        d = dict(zip(d.keys(), d))
        data.append(d)
    print '%s records total' % len(data)
    def chunkify():
        for i in range(0, len(data), 1000):
            yield data[i:i+1000]

    rq = client.get_model('postsale.request')
    def worker(data):
        print cth().getName(), len(data)
        rq.import_(data)
    for c in chunkify():
        split_work(c, worker)

def cleanup():
    client.get_model('postsale.request').import_cleanup()

def go():
    cleanup()
    create_pieces()
    requests()