from extractor import  extract_data, client
import re


class extract_colors(extract_data):
    model = 'product.color'
    query = """
        select distinct
            thema.xid(rtrim(COD_COLORE) + '/' + isnull(C.COLORE, 'SC')) id,
            rtrim(COD_COLORE) code,
            isnull(C.COLORE, 'SC') description,
            rtrim(COD_COLORE) + ' ' + isnull(C.COLORE, '') name
        from MAGAZZINO_1 M
        left join thema.colori C on thema.fn_colore_da_descr(M.DESCR_PRODOTTO) = C.COLORE_DA_DESCR
        where COD_COLORE is not null;
    """

class extract_tpl(extract_data):
    model = 'product.template'
    query = """
        select thema.xid('TPL' + rtrim(COD_PRODOTTO)) id,
          case
          when substring(DESCR_PRODOTTO, 1, 1) = '*' then substring(DESCR_PRODOTTO, 2, 100)
          else DESCR_PRODOTTO
          end name,
          thema.xid(rtrim(m.COD_LINEA)) [collection_id/id],
        case COD_CAT_MERC
          when 'OVM' then 'thema_product.product_category_eye_metal'
          when 'OVP' then 'thema_product.product_category_eye_plastic'
          else
            case substring(COD_CAT_MERC, 2, 1)
              when 'V' then 'thema_product.product_category_eye'
              when 'S' then 'thema_product.product_category_sun'
              when 'P' then 'thema_product.product_category_reading'
            end
        end [categ_id/id],
        thema.xid('PMD' + rtrim(COD_MODELLO)) [model_id/id],
        case substring(COD_CAT_MERC, 3, 1)
          when 'P' then 'thema_product.material_plastic'
          when 'M' then 'thema_product.material_metal'
        end [material_id/id],
        thema.xid(rtrim(COD_COLORE) + '/' + isnull(C.COLORE, 'SC')) [color_id/id],
        cast(NASO as float) bridge_length,
        cast(LUNGHEZZA_ASTA as int) temple_length,
        'product' type,
        COD_CALIBRO eye_size,
        case when substring(DESCR_PRODOTTO, 1, 1) = '*' then 'obsolete' else 'sellable' end state,
        COD_CAT_MERC
        from MAGAZZINO_1 m

        left join thema.colori C on C.COLORE_DA_DESCR = thema.fn_colore_da_descr(m.DESCR_PRODOTTO)
        where substring(COD_CAT_MERC, 1, 1) = 'O'
        and COD_CAT_MERC IS NOT NULL and m.COD_PRODOTTO in (select COD_PRODOTTO from oerp.prodotti_esportabili)
    """

    extra_fields = {
        'eye_size': 'eye_size',
        'power': 'power',
        'ext_shape_code': 'lens_shape',
    }
    exclude = ('COD_CAT_MERC', )

    def is_3piece(self, row):
        return str(bool(self.lens_shape(row)))

    lshrx = re.compile(r'.+(AA|BB|CC|DD)')
    def lens_shape(self, row):
        m = self.lshrx.match(row['id'])
        if m:
            return m.group(1)[0]


    prx = re.compile(r'[^\+]*\+(\d\d\d)')
    def power(self, row):
        m = self.prx.match(row['id'])
        if m:
            return float(m.group(1)) / 100

    def eye_size(self, row):
        try:
            sz = int(row['eye_size'])
        except:
            return None
        else:
            return sz if 1 < sz < 80 else None

    def categ_id(self, row):
        ccm = row['COD_CAT_MERC']
        cid = None
        if ccm:
            ccm = ccm[:2]
            if ccm == 'OS':
                cid = 'product_category_sun'
            if ccm == 'OV':
                cid = 'product_category_eye'
            if ccm == 'OP':
                cid = 'product_category_reading'
            if cid:
                cid = 'thema_product.' + cid
                return cid

    def is_frame(self, row):
        return str(bool(row['COD_CAT_MERC'] and row['COD_CAT_MERC'][0] == 'O'))



class extract_prod(extract_data):
    model = 'product.product'
    query = """
    select thema.xid(rtrim(M.COD_PRODOTTO)) id,
    rtrim(M.COD_PRODOTTO) default_code,
    thema.xid(rtrim('TPL' + M.COD_PRODOTTO)) [product_tmpl_id/id],
    COD_BARRE ean13,
    RF.EPC rfid_epc
    from MAGAZZINO_1 M
    left join rfid_epc RF on RF.COD_PRODOTTO = M.COD_PRODOTTO and RF.COD_DEPOSITO = '001'
    where isnull(COD_CAT_MERC, '') like 'O%'
    and COD_CAT_MERC IS NOT NULL
    and M.COD_PRODOTTO in (select COD_PRODOTTO from oerp.prodotti_esportabili)
    """


def go():
    c = extract_colors()
    c.bulk_import()
    m = extract_tpl()
    m.bulk_import(context={'import': True, 'ignore_methods': True})
    o = extract_prod()
    o.bulk_import(context={'import': True, 'ignore_methods': True})

if __name__ == '__main__':
    go()