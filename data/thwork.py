import threading
cth = lambda: threading.currentThread()

def split_work(data, worker, nthreads=20):
    chunk_size = len(data) / nthreads
    threads = []
    for i in range(nthreads):
        dt = data[i*chunk_size:(i+1)*chunk_size] if i < nthreads - 1 else data[i*chunk_size:]
        tr = threading.Thread(target=worker, args=(dt,))
        tr.setDaemon(True)
        tr.start()
        threads.append(tr)
    for t in threads:
        t.join()
