from extractor import extract_data, client





class stock_journal(extract_data):
    model = 'stock.journal'

    query = """
            SELECT 'sj_vendite' id, 'Vendite' name
            UNION
            SELECT 'sj_sost' id, 'Assistenza' name
            UNION
            SELECT 'sj_gv' id, 'Greenvision' name
         """


class del_type_seq(extract_data):
    model = 'ir.sequence'
    query = """
        select distinct 'seq_picking_' + ts.interno id,
          'stock.picking.out.' + ts.interno name,
          upper(ts.interno) + '/%(year)s/' prefix,
          4 padding,
          1 number_increment,
          'standard' implementation,
          'base.main_company' [company_id/id]
        FROM oerp.tipi_sped ts
        UNION
        select distinct 'seq_delivery_' + upper(ts.esterno) id,
          'delivery.' + ts.esterno name,
          upper(ts.esterno) + '/%(year)s/' prefix,
          4 padding,
          1 number_increment,
          'no_gap' implementation,
          'base.main_company' [company_id/id]
        FROM oerp.tipi_sped ts
        UNION
        select distinct 'seq_picking_' + ts.interno id,
          'stock.picking.out.' + ts.interno name,
          upper(ts.interno) + '/%(year)s/' prefix,
          4 padding,
          1 number_increment,
          'standard' implementation,
          'greenvision' [company_id/id]
        FROM oerp.tipi_sped ts
        UNION
        select distinct 'seq_delivery_' + upper(ts.esterno) id,
          'delivery.' + ts.esterno name,
          upper(ts.esterno) + '/%(year)s/' prefix,
          4 padding,
          1 number_increment,
          'no_gap' implementation,
          'greenvision' [company_id/id]
        FROM oerp.tipi_sped ts
    """


class delivery_type(extract_data):
    model = 'thema.delivery.type'
    query = """
        SELECT ts.id,
          'seq_picking_' + ts.interno [internal_id/id],
          'seq_delivery_' + ts.esterno [public_id/id],
          name,
          [stock_journal_id/id],
          [location_id/id],
          [location_dest_id/id],
          causale shipment_reason,
          CASE WHEN (ts.interno='SAG' OR ts.interno='SAGE' OR ts.interno='GVS')  THEN 1 ELSE 0 END is_anticipate
        FROM oerp.tipi_sped ts
    """


class delivery(extract_data):
    model = 'delivery.delivery'
    query = """
        SELECT
          'DELV' + dbo.fn_RifDocumento(tb.TIPO_DOC, tb.ANNO_DOC, tb.NUMERO_DOC) id,
          rtrim(xp.xid + isnull(tb.DESTINAZIONE, '')) [partner_id/id],
           case COD_DEPOSITO
            when 'GV' then ltrim(dbo.fn_RifDocumento('9/', tb.ANNO_BOLLA, tb.NUMERO_BOLLA))
            else dbo.fn_RifDocumento(tb.TIPO_BOLLA, tb.ANNO_BOLLA, tb.NUMERO_BOLLA)
            end name,
           case COD_DEPOSITO
            when 'GV' then ltrim(dbo.fn_RifDocumento('9/', tb.ANNO_BOLLA, tb.NUMERO_BOLLA))
            else dbo.fn_RifDocumento(tb.TIPO_BOLLA, tb.ANNO_BOLLA, tb.NUMERO_BOLLA)
            end number,
           NULL [invoice_id/id],
           case when tb.TIPO_BOLLA is null then 'open' else 'done' end state,
           case COD_DEPOSITO
           when 'GV' then 'greenvision'
           else 'base.main_company' end [company_id/id],
           ts.id [delivery_type_id/id],
          'VT' + COD_VETTORE [carrier_id/id],
          CONVERT(VARCHAR(24),isnull(tb.DATA_BOLLA, tb.DATA_REGISTRAZ), 126) date,
          cast(tb.PESO as float) weight,
          cast(tb.PESO_NETTO as float) weight_net,
          tb.NUMERO_COLLI number_of_packages,
          case tb.COD_PORTO
          when 'PA' then 'stock.incoterm_CIF'
          else 'stock.incoterm_EXW' end [incoterm_id/id],
          case when tpg.F_CONTRASSEGNO = 1 then 1 else 0 end cod
        FROM T_BOLLE_VENDITA tb
        INNER JOIN oerp.x_partner xp ON tb.TIPO_ANAGRAFICO = xp.TIPO_ANAGRAFICO and xp.CAMPO_RICERCA = tb.CAMPO_RICERCA
        inner join TAB_PAGAMENTI tpg ON tpg.COD_PAGAMENTO = tb.COD_PAGAMENTO
        left join oerp.tipi_sped ts on substring(ts.id, 3, 100) = tb.COD_DOC_BOLCLI
        WHERE ('{datai}' = 'None' OR tb.DATA_REGISTRAZ >= '{datai}')
        AND ('{dataf}' = 'None' OR tb.DATA_REGISTRAZ <= '{dataf}')
        UNION
        SELECT
          'DELV' + dbo.fn_RifDocumento(tb.TIPO_DOC, tb.ANNO_DOC, tb.NUMERO_DOC) id,
          rtrim(xp.xid + isnull(tb.DESTINAZIONE, '')) [partner_id/id],
           dbo.fn_RifDocumento(tb.TIPO_DOC, tb.ANNO_DOC, tb.NUMERO_DOC) name,
           NULL number,
           tb.TIPO_DOC+substring(cast(tb.ANNO_DOC AS varchar),3,2)+'/'+cast(tb.NUMERO_DOC AS varchar) [invoice_id/id],
           'done' state,
           case COD_DEPOSITO
           when 'GV' then 'greenvision'
           else 'base.main_company' end [company_id/id],
           'tsBC' [delivery_type_id/id],
          'VT' + COD_VETTORE [carrier_id/id],
          CONVERT(VARCHAR(24),tb.DATA_DOC, 126) date,
          cast(tb.PESO as float) weight,
          cast(tb.PESO_NETTO as float) weight_net,
          tb.NUMERO_COLLI number_of_packages,
          case tb.COD_PORTO
          when 'PA' then 'stock.incoterm_CIF'
          else 'stock.incoterm_EXW' end [incoterm_id/id],
          case when tpg.F_CONTRASSEGNO = 1 then 1 else 0 end cod
        FROM T_FATT_VENDITA tb
        INNER JOIN oerp.x_partner xp ON xp.TIPO_ANAGRAFICO = 'C' and xp.CAMPO_RICERCA = tb.CAMPO_RICERCA
        INNER JOIN TAB_DOC_FATVEN tf ON tf.COD_DOC_FATVEN = tb.COD_DOC_FATVEN
        INNER JOIN TAB_PAGAMENTI tpg ON tpg.COD_PAGAMENTO = tb.COD_PAGAMENTO
        where tf.F_DIF_IMM = 'I'
        AND ('{datai}' = 'None' OR tb.DATA_DOC >= '{datai}')
        AND ('{dataf}' = 'None' OR tb.DATA_DOC <= '{dataf}')


    """


class pickings(extract_data):
    model = 'stock.picking'

    query = """
         SET NOCOUNT ON;
          SELECT DISTINCT
            ID_BOLLA,
            ID_ORDINE
          INTO #b2
          FROM oerp.spedizioni_bolle2;

          SELECT DISTINCT
            ID_FATTURA,
            ID_ORDINE
          INTO #f2
          FROM oerp.spedizioni_fatture2;

          SELECT
            *
          INTO #osp
          FROM oerp.ordina_spedizioni;

          SELECT
            thema.xid(
                'PKB' + cast(b.ID_BOLLA AS VARCHAR(20)) + '/' +
                isnull(cast(b.ID_ORDINE AS VARCHAR(10)), '')
            )                                                                     id,
            'out'                                                                 [type],
            isnull(
                dbo.fn_RifDocumento(o.TIPO_DOC, o.ANNO_DOC, o.NUMERO_DOC) + '/' + cast(ob.n AS VARCHAR(10)),
                dbo.fn_RifDocumento(tb.TIPO_DOC, tb.ANNO_DOC, tb.NUMERO_DOC)
            )                                                                     name,
            'DELV' + dbo.fn_RifDocumento(tb.TIPO_DOC, tb.ANNO_DOC, tb.NUMERO_DOC) [delivery_id/id],
            rtrim(xp.xid + isnull(tb.DESTINAZIONE, ''))                           [partner_id/id],
            convert(VARCHAR(100), tb.DATA_BOLLA, 126)                             [date],
            ts.id                                                                 [delivery_type_id/id],
            ts.[stock_journal_id/id],
            'done'                                                                state,
            convert(VARCHAR(100), tb.DATA_BOLLA, 126)                             [date_done],
            ts.[location_id/id],
            ts.[location_dest_id/id]

          FROM #b2 b
            INNER JOIN T_BOLLE_VENDITA tb ON tb.ID_BOLLA = b.ID_BOLLA
            LEFT JOIN T_ORDINI_CLI o ON o.ID_ORDINE = b.ID_ORDINE
            LEFT JOIN #osp ob ON ob.ID_DOC = b.ID_BOLLA AND ob.typ = 'B' AND ob.ID_ORDINE = b.ID_ORDINE
            LEFT JOIN oerp.x_partner xp ON xp.TIPO_ANAGRAFICO = tb.TIPO_ANAGRAFICO AND tb.CAMPO_RICERCA = xp.CAMPO_RICERCA
            INNER JOIN oerp.tipi_sped ts ON substring(ts.id, 3, 10) = tb.COD_DOC_BOLCLI
          WHERE ('{datai}' = 'None' OR tb.DATA_REGISTRAZ >= '{datai}')
                AND ('{dataf}' = 'None' OR tb.DATA_REGISTRAZ <= '{dataf}')
          UNION ALL
          SELECT
            thema.xid(
                'PKF' + cast(b.ID_FATTURA AS VARCHAR(20)) + '/' +
                isnull(cast(b.ID_ORDINE AS VARCHAR(10)), '')
            )                                                                     id,
            'out'                                                                 [type],
            isnull(
                dbo.fn_RifDocumento(o.TIPO_DOC, o.ANNO_DOC, o.NUMERO_DOC) + '/' + cast(ob.n AS VARCHAR(10)),
                dbo.fn_RifDocumento(tb.TIPO_DOC, tb.ANNO_DOC, tb.NUMERO_DOC)
            )                                                                     name,
            'DELV' + dbo.fn_RifDocumento(tb.TIPO_DOC, tb.ANNO_DOC, tb.NUMERO_DOC) [delivery_id/id],
            rtrim(xc.xid + isnull(tb.DESTINAZIONE, ''))                           [partner_id/id],
            convert(VARCHAR(100), tb.DATA_DOC, 126)                               [date],
            ts.id                                                                 [delivery_type_id/id],
            ts.[stock_journal_id/id],
            'done'                                                                state,
            convert(VARCHAR(100), tb.DATA_DOC, 126)                               [date_done],
            ts.[location_id/id],
            ts.[location_dest_id/id]

          FROM #f2 b
            INNER JOIN T_FATT_VENDITA tb ON tb.ID_FATTURA = b.ID_FATTURA
            LEFT JOIN T_ORDINI_CLI o ON o.ID_ORDINE = b.ID_ORDINE
            LEFT JOIN #osp ob ON ob.ID_DOC = b.ID_FATTURA AND ob.typ = 'F' AND ob.ID_ORDINE = o.ID_ORDINE
            LEFT JOIN oerp.x_clienti xc ON xc.CAMPO_RICERCA = tb.CAMPO_RICERCA
            INNER JOIN oerp.tipi_sped ts ON ts.id = 'tsBC'
          WHERE ('{datai}' = 'None' OR tb.DATA_DOC >= '{datai}')
                AND ('{dataf}' = 'None' OR tb.DATA_DOC <= '{dataf}')
    """

    def bulk_import(self, model=None, start=None, end=None, context=None):
        pk = client.get_model('stock.picking')
        mv = client.get_model('stock.move')
        ids = pk.search([])
        names = pk.read(ids, fields=['name'])
        xmlids = pk.get_external_id(ids)
        xmlids = {int(k): v.split('.')[-1] for k, v in xmlids.items()}
        names = {xmlids[el['id']]: el['name'] for el in names}
        xmlids = {v: k for k, v in xmlids.items()}
        for d in self.data:
            if names[d['id']] != d['name']:
                id = xmlids[d['id']]
                move_ids = pk.read(id, fields=['move_lines'])['move_lines']
                mv.write(move_ids, {'state': 'draft'})
                mv.unlink(move_ids)
                pk.write(id, {'state': 'draft'})
                pk.unlink([id])
        return super(pickings, self).bulk_import(model, start, end, context)

class pickings_bo(extract_data):
    model = 'stock.picking'
    query = """
        WITH b AS (
            SELECT DISTINCT
              ID_BOLLA,
              ID_ORDINE
            FROM oerp.spedizioni_bolle2
        ), f AS (
            SELECT DISTINCT
              ID_FATTURA,
              ID_ORDINE
            FROM oerp.spedizioni_fatture2
        ),
            sp AS (
            SELECT
              os.*
            FROM b
              INNER JOIN oerp.ordina_spedizioni os ON os.typ = 'B' AND os.ID_DOC = b.ID_BOLLA AND os.ID_ORDINE = b.ID_ORDINE
            UNION ALL
            SELECT
              os.*
            FROM f
              INNER JOIN oerp.ordina_spedizioni os ON os.typ = 'F' AND os.ID_DOC = f.ID_FATTURA AND os.ID_ORDINE = f.ID_ORDINE

          )
        SELECT
          CASE WHEN os.typ = 'B' THEN 'PKB' ELSE 'PKF' END
          + cast(os.ID_DOC AS VARCHAR(20)) + '/' + isnull(cast(os.ID_ORDINE AS VARCHAR(10)), '')   id,
          CASE WHEN os2.typ = 'B' THEN 'PKB' ELSE 'PKF' END
          + cast(os2.ID_DOC AS VARCHAR(20)) + '/' + isnull(cast(os2.ID_ORDINE AS VARCHAR(10)), '') [backorder_id/id]
        FROM sp os
          INNER JOIN sp os2 ON os.ID_ORDINE = os2.ID_ORDINE AND os2.n = os.n - 1

    """


class picking_moves(extract_data):
    model = 'stock.move'
    chunk_size = 50000

    query = """
        SELECT
          'PKBD' + cast(b.ID_BOLLA AS VARCHAR(20)) + '/' + cast(b.PROGRESSIVO_RIGA AS VARCHAR(10))     id,
          'PKB' + cast(b.ID_BOLLA AS VARCHAR(20)) + '/' + isnull(cast(b.ID_ORDINE AS VARCHAR(10)), '') [picking_id/id],
          '[' + rtrim(db.COD_PRODOTTO) + '] ' + isnull(db.DESCR_PRODOTTO, '/')                         name,
          thema.xid(db.COD_PRODOTTO)                                                                   [product_id/id],
          cast(db.QTA_BOLLA AS FLOAT)                                                                  [product_qty],
          CASE db.UNITA_DI_MISURA
          WHEN 'PZ' THEN 'product.product_uom_unit'
          WHEN 'PA' THEN 'thema_product.product_uom_pair'
          END                                                                                          [product_uom/id],
          isnull(ts.[location_id/id], isnull('dp' + rtrim(
              CASE db.COD_DEPOSITO
              WHEN '001' THEN NULL
              ELSE cast(db.COD_DEPOSITO AS VARCHAR(100))
              END
          ), 'stock.stock_location_stock'))                                                            [location_id/id],
          isnull(ts.[location_dest_id/id], 'stock.stock_location_customers')                           [location_dest_id/id],
          rtrim(isnull(xf.xid, xc.xid + isnull(tb.DESTINAZIONE, '')))                                  [partner_id/id],
          'done'                                                                                       state,
          CASE db.COD_DEPOSITO
          WHEN 'GV' THEN 'greenvision' ELSE 'main_company' END                                         [company_id/id]
        FROM oerp.spedizioni_bolle2 b
          INNER JOIN D_BOLLE_VENDITA db ON db.ID_BOLLA = b.ID_BOLLA AND db.PROGRESSIVO_RIGA = b.PROGRESSIVO_RIGA
          INNER JOIN T_BOLLE_VENDITA tb ON tb.ID_BOLLA = db.ID_BOLLA
          INNER JOIN oerp.tipi_sped ts ON substring(ts.id, 3, 10) = tb.COD_DOC_BOLCLI
          LEFT JOIN oerp.x_clienti xc ON xc.CAMPO_RICERCA = tb.CAMPO_RICERCA AND tb.TIPO_ANAGRAFICO = 'C'
          LEFT JOIN oerp.x_fornitori xf ON xf.COD_FORNITORE = tb.CAMPO_RICERCA AND tb.TIPO_ANAGRAFICO = 'F'
        WHERE ('{datai}' = 'None' OR tb.DATA_REGISTRAZ >= '{datai}')
        AND ('{dataf}' = 'None' OR tb.DATA_REGISTRAZ <= '{dataf}')
        UNION ALL
        SELECT
          'PKFD' + cast(b.ID_FATTURA AS VARCHAR(20)) + '/' + cast(b.PROGRESSIVO_RIGA AS VARCHAR(10))     id,
          'PKF' + cast(b.ID_FATTURA AS VARCHAR(20)) + '/' + isnull(cast(b.ID_ORDINE AS VARCHAR(10)), '') [picking_id/id],
          isnull(db.DESCR_PRODOTTO, '/')                                                                 name,
          thema.xid(db.COD_PRODOTTO)                                                                     [product_id/id],
          cast(db.QTA_FATTURATA AS FLOAT)                                                                [product_qty],
          CASE db.UNITA_DI_MISURA
          WHEN 'PZ' THEN 'product.product_uom_unit'
          WHEN 'PA' THEN 'thema_product.product_uom_pair'
          END                                                                                            [product_uom/id],
          isnull(ts.[location_id/id], isnull('dp' + rtrim(
              CASE db.COD_DEPOSITO
              WHEN '001' THEN NULL
              ELSE cast(db.COD_DEPOSITO AS VARCHAR(100))
              END
          ), 'stock.stock_location_stock'))                                                              [location_id/id],
          isnull(ts.[location_dest_id/id], 'stock.stock_location_customers')                             [location_dest_id/id],
          rtrim(xc.xid + isnull(tb.DESTINAZIONE, ''))                                                    [partner_id/id],
          'done'                                                                                         state,
          CASE db.COD_DEPOSITO
          WHEN 'GV' THEN 'greenvision' ELSE 'main_company' END                                           [company_id/id]
        FROM oerp.spedizioni_fatture2 b
          INNER JOIN D_FATT_VENDITA db ON db.ID_FATTURA = b.ID_FATTURA AND db.PROGRESSIVO_RIGA = b.PROGRESSIVO_RIGA
          INNER JOIN T_FATT_VENDITA tb ON tb.ID_FATTURA = db.ID_FATTURA
          INNER JOIN oerp.tipi_sped ts ON ts.id = 'tsBC'
          LEFT JOIN oerp.x_clienti xc ON xc.CAMPO_RICERCA = tb.CAMPO_RICERCA
        WHERE ('{datai}' = 'None' OR tb.DATA_DOC >= '{datai}')
        AND ('{dataf}' = 'None' OR tb.DATA_DOC <= '{dataf}')

    """

class pickings_ordini(extract_data):
    model = 'stock.picking'
    query = """
        SELECT
          *
        FROM oerp.spedizioni_ordini_t t
          INNER JOIN T_ORDINI_CLI o ON o.ID_ORDINE = t.ID_ORDINE
          INNER JOIN TAB_DOC_ORDCLI tdo ON tdo.COD_DOC_ORDCLI = o.COD_DOC_ORDCLI
          INNER JOIN oerp.tipi_sped ts ON (substring(ts.id, 3, 10) = tdo.COD_DOC_GENERATO AND tdo.TIPO_FATTURAZ = 'D')
                                          OR (ts.id = 'tsBC' AND tdo.TIPO_FATTURAZ = 'I')
          LEFT JOIN oerp.x_fornitori xf ON xf.COD_FORNITORE = o.CAMPO_RICERCA AND o.TIPO_ANAGRAFICO = 'F'
          LEFT JOIN oerp.x_clienti xc ON xc.CAMPO_RICERCA = o.CAMPO_RICERCA AND o.TIPO_ANAGRAFICO = 'C'
    """
def bolle(datai, dataf):
    b = pickings(datai=datai, dataf=dataf)
    b.bulk_import(context={'ignore_methods': True})
    b = pickings_bo(datai=datai, dataf=dataf)
    b.bulk_import(context={'ignore_methods': True, 'nocreate': True})
    b = picking_moves(datai=datai, dataf=dataf)
    b.bulk_import(context={'ignore_methods': True, 'import_skip_store': True})



def go(datai, dataf):
    l = stock_location()
    l.load()
    l.print_messages()
    j = stock_journal()

    j.load()
    j.print_messages()
    sq = del_type_seq()
    sq.load()
    sq.print_messages()
    dt = delivery_type()
    dt.load()
    dt.print_messages()
    dl = delivery(datai=datai, dataf=dataf)
    dl.bulk_import(context={'import': True})
    bolle(datai=datai, dataf=dataf)


if __name__ == '__main__':
    dt = delivery_type()
    dt.load()
    dt.print_messages()
    '''
    import sys
    if len(sys.argv) > 1:
        datai = sys.argv[1]; dataf = sys.argv[2]
    else:
        datai = None; dataf = '2014-07-22'

    go(datai, dataf)
    '''
