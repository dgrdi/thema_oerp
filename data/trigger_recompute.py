from requests.exceptions import ConnectionError
import sys
from extractor import client
from thwork import cth, split_work
import argparse
from threading import Lock
import time

parser = argparse.ArgumentParser()
parser.add_argument('-d', help='domain')
parser.add_argument('model')
parser.add_argument('fields', nargs='+')
parser.add_argument('-b', help='batch size')
parser.add_argument('-r', help='reset all records')
parser.add_argument('-t', help='threads')


def trigger_recompute(model, fields, domain=None, batch=500000, threads=20):
    errors = []
    l = Lock()
    if batch is None:
        batch = 500000
    m = client.get_model(model)
    if domain == 'reset':
        ids = m.store_get_null(fields)
    else:
        if domain is None:
            domain = []
        else:
            domain = eval(domain)
        ids = m.search(domain)

    def chunkify(data):
        for i in range(0, len(data), batch):
            yield data[i:i+batch]
    def worker(data):
        print cth().getName(), 'Triggering %s records' % len(data)
        done = False
        while not done:
            try:
                m.store_trigger_recompute(data, fields)
            except ConnectionError as e:
                with l:
                    errors.append(e)
                raise
            else:
                done = True
    for c in chunkify(ids):
        split_work(c, worker, nthreads=threads)
    return errors

def reset_first(model, fields, val, batch):
    val = eval(val)
    m = client.get_model(model)
    m.store_set_null(fields, val)
    errors = True
    if val is None:
        domain = 'reset'
    else:
        domain = [(f, '=', val) for f in fields]
        for i in range(len(domain) - 1):
            domain.insert(0, '|')
        domain = repr(domain)
    while errors:
        errors = trigger_recompute(model, fields, domain=domain, batch=batch)
        time.sleep(1)


if __name__ == '__main__':
    p = parser.parse_args(sys.argv[1:])
    if p.r:
        reset_first(p.model, p.fields, p.r, p.b)
    else:
        trigger_recompute(p.model, p.fields, p.d, p.b and int(p.b), threads=int(p.t) if p.t else 6)