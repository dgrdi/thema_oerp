import csv
from pprint import pprint
import openerp_jsonrpc_client as ojc
import threading
import types
from functools import wraps

from db import db
import settings

class ModelWrapper(object):
    class SaneException(Exception, ojc.OpenERPJSONRPCClientException):
        def __init__(self, message, data):
            self.data = data
            super(ModelWrapper.SaneException, self).__init__(message)

        def __str__(self):
            return '%s:\n%s' % (self.message, '\n'.join('%s: %s' % (k, v) for k, v in self.data.items()))

    def __init__(self, model):
        self._model = model

    def __getattr__(self, item):
        itm = getattr(self._model, item)
        if isinstance(itm, (types.FunctionType, types.MethodType)):
            @wraps(itm)
            def wrapper(*a, **k):
                try:
                    return itm(*a, **k)
                except ojc.OpenERPJSONRPCClientException as e:
                    raise self.SaneException(e.message, e.data)
            return wrapper
        return itm

class ClientWrapper(object):
    def __init__(self, client):
        self._client = client

    def get_model(self, name):
        mod = self._client.get_model(name)
        return ModelWrapper(mod)

    def __getattr__(self, item):
        return getattr(self._client, item)


try:
    client = ClientWrapper(ojc.OpenERPJSONRPCClient(settings.OERP_SERVER))
    client.session_authenticate(db=settings.OERP_DB, login=settings.OERP_USER, password=settings.OERP_PASSWD)
except ojc.OpenERPJSONRPCClientException as e:
    print pprint(e.data['debug'])
    raise


class extract_data(object):
    model = None
    extra_fields = {}
    exclude = []
    query = None
    threads = 20
    chunk_size = 50000
    azienda = 'AZIENDA_TOP'

    extra_map = {
        'AZIENDA_TOP': '',
        'AZIENDA_TEC': 'ca',
        'AZIENDA_TES': 'es'
    }
    cod_map = {
        'AZIENDA_TOP': 'it',
        'AZIENDA_TEC': 'ca',
        'AZIENDA_TES': 'es',
    }
    xid_map = {
        'AZIENDA_TOP': 'base.main_company',
        'AZIENDA_TES': 'thema_spagna',
        'AZIENDA_TEC': 'thema_canaria',
    }

    @property
    def extra(self):
        return self.extra_map[self.azienda]

    @property
    def company_id(self):
        ir = client.get_model('ir.model.data')
        dt = ir.search([('name', '=', self.xid_map[self.azienda].split('.')[-1]), ('model', '=', 'res.company')])[0]
        dt = ir.read(dt, fields=['res_id'])
        return dt['res_id']

    def __init__(self, query=None, azienda=None, **kw):
        if azienda is not None:
            self.azienda = azienda
        self.args = {
            'azienda': self.extra,
            'code': self.cod_map[self.azienda],
            'xid_azienda': self.xid_map[self.azienda],
        }
        self.args.update(kw)
        self._setup_extra()
        if query is not None:
            self.query = query
        with db.session as s:
            s.execute('USE %s;' % self.azienda)
            print('loading data from db...')
            print 'USE %s;' % self.azienda
            print self.query.format(**self.args)
            res = s.execute(self.query.format(**self.args))
            print('processing data..')
            self.data = []
            dt = res.fetchmany(1000)
            while dt:
                self.data.extend(self.process_row(r) for r in dt)
                dt = res.fetchmany(1000)
            self.data = self.process_set(self.data)
            print('done')

    @property
    def fields(self):
        if len(self.data):
            return self.data[0].keys()

    def matrix(self, start, end):
        return [x.values() for x in self.data[start:end]]

    def _setup_extra(self):
        for k, v in self.extra_fields.items():
            if hasattr(v, '__call__'):
                pass
            elif isinstance(v, basestring):
                v = getattr(self, v)
            else:
                raise ValueError('Invalid field: {0}'.format(v))
            self.extra_fields[k] = v

    def process_row(self, row):
        row = dict(row.items())
        for k, v in self.extra_fields.items():
            row[k] = v(row)
        for k in self.exclude:
            row.pop(k)
        return row

    def process_set(self, rows):
        return rows

    def load(self, model=None, start=None, end=None, context=None):
        if model is None: model = self.model
        if model is None: raise ValueError('need a model name')
        if context is None: context = {'import': True}
        start = start if start else 0
        end = end if end else len(self.data)

        try:
            md = client.get_model(model)
            print('exporting data to oerp (records: {0})'.format(end-start))
            res = md.load(self.fields, self.matrix(start, end), context=context)
        except ojc.OpenERPJSONRPCClientException as e:
            for v in e.data.values():
                print(v)
            raise
        else:
            self.created = res['ids'] if 'ids' in res else []
            self.errors = res['messages'] if 'messages' in res else []
            return len(self.errors) == 0

    @classmethod
    def purge(cls, model):
        md = client.get_model(model)
        ids = md.search([])
        md.unlink(ids)

    def print_messages(self):
        print('\n'.join([x['message'] for x in self.errors]))

    def bulk_import(self, model=None, start=None, end=None, context=None):
        threads = []
        start = start or 0
        end = end or len(self.data)
        step = (end - start) / self.threads
        for i in range(self.threads):
            kw = {
                'model': model,
                'start': start + i * step,
                'end': start + (i + 1) * step if i < self.threads - 1 else end,
                'context': context
            }
            t = threading.Thread(target=self.do_bulk_import, kwargs=kw)
            t.setDaemon(True)
            t.start()
            threads.append(t)
        for t in threads:
            t.join()

    def do_bulk_import(self, model=None, start=None, end=None, context=None):
        if model is None: model = self.model
        if model is None: raise ValueError('need a model name')
        context = context or {}
        context['import'] = True
        start = start if start else 0
        end = end if end else len(self.data)
        print('## {1}: model {0}'.format(model, threading.currentThread().getName()))
        print('## {0}: start {1}, end {2}'.format(threading.currentThread().getName(), start, end))
        def chunkify(lst):
            ln = len(lst)
            for i in range(0, len(lst), self.chunk_size):
                yield lst[i:i+self.chunk_size]
        try:
            md = client.get_model(model)
            matrix = self.matrix(start, end)
            for i, chunk in enumerate(chunkify(matrix)):
                print('##{1}: exporting data to oerp (records: {0}, chunk: {2})'.format(len(chunk),
                                                                                        threading.currentThread().getName(),
                                                                                        i))
                res = md.bulk_import(self.fields, chunk, context=context)
        except ojc.OpenERPJSONRPCClientException as e:
            for v in e.data.values():
                print(threading.currentThread().getName(), v)
            raise


class extract_data_pg(extract_data):
    def __init__(self, query=None, azienda=None, **kw):
        if azienda is not None:
            self.azienda = azienda
        self.args = {
            'azienda': self.extra,
            'code': self.cod_map[self.azienda],
            'xid_azienda': self.xid_map[self.azienda],
        }
        self.args.update(kw)
        self._setup_extra()
        if query is not None:
            self.query = query
        with db.pg_session as s:
            print('loading data from db...')
            print self.query.format(**self.args)
            res = s.execute(self.query.format(**self.args))
            print('processing data..')
            self.data = []
            dt = res.fetchmany(1000)
            while dt:
                self.data.extend(self.process_row(r) for r in dt)
                dt = res.fetchmany(1000)
            self.data = self.process_set(self.data)
            print('done')


class extract_csv(extract_data):
    csv = None

    def __init__(self, csvfname=None):
        if not csvfname:
            csvfname = self.csv
        print('loading data from csv...')
        with open(csvfname) as fo:
            dr = csv.DictReader(fo)
            res = list(dr)
        self.data = map(self.process_row, res)
        self.data = self.process_set(self.data)
        print('done')
