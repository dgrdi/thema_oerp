from extractor import client
import re

taxes = [
    '22cee',
    '22art17',
    'art9art17',
    '22art7ter',
]

def get_transit_account_acccount(company_id):
    r = client.get_model('account.account').search([('code', '=', '404556'), ('company_id', '=', company_id)])
    return r[0]

def generate_transit_acct(company_id):
    root = client.dataset_search_read('account.tax.code', fields=['id'], domain=[
        ('parent_id', '=', False),
        ('company_id', '=', company_id),
    ])['records'][0]
    tc = client.get_model('account.tax.code')
    parent_vals = {
        'id': str(company_id) + 'IVT',
        'code': 'IVT',
        'parent_id/.id': root['id'],
        'name': 'Conti di transito',
        'company_id/.id': company_id,
    }
    tc.load(parent_vals.keys(), [parent_vals.values()])
    parent = tc.search([('code', '=', 'IVT'), ('company_id', '=', company_id)])[0]

    child_vals = {
        'id': str(company_id) + 'IVTtr',
        'code': 'IVTtr',
        'parent_id/.id': parent,
        'name': 'Transito IVA',
        'company_id/.id': company_id
    }
    tc.load(child_vals.keys(), [child_vals.values()])
    child = tc.search([('code', '=', 'IVTtr'), ('company_id', '=', company_id)])[0]
    return  child

def get_transit_acct(company_id):
    tc = client.get_model('account.tax.code')
    res = tc.search([('code', '=', 'IVTtr'), ('company_id', '=', company_id)])
    if not res:
        return generate_transit_acct(company_id)
    return res[0]


def generate_child(tax):
    transit_acct = get_transit_account_acccount(tax['company_id'][0])
    transit_tax = get_transit_acct(tax['company_id'][0])
    if tax['child_ids']:
        return None

    first, second = tax.copy(), tax.copy()
    first.update({
        'amount': 1,
        'description': first['description'] + 'plus',
        'name': first['name']+ ' (Carico)',
        'sequence': 100,

    })
    second.update({
        'description': second['description'] + 'minus',
        'name': second['name']+ ' (Giro)',
        'amount': -1,
        'base_code_id': None,
        'ref_base_code_id': None,
        'account_collected_id': transit_acct,
        'account_paid_id': transit_acct,
        'tax_code_id': transit_tax,
        'ref_tax_code_id': transit_tax,
        'sequence': 200,
    })
    def cleanm2(o):
        for k, v in o.iteritems():
            if isinstance(v, list) and len(v) == 2:
                o[k] = v[0]
        return o

    writ = {
        'child_depend': True,
        'base_code_id': None,
        'ref_base_code_id': None,
        'account_collected_id': None,
        'account_paid_id': None,
        'tax_code_id': None,
        'ref_tax_code_id': None,
        'child_ids': [(0, 0, cleanm2(first)), (0, 0, cleanm2(second))]
    }
    return writ

def update_taxes():
    tx = client.get_model('account.tax')
    dom = [('description', 'like', a + '%') for a in taxes]
    dom = ['|' for _ in range(len(dom) - 1 )] + dom
    dom.insert(0, ('parent_id', '=', False))
    txdata = client.dataset_search_read('account.tax', domain=dom)
    for t in txdata['records']:
        vals = generate_child(t)
        if vals:
            tx.write(t['id'], vals)

def generate_auto():
    tx = client.get_model('account.tax')
    def find_comp(name):
        res = client.dataset_search_read('account.tax',
                                       domain=[
                                           ('description', 'like', name + '%'),
                                           ('type_tax_use', '=', 'sale'),
                                           ('parent_id', '=', False),
                                       ])
        return set(r['company_id'][0] for r in res['records'])

    def gen_vals(name, company):
        nname = 'auto' + name
        sale = tx.search([
            ('description', 'like', name + '%'),
            ('company_id', '=', company),
            ('type_tax_use', '=', 'sale'),
            ('parent_id', '=', False),
            ('reverse_charge', '=', False)
        ])
        purchase = tx.search([
            ('description', 'like', name + '%'),
            ('company_id', '=', company),
            ('type_tax_use', '=', 'purchase'),
            ('parent_id', '=', False),
            ('reverse_charge', '=', False)
        ])
        assert len(sale) == 1, sale
        assert len(purchase) == 1, purchase
        sale, purchase = sale[0], purchase[0]
        longname = tx.read(sale, fields=['name'])['name']
        longname = re.match('([^\(]+)', longname).group(1).strip()
        return {
            'id': nname + str(company),
            'description': nname,
            'name': longname,
            'type': 'none',
            'type_tax_use': 'purchase',
            'company_id/.id': company,
            'reverse_charge': 'True',
            'rc_sale_tax_id/.id': sale,
            'rc_purchase_tax_id/.id': purchase
        }

    vals = []
    for name in taxes:
        for company in find_comp(name):
            vals.append(gen_vals(name, company))
    if vals:
        keys = vals[0].keys()
        data = [[el[k] for k in keys] for el in vals]
        print tx.load(keys, data)


def clean():
    res = client.dataset_search_read('account.tax', domain=[('parent_id', '!=', False), ('child_ids', '!=', False)])
    dl = [id for el in res['records'] for id in el['child_ids'] ]
    client.get_model('account.tax').unlink(dl)
    res = client.dataset_search_read('account.tax', domain=[('reverse_charge', '=', True)])
    dl = [id for el in res['records'] for id in el['child_ids'] ]
    if dl:
        client.get_model('account.tax').unlink(dl)



if __name__ == '__main__':
    update_taxes()
    generate_auto()
