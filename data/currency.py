from extractor import  extract_data

class currency_rate(extract_data):
    model = 'res.currency.rate'
    query = "select * from oerp.currency_rate"

    def process_set(self, rows):
        ids = set()
        ret = []
        for r in rows:
            if r['id'] not in ids:
                ids.add(r['id'])
                ret.append(r)
        return ret

def go():
    cr = currency_rate()
    cr.bulk_import()

if __name__ == '__main__':
    go()