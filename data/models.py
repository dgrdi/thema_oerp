from extractor import extract_data

class extract_mod(extract_data):
    model = 'product.model'
    query = """
        select thema.xid('PMD' + rtrim(COD_MODELLO)) id,
          rtrim(COD_MODELLO) name,
          thema.xid(coll.collection) [collection_id/id],
        case substring(cat.COD_CAT_MERC, 2, 1)
          when 'V' then 'thema_product.product_category_eye'
          when 'S' then 'thema_product.product_category_sun'
          when 'P' then 'thema_product.product_category_reading'
        end [categ_id/id]
        from TAB_MODELLO MOD
        cross apply (
          select top 1 COD_CAT_MERC from MAGAZZINO_1 where COD_MODELLO = MOD.COD_MODELLO
        ) cat
        outer apply (
          select top 1 rtrim(COD_LINEA) collection from MAGAZZINO_1 where COD_MODELLO = MOD.COD_MODELLO and COD_LINEA IS NOT NULL
        ) coll
        where cat.COD_CAT_MERC is not null
    """


def go():
    m = extract_mod()
    m.bulk_import(context={'import': True, 'ignore_methods': True})

if __name__ == '__main__':
    go()