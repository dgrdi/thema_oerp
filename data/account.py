from extractor import client, extract_data
import datetime
import pprint

COMPANY_MAP = {
    'main_company': 'it',
    'thema_canaria': 'ca',
    'thema_spagna': 'es',
}

def id_company_map():
    cmp = client.get_model('res.company')
    ids = cmp.get_external_id(cmp.search([]))
    return  {int(k): COMPANY_MAP[v.split('.')[-1]] for k, v in ids.items() if v.split('.')[-1] != 'greenvision'}

def company_map_id():
    return {v: k for k, v in id_company_map().items()}

def set_xid():
    """imposta xid sui conti"""
    acc = client.get_model('account.account')
    md = client.get_model('ir.model.data')
    accounts = acc.read(acc.search([]), fields=('company_id', 'code'))
    cmpobj = client.get_model('res.company')
    companies = cmpobj.get_external_id(cmpobj.search([]))
    companies = {int(k): v.split('.')[-1] for k, v in companies.items()}
    already_has_xid = [x['res_id'] for x in md.read(md.search([('model', '=', 'account.account')]), fields=('res_id', ))]
    to_create = []
    for a in filter(lambda x: x['id'] not in already_has_xid, accounts):
        to_create.append({
            'name': (COMPANY_MAP[companies[a['company_id'][0]]] + a['code']).replace('.', '_'),
            'model': 'account.account',
            'res_id': a['id'],
        })
    msg=' account.account CREATED'
    if (len (to_create)>0):
        md.bulk_create(to_create)
        msg=str(len (to_create)) + msg
    else:
        msg='NO' + msg
    print (msg)


def set_xid_tax():
    """imposta xid sulle aliguote delle tasse"""
    acc = client.get_model('account.tax')
    md = client.get_model('ir.model.data')
    acc_tax = acc.read(acc.search([]), fields=('company_id', 'description'))

    cmpobj = client.get_model('res.company')
    companies = cmpobj.get_external_id(cmpobj.search([]))
    companies = {int(k): v.split('.')[-1] for k, v in companies.items()}
    already_has_xid = [x['res_id'] for x in md.read(md.search([('model', '=', 'account.tax')]), fields=('res_id', ))]
    to_create = []
    for a in filter(lambda x: x['id'] not in already_has_xid, acc_tax):
        to_create.append({
            'name': (COMPANY_MAP[companies[a['company_id'][0]]]  + a['description']).replace('.', '_').replace(' ', ''),
            'model': 'account.tax',
            'res_id': a['id'],
        })
    msg=' account.tax CREATED'
    if (len (to_create)>0):
        md.bulk_create(to_create)
        msg=str(len (to_create)) + msg
    else:
        msg='NO' + msg
    print (msg)

def set_xid_tax_code():
    acc = client.get_model('account.tax.code')
    md = client.get_model('ir.model.data')
    acc_tax = acc.read(acc.search([('code', '!=', False)]), fields=('company_id', 'code'))
    cmpobj = client.get_model('res.company')
    companies = cmpobj.get_external_id(cmpobj.search([]))
    companies = {int(k): v.split('.')[-1] for k, v in companies.items()}
    already_has_xid = [x['res_id'] for x in md.read(md.search([('model', '=', 'account.tax.code')]), fields=('res_id', ))]
    to_create = []
    for a in filter(lambda x: x['id'] not in already_has_xid, acc_tax):
        to_create.append({
            'name': (COMPANY_MAP[companies[a['company_id'][0]]]  + a['code']).replace('.', '_').replace(' ', ''),
            'model': 'account.tax.code',
            'res_id': a['id'],
        })
    msg=' account.tax.code CREATED'
    if (len (to_create)>0):
        md.bulk_create(to_create)
        msg=str(len (to_create)) + msg
    else:
        msg='NO' + msg
    print (msg)



def create_fiscalyears():
    fys = range(2007, 2016)
    fym = client.get_model('account.fiscalyear')
    fields = ('id', 'name','code', 'date_start', 'date_stop', 'company_id/id')
    datas = []
    companies = company_map_id()
    for cmp, cmpk in COMPANY_MAP.iteritems():
        for f in fys:
            ex = fym.search([('date_start', '=', datetime.date(f, 1, 1).strftime('%Y-%m-%d')),
                             ('company_id', '=', companies[cmpk])])
            if not ex:
                datas.append(('it' + unicode(f), unicode(f), unicode(f),
                              datetime.date(f, 1, 1).strftime('%Y-%m-%d'),
                              datetime.date(f, 12, 31).strftime('%Y-%m-%d'),
                              cmp,
                ))
    ret = fym.load(fields, datas)
    if ret['messages']:
        print pprint.pprint(ret['messages'])
        raise RuntimeError()
    for f in fys:
        for fyid in fym.search([('name', '=', unicode(f))]):
            ps = fym.read(fyid, fields=('period_ids',))
            if not ps['period_ids']:
                fym.create_period([fyid])

def fix_fy_ids():
    fym = client.get_model('account.fiscalyear')
    pm = client.get_model('account.period')
    md = client.get_model('ir.model.data')
    fydata = fym.read(fym.search([]))
    fydata = {f['id']: f for f in fydata}
    pdata = pm.read(pm.search([]))
    pdata = {p['id']: p for p in pdata}
    xids = md.search([('model', 'in', ('account.fiscalyear', 'account.period'))])
    companies = id_company_map()
    fields = ('model', 'name', 'res_id', 'module')
    datas = []
    for f in fydata.values():
        datas.append(('account.fiscalyear',
                      companies[f['company_id'][0]] + f['code'],
                      f['id'],
                      '__account__',
        ))
    for p in pdata.values():
        id = p['date_start'][:7].replace('-', '')
        if p['special']:
            id = p['date_start'][:4] + 'oc'
        datas.append(('account.period',
                      companies[fydata[p['fiscalyear_id'][0]]['company_id'][0]] + id,
                      p['id'],
                      '__account__'
        ))
    md.unlink(xids)
    msg = md.load(fields, datas)
    if msg['messages']:
        print pprint.pprint(msg['messages'])
        raise RuntimeError()


def fix_journal_ids():
    jm = client.get_model('account.journal')
    md = client.get_model('ir.model.data')
    xids = md.search([('model', '=', 'account.journal')])
    fields = ('model', 'name', 'res_id', 'module')
    datas = []
    journal_data = jm.read(jm.search([]))
    companies = id_company_map()
    for j in journal_data:
        datas.append((
            'account.journal',
            companies[j['company_id'][0]] + j['code'],
            j['id'],
            '__account__'
        ))
    md.unlink(xids)
    res = md.load(fields, datas)
    if res['messages']:
        print pprint.pprint(res['messages'])
        raise RuntimeError()


class account(extract_data):
    model = 'account.account'
    query = """
        SELECT '{code}' + rtrim(KEY_CONTO) id,
        '{code}' + rtrim(KEY_CONTO) code,
        DESCR_CONTO name,
        case
          when len(KEY_CONTO) = 2 then '{code}' + '0'
           else '{code}' + substring(KEY_CONTO, 1, len(KEY_CONTO) - 2)
        end [parent_id/id],
        case PARAM_BILANCIO
        when 'A' then 'account.data_account_type_asset'
        when 'P' then 'account.data_account_type_liability'
        when 'C' then 'account.data_account_type_expense'
        when 'R' then 'account.data_account_type_income'
        else 'account.data_account_type_asset' end [user_type/id],
        '{xid_azienda}' [company_id/id],
        'other' type,
        1 reconcile
        FROM ANAG_CONTO
        where KEY_CONTO not like '9091%'
    """

def go():
    for az in ['AZIENDA_TOP', 'AZIENDA_TES','AZIENDA_TEC']:
        ac = account(azienda=az)
        ac.bulk_import(context={'nowrite': True, 'import': True, 'import_skip_store':True})
    set_xid()
    set_xid_tax()
    set_xid_tax_code()
    create_fiscalyears()
    fix_fy_ids()
    fix_journal_ids()

if __name__ == '__main__':
    go()
