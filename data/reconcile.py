from db import session
from extractor import extract_data, client


def load_data():
    with session as s:
        res = s.execute("""
            SELECT
                TIPO_SEZIONALE,
                CAMPO_RICERCA,
                ID_SCADENZA,
                ID_PRIMA_NOTA,
                ID_PRIMA_NOTA_C
            FROM oerp.chiusura_scadenze c
            WHERE ID_PRIMA_NOTA_C IS NOT NULL
            ORDER BY ID_SCADENZA, ID_PRIMA_NOTA
        """)
        return list(res.fetchall())


def build_graph(data):
    nodes = {}
    for ts, cr, scad, pn, pn_c in data:
        nodes.setdefault((ts, cr, pn, scad), set()).add((pn_c, None))
        nodes.setdefault((ts, cr, pn_c, None), set()).add((pn, scad))
    return nodes


def find_subgraphs(graph):
    res = {}
    visited = set()
    i = 0
    for el, nodes in graph.items():
        part = el[:2]
        nod = el[2:]
        if nod in visited:
            continue
        subg = nodes.union({nod})
        l = None
        while len(subg) != l:
            l = len(subg)
            subg.update(set.union(*(graph[part + e] for e in subg)))
        i += 1
        visited.update(subg)
        res[i] = subg
    return res

def load_ids():
    with session as s:
        res = s.execute('''
            SELECT pn.ID_PRIMA_NOTA, pn.ID_SCADENZA, pn.xid
            FROM oerp.x_prima_nota pn
        ''').fetchall()
    ids = {}
    for pn, scad, xid in res:
        ids[(pn, scad)] = xid
    return ids

def load_rec():
    data = load_data()
    groups = find_subgraphs(build_graph(data))
    ids = load_ids()
    payload = []
    for g in groups.itervalues():
        g = [ids[(el[0], el[1])] for el in g]
        payload.append(g)
    client.get_model('account.move.line').reconcile_multiple(payload, context={'novalidate': True, 'no_store_function': True, 'no_store_compute': True})

if __name__ ==  '__main__':
    load_rec()

'''
def save_data(recdata):
    with session as s:
        s.execute("""
          DELETE FROM oerp.rec_d WHERE rec_typ = 'S';
          DELETE FROM oerp.rec_h WHERE typ = 'S';
        """)
        s.execute("""
            INSERT INTO oerp.rec_h(typ, id)
            SELECT 'S', :id
        """, [{'id': v} for v in recdata.keys()])

        s.execute("""
            INSERT INTO oerp.rec_d (rec_typ, rec_id, ID_PRIMA_NOTA, ID_SCADENZA)
            SELECT 'S', :id, :pn, :scad
        """, [{'id': id, 'pn': pn, 'scad': scad} for id, vals in recdata.items() for pn, scad in vals])

        s.commit()

def update_balance():
    with session as s:
        s.execute("""
            UPDATE rh
                SET saldo = d.delta
            FROM oerp.rec_h rh
              INNER JOIN (
              SELECT
                r.rec_typ,
                r.rec_id,
                sum(case when pn.F_DARE_AVERE = 'D'
                      then  isnull(s.IMPORTO_LIRE, pn.IMPORTO_LIRE)
                      else -isnull(s.IMPORTO_LIRE, pn.IMPORTO_LIRE)
                    end
                 ) delta
              FROM oerp.rec_d r
              LEFT JOIN MOV_SCADENZE s ON s.ID_SCADENZA = r.ID_SCADENZA
              INNER JOIN PRIMA_NOTA pn ON pn.ID_PRIMA_NOTA = r.ID_PRIMA_NOTA
              GROUP BY r.rec_typ, r.rec_id
            ) d ON d.rec_typ = rh.typ AND d.rec_id = rh.id
        """)
        s.commit()

def find_duplicates(recdata):
    seen = {}
    for id, vals in recdata.items():
        for v in vals:
            if seen.get(v):
                print '%s in %s and %s' % (v, seen.get(v), id)
            else:
                seen[v] = id

def calculate_reconcile():
    save_data(find_subgraphs(build_graph(load_data())))


class extract_rec(extract_data):
    model = 'account.move.reconcile'
    query = """
    SELECT
        thema.xid('I' + h.typ + cast(h.id as varchar) ) id,
        thema.xid('I' + h.typ + cast(h.id as varchar) ) name,
        'auto' type
    FROM oerp.rec_h h
    """

class set_rec(extract_data):
    model = 'account.move.line'
    query = """
        SELECT
          pn.xid id,
          case
            when h.saldo <> 0 then thema.xid('I' + h.typ + cast(h.id as varchar) )
            else NULL
          end [reconcile_partial_id/id],
          case
            when h.saldo  = 0 then thema.xid('I' + h.typ + cast(h.id as varchar) )
            else NULL
          end [reconcile_id/id],
          aj.journal_xid [journal_id/id]
         FROM oerp.rec_d d
        INNER JOIN oerp.rec_h h ON h.typ = d.rec_typ AND d.rec_id = h.id
        INNER JOIN oerp.x_prima_nota pn ON pn.ID_PRIMA_NOTA = d.ID_PRIMA_NOTA
        INNER JOIN PRIMA_NOTA ppn ON d.ID_PRIMA_NOTA = ppn.ID_PRIMA_NOTA
        INNER JOIN ARTICOLI a ON a.NUM_ARTICOLO = ppn.NUM_ARTICOLO AND a.ESERCIZIO = ppn.ESERCIZIO
        INNER JOIN oerp.art_journal aj ON aj.ID_ARTICOLO = a.ID_ARTICOLO
        AND (pn.ID_SCADENZA = d.ID_SCADENZA OR d.ID_SCADENZA IS NULL)
    """



if __name__ == '__main__':
    calculate_reconcile()
    update_balance()
    rec = extract_rec()
    rec.bulk_import()
    srec = set_rec()
    srec.bulk_import(context={'ignore_methods': True})


'''
