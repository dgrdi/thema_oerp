
from openerp.tools import config
from openerp.pooler import get_db_and_pool

config.parse_config(['--db_host=localhost', '-d', '<database>', '-r', '<db_user>', '-w', '<db_passwd>'])

connection, pool = get_db_and_pool('<database>')
cr = connection.cursor()
invoice = pool.get('account.invoice')
ids = invoice.search(cr, 1, [('state', '=', 'open')])
invoice.delete_workflow(cr, 1, ids)
invoice.create_workflow(cr, 1, ids)

from openerp.netsvc import LocalService
wkf = LocalService('workflow')
for id in ids: wkf.trg_delete(1, 'account.invoice', id, cr)
for id in ids: wkf.trg_create(1, 'account.invoice', id, cr)