import extractor

class users(extractor.extract_data):
    model = 'res.users'
    query = """
    SELECT
        thema.xid(rtrim(UserName)) id,
        'True' employee,
        Nominativo name,
        'it_IT' lang,
        lower(rtrim(UserName)) login,
        lower(rtrim(UserName)) new_password,
        'base.main_company' [company_id/id],
        'base.main_company' [company_ids/id]
    FROM TO88Master.dbo.VSMasterUsers
    WHERE len(UserName) > 3
    and UserName <> 'Administrator'
    """

    def process_set(self, rows):
        unames = set()
        ret =[]
        for r in rows:
            if r['login'] not in unames:
                unames.add(r['login'])
                ret.append(r)
        return ret


def go():
    u = users()
    u.load()

if __name__ == '__main__':
    go()