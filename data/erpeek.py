#!/usr/bin/env python
import xmlrpclib
import argparse
import pprint
import re
parser = argparse.ArgumentParser(description='Inspect data on OpenERP objects')
parser.add_argument('ids',nargs='*',help='end with a list of IDs if you are not using search parameters')
parser.add_argument('-m','--model',required=True,help='the type of object to find')
parser.add_argument('--server',default='http://localhost:8069',help='Full url to your host server xmlrpc port (default http://localhost:8069)')
parser.add_argument('-u','--user',required=True,help='username')
parser.add_argument('-d','--db',required=True,help='database')
parser.add_argument('-p','--password',required=True,help='password (yes this will be in your bash history and ps from other users)')
parser.add_argument('-s','--search',action='append', dest='search',help='A search condition (multiple allowed)')
parser.add_argument('-f',action='append', dest='fields', help='restrict the output to certain fields (multiple allowed)')

args = parser.parse_args()
#print args
sock = xmlrpclib.ServerProxy(args.server+'/xmlrpc/common')
uid = sock.login(args.db ,args.user ,args.password)
sock = xmlrpclib.ServerProxy(args.server+'/xmlrpc/object')
#do some searching if they pass in a search query, this will return a bunch of IDs to pretty print
#or alternatively the user can pass in a bunch of IDs on the command line to print
searchquery=[]
if(args.search):
    print "search parsing"
    #supported operators are
    #=, !=, >, >=, <, <=, like, ilike, in, not in, child_of, parent_left, parent_right
    for s in args.search:
        query=re.search('(.*)(=|!=|>|>=|<|<=|like|ilike|in|not in|child_of|parent_left|parent_right)(.*)',s)
        field= query.group(1).strip()
        operator= query.group(2).strip()
        value= query.group(3).strip()
        searchquery.append((field,operator,value))
    #print searchquery
    ids = sock.execute(args.db, uid, args.password, args.model, 'search', searchquery)
else:
    ids=args.ids

if (args.fields):
    data = sock.execute(args.db, uid, args.password, args.model, 'read', ids, args.fields )
else:
    data = sock.execute(args.db, uid, args.password, args.model, 'read', ids )

pp = pprint.PrettyPrinter(indent=4)
pp.pprint(data)