from db import session

epc_data = None
max_epc = None


def load_data(s):
    global epc_data
    global max_epc
    if epc_data is not None:
        return
    epc_data = {True: {}, False: {}}
    res = s.execute("SELECT EPC, COD_DEPOSITO, rtrim(COD_PRODOTTO) from rfid_epc ORDER BY EPC").fetchall()
    for epc, cdep, pcode in res:
        neutral = (cdep != '001')
        epc_data[neutral][pcode] = epc
    max_epc = epc


def get_or_create_epc(s, product_code, neutral):
    load_data(s)
    product_code = product_code.strip()
    epc = epc_data[neutral].get(product_code)
    if not epc:
        epc = create_epc(s, product_code, neutral)
    return epc


def create_epc(s, product_code, neutral):
    global max_epc
    cdep = 'NEU' if neutral else  '001'
    max_epc += 1
    print 'Generating EPC for %s, %s' % (product_code, neutral)
    v = locals().copy()
    v['max_epc'] = max_epc
    s.execute("INSERT INTO rfid_epc (COD_PRODOTTO, COD_DEPOSITO, EPC) VALUES (:product_code, :cdep, :max_epc)",
              v)
    epc_data[neutral][product_code] = max_epc
    return max_epc

def generate(s, product_code, neutral, qty, rif):
    epc = get_or_create_epc(s, product_code, neutral)
    qty = int(float(qty) * 1.05)
    cdep = '001' if not neutral else 'NEU'
    print 'Generating %s pieces for product %s, %s' % (qty, product_code, cdep)
    s.execute("""
        INSERT INTO thema.rfid_esistenti (DATA_CREAZIONE, EPC, PROGRESSIVO, rif)
        SELECT convert(date, getdate()), rf.EPC, rf.PROGRESSIVO, :rif
        FROM thema.genera_rfid(:cdep, :product_code, :qty) rf
    """, locals())

def parse_line(s, rif, product_code, qty_nonneutral, qty_neutral):
    if qty_neutral:
        generate(s, product_code, True, qty_neutral, rif)
    if qty_nonneutral:
        generate(s, product_code, False, qty_nonneutral, rif)

def parse_file(s, fname):
    import csv
    with open(fname) as fo:
        reader = csv.reader(fo)
        for line in reader:
            if line and line[0]:
                print 'rif: %s, product. %s, nonneutral: %s, neutral: %s' % (line[0], line[1], line[3], line[4])
                parse_line(s, line[0], line[1], line[3], line[4])


def generate_files(s, date_min, date_max):
    res = s.execute("""
        SELECT
          e.rif, rf.epc, rf.sku, rf.DESCRIPTION, rf.ean13, rf.internal_barcode, rf.internal_label
        FROM thema.dati_rfid rf
        INNER JOIN thema.rfid_esistenti e ON e.EPC = rf.NUM_EPC and e.PROGRESSIVO = rf.PROGRESSIVO
        WHERE rf.DATA_CREAZIONE >= :date_min AND rf.DATA_CREAZIONE <= :date_max
        ORDER BY e.rif, rf.epc, rf.PROGRESSIVO
    """, locals()).fetchall()
    import csv
    rif = None
    fo = None
    fields = ['epc', 'sku', 'DESCRIPTION', 'ean13', 'internal_barcode', 'internal_label']
    for el in res:
        el = dict(zip(el.keys(), el))
        el_rif = el.pop('rif')
        if el_rif != rif:
            print el_rif
            if fo:
                fo.close()
            rif = el_rif
            fo = open('%s.csv' % rif.replace(' ', '_').replace('/', '_'), 'wb')
            writer = csv.DictWriter(fo, fields)
            writer.writeheader()
        writer.writerow(el)
    fo.close()
