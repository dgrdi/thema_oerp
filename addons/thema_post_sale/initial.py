from openerp.osv import orm, fields

import logging

logger = logging.getLogger(__name__)


class request(orm.Model):
    _inherit = 'postsale.request'


    def import_cleanup(self, cr, uid, context=None):
        imd = self.pool.get('ir.model.data')
        existing = imd.search(cr, uid, [('model', '=', 'postsale.request')])
        existing = imd.read(cr, uid, existing, fields=['res_id'], context=context)
        # remove all previous things that were not already shipped
        exids = [el['res_id'] for el in existing]
        rm = self.search(cr, uid, [('id', 'in', exids), '|', ('sale_id', '=', False), ('sale_id.state', '!=', 'done')],
                         context=context)
        sales = self.read(cr, uid, rm, fields=['sale_id'], context=context)
        logger.debug('Unlinking %s requests', len(rm))
        self.unlink(cr, uid, rm, context=context)
        usales = [el['sale_id'][0] for el in sales if el['sale_id']]
        logger.debug('Unlinking %s sale orders', len(usales))
        self.pool.get('sale.order').unlink(cr, uid, usales, context=context)


    def import_(self, cr, uid, data, context=None):
        new_data = []
        product = self.pool.get('product.product')
        part = self.pool.get('product.part')
        partner = self.pool.get('res.partner')
        part_ids = part.search(cr, uid, [], context=context)
        part_ids = part.get_external_id(cr, uid, part_ids, context=context)
        part_ids = {v.split('.')[-1]: int(k) for k, v in part_ids.items()}
        imd = self.pool.get('ir.model.data')
        dtype = imd.get_object_reference(cr, uid, '', 'ts03')[-1]
        partner_data = partner.read(cr, uid,
                                    partner.search(cr, uid, [('customer', '=', True)], context={'active_test': False}),
                                    fields=['code'], context=context)
        partner_data = {el['code']: el['id'] for el in partner_data}

        pids = ['part_temple_left', 'part_temple_right', 'part_nose']
        pids = map(lambda x: imd.get_object_reference(cr, uid, 'thema_post_sale', x)[-1], pids)

        product_ids = product.search(cr, uid,
                                     [('default_code', 'in',
                                       list({el['product_code'].strip() for el in data if el['product_code']}))],
                                     context=context)
        product_data = product.find_parts(cr, uid, product_ids, pids, context=context)
        product_codes = product.read(cr, uid, product_ids, fields=['default_code'], context=context)
        product_codes = {el['default_code']: el['id'] for el in product_codes}

        existing = imd.search(cr, uid, [('model', '=', 'postsale.request')])
        existing = imd.read(cr, uid, existing, fields=['name'], context=context)
        existing = set(el['name'] for el in existing)

        user = self.pool.get('res.users')
        users = user.read(cr, uid, user.search(cr, uid, []), fields=['login'])
        users = {el['login']: el['id'] for el in users}
        origins = {
            'online': imd.get_object_reference(cr, uid, 'thema_order_types', 'origin_internet')[-1],
            'phone': imd.get_object_reference(cr, uid, 'thema_order_types', 'origin_phone')[-1],
        }

        def get_gen_product(product_type):
            if not product_type or product_type == 'frame':
                return [imd.get_object_reference(cr, uid, 'thema_post_sale', 'product_generic_frame')[-1]]
            elif product_type == 'temple':
                return [
                    imd.get_object_reference(cr, uid, 'thema_post_sale', 'product_generic_temple_left')[-1],
                    imd.get_object_reference(cr, uid, 'thema_post_sale', 'product_generic_temple_right')[-1],
                ]
            else:
                return [imd.get_object_reference(cr, uid, 'thema_post_sale', 'product_generic_%s' % product_type)[-1]]

        new_ids = []
        new_cancel = []
        new_process = []
        new_frames = []

        for d in data:
            if d['id'] in existing:
                continue
            name = None
            partner_id = partner_data.get(d['partner_code'])
            if not partner_id:
                logger.debug('Cannot find partner for code: %s', d['partner_code'])
                continue
            product_id = product_codes.get(d['product_code'])
            if not product_id:
                logger.warning('Cannot find product for code: %s', d['product_code'])
            if d['part_type'] == 'temple' and product_id:
                products = []
                for t in ('temple_left', 'temple_right'):
                    pid = part_ids['part_%s' % t]
                    pt = product_data.get(product_id, {}).get(pid)
                    if pt:
                        products.append(pt)
                    else:
                        logger.warning('Cannot find product for %s, %s (looking for temple pair)', d['product_code'], t)
                        products = get_gen_product('temple')
                        break
            else:
                if 'part_type' in d and d['part_type'] != 'frame' and d['part_type'] != 'temple' and product_id:
                    pid = part_ids['part_%s' % d['part_type']]
                    product_id = product_data.get(product_id, {}).get(pid)
                if not product_id:
                    logger.warning('Cannot find product for %s, %s', d['product_code'], d['part_type'])
                    products = get_gen_product(d['part_type'])
                    name = d['name']
                else:
                    products = [product_id]

            new_data.append({
                'product_ids': products,
                'delivery_type_id': dtype,
                'partner_id': partner_id,
                'note': d['note'],
                'date': d['date'],
                'user_id': users.get(d['user'], False),
                'origin_id': origins.get(d['origin']),
            })

            new_ids.append(d['id'])
            if name:
                new_data[-1]['name'] = name
            new_cancel.append(True if d['operation'] == 'cancel' else False)
            new_process.append(True if d['operation'] == 'process' else False)
            new_frames.append(True if d['frame'] else False)
            logger.debug('Registrando richiesta %s: prodotto %s', d['id'], product_id)
        cids = self.register_requests(cr, uid, new_data, context=context)
        for id, newid in zip(cids, new_ids):
            imd.create(cr, uid, {
                'module': '__import__',
                'model': 'postsale.request',
                'res_id': id,
                'name': newid,
            })

        so = self.pool.get('sale.order')
        sol = self.pool.get('sale.order.line')
        for i, req in enumerate(self.browse(cr, uid, cids, context=context)):
            if new_frames[i]:
                line = req.sale_id.order_line[0]
                frame = line.product_id.frame_id
                if frame:
                    vals = {
                        'product_id': frame.id,
                        'line_type_id': line.line_type_id.id,
                        'product_uom_qty': line.product_uom_qty,
                        'product_uom': line.product_uom.id,
                    }
                    vals = sol._prepare_line(cr, uid, req.sale_id, vals, context=context)
                    sol.write(cr, uid, req.sale_id.order_line[0].id, vals, context=context)

                    if len(req.sale_id.order_line) > 1:
                        sol.unlink(cr, uid, [el.id for el in req.sale_id.order_line[1:]], context=context)
            if new_cancel[i]:
                so.action_cancel(cr, uid, [req.sale_id.id], context=context)
            if new_process[i]:
                req.refresh()
                so.write(cr, uid, req.sale_id.id, {'state': 'done'}, context=context)
                sol.write(cr, uid, [el.id for el in req.sale_id.order_line], {'state': 'done'}, context=context)





