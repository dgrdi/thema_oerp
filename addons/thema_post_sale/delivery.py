from openerp.osv import orm, fields


class delivery_type(orm.Model):
    _inherit = 'thema.delivery.type'

    _columns = {
        'postsale': fields.boolean('Assistenza post-vendita'),
        'is_anticipate': fields.boolean('Sostituzione anticipata'),
    }

    _defaults = {
        'is_anticipate': 0
    }


class invoice_type(orm.Model):
    _inherit = 'sale_journal.invoice.type'

    _columns = {
        'postsale': fields.boolean('Assistenza post-vendita'),
    }