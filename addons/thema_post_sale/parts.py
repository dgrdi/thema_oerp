from openerp.osv import orm, fields
from .combinator import FieldCombinator
from logging import getLogger

logger = getLogger(__name__)


class replacement_part(orm.Model):
    _name = 'product.part'

    _columns = {
        'name': fields.char('Nome', required=True),
        'parent_id': fields.many2one('product.part', 'Parte di'),
        'categ_id': fields.many2one('product.category', 'Categoria prodotto', required=True),
        'field_ids': fields.many2many('ir.model.fields', 'product_part_fields_rel', 'part_id', 'field_id',
                                      string='Campi prodotto'),
        'prefix': fields.char('Prefisso codice'),
        'suffix': fields.char('Suffisso codice'),
        'child_ids': fields.one2many('product.part', 'parent_id', 'Componenti'),
    }

    _sql_constraints = [
        ('uniq_name', "UNIQUE (name)", "Il nome del pezzo di ricambio deve essere univoco!")
    ]


class product(orm.Model):
    _inherit = 'product.template'

    _columns = {
        'part_id': fields.many2one('product.part', 'Parte di ricambio'),
        'frame_id': fields.many2one('product.product', 'Motatura di riferimento'),
    }


class model(orm.Model):
    _inherit = 'product.model'
    _columns = {
        'part_ids': fields.many2many('product.product', 'product_model_parts_rel', 'model_id', 'product_id',
                                     string='Pezzi di ricambio', domain=[('part_id', '!=', False)]),
        'product_ids': fields.one2many('product.product', 'model_id', domain=[('part_id', '=', False)],
                                       string='Prodotti'),
    }

    def generate_parts(self, cr, uid, ids, part_ids, context=None):
        if not part_ids or not ids:
            return {}
        fields = {}
        all_fields = set()
        # expand parts into their components (childs)
        all_parts = []
        for part in self.pool.get('product.part').browse(cr, uid, part_ids, context=context):
            parts = [part]
            clean = False
            while not clean:
                clean = True
                for p in parts:
                    if p.child_ids:
                        clean = False
                        parts.remove(p)
                        parts.extend(p.child_ids)
            all_parts.extend(parts)
            for p in parts:
                for f in p.field_ids:
                    all_fields.add(f.name)
                    fields.setdefault(p.id, set()).add(f.name)

        # read product data, from the parts and from saleable related parts
        product = self.pool.get('product.product')
        product_ids = product.search(cr, uid, [('model_id', 'in', ids)], context=context)
        values = product.read(cr, uid, product_ids,
                              fields=list(all_fields) + ['model_id', 'part_id', 'default_code', 'name'],
                              context=context)
        data = {}
        data_parts = {}
        for v in values:
            if v['part_id']:
                data_parts.setdefault(v['model_id'][0], {}).setdefault(v['part_id'][0], []).append(v)
            else:
                data.setdefault(v['model_id'][0], []).append(v)
        created = {}
        for part in all_parts:
            for model in self.browse(cr, uid, ids, context=context):
                created[model.id] = []
                if not data.get(model.id):
                    continue
                comb = FieldCombinator(product, data.get(model.id, []), fields[part.id])
                comb_products = comb.copy()
                comb.fill(data_parts.get(model.id, {}).get(part.id, []))
                comb_products.fill(data[model.id])
                for point in comb:
                    if not comb.get(point):
                        ref = comb_products.get_orm(cr, uid, point, context=context)
                        if ref:
                            logger.debug('Generating part %s for model %s with dimension values %s', part.name,
                                         model.name, point)
                            cid = product.copy(cr, uid, ref['id'], self._prepare_part(cr, uid, part, ref),
                                               context=context)
                            product.write(cr, uid, cid, {'name': '%s %s' % (part.name, ref['name'])}, context=context)
                            created[model.id].append(cid)
                if created.get(model.id):
                    self.write(cr, uid, model.id, {'part_ids': [(4, id) for id in created[model.id]]}, context=context)
        return created

    def _prepare_part(self, cr, uid, part, ref_values):
        ref = ref_values.copy()
        ref = {
            'name': '%s %s' % (part.name, ref['name']),
            'default_code': False if not ref['default_code'] else '%s%s%s' % (part.prefix and part.prefix or '',
                                                                              ref['default_code'],
                                                                              part.suffix and part.suffix or ''),
            'part_id': part.id,
            'sale_ok': False,
            'ean13': False,
            'categ_id': part.categ_id.id,
            'frame_id': ref['id'],
        }
        return ref




class generate_wiz(orm.TransientModel):
    _name = 'product.part.generate'
    _columns = {
        'part_ids': fields.many2many('product.part', string='Parti di ricambio')
    }

    def action_generate(self, cr, uid, ids, context=None):
        context = context or []
        part_ids = self.read(cr, uid, ids[0], context=context)['part_ids']
        model_ids = context.get('active_ids', [])
        parts = self.pool.get('product.model').generate_parts(cr, uid, model_ids, part_ids, context=context)
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'product.product',
            'domain': repr([('id', 'in', [el for l in parts.values() for el in l])]),
            'view_mode': 'tree,form',
            'name': 'Prodotti generati',
        }