from openerp.osv import orm, fields

class location(orm.Model):
    _inherit = 'stock.location'

    _columns = {
        'postsale': fields.boolean('Assistenza post-vendita')
    }
