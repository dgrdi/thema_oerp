from openerp.osv import orm, fields
from openerp.addons import decimal_precision as dp
from openerp.osv.osv import except_osv


class sale_order(orm.Model):
    _inherit = 'sale.order'
    _columns = {
        'postsale_origin_id': fields.many2one('sale.order', ondelete='cascade', string='Ordine origine'),
    }


class response_data(orm.Model):
    _name = 'postsale.response.data'
    _inherit = 'devutils.copyonwrite'

    _columns = {
        'user_id': fields.many2one('res.users', 'Utente'),
        'state': fields.selection([('open', 'Aperta'), ('wip', 'In evasione'), ('done', 'Evasa')],
                                  string='Stato', required=True, readonly=True),
        'carrier_id': fields.many2one('delivery.carrier', 'Vettore'),
        'weight': fields.float(digits_compute=dp.get_precision('Product Weight'), string='Peso'),
        'number_of_packages': fields.integer('Numero colli'),
        'incoterm_id': fields.many2one('stock.incoterms', 'Incoterm'),
    }

    _defaults = {
        'state': 'open',
        'weight': 0.1,
        'number_of_packages': 1,
        'incoterm_id': lambda s, cr, uid, ctx=None: s.pool.get('ir.model.data'). \
            get_object_reference(cr, uid, 'stock', 'incoterm_CFR')[-1],
    }


class response(orm.Model):
    _auto = False
    _name = 'postsale.response'
    _inherits = {
        'postsale.response.data': 'data_id'
    }

    _columns = {
        'data_id': fields.many2one('postsale.response.data', ondelete='cascade', required=True),
        'partner_id': fields.many2one('res.partner', 'Partner', readonly=True),
        'delivery_type_id': fields.many2one('thema.delivery.type', 'Tipo bolla', readonly=True),
        'name': fields.related('partner_id', 'display_name', string='Nome', type='char', readonly=True),
        'line_ids': fields.one2many('postsale.response.line', 'response_id', string='Righe richiesta'),
        'tot_qty': fields.float('Totale pz. richiesti', digits_compute=dp.get_precision('Product UoS'), readonly=True),
        'date_first': fields.date('Data prima richiesta', readonly=True),
        'date_last': fields.date('Data ultima richiesta', readonly=True),
    }

    def init(self, cr):
        cr.execute("""
          DROP VIEW IF EXISTS postsale_response;
          CREATE VIEW postsale_response AS
            SELECT
              (so.partner_id << 16) + so.delivery_type_id id,
              (so.partner_id << 16) + so.delivery_type_id data_id,
              so.partner_id partner_id,
              so.delivery_type_id,
              SUM(ol.product_uom_qty) tot_qty,
              MIN(so.date_order) date_first,
              MAX(so.date_order) date_last
            FROM sale_order so
            INNER JOIN sale_order_line ol ON ol.order_id = so.id
            INNER JOIN thema_delivery_type dt ON dt.id = so.delivery_type_id
            WHERE dt.postsale
            AND so.state IN ('draft', 'progress')
            AND ol.state in ('draft', 'confirmed')
            GROUP BY so.partner_id, so.delivery_type_id
        """)

    def write(self, cr, user, ids, vals, context=None):
        # check for concurrency
        if isinstance(ids, (int, long)):
            ids = [ids]
        if self.search(cr, user, [('id', 'in', ids), ('user_id', '!=', user)], context=context):
            raise except_osv('Attenzione!', 'Qualcun altro sta gia` evadendo questa richiesta.')
        vals['user_id'] = user
        return super(response, self).write(cr, user, ids, vals, context=context)


    def action_confirm(self, cr, uid, ids, context=None):
        deliveries = []
        for resp in self.browse(cr, uid, ids, context=context):
            dv = self._process_response(cr, uid, resp, context=context)
            if dv:
                deliveries.append(dv)
        self.pool.get('postsale.response.data').unlink(cr, uid, ids, context=context)
        act = {
            'type': 'ir.actions.act_window',
            'res_model': 'delivery.delivery',
            'view_mode': 'tree,form',
            'domain': repr([('id', 'in', deliveries)]),
            'name': 'Bolle generate',
        }
        if len(deliveries) == 1:
            act['view_mode'] = 'form'
            act['res_id'] = deliveries[0]
        return act

    def _process_response(self, cr, uid, resp, context=None):
        lines = {}
        orders = {}
        for line in resp.line_ids:
            orders[line.order_id.id] = line.order_id
            lines.setdefault(line.order_id.id, []).append(line)
        picking_ids = []
        dv = False
        resp_data = {
            'carrier_id': resp.carrier_id and resp.carrier_id.id or False,
            'weight': resp.weight,
            'number_of_packages': resp.number_of_packages,
            'incoterm_id': resp.incoterm_id and resp.incoterm_id.id or False
        }
        for order in orders.values():
            picking_ids.extend(self._process_sale(cr, uid, order, lines[order.id], context=context))
        pick = self.pool.get('stock.picking')
        delivery = self.pool.get('delivery.delivery')
        if picking_ids:
            p = picking_ids[0]
            dv = pick.action_delivery_create(cr, uid, [p], context=context)[p]
            for p in picking_ids[1:]:
                pick.action_delivery_append(cr, uid, p, dv, context=context)
            delivery.write(cr, uid, dv, resp_data, context=context)
            delivery.action_assign(cr, uid, [dv], context=context)
        return dv


    def _process_sale(self, cr, uid, order, lines, context=None):
        states = {}
        so = self.pool.get('sale.order')
        sol = self.pool.get('sale.order.line')
        pick = self.pool.get('stock.picking.out')
        rld = self.pool.get('postsale.response.line.data')

        for line in lines:
            states.setdefault(line.operation, []).append(line.id)

        def copy(operation):
            line_ids = states.pop(operation, None)
            if line_ids:
                rld.unlink(cr, uid, line_ids, context=context)
                if states:
                    nid = so.copy(cr, uid, order.id, {'order_line': [], 'postsale_origin_id': order.id, },
                                  context=context)
                    sol.write(cr, uid, line_ids, {'order_id': nid}, context=context)
                    return nid
                else:
                    return order.id

        pick_ids = []
        for op in states.keys():
            ord_id = copy(op)
            if op == 'cancel':
                so.action_cancel(cr, uid, [ord_id], context=context)
            elif op == 'block':
                so.action_block(cr, uid, [ord_id], context=context)
            elif op == 'process':
                so.action_button_confirm(cr, uid, [ord_id], context=context)
                order = so.browse(cr, uid, ord_id, context=context)
                pick_ids = [p.id for p in order.picking_ids]
                pick.force_assign(cr, uid, pick_ids)
                pick.action_move(cr, uid, pick_ids, context=context)
                so.write(cr, uid, ord_id, {'state': 'done'}, context=context)
        return pick_ids


class response_line_data(orm.Model):
    _inherit = 'devutils.copyonwrite'
    _name = 'postsale.response.line.data'

    _columns = {
        'operation': fields.selection([('cancel', 'Rifiuta'), ('block', 'Blocca'), ('process', 'Evadi')],
                                      string='Operazione'),
    }


class response_line(orm.Model):
    _auto = False
    _name = 'postsale.response.line'

    _inherits = {
        'postsale.response.line.data': 'data_id',
        'sale.order.line': 'order_line_id',
    }

    _columns = {
        'response_id': fields.many2one('postsale.response', readonly=True),
        'data_id': fields.many2one('postsale.response.line.data', 'Dati', ondelete='cascade', required=True),
        # these two are to alter the domain on product_id
        'order_line_id': fields.many2one('sale.order.line', 'Riga ordine', ondelete='cascade', required=True),
        'order_id': fields.many2one('sale.order', 'Ordine', readonly=True),
        'date': fields.date('Data', readonly=True),
        'product_id': fields.function(lambda s, *a, **k: s._product_id(*a, **k),
                                      fnct_inv=lambda s, *a, **k: s._product_id_set(*a, **k),
                                      type='many2one', string='Prodotto', relation='product.product'),
        'note': fields.related('order_line_id', 'order_id', 'note', type='text', readonly=True, string='Note'),
        'available': fields.related('product_id', 'qty_available_postsale', type='float',
                                    digits_compute=dp.get_precision('Product UoS'), string='Disponibile',
                                    readonly=True),
        'stock': fields.related('product_id', 'qty_available', type='float',
                                digits_compute=dp.get_precision('Product UoS'),
                                string='Giacenza tot.', readonly=True),
        'is_frame': fields.function(lambda s, *a, **k: s._parts(*a, **k), multi='parts',
                                    type='boolean', string='Montatura'),
        'can_expand': fields.function(lambda s, *a, **k: s._parts(*a, **k), multi='parts',
                                      type='boolean', string='Puo essere espanso'),

    }

    # product_id is proxied through a function field to get rid of the domain for it set in sale.order.line.
    def _product_id(self, cr, uid, ids, field, arg, context=None):
        v = self.pool.get('sale.order.line').read(cr, uid, ids, fields=['product_id'], context=context)
        return {el['id']: el['product_id'] for el in v}

    def _product_id_set(self, cr, uid, ids, field, value, arg, context=None):
        return self.pool.get('sale.order.line').write(cr, uid, ids, {field: value}, context=context)

    def _available(self, cr, uid, ids, field, arg, context=None):
        locations = self.pool.get('stock.location').search(cr, uid, [('usage', '!=', 'view'), ('postsale', '=', True)],
                                                           context=context)
        ctx = (context or {}).copy()
        ctx['location'] = locations
        res = {}
        for line in self.browse(cr, uid, ids, context=ctx):
            res[line.id] = line.product_id and line.product_id.qty_available or 0
        return res

    def _parts(self, cr, uid, ids, field, value, arg, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line.id] = {
                'is_frame': not line.product_id.frame_id,
                'can_expand': bool(
                    line.product_id.part_id and line.product_id.part_id.parent_id and line.product_id.frame_id)
            }
        return res


    def init(self, cr):
        cr.execute("""
            DROP VIEW IF EXISTS postsale_response_line;
            CREATE VIEW postsale_response_line AS
            SELECT l.id id,
                   l.id order_line_id,
                   l.order_id,
                   o.date_order date,
                   l.id data_id,
                   (o.partner_id << 16) + o.delivery_type_id response_id
            FROM sale_order_line l
            INNER JOIN sale_order o ON o.id = l.order_id
            INNER JOIN thema_delivery_type dt ON dt.id = o.delivery_type_id
            WHERE dt.postsale
            AND l.state in ('draft', 'confirmed')
            AND o.state in ('draft', 'process');
        """)

    def write(self, cr, user, ids, vals, context=None):
        # check for concurrency
        if isinstance(ids, (int, long)):
            ids = [ids]
        if self.search(cr, user, [('id', 'in', ids), ('response_id.user_id', '!=', user)], context=context):
            raise except_osv('Attenzione!', 'Qualcun altro sta gia` evadendo questa richiesta.')
        if isinstance(ids, (int, long)):
            ids = [ids]
        responses = self.read(cr, user, ids, fields=['response_id'], context=context)
        responses = list({el['response_id'][0] for el in responses})
        self.pool.get('postsale.response').write(cr, user, responses, {'user_id': user}, context=context)
        return super(response_line, self).write(cr, user, ids, vals, context=context)

    def onchange_product_id(self, cr, uid, ids, product_id, name, qty, uom_id, context=None):
        val = {}
        id = ids[0]
        if product_id:
            line = self.browse(cr, uid, id, context=context)
            val = self.pool.get('sale.order.line')._prepare_line(cr, uid, line.order_id,
                                                                 {'product_id': product_id,
                                                                  'product_uom_qty': qty, 'product_uom': uom_id,
                                                                  'line_type_id': line.line_type_id.id},
                                                                 context=context)
            prod = self.pool.get('product.product').browse(cr, uid, product_id, context=context)
            val.update({
                'is_frame': not prod.frame_id,
                'can_expand': bool(prod.part_id and prod.part_id.parent_id and prod.frame_id),
            })
        return {'value': val}

    def action_change_frame(self, cr, uid, ids, context=None):
        ol = self.pool.get('sale.order.line')
        for line in self.browse(cr, uid, ids):
            if line.product_id and line.product_id.frame_id:
                vals = {
                    'product_id': line.product_id.frame_id.id,
                    'product_uom_qty': line.product_uom_qty,
                    'product_uom': line.product_uom.id,
                    'line_type_id': line.line_type_id.id,
                }
                vals = ol._prepare_line(cr, uid, line.order_id, vals, context=context)
                ol.write(cr, uid, line.id, vals, context=context)
        return True

    def action_change_parent(self, cr, uid, ids, context=None):
        ol = self.pool.get('sale.order.line')
        product = self.pool.get('product.product')
        for line in self.browse(cr, uid, ids):
            if line.product_id and line.product_id.part_id and line.product_id.part_id.parent_id \
                    and line.product_id.frame_id:
                par = line.product_id.part_id.parent_id
                parts = product.find_parts(cr, uid, line.product_id.frame_id.id, [el.id for el in par.child_ids],
                                           context=context)
                for p in parts.values():
                    if p:
                        vals = {
                            'product_id': p[0],
                            'product_uom_qty': line.product_uom_qty,
                            'product_uom': line.product_uom.id,
                            'line_type_id': line.line_type_id.id,
                            'order_id': line.order_id.id,
                        }
                        vals = ol._prepare_line(cr, uid, line.order_id, vals)
                        ol.create(cr, uid, vals, context=context)
        return True


def action_cancel(self, cr, uid, ids, context=None):
    self.write(cr, uid, ids, {'operation': 'cancel'}, context=context)
    return True


def action_block(self, cr, uid, ids, context=None):
    self.write(cr, uid, ids, {'operation': 'block'}, context=context)
    return True


def action_do(self, cr, uid, ids, context=None):
    self.write(cr, uid, ids, {'operation': 'process'}, context=context)
    return True


