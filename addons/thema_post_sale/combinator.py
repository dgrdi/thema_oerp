from itertools import product
from collections import OrderedDict
from operator import itemgetter
from functools import partial
import collections


class Combinator(object):
    """
    A "combinator" object, for operating on an n-dimensional discrete space.
    Initialize with a dict describing the dimensions, where keys are arbitrary labels, and values are iterables
        that contain all the possible values for the dimensions.
    Iterating over the combinator object will yield every "point" in the n-space, represented as a dict mapping
    dimension labels to their values.

    The space can be populated and queried. add()ing a dict will populate the point in space described by the dict,
     as above. get(point) will then retrieve that particular dict as long as `point` describes the same point in
     the n-space.
    """

    def __init__(self, dimensions):
        self.dimensions = dimensions
        self.data = {}

    def __iter__(self):
        aggr = None
        for f in self.dimensions:
            aggr = self.combine(f, aggr)
        for el in aggr:
            res = dict(zip(reversed(list(self.dimensions)), self.unpack(el)))
            yield res

    def combine(self, field, iterable=None):
        if iterable is None:
            return self.dimensions[field]
        return product(self.dimensions[field], iterable)

    def unpack(self, tuple):
        if not hasattr(tuple, '__len__'):
            return [tuple]
        return [tuple[0]] + self.unpack(tuple[1])

    def fill(self, iterable):
        for el in iterable:
            self.add(el)

    def add(self, data):
        self.data[self.key(data)] = data

    def get(self, data):
        return self.data.get(self.key(data))


    def key(self, data):
        return tuple([(el, self.tuplify(data.get(el))) for el in self.dimensions])


    def tuplify(self, thing):
        if isinstance(thing, list):
            return tuple(thing)
        else:
            return thing

    def listify(self, thing):
        if isinstance(thing, tuple):
            return list(thing)
        else:
            return thing

    def copy(self):
        c = Combinator(self.dimensions)
        c.fill(self.data.values())
        return c


class FieldCombinator(Combinator):
    """
    A version of Combinator to operate on the space of a set of orm records, where the dimensions are some of the
    fields.
    """

    def __init__(self, object, data, fields):
        """
        :param object: an orm.Model object
        :param data: a list of dicts as from Model.read() that together define the space. These will define
            which values are possible for each dimension.
        :param fields: fields to combine (these will be the dimension names).
        """
        self.object = object
        self.types = {f: object._all_columns[f].column._type for f in fields}
        self.process_fields = [(el, type) for el, type in self.types.items() if
                               type in ('many2one', 'many2many', 'one2many')]
        if data:
            dimensions = OrderedDict()
            for field, type in self.types.items():
                values = map(self.listify, set(map(partial(self.cleaner, type), map(itemgetter(field), data))))
                dimensions[field] = values
        else:
            dimensions = {}
        super(FieldCombinator, self).__init__(dimensions)

    def cleaner(self, type, value):
        if type == 'many2one' and isinstance(value, collections.Iterable):
            return value[0]
        if type in ('one2many', 'many2many') and isinstance(value, collections.Iterable):
            return tuple(value)
        return value

    def data_cleaner(self, data):
        data = data.copy()
        for field, type in self.process_fields:
            data[field] = self.cleaner(data[field], type)
        return data

    def key(self, data):
        data = {el: self.cleaner(self.types.get(el, None), data.get(el)) for el in self.dimensions}
        return super(FieldCombinator, self).key(data)

    def get_clean(self, data):
        val = self.get(data)
        if val:
            return self.data_cleaner(val)
        return val

    def get_orm(self, cr, uid, data, context=None):
        """
        Will return a dict ready to be fed to Model.create() or Model.write() (cleans up 2many & 2one fields).
        """
        vals = self.get_clean(data)
        if vals:
            for field, value in vals.items():
                if isinstance(vals[field], collections.Iterable):
                    type = self.object._all_columns[field].column._type
                    if type == 'one2many':
                        obj = self.object._all_columns[field].column._obj
                        obj = self.object.pool.get(obj)
                        read_vals = [obj.copy_vals(cr, uid, id, context=context) for id in data[field]]
                        vals[field] = [(0, 0, v) for v in read_vals]
                    if type == 'many2many':
                        vals[field] = [(6, 0, vals[field])]
                    if type == 'many2one':
                        vals[field] = vals[field][0]
        return vals

    def copy(self):
        c = FieldCombinator(self.object, None, self.types.keys())
        c.dimensions = self.dimensions
        c.fill(self.data.values())
        return c