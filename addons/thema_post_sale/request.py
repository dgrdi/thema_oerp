from collections import OrderedDict
from openerp.osv import orm, fields
from dateutil.parser import parse

STATE_SEL = [('draft', 'Bozza'), ('open', 'Aperta'), ('cancel', 'Annullata'), ('blocked', 'Bloccata'),
             ('done', 'Evasa')]


class request(orm.Model):
    _name = 'postsale.request'

    _columns = {
        'date': fields.date('Data'),
        'company_id': fields.many2one('res.company', 'Azienda', required=True),
        'partner_id': fields.many2one('res.partner', 'Partner', required=True),
        'name': fields.function(lambda s, *a, **k: s._name_f(*a, **k),
                                type='char', string='Nome'),
        'delivery_type_id': fields.many2one('thema.delivery.type', domain=[('postsale', '=', True)],
                                            string='Tipo consegna', required=True),
        'invoice_type_id': fields.many2one('sale_journal.invoice.type', domain=[('postsale', '=', True)],
                                           string='Tipo fatturazione', required=True),
        'pricelist_id': fields.many2one('product.pricelist', string='Listino', required=True),
        'origin_id': fields.many2one('sale.order.origin', 'Origine', required=True),
        'line_ids': fields.one2many('postsale.request.line', 'request_id', 'Righe richiesta'),
        'model_id': fields.dummy(type='many2one', relation='product.model', string='Modello'),
        'product_ids': fields.dummy(type='one2many', relation='postsale.request.product',
                                    string='Pezzi di ricambio'),
        'user_id': fields.many2one('res.users', 'Commerciale'),
        'note': fields.text('Note'),
        'sale_id': fields.many2one('sale.order', ondelete='cascade'),
        'state': fields.function(lambda s, *a, **k: s._state(*a, **k), type='selection',
                                 selection=STATE_SEL, string='Stato'),
    }

    _defaults = {
        'date': fields.date.context_today,
        'origin_id': lambda s, c, u, ctx=None: s.pool.get('ir.model.data'). \
            get_object_reference(c, u, 'thema_order_types', 'origin_phone')[1],
        'invoice_type_id': lambda s, c, u, ctx=None: s.pool.get('ir.model.data'). \
            get_object_reference(c, u, 'thema_post_sale', 'invoice_type_postsale')[1],
        'user_id': lambda s, c, u, ctx=None: u,
    }

    def _name_f(self, cr, uid, ids, field, arg, context=None):
        res = {}
        product = self.pool.get('product.product')
        partner = self.pool.get('res.partner')
        for el in self.browse(cr, uid, ids, context=context):
            if el.partner_id:
                sp = partner.name_get(cr, uid, [el.partner_id.id], context=context)[0][1]
            else:
                sp = ''
            if len(el.line_ids) == 1:
                sr = product.name_get(cr, uid, [el.line_ids[0].product_id.id], context=context)[0][1]
                sr = '%s x %s' % (el.line_ids[0].qty, sr)
            else:
                sr = '%s pezzi' % sum(l.qty for l in el.line_ids)
            res[el.id] = '%s: %s' % (sp, sr)
        return res

    def _state(self, cr, uid, ids, field, arg, context=None):
        res = {}
        for req in self.browse(cr, uid, ids, context=context):
            if not req.sale_id:
                res[req.id] = 'draft'
            elif req.sale_id.has_delivery:
                res[req.id] = 'done'
            elif req.sale_id.state in ('cancel', 'blocked'):
                res[req.id] = req.sale_id.state
            elif req.sale_id.state in ('draft', 'done', 'confirmed'):
                res[req.id] = 'open'
            else:
                res[req.id] = False
        return res

    def onchange_partner_id(self, cr, uid, ids, partner_id, context=None):
        val = {}
        if partner_id:
            partner = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
            val.update({
                'company_id': partner.company_id.id,
                'pricelist_id': partner.property_product_pricelist and partner.property_product_pricelist.id or False,
            })
        return {'value': val}

    def onchange_model_id(self, cr, uid, ids, model_id, context=None):
        val = {}
        context = context or {}
        if model_id:
            line_ids = []
            model = self.pool.get('product.model').browse(cr, uid, model_id, context=context)
            for p in model.part_ids:
                line_ids.append({
                    'product_id': p.id,
                    'type': p.part_id.name,
                    'available': p.qty_available_postsale,
                    'total': p.qty_available,
                    'sort_key': (p.frame_id and p.frame_id.id or 0, p.color_id and p.color_id.id or 0, p.temple_length,
                                 p.part_id and p.part_id.id or 0),
                })
            for p in model.product_ids:
                line_ids.append({
                    'product_id': p.id,
                    'type': 'Montatura completa',
                    'available': p.qty_available_postsale,
                    'total': p.qty_available,
                    'sort_key': (p.id, p.color_id and p.color_id.id or 0, p.temple_length, 0)
                })
            line_ids.sort(key=lambda v: v['sort_key'])
            for el in line_ids: el.pop('sort_key')
            val['product_ids'] = line_ids
        return {'value': val}

    def onchange_product_ids(self, cr, uid, ids, product_ids, line_ids, context=None):
        products = self.resolve_2many_commands(cr, uid, 'product_ids', product_ids, context=context)
        lines = self.resolve_2many_commands(cr, uid, 'line_ids', line_ids, context=context)
        clean = lambda x: x[0] if isinstance(x, (list, tuple)) else x
        new_lines = []
        products = {clean(el['product_id']): el.get('qty', 0) for el in products}
        seen = set()
        for l in lines:
            product_id = clean(l['product_id'])
            if product_id in products:
                if product_id in seen or not products[product_id]:
                    continue
                seen.add(product_id)
                l['qty'] = products[product_id]
            new_lines.append(l)
        for p in set(p for p, v in products.items() if v) - seen:
            new_lines.append({
                'product_id': p,
                'name': self.pool.get('product.product').name_get(cr, uid, [p], context=context)[0][1],
                'qty': products[p]
            })
        return {'value': {'line_ids': new_lines}}

    def create_order(self, cr, uid, id, context=None):
        obj = self.browse(cr, uid, id, context=context)
        order_data = self._prepare_order(cr, uid, obj, context=context)
        sale = self.pool.get('sale.order')
        ctx = (context or {}).copy()
        if 'fiscalyear_id' not in ctx:
            ctx['fiscalyear_id'] = self.pool.get('account.fiscalyear').find(cr, uid, dt=obj.date, context=ctx)
            ctx['company_id'] = obj.company_id.id
        cid = sale.create(cr, uid, order_data, context=ctx)
        order = sale.browse(cr, uid, cid, context=context)
        lines = []
        for line in obj.line_ids:
            line_data = self._prepare_order_line(cr, uid, order, line, context=context)
            lines.append((0, 0, line_data))
        sale.write(cr, uid, order.id, {'order_line': lines}, context=context)
        self.write(cr, uid, id, {'sale_id': order.id}, context=context)
        return cid

    def action_confirm(self, cr, uid, ids, context=None):
        cids = []
        for id in self.search(cr, uid, [('id', 'in', ids), ('sale_id', '=', False)]):
            cids.append(self.create_order(cr, uid, id, context=context))
        act = {
            'type': 'ir.actions.act_window',
            'name': 'Nuova richiesta',
            'res_model': 'postsale.request',
            'view_mode': 'form',
        }
        return act

    def _prepare_order(self, cr, uid, request, context=None):
        addr = self.pool.get('res.partner').address_get(cr, uid, [request.partner_id.id], ['invoice'], context=context)
        otid = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'thema_post_sale', 'order_type_postsale')[
            -1]
        return {
            'order_type_id': otid,
            'partner_id': request.partner_id.id,
            'partner_shipping_id': request.partner_id.id,
            'partner_invoice_id': addr['invoice'],
            'order_policy': 'picking',
            'user_id': request.user_id.id,
            'payment_term': request.partner_id.property_payment_term and request.partner_id.property_payment_term.id or False,
            'invoice_type_id': request.invoice_type_id.id,
            'delivery_type_id': request.delivery_type_id.id,
            'fiscal_position': request.partner_id.property_account_position and request.partner_id.property_account_position.id or False,
            'pricelist_id': request.pricelist_id.id,
            'origin_id': request.origin_id.id,
            'date_order': request.date,
            'company_id': request.company_id.id,
            'note': request.note,
            'order_company_id': request.company_id.id,
        }


    def _prepare_order_line(self, cr, uid, sale, line, context=None):
        ctx = (context or {}).copy()
        ctx['force_company'] = sale.order_company_id.id
        sl = self.pool.get('sale.order.line')
        vals = {
            'line_type_id':
                self.pool.get('ir.model.data').get_object_reference(cr, uid, 'thema_post_sale', 'line_type_postsale')[
                    -1],
            'name': line.name,
            'product_id': line.product_id.id,
            'product_uom_qty': line.qty,
            'product_uom': line.product_id.uom_id.id,
        }
        vals = sl._prepare_line(cr, uid, sale, vals, context=context)
        return vals

    def register_requests(self, cr, uid, data, context=None):
        """
        Data: a list of dictionaries containing at least:
            delivery_type_id,
            partner_id,
            product_ids,
        """
        partner = self.pool.get('res.partner')
        product = self.pool.get('product.product')
        cids = []
        for d in data:
            for f in ['delivery_type_id', 'partner_id', 'product_ids']: assert f in d, '%s missing in %s' % (f, d)
            p = partner.browse(cr, uid, d['partner_id'], context=context)
            date = d.get('date', fields.date.context_today(self, cr, uid, context=context))
            d.update({
                'date': date,
                'company_id': p.company_id and p.company_id.id or False,
                'pricelist_id': p.property_product_pricelist and p.property_product_pricelist.id or False,
            })
            product_ids = d.pop('product_ids')
            names = dict(product.name_get(cr, uid, product_ids, context=context))
            d['line_ids'] = [(0, 0, {'product_id': p, 'name': d.get('name') and d['name'] or names[p], 'qty': 1}) for p
                             in product_ids]
            cids.append(self.create(cr, uid, d, context=context))
        self.action_confirm(cr, uid, cids, context=context)
        return cids


class request_line(orm.Model):
    _name = 'postsale.request.line'
    _columns = {
        'request_id': fields.many2one('postsale.request', 'Richiesta', required=True, ondelete='cascade'),
        'product_id': fields.many2one('product.product', 'Prodotto', required=True),
        'name': fields.char('Descrizione', required=True),
        'qty': fields.integer('Quantita`', required=True),
    }


class request_product(orm.TransientModel):
    _name = 'postsale.request.product'

    _columns = {
        'request_id': fields.many2one('postsale.request', 'Richiesta', readonly=True, ondelete='cascade'),
        'product_id': fields.many2one('product.product', 'Prodotto', readonly=True),
        'type': fields.char('Tipo', readonly=True),
        'qty': fields.integer('Quantita` richiesta'),
        'available': fields.integer('Quantita` disponibile', readonly=True),
        'total': fields.integer('Quantita` totale', readonly=True),
    }
