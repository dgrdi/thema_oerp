# coding=utf-8
from openerp.osv import orm, fields
from openerp.addons import decimal_precision as dp
from thema_product.availability import product_avail_field
from devutils.sql_func import sql_function


class requested_field(sql_function):
    def query_req(self, fields, ids):
        if ids is not None:
            pf1, pf2 = 'AND p.id IN %s', 'WHERE p.id IN %s'
            args = [tuple(ids), tuple(ids)]
        else:
            pf1, pf2 = '', ''
            args = []
        q = """
                WITH rq AS (
                    SELECT
                      p.id,
                      tools.round(COALESCE(sum(sol.product_uom_qty * c.factor), 0 :: NUMERIC),
                                  NULL, u.rounding) qty_requested_postsale
                    FROM product_product p
                      INNER JOIN product_template t ON p.product_tmpl_id = t.id
                      INNER JOIN sale_order_line sol ON sol.product_id = p.id
                      INNER JOIN sale_order o ON o.id = sol.order_id
                      INNER JOIN thema_delivery_type dt ON dt.id = o.delivery_type_id
                      INNER JOIN product_uom_conversion c ON c.uom_from_id = sol.product_uom AND c.uom_to_id = t.uom_id
                      INNER JOIN product_uom u ON t.uom_id = u.id
                    WHERE dt.postsale
                          AND o.state IN ('draft', 'process', 'blocked')
                          AND sol.state IN ('draft', 'confirmed')
                          {0}
                    GROUP BY p.id, t.uom_id, u.rounding
                )
                SELECT
                  p.id,
                  COALESCE(rq.qty_requested_postsale, 0 :: NUMERIC) qty_requested_postsale
                FROM product_product p
                  LEFT JOIN rq ON rq.id = p.id
                  {1}
        """.format(pf1, pf2)
        return q, args

    def query_read(self, fields, ids):
        return self.query_req(fields, ids)

    def query(self, fields):
        return self.query_req(fields, None)


class product(orm.Model):
    _inherit = 'product.product'

    _identity = lambda s, c, u, i, f, a, context=None: {id: dict.fromkeys(f, id) for id in i}

    _columns = {
        'qty_requested_postsale': requested_field('Richieste assistenza', type='float',
                                                  digits_compute=dp.get_precision('Product UoS')),
        'qty_available_postsale': product_avail_field('Giacenza assistenza', location_domain=[('postsale', '=', True)]),
    }


    def find_parts(self, cr, uid, ids, part_ids, context=None):
        if not ids:
            return {}
        parts = self.pool.get('product.part').browse(cr, uid, part_ids, context=context)
        cr.execute("SELECT DISTINCT model_id FROM product_product p "
                   "INNER JOIN product_template t ON t.id = p.product_tmpl_id "
                   "WHERE p.id IN %s AND model_id IS NOT NULL", [tuple(ids)])
        models = self.pool.get('product.model').browse(cr, uid, [el[0] for el in cr.fetchall()], context=context)
        models = {el.id: el for el in models}

        def get_val(record, field):
            val = record[field]
            if isinstance(val, orm.browse_record):
                val = val and val.id or False
            return val

        res = {}
        for prod in self.browse(cr, uid, ids, context=context):
            vals = res[prod.id] = {}
            for pt in parts:
                if not prod.model_id:
                    vals[pt.id] = False
                    continue
                for candidate in models[prod.model_id.id].part_ids:
                    if not candidate.part_id or candidate.part_id.id != pt.id:
                        continue
                    for field in pt.field_ids:
                        if get_val(candidate, field.name) != get_val(prod, field.name):
                            break
                    else:
                        vals[pt.id] = candidate.id
                        break
                else:
                    vals[pt.id] = False
        return res

