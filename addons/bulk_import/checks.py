from openerp.osv.orm import Model

def check_base(obj, method):
    if obj == Model or obj == object:
        return
    if method in obj.__dict__:
        raise NotImplementedError("bulk_{m}() will not call {m}() on the object!"
                                  "\nmethod defined in: {c}"
                                  "\npass 'ignore_methods' in context to ignore.".format(m=method, c=obj))
    for b in obj.__bases__:
        check_base(b, method)


def check_user(uid):
    if uid != 1:
        raise SystemError('Only administrator is allowed to run this.')
