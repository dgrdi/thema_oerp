from logging import getLogger
from openerp.osv.orm import browse_record
from openerp.osv import fields
from openerp.osv import fields as osv_fields
from psycopg2 import OperationalError
from psycopg2.errorcodes import SERIALIZATION_FAILURE, DEADLOCK_DETECTED
import random

logger = getLogger(__name__)

def write_fnct(cr, uid, obj, ids_by_vals_by_field, context):
    result = []
    logger.debug('Calling functional writes: %s calls', len([c for cs in ids_by_vals_by_field.values() for c in cs]))
    for field, ids_by_vals in ids_by_vals_by_field.items():
        if isinstance(obj._columns[field], fields.related):
            logger.debug('Trying to write to related field: %s, skipping...', field)
            continue
        logger.debug('Calling set for field %s (type %s), %s calls', field, obj._columns[field]._type, len(ids_by_vals))
        i = 0
        for val, ids in ids_by_vals.items():
            i += 1
            if isinstance(obj._columns[field], fields.property):
                logger.debug('Writing property field %s for %s records with value %s', field, len(ids), val)
                write_property(cr, uid, obj, ids, field, val, context)
            elif isinstance(obj._columns[field], fields.function):
                logger.debug('Setting value %s on %s records', val, len(ids))
                result += obj._columns[field].set(cr, obj, ids, field, val, uid, context) or []
            else:
                if i < 10:
                    logger.debug('Setting value %s on %s records one by one', val, len(ids))
                for id  in ids:
                    result += obj._columns[field].set(cr, obj, id, field, val, uid, context) or []
    return result


def write_property(cr, uid, obj, ids, field, val, context):
    """
    Writes property field `field` on ids, in one pass.
    """
    col = obj._columns[field]
    def_id = col._field_get(cr, uid, obj._name, field)
    company = obj.pool.get('res.company')
    cid = company._company_default_get(cr, uid, obj._name, def_id, context=context)
    context_company = dict(context, company_id=cid)
    nids = col._get_by_id(obj, cr, uid, [field], ids, context_company)
    if nids:
        cr.execute('DELETE FROM ir_property WHERE id IN %s', [tuple(nids)])
    default_val = col._get_default(obj, cr, uid, field, context)

    property_create = False
    if isinstance(default_val, browse_record):
        if default_val.id != val:
            property_create = True
    elif default_val != val:
        property_create = True

    if property_create:
        propdef = obj.pool.get('ir.model.fields').browse(cr, uid, def_id, context=context)
        prop= {
            'name': propdef.name,
            'value': val,
            'company_id': cid,
            'fields_id': def_id,
            'type': col._type
        }
        props = []
        for id in ids:
            p = prop.copy()
            p.update({
                'res_id': obj._name + ',' + str(id)
            })
            props.append(p)
        prop_obj = obj.pool.get('ir.property')
        props = map(lambda p: prop_obj._update_values(cr, uid, None, p), props)
        prop_obj.bulk_create(cr, uid, props, context=context)

def recompute_stored(cr, user, obj, result, context):
    context = context or {}
    if context.get('import_skip_store', None) == True:
        logger.debug('Skipping stored field recompute')
        return
    logger.debug('Recomputing stored function fields, %s calls', len(result))
    result.sort()
    done = {}
    context['store_ignore_magic'] = True
    for order, object, ids_to_update, fields_to_recompute in result:
        key = (object, tuple(fields_to_recompute))
        done.setdefault(key, {})
        # avoid to do several times the same computation
        todo = []
        for id in ids_to_update:
            if id not in done[key]:
                done[key][id] = True
                todo.append(id)
        logger.debug('Object: %s; fields: %s', object, fields_to_recompute)
        store_set_values(obj.pool.get(object), cr, user, todo, fields_to_recompute, context)
    context.pop('store_ignore_magic')

import time
import datetime
from openerp import SUPERUSER_ID


def store_set_values(self, cr, uid, ids, fields, context):
    """Calls the fields.function's "implementation function" for all ``fields``, on records with ``ids`` (taking care of
       respecting ``multi`` attributes), and stores the resulting values in the database directly."""
    if not ids:
        return True
    logger.debug('Store recompute: performing magic...')
    ## BEGIN MAGIC
    field_flag = False
    field_dict = {}
    if self._log_access and not context.get('store_ignore_magic', None):
        cr.execute('select id,write_date from '+self._table+' where id IN %s', (tuple(ids),))
        res = cr.fetchall()
        for r in res:
            if r[1]:
                field_dict.setdefault(r[0], [])
                res_date = time.strptime((r[1])[:19], '%Y-%m-%d %H:%M:%S')
                write_date = datetime.datetime.fromtimestamp(time.mktime(res_date))
                for i in self.pool._store_function.get(self._name, []):
                    if i[5]:
                        up_write_date = write_date + datetime.timedelta(hours=i[5])
                        if datetime.datetime.now() < up_write_date:
                            if i[1] in fields:
                                field_dict[r[0]].append(i[1])
                                if not field_flag:
                                    field_flag = True
        del res
    logger.debug('Store recompute: magic complete; calculating values...')
    todo = {}
    keys = []
    for f in fields:
        if self._columns[f]._multi not in keys:
            keys.append(self._columns[f]._multi)
        todo.setdefault(self._columns[f]._multi, [])
        todo[self._columns[f]._multi].append(f)
    # keys = a list of field 'multi' keys, and False (for fields with no 'multi')
    result = {}
    m2ofields = set(c for c, v in self._columns.items() if v._type == 'many2one')
    def fix_m2o(v):
        for f in [f2 for f2 in m2ofields if f2 in v]:
            if not isinstance(v[f], (int, long)) and v[f]:
                v[f] = v[f][0]
        return v

    for key in keys:
        val = todo[key]
        if key:
            logger.debug('Calculating values for fields: %s', val)
            # use admin user for accessing objects having rules defined on store fields
            r = self._columns[val[0]].get(cr, self, ids, val, SUPERUSER_ID, context=context)
            # r is a map from ids to maps from field names to values
            for id, values in r.iteritems():
                result.setdefault(id, {}).update(fix_m2o(values))
        else:
            for f in val:
                logger.debug('Calculating values for field: %s', f)
                if isinstance(self._columns[f], osv_fields.related):
                    r = read_related(self, cr, self._columns[f], ids)
                else:
                    r = self._columns[f].get(cr, self, ids, f, SUPERUSER_ID, context=context)
                # r is a map from ids to values
                for id, val in r.iteritems():
                    if f in m2ofields and not isinstance(val, (int, long)) and val:
                        val = val[0]
                    result.setdefault(id, {})[f] = val
    # remove fields marked by MAGIC
    logger.debug('Values calculated.')
    special = {}
    special_ids = set()

    if field_flag:
        for id, values in result.iteritems():
            for f in field_dict[id]:
                values.pop(f, None)
            special.setdefault(tuple(sorted(field_dict[id])), []).append(id)
            special_ids.add(id)

    def split_by_col():
        ids = set(result.iterkeys()) - special_ids
        if ids:
            cols = result[iter(ids).next()].keys()
            yield cols, ids
        for ids in special.itervalues():
            cols = result[iter(ids).next()].keys()
            yield cols, ids

    logger.debug('Writing values: %s queries', len(special) + 1)
    for cols, ids in split_by_col():
        logger.debug('Building query...')
        fdef = ', '.join(['"%s"' % c for c in cols])
        val_placeh = '(%s)' % ', '.join([self._columns[c]._symbol_set[0] for c in cols] + ['%s'])
        get_values = lambda v: [self._columns[c]._symbol_set[1](v.get(c, False)) for c in cols]

        values = []
        for id in ids:
            values.append(get_values(result[id]) + [id])

        table = self._table
        upst = ', '.join(['"%s"="p_updata"."%s"' % (c, c) for c in cols])

        def chunkify(lst):
            ln = len(lst)
            for i in range(0, ln, 200000):
                upper = min(i+200000, ln)
                yield upper-i, (lst[j] for j in range(i, upper))


        cr.execute("""
            CREATE TEMP TABLE p_updata AS
            SELECT {fdef}, 1 t_imp_id
            FROM {table}
            LIMIT 0;
        """.format(**locals()))

        for ln, chunk in chunkify(values):

            placeholders = ',\n'.join([val_placeh for _ in range(ln)])
            cr.execute("""
                INSERT INTO p_updata
                VALUES
                {placeholders};
            """.format(**locals()), [v for vs in chunk for v in vs])
        logger.debug('Executing...')
        locked = False
        i = 0
        while not locked:
            i += 1
            cr.execute("SELECT pg_try_advisory_lock(111);")
            locked = cr.fetchall()[0][0]
            if not locked:
                if i > 10:
                    cr.rollback()
                    time.sleep(random.random() * 10)
                    op = OperationalError()
                    op.pgcode = DEADLOCK_DETECTED
                    raise op
                else:
                    time.sleep(1)


        cr.execute("""
            UPDATE {table}
            SET {upst}
            FROM p_updata WHERE {table}.id = p_updata.t_imp_id;
            DO LANGUAGE plpgsql $$ BEGIN PERFORM pg_advisory_unlock(111); END $$;
            DROP TABLE p_updata;
        """.format(**locals()))
    logger.debug('Done!')
    return True

def read_related(obj, cr, field, ids):
    res = {}
    rev = {i: [i] for i in ids}
    rids = ids
    for i, f in enumerate(field.arg):
        res = {}
        last = i == len(field.arg) - 1
        vals = obj.read(cr, SUPERUSER_ID, rids, fields=[f])
        for v in vals:
            val = v[f]
            if not last and not isinstance(val, (int, long)):
                val = val[0]
            for id in rev[v['id']]:
                res[id] = val
        if not last:
            rev = {}
            obj = obj.pool.get(obj._columns[f]._obj)
            for k, v in res.iteritems():
                rev.setdefault(v, []).append(k)
        rids = {k for k in rev.iterkeys() if k}
    return res