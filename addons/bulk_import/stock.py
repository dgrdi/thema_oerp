from openerp.osv import orm, fields, osv
from openerp.netsvc import LocalService

class stock_picking(orm.Model):

    _inherit = 'stock.picking'

    def update_workflow(self, cr, uid, context=None):
        wf = LocalService('workflow')
        cr.execute("""
            SELECT p.id
            FROM stock_picking p
            INNER JOIN wkf_instance i on i.res_type = 'stock.picking' and i.res_id = p.id
              INNER JOIN wkf_workitem wi on wi.inst_id = i.id
              INNER JOIN wkf_activity a on a.id = wi.act_id
            where a.name <> 'confirmed' and p.state = 'confirmed'
        """)
        pids = [el[0] for el in cr.fetchall()]
        for pid in pids:
            wf.trg_validate(uid, 'stock.picking', pid, 'button_confirm', cr)
        return len(pids)