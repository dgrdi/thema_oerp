"""
Faster default value evaluation for models

Modifies Model.default_get so that when context['import'] is true, we:
 - do not check for user default values
 - load property fields defaults from a cache

to minimize DB activity.
"""

from openerp.osv.orm import Model, browse_record, browse_null
from openerp.osv import orm, fields
import logging
logger = logging.getLogger(__name__)

get_df = Model.default_get


property_cache = {}
def get_property(pool, cr, uid, name, model, context=None):
    if (name, model) not in property_cache.keys():
        property_obj = pool.get('ir.property')
        property_cache[(name, model)] = property_obj.get(cr, uid, name, model, context=context)
    return property_cache[(name, model)]

def default_get(self, cr, uid, fields_list, context=None):
    context = context or {}
    if not context.get('import'):
        return get_df(self, cr, uid, fields_list, context)
    else:
        cache = context.get('import_default_cache')
        if cache:
            return {k: v for k, v in cache.items() if k in fields_list}

        # trigger view init hook
        self.view_init(cr, uid, fields_list, context)

        if not context:
            context = {}
        defaults = {}

        # get the default values for the inherited fields
        for t in self._inherits.keys():
            defaults.update(self.pool.get(t).default_get(cr, uid, fields_list,
                context))

        # get the default values defined in the object
        for f in fields_list:
            if f in self._defaults:
                if callable(self._defaults[f]):
                    logger.debug('Calculating defaults for field %s', f)
                    defaults[f] = self._defaults[f](self, cr, uid, context)
                else:
                    defaults[f] = self._defaults[f]

            fld_def = ((f in self._columns) and self._columns[f]) \
                    or ((f in self._inherit_fields) and self._inherit_fields[f][2]) \
                    or False

            if isinstance(fld_def, fields.property):
                logger.debug('Calculating property default for field %s', f)
                prop_value = get_property(self.pool, cr, uid, f, self._name, context=context)
                if prop_value:
                    if isinstance(prop_value, (browse_record, browse_null)):
                        defaults[f] = prop_value.id
                    else:
                        defaults[f] = prop_value
                else:
                    if f not in defaults:
                        defaults[f] = False

        # get the default values from the context
        for key in context or {}:
            if key.startswith('default_') and (key[8:] in fields_list):
                defaults[key[8:]] = context[key]
        return defaults


Model.default_get = default_get