from operator import itemgetter
from openerp.osv.orm import Model, browse_record, browse_null
from logging import getLogger
from checks import check_user
logger = getLogger(__name__)

def bulk_import(self, cr, uid, fields, data, context=None):
    """
    Parameters fields and data are *similar* to Model.load(), but only the 'id'
    field and names like '<field>/id' get translated (by looking them up by XML id),
    everything else is fed directly to write() or create().

    There is NO access security control. Will throw an exception if uid is not 1.
    """
    cr.execute("SET TRANSACTION ISOLATION LEVEL READ COMMITTED;")
    check_user(uid)
    if context is None:
        context = {}
    mdata = self.pool.get('ir.model.data')
    mfields = self.pool.get('ir.model.fields')

    rel_fields = [f for f in fields if f[-3:] == '/id' or f == 'id']
    model_data = {}
    data = [dict(zip(fields, e)) for e in data]

    for f in rel_fields:
        logger.debug('Fetching XML ids for field %s', f)
        if f == 'id':
            rel = self._name
        else:
            fn = f[:-3]
            try:
                fn = mfields.search(cr, uid, [('model', '=', self._name), ('name', '=', fn)], context=context)[0]
            except IndexError:
                raise KeyError('field not found on model {0}: {1}'.format(self._name, fn))
            rel = mfields.read(cr, uid, fn, fields=('relation', ), context=context)['relation']
            if rel.startswith('stock.picking'):
                rel = 'stock.picking'
        ids = map(itemgetter(f), data)
        ids = filter(lambda x: x, ids)
        ids = [i for id in ids for i in id.split(',')]
        ids = map(lambda x: x.split('.')[1] if '.' in x else x, ids)
        ids = mdata.search(cr, uid, [('model', '=', rel), ('name', 'in', ids)], context=context)
        ids = mdata.read(cr, uid, ids, fields=('res_id', 'name'), context=context)
        model_data[f] = {i['name']: i['res_id'] for i in ids}

    to_write = []
    to_create = []
    model_data_to_create = []

    logger.debug('Sweeping data for creation or update')
    for d in data:
        id = d.pop('id', None)
        for f in d.keys()[:]:
            if f in rel_fields:
                typ = self._columns[f[:-3]]._type
                origv = v =  d.pop(f)
                if v is not None:
                    v = v.split(',')
                    v = map(lambda x: x.split('.')[1] if '.' in x else x, v)
                    try:
                        v = map(lambda x: model_data[f][x], v)
                    except KeyError:
                        raise KeyError('One of the XML IDS in {0} was not found, '
                                       'for field {1} (original value: {2})'.format(v, f, origv))
                    if typ == 'many2many':
                        v = ((6, 0, tuple(v)),)
                    else:
                        assert len(v) == 1, 'Field not a many2many: %s, value: %s' % (typ, v)
                        v = v[0]

                d[f[:-3]] = v
        dbid = None
        if id:
            if '.' in id:
                id = id.split('.')[1]
            dbid = model_data['id'].get(id)
        if not dbid:
            dbid = d.pop('.id', None)
        if dbid:
            to_write.append((dbid, d))
        else:
            to_create.append(d)
            if id:
                model_data_to_create.append(id)

    del data
    def chunkify(lst):
        for i in range(0, len(lst), 500000):
            yield lst[i:i+500000]



    if to_write and not context.get('nowrite'):
        for slc in chunkify(to_write):
            logger.debug('Updating %s records', len(slc))
            self.bulk_write(cr, uid, slc, context=context)
    del to_write
    if to_create and not context.get('nocreate'):
        create_ids = []
        for slc in chunkify(to_create):
            logger.debug('Creating %s records', len(slc))
            create_ids += self.bulk_create(cr, uid, slc, context=context)
        del to_create
        if model_data_to_create:
            for slc in chunkify(zip(create_ids, model_data_to_create)):
                logger.debug('Saving XML ids for %s records', len(slc))
                d = [{'res_id': id, 'name': xid, 'model': self._name} for id, xid in slc]
                mdata.bulk_create(cr, uid, d, context=context)




Model.bulk_import = bulk_import
