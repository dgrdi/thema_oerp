from openerp.osv import orm, fields
from logging import getLogger
from osv.orm import except_orm
from osv.osv import except_osv

logger = getLogger(__name__)

class move_line(orm.Model):
    _inherit = 'account.move.line'

    def reconcile_multiple(self, cr, uid, groups, context=None):
        md = self.pool.get('ir.model.data')
        logger.debug('Reading move line ids')
        cr.execute('''
            SELECT md.name, ml.id, ml.reconcile_id
            FROM ir_model_data md
            INNER JOIN account_move_line ml ON md.model = 'account.move.line' AND md.res_id = ml.id
        ''')
        moves = {}
        for name, line_id, reconcile_id in cr.fetchall():
            moves[name] = (line_id, reconcile_id)
        moves_inv = {v[0]: k for k, v in moves.iteritems()}
        cnt = len(groups)
        step = int(cnt / 100)
        for i, xml_ids in enumerate(groups):
            if i % step == 0:
                logger.debug('Reconciling: %s%%', float(i) / cnt * 100)
            try:
                lines = [moves[n] for n in xml_ids]
            except KeyError:
                continue
            if any([l[1] for l in lines]):
                continue
            try:
                self.reconcile_partial(cr, uid, [l[0] for l in lines], context=context)
            except (except_osv, except_orm) as e:
                logger.debug('**** %s', xml_ids)
                logger.debug('ID\tXML ID\tPARTNER_ID\tPARTNER\tDEBIT\tCREDIT')
                partners = []
                for l in self.browse(cr, uid, [l[0] for l in lines]):
                    logger.debug('%s\t%s\t%s\t%s\t%s\t%s', l.id, moves_inv[l.id], l.partner_id.id, l.partner_id.name, l.debit, l.credit)
                    partners.append(l.partner_id.id)
                logger.debug('partners: %s', set(partners))
                raise
        return True


class reconcile(orm.Model):
    _inherit = 'account.move.reconcile'

    def _validate(self, cr, uid, ids, context=None):
        if context and context.get('novalidate', None):
            return
        return super(reconcile, self)._validate(cr, uid, ids, context=context)