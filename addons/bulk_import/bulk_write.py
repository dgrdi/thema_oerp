from fnct_write import write_fnct, recompute_stored
from checks import check_user, check_base
from logging import getLogger

logger = getLogger(__name__)
from openerp.osv.orm import Model

def bulk_write(self, cr, user, ids_by_values, context=None):
    """
    :param: ids_by_values: a list of elements in the form (ids, values). For each element,
            will write values in ids.

    This will not call Model's or any of its descendants' write() method. An exception is thrown
    if write() has been overridden. Pass 'ignore_methods' in context to go through.
    There is NO access security control. Will throw an exception if uid is not 1.
    """
    check_user(user)
    if not context:
        context = {}

    if 'ignore_methods' not in context:
        check_base(type(self), 'write')

    keys = ids_by_values[0][1].keys()
    for _, v in ids_by_values:
        assert v.keys() == keys

    ids_by_values = map(lambda (i, v): ((i, ), v) if isinstance(i, (int, long)) else (i, v), ids_by_values)
    all_ids = [i for ids in zip(*ids_by_values)[0] for i in ids]
    ids, vals = ids_by_values[0]
    vals = vals.copy()

    result = self._store_get_values(cr, user, all_ids, vals.keys(), context) or []
    vals.pop('parent_left', None)
    vals.pop('parent_right', None)

    fdef = []
    fval = []
    upd_todo = []
    updend = []
    for field in sorted(vals.keys()):
        if field in self._columns:
            if self._columns[field]._classic_write and not (hasattr(self._columns[field], '_fnct_inv')):
                fdef.append(field)
                fval.append(self._columns[field]._symbol_set[0])
            else:
                upd_todo.append(field)
        else:
            updend.append(field)

    if self._log_access:
        fdef.extend(('write_uid', 'write_date'))
        fval.extend(('%s', "(now() at time zone 'UTC')"))

    upd_todo.sort(lambda x, y: self._columns[x].priority-self._columns[y].priority)

    query_vals = []
    inh_writes = []

    ids_by_vals_by_field = {}

    for ids, vals in ids_by_values:

        # No direct update of parent_left/right
        vals.pop('parent_left', None)
        vals.pop('parent_right', None)

        upd1 = []
        for field in sorted(vals.keys()):
            if field in self._columns:
                if self._columns[field]._classic_write and not (hasattr(self._columns[field], '_fnct_inv')):
                    upd1.append(self._columns[field]._symbol_set[1](vals[field]))
            if field in self._columns \
                    and hasattr(self._columns[field], 'selection') \
                    and vals[field]:
                self._check_selection_field_value(cr, user, field, vals[field], context=context)

        if self._log_access:
            upd1.append(user)

        if len(fdef):
            query_vals.append(upd1 + [list(ids)])


        for field in upd_todo:
            v = vals[field]
            if isinstance(v, list):
                v = tuple(v)
            ids_by_vals_by_field.setdefault(field, {}).setdefault(vals[field], []).extend(ids)
            #ids_by_vals_by_field.setdefault(field, vals[field]).extend(ids)
            #ids_by_vals_by_field.setdefault(field, {})

        for table in self._inherits:
            col = self._inherits[table]
            v = {}
            for val in updend:
                if self._inherit_fields[val][0] == table:
                    v[val] = vals[val]
            if v:
                inh_writes.append((table, ids, v))

    tbl = self._table
    fval.append('%s')

    fval = ', '.join(fval)


    upst = []
    for f in fdef:
        upst.append('"{f}"=p_updata."{f}"'.format(tbl=tbl,f=f))
    upst = ', '.join(upst)
    fdef = ','.join('"{0}"'.format(f) for f in fdef)
    logger.debug('Updating classic write: %s values', len(query_vals))
    cr.execute("""
        CREATE TEMP TABLE p_updata AS
        select {fdef}, ARRAY[1] t_imp_ids
        FROM {tbl}
        LIMIT 0;
    """.format(**locals()))

    def chunkify(lst):
        ln = len(lst)
        for i in range(0, ln, 200000):
            upper = min(i+200000, ln)
            yield upper-i, (lst[j] for j in range(i, upper))

    for ln, chunk in chunkify(query_vals):
        placeholders = ', '.join('({0})'.format(fval) for _ in range(ln))
        cr.execute("""
            INSERT INTO p_updata VALUES {placeholders};
        """.format(**locals()), tuple(v for vs in chunk for v in vs))

    del query_vals

    cr.execute("""
        UPDATE {tbl} SET {upst}
        FROM p_updata WHERE {tbl}.id = ANY(p_updata.t_imp_ids);

        DROP TABLE p_updata;
    """.format(**locals()))

    result += write_fnct(cr, user, self, ids_by_vals_by_field, context)
    del ids_by_vals_by_field

    for table in self._inherits:
        writes = filter(lambda x: x[0] == table, inh_writes)
        col = self._inherits[table]
        tbl = self._table
        data = [(i, id) for i, w in enumerate(writes) for id in w[1]]
        placeholders = ', '.join('%s' for _ in data)
        if data:
            cr.execute("""
                CREATE TEMPORARY TABLE inh (
                    i int,
                    id int
                );
                INSERT INTO inh VALUES {placeholders};
                SELECT DISTINCT inh.i, {tbl}.{col}
                FROM inh
                INNER JOIN {tbl} ON inh.id = {tbl}.id
                ORDER BY inh.i;
            """.format(**locals()), data)
            del data
            inh_ids = []
            ci = None
            to_write = []
            for i, inh_id in cr.fetchall():
                if i != ci:
                    if ci != None:
                        to_write.append((inh_ids, writes[i][2]))
                    ci = i
                    inh = []
                inh_ids.append(inh_id)
            to_write.append((inh_ids, writes[i][2]))
            cr.execute('DROP TABLE inh;')
            logger.debug('Updating parent model instances: %s records', len(to_write))
            self.pool.get(table).bulk_write(cr, user, to_write, context=context)
            del to_write

    if self._parent_store and not context.get('no_store_compute', None):
        self._parent_store_compute(cr)

    if not context.get('novalidate'):
        logger.debug('Validating...')
        self._validate(cr, user, all_ids, context=context)

    result += self._store_get_values(cr, user, all_ids, vals.keys(), context)


    recompute_stored(cr, user, self, result, context)

    self._workflow_trigger(cr, user, all_ids, 'trg_write', context=context)
    return True

Model.bulk_write = bulk_write