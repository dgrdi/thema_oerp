import functools

from checks import check_user, check_base
from fnct_write import write_fnct, recompute_stored
from logging import getLogger
from openerp.osv.orm import Model
from openerp.osv import fields

logger = getLogger(__name__)

def bulk_create(self, cr, user, values, context=None):
    """
    :param: values: a list of dicts, each as for create().
    :return: a list of ids of the objects created. The order will match the order of values.

    This will not call Model's or any of its descendants' create() method. An exception is thrown
    if create() has been overridden. Pass 'ignore_methods' in context to go through.
    There is NO access security control. Will throw an exception if uid is not 1.
    """
    check_user(user)
    if not context:
        context = {}

    if 'ignore_methods' not in context:
        check_base(type(self), 'create')

    logger.debug('Calculating default values...')
    defaults = self.default_get(cr, user, self._all_columns.keys(), context=context)
    context['import_default_cache'] = defaults
    values = map(functools.partial(self._add_missing_default_values, cr, user, context=context), values)
    context.pop('import_default_cache')
    logger.debug('Default values calculated')
    keys = values[0].keys()

    for v in values:
        assert v.keys() == keys

    inh_creates = {}
    inh_writes = {}
    query_vals = []

    upd0, upd1 = '', ''
    upd_todo = []
    unknown_fields = []

    vals = values[0].copy()

    for table in self._inherits:
        inh_creates[table] = []
        inh_writes[table] = []
        upd0 += ',' + self._inherits[table]
        upd1 += ',%s'
        if self._inherits[table] in vals:
            del vals[self._inherits[table]]

    bool_fields = [x for x in self._columns.keys() if self._columns[x]._type=='boolean']
    for bool_field in bool_fields:
        if bool_field not in vals:
            vals[bool_field] = False

    for v in vals.keys():
        if v in self._inherit_fields and v not in self._columns:
            del vals[v]
        else:
            if (v not in self._inherit_fields) and (v not in self._columns):
                del vals[v]
    for field in sorted(vals.keys()):
        if self._columns[field]._classic_write:
            upd0 = upd0 + ',"' + field + '"'
            upd1 = upd1 + ',' + self._columns[field]._symbol_set[0]
            if (hasattr(self._columns[field], '_fnct_inv')) and not isinstance(self._columns[field], fields.related):
                upd_todo.append(field)
        else:
            if not isinstance(self._columns[field], fields.related):
                upd_todo.append(field)
    if self._log_access:
        upd0 += ',create_uid,create_date,write_uid,write_date'
        upd1 += ",%s,(now() at time zone 'UTC'),%s,(now() at time zone 'UTC')"

    upd_todo.sort(lambda x, y: self._columns[x].priority-self._columns[y].priority)

    for vals in values:
        tocreate = {}
        for v in self._inherits:
            if self._inherits[v] not in vals:
                tocreate[v] = {}
            else:
                tocreate[v] = {'id': vals[self._inherits[v]]}
        upd2 = []

        for v in vals.keys():
            if v in self._inherit_fields and v not in self._columns:
                (table, col, col_detail, original_parent) = self._inherit_fields[v]
                tocreate[table][v] = vals[v]
                del vals[v]
            else:
                if (v not in self._inherit_fields) and (v not in self._columns):
                    del vals[v]

        for table in tocreate:
            if self._inherits[table] in vals:
                del vals[self._inherits[table]]

            record_id = tocreate[table].pop('id', None)

            if record_id is None or not record_id:
                inh_creates[table].append(tocreate[table])
            else:
                inh_writes[table].append((record_id, tocreate[table]))

        #Start : Set bool fields to be False if they are not touched(to make search more powerful)

        for bool_field in bool_fields:
            if bool_field not in vals:
                vals[bool_field] = False
        #End

        for field in sorted(vals.keys()):
            if self._columns[field]._classic_write:
                upd2.append(self._columns[field]._symbol_set[1](vals[field]))
            if field in self._columns \
                    and hasattr(self._columns[field], 'selection') \
                    and vals[field]:
                self._check_selection_field_value(cr, user, field, vals[field], context=context)
        if self._log_access:
            upd2.extend((user, user))
        query_vals.append(upd2)

    for table in reversed(self._inherits.keys()):
        if inh_creates[table]:
            logger.debug('Creating parents: %s records', len(inh_creates[table]))
            create_ids = self.pool.get(table).bulk_create(cr, user, inh_creates[table], context=context)
            for v, id in zip(query_vals, create_ids):
                v.insert(0, id)
        if inh_writes[table]:
            logger.debug('Updating parents: %s records', len(inh_writes[table]))
            self.pool.get(table).bulk_write(cr, user, dict(inh_writes[table]).items(), context=context)
            for v, (id, vl) in zip(query_vals, inh_writes[table]):
                v.insert(0, id)

    logger.debug('Creating classic write: %s values', len(query_vals))
    cr.execute("""
        SELECT nextval(%s) FROM generate_series(1,%s);
    """, (self._sequence, len(query_vals)))
    create_ids = list(zip(*cr.fetchall())[0])
    for id, v in zip(create_ids, query_vals):
        v.insert(0, id)

    tbl = self._table

    def chunkify(lst):
        ln = len(lst)
        for i in range(0, ln, 200000):
            upper = min(i+200000, ln)
            yield upper-i, (lst[j] for j in range(i, upper))

    for ln, chunk in chunkify(query_vals):
        placeholders = ',\n'.join(['(%s{0})'.format(upd1) for _ in range(ln)])
        cr.execute("""
            INSERT INTO {tbl} (id{upd0}) VALUES {placeholders}
        """.format(**locals()), tuple(v for vs in chunk for v in vs))

    del query_vals


    result = []
    ids_by_vals_by_field = {}
    for i, val in zip(create_ids, values):
        for field in upd_todo:
            v = vals[field]
            if isinstance(v, list):
                v = tuple(v)
            ids_by_vals_by_field.setdefault(field, {}).setdefault(v, []).append(i)

    result += write_fnct(cr, user, self, ids_by_vals_by_field, context)
    del ids_by_vals_by_field


    if self._parent_store and not context.get('no_store_compute', None):
        logger.debug('Recomputing parent store...')
        self._parent_store_compute(cr)

    if not context.get('novalidate'):
        logger.debug('Validating...')
        self._validate(cr, user, create_ids, context)

    logger.debug('Getting values to recompute...')
    result += self._store_get_values(cr, user, create_ids,
                                    list(set(vals.keys() + self._inherits.values())),
                                    context)

    recompute_stored(cr, user, self, result, context)

    logger.debug('triggering workflow...')
    self._workflow_trigger(cr, user, create_ids, 'trg_create', context=context)
    return create_ids

Model.bulk_create = bulk_create