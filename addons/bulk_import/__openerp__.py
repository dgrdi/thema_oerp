{
    'name': 'Thema - bulk imoprt',
    'version': '0.2',
    'category': 'Administration',
    'description': """Faster, unsecure import for large amounts of data""",
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': ['base', 'stock'],
    'init_xml': [],
    'data':[
        ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': True,
}
