from openerp.osv import orm, fields

class statement(orm.Model):
    _name = 'thema.agent.statement'

    _columns = {
        'agent_id': fields.many2one('thema.agent', 'Agente'),
        'state': fields.selection([('draft', 'Bozza'), ('confirmed', 'Confermato')],
                                  string='Stato'),
        'date_epoch': fields.date('Data iniziale'),
        'date_end': fields.date('Data finale'),
    }
