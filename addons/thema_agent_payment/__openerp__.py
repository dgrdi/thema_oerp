{
    'name': 'Thema - pagamenti agenti',
    'version': '0.1',
    'category': 'Administration',
    'description': """Pagamenti provvigionio agenti""",
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': ['base', 'account', 'thema_commission'],
    'init_xml': [],
    'data': [
    ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': True,
}
