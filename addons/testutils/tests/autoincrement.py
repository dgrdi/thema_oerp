import unittest2
from testutils.factories.autoincrement import AutoIncrement
import mock

class TestAutoIncrement(unittest2.TestCase):
    def test_seq(self):
        mock_fac = mock.Mock()
        mock_fac.model = 'model'
        a1 = AutoIncrement()
        a1.register(mock_fac, 'field')
        a2 = AutoIncrement()
        a2.register(mock_fac, 'field')
        v1, v2 = a1.get_seq(), a2.get_seq()
        self.assertEqual((v1, v2), (1, 2))
