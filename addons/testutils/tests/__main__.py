import unittest2
import factory
import autoincrement

tests = [
    factory.TestFactory,
    autoincrement.TestAutoIncrement,
]

def get_suite():
    suite = unittest2.TestSuite()
    loader = unittest2.defaultTestLoader
    for t in tests:
        suite.addTest(loader.loadTestsFromTestCase(t))

    return suite

def run():
    runner = unittest2.TextTestRunner(verbosity=2)
    runner.run(get_suite())

if __name__ == '__main__':
    run()

