from unittest2 import TestCase
from testutils.factories import Factory

class TestFactory(TestCase):

    def test_defaults_inheritance(self):
        class Base(Factory):
            defaults = {
                'a': 'test',
                'b': 'test'
            }

        class Dev(Base):
            defaults = {
                'c': 'test',
                'b': 'test2'
            }

        self.assertDictEqual(Dev.defaults, {
            'a': 'test',
            'b': 'test2',
            'c': 'test',
        })



