from openerp.tools.config import config
import os
from openerp.modules import module
from datetime import datetime
import dateutil
from openerp.pooler import get_pool, get_db_and_pool
from openerp import SUPERUSER_ID
from openerp.osv.orm import MetaModel
import inspect

def setup_basic(config_file, database):
    config.parse_config(['-c', config_file, '-d', database])

def setup(database):
    cn, pool =  get_db_and_pool(database)
    cr = cn.cursor()
    if db_needs_setup(cr):
        setup_database(cr)
        cr.commit()
    return cn, pool

def setup_database(cr):
    cr.execute("""
        DO language plpgsql $$
        BEGIN
            IF NOT EXISTS (
                SELECT 1 FROM pg_namespace n WHERE n.nspname = 'oerptest'
            ) THEN
            CREATE SCHEMA oerptest;
            END IF;
        END
        $$;

        DROP TABLE IF EXISTS oerptest.moddates;
        CREATE TABLE oerptest.moddates (
          "module" varchar PRIMARY KEY,
          "modified" timestamp,
          "data_modified" timestamp
        );

        DROP FUNCTION IF EXISTS oerptest.update_times(varchar, timestamp, timestamp );
        CREATE FUNCTION oerptest.update_times(pmodule varchar, pmodified timestamp, pdata_modified timestamp)
          RETURNS VOID AS $b$
        BEGIN
          UPDATE oerptest.moddates SET
            modified = pmodified,
            data_modified = pdata_modified
          WHERE module = pmodule;
          INSERT INTO oerptest.moddates (module, modified, data_modified)
            SELECT pmodule, pmodified, pdata_modified
            WHERE NOT EXISTS (SELECT 1 FROM oerptest.moddates d WHERE d.module = pmodule);
        END
        $b$ LANGUAGE plpgsql;
    """)

def db_needs_setup(cr):
    cr.execute("SELECT 1 FROM pg_namespace WHERE nspname = 'oerptest'")
    return not bool(cr.fetchall())

class Module(object):

    def __init__(self, cr, name):
        self.name = name
        self._load_last_modified()
        self._load_previous_ts(cr)

    def _load_last_modified(self):
        info = module.load_information_from_description_file(self.name)
        data_files = set(info.get('data', []))
        mpath = module.get_module_path(self.name)
        data_files = [os.path.join(mpath, f) for f in data_files]
        files = map(inspect.getfile, self.classes)
        self.last_modified = datetime.fromtimestamp(max(map(os.path.getmtime, files))) if files else None
        self.data_last_modified = datetime.fromtimestamp(max(map(os.path.getmtime, data_files))) if data_files else None

    def _load_previous_ts(self, cr):
        cr.execute("SELECT modified, data_modified FROM oerptest.moddates WHERE module = %s", [self.name])
        r = cr.fetchone()
        parse = lambda v: v and dateutil.parser.parse(v)
        self.previous_modified, self.data_previous_modified = map(parse, r) if r else (None, None)

    @property
    def classes(self):
        return MetaModel.module_to_models.get(self.name, [])

    @property
    def modified(self):
        return self.previous_modified is None or self.previous_modified < self.last_modified

    @property
    def data_modified(self):
        return self.data_previous_modified is None or self.data_previous_modified < self.data_last_modified

    def update_db_schema(self, cr, pool):
        def obj_from_class(cl):
            if hasattr(cl, '_name') and cl._name:
                return pool.get(cl._name)
            if hasattr(cl, '_inherit') and cl._inherit:
                return pool.get(cl._inherit)
            assert False, cl
        if self.classes:
            objects = {obj_from_class(cl) for cl in self.classes}
            module.init_module_models(cr, self.name, objects)

    def save_modified(self, cr):
        cr.execute("SELECT oerptest.update_times(%s, %s, %s)",
                   [self.name, self.last_modified, self.data_last_modified])

def reload_modules(cr, pool, modules):
    to_upgrade = {m for m in modules if m.data_modified}
    others = {m for m in modules if m.modified and m not in to_upgrade}

    if to_upgrade:
        cr.commit()
        ir_module = pool.get('ir.module.module')
        ids = ir_module.search(cr, SUPERUSER_ID, [('name', 'in', [m.name for m in to_upgrade])])
        ir_module.button_immediate_upgrade(cr, SUPERUSER_ID, ids)
    for m in others:
        m.update_db_schema(cr, pool)

    for m in to_upgrade.union(others):
        m.save_modified(cr)
