from functools import wraps
from openerp.tests.common import SingleTransactionCase

def rollback(func):
    @wraps(func)
    def wrap(self, *args, **kwargs):
        res = func(*args, **kwargs)
        self.cr.rollback()
        return res
    return wrap

def rollback_on_exc(func):
    @wraps(func)
    def wrap(self, *args, **kwargs):
        try:
            return func(*args, **kwargs)
        except:
            self.cr.rollback()
            raise
    return wrap

def _rollbackmeta(name, bases, attrs):
    for k, v in attrs.items():
        if hasattr(v, '__call__'):
            attrs[k] = rollback_on_exc(v)
    return type(name, bases, attrs)


class RollbackCase(SingleTransactionCase):
    """
    A test case that rolls back on error, so that the following test
    do not all come back with a useless "transaction aborted" error.
    """
    __metaclass__ = _rollbackmeta

class SavepointCase(SingleTransactionCase):
    """
    A test case that uses savepoints instead of BEGIN..ROLLBACK.
    """

    @classmethod
    def setUpClass(cls):
        super(SavepointCase, cls).setUpClass()
        cls.cr.execute('SAVEPOINT otest;')

    def tearDown(self):
        super(SavepointCase, self).tearDown()
        self.cr.execute('ROLLBACK TO SAVEPOINT otest;')
