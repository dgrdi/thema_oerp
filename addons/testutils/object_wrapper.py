from openerp.tests.common import BaseCase
from openerp.modules.registry import RegistryManager
from openerp.tools import config
import threading
import inspect

DB = config['db_name']
if not DB and hasattr(threading.current_thread(), 'dbname'):
    DB = threading.current_thread().dbname

arg_map = {
    'uid': ('user', )
}

def args_from_self(*args):
    def dec(fnct):
        def wrap(*a, **k):
            self = inspect.currentframe().f_back.f_locals.get('self')
            if self is None:
                self = inspect.currentframe().f_back.f_locals.get('cls')
            if self:
                sref = []
                a = list(a)
                if isinstance(self, BaseCase):
                    if not hasattr(fnct, 'im_self'):
                        sref, a = a[:1], a[1:]
                    argspec = inspect.getargspec(fnct)
                    for i, arg in enumerate(args):
                        argv = getattr(self, arg)
                        if i >= len(a) or (argv != a[i] and (argspec.args[i+1] == arg or
                                                argspec.args[i+1] in arg_map.get(arg, []))):
                            a.insert(i, argv)
                return fnct(*(sref + a), **k)
            else:
                return fnct(*a, **k)
        return wrap
    return dec

class IdWrapper(int):
    __slots__ = ['obj', 'wobj', 'cr', 'uid', 'context']

    def __new__(cls, val, obj, cr, uid, context):
        res = super(IdWrapper, cls).__new__(cls, val)
        res.obj, res.cr, res.uid, res.context = obj, cr, uid, context
        res.wobj = ModelWrapper(obj._name)
        return res

    @property
    def browse(self):
        return self.obj.browse(self.cr, self.uid, self, context=self.context)

class IdListWrapper(list):

    def __init__(self, l, obj, cr, uid, context):
        super(IdListWrapper, self).__init__(l)
        self.obj, self.cr, self.uid, self.context = obj, cr, uid, context
        self.wobj = ModelWrapper(obj._name)

    @property
    def browse(self):
        return self.obj.browse(self.cr, self.uid, self, context=self.context)



class ModelWrapper(object):
    """
    When a method on the wrapped model is called from within an openerp test case,
    automatically adds self.cr and self.uid as the first two arguments passed to the method.
    """

    @classmethod
    def registry(cls, model):
        return RegistryManager.get(DB)[model]

    def __init__(self, model):
        self.obj = self.registry(model)

    def __getattr__(self, item):
        itm = getattr(self.obj, item)
        if not hasattr(itm, '__call__'):
            return itm
        return args_from_self('cr', 'uid')(itm)


