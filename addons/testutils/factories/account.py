from testutils.factories import Factory, AutoIncrementStr, Many2One
from datetime import date, timedelta
from testutils.factories.autoincrement import AutoIncrementDate
from testutils.factories.field import One2Many
from testutils.factories.partner import PartnerFactory


class AccountTypeFactory(Factory):
    model = 'account.account.type'
    defaults = {
        'code': AutoIncrementStr('act'),
        'name': AutoIncrementStr('acct type'),
    }

class AccountFactory(Factory):
    model = 'account.account'

    defaults = {
        'code': AutoIncrementStr('code'),
        'name': AutoIncrementStr('account'),
        'user_type': Many2One(AccountTypeFactory()),
    }

class JournalFactory(Factory):
    model = 'account.journal'

    defaults = {
        'name': AutoIncrementStr('journal'),
        'type': 'general',
        'code': AutoIncrementStr('j'),
    }

class MoveFactory(Factory):
    model = 'account.move'
    defaults = {
        'journal_id': Many2One(JournalFactory()),
        'name': AutoIncrementStr('move'),
        'line_id': One2Many(lambda: MoveLineFactory())
    }


class MoveLineFactory(Factory):
    model = 'account.move.line'

    defaults = {
        'name': AutoIncrementStr('move'),
        'move_id': Many2One(MoveFactory()),
        'account_id': Many2One(AccountFactory()),
        'credit': 0,
        'debit': 0,
    }

class TaxCodeFactory(Factory):
    model = 'account.tax.code'

    defaults = {
        'name': AutoIncrementStr('tax account')
    }

class TaxFactory(Factory):
    model = 'account.tax'
    defaults = {
        'name': AutoIncrementStr('tax'),
        'type': 'percent',
        'amount': 0.1,
        'account_collected_id': Many2One(AccountFactory()),
        'account_paid_id': Many2One(AccountFactory()),
        'tax_code_id': Many2One(TaxCodeFactory()),
        'ref_tax_code_id': Many2One(TaxCodeFactory()),
        'base_code_id': Many2One(TaxCodeFactory()),
        'ref_base_code_id': Many2One(TaxCodeFactory()),
        'child_ids': One2Many(lambda: TaxFactory()),
    }

class InvoiceFactory(Factory):
    model = 'account.invoice'
    defaults = {
        'partner_id': Many2One(PartnerFactory()),
        'journal_id': Many2One(JournalFactory()),
        'account_id': Many2One(AccountFactory()),
        'type': 'out_invoice',
        'invoice_line': One2Many(lambda: InvoiceLineFactory())
    }

class InvoiceLineFactory(Factory):
    model = 'account.invoice.line'
    defaults = {
        'invoice_id': Many2One(InvoiceFactory()),
        'name': AutoIncrementStr('Invoice line '),
        'price_unit': 10,
        'quantity': 1,
    }

class FiscalyearFactory(Factory):
    model  = 'account.fiscalyear'
    defaults = {
        'name': AutoIncrementStr('fiscalyear ', start=1900),
        'code': AutoIncrementStr(start=1900),
        'date_start': AutoIncrementDate(date(1900, 1, 1), timedelta(days=366)),
        'date_stop': AutoIncrementDate(date(1900, 12, 31), timedelta(days=366)),
    }

class PaymentTermFactory(Factory):
    model = 'account.payment.term'
    defaults = {
        'name': AutoIncrementStr('Payment term'),
        'line_ids': One2Many(lambda: PaymentTermLineFactory()),
    }

class PaymentTermLineFactory(Factory):
    model = 'account.payment.term.line'

    defaults = {
        'payment_id': Many2One(PaymentTermFactory()),
        'value': 'balance',
        'days': 0,
        'days2': 0,
    }

