from field import FactoryField

class AutoIncrement(FactoryField):
    vals = {}

    def get_seq(self):
        key = (self.factory.model,  self.name)
        val = self.vals.get(key, self.start - 1) + self.step
        self.vals[key] = val
        return val

    def get(self, cr, uid, val=None, context=None):
        if not val:
            return self.get_next()
        else:
            return val

    def get_next(self):
        raise NotImplementedError()

    def __init__(self, start=1, step=1):
        self.start = start
        self.step = step

class AutoIncrementInt(AutoIncrement):

    def get_next(self):
        return self.get_seq()

class AutoIncrementStr(AutoIncrement):

    def __init__(self, prefix=None, start=1, step=1):
        self.prefix = prefix or ''
        super(AutoIncrementStr, self).__init__(start, step)

    def get_next(self):
        return self.prefix + str(self.get_seq())


class AutoIncrementDate(AutoIncrement):

    def __init__(self, start, tdelta):
        super(AutoIncrementDate, self).__init__(1, 1)
        self.date_start = start
        self.tdelta = tdelta

    def get_next(self):
        dt = (self.get_seq() - 1) * self.tdelta + self.date_start
        return dt.strftime('%Y-%m-%d')