from factory import Factory
from autoincrement import AutoIncrementStr
from field import Many2Many

class UserFactory(Factory):
    model = 'res.users'
    defaults = {
        'name': AutoIncrementStr('user')
    }

class GroupFactory(Factory):
    model = 'res.groups'
    defaults = {
        'name': AutoIncrementStr('group'),
        'users': Many2Many(UserFactory()),
    }