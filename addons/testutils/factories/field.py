
class FactoryField(object):

    def register(self, factory, name):
        self.name = name
        self.factory = factory

    def get(self, cr, uid, val=None, context=None):
        return val

class RelatedField(FactoryField):
    def __init__(self, factory):
        self._subfactory = factory

    @property
    def subfactory(self):
        if hasattr(self._subfactory, '__call__'):
            self._subfactory = self._subfactory()
        return self._subfactory

class Many2One(RelatedField):

    def get(self, cr, uid, val=None, context=None):
        if isinstance(val, (int, long)):
            return val
        return self.subfactory.create(cr, values=val, uid=uid, context=context)

class One2Many(RelatedField):

    def get(self, cr, uid, val=None, context=None):
        if val is None:
            return []
        res = []
        for v in val:
            if len(v) == 3 and (v[0], v[1]) == (0, 0):
                v = self.subfactory.get_vals(cr, uid, v[2], context=context)
                res.append((0, 0, v))
            else:
                res.append(v)
        return res

class Many2Many(One2Many):
    pass
