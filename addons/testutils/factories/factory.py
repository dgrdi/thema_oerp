from testutils.factories.field import FactoryField
from testutils.object_wrapper import ModelWrapper, args_from_self, IdWrapper
from testutils.factories.field import RelatedField

class FactoryMetaclass(type):

    @classmethod
    def update_dict(cls, name, dct, parents):
        upd = {}
        for cl in parents:
            if hasattr(cl, name):
                upd.update(getattr(cl, name))
        upd.update(dct)
        return upd

    def __new__(cls, name, parents, attrs):
        attrs['defaults'] = cls.update_dict('defaults', attrs.get('defaults', {}), parents)
        return super(FactoryMetaclass, cls).__new__(cls, name, parents, attrs)

class Factory(object):
    __metaclass__ = FactoryMetaclass

    model = None
    defaults = {}

    def __init__(self, defaults=None):
        if defaults is not None:
            self.defaults.update(defaults)
        self.wobj = ModelWrapper(self.model)
        self.obj = self.wobj.obj

    def get_vals(self, cr, uid, vals, context=None):
        vals = vals or {}
        df = self.defaults.copy()
        df.update({k: v for k, v in vals.items() if not isinstance(df.get(k), FactoryField)})
        for k, v in vals.items():
            if v is None:
                df.pop(k, None)
        for k, v in df.items():
            if isinstance(v, FactoryField):
                v.register(self, k)
                df[k] = v.get(cr, uid, vals.get(k), context=context)
            else:
                df[k] = vals.get(k) or v
        return df

    @args_from_self('cr', 'uid')
    def create(self, cr, uid, values=None, context=None):
        values = self.get_vals(cr, uid, values, context=context)
        crid = self.obj.create(cr, uid, values, context=context)
        return IdWrapper(crid, self.obj, cr, uid, context)

