__author__ = 'demetrio'

from factory import Factory

class PartnerFactory(Factory):
    model =  'res.partner'
    defaults =  {
        'name': 'Partner'
    }
