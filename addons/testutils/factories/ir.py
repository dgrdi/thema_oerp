from testutils.factories import Factory, field, autoincrement as ai

class SequenceTypeFactory(Factory):
    model = 'ir.sequence.type'
    defaults = {
        'name': ai.AutoIncrementStr('testseqcode'),
        'code': ai.AutoIncrementStr('ts'),
    }

class SequenceCodeField(field.Many2One):

    def get(self, cr, uid, val=None, context=None):
        if isinstance(val, basestring):
            return val
        vals = self.subfactory.get_vals(cr, uid, val, context)
        self.subfactory.obj.create(cr, uid, vals, context=context)
        return vals['code']

class SequenceFactory(Factory):
    model = 'ir.sequence'
    defaults = {
        'name': ai.AutoIncrementStr('testseq'),
        'code': SequenceCodeField(SequenceTypeFactory()),
        'implementation': 'standard',
        'prefix': '',
        'suffix': '',
        'padding': 0,
    }

