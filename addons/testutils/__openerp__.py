# -*- coding: utf-8 -*-

{
    'name': 'Test utilities',
    'version': '0.1',
    'category': 'Development',
    'description': """Utilities for testing openerp modules.""",
    'author': 'Thema Optical SRL',
    'website': 'http://www.thema-optical.com',
    'depends': ['base'],
    'init_xml': [],
    'data':[

        ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': True,
}
