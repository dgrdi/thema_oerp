# coding=utf-8
from lxml import etree

WARN_CHANGED = u"La regola da applicare per il calcolo delle provvigioni è cambiata. " \
               u"Per le righe già inserite, sarà necessario ricalcolarle manualmente."
WARN_MISSING = u"Non esiste una regola per il calcolo delle provvigioni su questo documento. " \
               u"Per favore correggere il contratto agente."
WARN_CONTRACT = u"Impossibile trovare un contratto per l'agente. Per favore definire un contratto."


class CommissionHeader(object):
    _cm_view_id = None

    def _commission_view_id(self, cr, uid):
        if not self._cm_view_id:
            md = self.pool.get('ir.model.data')
            module, name = self.FORM_XID.split('.')
            mdid = md.search(cr, uid, [('name', '=', name), ('module', '=', module)])[0]
            self._cm_view_id = md.read(cr, uid, mdid, fields=['res_id'])['res_id']
        return self._cm_view_id

    def fields_view_get(self, cr, user, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):
        res = super(CommissionHeader, self).fields_view_get(cr, user, view_id, view_type, context, toolbar, submenu)
        if res['view_id'] == self._commission_view_id(cr, user):
            doc = etree.fromstring(res['arch'])
            lines = doc.xpath("//field[@name='%s']" % self.LINES_FIELD)[0]
            lines.set('on_change', "onchange_commission(partner_id, agent_id, area_manager_id, pricelist_id, "
                                   "document_rule_id, am_document_rule_id, %s, warn_done, "
                                   "context)" % self.LINES_FIELD)
            res['arch'] = etree.tostring(doc)
        return res


    def onchange_commission(self, cr, uid, ids, partner_id, agent_id, area_manager_id, pricelist_id,
                            document_rule_id, am_document_rule_id, line_ids, warn_done, context=None):
        val = {}
        warn = False

        if partner_id and (agent_id or area_manager_id):
            lt = self.pool.get('thema.line.type')
            fields = ['price_unit', 'discount', 'line_type_id', self.QTY_FIELD]
            vals = self.resolve_2many_commands(cr, uid, self.LINES_FIELD, line_ids, fields=fields, context=context)
            line_type = lambda v: v['line_type_id'] if isinstance(v['line_type_id'], (int, long)) \
                else v['line_type_id'][0]
            line_type_ids = set(line_type(v) for v in vals)
            line_types = lt.browse(cr, uid, list(line_type_ids), context=context)
            line_types = {el.id: el for el in line_types}
            comp = lambda v: lt.compute(line_types[line_type(v)], v['price_unit'], v[self.QTY_FIELD], v['discount'])
            total = sum(comp(el)['price_subtotal'] for el in vals)
            discount = sum(comp(el)['discount_amount'] for el in vals)
            ag = self.pool.get('thema.agent')
            dr = self.pool.get('thema.document.rule')
            params = {
                'total': total,
                'discount_ratio': discount / total * 100 if total else 0,
                'pricelist_id': pricelist_id,
                'customer_id': partner_id,
                'agent_id': agent_id
            }
            for aid, prop, cname in [(agent_id, 'document_rule_id', 'contract_id'),
                                     (area_manager_id, 'am_document_rule_id', 'am_contract_id')]:
                if aid:
                    contract = ag.browse(cr, uid, aid, context=context)[cname]
                    if contract:
                        drule_id = dr.find(cr, uid, contract.id, params, context=context)
                        val[prop] = drule_id
                        if locals()[prop] and drule_id != locals()[prop]:
                            warn = WARN_CHANGED
                        if not drule_id:
                            warn = WARN_MISSING
                    else:
                        warn = WARN_CONTRACT

        res = {'value': val}

        if warn and not warn_done:
            res['warning'] = {
                'title': u'Attenzione!',
                'message': warn
            }
            val['warn_done'] = True
        return res

    def find_document_rules(self, cr, uid, ids, contract_id=None, am_contract_id=None, context=None):
        res = {}
        dr = self.pool.get('thema.document.rule')
        for doc in self.browse(cr, uid, ids, context=context):
            contract_id = contract_id or (doc.agent_id.contract_id and doc.agent_id.contract_id.id or False)

            am_contract_id = am_contract_id or ((doc.agent_id and doc.agent_id.area_manager_id.contract_id \
                                                and doc.agent_id.area_manager_id.contract_id.id) or False)
            params = {
                'total': doc.amount_untaxed,
                'discount_ratio': doc.amount_discount / doc.amount_untaxed * 100 if doc.amount_untaxed else 0,
                'pricelist_id': doc.pricelist_id.id,
                'customer_id': doc.partner_id.id,
                'agent_id': doc.agent_id.id,
            }
            res[doc.id] = {
                'document_rule_id': dr.find(cr, uid, contract_id, params, context=context) if contract_id else False,
                'am_document_rule_id': dr.find(cr, uid, am_contract_id, params,
                                               context=context) if am_contract_id else False,
            }
        return res

