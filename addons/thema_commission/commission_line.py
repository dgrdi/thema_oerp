WARN_LINE = u"Non esiste una regola su come calcolare la provvigione su questa riga. Per favore controllare " \
            u"il contratto agente."


class CommissionLine(object):
    def onchange_commission(self, cr, uid, ids, document_rule_id, am_document_rule_id, pricelist_id,
                            product_id, line_type_id, price_unit, discount, context=None):

        val = self.onchange_line_type_id(cr, uid, ids, line_type_id, context=context)
        warn = None
        if line_type_id:
            lt = self.pool.get('thema.line.type')
            ltype = lt.browse(cr, uid, line_type_id, context=context)
            if ltype.type != 'sale':
                val['agent_commission'] = 0.0
                val['manager_commission'] = 0.0
            else:
                oprice = price_unit
                if pricelist_id and product_id:
                    oprice = self.pool.get('product.pricelist').price_get_multi(cr, uid, [pricelist_id],
                                                                                [(product_id, 1, None)],
                                                                                context=context)[product_id][
                        pricelist_id]
                    if not oprice:
                        oprice = price_unit
                if oprice:
                    act_price = price_unit * (100 - discount) / 100
                    rel_discount = (oprice - act_price) / oprice * 100
                    params = {
                        'product_id': product_id,
                        'discount': rel_discount
                    }
                    lr = self.pool.get('thema.line.rule')
                    calc = {}
                    for drule_id, prop in [(document_rule_id, 'agent_commission'),
                                           (am_document_rule_id, 'manager_commission')]:
                        if drule_id:
                            lrule_id = lr.find(cr, uid, drule_id, params, context=context)
                            if not lrule_id:
                                warn = WARN_LINE
                            else:
                                calc[prop] = lrule_id
                    if calc:
                        comm = lr.compute(cr, uid, [(v, rel_discount) for v in calc.values()])
                        for prop, cm in zip(calc.keys(), comm):
                            val[prop] = cm
        res = {'value': val}
        if warn:
            res['warning'] = {
                'title': 'Attenzione!',
                'message': warn,
            }
        return res

    def update_commission(self, cr, uid, ids, contract_id=None, am_contract_id=None, context=None):
        lines = self.browse(cr, uid, ids, context=context)
        header_ids = list({el[self.HEADER_FIELD].id for el in lines})
        header_obj = self.pool.get(self._columns[self.HEADER_FIELD]._obj)
        document_rules = header_obj.find_document_rules(cr, uid, header_ids, contract_id=contract_id,
                                                        am_contract_id=am_contract_id, context=context)
        pricelist_ids = list({
            el['pricelist_id'][0] for el in header_obj.read(cr, uid, header_ids, fields=['pricelist_id']) \
            if el['pricelist_id']
        })
        product_ids = list({el.product_id.id for el in lines if el.product_id})
        prices = self.pool.get('product.pricelist').price_get_multi(cr, uid, pricelist_ids,
                                                                    [(p, 1, None) for p in product_ids],
                                                                    context=context)
        comp = {}
        comp_am = {}
        zero_lines = []
        lr = self.pool.get('thema.line.rule')
        pl = self.pool.get('product.pricelist')
        for line in lines:
            if line.line_type_id.type != 'sale':
                zero_lines.append(line.id)
            else:
                oprice = line.price_unit
                if line[self.HEADER_FIELD].pricelist_id and line.product_id:
                    oprice = prices[line[self.HEADER_FIELD].pricelist_id.id][line.product_id.id]
                    if not oprice:
                        oprice = line.price_unit
                act_price = line.price_unit * (100 - line.discount) / 100
                rel_discount = (oprice - act_price) / oprice * 100
                params = {
                    'product_id': line.product_id and line.product_id.id or False,
                    'discount': rel_discount
                }
                drid = document_rules[line[self.HEADER_FIELD].id]['document_rule_id']
                am_drid = document_rules[line[self.HEADER_FIELD].id]['am_document_rule_id']
                if drid:
                    comp[line.id] = (lr.find(cr, uid, drid, params, context=context), rel_discount)
                if am_drid:
                    comp_am[line.id] = (lr.find(cr, uid, am_drid, params, context=context), rel_discount)
        self.write(cr, uid, zero_lines, {'agent_commission': 0.0, 'manager_commission': 0.0}, context=context)
        vals = {}
        for id, val in zip(comp, lr.compute(cr, uid, comp.values(), context=context)):
            vals.setdefault(id, {})['agent_commission'] = val
        for id, val in zip(comp_am, lr.compute(cr, uid, comp_am.values(), context=context)):
            vals.setdefault(id, {})['manager_commission'] = val
        for id, val in vals.items():
            self.write(cr, uid, id, val, context=context)
        #TODO Return something significant about possbile problems to be shown in wizard
        return True