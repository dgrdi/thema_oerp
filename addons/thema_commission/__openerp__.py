# -*- encoding: utf-8 -*-
##############################################################################
#
#    Copyright (c) 2013 ZestyBeanz Technologies Pvt. Ltd.
#    (http://wwww.zbeanztech.com)
#    contact@zbeanztech.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Thema Commission calculation',
    'version': '0.1',
    'sequence': 1,
    'category': 'Custom',
    'complexity': "easy",
    'description': """ This module handled the Commission calculation for Sales and invoice""",
    'author': 'Zesty Beanz Technologies PVT Ltd',
    'website': 'http://www.zbeanztech.com',
    'depends': [
        'thema_line_type',
        'sale',
        'thema_product',
        'thema_discounts',

        ],
    'data': [
        'ir.model.access.csv',
        'contract_view.xml',
        'sale_view.xml',
        'account_view.xml',
        'res_partner_view.xml',
        'update_wiz.xml'
        ],
    'demo_xml': [],
    'installable': True,
    'auto_install': True,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: