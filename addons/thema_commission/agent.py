from openerp.osv  import orm, fields


class agent(orm.Model):
    _name = 'thema.agent'
    _description = "Agente"
    _columns = {
        'name': fields.char('Name', size=32, required=True),
        'partner_id': fields.many2one('res.partner', 'Partner'),
    }


class contract(orm.Model):
    _name = 'thema.contract'
    _description = "Contratto provvigioni"
    _columns = {
        'name': fields.char('Nome', size=32, required=True),
        'document_rule_ids': fields.one2many('thema.document.rule', 'contract_id', 'Regole documento'),
    }

