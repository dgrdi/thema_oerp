from openerp.osv import fields, orm
import openerp.addons.decimal_precision as dp
from openerp.tools import float_round

class line_rule(orm.Model):
    _name = 'thema.line.rule'
    _rec_name = 'discount'
    _description = "Regola riga"
    _order = 'document_id, sequence'
    _columns = {
        'document_id': fields.many2one('thema.document.rule', 'Regola documento'),
        'sequence': fields.integer('Progressivo'),
        'prod_model_id': fields.many2one('product.model', 'Modello prodotto'),
        'prod_categ_id': fields.many2one('product.category', 'Categoria prodotto'),
        'prod_collection_id': fields.many2one('product.collection', 'Linea prodotto'),
        'discount': fields.float('Sconto minimo', digits_compute=dp.get_precision('Discount'), ),
        'base_commission': fields.float('Provvigione base'),
        'compensation': fields.float('Compensazione'),
        'allowance': fields.float('Franchigia'),
        'rounding_factor': fields.float('Fattore di arrotondamento'),
    }

    def find(self, cr, uid, document_rule_id, params, context=None):
        for k in ['product_id', 'discount']:
            assert k in params, 'Missing parameter: %s' % k
        rule_ids = self.search(cr, uid, [('document_id', '=', document_rule_id)], context=context)
        if params['product_id']:
            product = self.pool.get('product.product').browse(cr, uid, params['product_id'], context=context)
            cat = product.categ_id
            categories = set()
            if cat:
                categories.add(cat.id)
                while cat.parent_id:
                    cat = cat.parent_id
                    categories.add(cat.id)
            coll = product.collection_id
            collections = set()
            if coll:
                collections.add(coll.id)
                while coll.parent_id:
                    coll = coll.parent_id
                    collections.add(coll.id)
        else:
            product = None
            categories = []
            collections = []

        for rule in self.browse(cr, uid, rule_ids, context=context):
            if (not rule.prod_model_id or (product and rule.prod_model_id.id == product.model_id.id)) and \
                    (not rule.prod_categ_id or (product and rule.prod_categ_id.id in categories)) and \
                    (not rule.prod_collection_id or (product and rule.prod_collection_id.id in collections)) and \
                    (rule.discount <= params['discount']):
                return rule.id
        return False


    def compute(self, cr, uid, vals, context=None):
        """
        :param vals: a list of 2-tuples (rule_id, discount)
        :return: a list of commission values
        """
        if not vals:
            return []
        rule_ids = set(el[0] for el in vals)
        rules = self.browse(cr, uid, list(rule_ids), context=context)
        rules = {r.id: r for r in rules}
        res = []
        for rule_id, discount in vals:
            rule = rules[rule_id]
            cmp = rule.compensation * max([0, discount - rule.allowance])
            cmp = float_round(rule.base_commission - cmp, precision_rounding=(rule.rounding_factor or 0.01))
            res.append(cmp)
        return res