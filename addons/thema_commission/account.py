# -*- encoding: utf-8 -*-
# #############################################################################
#
# Copyright (c) 2013 ZestyBeanz Technologies Pvt. Ltd.
# (http://wwww.zbeanztech.com)
# contact@zbeanztech.com
#
# This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import orm, fields
import openerp.addons.decimal_precision as dp
from .commission_header import CommissionHeader
from .commission_line import CommissionLine
from lxml import etree


class account_invoice(CommissionHeader, orm.Model):
    _inherit = 'account.invoice'

    LINES_FIELD = 'invoice_line'
    QTY_FIELD = 'quantity'
    FORM_XID = 'account.invoice_form'

    _columns = {
        'pricelist_id': fields.many2one('product.pricelist', 'Listino'),
        'agent_id': fields.many2one('thema.agent', 'Agent'),
        'area_manager_id': fields.many2one('thema.agent', 'Area manager'),
        'total_commission': fields.float('Total Commission', digits_compute=dp.get_precision('Account')),
        'document_rule_id': fields.many2one('thema.document.rule', 'Regola documento'),
        'am_document_rule_id': fields.many2one('thema.document.rule', 'Regola documento CA'),
        'warn_done': fields.dummy(type='boolean')
    }

    _defaults = {
        'warn_done': False,
    }


    def onchange_partner_id(self, cr, uid, ids, type, partner_id, \
                            date_invoice=False, payment_term=False, partner_bank_id=False, company_id=False,
                            context=None):
        res = super(account_invoice, self).onchange_partner_id(cr, uid, ids, type, partner_id, date_invoice,
                                                               payment_term, partner_bank_id, company_id)
        if partner_id:
            p = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
            res['value'].update({
                'agent_id': p.agent_id and p.agent_id.id or False,
                'area_manager_id': p.agent_id.area_manager_id and p.agent_id.area_manager_id.id or False,
            })
        return res

class account_invoice_line(CommissionLine, orm.Model):
    _inherit = 'account.invoice.line'
    HEADER_FIELD = 'invoice_id'
    _columns = {
        'agent_commission': fields.float('Provv.', digits_compute=dp.get_precision('Account'), ),
        'manager_commission': fields.float('Provv. CA', digits_compute=dp.get_precision('Account'), ),
    }

    def product_id_change(self, cr, uid, ids, product, uom_id, qty=0, name='', type='out_invoice', partner_id=False,
                          fposition_id=False, price_unit=False, currency_id=False, context=None, company_id=None):
        res = super(account_invoice_line, self).product_id_change(cr, uid, ids, product, uom_id, qty, name, type,
                                                                  partner_id, fposition_id, price_unit, currency_id,
                                                                  context, company_id)
        context = context or {}
        cm = self.onchange_commission(cr, uid, ids, context.get('document_rule_id'), context.get('am_document_rule_id'),
                                      context.get('pricelist_id'), product, context.get('line_type_id'), price_unit,
                                      context.get('discount'), context=context)
        for k, v in cm.items():
            res.setdefault(k, {}).update(v)
        return res
