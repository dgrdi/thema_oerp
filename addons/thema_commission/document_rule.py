from openerp.osv import orm, fields
from openerp.addons import decimal_precision as dp


class document_rule(orm.Model):
    _name = 'thema.document.rule'
    _rec_name = 'min_total'
    _description = "Document Rule"
    _order = 'contract_id, sequence'
    _columns = {
        'contract_id': fields.many2one('thema.contract', 'Contratto'),
        'sequence': fields.integer('Progressivo'),
        'min_total': fields.float('Importo totale minimo', digits_compute=dp.get_precision('Account'), ),
        'discount_ratio': fields.float('Minima % sconto merce',
                                       digits_compute=dp.get_precision('Account'), ),
        'pricelist_id': fields.many2one('product.pricelist', 'Listino'),
        'customer_id': fields.many2one('res.partner', 'Cliente'),
        'cust_category_id': fields.many2one('res.partner.category', 'Categoria cliente'),
        'agent_id': fields.many2one('thema.agent', 'Agente'),
        'rule_ids': fields.one2many('thema.line.rule', 'document_id', 'Regole riga'),
    }

    def find(self, cr, uid, contract_id, params, context=None):
        for k in ['total', 'discount_ratio', 'pricelist_id', 'customer_id', 'agent_id']:
            assert k in params, "Missing parameter: %s" % k
        rule_ids = self.search(cr, uid, [('contract_id', '=', contract_id)], context=context)
        cust = self.pool.get('res.partner').browse(cr, uid, params['customer_id'], context=context)
        categories = set()
        for cat in cust.category_id:
            categories.add(cat.id)
            while cat.parent_id:
                cat = cat.parent_id
                categories.add(cat.id)

        for rule in self.browse(cr, uid, rule_ids, context=context):
            if (rule.min_total <= params['total']) and \
                    (rule.discount_ratio <= params['discount_ratio']) and \
                    (not rule.pricelist_id or params['pricelist_id'] == rule.pricelist_id.id) and \
                    (not rule.customer_id or rule.customer_id.id == cust.id) and \
                    (not rule.cust_category_id or rule.cust_category_id.id in categories) and \
                    (not rule.agent_id or params['agent_id'] == rule.agent_id.id):
                return rule.id
        return False
