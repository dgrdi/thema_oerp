
from openerp.osv import orm, fields

class cm_wizard(orm.TransientModel):
    _name = 'thema.commission.update.wiz'
    _columns = {
        'contract_id': fields.many2one('thema.contract', 'Contratto agente'),
        'am_contract_id': fields.many2one('thema.contract', 'Contratto capoarea'),
    }

    def _default(am=False):
        def df(self, cr, uid, context=None):
            if not context:
                return False
            ids = context.get('active_ids', [context.get('active_id')])
            if len(ids) != 1:
                return False
            ag = self.pool.get(context.get('active_model')).browse(cr, uid, ids[0], context=context)
            if am:
                ag = ag.area_manager_id
            return ag.contract_id and ag.contract_id.id or False
        return df

    _defaults = {
        'contract_id': _default('agent_id'),
        'am_contract_id': _default('area_manager_id'),
    }

    def execute(self, cr, uid, ids, context=None):
        context = context or {}
        wiz = self.browse(cr, uid, ids[0], context=context)
        doc_ids = context.get('active_ids', [context.get('active_id')])
        model = self.pool.get(context.get('active_model'))
        line_model = self.pool.get(model._columns[model.LINES_FIELD]._obj)
        line_ids = line_model.search(cr, uid, [(line_model.HEADER_FIELD, 'in', doc_ids)], context=context)
        line_model.update_commission(cr, uid, line_ids, wiz.contract_id and wiz.contract_id.id or False,
                                     wiz.am_contract_id and wiz.am_contract_id.id or False,
                                     context=context)

        return True
