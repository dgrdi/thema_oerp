# -*- coding: utf-8 -*-
# #############################################################################
#
# OpenERP, Open Source Management Solution
# Copyright (C) 2013-2014 ZestyBeanz Technologies Pvt Ltd(<http://www.zbeanztech.com>).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from lxml import etree
from openerp.osv import orm, fields
import openerp.addons.decimal_precision as dp
from .commission_header import CommissionHeader
from .commission_line import CommissionLine


class sale_order(CommissionHeader, orm.Model):
    _inherit = 'sale.order'

    LINES_FIELD = 'order_line'
    QTY_FIELD = 'product_uom_qty'
    FORM_XID = 'sale.view_order_form'

    _columns = {
        'agent_id': fields.many2one('thema.agent', 'Agente'),
        'total_commission': fields.float('Totale provvigioni', digits_compute=dp.get_precision('Account'),
                                         help="The amount of total commission"),
        'area_manager_id': fields.many2one('thema.agent', 'Capoarea'),
        'document_rule_id': fields.many2one('thema.document.rule', 'Regola documento'),
        'am_document_rule_id': fields.many2one('thema.document.rule', 'Regola documento AM'),
        'warn_done': fields.dummy(type='boolean')
    }

    _defaults = {
        'warn_done': False,
    }

    def _prepare_invoice(self, cr, uid, order, lines, context=None):
        res = super(sale_order, self)._prepare_invoice(cr, uid, order, lines, context=context)
        res.update({
            'pricelist_id': order.pricelist_id,
        })

    def onchange_partner_id(self, cr, uid, ids, partner_id, context=None):
        res = super(sale_order, self).onchange_partner_id(cr, uid, ids, partner_id, context=context)
        if partner_id:
            partner = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
            res['value'].update({
                'agent_id': partner.agent_id and partner.agent_id.id or False,
                'area_manager_id': partner.agent_id.area_manager_id and partner.agent_id.area_manager_id.id or False,
            })
        return res

    def onchange_commission(self, cr, uid, ids, partner_id, agent_id, area_manager_id, pricelist_id,
                            document_rule_id, am_document_rule_id, line_ids, warn_done, context=None):
        res = super(sale_order, self).onchange_commission(cr, uid, ids, partner_id, agent_id, area_manager_id,
                                                          pricelist_id,
                                                          document_rule_id, am_document_rule_id, line_ids, warn_done,
                                                          context=context)
        pricelist_change = self.onchange_pricelist_id(cr, uid, ids, pricelist_id, line_ids)
        for k, v in pricelist_change.items():
            res.setdefault(k, {}).update(v)
        return res


class sale_order_line(CommissionLine, orm.Model):
    _inherit = 'sale.order.line'
    HEADER_FIELD = 'order_id'

    _columns = {
        'agent_commission': fields.float('Provv.', digits_compute=dp.get_precision('Account'), ),
        'manager_commission': fields.float('Provv. CA', digits_compute=dp.get_precision('Account'), ),
    }


    def product_id_change(self, cr, uid, ids, pricelist, product, qty=0,
                          uom=False, qty_uos=0, uos=False, name='', partner_id=False,
                          lang=False, update_tax=True, date_order=False, packaging=False, fiscal_position=False,
                          flag=False, context=None):
        res = super(sale_order_line, self).product_id_change(cr, uid, ids, pricelist, product, qty,
                                                             uom, qty_uos, uos, name, partner_id,
                                                             lang, update_tax, date_order, packaging, fiscal_position,
                                                             flag, context=context)
        context = context or {}
        cm = self.onchange_commission(cr, uid, ids, context.get('document_rule_id'), context.get('am_document_rule_id'),
                                      pricelist, product, context.get('line_type_id'), context.get('price_unit'),
                                      context.get('discount'), context=context)
        for k, v in cm.items():
            res.setdefault(k, {}).update(v)
        return res

    def _prepare_line(self, cr, uid, sale, vals, context=None):
        vals = super(sale_order_line, self)._prepare_line(cr, uid, sale, vals, context=context)
        dr = self.pool.get('sale.order').find_document_rules(cr, uid, [sale.id], context=context)[sale.id]
        ocm = self.onchange_commission(cr, uid, [],
                                       document_rule_id=dr['document_rule_id'],
                                       am_document_rule_id=dr['am_document_rule_id'],
                                       pricelist_id=sale.pricelist_id and sale.pricelist_id.id or False,
                                       product_id=vals['product_id'],
                                       line_type_id=vals['line_type_id'],
                                       price_unit=vals['price_unit'],
                                       discount=vals['discount'],
                                       context=context)['value']
        vals.update(ocm)
        return vals


