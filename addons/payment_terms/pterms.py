# coding=utf-8
from openerp.osv import orm, fields
from payment_terms.datemapper import DateListMapper
from openerp.addons.decimal_precision import get_precision


class payment_term(orm.Model):
    _inherit = 'account.payment.term'

    _columns = {
        'exclude_ids': fields.one2many('account.payment.term.exclude',
                                       'payment_id',
                                       'Periodi di esclusione'),
        'cod': fields.boolean('Contrassegno'),
        'discount': fields.float('Sconto incondizionato', digits_compute=get_precision('Account')),
        'conditional_discount': fields.float('Sconto condizionato', digits_compute=get_precision('Account')),
        'conditional_discount_days': fields.integer('Termine (in GG) per lo sconto condizionato'),
    }

    def compute(self, cr, uid, id, value, date_ref=False, context=None):
        res = super(payment_term, self).compute(cr, uid, id, value, date_ref, context)
        if res:
            pay = self.browse(cr, uid, id, context=context)
            mapper = DateListMapper(pay.exclude_ids)
            dates = mapper.map(zip(*res)[0])
            res = [(d, r[1]) for r, d in zip(res, dates)]
        return res



class payment_term_eclude(orm.Model):
    _name = 'account.payment.term.exclude'

    _columns = {
        'payment_id': fields.many2one('account.payment.term', 'Pagamento', required=True,
                                      ondelete='cascade'),
        'month_start': fields.integer('Mese da', required=True),
        'day_start': fields.integer('Giorno da', required=True),
        'month_stop': fields.integer('Mese a', required=True),
        'day_stop': fields.integer('Giorno a', required=True),
        'days': fields.integer('Numero di giorni'),
        'days2': fields.integer('Giorno del mese'),
    }

    _sql_constraints = [
        (
            'datestart',
            "CHECK ( COALESCE(month_start, 0) > 0 AND COALESCE(day_start, 0) > 0 "
            "        AND fn_check_drange(month_start, day_start) )",
            u'La combinazione Mese da / Giorno da non è valida!',

        ),
        (
            'datestop',
            "CHECK ( COALESCE(month_stop, 0) > 0 AND COALESCE(day_stop, 0) > 0 "
            "        AND fn_check_drange(month_stop, day_stop) )",
            u'La combinazione Mese a / Giorno a non è valida!',
        )
    ]

    def init(self, cr):
        cr.execute('''
            CREATE OR REPLACE FUNCTION fn_check_drange(month int, day int)
              RETURNS boolean as $b$
            BEGIN
              PERFORM ('2000-' ||right('00' || month::varchar, 2) || '-' || right('00' || day::varchar, 2))::date;
              RETURN TRUE ;
            EXCEPTION
               WHEN datetime_field_overflow THEN
                   RETURN FALSE ;
            END
            $b$ LANGUAGE plpgsql;
        ''')

