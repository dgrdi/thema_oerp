from openerp.osv import orm, fields
from openerp.addons.decimal_precision import get_precision
import datetime

class test_payment(orm.TransientModel):
    _name = 'account.payment.term.test'

    _columns = {
        'payment_term_id': fields.many2one('account.payment.term', 'Termine di pagamento'),
        'date_ref': fields.date('Data di riferimento'),
        'amount': fields.float('Importo', digits_compute=get_precision('Account')),
        'line_ids': fields.one2many('account.payment.term.test.line', 'test_id', 'Risultato', readonly=True),
    }

    _defaults = {
        'date_ref': lambda *a: datetime.date.today().strftime('%Y-%m-%d'),
    }
    def onchange(self, cr, uid, ids, payment_term_id, date_ref, amount, context=None):
        res = self.pool.get('account.payment.term').compute(cr, uid, payment_term_id, amount, date_ref=date_ref, context=context)
        val = [{
            'date': r[0],
            'amount': r[1],
        } for r in res]
        return {
            'value': {
                'line_ids': val
            }
        }
        
class test_payment_line(orm.TransientModel):
    _name =  'account.payment.term.test.line'

    _columns = {
        'test_id': fields.many2one('account.payment.term.test', 'ID test'),
        'date': fields.date('Data'),
        'amount': fields.float('Importo', digits_compute=get_precision('Account')),
    }
