from openerp.osv import orm, fields
from openerp.addons import decimal_precision as dp


class invoice_due_date(orm.Model):
    _name = 'account.invoice.due_date'
    _order = 'invoice_id, date'

    _columns = {
        'invoice_id': fields.many2one('account.invoice', 'Fattura', ondelete='cascade', required=True, select=1),
        'date': fields.date('Data', required=True),
        'amount': fields.float('Importo', digits_compute=dp.get_precision('Account'), required=True),
    }


class invoice(orm.Model):
    _inherit = 'account.invoice'

    _columns = {
        'due_date_ids': fields.one2many('account.invoice.due_date', 'invoice_id', string='Scadenze'),
    }

    def button_compute(self, cr, uid, ids, context=None, **kwargs):
        super(invoice, self).button_compute(cr, uid, ids, context=context, **kwargs)
        self.compute_due_dates(cr, uid, ids, context=context)
        return True

    def compute_due_dates(self, cr, uid, ids, context=None):
        po = self.pool.get('account.payment.term')
        cr.execute("""
            DELETE FROM account_invoice_due_date WHERE invoice_id IN %s;
        """, [tuple(ids)])
        for inv in self.browse(cr, uid, ids, context=context):
            terms = None
            if inv.payment_term and inv.date_invoice:
                terms = po.compute(cr, uid, inv.payment_term.id, inv.amount_total, date_ref=inv.date_invoice,
                                   context=context)
            elif inv.date_due:
                terms = [(inv.date_due, inv.amount_total)]
            elif inv.date_invoice:
                terms = [(inv.date_invoice, inv.amount_total)]
            if terms:
                terms = [(0, 0, {'date': date, 'amount': amount}) for date, amount in terms]
                self.write(cr, uid, inv.id, {'due_date_ids': terms}, context=context)
