from unittest2 import TestCase
from openerp.tests.common import SingleTransactionCase
from psycopg2._psycopg import IntegrityError
from payment_terms.datemapper import DateMapper, DateListMapper
from testutils.transaction import RollbackCase
from testutils.factories.account import PaymentTermFactory



class TestDateMap(TestCase):

    @property
    def exc(self):
        return

    def test_date_map_in_month(self):
        exc = {
            'month_start': 1,
            'day_start': 1,
            'month_stop': 1,
            'day_stop': 31,
            'days': 10,
            'days2': 10,
        }
        mapper = DateMapper(exc)
        self.assertEqual(mapper.map('2000-01-01'), '2000-02-10')

    def test_date_map_mult(self):
        exc1 = {
            'month_start': 1,
            'day_start': 1,
            'month_stop': 1,
            'day_stop': 31,
            'days': 10,
            'days2': 10,
        }
        exc2 = {
            'month_start': 3,
            'day_start': 1,
            'month_stop': 3,
            'day_stop': 31,
            'days': 10,
            'days2': 10,
        }
        mapper = DateListMapper([exc1, exc2])
        dts = ['2000-01-01', '2000-03-01', '2000-09-01']
        res = mapper.map(dts)
        self.assertListEqual(res, ['2000-02-10', '2000-04-10', '2000-09-01'])

class TestPaymentTerm(RollbackCase):
    pfac = PaymentTermFactory()

    def test_compute(self):
        pt = self.pfac.create({
            'line_ids': [(0, 0, {
                'value': 'procent',
                'value_amount': 0.5,
                'days': 0,
                'days2': 0,
            }), (0, 0, {
                'value': 'balance',
                'days': 0,
                'days2': 1,
            })],
            'exclude_ids': [(0,0, {
                'month_start': 1,
                'day_start': 1,
                'month_stop': 1,
                'day_stop': 31,
                'days': 10,
                'days2': 10,
            }), (0, 0, {
                'month_start': 3,
                'day_start': 1,
                'month_stop': 3,
                'day_stop': 31,
                'days': 10,
                'days2': 10,
            })]
        })
        res = self.pfac.wobj.compute(pt, 100, date_ref='2000-01-01')
        self.assertSetEqual(set(res), {('2000-02-01', 50), ('2000-02-10', 50)})

    def test_invalid_rage(self):
        c = lambda: self.pfac.create({
            'exclude_ids': [(0, 0, {
                'month_start': 1,
                'day_start': 33,
            })]
        })
        self.assertRaises(IntegrityError, c)


