{
    'name': 'Payment terms',
    'version': '0.2',
    'category': 'Development',
    'description': """
        Termini di pagamento
    """,
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': ['base', 'account','sale'],
    'init_xml': [],
    'data':[
        'ir.model.access.csv',
        'views/payment.xml',
        'views/sale.xml',
        ],
    'demo_xml': [],
    'js': [],
    'qweb': [],
    'css': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}

