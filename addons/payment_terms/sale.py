from openerp.osv import orm, fields

class sale_order(orm.Model):
    _inherit = 'sale.order'
    _columns = {
        'cod': fields.boolean('Contrassegno'),
    }

    def onchange_payment_term(self, cr, uid, ids, payment_term, context=None):
        if payment_term:
            pt = self.pool.get('account.payment.term').browse(cr, uid, payment_term, context=context)
            return {'value': {'cod': pt.cod}}
        return {}

