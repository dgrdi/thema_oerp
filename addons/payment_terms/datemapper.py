import functools
from datetime import date, datetime
from dateutil.relativedelta import relativedelta

class DateMapper(object):
    DFMT = '%Y-%m-%d'

    def __init__(self, exc):
        self.exc = exc

    @property
    def dstart(self):
        return functools.partial(datetime, month=self.exc['month_start'], day=self.exc['day_start'])

    @property
    def dstop(self):
        return functools.partial(datetime, month=self.exc['month_stop'], day=self.exc['day_stop'])

    def translate(self, date):
        """
        :type date: datetime
        :rtype: date
        """
        date += relativedelta(days=self.exc['days'])
        d2 = self.exc['days2']
        if d2 < 0:
            date += relativedelta(day=1, months=1)
            date += relativedelta(days=d2)
        elif d2 > 0:
            date += relativedelta(day=d2, months=1)
        return date

    def map(self, date):
        """
        :type date: str
        :rtype: str
        """
        d = datetime.strptime(date, self.DFMT)
        d1 = self.dstart(d.year)
        d2 = self.dstop(d.year)
        if d1 <= d <= d2:
            return self.translate(d).strftime(self.DFMT)
        else:
            return date

class DateListMapper(object):
    def __init__(self, exc_data):
        self.mappers = map(DateMapper, exc_data)

    def map(self, dates):
        for m in self.mappers:
            dates = map(m.map, dates)
        return dates
