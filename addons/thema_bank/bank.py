from openerp.osv import orm, fields
import string

class res_partner_bank(orm.Model):
    _inherit = 'res.partner.bank'

    _columns = {
        'acc_number': fields.char('Numero conto', required=False),
        'abi': fields.char('Codice ABI'),
        'cab': fields.char('Codice CAB'),
    }

    def __init__(self, pool, cr):
        super(res_partner_bank, self).__init__(pool, cr)
        self.country_it = pool.get('res.country').search(cr, 1, [('code', '=', 'IT')])[0]


    def post_write(self, cr, uid, ids, context=None):
        """disable automatic creation of journals"""
        return True


    def abicab_from_iban(self, acc_number):
        if acc_number:
            acc_number = filter(lambda x: x in string.ascii_letters + string.digits, acc_number)
            if len(acc_number) == 27:
                return acc_number[5:10], acc_number[10:15]
        return False, False

    def on_change_bank(self, cr, uid, ids, state, country_id, acc_number, context=None):
        vals = {}
        if state == 'iban' and country_id == self.country_it:
            abi, cab = self.abicab_from_iban(acc_number)
            vals = {
                'abi': abi,
                'cab': cab
            }
        elif country_id and country_id != self.country_it:
            vals = {
                'abi': False,
                'cab': False
            }
        return {
            'value': vals
        }

    def _add_missing_default_values(self, cr, uid, values, context=None):
        vals = super(res_partner_bank, self)._add_missing_default_values(cr, uid, values, context)
        #add abi & cab if account type is iban and country is it
        if values.get('state') == 'iban' and values.get('country_id') == self.country_it and values.get('acc_number') \
                and not values.get('abi') and not values.get('iban'):
            abi, cab = self.abicab_from_iban(values.get('acc_number'))
            values.update({
                'abi': abi,
                'cab': cab
            })
        return values

    def update_bank_details(self, cr, uid, context=None):
        assert uid == 1
        partners = {}
        banks = {}
        for b in self.browse(cr, uid, self.search(cr, uid, [], context=context), context=context):
            banks.setdefault(b.partner_id.id, []).append(b.id)
            if b.partner_id.id not in partners:
                partners[b.partner_id.id] = self.onchange_partner_id(cr, uid, b.id, b.partner_id.id, context=context)['value']
        for pid, data in partners.iteritems():
            self.write(cr, uid, banks[pid], data, context=context)

