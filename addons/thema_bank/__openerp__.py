{
    'name': 'Thema - conti bancari ABI/CAB',
    'version': '0.1',
    'category': 'Administration',
    'description': """Gestione di conti bancari italiani (ABI/CAB)""",
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': ['base', 'account'],
    'init_xml': [],
    'data':[
        'data/bank.xml',
        'views/bank.xml',
        'views/company.xml',
        ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': True,
}
