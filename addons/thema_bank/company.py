from openerp.osv import orm, fields

class company(orm.Model):
    _inherit = 'res.company'
    _columns = {
        'sia_code': fields.char('Codice SIA')
    }

