{
    'name': 'Reverse charge',
    'version': '0.2',
    'category': 'UI',
    'description': """Reverse charge""",
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': ['base', 'account', 'account_hooks', 'tax_multiple'],
    'init_xml': [],
    'data':[
        'ir.model.access.csv',
        'views/journal.xml',
        'views/tax.xml',
        'views/invoice.xml',
        ],
    'demo_xml': [],
    'js': [],
    'qweb': [],
    'css': [],
    'test': [],
    'installable': True,
    'auto_install': True,
}

