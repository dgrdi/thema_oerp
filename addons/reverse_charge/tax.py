# coding=utf-8
from openerp.osv import orm, fields


class account_tax_template(orm.Model):
    _inherit = 'account.tax.template'
    _columns = {
        'reverse_charge': fields.boolean('Imposta per doppia registrazione contabile (autofatture)'),
        'rc_sale_tax_id': fields.many2one('account.tax.template', 'Imposta reg. vendite'),
        'rc_purchase_tax_id': fields.many2one('account.tax.template', 'Imposta reg. acquisti'),
    }

    def get_tax_vals(self, cr, uid, tax_template, tax_template_to_tax, tax_code_template_ref, context=None):
        vals = super(account_tax_template, self).get_tax_vals(self, cr, uid, tax_template, tax_template_to_tax,
                                                                  tax_code_template_ref, context)
        vals.update({
            'reverse_charge': tax_template_to_tax.get(tax_template.reverse_charge, None),
            'rc_sale_tax_id': tax_template_to_tax.get(tax_template.rc_sale_tax_id.id, None),
            'rc_purchase_tax_id': tax_template_to_tax.get(tax_template.rc_purchase_tax_id.id, None),
        })

class account_tax(orm.Model):
    _inherit = 'account.tax'

    _columns = {
        'reverse_charge': fields.boolean('Imposta per doppia registrazione contabile (autofatture)'),
        'rc_sale_tax_id': fields.many2one('account.tax', 'Imposta reg. vendite',
                                          help=u'Questa imposta verrà usata nelle autofatture generate automaticamente.'),
        'rc_purchase_tax_id': fields.many2one('account.tax', 'Imposta reg. acquisti',
                                              help=u'Questa imposta verrà usata nelle registrazioni acquisti delle '
                                                   u'autofatture generate automaticamente.')
    }
