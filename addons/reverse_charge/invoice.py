from openerp.osv import orm, fields
import copy
from openerp.netsvc import LocalService

class invoice(orm.Model):
    _inherit = 'account.invoice'

    _columns = {
        'reverse_charge': fields.boolean('Doppia registrazione'),
        'rc_move_sale_id': fields.many2one('account.move', 'Registrazione vendita'),
        'rc_move_purchase_id': fields.many2one('account.move', 'Registrazione acquisto'),
        'rc_invoice_id': fields.many2one('account.invoice', 'Autofattura'),
    }

    def action_move_create(self, cr, uid, ids, context=None):
        res = super(invoice, self).action_move_create(cr,uid, ids, context=context)
        rc_inv = self.search(cr, uid, [('id', 'in', ids), ('journal_id.reverse_charge', '=', True)], context=context)
        self.generate_rc_moves(cr, uid, rc_inv, context=context)
        return res

    def copy_data(self, cr, uid, id, default=None, context=None):
        default = default or {}
        reset = ['reverse_charge', 'rc_move_sale_id', 'rc_move_purchase_id', 'rc_invoice_id']
        default.update(dict.fromkeys(reset, False))
        return super(invoice, self).copy_data(cr, uid, id, default=default, context=context)


    def generate_rc_moves(self, cr, uid, ids, context=None):
        for inv in self.browse(cr, uid, ids, context=None):
            assert inv.type in ('in_invoice', 'in_refund')
            purchase, sale = self.rc_generate_from_invoice(cr, uid, inv.id, context=context)

            # save the references to the moves
            sale = self.browse(cr, uid, sale, context=context)
            purchase = self.browse(cr, uid, purchase, context=context)

            self.write(cr, uid, inv.id, {
                'reverse_charge': True,
                'rc_move_sale_id': sale.move_id.id,
                'rc_move_purchase_id': purchase.move_id.id,
                'rc_invoice_id': sale.id if inv.journal_id.rc_type == 'self' else False,
            })

            if inv.journal_id.rc_type == 'self':
                self.rc_delete(cr, uid, [purchase.id], context=context)
            else:
                self.rc_delete(cr, uid, [purchase.id, sale.id], context=context)

        return True

    def rc_map_copy_data(self, data, taxes):
        data = copy.deepcopy(data)
        purchase = data['type'] in ('in_invoice', 'in_refund')
        def map_tax(tax):
            tax = taxes[tax]
            if tax.reverse_charge:
                return tax.rc_purchase_tax_id.id if purchase else tax.rc_sale_tax_id.id
            return tax.id

        for line in data['invoice_line']:
            line = line[2]
            line['account_id'] = data['account_id']
            line['invoice_line_tax_id'] = [(6, 0, [map_tax(t) for t in line['invoice_line_tax_id'][0][2]])]

        return data

    def rc_read_taxes(self, cr, uid, data, context=None):
        taxes = []
        for line in data['invoice_line']:
            for tx in line[2]['invoice_line_tax_id']:
                taxes.extend(tx[2])
        taxes = self.pool.get('account.tax').browse(cr, uid, taxes, context=context)
        taxes = {t.id: t for t in taxes}
        return taxes


    def rc_data_purchase(self, inv, data):
        data.update({
            'journal_id': inv.journal_id.rc_purchase_journal_id.id,
        })
        return data



    def rc_data_sale(self, inv, data):
        if inv.journal_id.rc_type == 'self':
            partner = inv.company_id.partner_id
        else:
            partner = inv.partner_id
        data.update({
            'journal_id': inv.journal_id.rc_sale_journal_id.id,
            'partner_id': partner.id,
            'account_id': partner.property_account_receivable.id,
            'type': 'out_invoice' if inv.type == 'in_invoice' else 'out_refund',
        })
        return data

    def rc_generate_from_invoice(self, cr, uid, id, context=None):
        data = self.copy_data(cr, uid, id, default={
            'state':'draft',
            'number':False,
            'move_id':False,
            'move_name':False,
            'internal_number': False,
            'sent': False,
        }, context=context)
        invo = self.pool.get('account.invoice')
        taxes = self.rc_read_taxes(cr, uid, data, context=context)
        inv = self.browse(cr, uid, id, context=context)
        data_purchase = self.rc_data_purchase(inv, data)
        data_purchase = self.rc_map_copy_data(data_purchase, taxes)
        data_sale = self.rc_data_sale(inv, data)
        data_sale = self.rc_map_copy_data(data_sale, taxes)
        purchase = invo.create(cr, uid, data_purchase, context=context)
        sale = invo.create(cr, uid, data_sale, context=context)
        invo.copy_translations(cr, uid, id, purchase, context=context)
        invo.copy_translations(cr, uid, id, sale, context=context)
        self.button_reset_taxes(cr, uid, [sale, purchase], context=context)
        wf = LocalService('workflow')
        for id in [sale, purchase]:
            wf.trg_validate(uid, 'account.invoice', id, 'invoice_open', cr)
        self.rc_reconcile(cr, uid, [sale, purchase], context=context)
        return purchase, sale

    def rc_delete(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {
            'state': 'draft',
            'internal_number': False,
            'move_id': False,
        }, context=context)
        self.unlink(cr, uid, ids, context=context)

    def rc_reconcile(self, cr, uid, ids, context=None):
        ml = self.pool.get('account.move.line')
        for inv in self.browse(cr, uid, ids, context=context):
            line_ids = ml.search(cr, uid, [
                ('move_id', '=', inv.move_id.id),
                ('account_id', '=', inv.account_id.id)
            ], context=context)
            ml.reconcile_partial(cr, uid, line_ids, context=context)




