from openerp.tests.common import TransactionCase
from testutils.factories.account import JournalFactory

class TestJournal(TransactionCase):
    journalf = JournalFactory()

    def test_reverse_charge_needs_journals(self):
        c = lambda: self.journalf.create({
         'reverse_charge': True,
        })
        self.assertRaises(Exception, c)
