from openerp.tests.common import TransactionCase
from testutils.factories.account import TaxFactory


class TestTax(TransactionCase):
    taxf = TaxFactory()

    def test_tax_is_not_applicable(self):
        tx = self.taxf.create({
            'rc_sale_tax_id': self.taxf.create()
        })
        assert not tx.wobj._applicable([tx.browse], 0, None, None)
