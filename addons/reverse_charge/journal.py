from openerp.osv import orm, fields


class journal(orm.Model):
    _inherit = 'account.journal'
    _columns = {
        'reverse_charge': fields.boolean('Gen. automatica autofattura'),
        'rc_sale_journal_id': fields.many2one('account.journal', 'Sezionale autofatture',
                                              domain=[('type', '=', 'sale')]),
        'rc_purchase_journal_id': fields.many2one('account.journal', 'Sezionale reg. acquisti',
                                                   domain=[('type', '=', 'purchase')]),
        'rc_type': fields.selection([('reverse', 'Reverse charge'),
                                     ('self', 'Autofattura')], string='Tipo autofattura',),
    }

    _sql_constraints = [
        (
            'rc_needs_journal_and_type',
            "CHECK (NOT reverse_charge OR (rc_sale_journal_id IS NOT NULL AND"
            "                              rc_purchase_journal_id IS NOT NULL AND"
            "                              rc_type IS NOT NULL))",
            'Per i sezionali autofattura, vanno specificati i sezionali ed il tipo.'
        )
    ]


