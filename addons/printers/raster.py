import subprocess
import tempfile

def rasterize(fname):
    n = tempfile.NamedTemporaryFile('r', delete=False)
    n.close()
    args = ['pdftops', fname, n.name]
    subprocess.call(args)
    return n.name
