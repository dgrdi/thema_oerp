from openerp.osv import orm, fields, osv

class res_user(orm.Model):
    _inherit = 'res.users'

    _columns = {
        'default_printer': fields.selection(lambda s, *a, **k: s._printer_sel(*a, **k), 'Stampante predefinita'),
    }

    def _printer_sel(self, cr, uid, context=None):
        try:
            pr = self.pool.get('cups.server').get_printers(cr, uid, context=context)
        except:
            return []
        return [(name, '%s %s' % (p['printer-info'], p['printer-location'])) for name, p in pr.items()]
