# coding=utf-8
import cups
from openerp.osv import orm, fields, osv
from .printer import Printer
from openerp.tools import ormcache
from openerp import SUPERUSER_ID
import tempfile
from StringIO import StringIO
from .raster import rasterize
import os

class cups_server(orm.Model):
    _name = 'cups.server'

    _columns = {
        'name': fields.char('Nome', required=True),
        'address': fields.char('Indirizzo', required=True),
        'port': fields.integer('Porta'),
        'default': fields.boolean('Default'),
    }

    _defaults = {
        'port': 631
    }

    _constraints = [
        (lambda s, *a, **k: s._check_default(*a, **k), u'Solo un server può essere predefinito', ['default']),
    ]

    def _check_default(self, cr, uid, ids, context=None):
        df = self.search(cr, uid, [('default', '=', True)], context=context)
        if len(df) > 1:
            return False
        return True

    def _get_default(self, cr, uid, context=None):
        try:
            server_id = self.search(cr, uid, [('default', '=', True)], context=context)[0]
        except IndexError:
            raise osv.except_osv('Attenzione!', u'Nessun server di stampa e` impostato come predefinito.')
        return server_id

    def _get_connection(self, cr, uid, server_id=None, context=None):
        if server_id is None:
            server_id = self._get_default(cr, uid, context=context)
        srv = self.browse(cr, uid, server_id, context=context)
        cups.setServer(srv.address)
        cups.setPort(srv.port)
        return cups.Connection()

    def get_printers(self, cr, uid, server_id=None, context=None):
        cn = self._get_connection(cr, uid, server_id, context=context)
        return {name: Printer(cn, name, pdef) for name, pdef in cn.getPrinters().items()}

    def print_buffer(self, cr, uid, buf, printer_name=None, options=None, server_id=None, context=None):
        options = options or {}
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        if printer_name is None:
            printer_name = user.default_printer
            if not printer_name:
                raise osv.except_osv('Attenzione!', 'Nessuna stampante predefinita esiste per l\'utente.')
        cn = self._get_connection(cr, uid, server_id, context=context)
        if isinstance(buf, str):
            buf = StringIO(buf)
        tf = tempfile.NamedTemporaryFile('wb', delete=False)
        tf.write(buf.read())
        tf.close()
        rf = rasterize(tf.name)
        try:
            return cn.printFile(printer_name, rf, '%s job' % user.name, options)
        except:
            return False
        finally:
            os.remove(tf.name)
            os.remove(rf)

