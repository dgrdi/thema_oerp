from openerp.osv import orm, fields, osv
from openerp.netsvc import LocalService


class report_xml(orm.Model):
    _inherit = 'ir.actions.report.xml'

    def get_pdf(self, cr, uid, id, record_ids, context=None):
        context = context or {'lang': 'en_US'}
        rp = self.browse(cr, uid, id, context=context)
        datas = {
            'ids': record_ids,
            'report_type': 'pdf',
            'model': rp.model,
        }
        srv = LocalService('report.%s' % rp.report_name)
        return srv.create(cr, uid, record_ids, datas, context=context)[0]

    def print_(self, cr, uid, id, record_ids, server_id=None, printer_name=None, options=None, context=None):
        pdf = self.get_pdf(cr, uid, id, record_ids, context=context)
        return self.pool.get('cups.server').print_buffer(cr, uid, pdf, printer_name=printer_name,
                                                         server_id=server_id, options=options, context=context)

