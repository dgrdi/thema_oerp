{
    'name': 'Printers',
    'version': '0.2',
    'category': 'Printer support',
    'description': """Support for direct printing.""",
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': ['base'],
    'init_xml': [],
    'data':[
        'printers.xml',
        ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': True,
}
