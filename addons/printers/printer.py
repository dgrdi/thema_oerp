import cups

class Printer(dict):

    def __init__(self, connection, name, properties):
        self.name = name
        self.connection = connection
        super(Printer, self).__init__(properties)
        unpak = lambda l: {c['choice']: c for c in l}
        try:
            self['paper_sizes'] = unpak(self.ppd.findOption('PageSize').choices)
        except:
            self['paper_sizes'] = {}
        try:
            self['input_slots'] = unpak(self.ppd.findOption('InputSlot').choices)
        except:
            self['input_slots'] = {}

    _ppd = None

    @property
    def ppd(self):
        if self._ppd is None:
            self._ppd = cups.PPD(self.connection.getPPD(self.name))
        return self._ppd


