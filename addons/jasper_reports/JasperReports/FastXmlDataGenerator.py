import logging
from xml.sax.saxutils import XMLGenerator
from .fast import FastGenerator

logger = logging.getLogger(__name__)


class FastXmlDataGenerator(FastGenerator):
    def generate(self, filename):
        read_graph = self.build_read_graph(self.report, self.model)
        render_graph = self.build_render_graph(self.report)
        definition = self.build_definition(render_graph, read_graph, self.model)
        data = self.read(read_graph)
        fo = open(filename, 'wb')
        try:
            generator = XMLGenerator(out=fo, encoding='utf-8')
            generator.startDocument()
            generator.startElement('data', {})
            for id in self.ids:
                generator.startElement('record', {})
                self.build_record(generator, definition, data, id)
                generator.endElement('record')
            generator.endElement('data')
            generator.endDocument()
        finally:
            fo.close()

    def build_record(self, generator, definition, data, id):
        for mod, name, type, rel in definition:
            generator.startElement(name, {})
            val = data[mod][id][name]
            if type == 'many2one' and val:
                self.build_record(generator, rel, data, val[0])
            elif type in ('one2many', 'many2many') and val:
                generator.startElement('data', {})
                for i in val:
                    generator.startElement('record', {})
                    self.build_record(generator, rel, data, i)
                    generator.endElement('record')
                generator.endElement('data')
            else:
                generator.characters(self.format(type, val))
            generator.endElement(name)

    def format(self, type, value):
        if isinstance(value, dict):
            return u'|'.join(u'{lang}~{val}'.format(lang=lang, val=self.format(type, val)) \
                             for lang, val in value.items())
        return super(FastXmlDataGenerator, self).format(type, value)
