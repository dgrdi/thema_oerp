# #############################################################################
#
# Copyright (c) 2008-2012 NaN Projectes de Programari Lliure, S.L.
# http://www.NaN-tic.com
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# #############################################################################

import os
from lxml import etree
import re
from openerp.tools.safe_eval import safe_eval
import openerp.tools
import logging

logger = logging.getLogger(__name__)
dsourceRexp = re.compile(r""".*\$P\{REPORT_DATA_SOURCE\}.*""", re.MULTILINE)


class JasperReport(object):
    NS = 'http://jasperreports.sourceforge.net/jasperreports'
    NSS = {'jr': NS}

    def __init__(self, fileName='', pathPrefix='', tag=None):
        self.reportPath = fileName
        self.pathPrefix = pathPrefix.strip()
        if self.pathPrefix and self.pathPrefix[-1] != '/':
            self.pathPrefix += '/'
        self.language = 'xpath'
        self.relations = []
        self.fields = {}
        self.fieldNames = []
        self.subreports = []
        self.datasets = []
        self.copies = 1
        self.model = None
        self.copiesField = False
        self.isHeader = False
        if fileName:
            self.extractAll()

    @property
    def standardDirectory(self):
        jasperdir = openerp.tools.config.get('jasperdir')
        if jasperdir:
            if jasperdir.endswith(os.sep):
                return jasperdir
            else:
                return os.path.join(jasperdir, '')
        return os.path.join(os.path.abspath(os.path.dirname(__file__)), '../', 'report', '')

    @property
    def subreportDirectory(self):
        return os.path.join(os.path.abspath(os.path.dirname(self.reportPath)), '')

    def extractAll(self):
        doc = etree.parse(self.reportPath)
        for k, v in self.extractProperties(doc.xpath('/jr:jasperReport', namespaces=self.NSS)[0]).items():
            setattr(self, k, v)
        fieldTags = doc.xpath('jr:field', namespaces=self.NSS)
        self.fields, self.fieldNames = self.extractFields(fieldTags)

        for tag in doc.xpath('//jr:subreport', namespaces=self.NSS):
            self.subReport(tag)
        for tag in doc.xpath('//jr:datasetRun', namespaces=self.NSS):
            subrep = JasperReport()
            for k, v in self.extractProperties(tag.xpath('../../jr:reportElement', namespaces=self.NSS)[0]).items():
                setattr(subrep, k, v)
            fieldTags = doc.xpath('//jr:subDataset[@name="%s"]//jr:field' % tag.get('subDataset'), namespaces=self.NSS)
            subrep.fields, subrep.fieldNames = self.extractFields(fieldTags)
            self.subreports.append({
                'model': subrep.model,
                'pathPrefix': subrep.pathPrefix,
                'report': subrep,
                'filename': 'DATASET',
            })


    def extractFields(self, fieldTags):
        # fields and fieldNames
        fields = {}
        fieldNames = []
        for tag in fieldTags:
            name = tag.get('name')
            type = tag.get('class')
            path = tag.findtext('{%s}fieldDescription' % self.NS, '').strip()
            # Make the path relative if it isn't already
            if path.startswith('/data/record/'):
                path = self.pathPrefix + path[13:]
            # Remove language specific data from the path so:
            # Empresa-partner_id/Nom-name becomes partner_id/name
            # We need to consider the fact that the name in user's language
            # might not exist, hence the easiest thing to do is split and [-1]
            newPath = []
            for x in path.split('/'):
                newPath.append(x.split('-')[-1])
            path = '/'.join(newPath)
            if path in fields:
                logger.warning("WARNING: path '%s' already exists in report. This is not supported by the module. "
                               "Offending fields: %s, %s" % (path, fields[path]['name'], name))
            fields[path] = {
                'name': name,
                'type': type,
            }
            fieldNames.append(name)
        return fields, fieldNames

    def extractProperties(self, tag):
        # The function will read all relevant information from the jrxml file

        # Language

        # Note that if either queryString or language do not exist the default (from the constructor)
        # is XPath.
        props = {}
        langTags = tag.xpath('jr:queryString', namespaces=self.NSS)
        if langTags:
            if langTags[0].get('language'):
                props['language'] = langTags[0].get('language').lower()

        # Relations
        relationTags = tag.xpath('jr:property[@name="OPENERP_RELATIONS"]', namespaces=self.NSS)
        if relationTags and 'value' in relationTags[0].keys():
            relation = relationTags[0].get('value').strip()
            if relation.startswith('['):
                props['relations'] = safe_eval(relationTags[0].get('value'), {})
            else:
                props['relations'] = [x.strip() for x in relation.split(',')]
            props['relations'] = [self.pathPrefix + x for x in self.relations]

        # Repeat field
        copiesFieldTags = tag.xpath('jr:property[@name="OPENERP_COPIES_FIELD"]', namespaces=self.NSS)
        if copiesFieldTags and 'value' in copiesFieldTags[0].keys():
            props['copiesField'] = self.pathPrefix + copiesFieldTags[0].get('value')

        # Repeat
        copiesTags = tag.xpath('jr:property[@name="OPENERP_COPIES"]', namespaces=self.NSS)
        if copiesTags and 'value' in copiesTags[0].keys():
            props['copies'] = int(copiesTags[0].get('value'))

        props['isHeader'] = False
        headerTags = tag.xpath('jr:property[@name="OPENERP_HEADER"]', namespaces=self.NSS)
        if headerTags and 'value' in headerTags[0].keys():
            props['isHeader'] = True

        # Model
        props['model'] = None
        modelTags = tag.xpath('jr:property[@name="OPENERP_MODEL"]', namespaces=self.NSS)
        if modelTags and 'value' in modelTags[0].keys():
            props['model'] = modelTags[0].get('value')

        pathPrefixTags = tag.xpath('jr:property[@name="OPENERP_PATH_PREFIX"]', namespaces=self.NSS)
        if pathPrefixTags and 'value' in pathPrefixTags[0].keys():
            props['pathPrefix'] = pathPrefixTags[0].get('value')

        return props

    def testDataSource(self, tag):
        dsource = tag.findtext('{%s}dataSourceExpression' % self.NS, '')
        if not dsource or not dsourceRexp.match(dsource):
            return False
        return True

    def subReport(self, tag):

        if not self.testDataSource(tag):
            return

        subreportExpression = tag.findtext('{%s}subreportExpression' % self.NS, '')
        if not subreportExpression:
            return

        subreportExpression = subreportExpression.strip()
        subreportExpression = subreportExpression.replace('$P{STANDARD_DIR}', '"%s"' % self.standardDirectory)
        subreportExpression = subreportExpression.replace('$P{SUBREPORT_DIR}', '"%s"' % self.subreportDirectory)

        try:
            subreportExpression = safe_eval(subreportExpression, {})
        except:
            logger.warning("COULD NOT EVALUATE EXPRESSION: '%s'", subreportExpression)
            # If we're not able to evaluate the expression go to next subreport
            return
        if subreportExpression.endswith('.jasper'):
            subreportExpression = subreportExpression[:-6] + 'jrxml'

        props = self.extractProperties(tag.xpath('jr:reportElement', namespaces=self.NSS)[0])

        # Add our own pathPrefix to subreport's pathPrefix
        if 'pathPrefix' in props:
            subPrefix = '/'.join([self.pathPrefix, props['pathPrefix']])
        else:
            subPrefix = self.pathPrefix

        subreport = JasperReport(subreportExpression, subPrefix)
        self.subreports.append({
            'filename': subreportExpression,
            'model': props['model'],
            'pathPrefix': props.get('pathPrefix', ''),
            'report': subreport,
            'depth': 1,
        })


# vim:noexpandtab:smartindent:tabstop=8:softtabstop=8:shiftwidth=8:
