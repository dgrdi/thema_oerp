from .fast import FastGenerator
from struct import pack
#import os
#os.environ['PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION'] = 'cpp'
from .message_pb2 import Record, Data, FieldDefinition

class ProtocolBufferGenerator(FastGenerator):

    TYPE_MAP = {
        'char': FieldDefinition.CHAR,
        'boolean': FieldDefinition.BOOLEAN,
        'date': FieldDefinition.DATE,
        'timestamp': FieldDefinition.TIMESTAMP,
        'integer': FieldDefinition.INTEGER,
        'float': FieldDefinition.FLOAT,
        'binary': FieldDefinition.BINARY,
        'one2many': FieldDefinition.MANY,
        'many2many': FieldDefinition.MANY,
        'many2one': FieldDefinition.ONE,
        'text': FieldDefinition.CHAR,
    }

    def generate(self, fileName):
        read_graph = self.build_read_graph(self.report, self.model)
        render_graph = self.build_render_graph(self.report)
        df = self.build_definition(render_graph, read_graph, self.model)
        translatable = self.get_translatable_fields(read_graph)
        data = self.read(read_graph)
        root = Data()
        for mod, name, type, rel in df:
            field = root.fields.add()
            self.build_field_def(translatable, mod, field, name, type, rel)
        for id in self.ids:
            rec = root.records.add()
            self.build_record(translatable, rec, df, data, id)
        with open(fileName, 'wb') as fo:
            fo.write(root.SerializeToString())

    def build_field_def(self, translatable, model, field, name, type, rel):
        if name in translatable.get(model, []):
            field.type = FieldDefinition.MULTILANG
        else:
            field.type = self.TYPE_MAP.get(type, FieldDefinition.CHAR)
        field.name = name
        if rel is not None:
            for mod, name, type, rel in rel:
                subf = field.related.add()
                self.build_field_def(translatable, mod, subf, name, type, rel)

    def build_record(self, translatable, record, definition, data, id):
        for mod, name, type, rel in definition:
            field = record.fields.add()
            val = data[mod][id][name]
            if isinstance(val, dict) and name in translatable.get(mod, []):
                for lang, langval in val.items():
                    ldef = field.languages.add()
                    ldef.language = lang
                    ldef.data = langval
            elif type == 'many2one' and val:
                rec = field.related.add()
                self.build_record(translatable, rec, rel, data, val[0])
            elif type in ('one2many', 'many2many') and val:
                for i in val:
                    rec = field.related.add()
                    self.build_record(translatable, rec, rel, data, i)
            else:
                field.data = self.format(type, val)
