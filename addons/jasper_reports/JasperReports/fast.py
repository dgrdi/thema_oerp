from operator import itemgetter
from .AbstractDataGenerator import AbstractDataGenerator
from itertools import ifilter

REL_TYPES = ('many2many', 'many2one', 'one2many')


class FastGenerator(AbstractDataGenerator):
    def __init__(self, report, model, pool, cr, uid, ids, context):
        self.report = report
        self.model = model
        self.pool = pool
        self.cr = cr
        self.uid = uid
        self.ids = ids
        self.context = context
        self._languages = []
        self.imageFiles = {}
        self.temporaryFiles = []


    def build_read_graph(self, report, model, graph=None):
        """
        :param report: a JasperReport Instance
        :param model: the report model
        :param graph: the return value, used in recursive calls
        :return: a map from model names to dicts that map the model's field names to their column objects. (for the 'id'
        field there is no column object, so it is mapped to None).
        """
        graph = graph or {}
        for field in report.fields:
            obj = self.pool.get(model)
            for piece in field.split('/'):
                if piece == 'id':
                    graph.setdefault(obj._name, {})[piece] = None
                    continue
                try:
                    col_obj = obj._all_columns[piece].column
                except KeyError:
                    if piece == 'company_id':
                        obj = self.pool.get('company_id')
                        graph.setdefault('res.company', {})
                    continue
                graph.setdefault(obj._name, {})[piece] = col_obj
                if col_obj._type in REL_TYPES:
                    obj = self.pool.get(col_obj._obj)

        obj = self.pool.get(model)
        for subreport in report.subreports:
            prefix = subreport.get('pathPrefix')
            if not prefix:
                self.build_read_graph(subreport['report'], model, graph)
                continue
            try:
                rel = obj._all_columns[prefix].column
            except KeyError:
                if prefix == 'company_id':
                    self.build_read_graph(subreport['report'], 'res.company', graph)
                continue
            graph.setdefault(model, {}).update({prefix: rel})
            self.build_read_graph(subreport['report'], rel._obj, graph)
        return graph

    def read(self, graph):
        """
        :param graph: from self.build_read_graph
        :return: a dict mapping model names to other dict, that map record IDS to the result of the records' orm.read
        value.
        """
        data = {}
        todo = {self.model: self.ids}
        while todo:
            model = todo.keys()[0]
            ids = todo.pop(model)
            rx = self.pool.get(model).read(self.cr, self.uid, ids, fields=graph[model].keys(), context=self.context)
            rx = self.map_selections(graph, model, rx, self.context and self.context.get('lang') or False)
            data.setdefault(model, {}).update({el['id']: el for el in rx})
            rel_fields = [(f, c) for f, c in graph[model].items() if c and c._type in REL_TYPES]
            for f, c in rel_fields:
                for el in ifilter(itemgetter(f), rx):
                    if c._type == 'many2one' and el[f][0] not in data.get(c._obj, set()):
                        todo.setdefault(c._obj, set()).add(el[f][0])
                    elif c._type != 'many2one':
                        ids = [id for id in el[f] if id not in data.get(c._obj, set())]
                        if ids:
                            todo.setdefault(c._obj, set()).update(ids)
        data = self.read_translations(self.report, self.model, graph, data)
        return data

    def languages(self):
        if self._languages:
            return self._languages
        ids = self.pool.get('res.lang').search(self.cr, self.uid, [('translatable', '=', '1')])
        self._languages = self.pool.get('res.lang').read(self.cr, self.uid, ids, ['code'])
        self._languages = [x['code'] for x in self._languages]
        return self._languages

    def get_translatable_fields(self, graph):
        def all_fields(report, model):
            fs = [f for f, d in report.fields.items() if d['type'] == 'java.lang.Object']
            for sub in report.subreports:
                pp = sub.get('pathPrefix', None)
                mod = graph[model][pp]._obj if pp else model
                pp = pp + '/' if pp else ''
                fs.extend(pp + el for el in all_fields(sub['report'], mod))
            return fs
        todo = {}
        for field in all_fields(self.report, self.model):
            col = None
            obj = self.model
            for piece in field.split('/'):
                try:
                    col = graph[obj][piece]
                except KeyError:
                    break
                if col and col._obj:
                    obj = col._obj
            if col and (col.translate or col._type == 'selection'):
                todo.setdefault(obj, set()).add(piece)
        return todo


    def read_translations(self, report, model, graph, data, done=None):
        """
        Reads translations for field values, according to the report definition.
        :param report: JasperReport instance
        :param mod: the report model
        :param graph: from self.build_read_graph
        :param data: from self.read
        :param done: a map from model names to ids, that represents records for which the translations
        have already been loaded.
        :return: data, with translatable fields transformed to the format that the MultiLanguageDataSource expects.
        """
        done = done or {}
        ctx = self.context or {}
        ctx = ctx.copy()
        todo = self.get_translatable_fields(graph)
        orig_lang = self.context.get('lang')
        for mod, fields in todo.items():
            ids = set(data.get(mod, {}).keys()) - done.get(mod, set())
            if not ids:
                continue
            lang_data = {}
            for lang in self.languages():
                if lang == orig_lang:
                    continue
                ctx['lang'] = lang
                res = self.pool.get(mod).read(self.cr, self.uid, ids, fields=fields, context=ctx)
                res = self.map_selections(graph, mod, res, lang)
                lang_data[lang] = {el['id']: el for el in res}
            for id, v in data[mod].iteritems():
                for f in fields:
                    fdata = {lang: lang_data[lang][id][f] for lang in lang_data.keys()}
                    if orig_lang:
                        fdata[orig_lang] = v[f]
                    if isinstance(v[f], dict):
                        for val in fdata.values():
                            v[f].update(val)
                    else:
                        v[f] = fdata
            done.setdefault(mod, set()).update(ids)
        for subreport in report.subreports:
            pp = subreport.get('pathPrefix')
            obj = graph[model][subreport['pathPrefix']]._obj if pp else model
            self.read_translations(subreport['report'], obj, graph, data, done=done)
        return data

    def map_selections(self, graph, model, data, lang):
        """
        :param graph: from self.build_read_graph()
        :param data: a list of orm.read dicts
        :return: data, mapped so that selection fields have their human-readable value for the language lang.
        """
        if not data:
            return data
        sel_fields = [f for f in graph[model].keys() if graph[model][f] \
                      and graph[model][f]._type == 'selection' and f in data[0]]
        if sel_fields:
            context = self.context or {}
            context = context.copy()
            context['lang'] = lang
            field_data = self.pool.get(model).fields_get(self.cr, self.uid, allfields=sel_fields, context=context)
            for f in sel_fields:
                map = dict(field_data[f]['selection'])
                for el in data:
                    el[f] = {
                        'c': el[f],
                        lang: map[el[f]]
                    }
        return data

    def format(self, type, value):
        if type != 'boolean' and value is False:
            return u''
        if type == 'selection' and isinstance(value, dict):
            return u'%s' % value['c']
        if value is None:
            return u''
        if type == 'date':
            return u'%s 00:00:00' % value
        if type == 'float':
            return u'%.10f' % value
        if isinstance(value, str):
            return value.decode('utf-8')
        if not isinstance(value, unicode):
            return unicode(value)
        return value

    def build_render_graph(self, report):
        graph = {}
        for field in report.fields:
            node = graph
            for piece in field.split('/'):
                node = node.setdefault(piece, {})
        for subreport in report.subreports:
            pp = subreport.get('pathPrefix')
            subg = self.build_render_graph(subreport['report'])
            if pp:
                graph.setdefault(pp, {}).update(subg)
            else:
                graph.update(subg)
        return graph

    def build_definition(self, render_graph, read_graph, model):
        df = []
        for field, nodes in render_graph.items():
            if field == 'id':
                df.append((model, field, 'integer', None))
                continue
            col = read_graph[model].get(field)
            if not col:
                continue
            if col and col._type in REL_TYPES:
                nsdef = self.build_definition(nodes, read_graph, col._obj)
                df.append((model, field, col._type, nsdef))
            else:
                df.append((model, field, col._type, None))
        return df