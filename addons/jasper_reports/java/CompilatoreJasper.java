import com.nantic.jasperreports.Translator;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Hashtable;
import java.util.Locale;

/**
 * Created by federica on 07/04/14.
 */
public class CompilatoreJasper {


    public static void compile( String src, String dst )
    {
        File jrxmlFile;
        File jasperFile;
        try {
            System.setProperty("jasper.reports.compiler.class", "com.nantic.jasperreports.I18nGroovyCompiler");
            jrxmlFile = new File( src );
            jasperFile = new File( dst );
            if ( (! jasperFile.exists()) || (jrxmlFile.lastModified() > jasperFile.lastModified()) ) {
                JasperCompileManager.compileReportToFile( src, dst );

                Translator translator = null;
                JasperReport report = (JasperReport) JRLoader.loadObject(dst);

                Hashtable parameters =new Hashtable();
                parameters.put("REPORT_LOCALE", "it_IT");
                // Fill in report parameters
                JRParameter[] reportParameters = report.getParameters();
                for( int j=0; j < reportParameters.length; j++ ){
                    JRParameter jparam = reportParameters[j];
                    if ( jparam.getValueClassName().equals( "java.util.Locale" ) ) {
                        //REPORT_LOCALE
                        if ( ! parameters.containsKey( jparam.getName() ) )
                            continue;
                        String[] locales = ((String)parameters.get( jparam.getName() )).split( "_" );

                        Locale locale;
                        if ( locales.length == 1 )
                            locale = new Locale( locales[0] );
                        else
                            locale = new Locale( locales[0], locales[1] );

                        parameters.put( jparam.getName(), locale );

                        // Initialize translation system
                        // SQL reports will need to declare the TRANSLATOR paramter for translations to work.
                        // CSV/XML based ones will not need that because we will integrate the translator
                        // with the CsvMultiLanguageDataSource.
                        translator = new Translator( bundlePath(src), locale );
                        parameters.put( "TRANSLATOR", translator );

                    } else if( jparam.getValueClassName().equals( "java.lang.BigDecimal" )){
                        Object param = parameters.get( jparam.getName());
                        parameters.put( jparam.getName(), new BigDecimal( (Double) parameters.get(jparam.getName() ) ) );
                    }
                }

            }
        } catch (Exception e){
            e.printStackTrace();
            System.out.println( e.getMessage() );
        }
    }

    public static void main( String[] args )
    {
        if ( args.length == 2 )
            compile(args[0], args[1]);
        else
            if ( args.length == 1 )
                compile( args[0], args[0].substring(0, args[0].length()-5) + "jasper");
            else
                System.out.println("Two arguments needed. Example: java ReportCompiler src.jrxml dst.jasper");
    }

    /* Returns path where bundle files are expected to be */
    public static String bundlePath( String jrxmlPath ) {
        int index;
        index = jrxmlPath.lastIndexOf('.');
        if ( index != -1 )
            return jrxmlPath.substring( 0, index );
        else
            return jrxmlPath;
    }
}
