/*
Copyright (c) 2008-2012 NaN Projectes de Programari Lliure, S.L.
                        http://www.NaN-tic.com

WARNING: This program as such is intended to be used by professional
programmers who take the whole responsability of assessing all potential
consequences resulting from its eventual inadequacies and bugs
End users who are looking for a ready-to-use solution with commercial
garantees and support are strongly adviced to contract a Free Software
Service Company

This program is Free Software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
package com.nantic.jasperreports;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRAbstractTextDataSource;
import org.apache.xmlrpc.server.XmlRpcServer;
import org.apache.xmlrpc.webserver.WebServer;
//import org.apache.xmlrpc.webserver.*;
import org.apache.xmlrpc.server.PropertyHandlerMapping;
//import org.apache.xml.security.utils.Base64;

import net.sf.jasperreports.engine.util.JRLoader;

// Exporters
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
//Added by Prajul to print docx report
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
//End
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.JRTextExporterParameter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.oasis.JROdsExporter;

import java.lang.Object;
import java.util.HashMap;
import java.util.Hashtable;
import java.io.*;
import java.sql.*;
import java.lang.*;
import java.lang.Class;
import java.lang.Double;
import java.lang.Float;
import java.math.BigDecimal;
import java.io.InputStream;
import java.util.Locale;
import java.util.Enumeration;


public class JasperServer {
    public String mapPath(String path) {
        return path;
    }

    /* Compiles the given .jrxml (inputFile) */
    public Boolean compile(String jrxmlPath) throws java.lang.Exception {
        File jrxmlFile;
        File jasperFile;

        System.setProperty("jasper.reports.compiler.class", "com.nantic.jasperreports.I18nGroovyCompiler");

        jrxmlFile = new File(mapPath(jrxmlPath));
        jasperFile = new File(jasperPath(jrxmlPath));
        if ((!jasperFile.exists()) || (jrxmlFile.lastModified() > jasperFile.lastModified())) {
            JasperCompileManager.compileReportToFile(mapPath(jrxmlPath), jasperPath(jrxmlPath));
        }
        return true;
    }

    /* Returns path where bundle files are expected to be */
    public String bundlePath(String jrxmlPath) {
        int index;
        index = jrxmlPath.lastIndexOf('.');
        if (index != -1)
            return jrxmlPath.substring(0, index);
        else
            return jrxmlPath;
    }

    /* Returns the path to the .jasper file for the given .jrxml */
    public String jasperPath(String jrxmlPath) {
        return mapPath(bundlePath(jrxmlPath) + ".jasper");
    }

    public int execute(Hashtable connectionParameters, String jrxmlPath, String outputPath, Hashtable parameters, Hashtable properties) throws java.lang.Exception {
        try {
            return privateExecute(connectionParameters, jrxmlPath, outputPath, parameters, properties);
        } catch (Exception exception) {
            //exception.printStackTrace();
            throw exception;
        }
    }


    public HashMap getDBParams(Hashtable connectionParameters) {
        //Add DATABASE CONNECTION PARAMETERS
        HashMap<String, Object> dbp = new HashMap<String, Object>();
        dbp.put("DSN", connectionParameters.get("dsn"));
        dbp.put("USER", connectionParameters.get("user"));
        dbp.put("PASS", connectionParameters.get("password"));
        return dbp;
    }

    JRDataSource buildDataSource(Hashtable connectionParameters, Translator translator) throws Exception{
        JRDataSource dataSource = null;
        if (connectionParameters.containsKey("csv")) {
            String dFile = mapPath((String) connectionParameters.get("csv"));
            dataSource = new CsvMultiLanguageDataSource(dFile, "utf-8", translator);
        } else {
            if (connectionParameters.containsKey("xml")) {
                String dFile = mapPath((String) connectionParameters.get("xml"));
                dataSource = new XmlMultiLanguageDataSource((String) connectionParameters.get("xml"), "/data/record");
            }
            if (connectionParameters.containsKey("protoBuffer")) {
                String dFile = mapPath((String) connectionParameters.get("protoBuffer"));
                dataSource = new FileProtoBufferDataSource(dFile);
            }
            if (dataSource!= null) {
                ((JRAbstractTextDataSource) dataSource).setDatePattern("yyyy-MM-dd HH:mm:ss");
                ((JRAbstractTextDataSource) dataSource).setNumberPattern("#######0.##");
                ((JRAbstractTextDataSource) dataSource).setLocale(Locale.ENGLISH);
            }
        }
        if (dataSource == null) {
            throw new IllegalArgumentException("No data file was specified for either csv, xml, or protobuffer datasources.");
        }
        return dataSource;
    }

    public int privateExecute(Hashtable connectionParameters, String jrxmlPath, String outputPath, Hashtable parameters, Hashtable properties) throws java.lang.Exception {

        JasperReport report = null;
        byte[] result = null;
        JasperPrint jasperPrint = null;
        InputStream in = null;
        int index;

        System.out.println("executing JasperServer");
        // Ensure report is compiled
        compile(jrxmlPath);

        report = (JasperReport) JRLoader.loadObject(jasperPath(jrxmlPath));

        // Add SUBREPORT_DIR parameter
        index = jrxmlPath.lastIndexOf('/');
        if (index != -1)
            parameters.put("SUBREPORT_DIR", mapPath(jrxmlPath.substring(0, index + 1)));

        parameters.put("OERP_DATABASE", getDBParams(connectionParameters));


        // Declare it outside the parameters loop because we'll use it when we will create the data source.
        Translator translator = null;

        //Added by Prajul to pass properties from python report file
        Enumeration e = properties.keys();
        while (e.hasMoreElements()) {
            String prop_key = (String) e.nextElement();
            report.setProperty(prop_key, (String) properties.get(prop_key));
        }
        // Fill in report parameters
        parameters.put("CONNECTION_PARAMETERS", connectionParameters);


        JRParameter[] reportParameters = report.getParameters();
        for (int j = 0; j < reportParameters.length; j++) {
            JRParameter jparam = reportParameters[j];
            if (jparam.getValueClassName().equals("java.util.Locale")) {
                //REPORT_LOCALE
                if (!parameters.containsKey(jparam.getName()))
                    continue;
                String[] locales = ((String) parameters.get(jparam.getName())).split("_");

                Locale locale;
                if (locales.length == 1)
                    locale = new Locale(locales[0]);
                else
                    locale = new Locale(locales[0], locales[1]);

                parameters.put(jparam.getName(), locale);

                // Initialize translation system
                // SQL reports will need to declare the TRANSLATOR paramter for translations to work.
                // CSV/XML based ones will not need that because we will integrate the translator 
                // with the CsvMultiLanguageDataSource.
                translator = Translator.cached(bundlePath(jrxmlPath), locale);
                parameters.put("TRANSLATOR", translator);

            } else if (jparam.getValueClassName().equals("java.lang.BigDecimal")) {
                Object param = parameters.get(jparam.getName());
                parameters.put(jparam.getName(), new BigDecimal((Double) parameters.get(jparam.getName())));
                JasperReport rep;
            }
        }

        // Fill in report
        String language;
        if (report.getQuery() == null)
            language = "";
        else
            language = report.getQuery().getLanguage();

        if (language.equalsIgnoreCase("XPATH")) {
            // If available, use a CSV file because it's faster to process.
            // Otherwise we'll use an XML file.
            JRDataSource dataSource = buildDataSource(connectionParameters, translator);
            jasperPrint = JasperFillManager.fillReport(report, parameters, dataSource);
        } else if (language.equalsIgnoreCase("SQL")) {
            Connection connection = getConnection(connectionParameters);
            jasperPrint = JasperFillManager.fillReport(report, parameters, connection);
        } else {
            JREmptyDataSource dataSource = new JREmptyDataSource();
            jasperPrint = JasperFillManager.fillReport(report, parameters, dataSource);
        }

        // Create output file
        File outputFile = new File(mapPath(outputPath));
        JRAbstractExporter exporter;

        String output;
        if (connectionParameters.containsKey("output"))
            output = (String) connectionParameters.get("output");
        else
            output = "pdf";

        if (output.equalsIgnoreCase("html")) {
            exporter = new JRHtmlExporter();
            exporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.FALSE);
            exporter.setParameter(JRHtmlExporterParameter.HTML_HEADER, "");
            exporter.setParameter(JRHtmlExporterParameter.BETWEEN_PAGES_HTML, "");
            exporter.setParameter(JRHtmlExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporter.setParameter(JRHtmlExporterParameter.HTML_FOOTER, "");
        } else if (output.equalsIgnoreCase("csv")) {
            exporter = new JRCsvExporter();
        } else if (output.equalsIgnoreCase("xls")) {
            exporter = new JRXlsExporter();
        } else if (output.equalsIgnoreCase("rtf")) {
            exporter = new JRRtfExporter();
        } else if (output.equalsIgnoreCase("odt")) {
            exporter = new JROdtExporter();
        } else if (output.equalsIgnoreCase("ods")) {
            exporter = new JROdsExporter();
        } else if (output.equalsIgnoreCase("txt")) {
            exporter = new JRTextExporter();
            exporter.setParameter(JRTextExporterParameter.PAGE_WIDTH, new Integer(80));
            exporter.setParameter(JRTextExporterParameter.PAGE_HEIGHT, new Integer(150));
        } else if (output.equalsIgnoreCase("docx")) {
            exporter = new JRDocxExporter();
        } else if (output.equalsIgnoreCase("xlsx")) {
            exporter = new JRXlsxExporter();
        } else {
            exporter = new JRPdfExporter();
        }
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRExporterParameter.OUTPUT_FILE, outputFile);
        exporter.exportReport();
        return jasperPrint.getPages().size();
    }

    public static Connection getConnection(Hashtable datasource) throws java.lang.ClassNotFoundException, java.sql.SQLException {
        Connection connection;
        Class.forName("org.postgresql.Driver");
        connection = DriverManager.getConnection((String) datasource.get("dsn"), (String) datasource.get("user"),
                (String) datasource.get("password"));
        connection.setAutoCommit(true);
        return connection;
    }

    static void startServer(String host, int port, Class serverClass) {
        try {
            java.net.InetAddress localhost = java.net.Inet4Address.getByName(host);
            System.out.println("JasperServer: Attempting to start XML-RPC Server at " + localhost.toString() + ":" + port + "...");
            WebServer server = new WebServer(port, localhost);
            XmlRpcServer xmlRpcServer = server.getXmlRpcServer();

            PropertyHandlerMapping phm = new PropertyHandlerMapping();
            phm.addHandler("Report", serverClass);
            xmlRpcServer.setHandlerMapping(phm);

            server.start();
            System.out.println("JasperServer: Started successfully.");
            System.out.println("JasperServer: Accepting requests. (Halt program to stop.)");
        } catch (Exception e) {
            System.err.println("Jasper Server: " + e);
        }
    }

    public static void main(String[] args) {
        int port = 8090;
        if (args.length == 1) {
            port = Integer.parseInt(args[0]);
        }
        startServer("localhost", port, JasperServer.class);
    }
}
