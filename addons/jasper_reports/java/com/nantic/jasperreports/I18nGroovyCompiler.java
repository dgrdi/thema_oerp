/*
Copyright (c) 2008-2012 NaN Projectes de Programari Lliure, S.L.
                        http://www.NaN-tic.com

WARNING: This program as such is intended to be used by professional
programmers who take the whole responsability of assessing all potential
consequences resulting from its eventual inadequacies and bugs
End users who are looking for a ready-to-use solution with commercial
garantees and support are strongly adviced to contract a Free Software
Service Company

This program is Free Software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
package com.nantic.jasperreports;

import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.design.JRCompilationUnit;
import net.sf.jasperreports.compilers.JRGroovyCompiler;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.design.JRSourceCompileTask;
import net.sf.jasperreports.engine.design.JRCompilationSourceCode;
import net.sf.jasperreports.engine.JRExpression;
import net.sf.jasperreports.engine.design.JRDefaultCompilationSourceCode;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.JRExpressionChunk;
import net.sf.jasperreports.engine.design.JRDesignExpressionChunk;
import net.sf.jasperreports.engine.JRReport;
import net.sf.jasperreports.engine.JasperReportsContext;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.List;

public class I18nGroovyCompiler extends JRGroovyCompiler {
    static public List sourceCodeList = null;
    static private String newImport = "import com.nantic.jasperreports.Translator;\nimport com.nantic.jasperreports.CsvMultiLanguageDataSource;\nimport net.sf.jasperreports.engine.JRDataSource;\nimport java.sql.*;\nimport java.lang.*;";
    static private String newVariable = "public Translator translator = null;\n";
    String newFunction;

    public I18nGroovyCompiler() throws IOException {
        super();
        newFunction = getGroovyDef();
    }

    public I18nGroovyCompiler(JasperReportsContext ctx) throws IOException {
        super(ctx);
        newFunction = getGroovyDef();
    }

    String getGroovyDef() throws IOException {
        /*
        URL url = I18nGroovyCompiler.class.getResource("groovy.txt");
        byte[] s = Files.readAllBytes(FileSystems.getDefault().getPath(url.getPath()));
        String part = new String(s, "UTF-8");
        return part;
        */
        return "public bundlePath(String path){\n" + 
        "    int index;\n" + 
        "    index = path.lastIndexOf('.');\n" + 
        "    if (index != -1)\n" + 
        "        return path.substring(0, index);\n" + 
        "    else\n" + 
        "        return path;\n" + 
        "}\n" + 
        "\n" + 
        "public Translator getTranslator() {\n" + 
        "    if (this.translator == null) {\n" + 
        "        translator = parameter_REPORT_PARAMETERS_MAP.getValue().get(\"TRANSLATOR\");\n" + 
        "        if (translator == null) {\n" + 
        "            translator = Translator.NULL;\n" + 
        "        }\n" + 
        "    }\n" + 
        "    return translator;\n" + 
        "}\n" + 
        "\n" + 
        "public String tr(Locale locale, String text) {\n" + 
        "	return getTranslator().tr(locale, text);\n" + 
        "}\n" + 
        "public String tr(Locale locale, String text, Object o) {\n" + 
        "	return getTranslator().tr(locale, text, o);\n" + 
        "}\n" + 
        "public String tr(Locale locale, String text, Object o1, Object o2) {\n" + 
        "	return getTranslator().tr(locale, text, o1, o2);\n" + 
        "}\n" + 
        "public String tr(Locale locale, String text, Object o1, Object o2, Object o3) {\n" + 
        "	return getTranslator().tr(locale, text, o1, o2, o3);\n" + 
        "}\n" + 
        "public String tr(Locale locale, String text, Object o1, Object o2, Object o3, Object o4) {\n" + 
        "	return getTranslator().tr(locale, text, o1, o2, o3, o4);\n" + 
        "}\n" + 
        "public String tr(Locale locale, String text, Object[] objects) {\n" + 
        "	return getTranslator().tr(locale, text, objects);\n" + 
        "}\n" + 
        "public String tr(String text) {\n" + 
        "	return getTranslator().tr(text);\n" + 
        "}\n" + 
        "public String tr(String text, Object o) {\n" + 
        "	return getTranslator().tr(text, o);\n" + 
        "}\n" + 
        "public String tr(String text, Object o1, Object o2) {\n" + 
        "	return getTranslator().tr(text, o1, o2);\n" + 
        "}\n" + 
        "public String tr(String text, Object o1, Object o2, Object o3) {\n" + 
        "	return getTranslator().tr(text, o1, o2, o3);\n" + 
        "}\n" + 
        "public String tr(String text, Object o1, Object o2, Object o3, Object o4) {\n" + 
        "	return getTranslator().tr(text, o1, o2, o3, o4);\n" + 
        "}\n" + 
        "public String tr(String text, Object[] objects) {\n" + 
        "	return getTranslator().tr(text, objects);\n" + 
        "}\n" + 
        "public String trn(Locale locale, String text, String pluralText, long n) {\n" + 
        "	return getTranslator().trn(locale, text, pluralText, n);\n" + 
        "}\n" + 
        "public String trn(Locale locale, String text, String pluralText, long n, Object o) {\n" + 
        "	return getTranslator().trn(locale, text, pluralText, n, o);\n" + 
        "}\n" + 
        "public String trn(Locale locale, String text, String pluralText, long n, Object o1, Object o2) {\n" + 
        "	return getTranslator().trn(locale, text, pluralText, n, o1, o2);\n" + 
        "}\n" + 
        "public String trn(Locale locale, String text, String pluralText, long n, Object o1, Object o2, Object o3) {\n" + 
        "	return getTranslator().trn(locale, text, pluralText, n, o1, o2, o3);\n" + 
        "}\n" + 
        "public String trn(Locale locale, String text, String pluralText, long n, Object o1, Object o2, Object o3, Object o4) {\n" + 
        "	return getTranslator().trn(locale, text, pluralText, n, o1, o2, o3, o4);\n" + 
        "}\n" + 
        "public String trn(Locale locale, String text, String pluralText, long n, Object[] objects) {\n" + 
        "	return getTranslator().trn(locale, text, pluralText, n, objects);\n" + 
        "}\n" + 
        "public String trn(String text, String pluralText, long n) {\n" + 
        "	return getTranslator().trn(text, pluralText, n);\n" + 
        "}\n" + 
        "public String trn(String text, String pluralText, long n, Object o) {\n" + 
        "	return getTranslator().trn(text, pluralText, n, o);\n" + 
        "}\n" + 
        "public String trn(String text, String pluralText, long n, Object o1, Object o2) {\n" + 
        "	return getTranslator().trn(text, pluralText, n, o1, o2);\n" + 
        "}\n" + 
        "public String trn(String text, String pluralText, long n, Object o1, Object o2, Object o3) {\n" + 
        "	return getTranslator().trn(text, pluralText, n, o1, o2, o3);\n" + 
        "}\n" + 
        "public String trn(String text, String pluralText, long n, Object o1, Object o2, Object o3, Object o4) {\n" + 
        "	return getTranslator().trn(text, pluralText, n, o1, o2, o3, o4);\n" + 
        "}\n" + 
        "public String trn(String text, String pluralText, long n, Object[] objects) {\n" + 
        "	return getTranslator().trn(text, pluralText, n, objects);\n" + 
        "}\n" + 
        "public String trl(String localeCode, String text) {\n" + 
        "	return getTranslator().trl(localeCode, text);\n" + 
        "}\n" + 
        "public String trl(String localeCode, String text, Object o) {\n" + 
        "	return getTranslator().trl(localeCode, text, o);\n" + 
        "}\n" + 
        "public String trl(String localeCode, String text, Object o1, Object o2) {\n" + 
        "	return getTranslator().trl(localeCode, text, o1, o2);\n" + 
        "}\n" + 
        "public String trl(String localeCode, String text, Object o1, Object o2, Object o3) {\n" + 
        "	return getTranslator().trl(localeCode, text, o1, o2, o3);\n" + 
        "}\n" + 
        "public String trl(String localeCode, String text, Object o1, Object o2, Object o3, Object o4) {\n" + 
        "	return getTranslator().trl(localeCode, text, o1, o2, o3, o4);\n" + 
        "}\n" + 
        "public String trl(String localeCode, String text, Object[] objects) {\n" + 
        "	return getTranslator().trl(localeCode, text, objects);\n" + 
        "}\n" + 
        "public String frd(String localeCode, String text) {\n" + 
        "	return getTranslator().frd(localeCode, text);\n" + 
        "}\n" + 
        "public String frd(String localeCode, java.util.Date val) {\n" + 
        "	return getTranslator().frd(localeCode, val);\n" + 
        "}\n" + 
        "public String frn(String localeCode, java.math.BigDecimal val) {\n" + 
        "	return getTranslator().frn(localeCode, val);\n" + 
        "}";
        
    }

    protected JRCompilationSourceCode generateSourceCode(JRSourceCompileTask sourceTask) throws JRException {
        JRCompilationSourceCode superCode = super.generateSourceCode(sourceTask);
        String code = superCode.getCode();
        String existingCode;

        existingCode = "import java.net";
        code = code.replace(existingCode, newImport + "\n" + existingCode);

        existingCode = "void customizedInit";
        code = code.replace(existingCode, newFunction + "\n\n" + existingCode);

        existingCode = "private JRFillParameter parameter_JASPER_REPORT = null;";
        code = code.replace(existingCode, existingCode + "\n" + newVariable + "\n");

        JRDesignExpression ee;
        JRExpression[] expressions = new JRExpression[sourceTask.getExpressions().size()];
        int i = -1;
        for (Object o : sourceTask.getExpressions()) {
            JRExpression e = (JRExpression) o;
            i++;

            ee = new JRDesignExpression();
            if (e.getValueClass() != null) {
                ee.setValueClass(e.getValueClass());
            }
            ee.setValueClassName(e.getValueClassName());
            ee.setText(e.getText().replaceAll("_\\(", "a("));
            ee.setId(e.getId());
            if (e.getChunks() != null) {
                for (Object chunk : e.getChunks()) {
                    JRDesignExpressionChunk newChunk = new JRDesignExpressionChunk();
                    newChunk.setType(((JRExpressionChunk) chunk).getType());
                    newChunk.setText(((JRExpressionChunk) chunk).getText());
                    ee.addChunk(newChunk);
                }
            }
            expressions[i] = ee;
        }
        JRDefaultCompilationSourceCode newCode = new JRDefaultCompilationSourceCode(code, expressions);
        // Store last generated source code so it can be extracted
        if (sourceCodeList != null)
            sourceCodeList.add((Object) code);
        return newCode;
    }

    protected void checkLanguage(String language) throws JRException {
        if (
                !JRReport.LANGUAGE_GROOVY.equals(language)
                        && !JRReport.LANGUAGE_JAVA.equals(language)
                        && !language.equals("i18ngroovy")
                ) {
            throw new JRException(
                    "Language \"" + language
                            + "\" not supported by this report compiler.\n"
                            + "Expecting \"i18ngroovy\", \"groovy\" or \"java\" instead."
            );
        }
    }
}
