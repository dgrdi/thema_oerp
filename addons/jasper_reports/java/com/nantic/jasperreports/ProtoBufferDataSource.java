package com.nantic.jasperreports;

import com.nantic.jasperreports.Message;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.data.JRAbstractTextDataSource;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.ArrayList;
import java.util.List;


public class ProtoBufferDataSource extends JRAbstractTextDataSource implements SubDataSource {
    Translator translator;
    Message.Record record;
    List<Message.FieldDefinition> fields;
    HashMap<String, HashMap<String, int[]>> allFieldPaths;
    HashMap<String, HashMap<String, Message.FieldDefinition>> allFieldDefinitions;

    HashMap<String, int[]> fieldPaths;
    HashMap<String, Message.FieldDefinition> fieldDefinitions;

    List<Message.Record> records;
    int index = -1;
    int size;
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String prefix = "";
    HashMap<String, String> subPaths = new HashMap<String, String>();

    ProtoBufferDataSource() {
        this.setDatePattern("yyyy-MM-dd HH:mm:ss");
        this.setNumberPattern("#######0.##");
        this.setLocale(Locale.ENGLISH);
    }

    void init(List<Message.FieldDefinition> fields, List<Message.Record> records,
              String prefix, HashMap allFieldDefinitions, HashMap allFieldPaths) {
        this.fields = fields;
        this.records = records;
        this.prefix = prefix;
        this.size = records.size();
        this.allFieldDefinitions = allFieldDefinitions;
        this.allFieldPaths = allFieldPaths;
        if (!allFieldDefinitions.containsKey(prefix)) {
            allFieldDefinitions.put(prefix, new HashMap());
        }
        if (!allFieldPaths.containsKey(prefix)) {
            allFieldPaths.put(prefix, new HashMap());
        }
        this.fieldDefinitions = this.allFieldDefinitions.get(prefix);
        this.fieldPaths = this.allFieldPaths.get(prefix);
    }

    public boolean next() throws JRException {
        index += 1;
        if (index >= size) {
            return false;
        }
        record = records.get(index);
        return true;
    }

    public void moveFirst() throws JRException {
        index = -1;
        record = null;
    }

    private int[] getFieldPath(String fieldDes) throws JRException {
        List<Message.FieldDefinition> definitions = this.fields;
        String key = fieldDes;
        if (fieldPaths.get(fieldDes) == null) {
            Message.FieldDefinition lastDef = null;
            int i;
            LinkedList path = new LinkedList<Integer>();
            if (fieldDes.startsWith("/data/record/")) fieldDes = fieldDes.substring(13);
            for (String piece : fieldDes.split("/")) {
                piece = piece.split("-")[0];

                for (i = 0; i < definitions.size(); i++) {
                    if (definitions.get(i).getName().equals(piece)) {
                        break;
                    }
                }
                if (i == definitions.size()) {
                    path.clear();
                    break;
                }
                path.add(i);
                lastDef = definitions.get(i);
                definitions = lastDef.getRelatedList();
            }
            if (path.size() > 0) {
                fieldDefinitions.put(key, lastDef);
            }

            int[] arpath = new int[path.size()];
            i = 0;
            for (Integer j : (List<Integer>) path) {
                arpath[i] = j.intValue();
                i++;
            }
            fieldPaths.put(key, arpath);
        }
        return fieldPaths.get(key);

    }

    Message.Field getField(int[] path) {
        Message.Record rec = this.record;
        Message.Field field = null;
        for (int i : path) {
            if (rec == null) return null;
            field = rec.getFields(i);
            if (field.getRelatedCount() > 0) {
                rec = field.getRelated(0);
            } else {
                rec = null;
            }
        }
        return field;
    }

    void checkType(JRField field, Class expected) throws JRException {
        if (!field.getValueClass().isAssignableFrom(expected)) {
            throw new JRException("Field " + field.getName() + " is of type " + expected.getName() +
                    " but it is declared as " + field.getValueClass().getName() + "!");
        }
    }

    public Object getValue(JRField field, Message.Field value) throws JRException {
        switch (fieldDefinitions.get(field.getDescription()).getType()) {
            case MULTILANG:
                checkType(field, LanguageTable.class);
                LanguageTable t = new LanguageTable("en_US");
                for (Message.Field.LanguageData ldata : value.getLanguagesList()) {
                    t.put(ldata.getLanguage(), ldata.getData());
                }
                return t;
            case CHAR:
                checkType(field, String.class);
                return value.getData();
            case DATE:
            case TIMESTAMP:
                checkType(field, Date.class);
                try {
                    return dateFormat.parse(value.getData());
                } catch (ParseException e) {
                    throw new JRException(e);
                }
            case BOOLEAN:
                checkType(field, Boolean.class);
                return value.getData().equals("True");
            default:
                return convertStringValue(value.getData(), field.getValueClass());
        }
    }

    public Object getFieldValue(JRField jrField) throws JRException {
        Message.Field field = getField(getFieldPath(jrField.getDescription()));
        if (field == null) return null;
        if (field.getData().equals("") && field.getLanguagesCount() == 0) return null;
        return getValue(jrField, field);
    }


    String parseSubPath(String path) throws JRException {
        if (subPaths.get(path) == null) {
            List<String> splPath = new LinkedList<String>();
            for (String piece : path.split("/")) {
                piece = piece.trim();
                if (piece.isEmpty()) continue;
                splPath.add(piece);
            }
            int sz = splPath.size();
            if (sz >= 2 && splPath.get(sz - 2).equals("data") && splPath.get(sz - 1).equals("record")) {
                splPath = splPath.subList(0, sz - 2);
            }
            String rp = "";
            for (String piece : splPath) {
                rp = rp + piece + "/";
            }
            if (!rp.isEmpty()) {
                rp = rp.substring(0, rp.length() - 1);
            }
            subPaths.put(path, rp);
        }

        return subPaths.get(path);
    }

    public ProtoBufferDataSource subDataSource(String path) throws JRException {
        int[] p = getFieldPath(parseSubPath(path));
        ProtoBufferDataSource dSource = new ProtoBufferDataSource();
        if (p.length == 0) {
            List<Message.Record> rc = new ArrayList<Message.Record>(1);
            rc.add(this.record);
            dSource.init(this.fields, rc, this.prefix, allFieldDefinitions, allFieldPaths);
        } else {
            Message.FieldDefinition def = fieldDefinitions.get(parseSubPath(path));
            Message.Field field = getField(p);
            dSource.init(def.getRelatedList(), field.getRelatedList(), path, allFieldDefinitions, allFieldPaths);
        }
        return dSource;
    }

    public Translator getTranslator() {
        return translator;
    }
}
