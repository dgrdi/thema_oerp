package com.nantic.jasperreports;

import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by demetrio on 7/13/14.
 */
public class OerpRetriever {
    String url, db, login, password;
    boolean _authenticated;
    int uid;

    OerpRetriever(String url, String db, String login, String password) {
        this.url = url;
        this.db = db;
        this.login = login;
        this.password = password;
    }

    void authenticate() throws Exception {
        if (_authenticated) return;
        XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
        config.setServerURL(new URL(url + "/xmlrpc/common"));
        XmlRpcClient client = new XmlRpcClient();
        client.setConfig(config);
        String[] params = {db, login, password};
        uid = (Integer)client.execute("login", params);
        _authenticated = true;
    }


    byte[] getData(String reportName) throws Exception{
        authenticate();
        XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
        config.setServerURL(new URL(url + "/xmlrpc/object"));
        XmlRpcClient client = new XmlRpcClient();
        client.setConfig(config);
        Object[][] reportDomain = {{"report_name", "=", reportName}};
        Object[] searchParams = {db, uid, password, "ir.actions.report.xml", "search", reportDomain};
        Object[] res = (Object[])client.execute("execute", searchParams);
        Object[] fields = {"model"};
        Object[] readParams = {db, uid, password, "ir.actions.report.xml", "read", res[0], fields};
        Map read = (Map)client.execute("execute", readParams);
        Object[] idParams = {db, uid, password, read.get("model"), "search", new Object[0], 0, 20};
        Object[] ids = (Object[])client.execute("execute", idParams);
        HashMap action = new HashMap();
        action.put("model", read.get("model"));
        action.put("report_type", "pdf");
        action.put("report_name", reportName);
        HashMap context = new HashMap();
        context.put("active_model", read.get("model"));
        context.put("lang", "it_IT");
        Object[] lastParams = {db, uid, password, "ir.actions.report.xml", "generate_data_buffer", action, ids, context};
        return (byte[])client.execute("execute", lastParams);
    }

}
