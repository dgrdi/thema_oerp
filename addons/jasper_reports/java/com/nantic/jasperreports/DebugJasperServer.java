/**
 * Created by demetrio on 7/13/14.
 */

package com.nantic.jasperreports;

import java.util.HashMap;
import java.util.Hashtable;


public class DebugJasperServer extends JasperServer {
    public static void main(String[] args) {
        startServer("0.0.0.0", 8090, DebugJasperServer.class);
    }

    public String mapPath(String path) {
        /*
        if (path.startsWith("/vagrant")) {
            path = path.replaceAll("^/vagrant", "/home/demetrio/PycharmProjects/thema_oerp");
        }
        if (path.startsWith("/tmp")) {
            path = path.replaceAll("^/tmp", "/home/demetrio/PycharmProjects/thema_oerp/tmp");
        }
        */
        return path;
    }

    @Override
    public HashMap getDBParams(Hashtable connectionParameters) {
        return new HashMap();
    }
}
