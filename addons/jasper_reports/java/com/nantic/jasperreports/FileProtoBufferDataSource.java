
package com.nantic.jasperreports;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;


public class FileProtoBufferDataSource extends ProtoBufferDataSource {

    FileProtoBufferDataSource(String fileName) throws IOException {
        Path p = FileSystems.getDefault().getPath(fileName);
        SeekableByteChannel chan = Files.newByteChannel(p);
        int sz = (int) chan.size();
        ByteBuffer buf = ByteBuffer.allocate(sz);
        int tot = 0;
        while (tot < sz) {
            tot += chan.read(buf);
        }
        Message.Data data = Message.Data.parseFrom(buf.array());
        init(data.getFieldsList(), data.getRecordsList(), "", new HashMap(), new HashMap());
    }

}
