package com.nantic.jasperreports;

import mondrian.xmla.DataSourcesConfig;
import net.sf.jasperreports.engine.*;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfig;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import java.net.URL;
import java.util.HashMap;

/**
 * Created by demetrio on 7/13/14.
 */
public class DataSourceProvider implements JRDataSourceProvider {

    public boolean supportsGetFieldsOperation() {
        return false;
    }

    public JRField[] getFields(JasperReport report) throws JRException, UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    public JRDataSource create(JasperReport report) throws JRException {

        String url = report.getProperty("OPENERP_SERVER");
        if (url == null) {
            url = "http://localhost:8069";
        }
        String reportName = report.getProperty("OPENERP_REPORT_NAME");
        String db = report.getProperty("OPENERP_DB");
        if (db == null) {
            db = "thema1";
        }

        String login = report.getProperty("OPENERP_LOGIN");
        if (login == null) {
            login = "admin";
        }

        String password = report.getProperty("OPENERP_PASSWORD");
        if (password == null) {
            password = "admin";
        }
        OerpRetriever oerp = new OerpRetriever(url, db, login ,password);
        try {
            Message.Data data = Message.Data.parseFrom(oerp.getData(reportName));
            ProtoBufferDataSource dataSource = new ProtoBufferDataSource();
            dataSource.init(data.getFieldsList(), data.getRecordsList(), "", new HashMap(), new HashMap());
            return dataSource;
        } catch (Exception e) {
            throw new JRException(e);
        }
    }

    public void dispose(JRDataSource dataSource) throws JRException {

    }

}
