package com.nantic.jasperreports;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRRewindableDataSource;

/**
 * Created by demetrio on 7/13/14.
 */
public interface SubDataSource extends JRRewindableDataSource{
    JRDataSource subDataSource(String path) throws JRException;
}

