# #############################################################################
#
# Copyright (c) 2008-2012 NaN Projectes de Programari Lliure, S.L.
# http://www.NaN-tic.com
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# #############################################################################
#
# Some fix added by Omal for generating reports and subreports from wizard
#
# #############################################################################
import os
from openerp import report
from openerp import pooler
from openerp.osv import orm, osv
from openerp import tools
import tempfile
from openerp import netsvc
from openerp import release
import logging
from StringIO import StringIO
from multiprocessing.pool import Pool
from PyPDF2 import PdfFileMerger

logger = logging.getLogger(__name__)

from JasperReports import *


tools.config['jasperserver'] = tools.config.get('jasperserver', 'http://localhost')

# Determines the port where the JasperServer process should listen with its XML-RPC server for incomming calls
tools.config['jasperport'] = tools.config.get('jasperport', 8090)

# Determines the file name where the process ID of the JasperServer process should be stored
tools.config['jasperpid'] = tools.config.get('jasperpid', 'openerp-jasper.pid')

# Determines if temporary files will be removed
tools.config['jasperunlink'] = tools.config.get('jasperunlink', True)


class Report:
    def __init__(self, name, cr, uid, ids, data, context):
        self.name = name
        self.cr = cr
        self.uid = uid
        self.ids = ids
        self.data = data
        self.model = self.data.get('model', False) or context.get('active_model', False)
        self.context = context or {}
        self.pool = pooler.get_pool(self.cr.dbname)
        self.reportPath = None
        self.report = None
        self.temporaryFiles = []
        self.outputFormat = 'pdf'
        self.datasource_type = 'protoBuffer'

    def build_report(self):
        """
        If self.context contains "return_pages = True" it will return the number of pages
        of the generated report.
        """

        # * Get report path *
        # Not only do we search the report by name but also ensure that 'report_rml' field
        # has the '.jrxml' postfix. This is needed because adding reports using the <report/>
        # tag, doesn't remove the old report record if the id already existed (ie. we're trying
        # to override the 'purchase.order' report in a new module). As the previous record is
        # not removed, we end up with two records named 'purchase.order' so we need to destinguish
        # between the two by searching '.jrxml' in report_rml.
        ids = self.pool.get('ir.actions.report.xml').search(self.cr, self.uid, [('report_name', '=', self.name[7:]),
                                                                                ('report_rml', 'ilike', '.jrxml')],
                                                            context=self.context)
        data = self.pool.get('ir.actions.report.xml').read(self.cr, self.uid, ids[0], ['report_rml', 'jasper_output'])
        if 'form' in self.data and 'report_type' in self.data['form'] and self.data['form']['report_type']:
            self.data['report_type'] = self.data['form']['report_type']

        if 'report_type' in self.data:
            data['jasper_output'] = self.data['report_type']
        elif 'form' in self.data and 'report_type' in self.data['form']:
            data['jasper_output'] = self.data['form']['report_type']
        if data['jasper_output']:
            self.outputFormat = data['jasper_output']
        self.reportPath = data['report_rml']
        self.reportPath = os.path.join(self.addonsPath(), self.reportPath)
        if not os.path.lexists(self.reportPath):
            self.reportPath = self.addonsPath(path=data['report_rml'])

        # Get report information from the jrxml file
        logger.info("Requested report: '%s'" % self.reportPath)
        self.report = JasperReport(self.reportPath)

    def build_subreports(self):
        subreport_data = []
        for subreport in self.report.subreports:
            fd, fn = tempfile.mkstemp()
            os.close(fd)
            subreport_data.append({
                'parameter': subreport['parameter'],
                'dataFile': fn,
                'jrxmlFile': subreport['filename']
            })
        return subreport_data


    def build_data(self, fname):
        args = (self.report, self.model, self.pool, self.cr, self.uid, self.ids, self.context)
        if self.datasource_type == 'csv':
            generator = CsvBrowseDataGenerator(*args)
        elif self.datasource_type == 'xml':
            generator = FastXmlDataGenerator(*args)
        else:
            generator = ProtocolBufferGenerator(*args)
        generator.generate(fname)
        self.temporaryFiles += generator.temporaryFiles

    def cleanup(self):
        # Remove all temporary files created during the report
        if tools.config['jasperunlink']:
            for file in self.temporaryFiles:
                try:
                    os.unlink(file)
                except os.error, e:
                    logger.warning("Could not remove file '%s'." % file)
        self.temporaryFiles = []

    def execute(self):
        self.build_report()
        # Create temporary input (XML) and output (PDF) files
        fd, dataFile = tempfile.mkstemp()
        os.close(fd)
        fd, outputFile = tempfile.mkstemp()
        os.close(fd)
        self.temporaryFiles.append(dataFile)
        self.temporaryFiles.append(outputFile)
        logger.info("Temporary data file: '%s'" % dataFile)

        import time

        start = time.time()
        self.build_data(dataFile)

        def compile_all(report):
            self.compile(report.reportPath)
            for subr in report.subreports:
                if subr['filename'] != 'DATASET':
                    compile_all(subr['report'])

        compile_all(self.report)
        # Call the external java application that will generate the PDF file in outputFile
        pages = self.executeReport(dataFile, outputFile, [])
        elapsed = (time.time() - start) / 60
        logger.info("ELAPSED: %f" % elapsed)

        # Read data from the generated file and return it
        f = open(outputFile, 'rb')
        try:
            data = f.read()
        finally:
            f.close()
        if self.context.get('return_pages'):
            return ( data, self.outputFormat, pages )
        else:
            return ( data, self.outputFormat )

    def get_data(self, format='xml'):
        old_dtype = self.datasource_type
        self.datasource_type = format
        try:
            self.build_report()
            fd, dataFile = tempfile.mkstemp()
            os.close(fd)
            self.temporaryFiles.append(dataFile)
            self.build_data(dataFile)
            with open(dataFile) as fo:
                data = fo.read()
            self.cleanup()
            return data
        finally:
            self.datasource_type = old_dtype

    def path(self):
        return os.path.abspath(os.path.dirname(__file__))

    def addonsPath(self, path=False):
        if path:
            report_module = path.split(os.path.sep)[0]
            for addons_path in tools.config['addons_path'].split(','):
                if os.path.lexists(addons_path + os.path.sep + report_module):
                    return os.path.normpath(addons_path + os.path.sep + path)
        return os.path.dirname(self.path())

    def systemUserName(self):
        if os.name == 'nt':
            import win32api

            return win32api.GetUserName()
        else:
            import pwd

            return pwd.getpwuid(os.getuid())[0]

    def dsn(self):
        host = tools.config['db_host'] or 'localhost'
        port = tools.config['db_port'] or '5432'
        dbname = self.cr.dbname
        return 'jdbc:postgresql://%s:%s/%s' % ( host, port, dbname )

    def userName(self):
        return tools.config['db_user'] or self.systemUserName()

    def password(self):
        return tools.config['db_password'] or ''

    def executeReport(self, dataFile, outputFile, subreportDataFiles):
        locale = self.context.get('lang', 'en_US')

        connectionParameters = {
            'output': self.outputFormat,
            'dsn': self.dsn(),
            'user': self.userName(),
            'password': self.password(),
            'subreports': subreportDataFiles,
            self.datasource_type: dataFile,
        }
        parameters = {
            'STANDARD_DIR': self.report.standardDirectory,
            'REPORT_LOCALE': locale,
            'IDS': self.ids,
        }
        if 'parameters' in self.data:
            parameters.update(self.data['parameters'])
        server = JasperServer(tools.config['jasperserver'], int(tools.config['jasperport']))
        server.setPidFile(tools.config['jasperpid'])
        #=========================================================================
        #Added by prajul to pass jasper property from code
        #=========================================================================
        properties = {}
        if 'properties' in self.data:
            properties.update(self.data['properties'])
        #=========================================================================
        #End
        #=========================================================================
        return server.execute(connectionParameters, self.reportPath, outputFile, parameters, properties)

    def compile(self, jrxml):
        server = JasperServer(tools.config['jasperserver'], int(tools.config['jasperport']))
        server.setPidFile(tools.config['jasperpid'])
        server.compile(jrxml)


_pool = None


def get_proc_pool():
    global _pool
    if not _pool:
        _pool = Pool(processes=10)
    return _pool


class ReportWorker(object):
    def __init__(self, obj, db, uid, data, context=None):
        self.obj = obj
        self.uid = uid
        self.data = data
        self.context = context
        self.db = db

    def __call__(self, ids):
        cr = pooler.get_db(self.db).cursor()
        try:
            res = self.obj.create_single(cr, self.uid, ids, self.data, context=self.context)
            cr.rollback()
            return res
        finally:
            cr.close()


class report_jasper(report.interface.report_int):
    def __init__(self, name, model, parser=None):
        # Remove report name from list of services if it already
        # exists to avoid report_int's assert. We want to keep the
        # automatic registration at login, but at the same time we
        # need modules to be able to use a parser for certain reports.
        print 'init ' + str(name) + ', parser: ' + str(parser)

        if release.major_version == '5.0':
            if name in netsvc.SERVICES:
                del netsvc.SERVICES[name]
        else:
            if name in netsvc.Service._services:
                del netsvc.Service._services[name]
        super(report_jasper, self).__init__(name)
        self.model = model
        self.parser = parser


    def create_single(self, cr, uid, ids, data, context):
        data = data.copy()
        context = context.copy()
        name = self.name

        if self.parser:
            d = self.parser(cr, uid, ids, data, context)
            ids = d.get('ids', ids)
            name = d.get('name', self.name)
            # Use model defined in report_jasper definition. Necesary for menu entries.
            data['model'] = d.get('model', self.model)
            data['records'] = d.get('records', [])
            # data_source can be 'model' or 'records' and lets parser to return
            # an empty 'records' parameter while still executing using 'records'
            # data['data_source'] = d.get( 'data_source', 'xml-records' )
            data['data_source'] = d.get('data_source', 'model')
            data['parameters'] = d.get('parameters', {})
            data['properties'] = d.get('properties', {})

        r = Report(name, cr, uid, ids, data, context)
        # return ( r.execute(), 'pdf' )
        return r.execute()

    def create(self, cr, uid, ids, data, context=None):
        return self.create_single(cr, uid, ids, data, context)
        if len(ids) <= 50 or data.get('report_type') != 'pdf':
            return self.create_single(cr, uid, ids, data, context)
        else:
            pool = get_proc_pool()
            ln = len(ids) / pool._processes
            id_split = [ids[x:x + ln] for x in xrange(0, len(ids), ln)]
            worker = ReportWorker(self, cr.dbname, uid, data, context=context)
            res = pool.map(worker, id_split)
            merger = PdfFileMerger()
            out = StringIO()
            for el, typ in res:
                inf = StringIO(el)
                merger.append(inf)
            merger.write(out)
            return out.getvalue(), typ


if release.major_version == '5.0':
    # Version 5.0 specific code

    # Ugly hack to avoid developers the need to register reports
    import pooler
    import report

    def register_jasper_report(name, model):
        name = 'report.%s' % name
        # Register only if it didn't exist another "jasper_report" with the same name
        # given that developers might prefer/need to register the reports themselves.
        # For example, if they need their own parser.
        if netsvc.service_exist(name):
            if isinstance(netsvc.SERVICES[name], report_jasper):
                return
            del netsvc.SERVICES[name]
        report_jasper(name, model)


    # This hack allows automatic registration of jrxml files without
    # the need for developers to register them programatically.

    old_register_all = report.interface.register_all

    def new_register_all(db):
        value = old_register_all(db)

        cr = db.cursor()
        # Originally we had auto=true in the SQL filter but we will register all reports.
        cr.execute("SELECT * FROM ir_act_report_xml WHERE report_rml ilike '%.jrxml' ORDER BY id")
        records = cr.dictfetchall()
        cr.close()
        for record in records:
            register_jasper_report(record['report_name'], record['model'])
        return value

    report.interface.register_all = new_register_all
else:
    # Version 6.0 and later

    def register_jasper_report(report_name, model_name):
        name = 'report.%s' % report_name
        # Register only if it didn't exist another "jasper_report" with the same name
        # given that developers might prefer/need to register the reports themselves.
        # For example, if they need their own parser.
        parser = None
        if name in netsvc.Service._services:
            service = netsvc.Service._services[name]
            if isinstance(service, report_jasper):
                return
            if hasattr(service, 'parser'):
                parser = service.parser
            del netsvc.Service._services[name]

        if parser:
            report_jasper(name, model_name, parser=parser)
        else:
            report_jasper(name, model_name)

    class ir_actions_report_xml(osv.osv):
        _inherit = 'ir.actions.report.xml'

        def register_all(self, cr):
            # Originally we had auto=true in the SQL filter but we will register all reports.
            cr.execute("SELECT * FROM ir_act_report_xml WHERE report_rml ilike '%.jrxml' ORDER BY id")
            records = cr.dictfetchall()
            for record in records:
                register_jasper_report(record['report_name'], record['model'])
            return super(ir_actions_report_xml, self).register_all(cr)

    ir_actions_report_xml()

# vim:noexpandtab:smartindent:tabstop=8:softtabstop=8:shiftwidth=8:
