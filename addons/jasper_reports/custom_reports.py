from jasper_reports import jasper_report
__author__ = 'federica'


jasper_report.report_jasper('report.invoice_report', 'account.invoice')
jasper_report.report_jasper('report.partner_balance_report', 'partner.balance')