__author__ = 'dgirardi'
from openerp.osv import orm, fields


class move(orm.Model):
    _inherit = 'stock.move'

    _columns = {
        'invoice_line_id': fields.many2one('account.invoice.line', 'Riga fattura', select=1),
    }


class picking(orm.Model):
    _inherit = 'stock.picking'

    _columns = {
        'invoice_id': fields.many2one('account.invoice', 'Fattura', select=1),
    }

    def _invoice_hook(self, cr, uid, picking, invoice_id):
        self.write(cr, uid, picking.id, {'invoice_id': invoice_id})

    def _invoice_line_hook(self, cr, uid, move_line, invoice_line_id):
        self.pool.get('stock.move').write(cr, uid, move_line.id, {'invoice_line_id': invoice_line_id})


class invoice(orm.Model):
    _inherit = 'account.invoice'

    _columns = {
        'delivery_ids': fields.function(lambda s, *a, **k: s._delivery_ids(*a, **k),
                                        type='one2many', relation='delivery.delivery',
                                        string='Spedizioni'),
        'picking_ids': fields.one2many('stock.picking', 'invoice_id', 'Ordini di consegna'),

    }

    def _delivery_ids(self, cr, uid, ids, field, arg, context=None):
        cr.execute("""
            SELECT i.id, d.id
            FROM account_invoice i
            INNER JOIN stock_picking p ON p.invoice_id = i.id
            INNER JOIN delivery_delivery d ON p.delivery_id = d.id
            WHERE i.id IN %s
        """, [tuple(ids)])
        res = {}
        for inv_id, dev_id in cr.fetchall():
            res.setdefault(inv_id, []).append(dev_id)
        res.update(dict.fromkeys(set(ids) - set(res.keys()), []))
        return res

    def unlink(self, cr, uid, ids, context=None):
        pk = self.pool.get('stock.picking')
        for inv in self.browse(cr, uid, ids, context=context):
            for pick in inv.picking_ids:
                pk.write(cr, uid, pick.id, {'invoice_state': '2binvoiced'}, context=context)
        return super(invoice, self).unlink(cr, uid, ids, context=context)


class invoice_line(orm.Model):
    _inherit = 'account.invoice.line'

    _columns = {
        'stock_move_id': fields.function(lambda s, *a, **k: s._stock_move_id(*a, **k),
                                         type='many2one', relation='stock.move', string='Riga consegna'),
        'sale_line_id': fields.related('stock_move_id', 'sale_line_id', type='many2one', relation='sale.order.line',
                                       string='Riga ordine'),
        'sale_id': fields.function(lambda s, *a, **k: s._sale_delivery_id(*a, **k), type='many2one',
                                   relation='sale.order', string='Ordine di origine', multi='sale_delivery'),
        'delivery_id': fields.function(lambda s, *a, **k: s._sale_delivery_id(*a, **k), type='many2one',
                                       relation='delivery.delivery', string='Spedizione di origine', multi='sale_delivery'),
        'force_sale_id': fields.many2one('sale.order', 'Ordine rif.'),
        'force_delivery_id': fields.many2one('delivery.delivery', 'Spedizione rif.'),
    }

    def _sale_delivery_id(self, cr, uid, ids, fields, arg, context=None):
        cr.execute("""
            SELECT l.id, COALESCE(l.force_delivery_id, m.delivery_id) delivery_id,
                  COALESCE(l.force_sale_id, ol.order_id) sale_id
            FROM account_invoice_line l
            INNER JOIN stock_move m on m.invoice_line_id = l.id
            LEFT JOIN sale_order_line ol ON m.sale_line_id = ol.id
            WHERE l.id IN %s
        """, [tuple(ids)])
        res = {el.pop('id'): el for el in cr.dictfetchall()}
        res.update(dict.fromkeys(set(ids) - set(res), {'sale_id': False, 'delivery_id': False}))
        return res
        res = {}
        for l in self.browse(cr, uid, ids, context=context):
            sm = (l.stock_move_id and l.stock_move_id.delivery_id and l.stock_move_id.delivery_id.id) or False
            res[l.id] = {
                'sale_id': l.force_sale_id.id if l.force_sale_id else (l.sale_line_id and l.sale_line_id.id or False),
                'delivery_id': l.force_delivery_id.id if l.force_delivery_id else sm,
            }
        return res

    def _stock_move_id(self, cr, uid, ids, field, args, context=None):
        cr.execute("""
            SELECT il.id, sm.id
            FROM account_invoice_line il
            INNER JOIN stock_move sm ON sm.invoice_line_id = il.id
            WHERE il.id IN %s;
        """, [tuple(ids)])
        res = dict(cr.fetchall())
        res.update(dict.fromkeys(set(ids) - set(res), False))
        return res

class delivery_delivery(orm.Model):
    _inherit = 'delivery.delivery'

    def _prepare_shipping_invoice_line(self, cr, uid, delivery, invoice, context=None):
        vals = super(delivery_delivery, self)._prepare_shipping_invoice_line(cr, uid, delivery, invoice, context=context)
        if vals:
            vals.update({
                'force_delivery_id': delivery.id,
            })
        return vals