from openerp.addons.jasper_reports import jasper_report
jasper_report.register_jasper_report('report.jr.invoice', 'account.invoice')
jasper_report.register_jasper_report('report.jr.delivery', 'delivery.delivery')
jasper_report.register_jasper_report('report.jr.partnerbalance', 'partner.balance')