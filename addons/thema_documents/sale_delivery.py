from openerp.osv import orm, fields


class sale(orm.Model):
    _inherit = 'sale.order'
    _columns = {
        'delivery_ids': fields.function(lambda s, *a, **k: s._delivery_ids(*a, **k),
                                        string='Spedizioni', type='many2many', relation='delivery.delivery'),
        'has_invoice': fields.function(lambda s, *a, **k: s._has_docs(*a, **k), multi='has_docs',
                                       type='boolean', string='Ha fattura'),
        'has_delivery': fields.function(lambda s, *a, **k: s._has_docs(*a, **k), multi='has_docs',
                                        type='boolean', string='Ha spedizione'),

    }

    def _delivery_ids(self, cr, uid, ids, field, arg, context=None):
        cr.execute("""
            SELECT so.id, dv.id
            FROM sale_order so
            INNER JOIN stock_picking sp ON sp.sale_id = so.id
            INNER JOIN delivery_delivery dv ON dv.id = sp.delivery_id
            WHERE so.id IN %s
        """, [tuple(ids)])
        res = {}
        for so_id, dv_id in cr.fetchall():
            res.setdefault(so_id, []).append(dv_id)
        res.update(dict.fromkeys(set(ids) - set(res), []))
        return res

    def _has_docs(self, cr, uid, ids, field, arg, context=None):
        data = self.read(cr, uid, ids, fields=['invoice_ids', 'delivery_ids'], context=context)
        res = {el['id']: {'has_invoice': bool(el['invoice_ids']),
                          'has_delivery': bool(el['delivery_ids'])}
               for el in data}
        return res

    def action_view_delivery_delivery(self, cr, uid, ids, context=None):
        deliveries = []
        for so in self.browse(cr, uid, ids, context=None):
            deliveries.extend(i.id for i in so.delivery_ids)
        act = {
            'type': 'ir.actions.act_window',
            'name': 'Spedizioni',
            'res_model': 'delivery.delivery',
            'view_mode': 'tree,form',
            'domain': repr([('id', 'in', deliveries)]),
        }
        if len(deliveries) == 1:
            act['view_mode'] = 'form'
            act['res_id'] = deliveries[0]
        return act