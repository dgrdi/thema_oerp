from openerp.osv import orm, fields
from openerp.addons import decimal_precision as dp


class invoice(orm.Model):
    _inherit = 'account.invoice'

    _columns = {
        'ship_charge': fields.function(lambda s, *a, **k: s._calc_charge(*a, **k),
                                       type='float',
                                       digits_compute=dp.get_precision('Account'),
                                       string='Spese Trasporto',
                                       multi='charge'),
        'collect_charge': fields.function(lambda s, *a, **k: s._calc_charge(*a, **k),
                                          type='float',
                                          digits_compute=dp.get_precision('Account'),
                                          string='Spese di servizio',
                                          multi='charge'),
    }

    def _calc_charge(self, cr, uid, ids, field, arg, context=None):
        res = {}
        cr.execute("""
            SELECT i.id,
             SUM(case lt.type when 'shipping' then il.price_subtotal_flat else 0::float end) ship_charge,
             SUM(case lt.type when 'collect' then il.price_subtotal_flat else 0::float end) collect_charge
            FROM account_invoice i
            INNER JOIN account_invoice_line il ON il.invoice_id = i.id
            INNER JOIN thema_line_type lt ON lt.id = il.line_type_id
            WHERE lt.type in ('shipping', 'collect')
            AND i.id IN %s
            GROUP BY i.id
        """, [tuple(ids)])
        res = {el.pop('id'): el for el in cr.dictfetchall()}
        res.update(dict.fromkeys(set(ids) - set(res), {'ship_charge': 0.0, 'collect_charge': 0.0} ))
        return res
