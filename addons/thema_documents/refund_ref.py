"""
Add a reference to the originating invoice for refunds.
"""

from openerp.osv import orm, fields

class Invoice(orm.Model):
    _inherit = 'account.invoice'
    _columns = {
        'ref_invoice_id': fields.many2one('account.invoice', 'Fattura originaria',
                                          readonly=1,
                                          states={'draft': [('readonly', False)]}),
    }

    def _prepare_refund(self, cr, uid, invoice, date=None, period_id=None, description=None, journal_id=None, context=None):
        data = super(Invoice, self)._prepare_refund(cr, uid, invoice, date=date, period_id=period_id,
                                                    description=description, journal_id=journal_id, context=context)
        data.update({
            'ref_invoice_id': invoice['id'],
        })
        return data

class Picking(orm.Model):
    _inherit = 'stock.picking'

    def _prepare_invoice(self, cr, uid, picking, partner, inv_type, journal_id, context=None):
        data = super(Picking, self)._prepare_invoice(cr, uid, picking, partner, inv_type, journal_id, context=context)
        if inv_type in ('out_refund', 'in_refund') and picking.invoice_id:
            data.update({
                'ref_invoice_id': picking.invoice_id.id
            })
        return data