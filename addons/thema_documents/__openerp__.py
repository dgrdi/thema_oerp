{
    'name': 'Thema - documenti',
    'version': '0.1',
    'category': '',
    'description': """Modifiche a documenti (fatture, bolle, etc)""",
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': ['base', 'sale', 'stock', 'sale_stock', 'account', 'thema_delivery'],
    'init_xml': [],
    'data': [
        'views/invoice.xml',
        'views/sale.xml',
        'reports/balance.xml',
        'reports/invoice.xml',
        'reports/delivery.xml',
    ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': True,
}
