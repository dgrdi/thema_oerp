from openerp.osv import orm, fields

class partner_parents(orm.Model):
    """
    Helper object to avoid out of memory error on recomputing of display_name (from account_report_company).
    """

    _name = 'res.partner.parents'
    _auto = False

    _columns = {
        'parent_id': fields.many2one('res.partner', 'Parent', readonly=True)
    }

    def init(self, cr):
        cr.execute("""
            DROP VIEW IF EXISTS res_partner_parents;
            CREATE OR REPLACE VIEW res_partner_parents AS
            SELECT
                p1.id,
                p2.id parent_id
            FROM
                res_partner p1, res_partner p2
                WHERE
                        p1.parent_left > p2.parent_left
                    AND p1.parent_left < p2.parent_right
                    AND p1.parent_right > p2.parent_left
                    AND p1.parent_right < p2.parent_right;
        """)

