from openerp.osv import fields, orm
from logging import getLogger

from operator import itemgetter

logger = getLogger(__name__)

class partner(orm.Model):
    _inherit = 'res.partner'
    _rec_name = 'display_name'

    def _field_category_notes(self, cr, uid, ids, field_name, arg, context=None):
        categ_ids = self.read(cr, uid, ids, fields=('category_id',), context=context)
        def get_notes(categ_ids):
            notes = self.pool.get('res.partner.category').read(cr, uid, categ_ids, fields=('name', 'note'), context=context)
            notes = filter(itemgetter('note'), notes)
            return '<table>{0}</table>'.format(
                ''.join('''
                            <tr>
                                <th style="padding-right: 1em;">{0}</th>
                                <td>
                                    <div style="white-space: pre-wrap">{1}</div>
                                </td>
                            </tr>
                        '''.format(n['name'], n['note']) for n in notes)
            )
        return {c['id']: get_notes(c['category_id']) for c in categ_ids}

    def _field_category_default_note_ids(self, cr, uid, ids, field_name, arg, context=None):
        categ_ids = self.read(cr, uid, ids, fields=('category_id',), context=context)
        def get_notes(categ_ids):
            nids = self.pool.get('res.partner.category').read(cr, uid, categ_ids, fields=('default_note_ids',), context=context)
            return list(set(id for ids in nids for id in ids['default_note_ids']))
        return  {c['id']: get_notes(c['category_id']) for c in categ_ids}

    _display_name = lambda self, *args, **kwargs: self._display_name_compute(*args, **kwargs)

    def _display_name_idfind(self, cr, uid, ids, context=None):
        """Override account_report_company to NOT use child_of in search, which causes memory errors."""
        res = self.pool.get('res.partner.parents').search(cr, uid, [('parent_id', 'in', ids)])
        return res + ids

    _display_name_store_triggers = {
        'res.partner': (_display_name_idfind,
                        ['parent_id', 'is_company', 'name'], 10)
    }


    def _address_display(self, cr, uid, ids, name, args, context=None):
        res = {}
        for partner in self.browse(cr, uid, ids, context=context):
            res[partner.id] = self._display_address(cr, uid, partner, without_company=True, context=context)
        return res


    _columns = {
        'code': fields.char('Codice', readonly=True),

        'legal_name': fields.related('commercial_partner_id', 'name', string='Ragione sociale', readonly=True,
                                     type='char'),

        ## contact info
        'admin_email': fields.char('Email amministrativa', help="Email per l'invio di fatture."),
        'cert_email': fields.char('PEC (Posta elettronica certificata)'),

        'invoice_partner': fields.many2one('res.partner', 'Cliente fatturazione', domain=[('parent_id', '=', False)]),

        'fiscal_code': fields.char('Codice Fiscale'),

        ##notes
        'category_notes': fields.function(_field_category_notes, string='Note di categoria', type='html'),

        'default_category_note_ids': fields.function(_field_category_default_note_ids,
                                                     string='Categorie default note',
                                                     type='one2many',
                                                     relation='partner.note.default'),

        'default_note_ids': fields.one2many('partner.note.default', 'partner_id', 'Note preimpostate'),

        'display_name': fields.function(_display_name, type='char', string='Name',
                                        store=_display_name_store_triggers, select=1),

    }

    _defaults = {
        'type': 'invoice', # type 'default' is wildcard and thus inappropriate
    }

    def on_change_category_id(self, cr, uid, ids, category_id, context=None):
        if len(ids) > 1:
            return {}
        cr.execute('SAVEPOINT categ_change;')
        if not ids:
            id = self.create(cr, uid, {'name': 'x'}, context=context)
        else:
            id = ids[0]
        self.write(cr, uid, id, {'category_id': category_id})
        notes = self.read(cr, uid, id, fields=('category_notes', 'default_category_note_ids'), context=context)
        cr.execute('ROLLBACK TO SAVEPOINT categ_change;')
        notes.pop('id')
        res =  {
            'value': notes,
        }
        return res


    def address_get(self, cr, uid, ids, adr_pref=None, context=None):
        addresses= super(partner, self).address_get(cr, uid, ids, adr_pref, context)
        if 'invoice' in addresses:
            id = ids[len(ids)-1]
            par=self.browse(cr, uid, id, context=context)
            if par.is_company or not par.parent_id:
                #is a parent
                if par.invoice_partner:
                    #there is an invoice partner (fatturazione centralizzata)
                    addresses['invoice']=par.invoice_partner.id
            else:
                #not a parent
                if not par.type or par.type!='invoice':
                    #child not of invoice type
                    addresses['invoice']=self.address_get(cr, uid, [par.parent_id.id], adr_pref, context)['invoice']
                    #if not par.parent_id.invoice_partner:
                    #    #there is an invoice partner (fatturazione centralizzata)
                    #    addresses['invoice']=par.parent_id.invoice_partner.id
        return addresses

    def name_get(self, cr, uid, ids, context=None):
        if isinstance(ids, (int, long)):
            ids = [ids]
        res = super(partner, self).name_get(cr, uid, ids, context=None)
        codes = self.read(cr, uid, ids, fields=['code'])
        codes = {el['id']: el['code'] for el in codes}
        res = [(id, '[%s] %s' % (codes[id], nam) if codes[id] else nam) for id, nam in res]
        return res

    def name_search(self, cr, user, name='', args=None, operator='ilike', context=None, limit=100):
        return orm.Model.name_search.__get__(self)(cr, user, name=name, args=args, operator=operator,
                                                   context=context, limit=limit)



class partner_category(orm.Model):
    _inherit = 'res.partner.category'

    _columns = {
        'note': fields.text('Note'),
        'default_note_ids': fields.one2many('partner.note.default', 'category_id', 'Note di categoria'),
    }

