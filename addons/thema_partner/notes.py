from openerp.osv import orm, fields

class partner_default_note(orm.Model):

    _name = 'partner.note.default'

    _columns = {
        'partner_id': fields.many2one('res.partner', 'Partner'),
        'category_id': fields.many2one('res.partner.category', 'Categoria'),

        'order_internal': fields.boolean('Ordine di vendita (interno)'),
        'order_header': fields.boolean('Testata ordine di vendita'),
        'order_footer': fields.boolean(u'Pie di pagina ordine di vendita'),

        'delivery_internal': fields.boolean('Ordine di consegna (interno)'),
        'delivery_header': fields.boolean('Testata ordine di consegna'),
        'delivery_footer': fields.boolean('Pie di pagina ordine di consegna'),

        'invoice_internal': fields.boolean('Fattura (interno)'),
        'invoice_header': fields.boolean('Testata fattura'),
        'invoice_footer': fields.boolean('Pie di pagina fattura'),

        'note': fields.text('Note'),
    }
