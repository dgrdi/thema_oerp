# coding=utf-8
from openerp.osv import orm, fields

class res_partner(orm.Model):
    """
    Set parent store on res_partner, more efficient calculation of commercial
    partners
    """
    _inherit = 'res.partner'
    _parent_store = True


    _columns = {
        'parent_id': fields.many2one('res.partner', 'Related Company', select=True, ondelete='restrict'),
        'parent_left': fields.integer('Left parent', select=True),
        'parent_right': fields.integer('Right parent', select=True),
    }



class partner_commercial(orm.Model):
    _name = 'res.partner.commercial'

    _auto = False
    _columns = {
        'commercial_partner_id': fields.many2one('res.partner', u'Entità commerciale', readonly=True)
    }

    def init(self, cr):
        cr.execute("""
        DROP VIEW IF EXISTS res_partner_commercial;
        CREATE OR REPLACE VIEW res_partner_commercial AS
        WITH parents AS (
            SELECT
                p1.id,
                p2.id commercial_partner_id,
                ROW_NUMBER() OVER (PARTITION BY p1.id ORDER BY p2.parent_left) n
            FROM
                res_partner p1, res_partner p2
                WHERE
                        p1.parent_left > p2.parent_left
                    AND p1.parent_left < p2.parent_right
                    AND p1.parent_right > p2.parent_left
                    AND p1.parent_right < p2.parent_right
                    AND p2.is_company
        ) SELECT
            p.id, COALESCE(cp.commercial_partner_id, p.id) commercial_partner_id
          FROM res_partner p
          LEFT JOIN parents cp ON cp.id = p.id
          WHERE COALESCE(cp.n, 1) = 1;
        """)

class res_partner2(orm.Model):
    _inherit = 'res.partner'

    _commercial_partner_id = lambda self, *args, **kwargs: self._commercial_partner_compute(*args, **kwargs)

    _columns = {
        'commercial_partner_id': fields.function(_commercial_partner_id,
                                                 type='many2one',
                                                 relation='res.partner',
                                                 string='Commercial Entity',
                                                 store = {
                                                     'res.partner': (
                                                         lambda o, c, u, i, *a: i,
                                                         ['parent_id', 'is_company'],
                                                         10
                                                     )
                                                 })
    }

    def _commercial_partner_compute(self, cr, uid, ids, name, args, context=None):
        cm = self.pool.get('res.partner.commercial')
        res = cm.read(cr, uid, ids, fields=['commercial_partner_id'])
        res = {r['id']: r['commercial_partner_id'][0] for r in res}
        return res
