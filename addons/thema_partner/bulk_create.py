from openerp.osv import orm, fields
import logging
logger = logging.getLogger(__name__)

class partner(orm.Model):
    _inherit = 'res.partner'

    def bulk_create(self, cr, uid, vals, context=None):
        create_ids = super(partner, self).bulk_create(cr, uid, vals, context)
        logger.debug('Calling _fields_sync and _handle_first_contact_creation on res.partner...')
        partners = self.browse(cr, uid, create_ids, context=context)
        partners = sorted(partners, key=lambda p: create_ids.index(p['id']))
        for p, val in zip(partners, vals):
            self._fields_sync(cr, uid, p, val, context=context)
            self._handle_first_contact_creation(cr, uid, p, context=context)
        return create_ids

    def bulk_write(self, cr, uid, ids_by_values, context=None):
        super(partner, self).bulk_write(cr, uid, ids_by_values, context=context)
        logger.debug('Calling _fields_sync on res.partner...')
        ids_by_values = map(lambda (a, b): ((a,), b) if isinstance(a, (int, long)) else (a, b), ids_by_values)
        vals_by_id = {i: v for ids, v in ids_by_values for i in ids}
        partners = self.browse(cr, uid, vals_by_id.keys(), context=context)
        for p in partners:
            val = vals_by_id[p.id]
            #if 'recalculate_tree' in context and context.recalculate_tree==True:
            if 'recalculate_tree' in context:
                self._fields_sync(cr, uid, p, val, context=context)
        return True

    def _validate(self, cr, uid, ids, context=None):
        if context and context.get('import'):
            pass #do not validate on initial import
        else:
            return super(partner, self)._validate(cr, uid, ids, context)

