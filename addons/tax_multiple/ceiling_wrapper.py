"""
Per le tasse con un massimale, questo aggeggio emula una tassa ad albero:

                    T0
                /       \
            Isotto      Isopra
               |           |
            Tassa       Tassa0

L'imponibile viene spezzato tra Isotto e Isopra per gli importi che raggiungono e superano il
massimale rispettivamente; la tassa viene applicata sulla parte sotto il massimale, mentre
per la parte sopra viene solamente registrato l'imponibile (nel campo `over_ceil_base_tax_id`).
"""

from openerp.osv.orm import browse_record, browse_null


class TaxWrapper(object):
    def __init__(self, tax, overrides=None):
        self.tax = tax
        self.overrides = overrides or {}

    def __getattr__(self, item):
        if item in self.overrides:
            return self.overrides[item] if self.overrides[item] is not False else browse_null()
        else:
            tx = object.__getattribute__(self, 'tax')
            return getattr(tx, item)

    def __getitem__(self, item):
        return getattr(self, item)

class CeilingTaxWrapper(TaxWrapper):
    def __init__(self, tax, under, over):

        none_tax = {
            'base_code_id': False,
            'ref_base_code_id': False,
            'tax_code_id': False,
            'ref_tax_code_id': False,
        }
        under_base = none_tax.copy()
        under_base.update({
            'amount': under,
            'type': 'fixed',
            'child_depend': True,
            'child_ids': [tax]
        })
        over_leaf = none_tax.copy()
        over_leaf.update({
            'base_code_id': tax.over_ceil_base_code_id,
            'ref_base_code_id': tax.over_ceil_base_code_id,
            'type': 'percent',
            'amount': 0.0,
        })
        over_base = none_tax.copy()
        over_base.update({
            'amount': over,
            'type': 'fixed',
            'child_depend': True,
            'child_ids': [TaxWrapper(tax, over_leaf)]
        })
        ov  = none_tax.copy()
        ov.update({
            'child_depend': True,
            'amount': 1,
            'type': 'percent',
            'child_ids': [TaxWrapper(tax, under_base), TaxWrapper(tax, over_base)]
        })
        super(CeilingTaxWrapper, self).__init__(tax, ov)

