from openerp.osv import orm, fields
from tax_multiple.consolidate import consolidate_for_create


class account_invoice(orm.Model):
    _inherit = 'account.invoice'

    def finalize_invoice_move_lines(self, cr, uid, invoice_browse, move_lines):
        lines = super(account_invoice, self).finalize_invoice_move_lines(cr, uid, invoice_browse, move_lines)
        return consolidate_for_create(lines)
