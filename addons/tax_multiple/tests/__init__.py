__author__ = 'dgirardi'
import test_tax
import test_consolidation
import test_move
import test_invoice
import test_parameters
import test_ceiling
checks = [
    test_tax,
    test_consolidation,
    test_move,
    test_invoice,
    test_parameters,
    test_ceiling,
]