from openerp.netsvc import LocalService
from openerp.tests.common import TransactionCase
from testutils.factories.account import InvoiceLineFactory, TaxFactory, InvoiceFactory
from testutils.object_wrapper import ModelWrapper


class TestInvoiceTax(TransactionCase):
    invoicelinef = InvoiceLineFactory()
    invoicef = InvoiceFactory()
    taxf = TaxFactory()
    wmove = ModelWrapper('account.move')

    def test_multiple_tax(self):
        t1 = self.taxf.create({'amount': 0.1})
        t2 = self.taxf.create({'amount': 0.1})
        il = self.invoicelinef.create({
            'invoice_line_tax_id': [(6, 0, [t1, t2])],
            'price_unit': 100,
            'quantity': 1,
        })
        self.invoicef.wobj._workflow_signal([il.browse.invoice_id.id], 'invoice_open')
        move = il.browse.invoice_id.move_id
        self.assertEqual(len(move.line_id), 4)
        #are tax amounts ok?
        self.assertEqual(t1.browse.base_code_id.sum, 100)
        self.assertEqual(t1.browse.tax_code_id.sum, 10)
        self.assertEqual(t2.browse.base_code_id.sum, 100)
        self.assertEqual(t2.browse.tax_code_id.sum, 10)


    def test_single_tax(self):
        t1 = self.taxf.create({'amount': 0.1})
        il = self.invoicelinef.create({
            'invoice_line_tax_id': [(6, 0, [t1])],
            'price_unit': 100,
            'quantity': 1,
        })
        self.invoicef.wobj._workflow_signal([il.browse.invoice_id.id], 'invoice_open')
        move = il.browse.invoice_id.move_id
        self.assertEqual(len(move.line_id), 3)
        self.assertEqual((t1.browse.base_code_id.sum, t1.browse.tax_code_id.sum), (100, 10))
