from openerp.tests.common import TransactionCase
from testutils.factories.account import *


class test_tax(TransactionCase):
    move_line_fc = MoveLineFactory()
    tax_code_fc = TaxCodeFactory()

    def test_write_read_fields(self):
        tc = self.tax_code_fc.create()
        ml = self.move_line_fc.create({
            'tax_code_id': tc,
            'tax_amount': 10
        })
        ml = self.move_line_fc.wobj.browse(ml)
        self.assertEqual(ml.tax_code_id.id, tc)
        self.assertEqual(ml.tax_amount, 10)

    def test_write_multiple(self):
        tc1 = self.tax_code_fc.create()
        tc2 = self.tax_code_fc.create()
        ml = self.move_line_fc.create({
            'tax_ids': [
                (0, 0, {'tax_code_id': tc1, 'tax_amount': 10}),
                (0, 0, {'tax_code_id': tc2, 'tax_amount': 20})
            ]
        })
        ml = self.move_line_fc.wobj.browse(ml)
        self.assertEqual(ml.tax_code_id.id, tc1)
        self.assertEqual(ml.tax_amount, 10)

    def test_set_multiple(self):
        tc1 = self.tax_code_fc.create()
        tc2 = self.tax_code_fc.create()
        ml = self.move_line_fc.create({
            'tax_ids': [
                (0, 0, {'tax_code_id': tc1, 'tax_amount': 10}),
                (0, 0, {'tax_code_id': tc2, 'tax_amount': 20})
            ]
        })
        self.move_line_fc.wobj.write(ml, {
            'tax_amount': 30
        })
        ml = self.move_line_fc.wobj.browse(ml)
        self.assertEqual(ml.tax_ids[0].tax_amount, 30)

    def test_set_null(self):
        ml = self.move_line_fc.create({
            'tax_code_id': None,
            'tax_amount': None,
        })
        ml = self.move_line_fc.wobj.browse(ml)
        self.assertEqual(len(ml.tax_ids), 0)

    def test_set_null_after_non_null(self):
        ml = self.move_line_fc.create({
            'tax_code_id': self.tax_code_fc.create(),
            'tax_amount': 10,
        })
        self.move_line_fc.wobj.write(ml, {
            'tax_code_id': None,
            'tax_amount': 0,
        })
        ml = self.move_line_fc.wobj.browse(ml)
        self.assertEqual((ml.tax_code_id.id, ml.tax_amount), (False, 0))


    def test_set_null_delets(self):
        ml = self.move_line_fc.create({
            'tax_code_id': self.tax_code_fc.create(),
            'tax_amount': 10
        })
        self.move_line_fc.wobj.write(ml, {'tax_code_id': None, 'tax_amount': None})
        ml = self.move_line_fc.wobj.browse(ml)
        self.assertEqual(len(ml.tax_ids), 0)


    def test_tax_amount(self):
        tc = self.tax_code_fc.create()
        ml = self.move_line_fc.create({
            'tax_code_id': tc,
            'tax_amount': 10
        })
        tc = self.tax_code_fc.wobj.browse(tc)
        self.assertEqual(tc.sum, 10)

    def test_tax_amount_multiple(self):
        tc1 = self.tax_code_fc.create()
        tc2 = self.tax_code_fc.create()
        ml = self.move_line_fc.create({
            'tax_ids': [
                (0, 0, {'tax_code_id': tc1, 'tax_amount': 10}),
                (0, 0, {'tax_code_id': tc2, 'tax_amount': 20}),
            ]
        })
        tc1, tc2 = self.tax_code_fc.wobj.browse([tc1, tc2])
        self.assertEqual((tc1.sum, tc2.sum), (10, 20))

    def test_search(self):
        tc = self.tax_code_fc.create()
        ml = self.move_line_fc.create({
            'tax_code_id': tc,
            'tax_amount': 10,
        })
        res = self.move_line_fc.wobj.search([('tax_code_id', '=', tc)])
        self.assertListEqual(res, [ml])

    def test_search_o2m(self):
        tc1 = self.tax_code_fc.create()
        tc2 = self.tax_code_fc.create()
        ml = self.move_line_fc.create({
            'tax_ids': [
                (0, 0, {'tax_code_id': tc1, 'tax_amount': 10}),
                (0, 0, {'tax_code_id': tc2, 'tax_amount': 20}),
            ]
        })
        res = self.move_line_fc.wobj.search([('tax_code_id', '=', tc2)])
        self.assertListEqual(res, [ml])


