from openerp.tests.common import TransactionCase
from testutils.factories.account import TaxFactory, AccountFactory, TaxCodeFactory, MoveLineFactory


class TestMoveTax(TransactionCase):
    accountf = AccountFactory()
    taxcodef = TaxCodeFactory()
    taxf = TaxFactory()
    movelinef = MoveLineFactory()

    def test_multiple_tax(self):
        ac = self.accountf.create()
        t1 = self.taxf.create({
            'account_collected_id': False,
            'account_paid_id': False,
            'child_depend': True,
            'amount': 1,
            'child_ids': [(0, 0, {'amount': 0, 'account_collected_id': ac, 'sequence': 10}),
                          (0, 0, {'account_collected_id': ac, 'sequence': 20})]
        })
        ml = self.movelinef.create({
            'debit': 100,
            'account_tax_id': t1
        })
        ml = self.movelinef.wobj.browse(ml)
        self.assertEqual(len(ml.move_id.line_id), 2)
        t1 = self.taxf.wobj.browse(t1)
        ct1, ct2 = t1.child_ids
        self.assertEqual(ct1.tax_code_id.sum, 0)
        self.assertEqual(ct1.base_code_id.sum, 100)
        self.assertEqual(ct2.tax_code_id.sum, 10)
        self.assertEqual(ct2.base_code_id.sum, 100)

    def test_multiple_tax_single_base(self):
        ac = self.accountf.create()
        t1 = self.taxf.create({
            'account_collected_id': False,
            'account_paid_id': False,
            'child_depend': True,
            'amount': 1,
            'child_ids': [(0, 0, {'amount': 0, 'account_collected_id': ac, 'sequence': 10, 'base_code_id': False,}),
                          (0, 0, {'account_collected_id': ac, 'sequence': 20, 'base_code_id': False})]
        })
        ml = self.movelinef.create({
            'debit': 100,
            'account_tax_id': t1,
        })
        ml = self.movelinef.wobj.browse(ml)
        self.assertEqual(len(ml.move_id.line_id), 2)
        t1 = self.taxf.wobj.browse(t1)
        self.assertEqual(t1.base_code_id.sum, 100)


