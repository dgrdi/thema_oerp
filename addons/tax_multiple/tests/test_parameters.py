from openerp.osv.osv import except_osv
from openerp.tests.common import TransactionCase
from tax_multiple.tests.factories import TaxParameterFactory, TaxWParameterFactory
from testutils.factories.account import FiscalyearFactory, TaxFactory, MoveLineFactory, InvoiceLineFactory, \
    InvoiceFactory


class TestTaxParameter(TransactionCase):
    paramf = TaxParameterFactory()
    fiscalyearf = FiscalyearFactory()
    taxf = TaxFactory()
    taxpf = TaxWParameterFactory()
    movelinef = MoveLineFactory()
    invoicelinef = InvoiceLineFactory()
    invoicef = InvoiceFactory()

    def test_multiple_values_for_same_year_rases(self):
        fy = self.fiscalyearf.create()
        create = lambda: self.paramf.create({
            'item_ids': [(0, 0, {'fiscalyear_id': fy}), (0, 0, {'fiscalyear_id': fy})]
        })
        self.assertRaises(Exception, create)

    def test_get_nonex_value_raises(self):
        p = self.paramf.create()
        fy = self.fiscalyearf.create()
        get = lambda: p.wobj.get(self.cr, self.uid, p, fy)
        self.assertRaises(except_osv, get)


    def test_get_by_fiscalyear(self):
        fy1 = self.fiscalyearf.create()
        fy2 = self.fiscalyearf.create()
        p = self.paramf.create({
            'item_ids': [(0, 0, {'fiscalyear_id': fy1, 'amount': 10}),
                         (0, 0, {'fiscalyear_id': fy2, 'amount': 20})]
        })
        self.assertEqual(p.wobj.get(p, fy1), 10)
        self.assertEqual(p.wobj.get(p, fy2), 20)

    def test_tax_by_param(self):
        fy1 = self.fiscalyearf.create()
        fy2 = self.fiscalyearf.create()
        p = self.paramf.create({
            'item_ids': [(0, 0, {'fiscalyear_id': fy1, 'amount': 10}),
                         (0, 0, {'fiscalyear_id': fy2, 'amount': 20})]
        })
        tx = self.taxf.create({
            'amount_param_id': p,
            'amount': None,

        })
        tx_fy1 = tx.wobj.browse(tx, context={'fiscalyear_id': fy1})
        tx_fy2 = tx.wobj.browse(tx, context={'fiscalyear_id': fy2})
        self.assertEqual(tx_fy1.amount, 10)
        self.assertEqual(tx_fy2.amount, 20)

    def test_tax_compute_move_line(self):
        fy1 = self.fiscalyearf.create()
        fy2 = self.fiscalyearf.create()
        fy1.wobj.create_period([fy1, fy2])
        p = self.paramf.create({
            'item_ids': [(0, 0, {'fiscalyear_id': fy1, 'amount': 10}),
                         (0, 0, {'fiscalyear_id': fy2, 'amount': 20})]
        })
        tx = self.taxf.create({
            'amount': None,
            'amount_param_id': p
        })
        line_fy1 = self.movelinef.create({
            'account_tax_id': tx,
            'date': fy1.browse.date_start,
            'debit': 1,
            'move_id': {
                'period_id': fy1.browse.period_ids[0].id,
            },
        })
        line_fy2 = self.movelinef.create({
            'account_tax_id': tx,
            'date': fy2.browse.date_start,
            'debit': 1,
            'move_id': {
                'period_id': fy2.browse.period_ids[0].id,
            },
        })
        tx_fy1 = tx.wobj.browse(tx, context={'fiscalyear_id': fy1})
        tx_fy2 = tx.wobj.browse(tx, context={'fiscalyear_id': fy2})
        self.assertEqual(tx_fy1.tax_code_id.sum, 10)
        self.assertEqual(tx_fy2.tax_code_id.sum, 20)

    def test_tax_parameter_invoice(self):
        fy1 = self.fiscalyearf.create()
        fy2 = self.fiscalyearf.create()
        fy1.wobj.create_period([fy1, fy2])
        p = self.paramf.create({
            'item_ids': [(0, 0, {'fiscalyear_id': fy1, 'amount': 10}),
                         (0, 0, {'fiscalyear_id': fy2, 'amount': 20})]
        })
        tx = self.taxf.create({
            'amount': None,
            'amount_param_id': p
        })
        il_fy1 = self.invoicelinef.create({
            'invoice_id': {
                'date_invoice': fy1.browse.date_start,
            },
            'invoice_line_tax_id': [(6, 0, [tx])],
            'quantity': 1,
            'price_unit': 1,
        })
        il_fy2 = self.invoicelinef.create({
            'invoice_id': {
                'date_invoice': fy2.browse.date_start,
            },
            'invoice_line_tax_id': [(6, 0, [tx])],
            'quantity': 1,
            'price_unit': 1,
        })
        self.invoicef.wobj._workflow_signal([il_fy1.browse.invoice_id.id, il_fy2.browse.invoice_id.id], 'invoice_open')
        tx_fy1 = tx.wobj.browse(tx, context={'fiscalyear_id': fy1})
        tx_fy2 = tx.wobj.browse(tx, context={'fiscalyear_id': fy2})
        self.assertEqual(tx_fy1.tax_code_id.sum, 10)
        self.assertEqual(tx_fy2.tax_code_id.sum, 20)





