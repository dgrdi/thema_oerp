from testutils.factories.account import FiscalyearFactory, TaxFactory
from testutils.factories.autoincrement import AutoIncrementStr
from testutils.factories.factory import Factory
from testutils.factories.field import Many2One, One2Many


class TaxParameterFactory(Factory):
    model = 'account.tax.parameter'
    defaults = {
        'name': AutoIncrementStr('tax parameter '),
        'item_ids': One2Many(lambda: TaxParameterItemFactory())
    }

class TaxParameterItemFactory(Factory):
    model = 'account.tax.parameter.item'
    defaults = {
        'param_id': Many2One(TaxParameterFactory()),
        'fiscalyear_id': Many2One(FiscalyearFactory()),
        'amount': 0.1,
    }

class TaxWParameterFactory(TaxFactory):
    defaults = {
        'amount_param_id': Many2One(TaxParameterFactory()),
        'base_ceiling_param_id': Many2One(TaxParameterFactory())
    }
