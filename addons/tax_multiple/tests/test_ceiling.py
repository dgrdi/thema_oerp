from openerp.tests.common import TransactionCase
from tax_multiple.tests.factories import TaxParameterFactory, TaxWParameterFactory
from testutils.factories.account import TaxFactory, FiscalyearFactory, MoveLineFactory, TaxCodeFactory, InvoiceFactory
from testutils.factories.partner import PartnerFactory


class TestCeiling(TransactionCase):
    taxparamf = TaxParameterFactory()
    fiscalyearf = FiscalyearFactory()
    taxf = TaxWParameterFactory()
    partnerf = PartnerFactory()
    movelinef = MoveLineFactory()
    taxcodef = TaxCodeFactory()
    invoicef = InvoiceFactory()

    def test_ceiling_read(self):
        fy1 = self.fiscalyearf.create()
        fy2 = self.fiscalyearf.create()
        tx = self.taxf.create({
            'amount_param_id': None,
            'base_ceiling_param_id': {
                'item_ids': [(0, 0, {'amount': 100, 'fiscalyear_id': fy1}),
                              (0, 0, {'amount': 200, 'fiscalyear_id': fy2})]
            }
        })
        tx_fy1 = tx.obj.browse(self.cr, self.uid, tx, context={'fiscalyear_id': fy1})
        tx_fy2 = tx.obj.browse(self.cr, self.uid, tx, context={'fiscalyear_id': fy2})
        self.assertEqual(tx_fy1.base_ceiling, 100)
        self.assertEqual(tx_fy2.base_ceiling, 200)

    def test_ceiling_compute(self):
        fy = self.fiscalyearf.create()
        tx = self.taxf.create({
            'amount_param_id': None,
            'base_ceiling_param_id': {
                'item_ids': [(0, 0, {'amount': 100, 'fiscalyear_id': fy})]
            }
        })
        pt = self.partnerf.create()
        tx_fy1 = tx.obj.browse(self.cr, self.uid, tx, context={'fiscalyear_id': fy})
        res = tx.wobj.compute_all([tx_fy1], 200, 1, product=None, partner=pt.browse)
        self.assertEqual(res['total_included'], 210)
        self.assertEqual(len(res['taxes']), 2)


    def test_partner_totals(self):
        p = self.partnerf.create()
        tc = self.taxcodef.create()
        fy1 = self.fiscalyearf.create()
        fy1.wobj.create_period([fy1])
        ml = self.movelinef.create({
            'partner_id': p,
            'tax_code_id': tc,
            'tax_amount': 100,
            'period_id': fy1.browse.period_ids[0].id,
        })
        tot = tc.wobj.get_partner_totals([tc], p, fy1)
        self.assertEqual(tot[tc], 100)

    def test_ceiling_bases(self):
        p = self.partnerf.create()
        tc_under = self.taxcodef.create()
        tc_over = self.taxcodef.create()
        tc_tax = self.taxcodef.create()
        fy = self.fiscalyearf.create()
        fy.wobj.create_period([fy])
        pt = self.partnerf.create()
        tx = self.taxf.create({
            'amount_param_id': None,
            'base_ceiling_param_id': {
                'item_ids': [(0, 0, {'amount': 100, 'fiscalyear_id': fy})]
            },
            'base_code_id': tc_under,
            'over_ceil_base_code_id': tc_over,
            'tax_code_id': tc_tax,
            'amount': 0.1,
            'type': 'percent',
        })
        ml = self.movelinef.create({
            'partner_id': pt,
            'account_tax_id': tx,
            'debit': 250,
            'move_id': {
                'period_id': fy.browse.period_ids[0].id,
            }
        })
        tcunder_fy = tc_under.wobj.browse(tc_under, context={'fiscalyear_id': fy})
        tcover_fy = tc_over.wobj.browse(tc_over, context={'fiscalyear_id': fy})
        tctax_fy = tc_tax.wobj.browse(tc_tax, context={'fiscalyear_id': fy})
        self.assertEqual(tcunder_fy.sum, 100)
        self.assertEqual(tcover_fy.sum, 150)
        self.assertEqual(tctax_fy.sum, 100 * 0.1)

    def test_ceiling_invoice(self):
        tc_under = self.taxcodef.create()
        tc_over = self.taxcodef.create()
        tc_tax = self.taxcodef.create()
        fy = self.fiscalyearf.create()
        fy.wobj.create_period([fy])
        pt = self.partnerf.create()
        tx = self.taxf.create({
            'amount_param_id': None,
            'base_ceiling_param_id': {
                'item_ids': [(0, 0, {'amount': 100, 'fiscalyear_id': fy})]
            },
            'base_code_id': tc_under,
            'over_ceil_base_code_id': tc_over,
            'tax_code_id': tc_tax,
            'amount': 0.1,
            'type': 'percent',
        })
        inv = self.invoicef.create({
            'invoice_line': [(0, 0, {
                'invoice_line_tax_id': [(6, 0, [tx])],
                'price_unit': 100,
            }),
            (0,0, {
                'invoice_line_tax_id': [(6, 0, [tx])],
                'price_unit': 100,
                'quantity': 2,
            })
            ],
            'type': 'in_invoice',
            'date_invoice': fy.browse.date_start,
        })
        inv.wobj._workflow_signal([inv], 'invoice_open')
        under_fy = tc_under.wobj.browse(tc_under, context={'fiscalyear_id': fy})
        over_fy = tc_over.wobj.browse(tc_over, context={'fiscalyear_id': fy})
        tax_fy = tc_tax.wobj.browse(tc_tax, context={'fiscalyear_id': fy})
        self.assertEqual(under_fy.sum, 100)
        self.assertEqual(over_fy.sum, 200)
        self.assertEqual(tax_fy.sum, 100*0.1)

{'taxes': [{'account_analytic_collected_id': False,
            'account_analytic_paid_id': False,
            'account_collected_id': 2630,
            'account_paid_id': 2631,
            'amount': 20.0,
            'base_code_id': 3346,
            'base_sign': 1.0,
            'id': 741,
            'name': u'tax13',
            'price_unit': 200.0,
            'ref_base_code_id': 3344,
            'ref_base_sign': 1.0,
            'ref_tax_code_id': 3345,
            'ref_tax_sign': 1.0,
            'sequence': 1,
            'tax_code_id': 3347,
            'tax_sign': 1.0}],
 'total': 200.0,
 'total_included': 220.0}