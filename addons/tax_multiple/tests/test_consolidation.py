from openerp.tests.common import TransactionCase
from tax_multiple.consolidate import consolidate_for_update, consolidate_taxes, consolidate_for_create
from testutils.factories.account import MoveFactory, AccountFactory, TaxCodeFactory
import unittest2



class TestConsolidation(TransactionCase):
    move_fc = MoveFactory()
    account_fc = AccountFactory()
    tax_code_fc = TaxCodeFactory()

    def test_consolidation_same_code(self):
        a1 = self.account_fc.create()
        tc1 = self.tax_code_fc.create()
        m = self.move_fc.create({
            'line_id': [
                (0, 0, {
                    'account_id': a1,
                    'debit': 10,
                    'tax_code_id': tc1,
                    'tax_amount': 10,
                }),
                (0, 0, {
                    'account_id': a1,
                    'tax_code_id': tc1,
                    'tax_amount': 10,
                    'debit': 0,
                    'credit': 0,
                })
            ]
        })
        self.move_fc.wobj.consolidate_tax([m])
        m = self.move_fc.wobj.browse(m)
        self.assertEqual(len(m.line_id), 1)
        ml = m.line_id[0]
        self.assertEqual(len(ml.tax_ids), 1)
        self.assertEqual((ml.tax_code_id.id, ml.tax_amount), (tc1, 20))

    def test_consolidation_different_codes(self):
        a1 = self.account_fc.create()
        tc1 = self.tax_code_fc.create()
        tc2 = self.tax_code_fc.create()
        m = self.move_fc.create({
            'line_id': [
                (0, 0, {
                    'account_id': a1,
                    'debit': 10,
                    'tax_code_id': tc1,
                    'tax_amount': 10,
                }),
                (0, 0, {
                    'account_id': a1,
                    'tax_code_id': tc2,
                    'tax_amount': 10,
                    'debit': 0,
                    'credit': 0,
                })
            ]
        })
        self.move_fc.wobj.consolidate_tax([m])
        m = self.move_fc.wobj.browse(m)
        self.assertEqual(len(m.line_id), 1)
        ml = m.line_id[0]
        self.assertEqual(len(ml.tax_ids), 2)
        self.assertEqual(sum([t.tax_amount for t in ml.tax_ids]), 20)





class TestConsolVal(unittest2.TestCase):
    def test_consolidation_for_update(self):
        tupd, tdel = consolidate_for_update(lines_different_codes)
        self.assertListEqual(tdel, [927])
        self.assertDictEqual(tupd, {926: [1073, 1074]})

    def test_consolidate_taxes_from_different_lines(self):
        tax_lines = [
            {
                'id': 1,
                'move_line_id': 1,
                'tax_code_id': 1,
                'tax_amount': 10,
            },
            {
                'id': 2,
                'move_line_id': 1,
                'tax_code_id': 1,
                'tax_amount': 10,
            },
            {
                'id': 3,
                'move_line_id': 2,
                'tax_code_id': 1,
                'tax_amount': 10,
            },
            {
                'id': 3,
                'move_line_id': 2,
                'tax_code_id': 1,
                'tax_amount': 10,
            },
        ]
        expected = [
            {
                'move_line_id': 1,
                'tax_code_id': 1,
                'tax_amount': 20
            },
            {
                'move_line_id': 2,
                'tax_code_id': 1,
                'tax_amount': 20,
            }
        ]
        res = consolidate_taxes(tax_lines)
        self.assertListEqual(sorted(expected), sorted(res))

    def test_consolidate_taxes_with_no_move_line_id(self):
        tax_lines = [
            {
                'tax_code_id': 1,
                'tax_amount': 10,
            },
            {
                'tax_code_id': 1,
                'tax_amount': 10,
            },
        ]
        expected = [
            {
                'tax_code_id': 1,
                'tax_amount': 20,
            }
        ]
        res = consolidate_taxes(tax_lines)
        self.assertListEqual(res, expected)

    def test_consolidate_no_update(self):
        tax_lines = [
            {
                'tax_code_id': 1,
                'tax_amount': 10
            }
        ]
        res = consolidate_taxes(tax_lines)
        self.assertIsNone(res)

    def test_consolidate_orm_read(self):
        tax_lines =[
            {
                'move_line_id': (1094, u'move16'),
                'tax_code_id': (1756, u'tax account16'),
                'tax_amount': 10.0,
                'id': 1279
            },
            {
                'move_line_id': (1094, u'move16'),
                'tax_code_id': (1756, u'tax account16'),
                'tax_amount': 10.0,
                'id': 1280
            }
        ]

        expected = [
            {
                'move_line_id': 1094,
                'tax_code_id': 1756,
                'tax_amount': 20
            },
        ]
        res = consolidate_taxes(tax_lines)
        self.assertListEqual(res, expected)

    def test_consolidate_multiple(self):
        tax_upd, line_del = consolidate_for_update(lines2)
        self.assertEqual(len(tax_upd), 2)
        self.assertEqual(len(line_del), 2)
        
    def test_multiple_consol(self):
        lines = consolidate_for_create(lines_create)
        lines2 = consolidate_for_create(lines)
        self.assertListEqual(lines, lines2)


lines_different_codes = [
    {'account_id': (1601, u'code11 account11'),
     'account_tax_id': False,
     'amount_currency': 0.0,
     'amount_residual': 0.0,
     'amount_residual_currency': 0.0,
     'analytic_account_id': False,
     'analytic_lines': [],
     'balance': 0.0,
     'blocked': False,
     'centralisation': u'normal',
     'company_id': (1, u'Thema Italia'),
     'credit': 0.0,
     'currency_id': False,
     'date': '2014-04-05',
     'date_created': '2014-04-05',
     'date_maturity': False,
     'debit': 0.0,
     'id': 927,
     'invoice': (False, ''),
     'journal_id': (927, u'journal13 (EUR)'),
     'move_id': (937, '*937'),
     'name': u'move12',
     'narration': False,
     'partner_id': False,
     'period_id': (5, u'04/2014'),
     'product_id': False,
     'product_uom_id': False,
     'quantity': 0.0,
     'reconcile': False,
     'reconcile_id': False,
     'reconcile_partial_id': False,
     'ref': False,
     'state': u'draft',
     'statement_id': False,
     'tax_amount': 10.0,
     'tax_code_id': (1564, u'tax account15'),
     'tax_ids': [1074]},
    {'account_id': (1601, u'code11 account11'),
     'account_tax_id': False,
     'amount_currency': 0.0,
     'amount_residual': 0.0,
     'amount_residual_currency': 0.0,
     'analytic_account_id': False,
     'analytic_lines': [],
     'balance': 0.0,
     'blocked': False,
     'centralisation': u'normal',
     'company_id': (1, u'Thema Italia'),
     'credit': 0.0,
     'currency_id': False,
     'date': '2014-04-05',
     'date_created': '2014-04-05',
     'date_maturity': False,
     'debit': 10.0,
     'id': 926,
     'invoice': (False, ''),
     'journal_id': (927, u'journal13 (EUR)'),
     'move_id': (937, '*937'),
     'name': u'move11',
     'narration': False,
     'partner_id': False,
     'period_id': (5, u'04/2014'),
     'product_id': False,
     'product_uom_id': False,
     'quantity': 0.0,
     'reconcile': False,
     'reconcile_id': False,
     'reconcile_partial_id': False,
     'ref': False,
     'state': u'draft',
     'statement_id': False,
     'tax_amount': 10.0,
     'tax_code_id': (1563, u'tax account14'),
     'tax_ids': [1073]}
]

lines2 = [{'analytic_lines': [], 'statement_id': False, 'journal_id': (2415, u'journal17 (EUR)'), 'currency_id': False, 'date_maturity': False, 'invoice': (False, ''), 'narration': False, 'partner_id': False, 'id': 2308, 'reconcile_partial_id': False, 'blocked': False, 'analytic_account_id': False, 'reconcile': False, 'amount_residual': 0.0, 'centralisation': u'normal', 'company_id': (1, u'Thema Italia'), 'tax_code_id': (3302, u'tax account26'), 'state': u'draft', 'amount_residual_currency': 0.0, 'debit': 10.0, 'ref': False, 'tax_ids': [2788], 'account_id': (2870, u'code13 account13'), 'period_id': (5, u'04/2014'), 'date_created': '2014-04-06', 'date': '2014-04-06', 'move_id': (2429, '*2429'), 'name': u'move15 tax3', 'reconcile_id': False, 'tax_amount': 10.0, 'product_id': False, 'account_tax_id': False, 'product_uom_id': False, 'credit': 0.0, 'amount_currency': 0.0, 'balance': 0.0, 'quantity': 0.0}, {'analytic_lines': [], 'statement_id': False, 'journal_id': (2415, u'journal17 (EUR)'), 'currency_id': False, 'date_maturity': False, 'invoice': (False, ''), 'narration': False, 'partner_id': False, 'id': 2307, 'reconcile_partial_id': False, 'blocked': False, 'analytic_account_id': False, 'reconcile': False, 'amount_residual': 0.0, 'centralisation': u'normal', 'company_id': (1, u'Thema Italia'), 'tax_code_id': (3301, u'tax account25'), 'state': u'draft', 'amount_residual_currency': 0.0, 'debit': 0.0, 'ref': False, 'tax_ids': [2787], 'account_id': (2873, u'code16 account16'), 'period_id': (5, u'04/2014'), 'date_created': '2014-04-06', 'date': '2014-04-06', 'move_id': (2429, '*2429'), 'name': u'move15 tax3', 'reconcile_id': False, 'tax_amount': 100.0, 'product_id': False, 'account_tax_id': False, 'product_uom_id': False, 'credit': 0.0, 'amount_currency': 0.0, 'balance': 0.0, 'quantity': 0.0}, {'analytic_lines': [], 'statement_id': False, 'journal_id': (2415, u'journal17 (EUR)'), 'currency_id': False, 'date_maturity': False, 'invoice': (False, ''), 'narration': False, 'partner_id': False, 'id': 2306, 'reconcile_partial_id': False, 'blocked': False, 'analytic_account_id': False, 'reconcile': False, 'amount_residual': 0.0, 'centralisation': u'normal', 'company_id': (1, u'Thema Italia'), 'tax_code_id': (3298, u'tax account22'), 'state': u'draft', 'amount_residual_currency': 0.0, 'debit': 0.0, 'ref': False, 'tax_ids': [2786], 'account_id': (2870, u'code13 account13'), 'period_id': (5, u'04/2014'), 'date_created': '2014-04-06', 'date': '2014-04-06', 'move_id': (2429, '*2429'), 'name': u'move15 tax2', 'reconcile_id': False, 'tax_amount': 0.0, 'product_id': False, 'account_tax_id': False, 'product_uom_id': False, 'credit': 0.0, 'amount_currency': 0.0, 'balance': 0.0, 'quantity': 0.0}, {'analytic_lines': [], 'statement_id': False, 'journal_id': (2415, u'journal17 (EUR)'), 'currency_id': False, 'date_maturity': False, 'invoice': (False, ''), 'narration': False, 'partner_id': False, 'id': 2305, 'reconcile_partial_id': False, 'blocked': False, 'analytic_account_id': False, 'reconcile': False, 'amount_residual': 0.0, 'centralisation': u'normal', 'company_id': (1, u'Thema Italia'), 'tax_code_id': (3297, u'tax account21'), 'state': u'draft', 'amount_residual_currency': 0.0, 'debit': 100.0, 'ref': False, 'tax_ids': [2785], 'account_id': (2873, u'code16 account16'), 'period_id': (5, u'04/2014'), 'date_created': '2014-04-06', 'date': '2014-04-06', 'move_id': (2429, '*2429'), 'name': u'move15', 'reconcile_id': False, 'tax_amount': 100.0, 'product_id': False, 'account_tax_id': (344, u'tax1'), 'product_uom_id': False, 'credit': 0.0, 'amount_currency': 0.0, 'balance': 0.0, 'quantity': 0.0}]

lines_create = [(0, 0, {'analytic_account_id': False, 'tax_code_id': 5433, 'analytic_lines': [], 'tax_amount': 100.0, 'name': u'Invoice line 1', 'ref': '', 'currency_id': False, 'credit': 100.0, 'product_id': False, 'date_maturity': False, 'debit': False, 'date': '2014-04-06', 'amount_currency': 0, 'product_uom_id': False, 'quantity': 1.0, 'partner_id': 53, 'account_id': 260}), (0, 0, {'analytic_account_id': False, 'tax_code_id': 5437, 'analytic_lines': [], 'tax_amount': 100.0, 'name': u'Invoice line 1', 'ref': '', 'currency_id': False, 'credit': False, 'product_id': False, 'date_maturity': False, 'debit': False, 'date': '2014-04-06', 'amount_currency': 0, 'product_uom_id': False, 'quantity': 1.0, 'partner_id': 53, 'account_id': 260}), (0, 0, {'analytic_account_id': None, 'tax_code_id': 5434, 'analytic_lines': [], 'tax_amount': 10.0, 'name': u'tax7', 'ref': '', 'currency_id': False, 'credit': 10.0, 'product_id': False, 'date_maturity': False, 'debit': False, 'date': '2014-04-06', 'amount_currency': 0, 'product_uom_id': False, 'quantity': 1, 'partner_id': 53, 'account_id': 4058}), (0, 0, {'analytic_account_id': None, 'tax_code_id': 5438, 'analytic_lines': [], 'tax_amount': 10.0, 'name': u'tax8', 'ref': '', 'currency_id': False, 'credit': 10.0, 'product_id': False, 'date_maturity': False, 'debit': False, 'date': '2014-04-06', 'amount_currency': 0, 'product_uom_id': False, 'quantity': 1, 'partner_id': 53, 'account_id': 4060}), (0, 0, {'analytic_account_id': False, 'tax_code_id': False, 'analytic_lines': [], 'tax_amount': False, 'name': '/', 'ref': '', 'currency_id': False, 'credit': False, 'product_id': False, 'date_maturity': '2014-04-06', 'debit': 120.0, 'date': '2014-04-06', 'amount_currency': 0, 'product_uom_id': False, 'quantity': 1.0, 'partner_id': 53, 'account_id': 4062})]