from openerp.osv import orm, fields
from openerp import tools
from tax_multiple.consolidate import consolidate_for_create, consolidate_for_update
from openerp.tools import float_utils
from openerp.addons.decimal_precision import decimal_precision as dp

class account_move_line(orm.Model):
    _inherit = 'account.move.line'

    def create(self, cr, uid, vals, context=None, check=True):
        context = context or {}
        tax_id = vals.pop('account_tax_id', None)   # we'll handle taxes here
        result = super(account_move_line, self).create(cr, uid, vals, context=context, check=check)

        #some taxes may need the partner, we pass it in context
        if 'partner_id' in vals:
            context['partner_id'] = vals.get('partner_id')

        cr_browse = self.browse(cr, uid, result, context=context)
        tax_obj = self.pool.get('account.tax')
        journal = cr_browse.journal_id
        move_obj = self.pool.get('account.move')
        if tax_id:
            tax_id = tax_obj.browse(cr, uid, tax_id, context=context)
            total = vals['debit'] - vals['credit']
            if journal.type in ('purchase_refund', 'sale_refund'):
                base_code = 'ref_base_code_id'
                tax_code = 'ref_tax_code_id'
                account_id = 'account_paid_id'
                base_sign = 'ref_base_sign'
                tax_sign = 'ref_tax_sign'
            else:
                base_code = 'base_code_id'
                tax_code = 'tax_code_id'
                account_id = 'account_collected_id'
                base_sign = 'base_sign'
                tax_sign = 'tax_sign'
            tmp_cnt = 0
            tax_moves = []
            prec = dp.get_precision('Account')(cr)[1]

            for tax in tax_obj.compute_all(cr, uid, [tax_id], total, 1.00, force_excluded=True).get('taxes'):
                #create the base movement
                base = tax['price_unit']
                if tmp_cnt == 0 and float_utils.float_compare(base, abs(total), precision_digits=prec) == 0:
                    if tax[base_code]:
                        tmp_cnt += 1
                        self.write(cr, uid,[result], {
                            'tax_code_id': tax[base_code],
                            'tax_amount': tax[base_sign] * abs(total)
                        })
                else:
                    data = {
                        'move_id': vals['move_id'],
                        'name': tools.ustr(vals['name'] or '') + ' ' + tools.ustr(tax['name'] or ''),
                        'date': vals['date'],
                        'partner_id': vals.get('partner_id',False),
                        'ref': vals.get('ref',False),
                        'account_tax_id': False,
                        'tax_code_id': tax[base_code],
                        'tax_amount': tax[base_sign] * base,
                        'account_id': vals['account_id'],
                        'credit': 0.0,
                        'debit': 0.0,
                    }
                    if data['tax_code_id']:
                        self.create(cr, uid, data, context=context)
                #create the Tax movement
                data = {
                    'move_id': vals['move_id'],
                    'name': tools.ustr(vals['name'] or '') + ' ' + tools.ustr(tax['name'] or ''),
                    'date': vals['date'],
                    'partner_id': vals.get('partner_id',False),
                    'ref': vals.get('ref',False),
                    'account_tax_id': False,
                    'tax_code_id': tax[tax_code],
                    'tax_amount': tax[tax_sign] * abs(tax['amount']),
                    'account_id': tax[account_id] or vals['account_id'],
                    'credit': tax['amount']<0 and -tax['amount'] or 0.0,
                    'debit': tax['amount']>0 and tax['amount'] or 0.0,
                }
                if data['tax_code_id']:
                    self.create(cr, uid, data, context=context)
            move_obj.consolidate_tax(cr, uid, [cr_browse.move_id.id], context=context)

        if check and not context.get('novalidate') and ((not context.get('no_store_function')) or journal.entry_posted):
            tmp = move_obj.validate(cr, uid, [vals['move_id']], context)
            if journal.entry_posted and tmp:
                move_obj.button_validate(cr,uid, [vals['move_id']], context)

        return result

