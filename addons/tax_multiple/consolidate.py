def equals(l1, l2):
    ignore_fields = ('debit', 'credit', 'id', 'tax_code_id',
                     'tax_ids', 'tax_amount', 'name', 'account_tax_id')
    l1, l2 = l1.copy(), l2.copy()
    for f in ignore_fields:
        l1.pop(f, None); l2.pop(f, None)
    #for k, v in l1.items():
    #    if l2.get(k) != v:
    #        print 'field %s is different: %s vs %s' % (k, v, l2.get(k))
    return l1 == l2

def find_zero(lines):
    '''splits a list of dicts containing line values in those that have debit==credit==, and those that dont.

    :return: two lists, the first with debit and credit 0, the second with the rest.'''
    noamount = [v for v in lines if v.get('credit', 0) == v['debit'] == 0]
    lines = [v for v in lines if v['credit'] != 0 or v['debit'] != 0]
    return noamount, lines

def consolidate_for_update(lines):
    '''
    calculates the update to be done on a set of move lines to remove lines with debit==credit==0 that are there
    only for the purpose of their tax.

    :param lines: a list of dicts containing move line values
    :return: tax_upd, line_del, where tax_upd is a dict mapping a move line id to a list of move line tax ids
     that should be set for that line; and tax_del is a list of line tax ids that should be deleted
    '''
    tax_upd = {}
    line_del = []
    noamount, lines = find_zero(lines)
    for line0 in noamount:
        for line in lines:
            if equals(line0, line):
                tax_upd.setdefault(line['id'], []).extend(line['tax_ids'] + line0['tax_ids'])
                line_del.append(line0['id'])
                break
    return tax_upd, line_del

def consolidate_taxes(line_taxes):
    '''
    calculates the update to be done on a set of move line tax ids that belong to the same move line,
    to group them by tax code (summing the amounts).
    :param line_taxes: a list of dicts containing move line tax values
    :return: None if no update is needed, otherwise a list of move line tax values dicts that should
    replace the move line taxes in line_taxes.
    '''
    lines_by_code_by_move = {}
    noupdate = True
    for line in line_taxes:
        lines_by_code_by_move.setdefault(line.get('move_line_id'), {}).\
            setdefault(line.get('tax_code_id'), []).append(line)
    res = []
    for move, taxes in lines_by_code_by_move.items():
        for code, lines in taxes.items():
            if len(lines) > 1:
                noupdate = False
            r = {
                'tax_amount': sum(l['tax_amount'] for l in lines)
            }
            if move:
                r['move_line_id'] = move if isinstance(move, (int, long)) else move[0]
            if code:
                r['tax_code_id'] = code if isinstance(code, (int, long)) else code[0]
            res.append(r)
    return None if noupdate else res

def clean_tax_values(line):
    tc, ta = line.pop('tax_code_id', False), line.pop('tax_amount', 0)
    if 'tax_ids' not in line:
        line['tax_ids'] = [(0, 0, {'tax_code_id': tc, 'tax_amount': ta})]
    return line


def consolidate_for_create(lines):
    assert all([l[0] == l[1] == 0 for l in lines]), 'Line 3-tuples are not for create'
    lines = [l[2] for l in lines]
    lines = map(clean_tax_values, lines)
    noamount, lines = find_zero(lines)
    res = []
    for line0 in noamount:
        for line in lines:
            if equals(line0, line):
                line['tax_ids'].extend(line0['tax_ids'])
                break
        else:
            res.append(line0)
    res.extend(lines)
    return [(0, 0, l) for l in res]



