
from openerp.osv import orm, fields

class account_tax_code(orm.Model):
    _inherit = 'account.tax.code'

    def _sum(self, cr, uid, ids, name, args, context, where ='', where_params=()):
        parent_ids = tuple(self.search(cr, uid, [('parent_id', 'child_of', ids)]))
        invjoin, invwhere = '', ''

        if context.get('based_on', 'invoices') == 'payments':
            invjoin = 'LEFT JOIN account_invoice invoice ON invoice.move_id move.id'
            invwhere = "AND ((invoice.state = 'paid' ) OR invoice.id IS NULL)"

        q = '''
            SELECT tx.tax_code_id, COALESCE(sum(tx.tax_amount), 0)
            FROM account_move_line line
            INNER JOIN account_move move on move.id = line.move_id
            INNER JOIN account_move_line_tax tx on tx.move_line_id = line.id
            {invjoin}
            WHERE tx.tax_code_id IN %s
            {invwhere}
            {where}
            GROUP BY tx.tax_code_id
        '''.format(**locals())

        cr.execute(q, (parent_ids, ) + where_params)
        res=dict(cr.fetchall())
        obj_precision = self.pool.get('decimal.precision')
        res2 = {}
        for record in self.browse(cr, uid, ids, context=context):
            def _rec_get(record):
                amount = res.get(record.id, 0.0)
                for rec in record.child_ids:
                    amount += _rec_get(rec) * rec.sign
                return amount
            res2[record.id] = round(_rec_get(record), obj_precision.precision_get(cr, uid, 'Account'))
        return res2

account_tax_code()