# -*- coding: utf-8 -*-

{
    'name': 'Multiple taxes per move line',
    'version': '0.1',
    'category': 'Accounting',
    'description': """Allow multiple taxes for a single move line.""",
    'author': 'Thema Optical SRL',
    'website': 'http://www.thema-optical.com',
    'depends': ['base', 'account', 'account_hooks', 'client_improv'],
    'init_xml': [],
    'data':[
        'init.xml',
        'ir.model.access.csv',
        'views/move_line_view.xml',
        #'views/tax_view.xml',
        #'views/invoice_view.xml',
        ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': True,
}
