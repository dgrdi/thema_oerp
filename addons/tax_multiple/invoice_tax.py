from openerp.osv import orm, fields
from openerp.addons import decimal_precision as dp


class invoice_line(orm.Model):
    _inherit = 'account.invoice.line'

    _columns = {
        'tax_rate': fields.function(lambda s, *a, **k: s._tax_rate(*a, **k),
                                    type='float',
                                    digits_compute=dp.get_precision('Account'),
                                    string='Aliquota IVA',
                                    multi='tax'),
        'tax_code_id': fields.function(lambda s, *a, **k: s._tax_rate(*a, **k),
                                       type='many2one',
                                       relation='account.tax.code',
                                       string='Codice IVA',
                                       multi='tax'),
    }

    def _tax_rate(self, cr, uid, ids, field, arg, context=None):
        cr.execute("""
            SELECT l.id,
               t.amount * 100 tax_rate,
               case
                when i.type in ('in_invoice', 'out_invoice') then t.tax_code_id
                else t.ref_tax_code_id
               end tax_code_id
            FROM account_invoice_line l
            INNER JOIN account_invoice i ON i.id = l.invoice_id
            INNER JOIN account_invoice_line_tax lt ON lt.invoice_line_id = l.id
            INNER JOIN account_tax t ON t.id = lt.tax_id
            WHERE l.id IN %s
        """, [tuple(ids)])
        res = {el.pop('id'): el for el in cr.dictfetchall()}
        res.update(dict.fromkeys(set(ids) - set(res), {'tax_rate': 0.0, 'tax_code_id': False}))
        return res