# coding=utf-8
from openerp.osv import orm, fields
from openerp.osv.osv import except_osv
from ceiling_wrapper import CeilingTaxWrapper
from openerp.addons.decimal_precision import decimal_precision as dp
import time


class account_tax_template(orm.Model):
    _inherit = 'account.tax.template'
    _columns = {
        'base_ceiling_param_id': fields.many2one('account.tax.parameter', 'Parametro massimale imponibile'),
        'over_ceil_base_code_id': fields.many2one('account.tax.code.template', 'Conto imponibile oltre il massimale'),
        'ref_over_ceil_base_code_id': fields.many2one('account.tax.code.template', 'Conto imponibile oltre il massimale (NC)'),
    }

    def get_tax_vals(self, cr, uid, tax_template, tax_template_to_tax, tax_code_template_ref, context=None):
        res = super(account_tax_template, self).get_tax_vals(cr, uid, tax_template, tax_template_to_tax, tax_code_template_ref, context)
        res.update({
            'base_ceiling_param_id': tax_template.base_ceiling_param_id.id,
            'over_ceil_base_code_id': tax_code_template_ref[tax_template.over_ceil_base_code_id.id],
            'ref_over_ceil_base_code_id': tax_code_template_ref[tax_template.ref_over_ceil_base_code_id.id],
        })
        return res


class account_tax(orm.Model):
    _inherit = 'account.tax'

    def _field_ceiling(self, cr, uid, ids, field_name, arg, context):
        res = {}
        param_obj = self.pool.get('account.tax.parameter')
        fyear_obj = self.pool.get('account.fiscalyear')
        fiscalyear_id = context.get('fiscalyear_id')
        if not fiscalyear_id and 'period_id' in context:
            fiscalyear_id = self.pool.get('account.period').browse(cr, uid, context['period_id'],
                                                                   context=context).fiscalyear_id.id
        if not fiscalyear_id:
            fiscalyear_id = fyear_obj.find(cr, uid, context=context)
        for tx in self.browse(cr, uid, ids, context=context):
            if not tx.base_ceiling_param_id:
                res[tx.id] = False
            else:
                res[tx.id] = param_obj.get(cr, uid, tx.base_ceiling_param_id.id, fiscalyear_id, context=context)
        return res

    _columns = {
        'base_ceiling_param_id': fields.many2one('account.tax.parameter', 'Parametro massimale imponibile'),
        'over_ceil_base_code_id': fields.many2one('account.tax.code', 'Conto imponibile oltre il massimale'),
        'ref_over_ceil_base_code_id': fields.many2one('account.tax.code', 'Conto imponibile oltre il massimale (NC)'),
        'base_ceiling': fields.function(_field_ceiling, type='float', string='Massimale imponibile'),
    }

    def compute_all(self, cr, uid, taxes, price_unit, quantity, product=None, partner=None, force_excluded=False):
        taxes_rec = []
        fy_obj = self.pool.get('account.fiscalyear')
        tots = {}
        ceils = {}
        for t in taxes:
            if t.base_ceiling_param_id:
                fiscalyear_id = t._context.get('fiscalyear_id')
                if not fiscalyear_id and 'period_id' in t._context:
                    fiscalyear_id = self.pool.get('account.period').browse(cr, uid, t._context['period_id']).\
                        fiscalyear_id.id
                if not fiscalyear_id:
                    fiscalyear_id = fy_obj.find(cr, uid)
                if partner is None and 'partner_id' in t._context:
                    partner_id = t._context.get('partner_id')
                elif partner:
                    partner_id = partner.id
                else:
                    raise except_osv(u'Errore!', u"Per l'imposta '%s' è necessario impostare un Partner." % t.name)
                cur_tot = self.pool.get('account.tax.code').get_partner_totals(cr, uid, [t.base_code_id.id],
                                                                               partner_id,
                                                                               fiscalyear_id,
                                                                               context=t._context)[t.base_code_id.id]
                tot = price_unit * quantity
                ceil = t.base_ceiling
                under, over = ceil - cur_tot, cur_tot + tot - ceil
                ceils[t.id] = ceil
                tots[t.id] = cur_tot
                if over > 0:
                    taxes_rec.append(CeilingTaxWrapper(t, under, over))
                    continue
            taxes_rec.append(t)
        res= super(account_tax, self).compute_all(cr, uid, taxes_rec, price_unit, quantity, product, partner, force_excluded)
        for t in res['taxes']:
            t['base_ceiling'] = ceils.get(t['id'], 0.0)
            t['yearly_tot'] = tots.get(t['id'], 0.0)
        return res


class account_tax_code(orm.Model):
    _inherit = 'account.tax.code'

    def get_partner_totals(self, cr, uid, ids, partner_id, fiscalyear_id, context=None):
        context = context or {}
        cr.execute('''
            SELECT mlt.tax_code_id, COALESCE(SUM(mlt.tax_amount), 0)
            FROM account_move_line_tax mlt
            INNER JOIN account_move_line ml ON ml.id = mlt.move_line_id
            INNER JOIN account_period p ON p.id = ml.period_id
            WHERE mlt.tax_code_id IN %s
            AND ml.partner_id = %s
            AND p.fiscalyear_id = %s
            GROUP BY mlt.tax_code_id
        ''', [tuple(ids), partner_id, fiscalyear_id])
        res =  dict(cr.fetchall())
        res.update({id: 0.0 for id in ids if id not in res})
        rtot = context.get('partner_tax_running_tot', {})   # set in account.invoice.tax
        for k, v in rtot.items():
            if k in res:
                res[k] += v
        return res

class account_invoice_tax(orm.Model):
    _inherit = 'account.invoice.tax'

    _columns = {
        'base_ceiling': fields.float('Massimale', digits_compute=dp.get_precision('Account')),
        'yearly_tot': fields.float('Tot. imp. precedente', digits_compute=dp.get_precision('Account')),
    }

    def compute(self, cr, uid, invoice_id, context=None):
        # for taxes that have ceilings, we might go over it between lines - we need to keep a running total
        context = context or {}
        rtot = context['partner_tax_running_tot'] = {}

        tax_grouped = {}
        tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')
        inv = self.pool.get('account.invoice').browse(cr, uid, invoice_id, context=context)
        cur = inv.currency_id
        company_currency = self.pool['res.company'].browse(cr, uid, inv.company_id.id).currency_id.id

        for line in inv.invoice_line:
            for tax in tax_obj.compute_all(cr, uid, line.invoice_line_tax_id, (line.price_unit* (1-(line.discount or 0.0)/100.0)), line.quantity, line.product_id, inv.partner_id)['taxes']:
                val={}
                val['invoice_id'] = inv.id
                val['name'] = tax['name']
                val['amount'] = tax['amount']
                val['manual'] = False
                val['sequence'] = tax['sequence']
                val['base'] = cur_obj.round(cr, uid, cur, tax['price_unit'] * line['quantity'])
                val['base_ceiling'] = tax['base_ceiling']
                val['yearly_tot'] = tax['yearly_tot']

                if inv.type in ('out_invoice','in_invoice'):
                    val['base_code_id'] = tax['base_code_id']
                    val['tax_code_id'] = tax['tax_code_id']
                    val['base_amount'] = cur_obj.compute(cr, uid, inv.currency_id.id, company_currency, val['base'] * tax['base_sign'], context={'date': inv.date_invoice or time.strftime('%Y-%m-%d')}, round=False)
                    val['tax_amount'] = cur_obj.compute(cr, uid, inv.currency_id.id, company_currency, val['amount'] * tax['tax_sign'], context={'date': inv.date_invoice or time.strftime('%Y-%m-%d')}, round=False)
                    val['account_id'] = tax['account_collected_id'] or line.account_id.id
                    val['account_analytic_id'] = tax['account_analytic_collected_id']
                else:
                    val['base_code_id'] = tax['ref_base_code_id']
                    val['tax_code_id'] = tax['ref_tax_code_id']
                    val['base_amount'] = cur_obj.compute(cr, uid, inv.currency_id.id, company_currency, val['base'] * tax['ref_base_sign'], context={'date': inv.date_invoice or time.strftime('%Y-%m-%d')}, round=False)
                    val['tax_amount'] = cur_obj.compute(cr, uid, inv.currency_id.id, company_currency, val['amount'] * tax['ref_tax_sign'], context={'date': inv.date_invoice or time.strftime('%Y-%m-%d')}, round=False)
                    val['account_id'] = tax['account_paid_id'] or line.account_id.id
                    val['account_analytic_id'] = tax['account_analytic_paid_id']

                if val['base_code_id'] in rtot:
                    rtot[val['base_code_id']] += val['base_amount']
                else:
                    rtot[val['base_code_id']] = val['base_amount']

                key = (val['tax_code_id'], val['base_code_id'], val['account_id'], val['account_analytic_id'])
                if not key in tax_grouped:
                    tax_grouped[key] = val
                else:
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base'] += val['base']
                    tax_grouped[key]['base_amount'] += val['base_amount']
                    tax_grouped[key]['tax_amount'] += val['tax_amount']

        for t in tax_grouped.values():
            t['base'] = cur_obj.round(cr, uid, cur, t['base'])
            t['amount'] = cur_obj.round(cr, uid, cur, t['amount'])
            t['base_amount'] = cur_obj.round(cr, uid, cur, t['base_amount'])
            t['tax_amount'] = cur_obj.round(cr, uid, cur, t['tax_amount'])

        context.pop('partner_tax_running_tot')

        return tax_grouped

class account_invoice_line(orm.Model):
    _inherit = 'account.invoice.line'

    def move_line_get(self, cr, uid, invoice_id, context=None):
        context = context or {}
        if 'fiscalyear_id' not in context:
            context['fiscalyear_id'] = self.pool.get('account.invoice').\
                browse(cr, uid, invoice_id, context=context).fiscalyear_id.id
        ptot = context['partner_tax_running_tot'] = {}
        res = []
        tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')
        if context is None:
            context = {}
        inv = self.pool.get('account.invoice').browse(cr, uid, invoice_id, context=context)
        company_currency = self.pool['res.company'].browse(cr, uid, inv.company_id.id).currency_id.id
        for line in inv.invoice_line:
            mres = self.move_line_get_item(cr, uid, line, context)
            if not mres:
                continue
            res.append(mres)
            tax_code_found= False
            for tax in tax_obj.compute_all(cr, uid, line.invoice_line_tax_id,
                    (line.price_unit * (1.0 - (line['discount'] or 0.0) / 100.0)),
                    line.quantity, line.product_id,
                    inv.partner_id)['taxes']:
                if inv.type in ('out_invoice', 'in_invoice'):
                    tax_code_id = tax['base_code_id']
                    tax_amount = tax['price_unit'] * tax['base_sign']
                else:
                    tax_code_id = tax['ref_base_code_id']
                    tax_amount = tax['price_unit'] * tax['ref_base_sign']

                if tax_code_id in ptot:
                    ptot[tax_code_id] += tax_amount
                else:
                    ptot[tax_code_id] = tax_amount

                if tax_code_found:
                    if not tax_code_id:
                        continue
                    res.append(self.move_line_get_item(cr, uid, line, context))
                    res[-1]['price'] = 0.0
                    res[-1]['account_analytic_id'] = False
                elif not tax_code_id:
                    continue
                tax_code_found = True

                res[-1]['tax_code_id'] = tax_code_id
                res[-1]['tax_amount'] = cur_obj.compute(cr, uid, inv.currency_id.id, company_currency, tax_amount, context={'date': inv.date_invoice})
        context.pop('partner_tax_running_tot')
        return res


