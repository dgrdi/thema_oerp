from openerp.osv import orm, fields
from openerp.addons.decimal_precision import decimal_precision
from consolidate import consolidate_for_update, consolidate_for_create, consolidate_taxes


class account_move_line_tax(orm.Model):
    _name = 'account.move.line.tax'
    _order = 'id'
    _columns = {
        'move_line_id': fields.many2one('account.move.line', 'Movimento contabile'),
        'tax_code_id': fields.many2one('account.tax.code', 'Conto imposta'),
        'tax_amount': fields.float('Importo imposta', digits_compute=decimal_precision.get_precision('Account'))
    }

    def _create_index(self, cr, uid, context=None):
        cr.execute('''
            DROP INDEX IF EXISTS account_move_line_tax_mli;
            CREATE INDEX account_move_line_tax_mli ON account_move_line_tax(move_line_id);
        ''')
        return True

class account_move_line(orm.Model):
    _inherit = 'account.move.line'

    def _get_tax(self, cr, uid, ids, field_names, arg, context=None):
        #return the first tax code (or amount) for the line
        if isinstance(ids, (int, long)):
            ids = [ids]
        taxes = self.read(cr, uid, ids, fields=('tax_ids', ), context=context)
        taxes = {t['id']: t['tax_ids'] for t in taxes}
        tax_ids = [t[0] for t in taxes.values() if t]
        tax_vals = self.pool.get('account.move.line.tax').read(cr, uid, tax_ids,
                                                               fields=field_names + ['move_line_id'], context=context)
        for t in tax_vals:
            t.pop('id')
            taxes[t.pop('move_line_id')[0]] = t
        missing_v = {
            'tax_code_id': False,
            'tax_amount': 0
        }
        for k in set(missing_v.keys()) - set(field_names):
            missing_v.pop(k)
        for k in [k for k, v in taxes.items() if not v]:
            taxes[k] = missing_v.copy()
        return taxes

    def _set_tax(self, cr, uid, ids, field_name, value, arg, context=None):
        #set the tax code or amount on the first tax for the line, or create one if it is missing;
        if isinstance(ids, (int, long)):
            ids = [ids]
        taxes = self.read(cr, uid, ids, fields=('tax_ids', ), context=context)
        val = {
            field_name: value
        }
        write_ids = []
        tax_obj = self.pool.get('account.move.line.tax')
        for t in taxes:
            if t['tax_ids']:
                write_ids.append(t['tax_ids'][0])
            elif value:
                v = val.copy()
                v.update({'move_line_id': t['id']})
                tax_obj.create(cr, uid, v, context=context)

        if not value:
            # if val is null, delete rows where both tax code and amount would become null
            other_field = 'tax_code_id' if field_name == 'tax_amount' else 'tax_amount'
            taxes = tax_obj.read(cr, uid, write_ids, fields=(other_field, ), context=context)
            delete_ids = [t['id'] for t in taxes if not t[other_field]]
            write_ids = [i for i in write_ids if i not in delete_ids]
            tax_obj.unlink(cr, uid, delete_ids, context=context)

        tax_obj.write(cr, uid, write_ids, val, context=context)

    def _search_tax(self, cr, uid, obj, name, args, context=None):
        res = []
        for field, op, val in args:
            res.append((field.replace(name, 'tax_ids.' + name), op, val))
        return res

    _columns = {
        'tax_code_id': fields.function(_get_tax,
                                       fnct_inv=_set_tax,
                                       fnct_search=_search_tax,
                                       multi='tax_multiple',
                                       type='many2one',
                                       relation='account.tax.code',
                                       string='Conto imposta'),
        'tax_amount': fields.function(_get_tax,
                                      fnct_inv=_set_tax,
                                      fnct_search=_search_tax,
                                      multi='tax_multiple',
                                      type='float',
                                      string='Importo imposta'),
        'tax_ids': fields.one2many('account.move.line.tax', 'move_line_id', 'Imposte'),
    }


class account_move(orm.Model):
    _inherit = 'account.move'

    def consolidate_tax(self, cr, uid, ids=None, context=None):
        if isinstance(ids, (int, long)):
            ids = [ids]
        line_obj = self.pool.get('account.move.line')
        domain = []
        if ids is None:
            domain.append(('move_id.state', '=', 'draft'))
        else:
            domain.append(('move_id', 'in', ids))
        domain.extend([('debit', '=', 0), ('credit', '=', 0)])
        lines = line_obj.search(cr, uid, domain, context=context)
        line_ids = line_obj.search(cr, uid, [('move_id.line_id', 'in', lines)])
        lines = line_obj.read(cr, uid, line_ids, context=context)
        tax_upd, line_del = consolidate_for_update(lines)
        tax_obj = self.pool.get('account.move.line.tax')
        for line_id, taxes in tax_upd.items():
            tax_obj.write(cr, uid, taxes, {'move_line_id': line_id})
        line_obj.unlink(cr, uid, line_del)
        tax_ids = tax_obj.search(cr, uid, [('move_line_id', 'in', line_ids)], context=context)
        taxes = tax_obj.read(cr, uid, tax_ids, context=context)
        taxes = consolidate_taxes(taxes)
        if taxes:
            tax_obj.unlink(cr, uid, tax_ids, context=context)
            for t in taxes:
                tax_obj.create(cr, uid, t, context=context)
        return True

