# coding=utf-8
from openerp.osv import orm, fields
from openerp.osv.osv import except_osv
from openerp import pooler, SUPERUSER_ID
from openerp.addons.decimal_precision import decimal_precision as dp
from operator import itemgetter
import inspect

def get_precision_tax():
    def change_digit_tax(cr):
        res = pooler.get_pool(cr.dbname).get('decimal.precision').precision_get(cr, SUPERUSER_ID, 'Account')
        return (16, res+2)
    return change_digit_tax

class account_tax_parameter(orm.Model):
    _name = 'account.tax.parameter'
    _description = 'Parametri fiscali'

    _columns = {
        'name': fields.char('Nome'),
        'item_ids': fields.one2many('account.tax.parameter.item', 'param_id', 'Valori')
    }

    def get(self, cr, uid, id, fiscalyear_id, context=None):
        itmobj = self.pool.get('account.tax.parameter.item')
        res = itmobj.search(cr, uid, [('param_id', '=', id), ('fiscalyear_id', '=', fiscalyear_id)], context=context)
        if not res:
            fy = self.pool.get('account.fiscalyear').browse(cr, uid, fiscalyear_id, context=context)
            prm = self.browse(cr, uid, id, context=context)
            raise except_osv(u'Errore!', u"Il parametro fiscale '%s' non è definito per l'esercizio %s" % (prm.name, fy.code))
        else:
            return itmobj.read(cr, uid, res[0], context=context)['amount']


class account_tax_parameter_item(orm.Model):
    _name = 'account.tax.parameter.item'
    _description = 'Valori dei parametri fiscali'

    _columns = {
        'param_id': fields.many2one('account.tax.parameter', 'Esercizio'),
        'fiscalyear_id': fields.many2one('account.fiscalyear'),
        'amount': fields.float('Importo', digits_compute=get_precision_tax())
    }
    _sql_constraints = [
        ('item_fy_uniq', "UNIQUE (param_id, fiscalyear_id)", 'Il valore deve essere univoco per anno fiscale.'),
    ]


class account_tax_template(orm.Model):
    _inherit = 'account.tax.template'
    _columns = {
        'amount_param_id': fields.many2one('account.tax.parameter', 'Parametro fiscale importo'),
    }

    def get_tax_vals(self, cr, uid, tax_template, context=None):
        res = super(account_tax_template, self).get_tax_vals(cr, uid, tax_template, context)
        res.update({
            'amount_param_id': tax_template.amount_param_id.id,
        })


class account_tax(orm.Model):
    _inherit = 'account.tax'

    def _read_amount(self, cr, uid, ids, field_name, arg, context=None):
        context = context or {}
        fiscalyear_id = context.get('fiscalyear_id')
        period_id = context.get('period_id')
        if period_id and not fiscalyear_id:
            fiscalyear_id = self.pool.get('account.period').browse(cr, uid, period_id, context=context).fiscalyear_id.id
        if not fiscalyear_id:
            fiscalyear_id = self.pool.get('account.fiscalyear').find(cr, uid)
        paramobj = self.pool.get('account.tax.parameter')
        taxes = self.browse(cr, uid, ids, context=context)
        res = {}
        for t in taxes:
            if t.amount_param_id:
                res[t.id] = paramobj.get(cr, uid, t.amount_param_id.id, fiscalyear_id, context=context)
            else:
                res[t.id] = t.amount_fixed
        return res

    def _write_amount(self, cr, uid, ids, field_name, val, arg, context=None):
        if isinstance(ids, (int, long)):
            ids = [ids]
        for t in self.browse(cr, uid, ids, context=context):
            if not t.amount_param_id:
                self.write(cr, uid, t.id, {'amount_fixed': val})

    _columns = {
        'amount': fields.function(_read_amount, fnct_inv=_write_amount, type='float', string='Importo'),
        'amount_fixed': fields.float('Importo (valore fisso)', digits_compute=get_precision_tax()),
        'amount_param_id': fields.many2one('account.tax.parameter', 'Parametro fiscale importo')
    }


class account_invoice(orm.Model):
    _inherit = 'account.invoice'

    def _field_fiscalyear(self, cr, uid, ids, field_name, arg, context):
        res = {}
        fy = self.pool.get('account.fiscalyear')
        data = self.read(cr, uid, ids, fields=['date_invoice', 'period_id'])
        dates = set(map(itemgetter('date_invoice'), filter(lambda d: not d['period_id'], data)))
        periods = set([p[0] for p in map(itemgetter('period_id'), data) if p])
        dates = {d: fy.find(cr, uid, dt=d, context=context) for d in dates}
        periods = self.pool.get('account.period').read(cr, uid, periods, fields=['fiscalyear_id'], context=context)
        periods = {p['id']: p['fiscalyear_id'][0] for p in periods}
        for d in data:
            if d['period_id']:
                res[d['id']] = periods[d['period_id'][0]]
            elif d['date_invoice']:
                res[d['id']] = dates[d['date_invoice']]
            else:
                res[d['id']] = False
        return res

    _columns = {
        'fiscalyear_id': fields.function(_field_fiscalyear,
                                         type='many2one',
                                         relation='account.fiscalyear',
                                         string='Anno fiscale',
                                         store = {
                                             'account.invoice': (
                                                 lambda o, c, u, i, *a: i,
                                                 ('period_id', 'date_invoice'),
                                                 10
                                             )
                                         })
    }


class account_invoice_tax(orm.Model):
    _inherit = 'account.invoice.tax'

    def compute(self, cr, uid, invoice_id, context=None):
        context = context or {}
        if 'fiscalyear_id' not in context:
            inv = self.pool.get('account.invoice').browse(cr, uid, invoice_id, context=context)
            context['fiscalyear_id'] = inv.fiscalyear_id.id
        return super(account_invoice_tax, self).compute(cr, uid, invoice_id, context)

_amount_line_indir = lambda s, *a, **k: s._amount_line(*a, **k)

class account_invoice_line(orm.Model):
    _inherit = 'account.invoice.line'

    def _amount_line(self, cr, uid, ids, field_name, arg, context):
        #redefining parent because it does not pass context to browse
        context = context or {}
        lines_by_fyear = {}
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            lines_by_fyear.setdefault(line.invoice_id.fiscalyear_id.id, []).append(line.id)
        tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')
        for fiscalyear_id, line_ids in lines_by_fyear.items():
            ccontext = context.copy()
            ccontext['fiscalyear_id'] = fiscalyear_id
            for line in self.browse(cr, uid, line_ids, context=ccontext):
                price = line.price_unit * (1-(line.discount or 0.0)/100.0)
                taxes = tax_obj.compute_all(cr, uid, line.invoice_line_tax_id, price, line.quantity, product=line.product_id, partner=line.invoice_id.partner_id)
                res[line.id] = taxes['total']
                if line.invoice_id:
                    cur = line.invoice_id.currency_id
                    res[line.id] = cur_obj.round(cr, uid, cur, res[line.id])
        return res


    _columns = {
        'price_subtotal': fields.function(_amount_line_indir, string='Amount', type="float",
            digits_compute= dp.get_precision('Account'), store=True),
    }

    def move_line_get(self, cr, uid, invoice_id, context=None):
        context = context or {}
        if 'fiscalyear_id' not in context:
            context['fiscalyear_id'] = self.pool.get('account.invoice').\
                browse(cr, uid, invoice_id, context=context).fiscalyear_id.id
        return super(account_invoice_line, self).move_line_get(cr, uid, invoice_id, context)


