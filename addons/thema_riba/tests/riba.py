from unittest2.case import TestCase
from unittest2 import main
import csv
import datetime
import sys
sys.path.append('..')

from CBI_Riba import get_efdistinta_string



data = {
    'common': {
        'cod_sia': 'Y6232',
        'rag_soc': 'Thema Optical s.r.l.',
        'indirizzo': 'VIA GEI, 29',
        'localita': 'DOMEGGE DI CADORE',
        'cap_prov': '32040 BL',
        'piva': '00822840252',
        'data_creaz': datetime.date(2014,1, 3),
        'riferimento': '',
    },
    'data_2.csv': {
        'cod_abi': '5278',
        'cod_cab': '61080',
        'cod_conto': '603570411409',
        'id_distinta': '2960462',
    },
    'data_3.csv': {
        'cod_abi': '5034',
        'cod_cab': '61040',
        'cod_conto': '000000000067',
        'id_distinta': '2960477',
    },
    'data_4.csv': {
        'cod_abi': '2008',
        'cod_cab': '61080',
        'cod_conto': '000040158087',
        'id_distinta': '2960501',

    }
}

class test_riba(TestCase):
    datafiles = {
        'data_2.csv': 'riba2.txt',
        'data_3.csv': 'riba3.txt',
        'data_4.csv': 'riba4.txt',
    }

    def load_datafile(self, fname):
        fo = open(fname, 'rb')
        cfo = csv.DictReader(fo)
        slip_data = data['common'].copy()
        slip_data.update(data[fname])
        bill_data = []
        for dt in cfo:
            bill_data.append({
                'data_scad': datetime.datetime.strptime(dt['DATA_SCADENZA'][:10], '%Y-%m-%d').date(),
                'importo': float(dt['IMPORTO_EFF']),
                'cod_abi_cli': dt['CODICE_ABI'],
                'cod_cab_cli': dt['CODICE_CAB'],
                'denom_banca': dt['DESCR_BANCA'],
                'id_cliente': dt['CAMPO_RICERCA'],
                'rag_soc_cli': dt['RAG_SOCIALE'],
                'piva_cli': dt['PARTITA_IVA'],
                'indirizzo_cli': dt['INDIRIZZO'],
                'localita_prov': '{0} {1}'.format(dt['LOCALITA'], dt['PROVINCIA']),
                'cap_cli': dt['CAP'],
                'is_banca_cli': False,
                'rif_fattura': dt['RIF_FAT'],
                'rif_effetto': dt['NUMERO_EFF'],

            })
        return slip_data, bill_data

    def test_header(self):
        for fname, cfr in self.datafiles.items():
            sd, bd = self.load_datafile(fname)
            ret = get_efdistinta_string(sd, bd).split('\n')[0].strip()
            cfr = open(cfr, 'rb').readlines()[0].strip()
            try:
                self.assertEqual(ret, cfr)
            except AssertionError:
                print '\n**{0}**\n**{1}}}**'.format(ret, cfr)
                raise

    def test_tail(self):
        for fname, cfr in self.datafiles.items():
            sd, bd = self.load_datafile(fname)
            ret = get_efdistinta_string(sd, bd).split('\n')[-1].strip()
            cfr = open(cfr, 'rb').readlines()[-1].strip()
            try:
                self.assertEqual(ret, cfr)
            except AssertionError:
                print '\n**{0}**\n**{1}**'.format(ret, cfr)
                raise




if __name__ == '__main__':
    main()
