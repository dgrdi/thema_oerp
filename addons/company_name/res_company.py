from openerp.osv import orm, fields

class res_company(orm.Model):
    _inherit = 'res.company'

    _columns = {
        'name': fields.char('Nome azienda', required=True),
    }