# -*- coding: utf-8 -*-

{
    'name': 'Unlink company name from partner name',
    'version': '0.1',
    'category': 'Base configuration',
    'description': """Makes the company name independent from the related partner name.""",
    'author': 'Thema Optical SRL',
    'website': 'http://www.thema-optical.com',
    'depends': ['base'],
    'init_xml': [],
    'data':[

        ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': True,
}
