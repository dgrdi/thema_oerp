# -*- encoding: utf-8 -*-
{
    'name': 'Thema - multiple discounts',
    'version': '0.1',
    'sequence': 1,
    'category': 'Custom',
    'description': """Multiple discounts on sales and orders""",
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': [
        'sale',
        'account',
        'payment_terms',
        'sale_stock',
        'tax_multiple', # tax_multiple redefines 'price_subtotal' on account.invoice.line
        ],
    'data': [
        'views/sale.xml',
        'views/invoice.xml',
        'views/company.xml',
        ],
    'demo_xml': [],
    'installable': True,
    'auto_install': True,
}