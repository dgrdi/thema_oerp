"""
OpenERP prevede sconti solo sulle righe. Per gestire sconti successivi (sul totale di ordini o fatture),
un flag nel context cambia il valore dello sconto sulla riga con la composizione dello stesso con gli
sconti sul totale. Questo flag dovra' essere attivato quando si fanno operazioni che calcolano valori globali
usando i valori delle righe.

Ad esempio, le tasse vengono calcolate sulle singole righe e raggruppate; attivando il flag il totale delle tasse
terra' conto degli sconti globali. Per calcolare il totale di una singola riga, invece, non vogliamo che gli sconti
globali siano considerati, e quindi il flag dovra' rimanere disattivato.

"""
from openerp.osv import fields
from openerp.addons import decimal_precision as dp


class ComposeDiscount(object):

    DISCOUNT_FLAT = 'discount_flat'
    DISCOUNT = 'discount'
    COMPOSE_FLAG = 'discount_compose'

    def _get_composable_discounts(self, cr, uid, ids, context=None):
        """
        Should return a map from ids to lists of the discounts (in %) to be composed with the flat discount.
        """
        raise NotImplementedError()

    def _get_flat_discount(self, cr, uid, ids, context=None):
        """
        Should return  a map from ids to the flat, simple discount.
        """
        dc = self.read(cr, uid, ids, fields=[self.DISCOUNT_FLAT], context=context)
        return  {el['id']: el[self.DISCOUNT_FLAT] for el in dc}


    @staticmethod
    def _discount_get(self, cr, uid, ids, field_names, arg, context=None):
        context = context or {}
        dc = self._get_flat_discount(cr, uid, ids, context=context)
        if context.get(self.COMPOSE_FLAG):
            compose = lambda a, b: a + b - (a * b / 100)
            additional_dc = self._get_composable_discounts(cr, uid, ids, context=context)
            dc = {id: reduce(compose, discounts, dc[id]) for id, discounts in additional_dc.items()}
        return dc

    @staticmethod
    def _discount_set(self, cr, uid, ids, field, val, arg, context=None):
        self.write(cr, uid, ids, {self.DISCOUNT_FLAT: val}, context=context)
        return True

    @staticmethod
    def _discount_search(self, cr, uid, model, field, criterion, context):
        mapper = lambda t: (self.DISCOUNT_FLAT, t[1], t[2]) if t[0]==self.DISCOUNT else t
        return map(mapper, criterion)

    @classmethod
    def _discount_field(cls):
        return fields.function(cls._discount_get, fnct_inv=cls._discount_set, fnct_search=cls._discount_search,
                               string='Sconto (%)', type='float')

    @classmethod
    def _flat_discount_field(cls):
        return fields.float('Sconto su riga (%)', digits_compute=dp.get_precision('Account'))



class ComposeDiscountAmount(object):

    COMPOSE_FLAG = 'discount_compose'

    def _filter_for_discounts(self, cr, uid, ids, context=None):
        # should filter ids for ones that have discounts to be composed
        have_discount = self.search(cr, uid, [('id', 'in', ids), ('payment_discount', '>', 0)],
                                    context=context)
        return have_discount

    def _amount_all(self, cr, uid, ids, field_names, arg, context=None):
        context = context or {}
        # totali senza sconto cassa
        res_nocompose = super(ComposeDiscountAmount, self)._amount_all(cr, uid, ids, field_names, arg, context=context)
        #totali con sconto cassa, solo dove lo sconto cassa esiste
        have_discount = self._filter_for_discounts(cr, uid, ids, context=context)
        if have_discount:
            context[self.COMPOSE_FLAG] = True
            res_compose = super(ComposeDiscountAmount, self)._amount_all(cr, uid, have_discount,
                                                                         field_names, arg, context=context)
            context.pop(self.COMPOSE_FLAG)
        have_discount = set(have_discount)
        for id, data in res_nocompose.items():
            if id in have_discount:
                data = res_compose[id]
                data['amount_no_discount'] = res_nocompose[id]['amount_untaxed']
                data['amount_payment_discount'] = res_nocompose[id]['amount_untaxed'] - data['amount_untaxed']
                res_nocompose[id] = data
            else:
                data['amount_no_discount'] = data['amount_untaxed']
                data['amount_payment_discount'] = 0.0

        return res_nocompose
