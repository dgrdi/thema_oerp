# coding=utf-8
from openerp.osv import orm, fields
from openerp.addons import decimal_precision as dp
from .composite_discount import ComposeDiscount, ComposeDiscountAmount
from osv.osv import except_osv


def _amount_indir(self, cr, uid, ids, field_names, arg, context=None):
    return self._amount_all(cr, uid, ids, field_names, arg, context=context)

def _get_invoice_line(self, cr, uid, ids, context=None):
    result = {}
    for line in self.pool.get('account.invoice.line').browse(cr, uid, ids, context=context):
        result[line.invoice_id.id] = True
    return result.keys()

def _get_invoice_tax(self, cr, uid, ids, context=None):
    result = {}
    for tax in self.pool.get('account.invoice.tax').browse(cr, uid, ids, context=context):
        result[tax.invoice_id.id] = True
    return result.keys()

_amount_store = {
    'account.invoice': (lambda self, cr, uid, ids, c={}: ids, ['invoice_line', 'payment_discount'], 20),
    'account.invoice.tax': (_get_invoice_tax, None, 20),
    'account.invoice.line': (_get_invoice_line, ['price_unit','invoice_line_tax_id','quantity','discount','invoice_id'], 20),
}

_FIELD_FNCT = lambda string, help: fields.function(_amount_indir, digits_compute=dp.get_precision('Account'),
                                                   string=string, multi='sums', help=help, store=_amount_store)


class invoice(ComposeDiscountAmount, orm.Model):
    _inherit = 'account.invoice'

    _columns = {
        'payment_discount': fields.float('Sconto cassa (%)', digits_compute=dp.get_precision('Account')),
        'amount_no_discount': _FIELD_FNCT('Subtotale', 'Totale prima dello sconto cassa'),
        'amount_payment_discount': _FIELD_FNCT('Sconto cassa', 'Valore dello sconto cassa'),
        'amount_untaxed': _FIELD_FNCT('Imponibile', 'Totale imponibile'),
        'amount_tax': _FIELD_FNCT('Imposte', 'Totale imposte'),
        'amount_total': _FIELD_FNCT('Totale', 'Totale dopo imposte'),
    }


    def on_change_payment_term(self, cr, uid, ids, payment_term, context=None):
        if payment_term:
            r = self.pool.get('account.payment.term').read(cr, uid, payment_term, fields=['discount'])
            return {'value': {'payment_discount': r['discount']}}
        return {}

    def compute_invoice_totals(self, cr, uid, inv, company_currency, ref, invoice_move_lines, context=None):
        tot, tot_curr, lines = super(invoice, self).compute_invoice_totals(cr, uid, inv, company_currency, ref,
                                                          invoice_move_lines, context=None)
        if inv.amount_payment_discount:
            cur_obj = self.pool.get('res.currency')
            tot_curr -= inv.amount_payment_discount
            tot -= cur_obj.compute(cr, uid, inv.currency_id.id, company_currency, inv.amount_payment_discount,
                                   context=context)
        return tot, tot_curr, invoice_move_lines

    def finalize_invoice_move_lines(self, cr, uid, invoice_browse, move_lines):
        res = super(invoice, self).finalize_invoice_move_lines(cr, uid, invoice_browse, move_lines)
        if invoice_browse.amount_payment_discount:
            discount_acct = invoice_browse.company_id.payment_discount_account_id.id
            if not discount_acct:
                raise except_osv('Errore!', u"Non è stato specificato un conto per l'imputazione "
                                            u"degli sconti cassa. Per favore impostare il conto nell'anagrafica "
                                            u"dell'azienda.")

            if invoice_browse.type in ('out_invoice', 'in_refund'):
                debit = True
            else:
                debit = False

            res.append((0, 0, {
                'name': 'Sconto cassa',
                'account_id': discount_acct,
                'debit': invoice_browse.amount_payment_discount if debit else 0.0,
                'credit': invoice_browse.amount_payment_discount if not debit else 0.0,
            }))
        return res




_amount_line_indir = lambda s, *a, **k: s._amount_line_compose(*a, **k)
_amount_line_flat_indir = lambda s, *a, **k: s._amount_line(*a, **k)

class invoice_line(ComposeDiscount, orm.Model):
    """
    Make discount a functional field so that we can add additional discounts to it when needed
    and the rest of the system will work as-is.
    """

    _inherit = 'account.invoice.line'

    def _get_composable_discounts(self, cr, uid, ids, context=None):
        invoices = self.read(cr, uid, ids, fields=['invoice_id'], context=context)
        pay_dc = self.pool.get('account.invoice').read(cr, uid, set(el['invoice_id'][0] for el in invoices),
                                                  fields=['payment_discount'], context=context)
        pay_dc = {el['id']: el['payment_discount'] for el in pay_dc}
        res = {el['id']: [pay_dc[el['invoice_id'][0]]] for el in invoices}
        return res

    _columns = {
        'discount': ComposeDiscount._discount_field(),
        'discount_flat': ComposeDiscount._flat_discount_field(),
        # remove the store attribute on price_subtotal, so that it can vary based on
        # the context
        'price_subtotal': fields.function(_amount_line_indir, string='Amount', type="float",
                          digits_compute= dp.get_precision('Account')),
        # and store the 'original' subtotal separately
        'price_subtotal_flat': fields.function(_amount_line_flat_indir, string='Importo riga', type='float',
                                               digits_compute=dp.get_precision('Account'),
                                               store=True),
    }

    def _amount_line_compose(self, cr, uid, ids, field, arg, context=None):
        context = context or {}
        if context.get(self.COMPOSE_FLAG):
            return self._amount_line(cr, uid, ids, field, arg, context)
        else:
            vals = self.read(cr, uid, ids, fields=['price_subtotal_flat'], context=context)
            return {el['id']: el['price_subtotal_flat'] for el in vals}

    def _amount_line(self, cr, uid, ids, prop, unknow_none, unknow_dict):
        # identical to original _amount_line except we pass context to browse
        res = {}
        tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')
        for line in self.browse(cr, uid, ids, context=unknow_dict):
            price = line.price_unit * (1-(line.discount or 0.0)/100.0)
            taxes = tax_obj.compute_all(cr, uid, line.invoice_line_tax_id, price, line.quantity, product=line.product_id, partner=line.invoice_id.partner_id)
            res[line.id] = taxes['total']
            if line.invoice_id:
                cur = line.invoice_id.currency_id
                res[line.id] = cur_obj.round(cr, uid, cur, res[line.id])
        return res


    def move_line_get(self, cr, uid, invoice_id, context=None):
        # when calculating move lines, taxes should be calculated after ALL discounts,
        # while the line amounts themselves should not consider discounts on the invoice totals.
        # we can do that by calculating everything after all discount, and then
        # restoring the line total to the amount before global discounts.
        inv = self.pool.get('account.invoice').browse(cr, uid, invoice_id, context=context)
        if not inv.payment_discount:
            return super(invoice_line, self).move_line_get(cr, uid, invoice_id, context)

        context = context or {}
        context[self.COMPOSE_FLAG] = True
        res = super(invoice_line, self).move_line_get(cr, uid, invoice_id, context)
        context.pop(self.COMPOSE_FLAG)

        line_amounts = {l.id: l.price_subtotal for l in inv.invoice_line}
        for el in res:
            line_id = el.pop('line_id')
            if el['price'] > 0: # the price (line total) is zero when the move line is there
                                # only to populate a tax account. It should then remain zero.
                el['price'] = line_amounts[line_id]
        return res

    def move_line_get_item(self, cr, uid, line, context=None):
        res = super(invoice_line, self).move_line_get_item(cr, uid, line, context)
        res['line_id'] = line.id
        return res


class invoice_tax(orm.Model):
    _inherit = 'account.invoice.tax'

    def compute(self, cr, uid, invoice_id, context=None):
        context = context or {}
        # set compose flag so that tax is calculated after all discounts
        context[ComposeDiscount.COMPOSE_FLAG] = True
        res = super(invoice_tax, self).compute(cr, uid, invoice_id, context=context)
        context.pop(ComposeDiscount.COMPOSE_FLAG)
        return res
