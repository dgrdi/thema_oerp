# coding=utf-8
from openerp.osv import orm, fields
from osv.osv import except_osv


class stock_picking(orm.Model):
    _inherit = 'stock.picking'

    def _prepare_invoice(self, cr, uid, picking, partner, inv_type, journal_id, context=None):
        res = super(stock_picking, self)._prepare_invoice(cr, uid, picking, partner, inv_type,
                                                            journal_id, context=context)
        dc = set(l.sale_line_id.order_id.payment_discount for l in picking.move_lines)
        if len(dc) > 1:
            raise except_osv('Errore!', u"La consegna evade ordini con sconti cassa diversi; non è "
                                        u"possibile unificare le fatture.")
        res['payment_discount'] = iter(dc).next()
        return res

