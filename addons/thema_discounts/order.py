# coding=utf-8
from openerp.osv import orm, fields
from openerp.addons import decimal_precision as dp
from .composite_discount import ComposeDiscount, ComposeDiscountAmount


def _amount_indir(self, cr, uid, ids, field_names, arg, context=None):
    return self._amount_all(cr, uid, ids, field_names, arg, context=context)

def _get_order(self, cr, uid, ids, context=None):
    cr.execute("SELECT DISTINCT order_id FROM sale_order_line WHERE id IN %s", [tuple(ids)])
    return [el[0] for el in cr.fetchall()]

_amount_store = {
    'sale.order': (lambda self, cr, uid, ids, c=None: ids, ['order_line', 'payment_discount'], 10),
    'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
}

_FIELD_FNCT = lambda string, help: fields.function(_amount_indir, digits_compute=dp.get_precision('Account'),
                                                   string=string, multi='sums', help=help, store=_amount_store)

class sale_order(ComposeDiscountAmount, orm.Model):
    _inherit = 'sale.order'

    _columns = {
        'payment_discount': fields.float('Sconto cassa (%)', digits_compute=dp.get_precision('Account'), default=0),
        'amount_no_discount': _FIELD_FNCT('Subtotale', 'Totale prima dello sconto cassa'),
        'amount_payment_discount': _FIELD_FNCT('Sconto cassa', 'Valore dello sconto cassa'),
        'amount_untaxed': _FIELD_FNCT('Imponibile', 'Totale imponibile'),
        'amount_tax': _FIELD_FNCT('Imposte', 'Totale imposte'),
        'amount_total': _FIELD_FNCT('Totale', 'Totale dopo imposte'),
    }

    def _prepare_invoice(self, cr, uid, order, lines, context=None):
        invv = super(sale_order, self)._prepare_invoice(cr, uid, order, lines, context=context)
        invv['payment_discount'] = order.payment_discount
        return invv

    def on_change_payment_term(self, cr, uid, ids, payment_term, context=None):
        if payment_term:
            r = self.pool.get('account.payment.term').read(cr, uid, payment_term, fields=['discount'])
            return {'value': {'payment_discount': r['discount']}}
        return {}


class order_line(ComposeDiscount, orm.Model):
    _inherit = 'sale.order.line'

    def _get_composable_discounts(self, cr, uid, ids, context=None):
        orders = self.read(cr, uid, ids, fields=['order_id'], context=context)
        pay_dc = self.pool.get('sale.order').read(cr, uid, set(el['order_id'][0] for el in orders),
                                                  fields=['payment_discount'], context=context)
        pay_dc = {el['id']: el['payment_discount'] for el in pay_dc}
        res = {el['id']: [pay_dc[el['order_id'][0]]] for el in orders}
        return res

    _columns = {
        'discount': ComposeDiscount._discount_field(),
        'discount_flat': ComposeDiscount._flat_discount_field()
    }
