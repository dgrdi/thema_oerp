from openerp.osv import orm, fields

class company(orm.Model):
    _inherit = 'res.company'
    _columns = {
        'payment_discount_account_id': fields.many2one('account.account', 'Conto imputazione sconti cassa'),
    }

    def _check_payment_acct(self, cr, uid, ids, context=None):
        for company in self.browse(cr, uid, ids, context=context):
            if company.payment_discount_account_id  \
                    and company.payment_discount_account_id.company_id.id != company.id:
                return False
        return True

    _constraints = [
        (
            _check_payment_acct,
            'Il conto imputazione sconti deve appartnere all\'azienda!',
            ['payment_discount_account_id',]
        )

    ]