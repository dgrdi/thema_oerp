from openerp.osv import orm, fields


class category(orm.Model):
    _inherit = 'product.category'
    _columns = {
        'intra_goods_code_id': fields.many2one('intra.code', domain=[('type', '=', 'product')],
                                               string='Codice nomenclatura combinata'),
        'intra_service_code_id': fields.many2one('intra.code', domain=[('type', '=', 'service')],
                                                 string='Codice servizio INTRA'),
    }


class product_template(orm.Model):
    _inherit = 'product.template'
    _columns = {
        'intra_goods_code_id': fields.many2one('intra.code', domain=[('type', '=', 'product')],
                                               string='Codice nomenclatura combinata'),
        'intra_service_code_id': fields.many2one('intra.code', domain=[('type', '=', 'service')],
                                                 string='Codice servizio INTRA'),
        'res_intra_code_id': fields.function(lambda s, *a, **k: s._intra(*a, **k),
                                             type='many2one',
                                             relation='intra.code',
                                             string='Resolve intra code')

    }

    def _intra(self, cr, uid, ids, fields, arg, context=None):
        pss = {}
        categs = set()
        for p in self.browse(cr, uid, ids, context=context):
            if p.type == 'service':
                attr = 'intra_service_code_id'
            else:
                attr = 'intra_goods_code_id'
            pss[p.id] = p[attr]
            if not pss[p.id]:
                pss[p.id] = (p.categ_id, attr)
                categs.add((p.categ_id, attr))
            else:
                pss[p.id] = pss[p.id].id
        catpss = {}
        for cat, attr in categs:
            cur = cat
            code = cat[attr]
            while cur and not code:
                cur = cur.parent_id
                code = cur[attr] if cur else False
            catpss[(cat, attr)] = code.id if code else False
        return {k: v if isinstance(v, (int, long)) else catpss[v] for k, v in pss.iteritems()}
