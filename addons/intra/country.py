from openerp.osv import orm, fields

class country(orm.Model):
    _inherit = 'res.country'
    _columns = {
        'intra': fields.boolean('Presentazione INTRA'),
        'intra_vatcode': fields.char('Codice stato INTRA'),
    }