# coding=utf-8
from openerp.osv import orm, fields
import datetime
from .intragen.intra import generate_intra
from openerp.osv.osv import except_osv
from dateutil.relativedelta import relativedelta

class intra_generate(orm.TransientModel):
    _name = 'intra.generate'

    _columns = {
        'company_id': fields.many2one('res.company', 'Azienda', required=True),
        'year': fields.integer('Anno', required=True),
        'month': fields.integer('Mese', required=True),
    }

    def _default_company_id(self, cr, uid, ctx=None):
        company_id = ctx.get('company_id')
        if not company_id:
            cr.execute("SELECT res_id FROM ir_model_data WHERE model = 'res.company' AND name = 'main_company';")
            company_id = cr.fetchall()[0][0]
        return company_id

    _defaults = {
        'company_id': _default_company_id,
        'month': lambda s, cr, uid, ctx=None: (datetime.date.today() + relativedelta(months=-1)).month,
        'year': lambda s, cr, uid, ctx=None: (datetime.date.today() + relativedelta(months=-1)).year,
    }

    def action_generate(self, cr, uid, ids, context=None):
        assert len(ids) == 1
        context = context or {}
        inv = self.pool.get('account.invoice')
        o = self.browse(cr, uid, ids[0], context=context)
        cr.execute("""
            SELECT i.id
            FROM account_invoice i
            INNER JOIN account_move m ON i.move_id = m.id
            WHERE EXTRACT(year from m.date) = %s
            AND EXTRACT(month from m.date) = %s
            AND m.company_id = %s
            AND m.state = 'posted'
        """, [o.year, o.month, o.company_id.id])
        invoice_ids = [el[0] for el in cr.fetchall()]
        invoice_ids = self.pool.get('account.invoice').search(cr, uid, [
            ('id', 'in', invoice_ids),
            ('partner_id.country_id', '!=', o.company_id.country_id.id),
            '|',
            ('invoice_line.invoice_line_tax_id.intra', '=', True),
            ('partner_id.country_id.intra', '=', True),
        ])
        ctx = context.copy()
        ctx['discount_compose'] = True
        invoices = inv.browse(cr, uid, invoice_ids, context=ctx)
        intra, warnings = generate_intra(invoices)
        wid = self.pool.get('intra.warning').create(cr, uid, {
            'line_ids': [(0, 0, el) for el in warnings],
        })
        iid = []
        ino = self.pool.get('intra.header')
        num = ino._default_number(cr, uid, context)
        for data in intra:
            data.update({
                'number': num,
                'company_id': o.company_id.id,
                'year': o.year,
                'month': o.month,
                'period_type': 'month',
                'warning_id': wid,
            })
            num += 1
            iid.append(ino.create(cr, uid, data, context=context))
        if not iid:
            raise except_osv(u'Attenzione!', u'Non è stato generato nessun modello INTRA. Non esistono, '
                                             u'o non sono state contabilizzate, fatture di competenza '
                                             u'INTRA nel mese selezionato.')
        name = ino.name_get(cr, uid, iid[0], context=context)[0][1]
        self.pool.get('intra.warning').write(cr, uid, wid, {'name': 'Avvisi su ' + name}, context=context)
        il = self.pool.get('intra.line')
        il.schedule_vat_check(cr, uid, il.search(cr, uid, [('header_id', 'in', iid)], context=context), context=context)
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'intra.header',
            'domain': repr([('id', 'in', iid)]),
            'view_mode': 'tree,form',
            'name': 'Modelli INTRA generati',
        }






