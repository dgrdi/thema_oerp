from openerp.osv import orm, fields

INTRA_TERM_SEL = [
    ('E', 'E'),
    ('F', 'F'),
    ('C', 'C'),
    ('D', 'D'),
]

class incoterm(orm.Model):
    _inherit = 'stock.incoterms'
    _columns = {
        'intra_code': fields.function(lambda s, *a, **k: s._intra_term(*a, **k),
                                      string='Codice INTRA',
                                      type='selection',
                                      selecton=INTRA_TERM_SEL),
    }

    def _intra_term(self, cr, uid, ids, field, arg, context=None):
        """
        As of 2014, the INTRA code is the first character of the INCOTERM code
        """
        incoterms = self.read(cr, uid, ids, fields=['code'], context=context)
        return {i['id']: i['code'][0] for i in incoterms}
