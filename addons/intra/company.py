from openerp.osv import orm, fields

class company(orm.Model):
    _inherit = 'res.company'

    _columns = {
        'ua_code': fields.char('Codice utente abilitato ai servizi doganali'),
    }

