# coding=utf-8
from openerp.osv import orm, fields
import calendar
from openerp.tools.translate import _
import datetime
from openerp.tools import flatten

class intra_header(orm.Model):
    _name = 'intra.header'
    _columns = {
        'type': fields.selection([('intra1', 'INTRA-1 (Cessioni)'), ('intra2', 'INTRA-2 (Acquisti)')],
                                 string='Modello', required=True,
                                 readonly=True,
                                 states={'draft': [('readonly', False)]}),
        'name': fields.function(lambda s, *a, **k: s._display_name(*a, **k),
                                string='Nome',
                                type='char',
                                store=True),
        'warning_id': fields.many2one('intra.warning', 'Avvertimenti'),
        'warning_count': fields.related('warning_id', 'count', type='integer', string='Numero avvisi'),
        'state': fields.selection([('draft', 'Bozza'), ('done', 'Presentato')], string='Stato'),
        'company_id': fields.many2one('res.company', 'Azienda', required=True),
        'customs_no': fields.char('Sezione doganale', readonly=True),
        'protocol': fields.char('Protocollo', readonly=True),
        'period_type': fields.selection([('month', 'Mensile'), ('quarter', 'Trimestrale')],
                                        string=u'Periodicità', required=True,
                                        readonly=True,
                                        states={'draft': [('readonly', False)]}),
        'number': fields.integer('Numero progressivo univoco', required=True,
                                 readonly=True,
                                 states={'draft': [('readonly', False)]}),
        'year': fields.integer('Anno', required=True,
                               readonly=True,
                               states={'draft': [('readonly', False)]}),
        'month': fields.integer('Mese',
                                readonly=True,
                                states={'draft': [('readonly', False)]}),
        'quarter': fields.integer('Trimestre',
                                  readonly=True,
                                  states={'draft': [('readonly', False)]}),
        'quarter_info': fields.selection([
                                             ('1st', u'Solo 1° mese'),
                                             ('2nd', u'1° e 2° mese'),
                                             ('3rd', u'Trimestre completo'),
                                         ], string='Competenza', readonly=True,
                                         states={'draft': [('readonly', False)]}),
        'vat': fields.char('Partita IVA', required=True,
                           readonly=True,
                           states={'draft': [('readonly', False)]}),
        'surname_sub': fields.char('Cognome', readonly=True,
                                   states={'draft': [('readonly', False)]}),
        'name_sub': fields.char('Nome', readonly=True,
                                states={'draft': [('readonly', False)]}),
        'company': fields.char('Ragione sociale', readonly=True,
                               states={'draft': [('readonly', False)]}),
        'first_intra': fields.boolean('Nessun elenco presentato in precedenza',
                                      readonly=True,
                                      states={'draft': [('readonly', False)]}),
        'vat_changed': fields.boolean(u'Cessazione attività o variazione partita IVA',
                                      readonly=True,
                                      states={'draft': [('readonly', False)]}),
        'vat_delegate': fields.char('Partita IVA',
                                    readonly=True,
                                    states={'draft': [('readonly', False)]}),
        'name_delegate': fields.char('Cognome e nome o ragione sociale',
                                     readonly=True,
                                     states={'draft': [('readonly', False)]}),
        'date': fields.date('Data', required=True,
                            readonly=True,
                            states={'draft': [('readonly', False)]}),
        'lines_1': fields.function(lambda s, *a, **k: s._lines(*a, **k),
                                   type='integer',
                                   string='Totale righe sezione 1',
                                   multi='lines', ),
        'lines_2': fields.function(lambda s, *a, **k: s._lines(*a, **k),
                                   type='integer',
                                   string='Totale righe sezione 2',
                                   multi='lines'),
        'lines_3': fields.function(lambda s, *a, **k: s._lines(*a, **k),
                                   type='integer',
                                   string='Totale righe sezione 3',
                                   multi='lines'),
        'lines_4': fields.function(lambda s, *a, **k: s._lines(*a, **k),
                                   type='integer',
                                   string='Totale righe sezione 4',
                                   multi='lines'),
        'total_1': fields.function(lambda s, *a, **k: s._totals(*a, **k),
                                   type='integer',
                                   string='Ammontare complessivo sez. 1',
                                   multi='totals'),
        'total_2': fields.function(lambda s, *a, **k: s._totals(*a, **k),
                                   type='integer',
                                   string='Ammontare complessivo sez. 2',
                                   multi='totals'),
        'total_2_txt': fields.function(lambda s, *a, **k: s._totals(*a, **k),
                                   type='string',
                                   string='Ammontare complessivo sez. 2 in testo',
                                   multi='totals'),
        'total_3': fields.function(lambda s, *a, **k: s._totals(*a, **k),
                                   type='integer',
                                   string='Ammontare complessivo sez. 3',
                                   multi='totals'),
        'total_4': fields.function(lambda s, *a, **k: s._totals(*a, **k),
                                   type='integer',
                                   string='Ammontare complessivo sez. 4',
                                   multi='totals'),
        'line_ids': fields.one2many('intra.line', 'header_id', string='Righe', readonly=True),
        'line_1_ids': fields.one2many('intra.line', 'header_id', domain=[('type', '=', '1')],
                                      string='Righe sezione 1',
                                      readonly=True,
                                      states={'draft': [('readonly', False)]}),
        'line_2_ids': fields.one2many('intra.line', 'header_id', domain=[('type', '=', '2')],
                                      string='Righe sezione 2',
                                      readonly=True,
                                      states={'draft': [('readonly', False)]}),
        'line_3_ids': fields.one2many('intra.line', 'header_id', domain=[('type', '=', '3')],
                                      string='Righe sezione 3',
                                      readonly=True,
                                      states={'draft': [('readonly', False)]}),
        'line_4_ids': fields.one2many('intra.line', 'header_id', domain=[('type', '=', '4')],
                                      string='Righe sezione 4',
                                      readonly=True,
                                      states={'draft': [('readonly', False)]}),
        'line_1_ids_2': fields.one2many('intra.line', 'header_id', domain=[('type', '=', '1')],
                                        string='Righe sezione 1',
                                        readonly=True,
                                        states={'draft': [('readonly', False)]}),
        'line_2_ids_2': fields.one2many('intra.line', 'header_id', domain=[('type', '=', '2')],
                                        string='Righe sezione 2',
                                        readonly=True,
                                        states={'draft': [('readonly', False)]}),
        'line_3_ids_2': fields.one2many('intra.line', 'header_id', domain=[('type', '=', '3')],
                                        string='Righe sezione 3',
                                        readonly=True,
                                        states={'draft': [('readonly', False)]}),
        'line_4_ids_2': fields.one2many('intra.line', 'header_id', domain=[('type', '=', '4')],
                                        string='Righe sezione 4',
                                        readonly=True,
                                        states={'draft': [('readonly', False)]})


    }

    _sql_constraints = [
        (
            'monthly_requires_month',
            "CHECK ( period_type <> 'month' or COALESCE(month, 0) <> 0 )",
            u'E` necessario specificare il mese per elenchi a periodicità mensile.'
        ),
        (
            'quarterly_requires_quarter',
            "CHECK ( period_type <> 'quarter' or COALESCE(quarter, 0) <> 0 )",
            u'E` necessario specificare il trimestre per gli elenchi a periodicità trimestrale.'

        ),
        (
            'quarterly_requires_info',
            "CHECK ( period_type <> 'quarter' or quarter_info IS NOT NULL )",
            u'E` necessario specificare la rilevanza dell\'elenco nel trimestre (fino al primo, secondo, o terzo mese)',
        ),
        (
            'name_surname_company',
            "CHECK( (name_sub is not null and surname_sub is not null) or company is not null )",
            u'E` necessario specificare nome e cognome, o ragione sociale, del soggetto obbligato'
        ),

    ]

    _defaults = {
        'state': 'draft',
        'company_id': lambda s, cr, uid, ctx=None: s._browse_company(cr, uid, ctx).id,
        'date': fields.date.context_today,
        'number': lambda s, *a, **k: s._default_number(*a, **k),
        'vat': lambda s, cr, uid, ctx=None: s._browse_company(cr, uid, ctx).vat and s._browse_company(cr, uid, ctx).vat[
                                                                                    2:],
        'company': lambda s, cr, uid, ctx=None: s._browse_company(cr, uid, ctx).partner_id.legal_name or \
                                                s._browse_company(cr, uid, ctx).partner_id.name,
        'year': lambda s, *a, **k: datetime.date.today().year,
        'period_type': 'month',
        'month': lambda s, *a, **k: datetime.date.today().month,
    }

    def init(self, cr):
        # unique index on done to avoid duplicate numbers, but allow them in drafts
        cr.execute("DROP INDEX IF EXISTS idx_intra_uniq_number;")
        cr.execute("CREATE UNIQUE INDEX ON intra_header (vat, number) WHERE state = 'done';")

    def __init__(self, pool, cr):
        super(intra_header, self).__init__(pool, cr)
        # keep a handle to the main company
        md = self.pool.get('ir.model.data')
        mden = md.search(cr, 1, [
            ('name', '=', 'main_company'), ('model', '=', 'res.company')
        ])[0]
        self.main_company_id = md.read(cr, 1, mden, fields=['res_id'])['res_id']


    def _default_number(self, cr, uid, context=None):
        context = context or {}
        cid = context.get('company_id', self.main_company_id)
        cr.execute("SELECT COALESCE(max(number), 0) + 1 FROM intra_header WHERE company_id = %s AND state = 'done'",
                   [cid])
        return cr.fetchall()[0][0]

    def _browse_company(self, cr, uid, context=None):
        context = context or {}
        cid = context.get('company_id', self.main_company_id)
        return self.pool.get('res.company').browse(cr, uid, cid, context=context)


    def _lines(self, cr, uid, ids, fields, arg, context=None):
        cr.execute("""
            SELECT header_id, "type", count(*)
            FROM intra_line
            WHERE header_id IN %s
            GROUP BY header_id, "type"
        """, [tuple(ids)])
        res = {}
        empty = dict.fromkeys(['lines_%s' % i for i in range(1, 5)], 0)
        for header_id, typ, n in cr.fetchall():
            res.setdefault(header_id, empty)['lines_' + typ] = n
        res.update(dict.fromkeys(set(ids) - set(res.keys()), empty))
        return res

    def _totals(self, cr, uid, ids, fields, arg, context=None):
        cr.execute("""
            SELECT header_id, "type", sum(
              case
                when sign is null or sign = '+' then 1
                else -1
              end * amount
            )
            FROM intra_line
            WHERE header_id IN %s
            GROUP BY header_id, "type"
        """, [tuple(ids)])
        res = {}
        empty = dict.fromkeys(['total_%s' % i for i in range(1, 5)], 0)
        for header_id, typ, n in cr.fetchall():
            res.setdefault(header_id, empty)['total_' + typ] = n
        res.update(dict.fromkeys(set(ids) - set(res.keys()), empty))

        # calculate total_2_txt per intrastat
        for (i,r) in res.items():
            val2=r["total_2"]
            if val2<0:
                res[i]["total_2_txt"]=str(val2)[1:-1] + chr(int(str(val2)[-1:])+112)
            else:
                res[i]["total_2_txt"]=str(val2)
        return res

    def _display_name(self, cr, uid, ids, field, arg, context=None):
        res = {}
        context = context or {}
        for h in self.browse(cr, uid, ids, context=context):
            if h.period_type == 'quarter':
                res[h.id] = '%s° trimestre %s' % (h.quarter, h.year)
            else:
                with calendar.TimeEncoding(context.get('lang', 'en_US') + '.UTF-8') as encoding:
                    res[h.id] = '%s %s' % (calendar.month_name[h.month], h.year)
        return res

    def vat_check(self, cr, uid, ids, context=None):
        il = self.pool.get('intra.line')
        line_ids = il.search(cr, uid, [('header_id', 'in', ids)], context=context)
        return il.vat_check(cr, uid, line_ids, context=context)

    def export_intra(self, cr, uid, ids, context=None):
        context = context or {}
        obj = self.browse(cr, uid, ids[0], context=context)
        #controllo per trimestrale
        if obj.period_type=='quarter':
            raise NotImplementedError('Quarterly not completly implemented (section 1 and 2 logic missing)')
        encoder_code='intra_cess_exp'
        if obj.type=='intra2':
            encoder_code='intra_acq_exp'
        return self.pool.get('thema.edi.wizard').encode(cr, uid, encoder_code, ids, context=context)