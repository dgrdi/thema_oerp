from collections import namedtuple
from .encode import get_encoder


class RecordBase(object):

    def encode(self):
        vals = []
        for enc, val in zip(self.encoders, self):
            vals.append(enc.encode(val))
        return ''.join(vals) + '\r\n'

def record(name, fields):
    """
    :param fields: a list of 2- or 3- tuples (name, format, default)
    """
    names = zip(*fields)[0]
    rectpl = namedtuple(name, field_names=names)
    rectpl = type(name, (rectpl, RecordBase), {})
    encoders = []
    for tpl in fields:
        enc = get_encoder(tpl[1])
        if len(tpl) > 2 and tpl[2] is not None:
            enc.default = tpl[2]
        encoders.append(enc)
    rectpl.encoders = encoders
    return rectpl

