import re
from unidecode import unidecode


class AlphaEncoder(object):

    check_len = False
    nil = ''

    def __init__(self, width, default=None):
        self.width = width
        self.default = default

    def coalesce(self, value):
        if self.default is not None:
            return self.default
        if value is None or value is False:
            return self.nil
        return value


    def encode(self, value):
        value = self.coalesce(value)
        value = unidecode(str(value))
        if self.check_len and len(value) > self.width:
            raise ValueError('Cannot encode to format width %s, data would be truncated: %s' % (self.width, value))
        return "{val}{spc}".format(val=value,
                                   spc=' ' * (self.width - len(value)))

class NumEncoder(AlphaEncoder):
    check_len = True
    nil = 0

    def encode(self, value):
        value = self.coalesce(value)
        val = str(abs(value))
        if len(val) > self.width:
            raise ValueError('Cannot encode to format width %s, data would be truncated: %s' % (self.width, val))
        return '{z}{val}'.format(val=val,
                                 z='0' * (self.width - len(value)))

class SignedNumEncoder(NumEncoder):

    def encode(self, value):
        enc = super(SignedNumEncoder, self).encode(value)
        sign = '+' if value >= 0 else '-'
        return sign + enc


class DecEncoder(NumEncoder):

    nil = 0.0

    def __init__(self, pre_zero, post_zero):
        self.pre_zero = pre_zero
        self.post_zero = post_zero
        super(DecEncoder, self).__init__(pre_zero)

    def encode(self, value):
        value = self.coalesce(value)
        int_part, dec_part = str(value).split('.')
        dec_part = dec_part[:self.post_zero]
        left = super(DecEncoder, self).encode(int_part)
        right = '{v}{z}'.format(v=dec_part,
                                z='0' * (self.post_zero - len(dec_part)))
        return '{left}.{right}'.format(left=left, right=right)

formats = {
    re.compile(r'^A\((\d+)\)$'): lambda w: AlphaEncoder(int(w)),
    re.compile(r'^X\((\d+)\)$'): lambda w: AlphaEncoder(int(w)),
    re.compile(r'^9\((\d+)\)$'): lambda w: NumEncoder(int(w)),
    re.compile(r'^9\((\d+)\)V\((\d+)\)$'): lambda l, r: DecEncoder(int(l), int(r)),
    re.compile(r'^S9\((\d+)\)$'): lambda w: SignedNumEncoder(int(w))
}

def get_encoder(format_string):
    for s, f in formats.items():
        m = s.match(format_string)
        if m:
            return f(*m.groups())
    raise ValueError('No encoder found for format: %s' % format_string)

