# coding=utf-8
from .exc import IntraException
from .line import process_invoice
from dateutil.parser import parse
from openerp.tools.float_utils import float_is_zero

def intra_invoice(inv):
    if not inv.partner_id.country_id:
        raise IntraException(inv.id, u'Il cliente non ha indicata una nazione.')
    if not check_applicability(inv):
        if inv.partner_id.country_id.intra:
            raise IntraException(inv.id, u'La fattura è emessa verso un paese INTRA, ma non usa imposte INTRA.')
        return []
    check_country(inv); check_refund(inv)
    data = []
    vals = invoice_vals(inv)
    for el in process_invoice(inv):
        el.update(vals)
        data.append(el)
    check_amounts(inv, data)
    data = split_mass(inv, data)
    return data

def check_applicability(inv):
    applicable = False
    for line in inv.invoice_line:
        intra = any(tx.intra for tx in line.invoice_line_tax_id)
        if intra and not applicable:
            applicable = True
        if applicable and not intra:
            raise IntraException(inv.id, u'La fattura mescola imposte INTRA con imposte non INTRA')
    return applicable

def check_refund(inv):
    if inv.type in ('in_refund', 'out_refund') and not inv.ref_invoice_id:
        raise IntraException(inv.id, u'La nota di credito non ha un riferimento alla fattura originaria.')

def check_country(inv):
    if not inv.partner_id.country_id.intra:
        raise IntraException(inv.id, u'La fattura usa imposte INTRA, ma è emessa verso un paese non INTRA.')

def invoice_vals(inv):
    if not inv.partner_id.vat:
        raise IntraException(inv.id, u'Il cliente non ha una partita IVA.')
    return {
        'invoice_obj': inv,
        'partner_id': inv.partner_id.id,
        'country_id': inv.partner_id.country_id.id,
        'vat': inv.partner_id.vat[2:] if inv.partner_id.country_id.code != 'SM' else inv.partner_id.vat,
        'vat_complete': (inv.partner_id.country_id.code + inv.partner_id.vat[2:]).strip(),
        'sign': '+' if inv.type in ('in_invoice', 'out_invoice') else '-',
        'nature': '1',
        'intra_type': 'intra1' if inv.type in ('out_invoice', 'out_refund') else 'intra2',
        'invoice': inv.supplier_invoice_number if inv.type in ('in_invoice', 'in_refund') else inv.number,
        'invoice_date': inv.date_invoice,
    }

def split_mass(inv, invoice_data):
    tot_mass = sum(el.weight_net or el.weight for el in inv.delivery_ids)
    tot_qty = sum(el['quantity'] for el in invoice_data)
    if tot_mass:
        for el in invoice_data:
            el['net_mass'] = tot_mass * (el['quantity'] / tot_qty)
    return invoice_data

def check_amounts(inv, invoice_data):
    tot = 0.0
    if inv.type in ('out_invoice', 'in_refund'):
        k = 'debit'
    else:
        k = 'credit'
    for line in inv.move_id.line_id:
        if line.account_id.id == inv.account_id.id:
            tot += line[k]
    intra_tot = sum(el['amount'] for el in invoice_data)
    if not float_is_zero(tot - intra_tot, 2):
        raise IntraException(inv.id, u'Il totale contabile (%.2f) non corrisponde con il totale '
                                     u'INTRA (%.2f)' % (tot, intra_tot))


