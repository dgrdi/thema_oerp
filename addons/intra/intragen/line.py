from .exc import IntraException
from dateutil.parser import parse


def invoice_line(line):
    if line.line_type_id.intra_split:
        # linea il cui valore va ripartito su altre righe, fatto successivamente in group_lines
        return {
            'invoice_id': line.invoice_id.id,
            'invoice_line_ids': [line.id],
            'split': True,
            'net_mass': 0.0,
            'quantity': 0,
            'units_extra': 0,
            'amount': line.price_subtotal_company_currency,
            'amount_currency': line.price_subtotal if line.price_subtotal !=line.price_subtotal_company_currency else 0,
        }
    code = line.intra_code_id or line.product_id.res_intra_code_id
    if not code:
        raise IntraException(line.invoice_id.id,
                             u'La riga fattura non ha associata nessuna nomenclatura o codice servizio.',
                             line_id=line.id)
    ref = line.invoice_id.type in ('in_refund', 'out_refund')
    srv = code.type == 'service'
    if not ref and not srv:
        type = '1'
    elif not ref and srv:
        type = '3'
    elif ref and not srv:
        type = '2'
    else:
        type = '4'
    ref_inv = line.invoice_id.ref_invoice_id
    ref_inv_date = parse(ref_inv.date_invoice) if ref_inv else None
    data = {
        'invoice_id': line.invoice_id.id,
        'invoice_line_ids': [line.id],
        'ref_invoice_id': line.invoice_id.ref_invoice_id and line.invoice_id.ref_invoice_id.id or False,
        'split': False,
        'type': type,
        'code_id': code.id,
        'amount_currency': line.price_subtotal if line.price_subtotal !=line.price_subtotal_company_currency else 0,
        'amount': line.price_subtotal_company_currency,
        'net_mass': line.product_id and line.product_id.weight_net or 0.0,
        'quantity': line.quantity,
        'units_extra': int(line.quantity),
        'delivery_term': line.delivery_id.incoterm_id.intra_code if (line.delivery_id and line.delivery_id.incoterm_id) \
            else False,
        'last_country_id': line.delivery_id.partner_id.country_id.id if line.delivery_id else False,
        'orig_country_id': line.delivery_id.partner_id.country_id.id if line.delivery_id else False,
        'dest_country_id': line.delivery_id.partner_id.country_id.id if line.delivery_id else False,
        'orig_province_id': line.invoice_id.company_id.partner_id.state_id.id,
        'dest_province_id': line.invoice_id.company_id.partner_id.state_id.id,
        'ref_year': ref_inv_date and ref_inv_date.year,
        'ref_month': ref_inv_date and ref_inv_date.month,
        'ref_quarter': ref_inv_date and int((ref_inv_date.month - 1) / 3) + 1
    }

    if type == '4':
        # recupero della riga della dichiarazione precedente
        # attraverso il codice servizio
        line_ref = None
        for line in line.invoice_id.ref_invoice_id.invoice_line:
            for intra_line in line.intra_line_ids:
                if intra_line.code_id.id == data['code_id']:
                    line_ref = intra_line
                    break
            if line_ref:
                break
        if line_ref:
            data.update({
                'ref_line_id': line_ref.id,
                'ref_customs': line_ref.header_id.customs_no,
                'ref_protocol': line_ref.header_id.protocol,
                'ref_line_seq': line_ref.sequence,
                'amount_rect': line_ref.amount - line.price_subtotal_company_currency,
                'amount_currency_rect': line_ref.amount_currency - line.price_subtotal
            })

    return data


def process_invoice(inv):
    data = []
    for line in inv.invoice_line:
        if line.price_subtotal_company_currency:
            data.append(invoice_line(line))
    data = group_lines(data)
    return data


def merge_data(d1, d2):
    res = d1.copy()
    if 'sign' in d1:
        sign = 1 if d1['sign'] == d2['sign'] else -1
    else:
        sign = 1
    res['amount_currency'] += sign * d2['amount_currency']
    res['amount'] += sign * d2['amount']
    res['net_mass'] += sign * d2['net_mass']
    res['quantity'] += sign * d2['quantity']
    res['units_extra'] += sign * d2['units_extra']
    res['invoice_line_ids'] += d2['invoice_line_ids']
    for k in ['delivery_term', 'last_country_id', 'orig_country_id', 'dest_country_id',
              'orig_province_id', 'dest_province_id']:
        if not res[k]:
            res[k] = d2[k]
    return res


def group_lines(line_data):
    data = {}
    split = []
    for el in line_data:
        if el['split']:
            # linea da ripartire, ripartizione fatta in una seconda passata
            split.append(el)
            continue
        # raggruppamento per tipo (bene o servizio) e codice / nomenclatura
        key = (el['type'], el['code_id'])
        if key not in data:
            data[key] = el
        else:
            data[key] = merge_data(data[key], el)

    # divisione delle righe da ripartire in base al valore
    data = data.values()
    tot_val = sum(el['amount'] for el in data)
    if split and not data:
        raise IntraException(split[0]['invoice_id'], u'La fattura non ha nessun codice servizio o nomenclatura.')
    for s in split:
        for el in data:
            ratio = el['amount'] / tot_val
            el['amount_currency'] += s['amount_currency'] * ratio
            el['amount'] += s['amount'] * ratio
            el['net_mass'] += s['net_mass'] * ratio
            el['quantity'] += int(s['quantity'] * ratio)
            el['invoice_line_ids'] += s['invoice_line_ids']
    return data

