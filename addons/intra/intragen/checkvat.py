from suds import WebFault
import vatnumber
import time
import random
from multiprocessing.pool import ThreadPool

def worker(vat):
    time.sleep(random.random() / 5)
    try:
        return vatnumber.check_vies(vat)
    except WebFault as e:
        msg = e.fault.faultstring
        if msg == 'INVALID_INPUT':
            return False
        return msg

def check_vat(vats):
    res = {v: vatnumber.check_vat(v) for v in vats}
    check_vies = [k for k, v in res.iteritems() if v]
    if check_vies:
        pool = ThreadPool(processes=20)
        vies_res = pool.map(worker, check_vies)
        pool.close()
        vies_res = dict(zip(check_vies, vies_res))
        res.update(vies_res)
    return res

