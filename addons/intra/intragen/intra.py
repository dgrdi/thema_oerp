# coding=utf-8
from .invoice import intra_invoice
from .exc import IntraException
from .line import merge_data
from .checkvat import check_vat
from openerp.tools import float_utils


def generate_intra(invoices):
    warnings = []
    data = []
    for inv in invoices:
        try:
            data.extend(intra_invoice(inv))
        except IntraException as e:
            warnings.append({
                'name': e.message,
                'invoice_id': e.invoice_id,
                'invoice_line_id': e.line_id,
            })
    data = merge_refunds(data)
    data = set_service_sign(data)
    data = group_by_partner(data)
    data = round_values(data)
    #warnings.extend(vat_warnings(data))
    return prepare_write_data(data), warnings


def group_by_partner(invoice_data):
    res = {}
    skip = []
    for el in invoice_data:
        if el['type'] in ('3', '4'):
            # gli acquisti / cessioni relative a servizi devono avere un riferimento alla fattura
            skip.append(el)
            continue
        key = (el['intra_type'], el['type'], el['partner_id'], el['code_id'])
        if key not in res:
            res[key] = el
        else:
            res[key] = merge_data(res[key], el)
    return res.values() + skip


def merge_refunds(invoice_data):
    data = {}
    refs = []
    for el in invoice_data:
        key = (el['invoice_id'], el['intra_type'], el['type'], el['code_id'])
        assert key not in data, 'duplicate key: %s' % repr(key)
        if el['ref_invoice_id']: refs.append((key, el))
        data[key] = el
    for ref_key, ref in refs:
        key = (el['ref_invoice_id'], el['intra_type'], '1' if el['type'] == '2' else '3', el['code_id'])
        if key in data:
            data[key] = merge_data(data[key], el)
            data.pop(ref_key)
            if float_utils.float_is_zero(data[key]['amount'], 4):
                data.pop(key)
    return data.values()


def round_values(invoice_data):
    for el in invoice_data:
        for k in ['amount', 'amount_currency', 'net_mass']:
            el[k] = int(round(el[k], 0))
        if el['net_mass'] < 1:
            el['net_mass'] = 1
    return invoice_data


def prepare_write_data(invoice_data):
    intra1 = {'line_ids': [], 'type': 'intra1'}
    intra2 = {'line_ids': [], 'type': 'intra2'}
    for el in invoice_data:
        el['invoice_line_ids'] = [(6, 0, el['invoice_line_ids'])]
        if el['intra_type'] == 'intra1':
            intra1['line_ids'].append((0, 0, el))
        else:
            intra2['line_ids'].append((0, 0, el))
    res = [intra1, intra2]
    for el in res[:]:
        if not el['line_ids']:
            res.remove(el)
            continue
        numbers = dict.fromkeys(['1', '2', '3', '4'], 1)
        el['line_ids'].sort(key=lambda x: (x[2]['type'], x[2]['invoice_obj'].date_invoice))
        for d in el['line_ids']:
            d[2]['sequence'] = numbers[d[2]['type']]
            numbers[d[2]['type']] += 1
            for k in ['invoice_obj', 'intra_type', 'vat_complete', 'ref_invoice_id', 'split', 'quantity']:
                d[2].pop(k)
    return res


def vat_warnings(invoice_data):
    warnings = []
    vats = set(el['vat_complete'] for el in invoice_data)
    vat_check = check_vat(vats)
    inv_vat = {el['vat_complete']: el['invoice_id'] for el in invoice_data}
    for vat, valid in vat_check.items():
        if not valid:
            msg = u'La partita IVA %s non è valida' % vat
        elif not isinstance(valid, bool):
            msg = u'Non è stato possibile verificare la partita IVA %s (%s)' % (vat, valid)
        else:
            continue
        warnings.append({
            'name': msg,
            'invoice_id': inv_vat[vat],
        })
    return warnings

def set_service_sign(invoice_data):
    # per le rettifiche a servizi, non va pubblicata la variazione ma l'importo  corretto.
    # a questo punto abbiamo la variazione (per consentire la rettifica all'interno dello
    # stesso periodo); le note di credito che restano si riferiscono a periodi precedenti,
    # quindi cambiamo segno e importo al netto della riga originaria.
    for el in invoice_data:
        if el['type'] == '4':
            el['sign'] = '+'
            if el['amount_rect']:
                el['amount'] = el.pop('amount_rect')
                el['amount_currency'] = el.pop('amount_currency_rect')
    return invoice_data