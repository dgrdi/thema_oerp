
class IntraException(Exception):
    def __init__(self, invoice_id, message, line_id=None):
        self.invoice_id = invoice_id
        self.line_id = line_id
        super(Exception, self).__init__(message)
