# coding=utf-8
from openerp.osv import orm, fields
from openerp.addons import decimal_precision as dp
from .incoterms import INTRA_TERM_SEL
from .intragen.checkvat import check_vat

class intra_line(orm.Model):
    _name = 'intra.line'
    _rec_name = 'vat'
    _order = 'sequence'
    _columns = {
        'header_id': fields.many2one('intra.header', 'Frontespizio', required=True, select=1, ondelete='cascade'),
        'type': fields.selection([('1', 'bis (Beni)'),
                                  ('2', 'ter (Rettifiche beni)'),
                                  ('3', 'quater (Servizi)'),
                                  ('4', 'quinquies (Rettifiche servizi)'),
                                 ], string='Tipo', required=1),
        'sequence': fields.integer(u'N°', readonly=True),
        'partner_id': fields.many2one('res.partner', 'Partner', required=True),
        'country_id': fields.many2one('res.country', 'Nazione', required=True),
        'ref_month': fields.integer('Mese rif.'),
        'ref_quarter': fields.integer('Trimestre rif.'),
        'ref_year': fields.integer('Anno rif.'),
        'ref_line_id': fields.many2one('intra.line', 'Rif. riga', domain=[('type', '=', '3')]),
        'ref_customs': fields.char('Sez. doganale'),
        'ref_protocol': fields.char('Rif. protocollo'),
        'ref_line_seq': fields.integer('Rif. Prog. riga'),
        'vat': fields.char('Cod. IVA', required=True),
        'vat_state': fields.selection([('none', 'Nessuna verifica'), ('valid', 'Validata'), ('invalid', 'Non valida')],
                                      string='Verifica IVA'),
        'sign': fields.selection([('-', '-'), ('+', '+')], string='Segno'),
        'amount': fields.integer(u'Ammontare (€)', required=True),
        'amount_currency': fields.integer(u'Ammontare (valuta)', required=True),
        'nature': fields.selection([
                                       ('1', 'Acquisto o vendita'),
                                       ('2', 'Restituzione o sostituzione merci'),
                                       ('3', 'Aiuti (governativi e non)'),
                                       ('4', 'Vista lavorazione conto terzi'),
                                       ('5', 'Successiva lavorazione conto terzi'),
                                       ('6', 'Noleggio o leasing'),
                                       ('7', 'Difesa o protezione'),
                                       ('8', 'Materiale per costruzioni non fatturati separatamente'),
                                       ('9', 'Altro'),
                                   ], string='Natura trans.'),
        'code_id': fields.many2one('intra.code', 'Codice servizio / Nomencl.', required=True),
        'net_mass': fields.integer('Massa'),
        'units_extra': fields.integer(u'Unità suppl.'),
        'stat_value': fields.integer(u'Valore stat.'),
        'delivery_term': fields.selection(INTRA_TERM_SEL, string='Consegna'),
        'transport': fields.selection([
                                          ('1', 'Trasporto marittimo'),
                                          ('2', 'Trasporto ferroviario'),
                                          ('3', 'Trasporto stradale'),
                                          ('4', 'Trasporto aereo'),
                                          ('5', 'Spedizioni postali'),
                                          ('7', 'Installazioni fisse di trasporto'),
                                          ('8', 'Trasporto per vie d\'acqua'),
                                          ('9', 'Propulsione propria'),
                                      ], string='Trasporto'),
        'last_country_id': fields.many2one('res.country', 'Paese Prov.'),
        'orig_country_id': fields.many2one('res.country', 'Paese Orig.'),
        'dest_country_id': fields.many2one('res.country', 'Paese Dest.'),
        'orig_province_id': fields.many2one('res.country.state', 'Prov. Prov.'),
        'dest_province_id': fields.many2one('res.country.state', 'Prov. Dest.'),
        'invoice_id': fields.many2one('account.invoice', 'Fattura'),
        'invoice': fields.char(u'N° Fattura'),
        'invoice_date': fields.date('Data fattura'),
        'service_type': fields.selection([('I', 'Istantaneo'), ('R', u'A più riprese')],
                                         string=u'Modalità di erogazione'),
        'payment': fields.selection([('B', 'Bonifico'), ('A', 'Accredito'), ('X', 'Altro')],
                                    string=u'Modalità incasso'),
        'payment_country_id': fields.many2one('res.country', 'Paese di pagamento'),
        'invoice_line_ids': fields.many2many('account.invoice.line', 'invoice_line_intra_rel',
                                             'intra_line_id', 'invoice_line_id',
                                             string='Righe fattura', ),
    }

    _sql_constraints = [
        (
            'seq_uniq_per_section',
            "UNIQUE (header_id, type, sequence)",
            "Il numero progressivo deve essere univoco per sezione!"
        ),
        (
            'net_mass_ge_1',
            "CHECK (net_mass >= 1)",
            "La massa netta deve essere almeno 1."
        ),
        (
            'type_2_4_require_sign',
            "CHECK (type not in ('2', '4') OR sign IS NOT NULL)",
            "E` necessario specificare il segno per le righe di rettifica"
        ),


    ]

    _defaults = {
        'nature': '1',
        'units_extra': 0,
        'stat_value': 0,
        'transport': '3',
        'vat_state': 'none',

    }
    _constraints = [
        (
            lambda s, *a, **k: s._check_sequence(*a, **k),
            'La numerazione progressiva deve essere contigua',
            ['sequence', 'header_id'],
        )
    ]

    def name_get(self, cr, uid, ids, context=None):
        res = []
        for el in self.browse(cr, uid, ids, context=context):
            res.append([(el.id, '%s %s' % (el.vat, el.partner_id.display_name))])
        return res

    def name_search(self, cr, uid, name='', args=None, operator='ilike', context=None, limit=100):
        ids = self.search(cr, uid, [
            '|',
            ('partner_id.display_name', operator, name),
            ('vat', operator, name),
        ] + args, context=context, limit=limit)
        return self.name_get(cr, uid, ids, context=context)


    def onchange_partner_id(self, cr, uid, ids, partner_id, context=None):
        if partner_id:
            p = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
            return {'value': {'country_id': p.country_id.id, 'vat': p.vat[2:] if p.vat else False}}
        return {}

    def _check_sequence(self, cr, uid, ids, context=None):
        cr.execute("""
            SELECT header_id, "type", sequence
            FROM intra_line
            WHERE header_id IN (SELECT DISTINCT header_id FROM intra_line WHERE id IN %s)
            ORDER BY header_id, type, sequence
            """, [tuple(ids)])
        i = 0
        last = (None, None)
        for header_id, type, sequence in cr.fetchall():
            if last != (header_id, type):
                last = (header_id, type)
                i = 0
            i += 1
            if sequence != i:
                return False
        return True

    def default_sequence(self, cr, uid, values, context=None):
        if all(el in values for el in ['header_id', 'type']):
            cr.execute("SELECT COALESCE(max(sequence), 0) + 1 "
                       "FROM intra_line WHERE header_id = %s AND type = %s", [values['header_id'], values['type']])
            return cr.fetchall()[0][0]
        return False

    def _add_missing_default_values(self, cr, uid, values, context=None):
        if 'sequence' not in values:
            values['sequence'] = self.default_sequence(cr, uid, values, context=context)
        return values

    def schedule_vat_check(self, cr, uid, ids, recur=0, context=None):
        self.pool.get('ir.cron').create(cr, uid, {
            'name': 'Controllo partite IVA',
            'user_id': uid,
            'model': 'intra.line',
            'function': 'vat_check',
            'args': repr([ids, recur, True]),
        }, context=context)
        return True

    def vat_check(self, cr, uid, ids, recur=0, reschedule=False, context=None):
        data = {}
        for line in self.browse(cr, uid, ids, context=None):
            if line.country_id.code == 'SM':
                data[line.id] = (line.vat).strip()
            else:
                data[line.id] = (line.country_id.intra_vatcode + line.vat).strip()
        vats = set(data.values())
        vat_res = check_vat(vats)
        for id, vat in data.iteritems():
            if vat_res[vat] is True:
                self.write(cr, uid, id, {'vat_state': 'valid'}, context=context)
            elif vat_res[vat] is False:
                self.write(cr, uid, id, {'vat_state': 'invalid'}, context=context)
        recheck_ids = self.search(cr, uid, [('id', 'in', ids), ('vat_state', '=', 'none')], context=context)
        if recheck_ids and recur < 10 and reschedule:
            self.schedule_vat_check(cr, uid, ids, recur+1, True, context=context)
        return True

    def assign_sequence(self, cr, uid, ids, context=None):
        cr.execute("""
            SELECT header_id, "type", id, sequence
            FROM intra_line
            WHERE header_id IN (SELECT header_id FROM intra_line WHERE id IN %s)
            ORDER BY header_id, "type", sequence
        """, [tuple(ids)])
        last = (None, None)
        i = 0
        for header_id, type, id, sequence in cr.fetchall():
            if (header_id, type) != last:
                i = 0
                last = (header_id, type)
            i += 1
            if i != sequence:
                self.write(cr, uid, id, {'sequence': i}, context=context)
        return True


    def unlink(self, cr, uid, ids, context=None):
        cr.execute("SELECT id FROM intra_line WHERE header_id IN (SELECT header_id FROM intra_line WHERE id IN %s)",
                   [tuple(ids)])
        ex_ids = set(el[0] for el in cr.fetchall()) - set(ids)
        res = super(intra_line, self).unlink(cr, uid, ids, context=context)
        self.assign_sequence(cr, uid, ex_ids, context=context)
        return res

