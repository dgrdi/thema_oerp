from openerp.osv import orm, fields


class line_type(orm.Model):
    _inherit = 'thema.line.type'

    _columns = {
        'intra_split': fields.boolean('Ripartisci valore (INTRA)',
                                      help='Ripartisci il valore su altre nomenclature nelle dichiarazioni INTRA'),
    }