import csv
import sys
from xml.sax.saxutils import escape
def go():
    out = sys.stdout
    out.write("""
        <openerp>
            <data>
    """)
    def t(d):
        d= d.decode('utf-8').encode('ascii', 'xmlcharrefreplace')
        return escape(d)
    with open('intra.code.csv') as fo:
        r = csv.DictReader(fo)

        for line in r:
            out.write("""
                    <record id="%s" model="intra.code">
                        <field name="type">%s</field>
                        <field name="code">%s</field>
                        <field name="name">%s</field>
                    </record>
            """ % (t(line['id']),
                   t(line['type']),
                   t(line['code']),
                   t(line['name'])))
    out.write("""
        </data>
    </openerp>
    """)

if __name__ == '__main__':
    go()


