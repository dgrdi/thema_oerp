# coding=utf-8
from openerp.osv import orm, fields
from openerp.addons import decimal_precision as dp


class invoice_line(orm.Model):
    _inherit = 'account.invoice.line'

    _columns = {
        'intra_code_id': fields.many2one('intra.code', 'Cod. INTRA'),
        'price_subtotal_company_currency': fields.function(lambda s, *a, **k: s._subt_currency(*a, **k),
                                                           type='float',
                                                           digits_compute=dp.get_precision('Account'),
                                                           string=u'Totale riga in €'),
        'intra_line_ids': fields.many2many('intra.line', 'invoice_line_intra_rel',
                                           'invoice_line_id', 'intra_line_id',
                                           string='Righe INTRA', domain=[('header_id.state', '=', 'done')])
    }

    def _subt_currency(self, cr, uid, ids, fields, arg, context=None):
        context = context or {}
        cur = self.pool.get('res.currency')
        amt = self.read(cr, uid, ids, fields=['price_subtotal'], context=context)
        amt = {el['id']: el['price_subtotal'] for el in amt}
        res = {}
        for line in self.browse(cr, uid, ids, context=None):
            lc = line.invoice_id.currency_id.id
            cc = line.invoice_id.company_id.currency_id.id
            if lc == cc:
                res[line.id] = amt[line.id]
            else:
                ctx = context.copy()
                date = line.invoice_id.move_id and line.invoice_id.move_id.date or line.invoice_id.date
                ctx['date'] = date
                res[line.id] = cur.compute(cr, uid, lc, cc, amt[line.id],
                                           round=False, context=context)
        return res

    def product_id_change(self, cr, uid, ids, product, uom_id, qty=0, name='', type='out_invoice', partner_id=False,
                          fposition_id=False, price_unit=False, currency_id=False, context=None, company_id=None):
        res = super(invoice_line, self).product_id_change(cr, uid, ids, product, uom_id, qty=qty, name=name,
                                                             type=type, partner_id=partner_id,
                                                             fposition_id=fposition_id,
                                                             price_unit=price_unit, currency_id=currency_id,
                                                             context=context, company_id=company_id)
        if product:
            ic = self.pool.get('product.product').read(cr, uid, product, fields=['res_intra_code_id'],
                                                       context=context)['res_intra_code_id'][0]
            res['value']['intra_code_id'] = ic
        return res

    def _add_missing_default_values(self, cr, uid, values, context=None):
        res = super(invoice_line, self)._add_missing_default_values(cr, uid, values, context=context)
        if 'product_id' in res and res['product_id'] and 'intra_code_id' not in res:
            c = self.pool.get('product.product').read(cr, uid, res['product_id'], fields=['res_intra_code_id'],
                                                      context=context)
            res['intra_code_id'] = c['res_intra_code_id'] and c['res_intra_code_id'][0] or False
        elif 'intra_code_id' not in res:
            res['intra_code_id'] = False
        return res

