from openerp.osv import orm, fields

class intra_code(orm.Model):
    _name = 'intra.code'

    _columns = {
        'type': fields.selection([('product', 'Bene'),
                                  ('service', 'Servizio')],
                                 required=True, string='Tipo'),
        'code': fields.char('Codice', required=True, select=1),
        'name': fields.char('Descrizione'),
    }

    _sql_constraints = [
        (
            'uniq_type_code',
            "UNIQUE (type, code)",
            'Il codice servizio o nomenclatura deve essere univoco.'
        )
    ]

    def init(self, cr):
        self.load_initial(cr)


    def load_initial(self, cr):
        import os
        from StringIO import StringIO
        import datetime
        import csv
        from openerp import SUPERUSER_ID
        f = StringIO()
        cr.execute("SELECT type, code FROM intra_code;")
        existing = set(cr.fetchall())
        now = datetime.datetime.now().isoformat()
        def es(d):
            d.pop('id')
            d = {k: v.replace('\n', r'\n') for k, v in d.items()}
            d.update({
                'create_uid': SUPERUSER_ID,
                'create_date': now,
                'write_uid': SUPERUSER_ID,
                'write_date': now,
            })
            return d
        fields = ['code', 'type', 'name', 'create_uid', 'write_uid', 'create_date', 'write_date']
        with open(os.path.join(os.path.dirname(__file__), 'data', 'intra.code.csv')) as fo:
            r = csv.DictReader(fo)
            w = csv.DictWriter(f, fieldnames=fields, delimiter='\t')
            new = False
            for el in r:
                if (el['type'], el['code']) not in existing:
                    new = True
                    w.writerow(es(el))
            f.seek(0)
            if new:
                cr.copy_from(f, 'intra_code', columns=fields)

    def name_search(self, cr, user, name='', args=None, operator='ilike', context=None, limit=100):
        ids = self.search(cr, user, [
            '|',
            ('name', operator, name),
            ('code', operator, name)
        ] + args, context=context, limit=limit)
        return self.name_get(cr, user, ids, context=context)


    def name_get(self, cr, user, ids, context=None):
        data = self.read(cr, user, ids, fields=['code', 'name'], context=context)
        trun = lambda s: s[:20] + '...' if len(s) > 20 else s
        return [(d['id'], '%s %s' % (d['code'], trun(d['name']))) for d in data]
