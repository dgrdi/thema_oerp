from openerp.osv import orm, fields

class warning(orm.Model):
    _name = 'intra.warning'
    _columns = {
        'name': fields.char('Nome'),
        'date': fields.date('Data'),
        'line_ids': fields.one2many('intra.warning.line', 'warning_id', 'Avvertimenti', readonly=True),
        'count': fields.function(lambda s, *a, **k: s._count(*a, **k),
                                 type='integer',
                                 string='Numero'),
    }


    def _count(self, cr, uid, ids, fields, arg, context=None):
        cr.execute("SELECT warning_id, count(*) FROM intra_warning_line WHERE warning_id IN %s GROUP BY warning_id",
                   [tuple(ids)])
        res = dict(cr.fetchall())
        res.update(dict.fromkeys(set(ids) - set(res.keys()), 0))
        return res

class warning_line(orm.Model):
    _name = 'intra.warning.line'

    _columns = {
        'warning_id': fields.many2one('intra.warning'),
        'invoice_id': fields.many2one('account.invoice', 'Fattura'),
        'invoice_line_id': fields.many2one('account.invoice.line', 'Riga fattura'),
        'name': fields.char('Descrizione'),
        'form_view_ref': fields.function(lambda s, *a, **k: s._form_view_ref(*a, **k),
                                         type='char',
                                         string='View ref')
    }

    def _form_view_ref(self, cr, uid, ids, fields, arg, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line.id] = 'account.invoice_form' if line.invoice_id.type in ('out_invoice', 'out_refund') \
                else 'account.invoice_supplier_form'
        return res
