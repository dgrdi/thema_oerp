from openerp.osv import orm, fields

class tax(orm.Model):
    _inherit = 'account.tax'

    _columns = {
        'intra': fields.boolean('Tassa INTRA')
    }

