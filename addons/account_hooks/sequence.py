"""
Automatically generate fiscal year sequences if there is a fiscal year in context.
"""

from openerp.osv import orm, fields
from openerp.tools.cache import ormcache
from openerp import SUPERUSER_ID

class ir_sequence(orm.Model):
    _inherit = 'ir.sequence'

    @ormcache(skiparg=1)
    def _fiscalyear_exists(self, cr, sequence_id, fiscalyear_id):
        sfy = self.pool.get('account.sequence.fiscalyear')
        return bool(sfy.search(cr, SUPERUSER_ID, [('sequence_main_id', '=', sequence_id),
                                                  ('fiscalyear_id', '=', fiscalyear_id)]))


    def _create_fy_sequence(self, cr, sequence_id, fiscalyear_id):
        # substitute the year in prefix & suffix
        y = self.pool.get('account.fiscalyear').read(cr, SUPERUSER_ID, fiscalyear_id, fields=['date_start'])[
            'date_start'][:4]
        dt = self.read(cr, SUPERUSER_ID, sequence_id, fields=['prefix', 'suffix'])
        dt = {
            'suffix': dt['suffix'] and dt['suffix'].replace('%(year)s', y) or False,
            'prefix': dt['prefix'] and dt['prefix'].replace('%(year)s', y) or False,
        }
        new_seq = self.copy(cr, SUPERUSER_ID, sequence_id, dt)
        self.pool.get('account.sequence.fiscalyear').create(cr, SUPERUSER_ID, {
            'sequence_id': new_seq,
            'sequence_main_id': sequence_id,
            'fiscalyear_id': fiscalyear_id,
        })
        self._fiscalyear_exists.clear_cache(self, sequence_id, fiscalyear_id)

    def _next(self, cr, uid, seq_ids, context=None):
        if context and 'fiscalyear_id' in context:
            for seq in seq_ids:
                if not self._fiscalyear_exists(cr, seq, context['fiscalyear_id']):
                    self._create_fy_sequence(cr, seq, context['fiscalyear_id'])
        return super(ir_sequence, self)._next(cr, uid, seq_ids, context=context)
