from openerp.osv import orm, fields


class account_tax_template(orm.Model):
    _inherit = 'account.tax.template'

    def get_tax_vals(self, cr, uid, tax_template, tax_template_to_tax, tax_code_template_ref, context=None):
        vals = {
            'name':tax_template.name,
            'sequence': tax_template.sequence,
            'amount': tax_template.amount,
            'type': tax_template.type,
            'applicable_type': tax_template.applicable_type,
            'domain': tax_template.domain,
            'parent_id': tax_template.parent_id and ((tax_template.parent_id.id in tax_template_to_tax) and tax_template_to_tax[tax_template.parent_id.id]) or False,
            'child_depend': tax_template.child_depend,
            'python_compute': tax_template.python_compute,
            'python_compute_inv': tax_template.python_compute_inv,
            'python_applicable': tax_template.python_applicable,
            'base_code_id': tax_template.base_code_id and ((tax_template.base_code_id.id in tax_code_template_ref) and tax_code_template_ref[tax_template.base_code_id.id]) or False,
            'tax_code_id': tax_template.tax_code_id and ((tax_template.tax_code_id.id in tax_code_template_ref) and tax_code_template_ref[tax_template.tax_code_id.id]) or False,
            'base_sign': tax_template.base_sign,
            'tax_sign': tax_template.tax_sign,
            'ref_base_code_id': tax_template.ref_base_code_id and ((tax_template.ref_base_code_id.id in tax_code_template_ref) and tax_code_template_ref[tax_template.ref_base_code_id.id]) or False,
            'ref_tax_code_id': tax_template.ref_tax_code_id and ((tax_template.ref_tax_code_id.id in tax_code_template_ref) and tax_code_template_ref[tax_template.ref_tax_code_id.id]) or False,
            'ref_base_sign': tax_template.ref_base_sign,
            'ref_tax_sign': tax_template.ref_tax_sign,
            'include_base_amount': tax_template.include_base_amount,
            'description': tax_template.description,
            'type_tax_use': tax_template.type_tax_use,
            'price_include': tax_template.price_include
        }

    def get_account_vals(self, cr, uid, tax_template, context=None):
        return {
            'account_collected_id': tax_template.account_collected_id and tax_template.account_collected_id.id or False,
            'account_paid_id': tax_template.account_paid_id and tax_template.account_paid_id.id or False,
            
        }

    def _generate_tax(self, cr, uid, tax_templates, tax_code_template_ref, company_id, context=None):
        """
        This method generate taxes from templates.

        :param tax_templates: list of browse record of the tax templates to process
        :param tax_code_template_ref: Taxcode templates reference.
        :param company_id: id of the company the wizard is running for
        :returns:
            {
            'tax_template_to_tax': mapping between tax template and the newly generated taxes corresponding,
            'account_dict': dictionary containing a to-do list with all the accounts to assign on new taxes
            }
        """
        if context is None:
            context = {}
        res = {}
        todo_dict = {}
        tax_template_to_tax = {}

        for tax in tax_templates:
            vals_tax = self.get_tax_vals(cr, uid, tax, tax_template_to_tax, tax_code_template_ref, context)
            vals_tax.update({
                'company_id': company_id,
            })
            new_tax = self.pool.get('account.tax').create(cr, uid, vals_tax)
            tax_template_to_tax[tax.id] = new_tax
            #as the accounts have not been created yet, we have to wait before filling these fields
            todo_dict[new_tax] = self.get_account_vals(cr, uid, tax, context)
        res.update({'tax_template_to_tax': tax_template_to_tax, 'account_dict': todo_dict})
        return res
