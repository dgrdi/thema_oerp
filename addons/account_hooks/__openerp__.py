{
    'name': 'Account template hooks',
    'version': '0.2',
    'category': 'Development',
    'description': """Hooks to allow account (tax) templates to extend the fields that get copied into
                      accounts on generation.""",
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': ['base', 'account',],
    'init_xml': [],
    'data':[
        ],
    'demo_xml': [],
    'js': [],
    'qweb': [],
    'css': [],
    'test': [],
    'installable': True,
    'auto_install': True,
}

