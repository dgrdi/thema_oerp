from openerp.osv import orm, fields
from logging import getLogger
logger = getLogger(__name__)

class account_account_template(orm.Model):
    _inherit = 'account.account.template'

    def get_account_vals(self, cr, uid, account_template, context):
        vals = {
            'name': account_template.name,
            'currency_id': account_template.currency_id and account_template.currency_id.id or False,
            'code': account_template.code or '',
            'type': account_template.type,
            'user_type': account_template.user_type and account_template.user_type.id or False,
            'reconcile': account_template.reconcile,
            'shortcut': account_template.shortcut,
            'note': account_template.note,
            'financial_report_ids': account_template.financial_report_ids and [(6,0,[x.id for x in account_template.financial_report_ids])] or False,
        }
        return vals

    def generate_account(self, cr, uid, chart_template_id, tax_template_ref, acc_template_ref, code_digits, company_id, context=None):
        """
        This method for generating accounts from templates.

        :param chart_template_id: id of the chart template chosen in the wizard
        :param tax_template_ref: Taxes templates reference for write taxes_id in account_account.
        :paramacc_template_ref: dictionary with the mappping between the account templates and the real accounts.
        :param code_digits: number of digits got from wizard.multi.charts.accounts, this is use for account code.
        :param company_id: company_id selected from wizard.multi.charts.accounts.
        :returns: return acc_template_ref for reference purpose.
        :rtype: dict
        """
        if context is None:
            context = {}
        obj_acc = self.pool.get('account.account')
        company_name = self.pool.get('res.company').browse(cr, uid, company_id, context=context).name
        template = self.pool.get('account.chart.template').browse(cr, uid, chart_template_id, context=context)
        #deactivate the parent_store functionnality on account_account for rapidity purpose
        ctx = context.copy()
        ctx.update({'defer_parent_store_computation': True})
        level_ref = {}
        children_acc_criteria = [('chart_template_id','=', chart_template_id)]
        if template.account_root_id.id:
            children_acc_criteria = ['|'] + children_acc_criteria + ['&',('parent_id','child_of', [template.account_root_id.id]),('chart_template_id','=', False)]
        children_acc_template = self.search(cr, uid, [('nocreate','!=',True)] + children_acc_criteria, order='id')
        for account_template in self.browse(cr, uid, children_acc_template, context=context):
            # skip the root of COA if it's not the main one
            if (template.account_root_id.id == account_template.id) and template.parent_id:
                continue
            tax_ids = []
            for tax in account_template.tax_ids:
                tax_ids.append(tax_template_ref[tax.id])

            code_main = account_template.code and len(account_template.code) or 0
            code_acc = account_template.code or ''
            if code_main > 0 and code_main <= code_digits and account_template.type != 'view':
                code_acc = str(code_acc) + (str('0'*(code_digits-code_main)))
            parent_id = account_template.parent_id and ((account_template.parent_id.id in acc_template_ref) and acc_template_ref[account_template.parent_id.id]) or False
            #the level as to be given as well at the creation time, because of the defer_parent_store_computation in
            #context. Indeed because of this, the parent_left and parent_right are not computed and thus the child_of
            #operator does not return the expected values, with result of having the level field not computed at all.
            if parent_id:
                level = parent_id in level_ref and level_ref[parent_id] + 1 or obj_acc._get_level(cr, uid, [parent_id], 'level', None, context=context)[parent_id] + 1
            else:
                level = 0
            vals = self.get_account_vals(cr, uid, account_template, context)
            vals.update({
                'name': (template.account_root_id.id == account_template.id) and company_name or account_template.name,
                'parent_id': parent_id,
                'tax_ids': [(6,0,tax_ids)],
                'company_id': company_id,
                'level': level,
            })
            new_account = obj_acc.create(cr, uid, vals, context=ctx)
            acc_template_ref[account_template.id] = new_account
            level_ref[new_account] = level

        #reactivate the parent_store functionnality on account_account
        obj_acc._parent_store_compute(cr)
        return acc_template_ref

    def generate_recursive(self, cr, uid, id, parent_id, company_id, acc_template_ref, name=None, context=None):
        account_template = self.browse(cr, uid, id, context=context)
        assert not len(account_template.tax_ids), 'generate_recursive cant handle taxes'
        vals = self.get_account_vals(cr, uid, account_template, context)
        vals.update({
            'parent_id': parent_id,
            'company_id': company_id,
        })
        logger.debug('Creating account %s - %s', account_template.code or '', name or account_template.name)
        created = self.pool.get('account.account').create(cr, uid, vals, context=context)
        acc_template_ref[account_template.id] = created
        for c in account_template.child_parent_ids:
            self.generate_recursive(cr, uid, c.id, created, company_id, acc_template_ref, name=None, context=context)
        return acc_template_ref
