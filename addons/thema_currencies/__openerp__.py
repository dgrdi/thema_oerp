
{
    'name': 'Thema - valute',
    'version': '0.5',
    'sequence': 1,
    'category': 'Custom',
    'complexity': "easy",
    'description': """Gestione valute""",
    'author': 'Thema optical SRL',
    'website': 'http://www.thema-optical.com',
    'depends': [
        'account',
        'account_voucher',
        ],
    'data': [
        'payment_view.xml',
        ],
    'demo_xml': [],
    'installable': True,
    'auto_install': True,
    'application': True,
}
