"""
Allow payments in foreign curencies using journals with no secondary currency.
"""
from openerp.osv import orm, fields
from openerp.addons.account_voucher.account_voucher import resolve_o2m_operations


class voucher(orm.Model):
    _inherit = 'account.voucher'

    _columns = {
        'currency_id': fields.many2one('res.currency', 'Valuta'),
        'paid_amount_in_company_currency': fields.function(
            lambda s, *a, **k: s._paid_amount_in_company_currency(*a, **k),
            string='Paid Amount in Company Currency', type='float',
            readonly=True),
    }

    def _paid_amount_in_company_currency(self, cr, uid, ids, field, arg, context=None):
        """
        If the voucher currency is the same as the payment rate currency, we should not multiply the rates together.
        Instead the rate of the voucher currency should be set to the payment rate set on the voucher.
        """
        if context is None:
            context = {}
        res = {}
        ctx = context.copy()
        for v in self.browse(cr, uid, ids, context=context):
            ctx.update({'date': v.date})
            voucher = self.browse(cr, uid, v.id, context=ctx)
            if voucher.currency_id != voucher.payment_rate_currency_id:
                rate = voucher.currency_id.rate * voucher.payment_rate
            else:
                rate = voucher.payment_rate
            ctx.update({
                'voucher_special_currency': voucher.payment_rate_currency_id and voucher.payment_rate_currency_id.id or False,
                'voucher_special_currency_rate': rate,
            })
            res[voucher.id] = self.pool.get('res.currency').compute(cr, uid, voucher.currency_id.id,
                                                                    voucher.company_id.currency_id.id,
                                                                    voucher.amount, context=ctx)
        return res

    def recompute_payment_rate(self, cr, uid, ids, vals, currency_id, date, ttype, journal_id, amount, context=None):
        """
        For payment_rate_currency_id, instead of using the currency of the first line that is different from the
        voucher currency, we use the currency of the first line that is different from the *journal* currency.
        """
        if not currency_id:
            return {'value': {}}
        currency_obj = self.pool.get('res.currency')
        journal = self.pool.get('account.journal').browse(cr, uid, journal_id, context=context)
        company_id = journal.company_id.id
        payment_rate = 1.0
        ref_currency = journal.currency  or journal.company_id.currency_id
        payment_rate_currency_id = ref_currency.id
        ctx = context.copy()
        ctx.update({'date': date})
        o2m_to_loop = False
        if ttype == 'receipt':
            o2m_to_loop = 'line_cr_ids'
        elif ttype == 'payment':
            o2m_to_loop = 'line_dr_ids'
        if o2m_to_loop and 'value' in vals and o2m_to_loop in vals['value']:
            for voucher_line in vals['value'][o2m_to_loop]:
                if voucher_line['currency_id'] != ref_currency.id:
                    payment_rate_currency_id = voucher_line['currency_id']
                    tmp = currency_obj.browse(cr, uid, payment_rate_currency_id, context=ctx).rate
                    payment_rate = tmp / currency_obj.browse(cr, uid, ref_currency.id, context=ctx).rate
                    break
        vals['value'].update({
            'payment_rate': payment_rate,
            'payment_rate_currency_id': payment_rate_currency_id
        })
        #read the voucher rate with the right date in the context
        voucher_rate = self.pool.get('res.currency').read(cr, uid, currency_id, ['rate'], context=ctx)['rate']
        ctx.update({
            'voucher_special_currency_rate': payment_rate * voucher_rate,
            'voucher_special_currency': payment_rate_currency_id})
        res = self.onchange_rate(cr, uid, ids, payment_rate, amount, currency_id, payment_rate_currency_id, company_id, context=ctx)
        for key in res.keys():
            vals[key].update(res[key])
        return vals

    def onchange_currency_id(self, cr, uid, ids, partner_id, journal_id, amount, currency_id, type, date, context=None):
        res = self.onchange_partner_id(cr, uid, ids, partner_id, journal_id, amount, currency_id, type,
                                       date, context=context)
        res['value'].pop('currency_id', None)
        return res

    def onchange_line_ids(self, cr, uid, ids, line_dr_ids, line_cr_ids, amount, voucher_currency, type, context=None):
        """

        """
        res = super(voucher, self).onchange_line_ids(cr, uid, ids, line_dr_ids, line_cr_ids, amount,
                                                     voucher_currency, type, context=None)
        line_osv = self.pool.get("account.voucher.line")
        ml_osv = self.pool.get('account.move.line')
        company = None;
        currency = None
        if line_cr_ids or line_dr_ids:
            line_dr_ids = resolve_o2m_operations(cr, uid, line_osv, line_dr_ids, ['amount'], context)
            line_cr_ids = resolve_o2m_operations(cr, uid, line_osv, line_cr_ids, ['amount'], context)

            for line in line_dr_ids + line_cr_ids:
                ml = None
                if line.get('id'):
                    ml = line_osv.browse(cr, uid, line['id'], context=context).move_line_id
                elif line.get('move_line_id'):
                    ml = ml_osv.browse(cr, uid, line['move_line_id'], context=context)
                if ml and ml.currency_id:
                    if company is None:
                        company = ml.journal_id.company_id
                    if currency is None:
                        currency = ml.currency_id.id
                    elif currency != ml.currency_id.id:
                        res['value'].update({'currency_id': False})
                        return res
        if currency is None and company is not None and currency != voucher_currency:
            res['value'].update({'currency_id': company.currency_id.id})
        return res