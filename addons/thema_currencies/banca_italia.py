import requests
from openerp.osv import orm, fields
import csv
from dateutil.parser import parse
import datetime
import logging

logger = logging.getLogger(__name__)


class SvcBancaItalia(object):
    def build_url(self, date):
        url = "http://cambi.bancaditalia.it/cambi/QueryOneDateAllCur?lang=ita&rate=0&initDay={day}&initMonth={month}&" \
              "initYear={year}&refCur=euro&R1=csv".format(
            day='%02i' % date.day,
            month='%02i' % date.month,
            year='%s' % date.year,
        )
        return url

    def rate(self, currency_code, date):
        return self.rates(date)[currency_code]

    def rates(self, date):
        logger.debug('Fetching currency rates from bancaditalia.it for %s', date.strftime('%Y-%m-%d'))
        res = requests.get(self.build_url(date))
        res.raise_for_status()
        return self.parse_csv(iter(res.text.encode('utf-8').split('\n')))

    def dates_rates(self, dates):
        res = {}
        for d in dates:
            for cur, rate in self.rates(d).iteritems():
                res.setdefault(cur, {})[d] = rate
        return res

    def parse_csv(self, response):
        response.next();
        response.next()  # first two lines are garbage
        reader = csv.DictReader(response)
        data = {}
        for l in reader:
            cur, rate = l['Codice ISO'], l['Quotazione']
            if not cur or not rate:
                continue
            data[cur] = float(rate)
        return data


class currency(orm.Model):
    _inherit = 'res.currency'

    def get_rates_bancaitalia(self, cr, uid, dates, context=None):
        dates = [parse(d) if not isinstance(d, datetime.date) else d for d in dates]
        dates_db = {d.strftime('%Y-%m-%d') for d in dates}
        sv = SvcBancaItalia()
        for cur_code, rates in sv.dates_rates(dates).iteritems():
            try:
                curid = self.search(cr, uid, [('name', '=', cur_code)])[0]
            except IndexError:
                logger.debug('Currency %s does not exist', cur_code)
            ex = {}
            cur = self.browse(cr, uid, curid, context=context)
            for rate in cur.rate_ids:
                if rate.name in dates_db:
                    ex[rate.id] = rates.pop(parse(rate.name))
            wrdata = {
                'rate_ids': [(1, id, {'rate': rate}) for id, rate in ex.iteritems()]
                            + [(0, 0, {'name': date, 'rate': rate}) for date, rate in  rates.iteritems()]
            }
            self.write(cr, uid, curid, wrdata, context=context)


    def get_rates_from_invoice_dates(self, cr, uid, context=None):
        cr.execute("""
            SELECT DISTINCT
              inv.date_invoice
            FROM account_invoice inv
            INNER JOIN res_currency c ON inv.currency_id = c.id
            LEFT JOIN res_currency_rate r ON r.name = inv.date_invoice AND  r.currency_id = c.id
            WHERE c.name <> 'EUR'
            AND r.id IS NULL
        """)
        dates = [el[0] for el in cr.fetchall()]
        self.get_rates_bancaitalia(cr, uid, dates, context=context)


