# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-2014 ZestyBeanz Technologies Pvt Ltd(<http://www.zbeanztech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv, orm
import openerp.addons.decimal_precision as dp


class stock_picking(osv.osv):
    _inherit = 'stock.picking'
    
    
    def _prepare_invoice_line(self, cr, uid, group, picking, move_line, invoice_id,
        invoice_vals, context=None):
        res = super(stock_picking,  self)._prepare_invoice_line(cr, uid, group, picking, move_line, invoice_id, invoice_vals, context=context)
        if move_line.sale_line_id:
            res.update({
                'line_type_id': move_line.sale_line_id.line_type_id.id,
                'price_visible': move_line.sale_line_id.price_visible,
                'price_unit': move_line.sale_line_id.price_unit, 
                'no_tax': move_line.sale_line_id.no_tax,
                })
        return res

class stock_move(orm.Model):
    _inherit  = 'stock.move'

    _columns = {
        'line_type_id': fields.related('sale_line_id', 'line_type_id', type='many2one', relation='thema.line.type',
                                       string='Tipo riga')
    }


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: