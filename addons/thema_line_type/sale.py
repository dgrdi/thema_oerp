# -*- coding: utf-8 -*-
# #############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-2014 ZestyBeanz Technologies Pvt Ltd(<http://www.zbeanztech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp


class sale_order(osv.osv):
    def _amount_value_all(self, cr, uid, ids, field_name, arg, context=None):
        cur_obj = self.pool.get('res.currency')
        lt = self.pool.get('thema.line.type')
        res = {}
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {
                'amount_value': 0.0,
                'amount_discount': 0.0,
            }
            value = 0.0
            discount = 0.0
            cur = order.pricelist_id.currency_id
            for line in order.order_line:
                vals = lt.compute(line.line_type_id, line.price_unit, line.product_uom_qty, line.discount)
                value += vals['value_amount']
                discount += vals['discount_amount']
            res[order.id]['amount_value'] = cur_obj.round(cr, uid, cur, value)
            res[order.id]['amount_discount'] = discount
        return res

    def _get_order(self, cr, uid, ids, context=None):
        cr.execute("SELECT DISTINCT order_id FROM sale_order_line WHERE id IN %s", [tuple(ids)])
        return [el[0] for el in cr.fetchall()]

    def _amount_line_tax(self, cr, uid, line, context=None):
        res = super(sale_order, self)._amount_line_tax(cr, uid, line, context=context)
        if line.line_type_id.type == 'discount':
            return 0.0
        return res

    _inherit = 'sale.order'
    _columns = {
        'amount_value': fields.function(_amount_value_all, digits_compute=dp.get_precision('Account'),
                                        string='Total value of goods',
                                        store={
                                            'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
                                            'sale.order.line': (
                                            _get_order, ['price_unit', 'value', 'discount', 'product_uom_qty'], 10),
                                        },
                                        multi='value', help="The amount of total value of goods"),
        'amount_discount': fields.function(_amount_value_all, digits_compute=dp.get_precision('Account'),
                                           string='Total value of bulk discounts',
                                           store={
                                               'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
                                               'sale.order.line': (
                                               _get_order, ['price_unit', 'value', 'discount', 'product_uom_qty'], 10),
                                           },
                                           multi='value', help="The amount of total dscount."),
    }


class sale_order_line(osv.osv):
    def _amount_line(self, cr, uid, ids, field_name, arg, context=None):
        tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')
        lt = self.pool.get('thema.line.type')
        res = {}
        if context is None:
            context = {}
        for line in self.browse(cr, uid, ids, context=context):
            vals = lt.compute(line.line_type_id, line.price_unit, line.product_uom_qty, line.discount)
            res[line.id] = vals['price_subtotal']
            if res[line.id]:
                price = vals['price_unit'] * (1 - (line.discount or 0.0) / 100.0)
                taxes = tax_obj.compute_all(cr, uid, line.tax_id, price, line.product_uom_qty, line.product_id,
                                            line.order_id.partner_id)
                cur = line.order_id.pricelist_id.currency_id
                res[line.id] = cur_obj.round(cr, uid, cur, taxes['total'])
        return res

    def _value_amount_line(self, cr, uid, ids, field_name, arg, context=None):
        tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')
        lt = self.pool.get('thema.line.type')
        res = {}
        if context is None:
            context = {}
        for line in self.browse(cr, uid, ids, context=context):
            vals = lt.compute(line.line_type_id, line.price_unit, line.product_uom_qty, line.discount)
            res[line.id] = vals['value_amount']
            if res[line.id]:
                cur = line.order_id.pricelist_id.currency_id
                res[line.id] = cur_obj.round(cr, uid, cur, res[line.id])
        return res


    _inherit = 'sale.order.line'
    _columns = {
        'value_amount': fields.function(_value_amount_line, string='Value', digits_compute=dp.get_precision('Account'),
                                        store=True),
        'line_type_id': fields.many2one('thema.line.type', 'Line Type', readonly=True,
                                        states={'draft': [('readonly', False)]}),
        'price_subtotal': fields.function(_amount_line, string='Subtotal', digits_compute=dp.get_precision('Account')),
        'price_visible': fields.boolean('Price Visible', readonly=True, states={'draft': [('readonly', False)]}),
        'no_tax': fields.boolean('No Tax', readonly=True, states={'draft': [('readonly', False)]})
    }


    def onchange_line_type_id(self, cr, uid, ids, line_type_id, context=None):
        v = {'price_visible': False, 'no_tax': False}
        if line_type_id:
            type_obj = self.pool.get('thema.line.type').browse(cr, uid, line_type_id, context=context)
            if type_obj.type == 'gift':
                v.update({
                    'price_unit': 0.0,
                    'price_visible': True,
                })
            if type_obj.type in ['gift', 'discount']:
                v.update({'tax_id': [], 'no_tax': True})

        return {'value': v}

    def _prepare_order_line_invoice_line(self, cr, uid, line, account_id=False, context=None):
        res = super(sale_order_line, self)._prepare_order_line_invoice_line(cr, uid, line, account_id, context=context)
        res.update({
            'line_type_id': line.line_type_id.id,
            'price_visible': line.price_visible,
            'price_unit': line.price_unit,
            'no_tax': line.no_tax,
        })
        return res

    def _prepare_line(self, cr, uid, sale, vals, context=None):
        """
        Fills the missing parameters for the order line, given a sale order and some values.
        Required at least: product_id, product_qty, and line_type_id
        """
        for el in ['product_id', 'product_uom_qty', 'line_type_id']:
            assert el in vals
        vals = vals.copy()
        product_obj = self.pool.get('product.product')
        product = product_obj.browse(cr, uid, vals['product_id'], context=context)
        for f, p in [('product_uom', 'uom_id'), ('product_uos', 'uos_id')]:
            if f not in vals:
                vals[f] = product[p] and product[p].id or False
        if 'name' not in vals:
            vals['name'] = product_obj.name_get(cr, uid, [product.id], context=context)[0][1]
        if 'discount' not in vals:
            vals['discount'] = 0.0
        pvals = self.product_id_change(cr, uid, [],
                                       pricelist=sale.pricelist_id.id,
                                       product=vals['product_id'],
                                       qty=vals['product_uom_qty'],
                                       uom=vals['product_uom'],
                                       qty_uos=vals.get('product_uos_qty', vals['product_uom_qty']),
                                       uos=vals['product_uos'],
                                       name=None,
                                       partner_id=sale.partner_id.id,
                                       lang=sale.partner_id.lang,
                                       update_tax=True,
                                       date_order=sale.date_order,
                                       packaging=False,
                                       fiscal_position=sale.partner_id.property_account_position and \
                                                       sale.partner_id.property_account_position or False,
                                       flag=False,
                                       context=context)['value']
        pvals['tax_id'] = [(6, 0, pvals['tax_id'])]
        for k, v in pvals.items():
            if k not in vals:
                vals[k] = v
        lto = self.pool.get('thema.line.type')
        lt = lto.browse(cr, uid, vals['line_type_id'], context=context)
        cmp = lto.compute(lt, vals.get('price_unit', 0.0), vals['product_uom_qty'], vals.get('discount', 0.0))
        vals['price_unit'] = cmp['price_unit']
        vals['order_id'] = sale.id
        return vals
