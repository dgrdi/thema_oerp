# -*- coding: utf-8 -*-
# #############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-2014 ZestyBeanz Technologies Pvt Ltd(<http://www.zbeanztech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, osv

LINE_TYPE_SEL = [
    ('sale', 'Vendita'),
    ('gift', 'Omaggio'),
    ('discount', 'Sconto merce'),
    ('shipping', 'Spese di trasporto'),
    ('collect', 'Spese di incasso'),
    ('other', 'Altro'),
]


class line_type(osv.osv):
    _name = 'thema.line.type'
    _columns = {
        'name': fields.char('Name', size=32, required=True),
        'type': fields.selection(LINE_TYPE_SEL, string='Tipo', required=True),
    }

    def compute(self, line_type, price_unit, qty, discount):
        line_val = price_unit * qty * (100 - discount) / 100
        res = {
            'price_unit': price_unit,
            'value_amount': line_val,
            'price_subtotal': line_val,
            'discount_amount': 0.0,
        }
        if line_type.type == 'discount':
            res.update({
                'price_subtotal': 0.0,
                'discount_amount': line_val
            })
        elif line_type.type == 'gift':
            res.update({
                'price_unit': 0.0,
                'price_subtotal': 0.0,
                'value_amount': 0.0,
            })
        elif line_type.type != 'sale':
            res.update({
                'value_amount': 0.0,
            })
        return res
