# -*- encoding: utf-8 -*-
# #############################################################################
#
#    Copyright (c) 2013 ZestyBeanz Technologies Pvt. Ltd.
#    (http://wwww.zbeanztech.com)
#    contact@zbeanztech.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp.osv import orm, fields
import time


class account_invoice(osv.osv):
    _inherit = 'account.invoice'

    def _amount_value_all(self, cr, uid, ids, field_name, arg, context=None):
        cur_obj = self.pool.get('res.currency')
        lt = self.pool.get('thema.line.type')
        res = {}
        for invoice in self.browse(cr, uid, ids, context=context):
            res[invoice.id] = {
                'amount_value': 0.0,
                'amount_discount': 0.0,
            }
            value = 0.0
            discount = 0.0
            cur = invoice.currency_id
            for line in invoice.invoice_line:
                vals = lt.compute(line.line_type_id, line.price_unit, line.quantity, line.discount)
                value += vals['value_amount']
                discount += vals['discount_amount']
            res[invoice.id]['amount_value'] = cur_obj.round(cr, uid, cur, value)
            res[invoice.id]['amount_discount'] = discount
        return res

    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('account.invoice.line').browse(cr, uid, ids, context=context):
            result[line.invoice_id.id] = True
        return result.keys()

    _columns = {
        'amount_value': fields.function(_amount_value_all, digits_compute=dp.get_precision('Account'),
                                        string='Total value of goods',
                                        store={
                                            'account.invoice': (
                                            lambda self, cr, uid, ids, c={}: ids, ['invoice_line'], 10),
                                            'account.invoice.line': (
                                            _get_order, ['price_unit', 'value', 'discount', 'quantity', 'value_amount'],
                                            20),
                                        },
                                        multi='value'),
        'amount_discount': fields.function(_amount_value_all, digits_compute=dp.get_precision('Account'),
                                           string='Total value of bulk discounts',
                                           store={
                                               'account.invoice': (
                                               lambda self, cr, uid, ids, c={}: ids, ['invoice_line'], 10),
                                               'account.invoice.line': (
                                               _get_order, ['price_unit', 'value', 'discount', 'quantity', ], 10),
                                           },
                                           multi='value'),
    }


account_invoice()


class account_invoice_line(osv.osv):
    _inherit = 'account.invoice.line'


    def _amount_line(self, cr, uid, ids, prop, unknow_none, unknow_dict):
        amount_0_lines = set(
            self.search(cr, uid, [('id', 'in', ids), ('line_type_id.type', 'in', ['gift', 'discount'])],
                        context=unknow_dict))
        res = super(account_invoice_line, self)._amount_line(cr, uid, list(set(ids) - amount_0_lines),
                                                             prop, unknow_none, unknow_dict)
        res.update({id: 0.0 for id in amount_0_lines})
        return res

    def _value_amount_line(self, cr, uid, ids, field_name, arg, context=None):
        tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')
        lt = self.pool.get('thema.line.type')
        res = {}
        if context is None:
            context = {}
        for line in self.browse(cr, uid, ids, context=context):
            vals = lt.compute(line.line_type_id, line.price_unit, line.quantity, line.discount)
            res[line.id] = vals['value_amount']
            if line.invoice_id:  # lines can be invoice-less when invoicing sale orders
                cur = line.invoice_id.currency_id
                res[line.id] = cur_obj.round(cr, uid, cur, res[line.id])
        return res


    _columns = {
        'value_amount': fields.function(_value_amount_line, string='Value', digits_compute=dp.get_precision('Account'),
                                        store=True),
        'line_type_id': fields.many2one('thema.line.type', 'Tipo riga'),
        'price_subtotal': fields.function(_amount_line, string='Amount', type="float",
                                          digits_compute=dp.get_precision('Account'), store=True),
        'price_visible': fields.boolean('Price Visible'),
        'no_tax': fields.boolean('No Tax'),
    }

    def onchange_line_type_id(self, cr, uid, ids, line_type_id, context=None):
        v = {'price_visible': False, 'no_tax': False}
        if line_type_id:
            type_obj = self.pool.get('thema.line.type').browse(cr, uid, line_type_id, context=context)
            if type_obj.type == 'gift':
                v.update({
                    'price_unit': 0.0,
                    'price_visible': True,
                })
            if type_obj.type in ['gift', 'discount']:
                v.update({'invoice_line_tax_id': [], 'no_tax': True})

        return {'value': v}


class account_invoice_tax(orm.Model):
    _inherit = 'account.invoice.tax'

    def compute(self, cr, uid, invoice_id, context=None):
        tax_grouped = {}
        tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')
        inv = self.pool.get('account.invoice').browse(cr, uid, invoice_id, context=context)
        cur = inv.currency_id
        company_currency = self.pool['res.company'].browse(cr, uid, inv.company_id.id).currency_id.id
        for line in inv.invoice_line:
            # diff: ignore discount lines
            if line.line_type_id.type in ['discount', 'gift']:
                continue
            for tax in tax_obj.compute_all(cr, uid, line.invoice_line_tax_id,
                                           (line.price_unit * (1 - (line.discount or 0.0) / 100.0)), line.quantity,
                                           line.product_id, inv.partner_id)['taxes']:
                val = {}
                val['invoice_id'] = inv.id
                val['name'] = tax['name']
                val['amount'] = tax['amount']
                val['manual'] = False
                val['sequence'] = tax['sequence']
                val['base'] = cur_obj.round(cr, uid, cur, tax['price_unit'] * line['quantity'])

                if inv.type in ('out_invoice', 'in_invoice'):
                    val['base_code_id'] = tax['base_code_id']
                    val['tax_code_id'] = tax['tax_code_id']
                    val['base_amount'] = cur_obj.compute(cr, uid, inv.currency_id.id, company_currency,
                                                         val['base'] * tax['base_sign'], context={
                        'date': inv.date_invoice or time.strftime('%Y-%m-%d')}, round=False)
                    val['tax_amount'] = cur_obj.compute(cr, uid, inv.currency_id.id, company_currency,
                                                        val['amount'] * tax['tax_sign'],
                                                        context={'date': inv.date_invoice or time.strftime('%Y-%m-%d')},
                                                        round=False)
                    val['account_id'] = tax['account_collected_id'] or line.account_id.id
                    val['account_analytic_id'] = tax['account_analytic_collected_id']
                else:
                    val['base_code_id'] = tax['ref_base_code_id']
                    val['tax_code_id'] = tax['ref_tax_code_id']
                    val['base_amount'] = cur_obj.compute(cr, uid, inv.currency_id.id, company_currency,
                                                         val['base'] * tax['ref_base_sign'], context={
                        'date': inv.date_invoice or time.strftime('%Y-%m-%d')}, round=False)
                    val['tax_amount'] = cur_obj.compute(cr, uid, inv.currency_id.id, company_currency,
                                                        val['amount'] * tax['ref_tax_sign'],
                                                        context={'date': inv.date_invoice or time.strftime('%Y-%m-%d')},
                                                        round=False)
                    val['account_id'] = tax['account_paid_id'] or line.account_id.id
                    val['account_analytic_id'] = tax['account_analytic_paid_id']

                key = (val['tax_code_id'], val['base_code_id'], val['account_id'], val['account_analytic_id'])
                if not key in tax_grouped:
                    tax_grouped[key] = val
                else:
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base'] += val['base']
                    tax_grouped[key]['base_amount'] += val['base_amount']
                    tax_grouped[key]['tax_amount'] += val['tax_amount']

        for t in tax_grouped.values():
            t['base'] = cur_obj.round(cr, uid, cur, t['base'])
            t['amount'] = cur_obj.round(cr, uid, cur, t['amount'])
            t['base_amount'] = cur_obj.round(cr, uid, cur, t['base_amount'])
            t['tax_amount'] = cur_obj.round(cr, uid, cur, t['tax_amount'])
        return tax_grouped




        # vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: