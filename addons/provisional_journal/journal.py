from openerp.osv import orm, fields

class account_journal(orm.Model):
    _inherit = 'account.journal'
    _columns = {
        'provisional': fields.boolean('Provvisorio'),
        'provisional_post_journal_id': fields.many2one('account.journal', 'Sezionale registrazione definitiva'),
    }

