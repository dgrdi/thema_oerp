from openerp.osv import orm, fields

class find_provisional(orm.TransientModel):
    _name = 'account.move.provisional.find'

    _columns = {
        'date_maturity_start': fields.date('Da'),
        'date_maturity_stop': fields.date('A'),
        'date_start': fields.date('Da'),
        'date_stop': fields.date('A'),
    }
    _defaults = {
        'date_maturity_start': fields.date.context_today,
        'date_maturity_stop': fields.date.context_today,
    }

    def find(self, cr, uid, ids, context=None):
        el = self.browse(cr, uid, ids[0], context=context)
        domain = [
            ('date', '>=', el.date_start),
            ('date', '<=', el.date_stop),
            ('line_id.date_maturity', '>=', el.date_maturity_start),
            ('line_id.date_maturity', '<=', el.date_maturity_stop),
            ('provisional', '=', True)
        ]
        domain = [d for d in domain if d[2] != False]
        res = self.pool.get('account.move').search(cr, uid, domain, context=context)
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'res_model': 'account.move',
            'domain': "[('id', 'in', %s)]" % repr(res),
        }