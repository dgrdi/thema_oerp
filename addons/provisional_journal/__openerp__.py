{
    'name': 'Provisional journals',
    'version': '0.2',
    'category': 'Accounting',
    'description': """Journals for provisional account moves.""",
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': ['base', 'account'],
    'init_xml': [],
    'data':[
        'views/journal.xml',
        'views/move.xml',
        ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': True,
}
