# coding=utf-8
from openerp.osv import orm, fields
import datetime

class move(orm.Model):
    _inherit = 'account.move'

    def _get_provisional(obj, cr, uid, ids, context=None):
        return obj.pool.get('account.move').search(cr, uid, [('journal_id', 'in', ids)], context=context)

    _columns = {
        'provisional': fields.related('journal_id', 'provisional', string='Provvisioria', type='boolean',
                                      readonly=True, store={
                'account.journal': (
                    _get_provisional, ['provisional'], 20
                ),
                'account.move': (
                    lambda o, c, u, ids, ctx=None: ids,
                    ['journal_id'],
                    10
                )
            })
    }

    _sql_constraints = [
        (
            'provisional_never_poted',
            "CHECK (NOT provisional OR state <> 'posted') DEFERRABLE INITIALLY DEFERRED",
            u"Non si può pubblicare una scrittura provvisoria!"
        )
    ]


    def confirm_provisional(self, cr, uid, ids, context=None):
        """
        Una scrittura provvisoria viene copiata con la data di oggi.
        """
        confirmed = []
        date = fields.date.context_today(self, cr, uid, context=context),
        period_id = self.pool.get('account.period').find(cr, uid, dt=date, context=context)[0]
        for move in self.browse(cr, uid, ids, context=context):
            if move.provisional:
                self.write(cr, uid, move.id, {
                    'journal_id': move.journal_id.provisional_post_journal_id.id,
                    'date': date,
                    'period_id': period_id,
                })
                confirmed.append(move.id)
        act =  {
            'type': 'ir.actions.act_window',
            'res_model': 'account.move',
            'domain': "[('id', 'in', %s)]" % repr(confirmed),
            'view_mode': 'tree,form',
        }
        if len(confirmed) == 1:
            act['res_id'] = confirmed[0]
            act['view_mode'] = 'form,tree'
        return act


    def on_change_journal_id(self, cr, uid, ids, journal_id, context=None):
        j = self.pool.get('account.journal').browse(cr, uid, journal_id, context=context)
        return {
            'value': {
                'provisional': j.provisional if j else False
            }
        }
