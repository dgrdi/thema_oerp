"""
WARNING: company_id needs to be in context for the correct taxes to be picked when reading 'taxes_id'
from product.template.
'taxes_id' is used mainly in onchange events in the sale and invoice views. By default the company is NOT in context.
As of now (Jul. 2014) company_id is put in context by the module thema_commission.

WARNING: the default product_id_change method on sale.order.line does NOT pass along the correct context. This
 has been fixed directly on the openerp source tree (rev 0d6ed513cd).
"""


from openerp.osv import orm, fields


class product_tax(orm.Model):
    _name = 'product.tax'

    _columns = {
        'company_id': fields.many2one('res.company', 'Azienda', required=True),
        'product_tmpl_id': fields.many2one('product.template', 'Prodotto'),
        'product_categ_id': fields.many2one('product.category', 'Categoria prodotto'),
        'type': fields.selection([('customer', 'Cliente'), ('supplier', 'Fornitore')], string='Tipo', required=True),
        'tax_ids': fields.many2many('account.tax', 'product_tax_rel', 'product_tax_id', 'product_id',
                                    string='Imposte', required=True),
    }

    _sql_constraints = [
        ('uniq', "UNIQUE(company_id, product_tmpl_id, product_categ_id, type)", "Esiste gia` una definizione!"),
    ]

class product_category(orm.Model):
    _inherit = 'product.category'

    _columns = {
        'tax_ids': fields.one2many('product.tax', 'product_categ_id', string='Imposte',
                                     domain=[('type', '=', 'customer')]),
        'supplier_tax_ids': fields.one2many('product.tax', 'product_categ_id', string='Imposte fornitore',
                                            domain=[('type', '=', 'supplier')])
    }



class product_template(orm.Model):
    _inherit = 'product.template'
    _columns = {
        'taxes_id': fields.function(lambda s, *a, **k: s._product_taxes(*a, **k), type='many2many',
                                    relation='account.tax',
                                    string='Imposte cliente', multi='product_tax', readonly=True),
        'supplier_taxes_id': fields.function(lambda s, *a, **k: s._product_taxes(*a, **k), type='many2many',
                                             relation='account.tax', string='Imposte fornitore', multi='product_tax',
                                             readonly=True),
        'product_tax_ids': fields.one2many('product.tax', 'product_tmpl_id', string='Imposte prodotto (clienti)',
                                           domain=[('type', '=', 'customer')]),
        'product_supplier_tax_ids': fields.one2many('product.tax', 'product_tmpl_id',
                                                   string='Imposte prodotto (fornitori)',
                                                   domain=[('type', '=', 'supplier')]),
    }

    def _product_taxes(self, cr, uid, ids, fields, arg, context=None):
        context = context or {}
        res = {}
        cid = context.get('force_company', context.get('company_id'))
        if not cid:
            cid = self.pool.get('res.users').read(cr, uid, uid, fields=['company_id'], context=context)['company_id'][0]
        cat_cache = {}
        for prod in self.browse(cr, uid, ids, context=context):
            vals = {}
            for tx in prod.product_tax_ids + prod.product_supplier_tax_ids:
                if tx.company_id.id == cid:
                    f = 'taxes_id' if tx.type == 'customer' else 'supplier_taxes_id'
                    vals[f] = [el.id for el in tx.tax_ids]
            cat = prod.categ_id
            while cat and ('taxes_id' not in vals or 'supplier_taxes_id' not in vals):
                if cat.id in cat_cache:
                    res[prod.id] = cat_cache[cat.id]
                    break
                else:
                    cat_cache[cat.id] = vals
                    for tx in cat.tax_ids + cat.supplier_tax_ids:
                        if tx.company_id.id == cid:
                            f = 'taxes_id' if tx.type == 'customer' else 'supplier_taxes_id'
                            if f not in vals:
                                vals[f] = [el.id for el in tx.tax_ids]
                    cat = cat.parent_id
            for f in ('taxes_id', 'supplier_taxes_id'):
                if f not in vals:
                    vals[f] = []
            res[prod.id] = vals
        return res

