# coding=utf-8
from openerp import SUPERUSER_ID
from openerp.osv import orm, fields
from openerp.addons import decimal_precision as dp
from devutils.sql_func import sql_function


class product_availability(orm.Model):
    _auto = False
    _name = 'product.availability'

    def init(self, cr):
        from os import path as p

        with open(p.join(p.dirname(__file__), 'product.sql')) as fo:
            cr.execute(fo.read())


class product_avail_field(sql_function):
    def __init__(self, name, locations=None, location_domain=None, **kwargs):
        self.locations = locations
        self.location_domain = location_domain
        args = {
            'type': 'float',
            'digits_compute': dp.get_precision('Product UoS'),
        }
        args.update(kwargs)
        super(product_avail_field, self).__init__(name, **args)

    def init(self, obj, cr):
        locs = []
        if self.location_domain:
            self.locs = obj.pool.get('stock.location').search(cr, SUPERUSER_ID, self.location_domain)
        elif self.locations:
            imd = obj.pool.get('ir.model.data')
            for l in self.locations:
                if isinstance(l, basestring):
                    locs.append(imd.get_object_reference(cr, SUPERUSER_ID, *l.split('.'))[-1])
                elif isinstance(l, (int, long)):
                    locs.append(l)
                else:
                    raise ValueError('location not valid: {0}'.format(l))
            self.locs = locs
        else:
            raise ValueError('No location specified for product availability field!')

    def avail_query(self, field, ids):
        product_array = "%s" if ids is not None else "ARRAY(select id from product_product)"
        q = """
            SELECT pa.product_id id, COALESCE(SUM(qty), 0::NUMERIC) {0}
            FROM product.fn_availability(%s, {1}) pa
            GROUP BY pa.product_id
        """.format(field, product_array)
        p = [self.locs] + ([list(ids)] if ids is not None else [])
        return q, p

    def query(self, fields):
        return self.avail_query(fields[0], None)

    def query_read(self, fields, ids):
        return self.avail_query(fields[0], ids)


class product(orm.Model):
    _inherit = 'product.product'
    _identity = lambda s, c, u, i, f, a, context=None: {id: dict.fromkeys(f, id) for id in i}

    _columns = {
        'qty_available_physical': product_avail_field('Giacenza totale', ['stock.stock_location_locations'])
    }