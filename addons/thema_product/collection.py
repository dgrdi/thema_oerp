from openerp.osv import orm, fields
from .tagged import Tagged

class collection(Tagged, orm.Model):
    _name = 'product.collection'

    _parent_store = True

    _columns = {
        'name': fields.char('Nome', required=True),
        'parent_id': fields.many2one('product.collection', 'Linea madre'),
        'parent_left': fields.integer('Left parent'),
        'parent_right': fields.integer('Right parent'),
    }
    _columns.update(Tagged.TAG_FIELDS())

