from openerp.osv import orm, fields
from .tagged import Tagged

class model(Tagged, orm.Model):
    _name = 'product.model'
    _tag_inherit = ['collection_id']
    _columns = {
        'name': fields.char('Nome', required=True),
        'code': fields.char('Codice'),
        'collection_id': fields.many2one('product.collection', 'Linea'),
        'categ_id': fields.many2one('product.category', 'Categoria'),
    }
    _columns.update(Tagged.TAG_FIELDS())

    _sql_constraints = [
        ('code_unique', "UNIQUE(code)", 'Il codice modello deve essere univoco!')
    ]

