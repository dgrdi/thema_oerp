from openerp.osv import orm, fields
from .tagged import Tagged

class product_template(Tagged, orm.Model):
    _inherit = 'product.template'
    _tag_inherit = ['model_id', 'categ_id', 'color_id', 'material_id']

    _columns = {
        'model_id': fields.many2one('product.model', 'Modello'),
        'collection_id': fields.related('model_id', 'collection_id',
                                        string='Linea',
                                        relation='product.collection',
                                        type='many2one',
                                        readonly=True,
                                        store=True),
        'color_id': fields.many2one('product.color', 'Colore'),
        'material_id': fields.many2one('product.material', 'Materiale'),
        'eye_size': fields.integer('Calibro (mm)'),
        'bridge_length': fields.integer('Ponte (mm)'),
        'power': fields.float('Grado'),
        'temple_length': fields.integer('Lunghezza asta (mm)'),
        'ext_shape_code': fields.char('Codice forma lente'),


    }

    _columns.update(Tagged.TAG_FIELDS())


class product_product(orm.Model):
    _inherit = 'product.product'

    _columns = {
        'rfid_epc': fields.integer('Codice EPC per RFID'),
    }

