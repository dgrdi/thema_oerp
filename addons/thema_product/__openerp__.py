{
    'name': 'Thema product extensions',
    'version': '0.2',
    'category': 'Product management',
    'description': """Eyeglasses-related product extensions""",
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': ['base', 'product', 'client_improv', 'stock', 'account'],
    'init_xml': [],
    'data':[
        'data/res.groups.csv',
        'data/ir.model.access.csv',
        'data/product_categories_data.xml',
        'data/product_material.xml',
        'data/product_rfid_seq.xml',
        'data/uom.xml',
        'views/menu.xml',
        'views/product.xml',
        'views/collection.xml',
        'views/model.xml',
        'views/colors.xml',
        'views/materials_view.xml',
        'views/uom.xml',
        'views/category.xml',

        ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': True,
}
