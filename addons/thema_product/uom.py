# coding=utf-8
from openerp.osv import orm, fields

class product_uom(orm.Model):
    _inherit = 'product.uom'
    _columns = {
        'abbrev': fields.char('Abbreviazione', translate=True)
    }


class uom_conversion(orm.Model):
    _name = 'product.uom.conversion'
    _auto = False
    _columns = {
        'uom_from_id': fields.many2one('product.uom', u'Unità origine'),
        'uom_to_id': fields.many2one('product.uom', u'Unità destinazione'),
        'factor': fields.float('Fattore di conversione'),
        'rounding': fields.float('Fattore di arrotondamento'),
    }

    def init(self, cr):
        cr.execute("""
            DROP VIEW IF EXISTS product_uom_conversion CASCADE;
            CREATE VIEW product_uom_conversion AS
            SELECT
              (uf.id::int8) << 32 + ut.id id,
              uf.id                    uom_from_id,
              ut.id                    uom_to_id,
              CASE
              WHEN uf.category_id = ut.category_id THEN
                ut.factor / uf.factor
              ELSE NULL END            factor,

              ut.rounding
            FROM product_uom uf,
              product_uom ut;
        """)


