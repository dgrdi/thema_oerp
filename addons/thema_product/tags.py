# coding=utf-8
from openerp.osv import orm, fields

class tag_group(orm.Model):
    _name = 'product.tag.group'
    _columns = {
        'name': fields.char('Nome gruppo', required=True),
        'type': fields.selection([
            ('exclusive', 'Esclusivo - solo una opzione consentita'),
            ('inclusive', u'Inclusivo - più opzioni consentite')
        ], 'Tipo', required=True),
        'required': fields.boolean(u'Obbligatorio'),
    }
    #TODO check su inclusivo /esclusivo e obbligatorio


class tag(orm.Model):
    _name = 'product.tag'
    _rec_name = 'display_name'

    _columns = {
        'name': fields.char('Tag', required=True),
        'group_id': fields.many2one('product.tag.group', 'Gruppo', required=True),
        'display_name': fields.function(lambda s, *a, **k: s._display_name(*a, **k),
                                        string='Nome visualizzato', type='char', store=True),
    }

    _sql_constraints = [
        (
            'name_unique_per_group',
            "UNIQUE (name, group_id)",
            "Il nome del tag deve essere unico all'interno di un gruppo.",
        )
    ]


    def _display_name(self, cr, uid, ids, field, arg, context=None):
        res = {}
        for tag in self.browse(cr, uid, ids, context=context):
            res[tag.id] = '%s: %s' % (tag.group_id.name, tag.name)
        return res


