DO LANGUAGE plpgsql $$
BEGIN
  IF NOT exists(SELECT
                  1
                FROM information_schema.schemata
                WHERE schema_name = 'tools')
  THEN
    CREATE SCHEMA tools;
  END IF;
  IF NOT exists(SELECT
                  1
                FROM information_schema.schemata
                WHERE schema_name = 'product')
  THEN
    CREATE SCHEMA product;
  END IF;
END
$$;

DROP FUNCTION IF EXISTS tools.round( NUMERIC, INT, NUMERIC ) CASCADE;
CREATE FUNCTION tools.round(value NUMERIC, digits INT, factor NUMERIC)
  RETURNS NUMERIC SET search_path TO tools, PUBLIC AS $b$
BEGIN
  IF digits IS NOT NULL
  THEN
    RETURN round(value, digits);
  END IF;
  IF factor = 0
  THEN
    RETURN value;
  END IF;
  RETURN ceil(value / factor) * factor;
END
$b$ LANGUAGE plpgsql IMMUTABLE;

DROP FUNCTION IF EXISTS product.fn_availability ( INT [], INT [] ) CASCADE;
CREATE FUNCTION product.fn_availability(location_ids INT [], product_ids INT [])
  RETURNS TABLE(
  location_id INT,
  product_id INT,
  qty NUMERIC,
  uom_id INT
  ) SET search_path TO product, PUBLIC AS $b$
DECLARE
  all_locations INT [];
BEGIN
  all_locations := array(
      SELECT
        l2.id
      FROM stock_location l1, stock_location l2
      WHERE l1.parent_left <= l2.parent_left
            AND l1.parent_left < l2.parent_right
            AND l1.parent_right >= l2.parent_right
            AND l1.parent_right > l2.parent_left
            AND l1.id = ANY (location_ids)
  );

  RETURN QUERY
  SELECT
    l1.id                           location_id,
    p.id                            product_id,
    tools.round(coalesce(sum(min.qty), 0 :: NUMERIC) - coalesce(sum(mout.qty), 0 :: NUMERIC),
                NULL, uom.rounding) qty,
    tpl.uom_id

  FROM stock_location l1
    CROSS JOIN stock_location l2
    CROSS JOIN product_product p
    INNER JOIN product_template tpl ON p.product_tmpl_id = tpl.id
    INNER JOIN product_uom uom ON uom.id = tpl.uom_id
    LEFT JOIN
    (
      SELECT
        m.location_dest_id          location_id,
        m.product_id,
        sum(product_qty * c.factor) qty
      FROM stock_move m
        INNER JOIN product_product p ON p.id = m.product_id
        INNER JOIN product_template tp ON tp.id = p.product_tmpl_id
        INNER JOIN product_uom_conversion c ON c.uom_from_id = m.product_uom AND c.uom_to_id = tp.uom_id
      WHERE m.location_dest_id = ANY (all_locations)
            AND m.product_id = ANY (product_ids)
            AND m.state = 'done'
      GROUP BY m.location_dest_id, m.product_id, tp.uom_id
    ) min ON min.product_id = p.id AND min.location_id = l2.id
    LEFT JOIN
    (
      SELECT
        m.location_id,
        m.product_id,
        sum(product_qty * c.factor) qty
      FROM stock_move m
        INNER JOIN product_product p ON p.id = m.product_id
        INNER JOIN product_template tp ON tp.id = p.product_tmpl_id
        INNER JOIN product_uom_conversion c ON c.uom_from_id = m.product_uom AND c.uom_to_id = tp.uom_id
      WHERE m.location_id = ANY (all_locations)
            AND m.product_id = ANY (product_ids)
            AND m.state = 'done'
      GROUP BY m.location_id, m.product_id, tp.uom_id
    ) mout ON mout.location_id = l2.id AND mout.product_id = p.id
  WHERE l1.parent_left <= l2.parent_left
        AND l1.parent_left < l2.parent_right
        AND l1.parent_right >= l2.parent_right
        AND l1.parent_right > l2.parent_left
        AND l1.id = ANY (location_ids)
        AND p.id = ANY (product_ids)
  GROUP BY l1.id, p.id, tpl.uom_id, uom.rounding;

END
$b$ LANGUAGE plpgsql STABLE;

DROP VIEW IF EXISTS product_availability CASCADE;
CREATE VIEW product_availability AS
  SELECT
    pa.location_id << 32 + pa.product_id id,
    pa.*
  FROM product.fn_availability(array(SELECT
                                       id
                                     FROM stock_location),
                               array(SELECT
                                       id
                                     FROM product_product)) pa


