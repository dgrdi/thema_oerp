from openerp.osv import orm, fields
from .tagged import Tagged

class category(Tagged, orm.Model):
    _inherit = 'product.category'

    _columns = {

    }
    _columns.update(Tagged.TAG_FIELDS())
