from openerp.osv import orm, fields
from .tagged import Tagged

class material(Tagged, orm.Model):
    _name = 'product.material'
    _parent_store = True

    _columns = {
        'name': fields.char('Materiale', translate=True, required=True),
        'customs_name': fields.char('Materiale - su fatture',
                                    translate=True, required=True),
        'parent_id': fields.many2one('product.material', 'Genitore', ondelete='restrict'),
        'parent_left': fields.integer('Left parent', select=1),
        'parent_right': fields.integer('Right parent', select=1),

    }
    _columns.update(Tagged.TAG_FIELDS())
