from openerp.osv import orm, fields
from .tagged import Tagged

class color(Tagged, orm.Model):
    _name = 'product.color'
    _columns = {
        'name': fields.char('Nome'),
        'code': fields.char('Codice'),
        'description': fields.char('Descrizione'),
    }
    _columns.update(Tagged.TAG_FIELDS())

