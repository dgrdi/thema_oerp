from openerp.osv import orm, fields
import re


class Tagged(object):
    _tag_inherit = []

    @staticmethod
    def FN_TAGS_INHERIT():
        return fields.function(lambda s, *a, **k: s._tags_inherit(*a, **k),
                               fnct_search=lambda s, *a, **k: s._tags_search(*a, **k),
                               string='Tag ereditati',
                               type='many2many',
                               relation='product.tag')

    @staticmethod
    def FN_TAGS_ALL():
        return fields.function(lambda s, *a, **k: s._tags_all(*a, **k),
                               fnct_search=lambda s, *a, **k: s._tags_search(*a, **k),
                               string='Tutti i tag',
                               type='many2many',
                               relation='product.tag',
                               )

    @staticmethod
    def TAGS():
        return fields.many2many('product.tag', string='Tag')

    @staticmethod
    def TAG_FIELDS():
        return {
            'tag_ids': Tagged.TAGS(),
            'tag_inherit_ids': Tagged.FN_TAGS_INHERIT(),
            'tag_all_ids': Tagged.FN_TAGS_ALL(),
        }

    def _get_parents(self, cr, uid, obj):
        parents = self.search(cr, uid, [
                    ('parent_left', '<', obj.parent_left),
                    ('parent_left', '<', obj.parent_right),
                    ('parent_right', '>', obj.parent_right),
                    ('parent_right', '>', obj.parent_left)
        ], context=obj._context)
        return parents

    def _tags_inherit(self, cr, uid, ids, field, arg, context=None):
        res = {}
        for obj in self.browse(cr, uid, ids, context=context):
            res[obj.id] = set()
            for f in self._tag_inherit:
                if obj[f]:
                    res[obj.id].update(el.id for el in obj[f].tag_all_ids)
            if self._parent_store:
                for p in self.browse(cr, uid, self._get_parents(cr, uid, obj), context=context):
                    res[obj.id].update(el.id for el in p.tag_all_ids)
        return {k: list(v) for k, v in res.iteritems()}

    def _tags_all(self, cr, uid, ids, field, arg, context=None):
        inh = self._tags_inherit(cr, uid, ids, field, arg, context=context)
        res = {}
        for el in self.read(cr, uid, ids, fields=['tag_ids'], context=context):
            if el['id'] not in res:
                res[el['id']] = set()
            res[el['id']].update(el['tag_ids'] + inh[el['id']])
        return {k: list(v) for k, v in res.iteritems()}

    def _tags_search(self, cr, uid, model, field_name, criterion, context=None):
        field, op, c = criterion[0]
        ors = []
        for f in self._tag_inherit:
            ors.append(('{0}.tag_all_ids'.format(f), op, c))
        if field == 'tag_all_ids':
            ors.append(('tag_ids', op, c))
        if not ors:
            ors = [('id', '=', False)]
        domain = [ors.pop()]
        while ors:
            domain.extend([ors.pop(), '|'])
        domain.reverse()
        if self._parent_store:
            results = self.search(cr, uid, domain, context=context)
            if results:
                domain = ['|', ('id', 'in', results), ('id', 'child_of', results)]
            else:
                domain = [('id', '=', False)]
        return domain