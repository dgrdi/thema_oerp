# -*- encoding: utf-8 -*-
##############################################################################
#
#    Copyright (c) 2013 ZestyBeanz Technologies Pvt. Ltd.
#    (http://wwww.zbeanztech.com)
#    contact@zbeanztech.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from jasper_reports import jasper_report
from jr_invoice import JasperInvoiceParser


class jasper_invoice_report(JasperInvoiceParser.JasperInvoiceParser):
    def __init__(self, cr, uid, ids, data, context):
        super(jasper_invoice_report, self).__init__(cr, uid, ids, data, context)
        self.sheet_names = []

    def generate_data_source(self, cr, uid, ids, data, context):
        return 'xml_records'


    def generate_parameters(self, cr, uid, ids, data, context):
        if data['report_type'] == 'xls':
            return {'IS_IGNORE_PAGINATION': True}
        return {}

    def generate_properties(self, cr, uid, ids, data, context):
        return {
            'net.sf.jasperreports.export.xls.one.page.per.sheet': 'true',
            'net.sf.jasperreports.export.xls.sheet.names.all': '/'.join(self.sheet_names),
            'net.sf.jasperreports.export.ignore.page.margins': 'true',
            'net.sf.jasperreports.export.xls.remove.empty.space.between.rows': 'true'
        }

    def generate_records(self, cr, uid, ids, data, context):
        result = []
        internal_no = 0
        invoice_pool = self.pool.get('account.invoice')
        translation_pool = self.pool.get('ir.translation')
        inv_company_pool = self.pool.get('res.company')
        inv_partner_pool = self.pool.get('res.partner')
        inv_lines_pool = self.pool.get('account.invoice.line')

        invoice_ids = ids
        if invoice_ids:
            for invoice_obj in invoice_pool.browse(cr, uid, invoice_ids, context=context):
                partner = inv_partner_pool.browse(cr, uid, invoice_obj.partner_id.id, context=context)
                internal_no += 1
                data = {
                    #'internal_no':  internal_no,
                    'company_id': invoice_obj.company_id.id,
                    'display_name': partner.display_name,
                    'contact_address': partner.contact_address,
                    'city': partner.city,
                    'street': partner.street,
                    'zip': partner.zip,
                    'state_id': partner.state_id,

                    'internal_number': invoice_obj.internal_number,
                    'date_invoice': invoice_obj.date_invoice,
                    'amount_untaxed': invoice_obj.amount_untaxed,
                    'amount_total': invoice_obj.amount_total,
                    'amount_value': invoice_obj.amount_value,
                    'amount_tax': invoice_obj.amount_tax,
                }
                inv_lines_ids = inv_lines_pool.search(cr, uid, [('invoice_id', '=', invoice_obj.id)], context=context)
                records = []
                i = 0
                for inv_lines_obj in inv_lines_pool.browse(cr, uid, inv_lines_ids, context=context):
                    record = {
                        'quantity': inv_lines_obj.quantity
                    }
                    i = i + 1
                    records.append (record)

                data['invoice_line']=records
                result.append(data)

        self.sheet_names.append('InvoiceReport')
        return result


jasper_report.report_jasper('report.invoice_report_test1', 'jasper.invoice.report.wizard', parser=jasper_invoice_report)
jasper_report.report_jasper('report.invoice_report_test', 'account.invoice', parser=jasper_invoice_report)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
