CREATE OR REPLACE FUNCTION partner_balance.fn_partner_balance(
  partner_ids INT [],
  date_ref    DATE,
  move_states VARCHAR []
)
  RETURNS TABLE(
  partner_id INT,
  bal_move_id INT,
  move_group_id INT,
  move_id INT,
  move_line_id INT,
  state VARCHAR
  )
SET search_path TO partner_balance, PUBLIC AS $b$
DECLARE
  moves INT [];
BEGIN

  SELECT
    array(SELECT DISTINCT
            ml.move_id
          FROM account_move_line ml
          WHERE (ml.partner_id = ANY (partner_ids) OR partner_ids IS NULL))
  INTO moves;

  RETURN QUERY
  SELECT
    ml.partner_id,
    mlg.move_id,
    ml.move_group_id,
    ml.move_id,
    ml.id,
    s.state
  FROM account_move_line ml
    INNER JOIN account_move m ON m.id = ml.move_id
    INNER JOIN account_account a ON a.id = ml.account_id
    INNER JOIN account_move_line mlg ON mlg.id = ml.move_group_id
    LEFT JOIN bills_receivable.fn_state(moves, date_ref, move_states) s ON s.id = ml.move_id
  WHERE ml.date <= date_ref
        AND m.state = ANY (move_states)
        AND (ml.partner_id = ANY (partner_ids) OR partner_ids IS NULL)
        AND a.type IN ('receivable', 'payable');

END
$b$ LANGUAGE plpgsql STABLE;


DROP VIEW IF EXISTS partner_balance_line CASCADE;

CREATE OR REPLACE VIEW partner_balance_line AS
  WITH bd AS (
      SELECT
        d.id,
        d.move_line_id,
        d.state,
        d.move_id,
        d.balance_id,
        d.move_group_id,
        g.date        date_group,
        d.partner_id,
        d.bal_move_id,
        g.date_maturity,
        m.date        date_move,
        mm.bill_date_due :: DATE,
        CASE WHEN
          row_number()
          OVER (PARTITION BY balance_id, bal_move_id
            ORDER BY l.move_group_id, l.date, l.id) = 1 THEN bal_move_id
        ELSE NULL END bal_move_id_h,
        CASE WHEN
          row_number()
          OVER (PARTITION BY balance_id, bal_move_id, d.move_group_id
            ORDER BY g.id) = 1 THEN d.move_group_id
        ELSE NULL END move_group_id_h
      FROM partner_balance_data d
        INNER JOIN account_move m ON m.id = d.bal_move_id
        INNER JOIN account_move mm ON mm.id = d.move_id
        INNER JOIN account_move_line g ON g.id = d.move_group_id
        INNER JOIN account_move_line l ON l.id = d.move_line_id
  ), mv AS (
      SELECT
        d.balance_id,
        d.bal_move_id,
        -sum(CASE WHEN d.state IN ('emitted', 'deposited') THEN ml.debit - ml.credit
             ELSE 0 END)          exp,
        sum(ml.debit - ml.credit) balance,
        CASE
        WHEN sum(ml.debit - ml.credit) <> 0 THEN 'open' :: VARCHAR
        WHEN sum(CASE WHEN d.state IN ('emitted', 'deposited') THEN ml.debit - ml.credit
                 ELSE 0 END) <> 0 THEN 'exp' :: VARCHAR
        ELSE 'closed' :: VARCHAR
        END                       state
      FROM partner_balance_data d
        INNER JOIN account_move_line ml ON d.move_line_id = ml.id
      GROUP BY d.balance_id, d.bal_move_id
  ), gr AS (
      SELECT
        d.balance_id,
        d.move_group_id,
        -sum(CASE WHEN d.state IN ('emitted', 'deposited') THEN ml.debit - ml.credit
             ELSE 0 END)          exp,
        sum(ml.debit - ml.credit) balance,
        CASE
        WHEN sum(ml.debit - ml.credit) <> 0 THEN 'open' :: VARCHAR
        WHEN sum(CASE WHEN d.state IN ('emitted', 'deposited') THEN ml.debit - ml.credit
                 ELSE 0 END) <> 0 THEN 'exp' :: VARCHAR
        ELSE 'closed' :: VARCHAR
        END                       state
      FROM partner_balance_data d
        INNER JOIN account_move_line ml ON ml.id = d.move_line_id
      GROUP BY d.balance_id, d.move_group_id
  ) SELECT
    bd.id,
    row_number()
    OVER (
      ORDER BY bd.balance_id, bd.date_move, bd.bal_move_id, bd.date_maturity, ml.date, ml.id) "sequence",
    bd.balance_id,
    bd.bal_move_id,
    bd.move_group_id,
    bd.move_line_id,
    bd.date_move,
    bd.date_maturity                                                                          date_maturity_group,
    mv.exp                                                                                    exp_move,
    mv.balance                                                                                bal_move,
    mv.state                                                                                  state_move,
    gr.exp                                                                                    exp_group,
    gr.balance                                                                                bal_group,
    gr.state                                                                                  state_group,
    m.name                                                                                    name_move_h,
    m.ref                                                                                     ref_move_h,
    m.date                                                                                    date_move_h,
    mvh.balance                                                                               balance_move_h,
    mvh.exp                                                                                   exp_move_h,
    grh.balance                                                                               balance_group_h,
    grh.exp                                                                                   exp_group_h,
    g.name                                                                                    name_group_h,
    g.ref                                                                                     ref_group_h,
    g.date_maturity,
    mvh.bal_move_id                                                                           bal_move_id_h,
    grh.move_group_id                                                                         move_group_id_h,
    ml.date,
    ml.ref                                                                                    ref_line,
    ml.name                                                                                   name_line,
    ml.credit,
    ml.debit,
    bd.bill_date_due                                                                          bill_date_due,
    coalesce(bd.bill_date_due, ml.date) - bd.date_move                                        days,
    coalesce(j.currency, cmp.currency_id)                                                     currency_id,
    ml.amount_currency,
    CASE
    WHEN bd.state = 'emitted' THEN 'E' :: VARCHAR
    WHEN bd.state = 'deposited' THEN 'V' :: VARCHAR
    WHEN bd.state = 'unpaid' THEN 'I' :: VARCHAR
    WHEN bd.state = 'canceled' THEN 'R' :: VARCHAR
    WHEN bd.date_maturity <= b.date_ref AND gr.balance <> 0 THEN 'S' :: VARCHAR
    END                                                                                       flag_h
  FROM bd
    INNER JOIN partner_balance b ON b.id = bd.balance_id
    INNER JOIN mv ON mv.bal_move_id = bd.bal_move_id AND mv.balance_id = bd.balance_id
    INNER JOIN gr ON gr.move_group_id = bd.move_group_id AND gr.balance_id = bd.balance_id
    INNER JOIN account_move_line ml ON ml.id = bd.move_line_id
    LEFT JOIN mv mvh ON mvh.balance_id = bd.balance_id AND bd.bal_move_id_h = mvh.bal_move_id
    LEFT JOIN gr grh ON grh.balance_id = bd.balance_id AND bd.move_group_id_h = grh.move_group_id
    LEFT JOIN account_move m ON m.id = bd.bal_move_id_h
    LEFT JOIN account_move_line g ON g.id = bd.move_group_id_h
    INNER JOIN account_journal j ON ml.journal_id = j.id
    INNER JOIN res_company cmp ON cmp.id = j.company_id
  ORDER BY "sequence"