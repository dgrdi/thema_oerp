{
    'name': 'Partner balance',
    'version': '0.2',
    'category': 'Account',
    'description': """
        Estratti conto
    """,
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': ['base', 'account', 'thema_bills_receivable', 'client_improv'],
    'init_xml': [],
    'data':[
        'ir.model.access.csv',
        'balance_view.xml',
        ],
    'demo_xml': [],
    'js': [],
    'qweb': [],
    'css': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}

