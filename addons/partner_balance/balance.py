from openerp.osv import orm, fields
from openerp.addons import decimal_precision as dp

MOVE_STATES = [
    ('posted', 'Pubblicate'),
    ('draft', 'Tutte')
]


class balance_data(orm.TransientModel):
    _name = 'partner.balance.data'

    _columns = {
        'balance_id': fields.many2one('partner.balance', 'Estratto conto'),
        'partner_id': fields.many2one('res.partner', 'Partner'),
        'bal_move_id': fields.many2one('account.move', 'Partita'),
        'move_group_id': fields.many2one('account.move.line', 'Scadenza'),
        'move_id': fields.many2one('account.move', 'Articolo'),
        'move_line_id': fields.many2one('account.move.line', 'Voce contabile'),
        'state': fields.char('Stato effetto'),
    }


class balance(orm.TransientModel):
    _name = 'partner.balance'
    _columns = {
        'partner_id': fields.many2one('res.partner', 'Partner', readonly=True),
        'name': fields.related('partner_id', 'name', type='char', string='Nome', readonly=True),
        'date_ref': fields.date('Data riferimento', readonly=True),
        'move_states': fields.selection(MOVE_STATES, 'Registrazioni contabili', readonly=True),
        'data_ids': fields.one2many('partner.balance.data', 'balance_id', readonly=True),
        'currency_id': fields.related('line_ids', 'currency_id', type='many2one', relation='res.currency',
                                      string='Valuta'),
        'line_ids': fields.one2many('partner.balance.line', 'balance_id', 'Tutte le partite', readonly=True),
        'line_open_ids': fields.one2many('partner.balance.line', 'balance_id', 'Partite aperte',
                                         readonly=True,
                                         domain=[('state_move', '=', 'open')]),
        'line_exp_ids': fields.one2many('partner.balance.line', 'balance_id',
                                        'Partite aperte ed esposte',
                                        readonly=True, domain=[('state_move', 'in', ['open', 'exp'])]),
        'balance': fields.function(lambda s, *a, **k: s._totals(*a, **k),
                                   multi='totals',
                                   string='Saldo',
                                   type='float',
                                   digits_compute=dp.get_precision('Account')),
        'exp': fields.function(lambda s, *a, **k: s._totals(*a, **k),
                               multi='totals',
                               string='Totale esposto',
                               type='float',
                               digits_compute=dp.get_precision('Account')),
        'overdue': fields.function(lambda s, *a, **k: s._totals(*a, **k),
                                   multi='totals',
                                   string='Totale scaduto',
                                   type='float',
                                   digits_compute=dp.get_precision('Account')),
        'in_currency': fields.function(lambda s, *a, **k: s._in_currency(*a, **k),
                                       type='boolean', string='In valuta')

    }

    def init(self, cr):
        import os

        with open(os.path.join(os.path.dirname(__file__), 'balance.sql')) as fo:
            cr.execute(fo.read())

    def _totals(self, cr, uid, ids, fiels, arg, context=None):
        cr.execute("""
            SELECT
              b.id,
              coalesce(sum(l.debit - l.credit), 0) balance,
              coalesce(sum(l.exp_move_h), 0) exp,
              coalesce(sum(CASE WHEN l.date_maturity < b.date_ref
                          AND l.move_group_id_h IS NOT NULL
                          THEN l.balance_group_h ELSE 0 END ), 0) overdue
            FROM partner_balance b
            LEFT JOIN partner_balance_line l ON b.id = l.balance_id
            WHERE b.id IN %s
            GROUP BY b.id
        """, [tuple(ids)])
        return {el.pop('id'): el for el in cr.dictfetchall()}

    def _in_currency(self, cr, uid, ids, field, arg, context=None):
        cr.execute("""
            SELECT b.id,
            case when exists(select 1 from partner_balance_line l where l.balance_id = b.id and
              l.amount_currency <> 0) then TRUE else FALSE end
              FROM partner_balance b
            WHERE b.id IN %s
        """, [tuple(ids)])
        return dict(cr.fetchall())


    def populate(self, cr, uid, ids, context=None):
        for b in self.browse(cr, uid, ids, context=None):
            cr.execute("""
                SELECT *
                FROM partner_balance.fn_partner_balance(%s, %s, %s);
            """, [[b.partner_id.id], b.date_ref, ['draft', 'posted'] if b.move_states == 'draft' else ['posted']])
            self.write(cr, uid, b.id, {'data_ids': [(0, 0, c) for c in cr.dictfetchall()]})
        return True

    def generate(self, cr, uid, partner_ids, date_ref, move_states, context=None):
        bals = []
        for p in partner_ids:
            id = self.create(cr, uid, {
                'partner_id': p,
                'date_ref': date_ref,
                'move_states': move_states
            })
            bals.append(id)
        self.populate(cr, uid, bals, context=context)
        return bals

    def action_generate(self, cr, uid, partner_ids, date_ref=None, move_states='posted', context=None):
        if date_ref is None:
            date_ref = fields.date.context_today(self, cr, uid, context)
        ids = self.generate(cr, uid, partner_ids, date_ref, move_states)
        action = {
            'type': 'ir.actions.act_window',
            'name': 'Estratti conto',
            'res_model': 'partner.balance',
            'domain': "[('id', 'in', %s)]" % repr(ids),
            'view_mode': 'tree,form' if len(ids) > 1 else 'form'
        }
        if len(ids) == 1: action['res_id'] = ids[0]
        return action


STATE_SEL = [
    ('open', 'Aperta'),
    ('exp', 'Esposta'),
    ('closed', 'Chiusa'),
]


class balance_line(orm.Model):
    _name = 'partner.balance.line'
    _auto = False
    _order = 'sequence'
    _columns = {
        'balance_id': fields.many2one('partner.balance', 'Estratto conto'),
        'sequence': fields.integer('Sequenza'),
        'bal_move_id': fields.many2one('account.move', 'Partita'),
        'date_move': fields.date('Data partita'),
        'move_line_id': fields.many2one('account.move.line', 'Voce contabile'),
        'move_group_id': fields.many2one('account.move.line', 'Mov. scadenza'),
        'state_move': fields.selection(STATE_SEL, 'Stato partita'),
        'state_group': fields.selection(STATE_SEL, 'Stato scadenza'),
        'date_maturity_group': fields.date('Data Scadenza partita'),
        'exp_move_h': fields.float('Esposizione', digits_compute=dp.get_precision('Account')),
        'exp_group_h': fields.float('Esposizione', digits_compute=dp.get_precision('Account')),
        'balance_move_h': fields.float('Saldo', digits_compute=dp.get_precision('Account')),
        'ref_move_h': fields.char('Rif. partita'),
        'name_move_h': fields.char('Partita'),
        'date_move_h': fields.date('Data partita'),
        'date_maturity': fields.date('Scadenza partita'),
        'ref_line': fields.char('Rif. movimento'),
        'date': fields.date('Data movimento'),
        'name_line': fields.char('Movimento'),
        'debit': fields.float('Dare', digits_compute=dp.get_precision('Account')),
        'credit': fields.float('Avere', digits_compute=dp.get_precision('Account')),
        'amount_currency': fields.float('Importo valuta', digits_compute=dp.get_precision('Account')),
        'currency_id': fields.many2one('res.currency', 'Valuta'),
        'bill_date_due': fields.date('Scadenza effetto'),
        'days': fields.integer('GG.'),
        'flag_h': fields.char(''),
    }


class partner(orm.Model):
    _inherit = 'res.partner'

    def action_open_balance(self, cr, uid, ids, context):
        return self.pool.get('partner.balance').action_generate(cr, uid, ids, context=context)
