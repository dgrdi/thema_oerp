# coding=utf-8
from openerp.osv import orm, fields

class move_line(orm.Model):
    _inherit = 'account.move.line'

    _columns = {
        'move_group_id': fields.many2one('account.move.line', 'Riferimento partita', readonly=True),
    }

    def init(self, cr):
        sp = super(move_line, self)
        if hasattr(sp, 'init'):
            sp.init(cr)
        import os
        with open(os.path.join(os.path.dirname(__file__), 'init.sql')) as fo:
            cr.execute(fo.read())

    def _uninstall(self, cr):
        import os
        with open(os.path.join(os.path.dirname(__file__), 'uninstall.sql')) as fo:
            cr.execute(fo.read())
