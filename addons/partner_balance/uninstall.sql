DROP TRIGGER IF EXISTS trg_check_recursion_ins ON account_move_line;
DROP TRIGGER IF EXISTS trg_check_recursion_upd ON account_move_line;
DROP TRIGGER IF EXISTS trg_track_move_before_del ON account_move_line;
DROP TRIGGER IF EXISTS trg_track_move_before_upd ON account_move_line;
DROP TRIGGER IF EXISTS trg_track_move_after_ins ON account_move_line;
DROP TRIGGER IF EXISTS trg_track_move_after_upd ON account_move_line;
DROP TRIGGER IF EXISTS trg_update_move_group_id ON account_move_line;
