DO LANGUAGE plpgsql $$
BEGIN
  IF NOT exists(SELECT
                  1
                FROM information_schema.schemata
                WHERE schema_name = 'partner_balance')
  THEN
    CREATE SCHEMA partner_balance;
  END IF;
END
$$;

CREATE OR REPLACE FUNCTION partner_balance.fn_resolve_origs(ids INT [])
  RETURNS TABLE(
  id INT,
  orig_id INT,
  rec_id INT
  ) SET search_path TO partner_balance, PUBLIC AS $b$
BEGIN

  CREATE TEMP TABLE res AS
    SELECT
      ml.id                                              anchor,
      ml.id,
      ml.bill_orig_id,
      coalesce(ml.reconcile_id, ml.reconcile_partial_id) rec_id
    FROM
      account_move_line ml
    WHERE ml.id = ANY (ids);

  WHILE TRUE LOOP
    WITH f AS (
        SELECT
          ml.id,
          ml2.bill_orig_id,
          row_number()
          OVER (PARTITION BY ml.id
            ORDER BY ml2.bill_orig_id NULLS LAST) n
        FROM res
          INNER JOIN account_move_line ml ON ml.id = res.id AND res.bill_orig_id IS NULL
          INNER JOIN account_move_reconcile r ON r.id = ml.reconcile_id OR r.id = ml.reconcile_partial_id
          INNER JOIN account_move_line ml2 ON r.id = ml2.reconcile_id OR r.id = ml2.reconcile_partial_id
    ) UPDATE res
    SET bill_orig_id = f.bill_orig_id
    FROM f
    WHERE f.id = res.id
          AND f.n = 1;

    UPDATE res
    SET id         = ml.id,
      bill_orig_id = ml.bill_orig_id,
      rec_id       = coalesce(ml.reconcile_id, ml.reconcile_partial_id)
    FROM account_move_line ml
    WHERE ml.id = res.bill_orig_id;

    EXIT WHEN NOT found;

  END LOOP;
  RETURN QUERY SELECT
                 r.anchor,
                 r.id,
                 r.rec_id
               FROM res r;
  DROP TABLE res;
END
$b$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION partner_balance.fn_move_group_id(ids INT [])
  RETURNS TABLE(
  id INT,
  move_group_id INT
  ) SET search_path TO partner_balance, PUBLIC AS $b$
BEGIN
  RETURN QUERY
  WITH r AS (
      SELECT
        o.id,
        coalesce(ml.id, o.orig_id) move_group_id,
        row_number()
        OVER (PARTITION BY o.id
          ORDER BY ml.date, ml.id) n
      FROM
        fn_resolve_origs(ids) o
        LEFT JOIN account_move_line ml ON ml.reconcile_id = o.rec_id OR ml.reconcile_partial_id = o.rec_id
  )
  SELECT
    r.id,
    r.move_group_id
  FROM r
  WHERE r.n = 1;
END
$b$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION partner_balance.trg_check_recursion()
  RETURNS TRIGGER
SET search_path TO partner_balance, PUBLIC AS $b$
DECLARE
  i INT = 0;
BEGIN
  CREATE TEMP TABLE seen (
    id      INT,
    orig_id INT,
    done    BOOLEAN,
    nw      BOOLEAN
  );
  CREATE TEMP TABLE ids (
    id INT
  );
  INSERT INTO seen (id, orig_id, done, nw) VALUES (new.id, new.bill_orig_id, FALSE, TRUE);
  WHILE TRUE LOOP
    i := i + 1;
    IF i > 20
    THEN
      RAISE EXCEPTION $e$Probabile ciclo infinito nel controllo ricorsivita`, questo e` un errore nel trigger.
    Modulo partner_balance, tabella account_move_line, trigger partner_balance.trg_check_recursion().
     Operazione: imposta bill_orig_id a % sul record con id %$e$, new.bill_orig_id, new.id;
    END IF;
    INSERT INTO seen
      SELECT
        ml.id,
        ml2.bill_orig_id,
        FALSE,
        TRUE
      FROM seen s
        INNER JOIN account_move_line ml ON s.id = ml.id
        INNER JOIN account_move_reconcile r ON r.id = ml.reconcile_partial_id OR r.id = ml.reconcile_id
        INNER JOIN account_move_line ml2 ON ml2.reconcile_partial_id = r.id OR ml2.reconcile_id = r.id
      WHERE ml2.bill_orig_id IS NOT NULL AND ml2.id <> ml.id
            AND NOT s.done;
    WITH ins AS (
      INSERT INTO seen
        SELECT
          ml.id,
          ml.bill_orig_id,
          FALSE,
          TRUE
        FROM seen s
          INNER JOIN account_move_line ml ON ml.id = s.orig_id
        WHERE NOT s.done
      RETURNING orig_id
    ) INSERT INTO ids (id)
      SELECT
        ins.orig_id
      FROM ins;
    EXIT WHEN NOT found;

    IF exists(
        SELECT
          1
        FROM ids
          INNER JOIN seen ON seen.id = ids.id
    )
    THEN
      RAISE EXCEPTION $e$Ricorsivita` nei collegamenti insoluti!
      modulo partner_balance, tabella account_move_line, colonna bill_orig_id;
      id % forma un ciclo con se` stesso attraverso %$e$, new.id, new.bill_orig_id;
    END IF;

    UPDATE seen
    SET done = TRUE
    WHERE nw = FALSE
          AND done = FALSE;
    UPDATE seen
    SET nw = FALSE
    WHERE nw = TRUE;
    DELETE FROM ids;
  END LOOP;

  DROP TABLE seen;
  DROP TABLE ids;
  RETURN NULL;
END
$b$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION partner_balance.fn_update_move_group_id(ids INT [])
  RETURNS VOID
SET search_path TO partner_balance, PUBLIC
AS $b$
DECLARE
  changed INT [];
BEGIN
  CREATE TEMP TABLE oldg
  AS SELECT
       ml.id,
       ml.move_group_id
     FROM account_move_line ml
     WHERE ml.id = ANY (ids);

  WITH upd AS (
    UPDATE account_move_line
    SET move_group_id = n.move_group_id
    FROM fn_move_group_id(ids) n
    WHERE n.id = account_move_line.id AND n.move_group_id IS DISTINCT FROM account_move_line.move_group_id
    RETURNING account_move_line.move_group_id
  ) SELECT
      array(
          SELECT
            ml.id
          FROM account_move_line ml
          WHERE
            ml.id <> ANY (ids)
            AND ml.move_group_id IN (
              SELECT
                upd.move_group_id
              FROM upd
              UNION
              SELECT
                o.move_group_id
              FROM oldg o
                INNER JOIN account_move_line ml ON ml.id = o.id AND ml.move_group_id IS DISTINCT FROM o.move_group_id
            )
      )
    INTO changed;
  DROP TABLE oldg;
  IF array_length(changed, 1) > 0
  THEN
    PERFORM fn_update_move_group_id(changed);
  END IF;
END
$b$ LANGUAGE plpgsql;

CREATE UNLOGGED TABLE IF NOT EXISTS partner_balance.track_ids (
  txid         INT,
  move_line_id INT,
  reconcile_id INT
);

CREATE OR REPLACE FUNCTION partner_balance.trg_track_move_before()
  RETURNS TRIGGER
SET search_path TO partner_balance, PUBLIC AS $b$
BEGIN
  INSERT INTO track_ids (txid, reconcile_id)
  VALUES
    (txid_current(), old.reconcile_id),
    (txid_current(), old.reconcile_partial_id);
  IF TG_OP = 'DELETE'
  THEN
    RETURN old;
  END IF;
  RETURN new;
END
$b$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION partner_balance.trg_track_move_after()
  RETURNS TRIGGER
SET search_path TO partner_balance, PUBLIC AS $b$
BEGIN
  INSERT INTO track_ids (txid, move_line_id, reconcile_id)
  VALUES
    (txid_current(), new.id, new.reconcile_id),
    (txid_current(), NULL, new.reconcile_partial_id);
  RETURN NULL;
END
$b$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION partner_balance.trg_update_move_group_id()
  RETURNS TRIGGER
SET search_path TO partner_balance, PUBLIC AS $b$
BEGIN
  PERFORM fn_update_move_group_id(
      array(
          WITH rec AS (
              SELECT DISTINCT
                t.reconcile_id
              FROM track_ids t
              WHERE t.reconcile_id IS NOT NULL AND t.txid = txid_current()
          )
          SELECT
            ml.id
          FROM account_move_line ml INNER JOIN rec ON ml.reconcile_id = rec.reconcile_id
                                                      OR ml.reconcile_partial_id = rec.reconcile_id
          UNION
          SELECT
            t.move_line_id
          FROM track_ids t
          WHERE t.move_line_id IS NOT NULL
      )
  );
  DELETE FROM track_ids
  WHERE txid = txid_current();
  RETURN NULL;
END
$b$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION partner_balance.fn_disable_triggers()
  RETURNS VOID
SET search_path TO partner_balance, PUBLIC AS $b$
BEGIN

  DROP TRIGGER IF EXISTS trg_check_recursion_ins ON account_move_line;

  DROP TRIGGER IF EXISTS trg_check_recursion_upd ON account_move_line;


  DROP TRIGGER IF EXISTS trg_track_move_before_del ON account_move_line;

  DROP TRIGGER IF EXISTS trg_track_move_before_upd ON account_move_line;

  DROP TRIGGER IF EXISTS trg_track_move_after_ins ON account_move_line;

  DROP TRIGGER IF EXISTS trg_track_move_after_upd ON account_move_line;

  DROP TRIGGER IF EXISTS trg_update_move_group_id ON account_move_line;

END
$b$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION partner_balance.fn_enable_triggers()
  RETURNS VOID
SET search_path TO partner_balance, PUBLIC AS $b$
BEGIN
  CREATE CONSTRAINT TRIGGER trg_check_recursion_ins
  AFTER INSERT ON account_move_line
  FOR EACH ROW
  WHEN (new.bill_orig_id IS NOT NULL)
  EXECUTE PROCEDURE partner_balance.trg_check_recursion();

  CREATE CONSTRAINT TRIGGER trg_check_recursion_upd
  AFTER UPDATE ON account_move_line
  FOR EACH ROW
  WHEN (new.bill_orig_id IS NOT NULL AND (old.bill_orig_id IS DISTINCT FROM new.bill_orig_id))
  EXECUTE PROCEDURE partner_balance.trg_check_recursion();


  CREATE TRIGGER trg_track_move_before_del
  BEFORE DELETE
  ON account_move_line
  FOR EACH ROW
  WHEN (
    coalesce(old.reconcile_id, old.reconcile_partial_id) IS NOT NULL
  )
  EXECUTE PROCEDURE partner_balance.trg_track_move_before();

  CREATE TRIGGER trg_track_move_before_upd
  BEFORE UPDATE OF reconcile_id, reconcile_partial_id
  ON account_move_line
  FOR EACH ROW
  WHEN (
    (old.reconcile_partial_id IS DISTINCT FROM new.reconcile_partial_id)
    OR (old.reconcile_id IS DISTINCT FROM new.reconcile_partial_id)
  )
  EXECUTE PROCEDURE partner_balance.trg_track_move_before();

  CREATE TRIGGER trg_track_move_after_ins AFTER INSERT ON account_move_line
  FOR EACH ROW EXECUTE PROCEDURE partner_balance.trg_track_move_after();


  CREATE TRIGGER trg_track_move_after_upd
  AFTER UPDATE OF bill_orig_id, reconcile_partial_id, reconcile_id, "date"
  ON account_move_line
  FOR EACH ROW
  WHEN (
    (old.bill_orig_id IS DISTINCT FROM new.bill_orig_id)
    OR (old.reconcile_id IS DISTINCT FROM new.reconcile_id)
    OR (old.reconcile_partial_id IS DISTINCT FROM new.reconcile_partial_id)
  )
  EXECUTE PROCEDURE partner_balance.trg_track_move_after();


  CREATE TRIGGER trg_update_move_group_id
  AFTER INSERT OR DELETE OR UPDATE OF bill_orig_id, reconcile_id, reconcile_partial_id, "date"
  ON account_move_line
  FOR EACH STATEMENT
  EXECUTE PROCEDURE partner_balance.trg_update_move_group_id();

END
$b$ LANGUAGE plpgsql VOLATILE;

SELECT
  partner_balance.fn_disable_triggers();
SELECT
  partner_balance.fn_enable_triggers();
