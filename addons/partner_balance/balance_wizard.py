from openerp.osv import orm, fields
from .balance import MOVE_STATES


class balance_wizard(orm.TransientModel):
    _name = 'partner.balance.wizard'

    _columns = {
        'date_ref': fields.date('Data riferimento', required=True),
        'move_states': fields.selection(MOVE_STATES, 'Registrazioni contabili', required=True),
    }

    _defaults = {
        'date_ref': lambda s, c, u, ctx: fields.date.context_today(s, c, u, ctx),
        'move_states': 'posted',
    }

    def generate(self, cr, uid, ids, context):
        assert len(ids) == 1
        obj = self.browse(cr, uid, ids[0], context=context)
        return self.pool.get('partner.balance').action_generate(cr, uid,
                                                                partner_ids=context.pop('partner_ids'),
                                                                date_ref=obj.date_ref,
                                                                move_states=obj.move_states)
