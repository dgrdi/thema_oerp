from openerp.osv import orm, fields

class company(orm.Model):
    _inherit = 'res.company'
    _columns = {
        'code': fields.char('Codice'),
    }

    _codes_cache = {}
    #TODO better cache

    def get_codes(self, cr, uid, context=None):
        if not self._codes_cache:
            v = self.read(cr, uid, self.search(cr, uid, []), fields=['code'], context=context)
            v = {v['id']: v['code'] for v in v}
            self._codes_cache = v
        return self._codes_cache


class CompanyName(object):

    def name_get(self, cr, user, ids, context=None):
        if not ids:
            return []
        if isinstance(ids, (int, long)):
            ids = [ids]
        cmp = self.pool.get('res.company').get_codes(cr, user, context=context)
        if self._rec_name in self._all_columns:
            rec_name_column = self._all_columns[self._rec_name].column
            display = lambda r: rec_name_column.as_display_name(cr, user, self, r[self._rec_name], context=context)
            company_code = lambda r: r['company_id'] if isinstance(r['company_id'], (int, long)) else r['company_id'][0]
            name = lambda r: '[%s] %s' % (cmp[company_code(r)], display(r))
            data = self.read(cr, user, ids, [self._rec_name, 'company_id'], load='_classic_write', context=context)
            return [(r['id'], name(r))
                        for r in data]

        return [(id, "%s,%s" % (self._name, id)) for id in ids]


class account(orm.Model):
    _inherit = 'account.account'

    def name_get(self, cr, user, ids, context=None):
        if not ids:
            return []
        if isinstance(ids, (int, long)):
            ids = [ids]
        cmp = self.pool.get('res.company').get_codes(cr, user, context=context)
        if self._rec_name in self._all_columns:
            rec_name_column = self._all_columns[self._rec_name].column
            display = lambda r: rec_name_column.as_display_name(cr, user, self, r[self._rec_name], context=context)
            company_code = lambda r: r['company_id'] if isinstance(r['company_id'], (int, long)) else r['company_id'][0]
            name = lambda r: '[%s] %s %s' % (cmp[company_code(r)], r['code'], display(r))
            data = self.read(cr, user, ids, [self._rec_name, 'company_id', 'code'], load='_classic_write', context=context)
            return [(r['id'], name(r))
                        for r in data]

        return [(id, "%s,%s" % (self._name, id)) for id in ids]


class journal(CompanyName, orm.Model):
    _inherit = 'account.journal'

class period(CompanyName, orm.Model):
    _inherit = 'account.period'

class fy(CompanyName, orm.Model):
    _inherit = 'account.fiscalyear'
