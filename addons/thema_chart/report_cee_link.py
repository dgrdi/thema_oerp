# coding=utf-8
from openerp.osv import orm, fields


class report_cee_link(orm.Model):
    _auto = False
    _name = 'thema.report.cee.link'

    _columns = {
        'company_id': fields.many2one('res.company', 'Azienda', readonly=True),
        'account_id': fields.many2one('account.account', 'Conto', readonly=True),
        'cee_debit_id': fields.many2one('account.account', 'Conto CEE in dare', readonly=True),
        'cee_credit_id': fields.many2one('account.account', 'Conto CEE in avere', readonly=True),
        'problem': fields.selection([
            ('nolink_both', 'Il conto non è collegato a nessun conto CEE'),
            ('nolink_debit', 'Il conto non è collegato a un conto CEE in dare'),
            ('nolink_credit', 'Il conto non è collegato a un conto CEE in avere'),
            ('link_noleaf', 'Il conto è collegato ad un conto CEE, ma non è una foglia nel piano dei conti'),
            ('link_noconsol', 'Il conto è collegato ad un conto CEE che non è una foglia nel piano CEE'),
            ('link_noreport', 'Il conto è collegato ad un conto CEE, ma non è un conto di conto economico o stato '
                              'patrimoniale')
        ], 'Problema', readonly=True),
    }

    def init(self, cr, uid=1):
        cr.execute("""
            DROP VIEW IF EXISTS thema_report_cee_link;
            CREATE VIEW thema_report_cee_link AS
              SELECT
                row_number() OVER (ORDER BY account_id) id,
                a.company_id,
                p.account_id,
                a.cee_debit_id,
                a.cee_credit_id,
                p.problem
              FROM (
                  SELECT
                    a.id account_id,
                    'nolink_both' problem
                  FROM
                    account_account a
                    INNER JOIN account_account_type aty ON a.user_type = aty.id
                    where a.cee_debit_id IS NULL and a.cee_credit_id IS NULL
                      AND aty.report_type <> 'none'
                      AND a.type in ('liquidity', 'receivable', 'payable', 'other')
                  UNION ALL
                  SELECT
                    a.id account_id,
                    'nolink_debit' problem
                  FROM
                    account_account a
                    INNER JOIN account_account_type aty ON a.user_type = aty.id
                    where a.cee_debit_id IS NULL and a.cee_credit_id IS NOT NULL
                      AND aty.report_type <> 'none'
                      AND a.type in ('liquidity', 'receivable', 'payable', 'other')
                  UNION ALL
                  SELECT
                    a.id account_id,
                    'nolink_credit' problem
                  FROM
                    account_account a
                    INNER JOIN account_account_type aty ON a.user_type = aty.id
                    where a.cee_debit_id IS NOT NULL and a.cee_credit_id IS NULL
                      AND aty.report_type <> 'none'
                      AND a.type in ('liquidity', 'receivable', 'payable', 'other')
                  UNION ALL
                  SELECT
                    a.id account_id,
                    'link_noleaf' problem
                  FROM
                    account_account a
                    INNER JOIN account_account_type aty ON a.user_type = aty.id
                    where (a.cee_debit_id IS NOT NULL or a.cee_credit_id IS NOT NULL)
                      AND EXISTS (SELECT 1 from account_account ac WHERE ac.parent_id = a.id)
                  UNION ALL
                  SELECT
                    a.id account_id,
                    'link_noconsol' problem
                  FROM
                    account_account a
                    INNER JOIN account_account_type aty ON a.user_type = aty.id
                    LEFT JOIN account_account cdeb ON cdeb.id = a.cee_debit_id
                    LEFT JOIN account_account ccred ON ccred.id = a.cee_credit_id
                    where (a.cee_debit_id IS NOT NULL or a.cee_credit_id IS NOT NULL)
                      AND aty.report_type <> 'none'
                      AND a.type in ('liquidity', 'receivable', 'payable', 'other')
                      AND (cdeb.type <> 'consolidation' OR ccred.type <> 'consolidation')
                  UNION ALL
                  SELECT
                    a.id account_id,
                    'link_noreport' problem
                  FROM
                    account_account a
                    INNER JOIN account_account_type aty ON a.user_type = aty.id
                    where (a.cee_debit_id IS NOT NULL or a.cee_credit_id IS NOT NULL)
                      AND aty.report_type = 'none'
                      AND NOT EXISTS (SELECT 1 from account_account ac WHERE ac.parent_id = a.id)
              ) p
              INNER JOIN account_account a ON a.id = p.account_id
              ;
        """)