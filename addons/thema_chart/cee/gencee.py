import csv
import re
import sys
from pprint import pprint
fn = 'tabcee.txt'



roots = {
    'ATTIVO': '1ATT',
    'PASSIVO': '2PASS',
    'CONTO ECONOMICO': '3CE',
}

type_map = {
    'CEE': 'account.data_account_type_view',
    'CEE.1ATT': 'account.data_account_type_asset',
    'CEE.2PASS': 'account.data_account_type_liability',
    'CEE.3CE.A': 'account.data_account_type_income',
    'CEE.3CE.B': 'account.data_account_type_expense',
    'CEE.3CE.C.15': 'account.data_account_type_income',
    'CEE.3CE.C.16': 'account.data_account_type_income',
    'CEE.3CE.C.17': 'account.data_account_type_expense',
    'CEE.3CE.C.17-bis.1': 'account.data_account_type_income',
    'CEE.3CE.C.17-bis.2': 'account.data_account_type_income',
    'CEE.3CE.D.18': 'account.data_account_type_income',
    'CEE.3CE.D.19': 'account.data_account_type_expense',
    'CEE.3CE.E.20': 'account.data_account_type_income',
    'CEE.3CE.E.21': 'account.data_account_type_expense',
    'CEE.3CE.22': 'account.data_account_type_expense',
}
name_map = {
    'CEE.3CE.22': 'CEE.3CE.F22'
}

def get_type(code):
    for k in sorted(type_map.keys(), key=lambda x: len(x), reverse=True):
        if code[:len(k)] == k:
            return type_map[k]
    assert False, 'no type found for %s' % code

name_rx = re.compile(r'([\w-]+\)|-|\w+ -)\s*([^:]*)\:{0,1}\s*$')
def name_and_code(name):
    bc = roots.get(name)
    if bc is None:
        bc, name = name_rx.match(name).group(1, 2)
    else:
        bc = bc.strip()
    if bc == '-': bc = None
    return bc, name

def strip_punctuation(bc):
    if bc is None or not bc.strip(): return None
    if bc[-1] in ('-', ')'): bc = bc[:-1].strip()
    return bc

def build_tree():

    root =  {
        'title': '',
        'id': 'CEE',
        'code': 'CEE',
        'name': 'Piano conti CEE',
        'children': [],
        'parent': None,
        'user_type:id': get_type('CEE'),
        'type': 'view',
        'parent_id:id': None,
    }
    indentations = [-10]
    parent = root
    last = root
    counter = 0
    for l in csv.DictReader(open(fn)):
        if l['TIPO_RIGA'] == 'O': continue
        id, name = l['NUMERO_RIGA'], l['DES_RIGA']
        lvl = 0
        for c in name:
            if c!= ' ': break
            lvl += 1
        name = name.strip()
        try:
            bc, name = name_and_code(name)
        except:
            continue
        if name in roots.keys():
            lvl = -10
        if bc == '17-bis)':
            lvl -= 4
        if bc == '22)':
            lvl -= 3
        #print 'lcode: %s, level: %s (+%s), indentations: %s' % (last['code'], lvl, len(bc or ''), indentations)
        if lvl > indentations[-1]:
            indentations.append(lvl + len(bc or ''))
            parent = last
            last['type'] = 'view'
        while lvl + len(bc or '') < indentations[-1]:
            parent = parent['parent']
            indentations.pop()
        if bc is None:
            counter += 1
            code = parent['code'] + '.' + str(counter)
        else:
            counter = 0
            code = parent['code'] + '.' + strip_punctuation(bc)
        curacct = {
            'parent_id:id': parent['id'],
            'name': name,
            'id': 'CEE%s' % id,
            'title': strip_punctuation(bc) if bc not in roots.values() else '',
            'code': name_map.get(code) or code,
            'children': [],
            'parent': parent,
            'user_type:id': get_type(code),
            'type': 'consolidation',
        }

        parent['children'].append(curacct)
        last = curacct
    return root


def print_tree(root, indent=0):
    print ' ' * indent, root['title'], root['name'], '(%s)' % root['code'], ': %s' % root['user_type:id']
    for c in root['children']:
        print_tree(c, indent+4)


def build_csv(root):
    writer = csv.DictWriter(sys.stdout, set(root.keys()) - set(['parent', 'children']))
    writer.writeheader()
    def traverse(r):
        children = r.pop('children')
        r.pop('parent')
        writer.writerow(r)
        for c in children:
            traverse(c)
    traverse(root)

if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == 'show':
        print_tree(build_tree())
    else:
        build_csv(build_tree())



