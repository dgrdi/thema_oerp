# coding=utf-8
from openerp.osv import orm, fields
from logging import getLogger
from openerp.osv.orm import browse_record, browse_record_list

logger = getLogger(__name__)

class account_chart_template(orm.Model):
    _inherit = 'account.chart.template'

    _columns = {
        'cee_root_id': fields.many2one('account.account.template', 'Radice del conto CEE', domain=[('cee', '=', True)]),
        'skip_bank_generation': fields.boolean('Skip bank account and journal generation'),
    }

class account_account_template(orm.Model):
    _inherit = 'account.account.template'
    _columns =  {
        'cee': fields.boolean('Conto CEE'),
        'title': fields.char('Titolo', help='Viene usato nel report CEE per suddividere '
                                            'il bilancio in punti.'),
        'cee_debit_id': fields.many2one('account.account.template', 'Conto CEE per saldo in dare',
                                        domain=[('cee', '=', True)]),
        'cee_credit_id': fields.many2one('account.account.template', 'Conto CEE per saldo in avere',
                                         domain=[('cee', '=', True)]),
    }

    _sql_constraints = [
        (
            'cee_no_link',
            "CHECK( (cee_debit_id IS NULL and cee_credit_id IS NULL) OR NOT cee)",
            "Un conto CEE non può essere collegato ad un bilancio CEE!",
        ),
    ]

    def get_account_vals(self, cr, uid, account_template, context):
        vals = super(account_account_template, self).get_account_vals(cr, uid, account_template, context)
        vals.update({
            'cee': account_template.cee,
            'title': account_template.title,
        })
        return vals

    def generate_account(self, cr, uid, chart_template_id, tax_template_ref,
                         acc_template_ref, code_digits, company_id, context=None):
        res = super(account_account_template, self).generate_account(cr, uid, chart_template_id,
                                                                     tax_template_ref,
                                                                     acc_template_ref,
                                                                     code_digits,
                                                                     company_id,
                                                                     context)
        
        template_data = self.read(cr, uid, res.keys(),
                                  fields=('cee_debit_id', 'cee_credit_id'),
                                  context=context)

        # generate CEE chart

        cee_accounts = {}
        chart = self.pool.get('account.chart.template').browse(cr, uid, chart_template_id, context=context)
        if chart.cee_root_id:
            logger.debug('Creating CEE chart...')
            company = self.pool.get('res.company').browse(cr, uid, company_id, context=context)
            cee_accounts = self.generate_recursive(cr, uid, chart.cee_root_id.id,
                                                   parent_id=None, company_id=company_id, acc_template_ref={},
                                                   name=company.name + ' (CEE)',
                                                   context=context)

        # update generated accounts with data from templates
        template_data = {x['id']: x for x in template_data}
        for t in template_data.values():
            t.pop('id')
            t['cee_debit_id'] = cee_accounts.get(t['cee_debit_id'] and t['cee_debit_id'][0])
            t['cee_credit_id'] = cee_accounts.get(t['cee_credit_id'] and t['cee_credit_id'][0])

        acc_obj = self.pool.get('account.account')
        for template_id, account_id in res.items():
            acc_obj.write(cr, uid, account_id, template_data[template_id], context=context)
        return res
    

class account_account(orm.Model):
    _inherit = 'account.account'

    _columns = {
        'cee': fields.boolean('Conto CEE'),
        'title': fields.char('Titolo', help='Viene usato nel report CEE per suddividere '
                                            'il bilancio in punti.'),
        'cee_debit_id': fields.many2one('account.account', 'Conto CEE per saldo in dare',
                                        domain=[('cee', '=', True)]),
        'cee_credit_id': fields.many2one('account.account', 'Conto CEE per saldo in avere',
                                         domain=[('cee', '=', True)]),
        'child_consol_debit_ids': fields.many2many('account.account',
                                                    'account_account_consol_debit_rel',
                                                    'child_id', 'parent_id',
                                                    'Conti da consolidare in dare'),
        'child_consol_credit_ids': fields.many2many('account.account',
                                                    'account_account_consol_credit_rel',
                                                    'child_id', 'parent_id',
                                                    'Conti da consolidare in avere'),

    }

    _sql_constraints = [
        (
            'cee_no_link',
            "CHECK( (cee_debit_id IS NULL and cee_credit_id IS NULL) OR NOT cee)",
            "Un conto CEE non può essere collegato ad un bilancio CEE!",
        ),
    ]

    def _get_children_and_consol(self, cr, uid, ids, context=None):
        children = super(account_account, self)._get_children_and_consol(cr, uid, ids, context)
        child_ids = []
        if isinstance(ids, (int, long)): ids = [ids]
        for acc in self.browse(cr, uid, ids, context=context):
            for child in acc.child_consol_debit_ids:
                bal = self.read(cr, uid, child.id, fields=('balance',), context=context)
                if bal > 0:
                    child_ids.append(child.id)
            for child in acc.child_consol_credit_ids:
                bal = self.read(cr, uid, child.id, fields=('balance',), context=context)
                if bal < 0:
                    child_ids.append(child.id)
        if child_ids:
            child_ids += self._get_children_and_consol(cr, uid, child_ids, context)
        return children + child_ids

    def update_cee(self, cr, uid, ids, context=None):
        for account in self.browse(cr, uid, ids, context=context):
            # find accounts holding this one in consolidation
            consol = [
                '|',
                '|',
                ('child_consol_ids', '=', account.id),
                ('child_consol_debit_ids', '=', account.id),
                ('child_consol_credit_ids', '=', account.id),
            ]
            consol = self.search(cr, uid, consol, context=context)
            #wipe the link
            for c in consol:
                self.write(cr, uid, c, {
                    'child_consol_ids': [(3, account.id)],
                    'child_consol_debit_ids': [(3, account.id)],
                    'child_consol_credit_ids': [(3, account.id)],
                }, context=context)
            # and link the new one
            if account.cee_debit_id or account.cee_credit_id:
                if account.cee_debit_id == account.cee_credit_id:
                    self.write(cr, uid, account.cee_credit_id.id, {
                        'child_consol_ids': [(4, account.id)],
                    }, context=context)
                elif account.cee_debit_id:
                    self.write(cr, uid, account.cee_debit_id.id, {
                        'child_consol_debit_ids': [(4, account.id)],
                    })
                else:
                    self.write(cr, uid, account.cee_credit_id.id, {
                        'child_consol_credit_ids': [(4, account.id)],
                    })
        return True


    def action_update_cee(self, cr, uid, context=None):
        self.update_cee(cr, uid, context.get('active_ids'), context=context)
        return True

    #TODO check recursion in consolidated debit/credit, check that the same account is in at most one
    # of the 3 consolidated children lists
    #TODO check that cee accounts point to consolidation accounts



class wizard_multi_charts_accounts(orm.TransientModel):
    _inherit = 'wizard.multi.charts.accounts'

    def _create_bank_journals_from_o2m(self, cr, uid, obj_wizard, company_id, acc_template_ref, context=None):
        if obj_wizard.chart_template_id.skip_bank_generation:
            return True
        else:
            return super(wizard_multi_charts_accounts, self)._create_bank_journals_from_02m(cr, uid, obj_wizard,
                                                                                            company_id,
                                                                                            acc_template_ref,
                                                                                            context=None)

class account_tax(orm.Model):
    _inherit = 'account.tax'

    _sql_constraints = [
        ('name_company_uniq', 'unique(name, type_tax_use, company_id)', 'Tax Name must be unique per company!'),
    ]