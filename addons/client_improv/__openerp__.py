{
    'name': 'Client UI improvements',
    'version': '0.2',
    'category': 'UI',
    'description': """Client ui improvements""",
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': ['base', 'web'],
    'init_xml': [],
    'data':[
        ],
    'demo_xml': [],
    'js': ['static/js/*.js',
           'static/js/widgets/*.js',
           'static/js/keyboard/*.js'],
    'qweb': ['static/xml/*.xml'],
    'css': ['static/css/*.css'],
    'test': [],
    'installable': True,
    'auto_install': True,
}

