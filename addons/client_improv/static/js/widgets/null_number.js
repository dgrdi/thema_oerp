client_improv = typeof client_improv == 'undefined' ? {}: client_improv;
client_improv.widgets = client_improv.widgets || {};

client_improv.widgets.null_number = function(instance){
    var QWeb = instance.web.qweb;
    var _t = instance.web._t;
    var _lt = instance.web._lt;
    var module = client_improv.widgets;
    module.FieldNullNumber = instance.web.form.FieldFloat.extend({
        set_value: function(value){
            if (!value) {
                value = null;
            }
            this.set({'value': value});
        }
    });
    instance.web.form.widgets.add('nullnumber', 'client_improv.widgets.FieldNullNumber');
    instance.web.list.Column = instance.web.list.Column.extend({
        _format: function(row_data, options) {
            if (this.suppress && this.function == null) {
                var ctx = {};
                _.each(row_data, function(value, key){
                    ctx[key] = value.value;
                });
                if (py.eval(this.suppress, ctx)) {
                    return _.escape(instance.web.format_value(
                        undefined, this, options.value_if_empty
                    ));
                }
            }
            if (this.nullnumber && !row_data[this.id].value && _(['float', 'integer']).contains(this.type) ) {
                return _.escape(instance.web.format_value(
                    undefined, this, options.value_if_empty
                ));
            }
            return this._super(row_data, options);
        }
    });

};