client_improv = typeof client_improv == 'undefined' ? {} : client_improv;
client_improv.widgets = client_improv.widgets || {};

client_improv.widgets.details_button = function(inst){
    var QWeb = inst.web.qweb;
    var _t = inst.web._t,
       _lt = inst.web._lt;
    var mdl = client_improv.widgets;

    mdl.DetailButton = inst.web.list.Column.extend({
        icon: 'STOCK_OPEN',
        type: 'details',
        init: function(){
            this._super.apply(this, arguments);
            this.id = 'cimp_btn_details';
        },
        format: function(row_data, options){
            return QWeb.render('cimp.DetailButton', {
                widget: this,
                prefix: inst.session.prefix
            });
        }
    });

    inst.web.list.columns.add('details', 'instance.client_improv.widgets.DetailButton');

    inst.web.form.One2ManyListView = inst.web.form.One2ManyListView.extend({

        do_button_action: function(name, id, callback){
            if(name != 'cimp_btn_details'){
                this._super(name, id, callback);
            } else {
                this.button_open_details(id, callback);
            }
        },
        view_ref_cache: {},
        resolve_view_id: function(view_ref){
            var view_id = this.view_ref_cache[view_ref];
            var self = this;
            var res;
            if (!view_id){
                var md = new inst.web.Model('ir.model.data');
                var mdl = null, xid = view_ref;
                view_id = view_ref.split('.');
                if (view_id.length == 2) {
                    mdl = view_id[0];
                    xid = view_id[1];
                }
                var domain = [['name', '=', xid], ['model', '=', 'ir.ui.view']];
                if (mdl) {
                    domain.push(['module', '=', mdl]);
                }
                res = md.query(['res_id']).filter(domain).all().then(function(res){
                    view_id = res[0]['res_id'];
                    self.view_ref_cache[view_ref] = view_id;
                    return view_id;
                });
            } else {
                res = $.Deferred();
                res.resolveWith(this, [view_id]);
            }
            return res;
        },

        button_open_details: function(id, callback){
            var det_field = _(this.fields_view.arch.children).find(function(e){return e.tag == 'details'});
            var view_id = det_field.attrs.view_id  || null;
            var context = det_field.attrs.context;
            var def;

            if (view_id) {
                def = this.resolve_view_id(view_id);
            } else {
                def = $.Deferred();
                def.resolve();
            }
            var self = this;
            def.then(function(view_id){
                self.ViewManager.show_details(id, view_id, context, callback);
            });
        }
    });

    inst.web.form.One2ManyViewManager = inst.web.form.One2ManyViewManager.extend({


        get_defaults_from_context: function(context){
            var rx = /^default_(.*)$/;
            var df = _(context.eval()).map(function(k, v){return [k, rx.exec(v)]});
            df = _(df).filter(function(p){return p[1] !== null});
            df = _(df).map(function(p){ return [p[0], p[1][1]]});
            var res = {};
            _(df).each(function(p){res[p[1]] = p[0]});
            return res;
        },
        show_details: function(id, view_id, context, callback){
            var self = this;
            var pop = new inst.web.form.FormOpenPopup(this);
            pop.on('closed', this, function(){callback(id)});
            var ctx = this.o2m.build_context();
            ctx.add(context);
            var defaults = self.get_defaults_from_context(ctx);
            console.log(defaults);
            pop.show_element(self.o2m.field.relation, id, ctx, {
                view_id: view_id,
                title: _t("Open: ") + self.o2m.string,
                create_function: function(data, options) {
                    return self.o2m.dataset.create(data, options).done(function(r) {
                        self.o2m.dataset.set_ids(self.o2m.dataset.ids.concat([r]));
                        self.o2m.dataset.trigger("dataset_changed", r);
                    });
                },
                write_function: function(id, data, options) {
                    return self.o2m.dataset.write(id, data, {});
                },
                alternative_form_view: self.o2m.field.views && !view_id ? self.o2m.field.views["form"] : undefined,
                parent_view: self.o2m.view,
                child_name: self.o2m.name,
                read_function: function() {
                    return self.o2m.dataset.read_ids.apply(self.o2m.dataset, arguments).then(function(res){
                        return _(res).map(function(vals){

                            if (!vals.id || typeof vals.id == 'string') {
                                _.each(vals, function(v, k){
                                    if (!v) {
                                        vals[k] = defaults[k] || vals[k];
                                    }
                                });
                            }
                            console.log(vals);
                            return vals;
                        })
                    });
                },
                form_view_options: {'not_interactible_on_create':true},
                readonly: self.o2m.get("effective_readonly")
            });


        }

    });

};