client_improv = typeof client_improv == 'undefined' ? {} : client_improv;
client_improv.widgets = client_improv.widgets || {};

client_improv.widgets.many2many_button = function(inst) {
    var _t = inst.web._t;
    var mdl = client_improv.widgets;

    mdl.Many2ManyButton = inst.web.form.FieldMany2Many.extend({
        template: 'cimp.Many2ManyButton',
        events: {
            'click .cimp_many2manybutton_add': function(e){
                var pop = new inst.web.form.SelectCreatePopup(this);
                pop.select_element(
                    this.field.relation,
                    {
                        title: this.string
                    },
                    new inst.web.CompoundDomain(this.build_domain(), ["!", ["id", "in", this.dataset.ids]]),
                    this.build_context()
                );
                var self = this;
                pop.on("elements_selected", self, function(element_ids) {
                    _(element_ids).each(function (id) {
                        if(! _.detect(self.dataset.ids, function(x) {return x == id;})) {
                            self.dataset.set_ids(self.dataset.ids.concat([id]));
                            self.dataset_changed();
                        }
                    });
                });
            }
        },
        init: function(field_manager, node) {
            this._super(field_manager, node);
            this.dataset = new inst.web.form.Many2ManyDataSet(this, this.field.relation);
            this.dataset.m2m = this;
            var self = this;
            this.dataset.on('unlink', self, function(ids) {
                self.dataset_changed();
            });
            this.set_value([]);
            this.on('change:effective_readonly', this, function(){
                self.renderElement();
            })
        },

        initialize_content: function() {
            var self = this;
            this.$el.addClass('oe_form_field oe_form_field_many2many');

        },
        destroy_content: function(){
        }

    });

    inst.web.form.widgets.add('many2many_button', 'instance.client_improv.widgets.Many2ManyButton');
};