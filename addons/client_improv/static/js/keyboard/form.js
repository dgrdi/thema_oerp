client_improv = typeof client_improv == 'undefined' ? {} : client_improv;
client_improv.keyboard = client_improv.keyboard || {};

client_improv.keyboard.form = function(inst){
    var mdl = client_improv.keyboard;

    var sc = [
        new mdl.Shortcut('save', {
            key: 's',
            ctrlKey: true
        }),
        new mdl.Shortcut('delete', {
            key: 'd',
            ctrlKey: true
        }),
        new mdl.Shortcut('edit', {
            key: 'e',
            ctrlKey: true
        }),
        new mdl.Shortcut('new', {
            key: 'n',
            ctrlKey: true
        }),
        new mdl.Shortcut('cancel',{
            key: 'escape'
        })
    ];
    var shortcuts = {};
    for (var s in sc) shortcuts[sc[s].shortcutKey()] = sc[s];

    mdl.FormViewContext = mdl.Context.extend({
        name: 'form',
        init: function(view){
            this.view = view;
        },
        shortcuts: shortcuts,
        perform: function(action){
            var ga = _.bind(this.view.guard_active, this.view);
            switch(action) {
                case 'save':
                    ga(this.view.on_button_save)();
                    break;
                case 'delete':
                    ga(this.view.on_button_delete)();
                    break;
                case 'edit':
                    ga(this.view.on_button_edit)();
                    break;
                case 'new':
                    ga(this.view.on_button_create)();
                    break;
                case 'cancel':
                    ga(this.view.on_button_cancel)();
                    break;
            }
        }
    });

    var popsc = _.extend({}, shortcuts);
    delete shortcuts[mdl.charmap.escape];

    mdl.PopupFormContext = mdl.Context.extend({
        name: 'form',
        init: function(view){
            this.view = view;
        },
        shortcuts: popsc,
        perform: function(action){
            if (action == 'new') action = 'save-new';
            this.view.getParent().$buttonpane.find('.oe_abstractformpopup-form-' + action).click();
      }
    });

    inst.web.FormView = inst.web.FormView.extend({
        init: function(){
            this._super.apply(this, arguments);
            if (this.getParent() instanceof inst.web.form.AbstractFormPopup) {
                this.kb_context = new mdl.PopupFormContext(this);
            } else {
                this.kb_context = new mdl.FormViewContext(this);
            }
        },
        do_show: function(){
            mdl.keyboard_manager.register(this.kb_context);
            return this._super.apply(this, arguments);
        },
        do_hide: function(){
            mdl.keyboard_manager.unregister(this.kb_context);
            return this._super.apply(this, arguments);
        },
        destroy: function(){
            mdl.keyboard_manager.unregister(this.kb_context);
            return this._super();
        }
    });
};