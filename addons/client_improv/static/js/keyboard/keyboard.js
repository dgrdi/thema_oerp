client_improv = typeof client_improv == 'undefined' ? {} : client_improv;
client_improv.keyboard = client_improv.keyboard || {};

client_improv.keyboard.keyboard = function(inst){
    var mdl = client_improv.keyboard;

    mdl.Shortcut = inst.web.Class.extend({
        modifiers: {
            ctrlKey: 'c',
            altKey: 'm',
            shiftKey: 's'
        },
        which: null,
        init: function(action, spec){
            if (action != null) this.action = action;
            var self = this;
            _(this.modifiers).each(function (v, k){
                self[k] = spec[k] || false;
            });
            this.which = spec.which;
            if (spec.key){
                this.key = spec.key;
                this.which = mdl.charmap[spec.key];
            } else {
                this.key = mdl.charmap.reverse(this.which);
            }
        },
        shortcutKey: function(){
            return mdl.Shortcut.keyFor(this);
        }
    });

    mdl.Shortcut.keyFor = function(event){
        var res = [];
        _(mdl.Shortcut.prototype.modifiers).each(function(v, k){
            if (event[k]) res.push(v);
        });
        res.sort();
        res = res.join('') + event.which.toString();
        return res;
    };

    mdl.Context = inst.web.Class.extend({
        name: null,
        init: function(){
            this.shortcuts = {};
        },
        register: function(shortcut){
            this.shortcuts[shortcut.shortcutKey()] = shortcut;
        },
        trigger: function(event){
            var sc = this.shortcuts[mdl.Shortcut.keyFor(event)];
            if (sc != null) {
                event.preventDefault();
                this.perform(sc.action);
                return true;
            }
            return false;
        }
    });

    mdl.KeyboardManager = inst.web.Class.extend({
        contexts: {},
        init: function(){
            this.handle_key = _.bind(this.handle_key, this);
            $(document).on('keydown', this.handle_key);
        },
        handle_key: function(e){

            if (!e.altKey && !e.ctrlKey && !e.shiftKey && !mdl.charmap.simple[e.which]) return;
            for (var c in this.contexts) {
                var ctx = this.contexts[c][0];
                if (ctx != null) {
                    var res = ctx.trigger(e);
                    if (res) break;
                }
            }
        },
        register: function(context){
            var cx = this.contexts[context.name];
            if (this.contexts[context.name] == null) {
                this.contexts[context.name] = [];
            }
            this.contexts[context.name].splice(0, 0, context);
        },
        unregister: function(context){
            if (this.contexts[context.name]) {
                var i = this.contexts[context.name].indexOf(context);
                if (i > -1) {
                    this.contexts[context.name].splice(i, 1);
                }
            }
        }
    });

    mdl.keyboard_manager = new mdl.KeyboardManager();

};