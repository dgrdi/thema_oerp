client_improv = typeof client_improv == 'undefined' ? {} : client_improv;

openerp.client_improv = function(instance){
    instance.client_improv = client_improv;
    var cimp = instance.client_improv;
    cimp.widgets.many2many_button(instance);
    cimp.widgets.details_button(instance);
    cimp.widgets.null_number(instance);
    cimp.keyboard.main(instance);
};