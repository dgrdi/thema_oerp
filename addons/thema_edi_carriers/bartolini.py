from openerp.osv import orm, fields
import re
import string


class partner(orm.Model):
    _inherit = 'res.partner'
    _columns = {
        'phone_clean': fields.function(lambda s, *a, **k: s._phone_clean(*a, **k), type='char',
                                       string='Telefono (pulito)'),
    }

    def _phone_clean(self, cr, uid, ids, field, arg, context=None):
        phones = self.read(cr, uid, ids, fields=['phone'], context=context)
        clean = lambda x: filter(lambda y: y in string.digits, x) if x else False
        phones = {el['id']: clean(el['phone']) for el in phones}
        return phones


class delivery_delivery(orm.Model):
    _inherit = 'delivery.delivery'

    _columns = {
        'bartolini_vabcbo': fields.function(lambda s, *a, **k: s._bartolini(*a, **k), string='VABCBO',
                                            type='char', multi='bartolini'),
        'bartolini_vabtic': fields.function(lambda s, *a, **k: s._bartolini(*a, **k), string='VABTIC',
                                            type='char', multi='bartolini'),
    }

    def _bartolini(self, cr, uid, ids, field, arg, context=None):
        res = {}
        for delv in self.browse(cr, uid, ids, context=context):
            if delv.cod and delv.invoice_id and delv.invoice_id.amount_total:
                res.setdefault(delv.id, {})['bartolini_vabcbo'] = '4'
                res[delv.id]['bartolini_vabtic'] = 'TM'
            else:
                res.setdefault(delv.id, {})['bartolini_vabcbo'] = '1'
                res[delv.id]['bartolini_vabtic'] = ''
        return res


    def ds_email_alert(self, cr, uid, records, context=None):
        res = []
        for el in records:
            if el.partner_id and el.partner_id.email and el.partner_id.country_id.code == 'IT':
                res.append(el)
        return res

    def ds_email_alert_cont(self, cr, uid, records, context=None):
        res = []
        for el in records:
            if el.partner_id and el.partner_id.email and el.partner_id.country_id.code == 'IT' and len(
                    el.partner_id.email) > 35:
                res.append(el)
        return res

    def ds_phone_alert(self, cr, uid, records, context=None):
        phone_rx = re.compile('^(0039|39)[1-9]')
        res = []
        for el in records:
            if el.partner_id and el.partner_id.country_id.code == 'IT' and el.partner_id.phone_clean and phone_rx.match(
                    el.partner_id.phone_clean):
                res.append(el)
        return res


class country(orm.Model):
    _inherit = 'res.country'
    _columns = {
        'bartolini_code': fields.char('Codice bartolini'),
    }
