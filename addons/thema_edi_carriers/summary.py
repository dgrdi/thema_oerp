# coding=utf-8
import base64
from openerp.osv import orm, fields
from dateutil.parser import parse


class delivery_summary(orm.Model):
    _name = 'delivery.summary'
    _description = u'Borderò'

    _columns = {
        'name': fields.function(lambda s, *a, **k: s._name_g(*a, **k),
                                fnct_search=lambda s, *a, **k: s._name_s(*a, **k),
                                string='Nome'),
        'carrier_id': fields.many2one('delivery.carrier', 'Vettore', required=True),
        'date': fields.date('Data', required=True),
        'delivery_ids': fields.many2many('delivery.delivery', 'delivery_summary_rel', 'summary_id', 'delivery_id',
                                         string='Spedizioni',
                                         domain=[('state', '=', 'done'), ('carrier_id', '!=', False)]),
        'encoder_id': fields.related('carrier_id', 'encoder_id', string='Encoder', type='many2one',
                                     relation='thema.edi.encoder', readonly=True),
        'mail_template_id': fields.related('carrier_id', 'mail_template_id', string='Modello email', type='many2one',
                                           relation='email.template', readonly=True),
    }

    _defaults = {
        'date': fields.date.context_today,
    }

    def _name_g(self, cr, uid, ids, field, arg, context=None):
        res = {}
        for dv in self.browse(cr, uid, ids, context=context):
            res[dv.id] = "{0} del {1:%d/%m/%y}".format(dv.carrier_id.name, parse(dv.date))
        return res

    def _name_s(self, cr, uid, obj, field, domain, context=None):
        expand = lambda f, op, val: ['|', ('carrier_id.name', op, val), ('date', op, val)]
        return expand(*domain[0])


    def carrier_hashcode(self, cr, uid, carrier, context=None):
        return carrier.partner_id.id, carrier.encoder_id and carrier.encoder_id.id or False

    def generate(self, cr, uid, delivery_ids, context=None):
        delivs = {}
        carriers = {}
        dv = self.pool.get('delivery.delivery')
        delivery_ids = dv.search(cr, uid,
                                 [('state', '=', 'done'), ('carrier_id', '!=', False), ('id', 'in', delivery_ids)],
                                 context=context)
        for dv in self.pool.get('delivery.delivery').browse(cr, uid, delivery_ids, context=context):
            key = self.carrier_hashcode(cr, uid, dv.carrier_id, context=context)
            carriers[key] = dv.carrier_id.id
            delivs.setdefault(key, []).append(dv.id)
        cids = []
        for key, dvid in delivs.items():
            cids.append(self.create(cr, uid, {'carrier_id': carriers[key], 'delivery_ids': [(4, id) for id in dvid]},
                                    context=context))
        return cids


    def export_default(self, cr, uid, ids, context=None):
        enc = self.pool.get('thema.edi.wizard')
        for smy in self.browse(cr, uid, ids, context=context):
            encoder = smy.carrier_id and smy.carrier_id.encoder_id or None
            if encoder and encoder.model == 'delivery.delivery':
                return enc.encode(cr, uid, encoder.code, [el.id for el in smy.delivery_ids], context=context)
            elif encoder and encoder.model == 'delivery.summary':
                return enc.encode(cr, uid, encoder.code, [smy.id], context=context)

    def export_email(self, cr, uid, ids, context=None):
        enc = self.pool.get('thema.edi.encoder')
        mail = self.pool.get('mail.mail')
        tpl = self.pool.get('email.template')
        for smy in self.browse(cr, uid, ids, context=context):
            tplid = smy.carrier_id.mail_template_id and smy.carrier_id.mail_template_id.id or False
            encoder = smy.carrier_id.encoder_id and smy.carrier_id.encoder_id.code or False
            if tplid and encoder:
                if encoder.model == 'delivery.delivery':
                    record_ids = [el.id for el in smy.delivery_ids]
                elif encoder.model == 'delivery.summary':
                    record_ids = [smy.id]
                else:
                    continue
                files = enc.encode(cr, uid, encoder, record_ids, context=context)
                if files:
                    mval = tpl.generate_email(cr, uid, tplid, smy.id, context=context)
                    mval.update({
                        'auto_delete': False,
                        'attachment_ids': [(0, 0, {'name': fname, 'datas_fname': fname, 'datas': base64.b64encode(fconts)})
                                           for fname, fconts
                                           in files.items()]
                    })
                    mval.pop('email_from', False)
                    mail.create(cr, uid, mval, context=context)


class delivery_carrier(orm.Model):
    _inherit = 'delivery.delivery'

    _columns = {
        'summary_ids': fields.many2many('delivery.summary', 'delivery_summary_rel', 'delivery_id', 'summary_id',
                                        string=u'Borderò'),
    }


class delivery_summary_wizard(orm.TransientModel):
    _name = 'delivery.summary.wizard'

    _columns = {
        'carrier_ids': fields.many2many('delivery.carrier', string='Vettori'),
        'date_start': fields.date('Data iniziale'),
        'date_stop': fields.date('Data finale'),
        'exclude_done': fields.boolean(u'Escludi spedizioni già presenti in altri borderò'),
    }

    _defaults = {
        'carrier_ids': lambda s, c, u, ctx=None: s.pool.get('delivery.carrier').search(c, u, [], context=ctx),
        'date_start': fields.date.context_today,
        'date_stop': fields.date.context_today,
        'exclude_done': True,
    }

    def generate(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
        del_dom = [('carrier_id', 'in', [el.id for el in obj.carrier_ids])]
        if obj.date_start:
            del_dom.append(('date', '>=', obj.date_start))
        if obj.date_stop:
            del_dom.append(('date', '<=', obj.date_stop))
        if obj.exclude_done:
            del_dom.append(('summary_ids', '=', False))
        dv = self.pool.get('delivery.delivery')
        del_ids = dv.search(cr, uid, del_dom, context=context)
        cids = self.pool.get('delivery.summary').generate(cr, uid, del_ids, context=context)
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'delivery.summary',
            'name': u'Borderò',
            'view_mode': 'tree,form',
            'domain': repr([('id', 'in', cids)]),
        }
