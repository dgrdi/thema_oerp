from openerp.osv import orm, fields

class carrier(orm.Model):

    _inherit = 'delivery.carrier'

    _columns = {
        'encoder_id': fields.many2one('thema.edi.encoder', 'Formato file di esportazione',
                                      domain=[('model', 'in', ['delivery.delivery', 'delivery.summary'])]),
        'mail_template_id': fields.many2one('email.template', 'Modello email'),
    }

