from openerp.osv import orm, fields

class delivery_delivery(orm.Model):
    _inherit = 'delivery.delivery'

    _columns = {
        'cod_amount_text': fields.function(lambda s, *a, **k: s._sda(*a, **k), type='string',
                                      string='Importo contrassegno SDA', multi="sda"),
        'cod_pay_type': fields.function(lambda s, *a, **k: s._sda(*a, **k), type='string',
                                      string='Tipo Pagamento SDA', multi="sda"),
    }

    def _sda(self, cr, uid, ids, field, arg, context=None):
        res = {}
        for delv in self.browse(cr, uid, ids, context=context):
            res.setdefault(delv.id, {})
            if delv.cod and delv.invoice_id.cod_amount>0:
                res[delv.id]['cod_amount_text'] = {0:0>7}.format(delv.cod_amount)
                if delv.cod and delv.invoice_id.cod_amount>250:
                    res[delv.id]['cod_pay_type'] = 'ABM'
                else:
                    res[delv.id]['cod_pay_type'] = 'CON'
            else:
                res[delv.id]['cod_amount_text'] = ''
                res[delv.id]['cod_pay_type'] = ''

        return res