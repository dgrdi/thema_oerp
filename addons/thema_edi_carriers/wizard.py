from openerp.osv import orm, fields
from openerp.tools import flatten


class wizard(orm.TransientModel):
    _name = 'thema.edi.carriers.wizard'
    _columns = {
        'encoder_id': fields.many2one('thema.edi.encoder', domain=[('model', '=', 'delivery.delivery')],
                                      string='Formato'),
        'encoder_summary_id': fields.many2one('thema.edi.encoder',
                                              domain=[('model', 'in', ['delivery.summary', 'delivery.delivery'])],
                                              string='Formato'),
        'active_model': fields.dummy(type='char')
    }

    _defaults = {
        'active_model': lambda s, c, u, ctx=None: (ctx or {}).get('active_model')
    }

    def do_export(self, cr, uid, ids, context=None):
        context = context or {}
        obj = self.browse(cr, uid, ids[0], context=context)
        encoder = obj.encoder_id or obj.encoder_summary_id
        if context.get('active_model') == 'delivery.summary' and encoder.model == 'delivery.delivery':
            summaries = self.pool.get('delivery.summary').browse(cr, uid, context.get('active_ids'))
            record_ids = flatten([[dv.id for dv in el.delivery_ids] for el in summaries])
        else:
            record_ids = context.get('active_ids')
        return self.pool.get('thema.edi.wizard').encode(cr, uid, encoder.code, record_ids, context=context)

