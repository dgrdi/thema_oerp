# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-2014 ZestyBeanz Technologies Pvt Ltd(<http://www.zbeanztech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time

from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp import netsvc
from openerp import SUPERUSER_ID
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP, float_compare


class sale_order(osv.osv):
    _inherit = 'sale.order'
    _columns = {
        'sub_company': fields.boolean('Inter company order', readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},),
        'order_company_id': fields.many2one('res.company', 'Company', readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},),
        
        'sub_sale_id':  fields.many2one('sale.order', 'Sub sale order'),
        
        'state': fields.selection([
            ('draft', 'Draft Quotation'),
            ('sent', 'Quotation Sent'),
            ('cancel', 'Cancelled'),
            ('waiting_date', 'Waiting Schedule'),
            ('progress', 'Sales Order'),
            ('manual', 'Sale to Invoice'),
            ('sub_order', 'Sub Order'),
            ('shipping_except', 'Shipping Exception'),
            ('invoice_except', 'Invoice Exception'),
            ('done', 'Done'),
            ], 'Status', readonly=True,help="Gives the status of the quotation or sales order.\
              \nThe exception status is automatically set when a cancel operation occurs \
              in the invoice validation (Invoice Exception) or in the picking list process (Shipping Exception).\nThe 'Waiting Schedule' status is set when the invoice is confirmed\
               but waiting for the scheduler to run on the order date.", select=True),
        
        }
    
    
   
    
    _defaults = {
        'sub_company': False,
        'order_policy': 'picking',
        }
    
    def onchange_partner_id(self, cr, uid, ids, part, context=None):
        res = super(sale_order, self).onchange_partner_id(cr, uid, ids, part, context=context)
        if part:
            partner_obj = self.pool.get('res.partner').browse(cr, uid, part, context=context)
            res['value'].update({'order_company_id': partner_obj.company_id.id})
        return res
    
    def onchange_company_id(self, cr, uid, ids, company_id, context=None):
        if not company_id:
            return {'value': {}}
        company_obj = self.pool.get('res.company').browse(cr, uid, company_id, context=context)
        val = {'sub_company': False}
        
        if company_obj.sub_company:
            val[ 'sub_company'] =  True
        return {'value': val}
    
    def action_button_confirm(self, cr, uid, ids, context=None):
        
        sale_obj = self.browse(cr, uid, ids[0], context=context)
        if sale_obj.sub_company:
            
             wf_service = netsvc.LocalService('workflow')
             wf_service.trg_validate(uid, 'sale.order', ids[0], 'sub_order', cr)
             return True
        else:
            return super(sale_order, self).action_button_confirm(cr, uid, ids, context=context)
        
    def action_create_so(self, cr, uid, ids, context=None):
        line_pool =  self.pool.get('sale.order.line')
        for sale_obj in self.browse(cr, SUPERUSER_ID, ids, context=context):
            part = sale_obj.order_company_id.partner_id
            addr = self.pool.get('res.partner').address_get(cr, SUPERUSER_ID, [part.id], ['delivery', 'invoice', 'contact'])
            pricelist = part.property_product_pricelist and part.property_product_pricelist.id or False
            payment_term = part.property_payment_term and part.property_payment_term.id or False
            fiscal_position = part.property_account_position and part.property_account_position.id or False
            dedicated_salesman = part.user_id and part.user_id.id or uid
            date_order = time.strftime(DEFAULT_SERVER_DATE_FORMAT)
            val = {
                'partner_id': part.id,
                'partner_invoice_id': addr['invoice'],
                'partner_shipping_id': addr['delivery'],
                'payment_term': payment_term,
                'fiscal_position': fiscal_position,
                'user_id': dedicated_salesman,
                'order_company_id': sale_obj.order_company_id.parent_id.id,
                'pricelist_id': pricelist,
                'sub_sale_id': ids[0],
                'sub_company':  True,
                }
            val = self.copy_data(cr, uid, sale_obj.id, val, context=context)
            val.pop('name')
            val.pop('order_line')

            res_id = self.create(cr, SUPERUSER_ID, val, context=context)
            for line_obj in sale_obj.order_line:
                default = {
                    'company_id': sale_obj.order_company_id.parent_id.id,
                    'order_id': res_id,
                }

                if line_obj.product_id:
                    default.update(line_pool.product_id_change(cr, uid, [line_obj.id],
                                                               pricelist=pricelist,
                                                               product=line_obj.product_id.id,
                                                               qty=line_obj.product_uom_qty or 1.0,
                                                               uom=line_obj.product_uom.id,
                                                               uos=line_obj.product_uos and line_obj.product_uos.id or False,
                                                               name=line_obj.name,
                                                               partner_id=part.id,
                                                               lang=part.lang,
                                                               update_tax=True,
                                                               date_order=date_order,
                                                               packaging=False,
                                                               fiscal_position=fiscal_position,
                                                               flag=False,
                                                               context=context

                    )['value'])
                    default.update({
                        'old_price': line_obj.price_unit
                    })
                line_pool.copy(cr, SUPERUSER_ID, line_obj.id, default, context=context)

            wf_service = netsvc.LocalService('workflow')
            wf_service.trg_validate(SUPERUSER_ID, 'sale.order', res_id, 'order_confirm', cr)
            
        self.write(cr, uid, ids, {'state':  'sub_order'}, context=context)
        return True
                
                
    def _prepare_order_picking(self, cr, uid, order, context=None):
        res = super(sale_order, self)._prepare_order_picking(cr, uid, order, context=context)
        if order.sub_sale_id:
            res.update({
                'partner_id': order.sub_sale_id.partner_shipping_id.id,
                'sub_sale_id': order.sub_sale_id.id,
                'order_from_id': order.sub_sale_id.order_company_id.id,
                'sub_company':  True,
                })
        else:
            res.update({
                'order_from_id': order.company_id.id,
                })
            
        return res
    
sale_order()


class sale_order_line(osv.osv):
    _inherit = 'sale.order.line'
    _columns = {
        'old_price': fields.float('Unit Price'),
        }

sale_order_line()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: