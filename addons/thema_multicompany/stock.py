# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-2014 ZestyBeanz Technologies Pvt Ltd(<http://www.zbeanztech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp import SUPERUSER_ID

class stock_picking(osv.osv):
    _inherit = 'stock.picking'
    _columns = {
        'sub_sale_id':  fields.many2one('sale.order', 'Sub sale order'),
        'order_from_id': fields.many2one('res.company', 'Order From'),
        'sub_company': fields.boolean('Inter company order'),
        
        'invoiced':  fields.boolean('Sub company invoiced'),
        'invoice_id':  fields.many2one('account.invoice', 'Invoice'),
        }
    
    _defaults = {
        'sub_company': False,
        }
    
    def _prepare_invoice(self, cr, uid, picking, partner, inv_type, journal_id, context=None):
        res = super(stock_picking, self)._prepare_invoice(cr, uid, picking, partner, inv_type, journal_id, context=context)
        if context is None:
            context = {}
        if not context.get('multiple', False):
            if picking.sub_sale_id:
                partner_id = picking.partner_id 
                account_id = payment_term = False
                user_ids = self.pool.get('res.users').search(cr, SUPERUSER_ID, [('company_id', '=', picking.sub_sale_id.company_id.id)], context=context)
                if user_ids:
                    if inv_type in ('out_invoice', 'out_refund'):
                        account = self.pool.get('ir.property').get(cr, user_ids[0], 'property_account_receivable', 'res.partner', context=context)
                        account_id = account and account.id or False
                        
                        payment = self.pool.get('ir.property').get(cr, user_ids[0], 'property_payment_term', 'res.partner', context=context)
                        payment_term = payment and  payment.id or False
                    else:
                        account = self.pool.get('ir.property').get(cr, user_ids[0], 'property_account_payable', 'res.partner', context=context)
                        account_id = account and account.id or False
                        
                        payment = self.pool.get('ir.property').get(cr, user_ids[0], 'property_supplier_payment_term', 'res.partner', context=context)
                        payment_term = payment and  payment.id or False
                        
                    res.update({
                        'partner_id': partner_id.id,
                        'company_id': picking.sub_sale_id.order_company_id.id,
                        'account_id': account_id,
                        'payment_term': payment_term,
                        })
        else:
            res.update({
                'partner_id': picking.order_from_id.partner_id.id,
                })
        
        return res
    
    def _prepare_invoice_line(self, cr, uid, group, picking, move_line, invoice_id,
        invoice_vals, context=None):
        res = super(stock_picking, self)._prepare_invoice_line(cr, uid, group, picking, move_line, invoice_id,
        invoice_vals, context=context)
        if context is None:
            context= {}
            
        if not context.get('multiple', False):
            if picking.sub_sale_id:
                user_ids = self.pool.get('res.users').search(cr, SUPERUSER_ID, [('company_id', '=', picking.sub_sale_id.company_id.id)], context=context)
                if user_ids:
                    prop = self.pool.get('ir.property').get(cr, user_ids[0], 'property_account_income_categ', 'product.category', context=context)
                    if prop:
                        res.update({
                        'account_id': prop.id,
                        })
                if move_line.sale_line_id:
                    res.update({
                    'price_unit': move_line.sale_line_id.old_price,
                    })
        return res
    
    
    def action_invoice_create(self, cr, uid, ids, journal_id=False,
            group=False, type='out_invoice', context=None):
        res = super(stock_picking, self).action_invoice_create(cr, uid, ids, journal_id,
            group, type, context=context)
        if context.get('multiple', False):
            self.write(cr, uid, ids , {'invoiced': True}, context=context)
        return res
    
stock_picking()


class stock_picking_out(osv.osv):
    _inherit = 'stock.picking.out'
    _columns = {
        'sub_sale_id':  fields.many2one('sale.order', 'Sub sale order'),
        'order_from_id': fields.many2one('res.company', 'Order From'),
        'sub_company': fields.boolean('Inter company order'),
        'invoiced':  fields.boolean('Sub company invoiced'),
        'invoice_id':  fields.many2one('account.invoice', 'Invoice'),
        }
    
    
stock_picking_out()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: