from openerp.osv import orm, fields

class sale(orm.Model):
    _inherit = 'sale.order'
    _columns = {
        'order_type_id': fields.many2one('sale.order.type', 'Tipo ordine', required=False),
    }

    _defaults = {
        'order_type_id': lambda s, cr, uid, ctx=None:
        s.pool.get('ir.model.data').get_object_reference(cr, uid, 'thema_order_types', 'sale')[-1],
    }
