# -*- encoding: utf-8 -*-

{
    'name': 'Order types',
    'version': '0.1',
    'sequence': 1,
    'category': 'Custom',
    'complexity': "easy",
    'description': """ Tipi ordine """,
    'author': 'Thema Optical SRL',
    'website': 'http://www.thema-optical.com',
    'depends': [
        'thema_order_types',
    ],
    'data': [
    ],
    'demo_xml': [],
    'installable': True,
    'auto_install': True,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: