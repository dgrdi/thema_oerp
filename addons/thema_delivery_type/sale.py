# -*- coding: utf-8 -*-
# #############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-2014 ZestyBeanz Technologies Pvt Ltd(<http://www.zbeanztech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from dateutil.parser import parse

class sale_order(osv.osv):
    _inherit = 'sale.order'
    _columns = {
        'delivery_type_id': fields.many2one('thema.delivery.type', 'Tipo consegna', readonly=True,
                                            states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})
    }

    def _prepare_order_picking(self, cr, uid, order, context=None):
        res = super(sale_order, self)._prepare_order_picking(cr, uid, order, context=context)
        sequence_pool = self.pool.get('ir.sequence')
        context = context or {}
        ctx = context.copy()
        fy_obj = self.pool.get('account.fiscalyear')
        ctx.update({
            'company_id': order.company_id.id,
            'fiscalyear_id': fy_obj.find(cr, uid, dt=order.date_order)
        })
        name = sequence_pool.next_by_id(cr, uid, order.delivery_type_id.internal_id.id, context=ctx)
        res.update({
            'delivery_type_id': order.delivery_type_id.id,
            'name': name,
            'stock_journal_id': order.delivery_type_id.stock_journal_id.id,
        })
        return res

    def _prepare_order_line_move(self, cr, uid, order, line, picking_id, date_planned, context=None):
        res = super(sale_order, self)._prepare_order_line_move(cr, uid, order, line, picking_id, date_planned,
                                                               context=context)
        dt = order.delivery_type_id
        if dt.location_id:
            res['location_id'] = dt.location_id.id
        if dt.location_dest_id:
            res['location_dest_id'] = dt.location_dest_id.id
        return res

sale_order()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
