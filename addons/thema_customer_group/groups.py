from openerp.osv import orm, fields
import time


class membership(orm.Model):
    _name = 'thema.groups.membership'

    _columns = {
        'partner_id': fields.many2one('res.partner', 'Partner', required=True, domain=[('parent_id', '=', False)]),
        'group_id': fields.many2one('res.partner.category', 'Gruppo', required=True, domain=[('group', '=', True)]),
        'date_start': fields.date('Data inizio'),
        'date_stop': fields.date('Data fine'),
    }


class partner(orm.Model):
    _inherit = 'res.partner'

    _columns = {
        'group_membership_ids': fields.one2many('thema.groups.membership', 'partner_id',
                                                string='Appartenenza a gruppi'),
        'group_ids': fields.function(lambda s, *a, **k: s._group_ids(*a, **k),
                                     type='many2many', relation='res.partner.category', string='Gruppi attuali'),

    }

    def _group_ids(self, cr, uid, ids, field, arg, context=None):
        date = (context or {}).get('date', time.strftime('%Y-%m-%d'))
        res = {}
        for p in self.browse(cr, uid, ids, context=context):
            res[p.id] = []
            for memb in p.commercial_partner_id.group_membership_ids:
                if (not memb.date_start or memb.date_start <= date) and (
                            not memb.date_stop or memb.date_stop >= date):
                    res[p.id].append(memb.group_id.id)
        return res


class partner_category(orm.Model):
    _inherit = 'res.partner.category'

    _columns = {
        'group': fields.boolean('Gruppo di acquisto'),
        'group_membership_ids': fields.one2many('thema.groups.membership', 'group_id'),
        'group_partner_ids': fields.function(lambda s, *a, **k: s._group_partner_ids(*a, **k),
                                             type='many2many', relation='res.partner', string='Clienti in gruppo')
    }

    def _group_partner_ids(self, cr, uid, ids, field, arg, context=None):
        date = (context or {}).get('date', time.strftime('%Y-%m-%d'))
        res = {}
        for g in self.browse(cr, uid, ids, context=context):
            res[g.id] = []
            for memb in g.group_membership_ids:
                if (not memb.date_start or memb.date_start <= date) and (
                            not memb.date_stop or memb.date_stop >= date):
                    res[g.id].append(memb.partner_id.id)
        return res


def customer_group_ids(obj, cr, uid, ids, field, arg, context=None):
    res = {}
    for doc in obj.browse(cr, uid, ids, context=context):
        res[doc.id] = []
        if doc.out_of_group:
            continue
        for memb in doc.partner_id.commercial_partner_id.group_membership_ids:
            if (not memb.date_start or memb.date_start <= doc[arg]) and (
                        not memb.date_stop or memb.date_stop >= doc[arg]):
                res[doc.id].append(memb.group_id.id)
    return res


def customer_group_ids_search(obj, cr, uid, obj2, field, domain, context=None):
    assert len(domain) == 1
    f, op, v = domain[0]
    cat = obj.pool.get('res.partner.category')
    if isinstance(v, (str, unicode)):
        cat_ids = cat.name_search(cr, uid, v, operator=op, context=context)
        cat_ids = [el[0] for el in cat_ids]
    else:
        cat_ids = cat.search(cr, uid, [('id', op, v)], context=context)
    cat_ids = cat_ids + cat.search(cr, uid, [('id', 'child_of', cat_ids)], context=context)
    memberships = obj.pool.get('thema.groups.membership').search(cr, uid, [('group_id', 'in', cat_ids)],
                                                                 context=context)
    candidates = obj.search(cr, uid,
                            [('partner_id.commercial_partner_id.group_membership_ids', 'in', memberships)],
                            context=None)
    res = []
    for doc in obj.browse(cr, uid, candidates, context=context):
        if any(el.id in cat_ids for el in doc.customer_group_ids):
            res.append(doc.id)
    return [('id', 'in', res)]


class sale(orm.Model):
    _inherit = 'sale.order'
    _columns = {
        'customer_group_ids': fields.function(lambda s, *a, **k: s._customer_group_ids(*a, **k),
                                              fnct_search=lambda s, *a, **k: s._customer_group_ids_search(*a, **k),
                                              arg='date_order',
                                              type='many2many', relation='res.partner.category',
                                              string='Gruppi'),
    }

    def _customer_group_ids(self, *a, **k):
        return customer_group_ids(self, *a, **k)

    def _customer_group_ids_search(self, *a, **k):
        return customer_group_ids_search(self, *a, **k)


class invoice(orm.Model):
    _inherit = 'account.invoice'

    _columns = {
        'customer_group_ids': fields.function(lambda s, *a, **k: s._customer_group_ids(*a, **k),
                                              fnct_search=lambda s, *a, **k: s._customer_group_ids_search(*a, **k),
                                              arg='date_invoice',
                                              type='many2many', relation='res.partner.category',
                                              string='Gruppi'),
    }

    def _customer_group_ids(self, *a, **k):
        return customer_group_ids(self, *a, **k)

    def _customer_group_ids_search(self, *a, **k):
        return customer_group_ids_search(self, *a, **k)

