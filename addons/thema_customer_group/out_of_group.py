from openerp.osv import orm, fields


class sale(orm.Model):
    _inherit = 'sale.order'

    _columns = {
        'out_of_group': fields.boolean('Fuori gruppo'),
    }

    def _prepare_invoice(self, cr, uid, order, lines, context=None):
        vals = super(sale, self)._prepare_invoice(cr,  uid, order, lines, context=context)
        vals.update({'out_of_group': order.out_of_group})
        return vals


class invoice(orm.Model):
    _inherit = 'account.invoice'

    _columns = {
        'out_of_group': fields.boolean('Fuori gruppo'),
    }

    _defaults = {
        'out_of_group': False,
    }


class stock_picking(orm.Model):
    _inherit = 'stock.picking'

    def _get_invoice_group_hashcode(self, cr, uid, picking, partner, context=None):
        hash = super(stock_picking, self)._get_invoice_group_hashcode(cr, uid, picking, partner, context=context)
        return hash + (picking.sale_id and picking.sale_id.out_of_group or False, )

    def _prepare_invoice(self, cr, uid, picking, partner, inv_type, journal_id, context=None):
        vals = super(stock_picking, self)._prepare_invoice(cr, uid, picking, partner, inv_type, journal_id,
                                                           context=context)
        vals.update({'out_of_group': picking.sale_id and picking.sale_id.out_of_group or False})
        return vals