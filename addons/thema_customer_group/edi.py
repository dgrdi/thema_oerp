from openerp.osv import orm, fields, osv
import string
from functools import partial, wraps
from devutils.tools import call_once
from decimal import Decimal
from logging import getLogger
import types

logger = getLogger(__name__)


def wrap_log(fn):
    @wraps(fn)
    def wrapper(*a, **k):
        try:
            return fn(*a, **k)
        except Exception as e:
            logger.exception('Original exception: %s', e)
            raise

    return wrapper


class account_tax(orm.Model):
    _inherit = 'account.tax'
    _columns = {
        'rf_code': fields.integer('Codice rifatturazione',
                                  help='Codice usato nel tracciato di rifatturazione.'),
    }

    _sql_constraints = [('rf_code_range', "CHECK (rf_code >= 0 AND rf_code <= 99)",
                         "Il codice rifatturazione deve essere di due cifre!")]


class Pool(object):
    class Caller(object):

        def __init__(self, pool, model, context=None):
            self._model = model
            self._pool = pool
            self._context = context

        def __getattr__(self, item):
            fn = getattr(self._pool.pool.get(self._model), item)

            @wraps(fn)
            def wrapper(*a, **k):
                a = (self._pool.cr, self._pool.uid) + a
                k['context'] = self._context or self._pool.context
                try:
                    return fn(*a, **k)
                except TypeError:
                    k.pop('context')
                    return fn(*a, **k)

            return wrapper


    def __init__(self, pool, cr, uid, context):
        self.pool, self.cr, self.uid, self.context = pool, cr, uid, context

    def caller(self, model, context=None):
        return self.Caller(self, model, context=context)

    def get(self, model):
        return self.pool.get(model)


class Wrapper(object):
    def __getitem__(self, item):
        return getattr(self, item)

    def __getattr__(self, item):
        if item.startswith('__') and item.endswith('__'):
            raise AttributeError(item)
        return self._rec[item]


class InvoicePartLine(Wrapper):
    def __init__(self, pool, invoice, line, delivery=None):
        self._pool = pool
        self.invoice_id = invoice
        self._rec = line
        if delivery:
            self.delivery_id = delivery

    @property
    @call_once
    def frame_categories(self):
        imd = self._pool.caller('ir.model.data')
        cat = self._pool.caller('product.category')
        parid = imd.get_object_reference('thema_product', 'product_category_frame')[-1]
        return set(cat.search([('id', 'child_of', [parid])]) + [parid])

    @property
    @call_once
    def accessory_categories(self):
        imd = self._pool.caller('ir.model.data')
        cat = self._pool.caller('product.category')
        parid = imd.get_object_reference('thema_product', 'product_category_accessory')[-1]
        return set(cat.search([('id', 'child_of', [parid])]) + [parid])


    @property
    def rf_sequence(self):
        return self.invoice_id.invoice_line.index(self)

    @property
    def rf_type(self):
        t = self.line_type_id.type
        if t in ('discount', 'gift'):
            return 'O'
        else:
            return 'N'


    @property
    def rf_product_type(self):
        cat = self.product_id.categ_id and self.product_id.categ_id.id or None
        if cat in self.accessory_categories:
            return 'A'  # accessori
        elif cat in self.frame_categories:
            return 'M'  # montature
        else:
            return 'Z'  # altro

    @property
    def rf_tax_code(self):
        tx = self.invoice_line_tax_id
        if not tx:
            raise osv.except_osv('Errore!', 'Non e` definita l`imposta per la riga "%s" della '
                                            'fattura %s. Impossibile esportare.' % (self.name, self.invoice_id.number))
        if len(tx) > 1:
            raise osv.except_osv('Errore!', 'E` stata definita piu` di una imposta per la riga "%s" della fattura %s. '
                                            'Impossibile esportare.' % (self.name, self.invoice_id.number))
        tx = tx[0].rf_code
        if not tx:
            raise osv.except_osv('Errore!', 'La riga "%s" della fattura %s usa la tassa %s, che non '
                                            'ha definito un codice per l`esportazione. Per favore '
                                            'configurare l\'imposta con un codice di esportazione.' % (
                                     self.name, self.invoice_id.number, tx.name))
        return tx

    @property
    def rf_product_code(self):
        c = self.product_id.default_code
        if not c:
            c = 'ID-{0}'.format(self.product_id.id)
        return c


    @property
    def discount_amount(self):
        return self.quantity * self.price_unit * (100 - self.discount)


class InvoicePart(Wrapper):
    def __init__(self, pool, invoice, delivery, lines):
        self._pool = pool
        self._rec = invoice
        self._lines = map(partial(InvoicePartLine, pool, self, delivery=delivery), lines)
        self.invoice_line = [l for l in self._lines if l.line_type_id.type in ('sale', 'discount', 'gift')]
        self.delivery_id = delivery
        self.partner_id = delivery.partner_id


    @property
    def rf_type(self):
        if self._rec.type in ('out_invoice', 'in_invoice'):
            return '0'
        else:
            return '2'

    @property
    def rf_partner_code(self):
        if not self.partner_id.code:
            raise osv.except_osv('Il cliente "%s" non ha un codice impostato (manca il codice cliente).'
                                 ' Impossibile esportare.' % self.partner_id.name)
        cc = self.partner_id.code.split('/')
        clean = lambda s: int(filter(lambda y: y in string.digits, s))
        if len(cc) == 2:
            cc, dest = clean(cc[0]), clean(cc[1])
        elif len(cc) == 1:
            cc, dest = clean(cc[0]), 0
        else:
            assert False, self.partner_id.code
        return '{0:06}{1:02}'.format(cc, dest)

    _amount_types = {
        'sale': 'amount_net',
        'discount': 'amount_net',
        'gift': 'amount_net',
        'collect': 'amount_collect',
        'shipping': 'amount_shipping',
    }

    _default_tax_line = {
        'amount_net': Decimal(0.0),
        'amount_discount': Decimal(0.0),
        'amount_net_net': Decimal(0.0),
        'amount_shipping': Decimal(0.0),
        'amount_packaging': Decimal(0.0),
        'amount_collect': Decimal(0.0),
        'amount_other': Decimal(0.0),
        'amount_notes': Decimal(0.0),
        'vat_base': Decimal(0.0),
        'amount_vat': 0.0,
    }

    _tax_lines = None

    @property
    @wrap_log
    def tax_lines(self):
        if self._tax_lines:
            return self._tax_lines
        taxes = {}
        ctx = (self._pool.context or {}).copy()
        ctx[self._pool.get('account.invoice.line').COMPOSE_FLAG] = True
        il = self._pool.caller('account.invoice.line', context=ctx)
        composed = il.b_read_field([l.id for l in self._lines], 'price_subtotal')
        compute_tax = self._pool.caller('account.tax').compute_all
        for line in self._lines:
            tax_code = line.rf_tax_code
            df = {
                'invoice_id': self,
                'rf_tax_code': tax_code,
            }
            df.update(self._default_tax_line)
            tax = taxes.setdefault(tax_code, df)
            k = self._amount_types.get(line.line_type_id.type, 'amount_other')
            tax[k] += line.price_subtotal
            tot = composed[line.id]
            tax['amount_discount'] += (line.price_subtotal - tot)
            tax['vat_base'] += tot
            if k == 'amount_net':
                tax['amount_net_net'] += tot
            taxb = line.invoice_line_tax_id
            for t in taxb:
                t._convert = False
            tx = compute_tax(taxb, float(tot / line.quantity), float(line.quantity),
                             product=line.product_id and line.product_id.id or False,
                             partner=line.partner_id.id)['total_included'] - float(tot)
            tax['amount_vat'] += tx
        self._tax_lines = taxes.values()
        return self._tax_lines

    @property
    def rf_tot_base(self):
        return sum(x['vat_base'] for x in self.tax_lines) or Decimal(0.0)

    @property
    def rf_tot_vat(self):
        return sum(x['amount_vat'] for x in self.tax_lines) or Decimal(0.0)

    @property
    def rf_tot_invoice(self):
        return float(self.rf_tot_base) + float(self.rf_tot_vat)




def split_invoice(pool, inv):
    deliveries = {}
    last = None
    for line in inv.invoice_line:
        dv = line.delivery_id
        if not dv:
            dv = last
        else:
            last = dv
        if dv:
            deliveries.setdefault(dv.id, (dv, []))[-1].append(line)
    for dv, lines in deliveries.values():
        yield InvoicePart(pool, inv, dv, lines)


class invoice(orm.Model):
    _inherit = 'account.invoice'

    def rf_split_invoices(self, cr, uid, ids, context=None):
        pool = Pool(self.pool, cr, uid, context)
        for inv in self.b_browse(cr, uid, ids, context=context):
            for ip in split_invoice(pool, inv):
                yield ip

    def rf_ds_file_stop(self, cr, uid, records, context=None):
        num = lambda inv: 2 + len(inv.invoice_line) + len(inv.tax_lines)
        net = lambda inv: sum(x['amount_net'] for x in inv.tax_lines)
        return [{
            'rf_num_records': (sum(num(inv) for inv in records) or 0) + 1,
            'rf_num_inv': len(records) or 0,
            'rf_tot_net': sum(net(inv) for inv in records) or 0.0,
            'rf_tot_inv': sum(inv.rf_tot_invoice for inv in records) or 0.0,
            'rf_num_delivery': len(records) or 0,
        }]