# coding=utf-8
from openerp.osv import orm, fields

class location_stock(orm.Model):
    """
    View object on stock partitioned by location and product.
    """
    _name = 'stock.location.stock'

    _auto = False

    _field_qty = lambda s, *a, **k: s._field_qty_compute(*a, **k)

    _columns = {
        'location_id': fields.many2one('stock.location', 'Deposito', readonly=True),
        'product_id': fields.many2one('product.product', 'Deposito', readonly=True),
        'qty_available': fields.function(
            _field_qty,
            type='float',
            multi='qty',
            string=u'Quantità disponibile',
            readonly=True
        ),
        'virtual_available': fields.function(
            _field_qty,
            type='float',
            multi='qty',
            string=u'Quantità virtuale',
            readonly=True
        ),
        'incoming_qty': fields.function(
            _field_qty,
            type='float',
            multi='qty',
            string=u'Quantità virtuale',
            readonly=True
        ),
        'outgoing_qty': fields.function(
            _field_qty,
            type='float',
            multi='qty',
            string=u'Quantità virtuale',
            readonly=True
        ),

    }

    def init(self, cr):
        cr.execute("""
            DROP VIEW IF EXISTS stock_location_stock;
            CREATE OR REPLACE VIEW stock_location_stock AS
            SELECT
                (l.id::int8 << 32) + p.id::int8 id,
                l.id location_id,
                p.id product_id
            FROM
            stock_location l, product_product p
            INNER JOIN product_template t ON t.id = p.product_tmpl_id
            WHERE
                l.usage <> 'view'
                AND t.type = 'product'
        """)

    def _field_qty_compute(self, cr, uid, ids, field_names, arg, context=None):
        context = context or {}
        data = self.read(cr, uid, ids, fields=['product_id', 'location_id'], context=context)
        products = {d['product_id'][0] for d in data}
        locations = {d['location_id'][0] for d in data}
        res = {}
        pob = self.pool.get('product.product')
        for l in locations:
            context['location'] = l
            r = pob.read(cr, uid, products, fields=field_names, context=context)
            for el in r:
                res[(l << 32) + el['id']] = {f: el[f] for f in field_names}
        context.pop('location')
        return res
