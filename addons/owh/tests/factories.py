from testutils.factories.field import Many2Many
from testutils.factories.autoincrement import AutoIncrementStr
from testutils.factories import Factory
from testutils.factories.res import GroupFactory

class DeviceFactory(Factory):
    model = 'owh.device'
    defaults = {
        'name': AutoIncrementStr('device'),
        'group_ids': Many2Many(lambda: DeviceGroupFactory())
    }

class DeviceGroupFactory(Factory):
    model = 'owh.device.group'
    defaults = {
        'name': AutoIncrementStr('device group'),
        'device_ids': Many2Many(DeviceFactory()),
    }

class ProfileFactory(Factory):
    model = 'owh.setting.profile'
    defaults = {
        'name': AutoIncrementStr('profile'),
        'device_group_ids': Many2Many(DeviceGroupFactory()),
        'user_group_ids': Many2Many(GroupFactory()),
    }