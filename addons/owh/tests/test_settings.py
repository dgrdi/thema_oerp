from openerp.tests.common import SingleTransactionCase
from .factories import ProfileFactory, DeviceFactory

class TestSetting(SingleTransactionCase):
    profilef = ProfileFactory()
    devicef = DeviceFactory()

    def test_no_devicegroup(self):
        dev = self.devicef.create()
        p = self.profilef.create()
        res = p.obj.get_profile_ids(self.cr, self.uid, dev)
        self.assertIn(p, res)

    def test_devicegroup(self):
        dev = self.devicef.create({
            'group_ids': [(0, 0, {})]
        })
        group = dev.browse.group_ids[0].id
        p = self.profilef.create({
            'device_group_ids': [(6, 0, [group])]
        })
        res = p.obj.get_profile_ids(self.cr, self.uid, dev)
        self.assertIn(p, res)

    def test_devicegroup_denied(self):
        dev = self.devicef.create({
            'group_ids': [(0, 0, {})]
        })
        group = dev.browse.group_ids[0].id
        p = self.profilef.create({
            'device_group_ids': [(6, 0, [group])]
        })
        res = p.obj.get_profile_ids(self.cr, self.uid, dev+1)
        self.assertNotIn(p, res)
