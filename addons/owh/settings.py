# coding=utf-8
from openerp.osv import orm, fields

class owh_device(orm.Model):
    _name = 'owh.device'
    _columns = {
        'name': fields.char('Name', required=True),
        'group_ids': fields.many2many('owh.device.group', 'owh_device_groups_rel', 'device_id', 'group_id',
                                      'Groups'),
        'last_user_id': fields.many2one('res.users', 'Last user', readonly=True),
    }
    _sql_constraints = [
        ('name_unique', "UNIQUE(name)", 'Device name must be unique!'),
    ]

    def register(self, cr, uid, name, context=None):
        res = self.search(cr, uid, [('name', '=', name)], context=context)
        if not res:
            res = self.create(cr, uid, {'name': name}, context=context)
        else:
            res = res[0]
        self.write(cr, uid, res, {'last_user_id': uid}, context=context)
        return res

class owh_device_group(orm.Model):
    _name = 'owh.device.group'
    _columns = {
        'name': fields.char('Name', required=True),
        'device_ids': fields.many2many('owh.device', 'owh_device_groups_rel', 'group_id', 'device_id',
                                       'Devices'),
    }

class owh_setting_profile(orm.Model):
    _name = 'owh.setting.profile'
    _columns = {
        'name': fields.char('Nome', required=True),
        'priority': fields.integer('Priorita', default=100),
        'device_group_ids': fields.many2many('owh.device.group', 'owh_profile_device_groups_rel', 'profile_id', 'group_id',
                                             'Device groups'),
        'user_group_ids': fields.many2many('res.groups', 'owh_profile_user_groups_rel', 'profile_id', 'group_id',
                                           'User groups'),
    }

    def get_profile_ids(self, cr, uid, device_id, context=None):
        res = self.search(cr, uid, [
            '|', ('device_group_ids.device_ids', '=', device_id), ('device_group_ids', '=', False),
            '|', ('user_group_ids.users', '=', uid), ('user_group_ids', '=', False)
        ], context=context)
        return res

    def get_profiles(self, cr, uid, device_id, context=None):
        return self.read(cr, uid, self.get_profile_ids(cr, uid, device_id), context=context)

class owh_setting(orm.Model):
    _name = 'owh.setting'
    _columns = {
        'profile_id': fields.many2one('owh.setting.profile', 'Profile ID', required=True),
        'key': fields.char('Key', required=True),
        'value': fields.char('Value', help='JSON encoded value', required=True),
    }
    _sql_constraints = [
        ('unique_key_per_profile', "UNIQUE(profile_id, key)", 'Key must be unique per profile!'),
    ]

    def get_settings(self, cr, uid, device_id, context=None):
        profiles = self.pool.get('owh.setting.profile').get_profile_ids(cr, uid, device_id, context=context)
        return self.read(cr, uid, self.search(cr, uid, [('profile_id', 'in', profiles)]), context=context)
