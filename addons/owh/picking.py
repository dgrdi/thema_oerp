# coding=utf-8
from openerp.osv import orm, fields, osv
from openerp.tools import flatten
from openerp import SUPERUSER_ID
from openerp.netsvc import LocalService

class stock_picking(orm.Model):

    _inherit = 'stock.picking'

    def _total_pcs(self, cr, uid, ids, field_name, arg, context=None):
        cr.execute("""
            SELECT
              m.picking_id, SUM(m.product_qty)
            FROM stock_move m
            WHERE m.picking_id IN %s
            GROUP BY m.picking_id
        """, [tuple(ids)])
        res = dict(cr.fetchall())
        res.update({id: 0.0 for id in ids if id not in res})
        return res


    _columns = {
        'owh_user_id': fields.many2one('res.users', 'Operatore assegnato al picking'),
        'user_id': fields.many2one('res.users', 'Utente'),
        'total_pcs': fields.function(_total_pcs, type='float', string='Num. pezzi totali', readonly=True),
    }

    def owh_process_delivery(self, cr, uid, data, context=None):
        to_pick = {}
        for pick_data in data['pickings']:
            to_pick[pick_data['id']] = self.owh_split_moves(cr, uid, pick_data['id'], pick_data)
        to_pick.values()[-1].extend(self.owh_extras(cr, uid, to_pick.keys()[-1], data['context'], context=context))
        to_deliver = self.owh_assign(cr, uid, to_pick, context=context)
        if not to_deliver:
            raise osv.except_osv('Errore!', 'Nessun picking da spedire.')
        delivery_id = data['context'].get('append_to')
        if not delivery_id:
            pid = to_deliver.pop()
            delivery_id = self.action_delivery_create(cr, uid, [pid], context=context)[pid]
        for id in to_deliver:
            self.action_delivery_append(cr, uid, id, delivery_id, context=context)
        self.owh_update_delivery(cr, uid, delivery_id, data['context'], context=context)
        return delivery_id


    def owh_split_moves(self, cr, uid, id, data, context=None):
        """split lines that have been picked to different package / boxes. Return the lines that should be processed
        completely to match the data from the terminal."""
        mv = self.pool.get('stock.move')
        line_data = {el['id']: el for el in data['lines']}
        picking = self.browse(cr, uid, id, context=context)
        if picking.state not in ('confirmed', 'waiting'):
            raise osv.except_osv('Errore!', u'Il picking {0} è già stato evaso o annullato.'.format(picking.id))
        def error(line, msg):
            raise osv.except_osv('Errore!', u'Nella riga {0} del picking {1}: {2}'.format(line.id,
                                                                                         line.picking_id.id, msg))
        to_process = []
        for line in picking.move_lines:
            if line.state not in ('confirmed', 'waiting'):
                error(line, u'La riga è già stata evasa o annullata (stato: {0})'.format(line.state))
            if line.id not in line_data:
                error(line, 'La riga manca nei dati ricevuti da terminale.')
            ldata = line_data.pop(line.id)
            if ldata['product_id'] != line.product_id.id:
                error(line, u'I prodotti non corrispondono: {0} ({1}) a sistema '
                            u'contro {2} a terminale'.format(line.product_id.id, line.product_id.default_code,
                                                                   ldata['product_id']))
            if ldata['product_qty'] != line.product_qty:
                error(line, u'Le quantità non corrispondono: {0} a sistema contro {1} da '
                            u'terminale'.format(line.product_qty, ldata['product_qty']))
            infos = ldata.get('infos')
            if not infos:
                continue
            total_picked = sum(i['picked_qty'] for i in infos)
            if total_picked == 0:
                continue
            if total_picked > line.product_qty:
                error(line, u'La quantità prelevata a terminale ({0}) è superiore alla '
                            u'quantità di sistema ({1})'.format(total_picked, line.product_qty))
            for info in infos:
                box = info['box']
                package = info['package']
                qty = info['picked_qty']
                if qty < 0:
                    error(line, u'La quantità è negativa')
                if box < 0:
                    error(line, u'La scatola è negativa')
                if package < 0:
                    error(line, u'Il collo è negativo')
                if len(infos) == 1 and total_picked == line.product_qty:
                    mv.write(cr, uid, line.id, {'package': package, 'box': box})
                    to_process.append(line.id)
                    continue
                new_id = mv.copy(cr, uid, line.id, {
                    'product_qty': qty,
                    'package': package,
                    'box': box
                })
                to_process.append(new_id)
            if total_picked < line.product_qty:
                mv.write(cr, uid, line.id, {'product_qty': line.product_qty - total_picked})
        if line_data:
            raise osv.except_osv('Errore!', u'Ci sono righe a terminale che non hanno una riga '
                                            u'corrispondente a sistema.')
        return to_process

    def owh_extras(self, cr, uid, id, data, context=None):
        """add extra services like engravings, printings, etc to both the picking and the sale order"""
        md = self.pool.get('ir.model.data')
        line_type_id = md.get_object_reference(cr, uid, 'thema_line_type', 'sale')[1]
        extras = {
            'printings_no': ('thema_product', 'product_marking'),
            'laser_no': ('thema_product', 'product_laser'),
        }
        res = []
        for name, ref in extras.items():
            qty = data.get(name)
            if qty:
                product_id = md.get_object_reference(cr, uid, *ref)[1]
                res.append(self.add_sale_line(cr, SUPERUSER_ID, id, {
                    'line_type_id': line_type_id,
                    'product_id': product_id,
                    'product_uom_qty': qty
                }, context=context))
        return res


    def owh_assign(self, cr, uid, pickings, context=None):
        """Process pickings with do_partial.
            pickings is a dict from picking id to a list if line ids. Every line will be processed entirely."""
        mv = self.pool.get('stock.move')
        self.write(cr, uid, pickings, {
            'owh_user_id': False,
            'user_id': uid
        }, context=context)
        line_ids = flatten(pickings.values())
        line_data = mv.read(cr, uid, line_ids, fields=['product_qty', 'product_uom'])
        partial_data = {
            'move{0}'.format(itm['id']): {
                'product_qty': itm['product_qty'],
                'product_uom': itm['product_uom'] and itm['product_uom'][0] or False
            }
        for itm in line_data}
        rs = self.do_partial(cr, uid, pickings.keys(), partial_data, context=context)
        #self.force_assign(cr, uid, pickings.keys())
        picks = [itm.get('new_picking') or itm['delivered_picking'] for itm in rs.values()]
        picks = [itm for itm in picks if itm]
        return picks

    def owh_update_delivery(self, cr, uid, delivery_id, data, context=None):
        dv = self.pool.get('delivery.delivery')
        pk = self.pool.get('delivery.package')
        dv.write(cr, uid, delivery_id, {
            'owh_user_id': uid,
            'carrier_id': data['carrier'] or False,
            'weight': data['weight'] or 0.0,
            'number_of_packages': data['packages'] or 1,
            'shipping_charge': data['shipping_charge'] or 0.0
        })
        package_data = data.get('package_data')
        delivery = dv.browse(cr, uid, delivery_id, context=context)
        if package_data:
            pkeys = ['height', 'width', 'depth', 'weight']
            for pkg in delivery.package_ids:
                pdata = package_data.get(str(pkg.number))
                if pdata:
                    pdata = {k: pdata.get(k) or False for k in pkeys}
                    pk.write(cr, uid, pkg.id, pdata, context=context)


    def add_sale_line(self, cr, uid, id, vals, context=None):
        sale_line = self.pool.get('sale.order.line')
        sale = self.pool.get('sale.order')
        pick = self.browse(cr, uid, id, context=context)
        if not pick.sale_id:
            raise osv.except_osv('Errore!', 'Impossibile aggiungere riga a picking: nessun ordine collegato')
        vals = sale_line._prepare_line(cr, uid, pick.sale_id, vals, context=context)
        line_id = sale_line.create(cr, uid, vals, context=context)
        line = sale_line.browse(cr, uid, line_id, context=context)
        date_planned = sale._get_date_planned(cr, uid, pick.sale_id, line, pick.sale_id.date_order, context=context)
        pick_vals = sale._prepare_order_line_move(cr, uid, pick.sale_id, line, pick.id, date_planned, context=context)
        return  self.pool.get('stock.move').create(cr, uid, pick_vals, context=context)



class stock_picking_out(orm.Model):
    _inherit = 'stock.picking.out'
    _name = 'stock.picking.out'
    _table = 'stock_picking'

    _columns = {
        'owh_user_id': fields.many2one('res.users', 'Operatore assegnato al picking'),
        'user_id': fields.many2one('res.users', 'Utente')
    }

class stock_picking_in(orm.Model):
    _inherit = 'stock.picking.in'
    _name = 'stock.picking.in'
    _table = 'stock_picking'

    _columns = {
        'owh_user_id': fields.many2one('res.users', 'Operatore assegnato al picking'),
        'user_id': fields.many2one('res.users', 'Utente')
    }


