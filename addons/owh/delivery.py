
from openerp.osv import orm, fields, osv
from openerp.addons import decimal_precision as dp

class delivery_delivery(orm.Model):
    _inherit = 'delivery.delivery'
    _columns = {
        'owh_user_id': fields.many2one('res.users', 'Utente picking'),
        'total_pcs': fields.function(lambda s, *a, **k: s._total_pcs(*a, **k),
                                     type='float', digits_compute=dp.get_precision('Product UoS'),
                                     string='Totale pezzi')
    }

    def _total_pcs(self, cr, uid, ids, field, arg, context=None):
        if not ids:
            return {}
        cr.execute("SELECT dv.id, COALESCE(sum(mv.product_uom), 0) "
                   "FROM delivery_delivery dv "
                   "LEFT JOIN stock_move mv ON  mv.delivery_id = dv.id "
                   "WHERE dv.id IN %s "
                   "GROUP BY dv.id", [tuple(ids)])
        return dict(cr.fetchall())
