{
    'name': 'Warehouse tablet app',
    'version': '0.1',
    'category': '',
    'description': """Warehouse picking: tablet app""",
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': ['base', 'stock', 'cors', 'thema_delivery'],
    'init_xml': [],
    'data':[
        'data/ir.model.access.csv',
        'views/picking.xml',
        ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': True,
}
