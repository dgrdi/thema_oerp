from operator import itemgetter
from openerp.osv import orm, fields
from logging import getLogger
logger = getLogger(__name__)

class geo_info(orm.Model):
    _name = 'geo.info'
    _table = 'geo_info'
    _log_create = False
    _log_access = False

    _columns =  {
        'country_id': fields.many2one('res.country', 'Country', ondelete='cascade'),
        'postal_code': fields.char('Postal code'),
        'place_name': fields.char('Place name'),
        'state': fields.char('State'),
        'state_code': fields.char('State code'),
        'province': fields.char('Province'),
        'province_code': fields.char('Province code'),
        'community': fields.char('Community'),
        'community_code': fields.char('Community code'),
        'latitude': fields.float('Latitude', digits=(16,10)),
        'longitude': fields.float('Longitude', digits=(16,10)),
        'precision': fields.integer('Latitude/Longitude precision'),
    }

    def _load_data(self, cr, uid, context=None):
        cr.execute('DROP INDEX IF EXISTS geo_info_uniq_idx; '
                   'CREATE UNIQUE INDEX geo_info_uniq_idx ON geo_info(country_id, postal_code, place_name);')
        from .load import load_geo_data
        load_geo_data(self.pool, cr)
        st = self.pool.get('geo.country.state')
        st.generate_states(cr, uid, st.search(cr, uid, []), context)

class geo_country_state(orm.Model):
    _name = 'geo.country.state'
    _table = 'geo_country_state'

    _columns = {
        'country_id': fields.many2one('res.country', 'Country', ondelete='cascade'),
        'state_field': fields.selection([('state', 'State'),
                                         ('province', 'Province'),
                                         ('community', 'Community')],
                                        string='State field')
    }

    def generate_states(self, cr, uid, ids, context=None):
        state_obj = self.pool.get('res.country.state')
        to_create = []
        for cs in self.browse(cr, uid, ids, context):
            existing = state_obj.search(cr, uid, [('country_id', '=', cs.country_id.id)])
            existing = state_obj.read(cr, uid, existing, fields=('code',))
            existing = map(itemgetter('code'), existing)
            cr.execute("""
                SELECT DISTINCT
                    {f}_code,
                    {f}
                FROM geo_info
                WHERE country_id = %s
                  AND {f}_code NOT IN %s
                  AND {f}_code IS NOT NULL
                  AND {f} IS NOT NULL
                ;
            """.format(f=cs.state_field),
                       [cs.country_id.id, tuple([''] + existing)])
            for code, name in cr.fetchall():
                to_create.append((cs.country_id.code + '_' + code, cs.country_id.id, code, name))

        if to_create:
            logger.info('Generating %s res.country.state objects', len(to_create))
            res = state_obj.load(cr, uid, fields=('id', 'country_id/.id', 'code', 'name'), data=to_create)
            errors = [r['message'] for r in res['messages']]
            if errors:
                raise RuntimeError(errors)
        return True





