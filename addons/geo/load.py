import gzip
import os

def load_geo_data(pool, cr):
    cr.execute("""
        CREATE TEMP TABLE gd (
            country character varying,
            postal_code character varying,
            place_name character varying,
            state character varying,
            state_code character varying,
            province character varying,
            province_code character varying,
            community character varying,
            community_code character varying,
            latitude character varying,
            longitude character varying,
            precision character varying
        );

    """)
    fn = os.path.join(os.path.dirname(__file__),
                      'data/geo_info.csv.gz')

    with gzip.open(fn) as fo:
        cr.copy_from(fo, 'gd')

    cr.execute("""
        CREATE FUNCTION n(s character varying) RETURNS character varying AS $$
        BEGIN
            RETURN CASE s WHEN '' THEN NULL ELSE s END;
        END;
        $$ language plpgsql;

        CREATE TEMP TABLE gduniq AS
        SELECT
            gd.*,
            row_number() OVER (PARTITION BY gd.country, gd.postal_code, gd.place_name
                               ORDER BY precision DESC) n
        FROM gd
        WHERE gd.country <> '' AND gd.postal_code <> '' AND gd.place_name <> ''
        ;

        DROP TABLE gd;

        UPDATE geo_info gi
        SET
            state = n(gd.state),
            state_code = n(gd.state_code),
            province = n(gd.province),
            province_code = n(gd.province_code),
            community = n(gd.community),
            community_code = n(gd.community_code),
            latitude = cast(n(gd.latitude) AS numeric),
            longitude = cast(n(gd.longitude) AS numeric),
            precision = cast(n(gd.precision) AS int)
        FROM gduniq gd
        INNER JOIN res_country c ON c.code = gd.country
        WHERE
                gi.country_id = c.id
            AND gi.postal_code = gd.postal_code
            AND gi.place_name = gd.place_name
            AND gd.n = 1
        ;

        INSERT INTO geo_info (
            country_id,
            postal_code,
            place_name,
            state,
            state_code,
            province,
            province_code,
            community,
            community_code,
            latitude,
            longitude,
            precision
        )
        SELECT
            c.id,
            gd.postal_code,
            gd.place_name,
            n(gd.state),
            n(gd.state_code),
            n(gd.province),
            n(gd.province_code),
            n(gd.community),
            n(gd.community_code),
            cast(n(gd.latitude) AS numeric),
            cast(n(gd.longitude) AS numeric),
            cast(n(gd.precision) AS int)
        FROM gduniq gd
        INNER JOIN res_country c ON gd.country = c.code
        LEFT JOIN geo_info gi ON
                gi.country_id = c.id
            AND gi.postal_code = gd.postal_code
            AND gi.place_name = gd.place_name
        WHERE
                gi.id IS NULL
            AND gd.n = 1
        ;
        DROP TABLE gduniq;
        DROP FUNCTION n(character varying);
    """)
