{
    'name': 'Geographical information',
    'version': '0.1',
    'category': '',
    'description': """Geographical information on postal codes""",
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': ['base',],
    'init_xml': [],
    'data':[
        'data/ir.model.access.csv',
        'data/country_states.xml',
        'data/init.xml',
        ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': True,
}
