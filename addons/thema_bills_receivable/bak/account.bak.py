# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-2014 ZestyBeanz Technologies Pvt Ltd(<http://www.zbeanztech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
import datetime

from openerp.osv import fields, osv
from tools.translate import _
from openerp import netsvc

class account_journal(osv.osv):
    _inherit = 'account.journal'
    _columns = {
        'bill_receivable': fields.boolean('Bills Receivable'),
        'clearance_delay':  fields.integer('Clearance Delay(Days)'),
        'bill_deposit': fields.boolean('Bills Deposit'),
        'rec_journal_id': fields.many2one('account.journal', 'Automatic Reconcilation Journal'),
        }
    
account_journal()

class account_move(osv.osv):
    _inherit = 'account.move'
    _columns = {
        'bill_receivable': fields.boolean('Bills Receivable'),
        'bill_deposit': fields.boolean('Bills Deposit'),
        'due_date': fields.date('Due Date'),
        'clearance_delay': fields.integer('Clearance Delay(Days)'),
        }
    
account_move()

class account_move_line(osv.osv):
    _inherit = 'account.move.line'
    
    
    def _check_bills_receivable(self, cr, uid, ids, field, arg, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            if line.move_id.bill_receivable:
                res[line.id] = True
            else:
                res[line.id] = False
        return res
    
    _columns = {
        'bill_receivable': fields.function(_check_bills_receivable, type='boolean', string='Bills Receivable', store=True),
        'bill_deposit': fields.boolean('Bills Deposit'),
        'due_date': fields.date('Due Date'),
        'clearance_delay': fields.integer('Clearance Delay(Days)'),
        'invoice_id': fields.many2one('account.invoice', 'Invoice'),
        'invoiced': fields.boolean('Bills Invoiced'),
        }
    
account_move_line()


class account_invoice(osv.osv):
    _inherit = 'account.invoice'
    _columns = {
        'state': fields.selection([
            ('draft','Draft'),
            ('proforma','Pro-forma'),
            ('proforma2','Pro-forma'),
            ('open','Open'),
            ('paid','Paid'),
            ('fully_paid','Fully Paid'),
            ('cancel','Cancelled'),
            ],'Status', select=True, readonly=True, track_visibility='onchange',
            help=' * The \'Draft\' status is used when a user is encoding a new and unconfirmed Invoice. \
            \n* The \'Pro-forma\' when invoice is in Pro-forma status,invoice does not have an invoice number. \
            \n* The \'Open\' status is used when user create invoice,a invoice number is generated.Its in open status till user does not pay invoice. \
            \n* The \'Paid\' status is set automatically when the invoice is paid. Its related journal entries may or may not be reconciled. \
            \n* The \'Cancelled\' status is used when user cancel invoice.'),
        }
    
    def confirm_paid(self, cr, uid, ids, context=None):
        res =  super(account_invoice, self).confirm_paid(cr, uid, ids, context=context)
        via_bill =  False
        count = pay_count = 0
        for invoice_obj in self.browse(cr, uid, ids, context=context):
            pay_count = len(invoice_obj.payment_ids)
            for journal_line in invoice_obj.payment_ids:
                if journal_line.journal_id.bill_receivable:
                    via_bill = True
                    count += 1
        if not via_bill:
            self.write(cr, uid, ids, {'state': 'fully_paid'}, context=context)

        return True
    
account_invoice()


class account_voucher(osv.osv):
    _inherit = 'account.voucher'
    _columns = {
        'journal_id2': fields.many2one('account.journal', 'Bills', readonly=True, states={'draft':[('readonly',False)]}),
        'bill_receivable': fields.boolean('Bills Receivable', readonly=True, states={'draft':[('readonly',False)]}),
        'due_date': fields.date('Due Date', readonly=True, states={'draft':[('readonly',False)]}),
        'clearance_delay': fields.integer('Clearance Delay(Days)', readonly=True, states={'draft':[('readonly',False)]}),
        
        'bill_deposit': fields.boolean('Bills Deposit'),
        'automatic_recon': fields.boolean('Automatic Reconciled'),
        'bill' : fields.boolean('Bill'),
        'cleared': fields.boolean('Cleared'),
        }
    
    def onchange_journal(self, cr, uid, ids, journal_id, line_ids, tax_id, partner_id, date, amount, ttype, company_id, context=None):
        res = super(account_voucher, self).onchange_journal(cr, uid, ids, journal_id, line_ids, tax_id, partner_id, date, amount, ttype, company_id, context=context)
        journal = self.pool.get('account.journal').browse(cr, uid, journal_id, context=context)
        if res:
            res['value'].update({
                'clearance_delay': journal.clearance_delay,
                'bill': journal.bill_receivable,
                })
        return res
    
    
    def _get_invoice(self, cr, uid, move_id, context=None):
        res = {}
        invoice_id = False
        for line in move_id.line_id:
            if line.reconcile_id:
                for recon_line in line.reconcile_id.line_id:
                    if recon_line.invoice:
                        invoice_id = recon_line.invoice.id
                        break
                    
            if invoice_id:
                break
        return invoice_id
    
    
    def recomputes_voucher_lines(self, cr, uid, ids, journal_id2, journal_id, price, currency_id, ttype, date, context=None):
        """
        Returns a dict that contains new values and context

        @param args: other arguments
        @param context: context arguments, like lang, time zone

        @return: Returns a dict which contains new values, and context
        """
        
        def _remove_noise_in_o2m():
            """if the line is partially reconciled, then we must pay attention to display it only once and
                in the good o2m.
                This function returns True if the line is considered as noise and should not be displayed
            """
            if line.reconcile_partial_id:
                if currency_id == line.currency_id.id:
                    if line.amount_residual_currency <= 0:
                        return True
                else:
                    if line.amount_residual <= 0:
                        return True
            return False
        if context is None:
            context = {}
        context_multi_currency = context.copy()

        currency_pool = self.pool.get('res.currency')
        move_line_pool = self.pool.get('account.move.line')
        partner_pool = self.pool.get('res.partner')
        journal_pool = self.pool.get('account.journal')
        line_pool = self.pool.get('account.voucher.line')

        #set default values
        default = {
            'value': {'line_dr_ids': [] ,'line_cr_ids': [] ,'pre_line': False,},
        }

        #drop existing lines
        line_ids = ids and line_pool.search(cr, uid, [('voucher_id', '=', ids[0])]) or False
        if line_ids:
            line_pool.unlink(cr, uid, line_ids)
        if not journal_id2:
            return default
        journal = journal_pool.browse(cr, uid, journal_id2, context=context)
#        partner = partner_pool.browse(cr, uid, partner_id, context=context)
        currency_id = currency_id or journal.company_id.currency_id.id

        total_credit = 0.0
        total_debit = 0.0
        account_type = 'receivable'
        if ttype == 'payment':
            account_type = 'payable'
            total_debit = price or 0.0
        else:
            total_credit = price or 0.0
            account_type = 'receivable'
        if context.get('automatic_recon', False):
            ids = move_line_pool.search(cr, uid, [('due_date','=', time.strftime('%Y-%m-%d')), ('state','=','valid'), ('account_id.type', '=', 'receivable'), ('bill_deposit','=',True) ,('reconcile_id', '=', False)], context=context)
        else:
            if not context.get('move_line_ids', False):
                ids = move_line_pool.search(cr, uid, [('state','=','valid'), ('account_id.type', '=', 'receivable'), ('bill_receivable','=',True) ,('reconcile_id', '=', False)], context=context)
            else:
                ids = context['move_line_ids']
        invoice_id = context.get('invoice_id', False)
        company_currency = journal.company_id.currency_id.id
        move_line_found = False

        #order the lines by most old first
        ids.reverse()
        account_move_lines = move_line_pool.browse(cr, uid, ids, context=context)
#        import pdb
#        pdb.set_trace()
        #compute the total debit/credit and look for a matching open amount or invoice
        for line in account_move_lines:
            if _remove_noise_in_o2m():
                continue

            if invoice_id:
                if line.invoice.id == invoice_id:
                    #if the invoice linked to the voucher line is equal to the invoice_id in context
                    #then we assign the amount on that line, whatever the other voucher lines
                    move_line_found = line.id
                    break
            elif currency_id == company_currency:
                #otherwise treatments is the same but with other field names
                if line.amount_residual == price:
                    #if the amount residual is equal the amount voucher, we assign it to that voucher
                    #line, whatever the other voucher lines
                    move_line_found = line.id
                    break
                #otherwise we will split the voucher amount on each line (by most old first)
                total_credit += line.credit or 0.0
                total_debit += line.debit or 0.0
            elif currency_id == line.currency_id.id:
                if line.amount_residual_currency == price:
                    move_line_found = line.id
                    break
                total_credit += line.credit and line.amount_currency or 0.0
                total_debit += line.debit and line.amount_currency or 0.0
        default['value']['amount'] = total_credit
        #voucher line creation
        for line in account_move_lines:
            invoice_id = False
#            if line.move_id:
#                invoice_id = self._get_invoice(cr, uid, line.move_id, context=context)
            if _remove_noise_in_o2m():
                continue

            if line.currency_id and currency_id == line.currency_id.id:
                amount_original = abs(line.amount_currency)
                amount_unreconciled = abs(line.amount_residual_currency)
            else:
                #always use the amount booked in the company currency as the basis of the conversion into the voucher currency
                amount_original = currency_pool.compute(cr, uid, company_currency, currency_id, line.credit or line.debit or 0.0, context=context_multi_currency)
                amount_unreconciled = currency_pool.compute(cr, uid, company_currency, currency_id, abs(line.amount_residual), context=context_multi_currency)
            line_currency_id = line.currency_id and line.currency_id.id or company_currency
            rs = {
                'name':line.move_id.name,
                'type': line.credit and 'dr' or 'cr',
                'move_line_id':line.id,
                'account_id':line.account_id.id,
                'amount_original': amount_original,
                'amount': (move_line_found == line.id) and min(abs(price), amount_unreconciled) or 0.0,
                'date_original':line.date,
                'date_due':line.date_maturity,
                'amount_unreconciled': amount_unreconciled,
                'currency_id': line_currency_id,
                'invoice_id': invoice_id,
            }
            #in case a corresponding move_line hasn't been found, we now try to assign the voucher amount
            #on existing invoices: we split voucher amount by most old first, but only for lines in the same currency
            if not move_line_found:
                if currency_id == line_currency_id:
                    if line.credit:
                        amount = min(amount_unreconciled, abs(total_debit))
                        rs['amount'] = amount
                        total_debit -= amount
                    else:
                        amount = min(amount_unreconciled, abs(total_credit))
                        rs['amount'] = amount
                        total_credit -= amount

            if rs['amount_unreconciled'] == rs['amount']:
                rs['reconcile'] = True

            if rs['type'] == 'cr':
                rs.update({'reconcile':  True, 'amount': amount_original})
                default['value']['line_cr_ids'].append(rs)
            else:
                default['value']['line_dr_ids'].append(rs)

            if ttype == 'payment' and len(default['value']['line_cr_ids']) > 0:
                default['value']['pre_line'] = 1
            elif ttype == 'receipt' and len(default['value']['line_dr_ids']) > 0:
                default['value']['pre_line'] = 1
            default['value']['writeoff_amount'] = self._compute_writeoff_amount(cr, uid, default['value']['line_dr_ids'], default['value']['line_cr_ids'], price, ttype)
        return default

    def basic_onchange_partner(self, cr, uid, ids, partner_id, journal_id, ttype, context=None):
        partner_pool = self.pool.get('res.partner')
        journal_pool = self.pool.get('account.journal')
        res = {'value': {'account_id': False}}
        if not partner_id or not journal_id:
            return res

        journal = journal_pool.browse(cr, uid, journal_id, context=context)
        partner = partner_pool.browse(cr, uid, partner_id, context=context)
        account_id = False
        
        if journal.type in ('sale','sale_refund'):
            account_id = partner.property_account_receivable.id
        elif journal.type in ('purchase', 'purchase_refund','expense'):
            account_id = partner.property_account_payable.id
        else:
            account_id = journal.default_credit_account_id.id or journal.default_debit_account_id.id

        res['value']['account_id'] = account_id
        return res

    def journal_onchange(self, cr, uid, ids, journal_id, context=None):
        
        journal_pool = self.pool.get('account.journal')
        res = {'value': {'account_id': False}}
        if not journal_id:
            return res
        journal = journal_pool.browse(cr, uid, journal_id, context=context)
        res['value']['account_id'] = journal.default_credit_account_id.id or journal.default_debit_account_id.id
        return res
    
    def onchange_amounts(self, cr, uid, ids, amount, rate, journal_id2, journal_id, currency_id, ttype, date, payment_rate_currency_id, company_id, context=None):
        if context is None:
            context = {}
        ctx = context.copy()
        ctx.update({'date': date})
        #read the voucher rate with the right date in the context
        currency_id = currency_id or self.pool.get('res.company').browse(cr, uid, company_id, context=ctx).currency_id.id
        voucher_rate = self.pool.get('res.currency').read(cr, uid, currency_id, ['rate'], context=ctx)['rate']
        ctx.update({
            'voucher_special_currency': payment_rate_currency_id,
            'voucher_special_currency_rate': rate * voucher_rate})
        res = self.recomputes_voucher_lines(cr, uid, ids, journal_id2, journal_id, amount, currency_id, ttype, date, context=ctx)
        vals = self.onchange_rate(cr, uid, ids, rate, amount, currency_id, payment_rate_currency_id, company_id, context=ctx)
        for key in vals.keys():
            res[key].update(vals[key])
        return res

    
    def onchange_journal_id2(self, cr, uid, ids, journal_id2, journal_id, amount, currency_id, ttype, date, context=None):
        if not journal_id2:
            return {}
        if context is None:
            context = {}
        #TODO: comment me and use me directly in the sales/purchases views
        res = self.basic_onchange_partner(cr, uid, ids, 1, journal_id, ttype, context=context)
        if ttype in ['sale', 'purchase']:
            return res
        ctx = context.copy()
        # not passing the payment_rate currency and the payment_rate in the context but it's ok because they are reset in recompute_payment_rate
        ctx.update({'date': date})
        vals = self.recomputes_voucher_lines(cr, uid, ids, journal_id2, journal_id, amount, currency_id, ttype, date, context=ctx)
        vals2 = self.recompute_payment_rate(cr, uid, ids, vals, currency_id, date, ttype, journal_id2, amount, context=context)
        for key in vals.keys():
            res[key].update(vals[key])
        for key in vals2.keys():
            res[key].update(vals2[key])
        #TODO: can probably be removed now
        #TODO: onchange_partner_id() should not returns [pre_line, line_dr_ids, payment_rate...] for type sale, and not 
        # [pre_line, line_cr_ids, payment_rate...] for type purchase.
        # We should definitively split account.voucher object in two and make distinct on_change functions. In the 
        # meanwhile, bellow lines must be there because the fields aren't present in the view, what crashes if the 
        # onchange returns a value for them
        if ttype == 'sale':
            del(res['value']['line_dr_ids'])
            del(res['value']['pre_line'])
            del(res['value']['payment_rate'])
        elif ttype == 'purchase':
            del(res['value']['line_cr_ids'])
            del(res['value']['pre_line'])
            del(res['value']['payment_rate'])
            
        res['value']['amount'] = sum(item['amount_original'] for item in res['value']['line_cr_ids'])
        return res
    
   
    
   
    def account_move_get(self, cr, uid, voucher_id, context=None):
        res = super(account_voucher, self).account_move_get(cr, uid, voucher_id, context=context)
        voucher_record = self.browse(cr, uid, voucher_id, context=context)
        if voucher_record and voucher_record.bill_receivable or voucher_record.journal_id.bill_receivable:
            res.update({'bill_receivable':True})
        
        if voucher_record and voucher_record.bill_deposit or voucher_record.journal_id.bill_deposit:
            res.update({'bill_deposit':True})
        
        res.update({'date_maturity': '2014-05-17', 'due_date': voucher_record.due_date, 'clearance_delay': voucher_record.clearance_delay})
        return  res
    
    
    def writeoff_move_line_get(self, cr, uid, voucher_id, line_total, move_id, name, company_currency, current_currency, context=None):
        '''
        Set a dict to be use to create the writeoff move line.

        :param voucher_id: Id of voucher what we are creating account_move.
        :param line_total: Amount remaining to be allocated on lines.
        :param move_id: Id of account move where this line will be added.
        :param name: Description of account move line.
        :param company_currency: id of currency of the company to which the voucher belong
        :param current_currency: id of currency of the voucher
        :return: mapping between fieldname and value of account move line to create
        :rtype: dict
        '''
        currency_obj = self.pool.get('res.currency')
        move_line = {}

        voucher = self.pool.get('account.voucher').browse(cr,uid,voucher_id,context)
        current_currency_obj = voucher.currency_id or voucher.journal_id.company_id.currency_id

        if not currency_obj.is_zero(cr, uid, current_currency_obj, line_total):
            diff = line_total
            account_id = False
            write_off_name = ''
            if voucher.bill_receivable or voucher.bill_deposit or voucher.automatic_recon:
                account_id = voucher.journal_id2.default_credit_account_id.id
            else:
                if voucher.payment_option == 'with_writeoff':
                    account_id = voucher.writeoff_acc_id.id
                    write_off_name = voucher.comment
                elif voucher.type in ('sale', 'receipt'):
                    account_id = voucher.partner_id.property_account_receivable.id
                else:
                    account_id = voucher.partner_id.property_account_payable.id
            sign = voucher.type == 'payment' and -1 or 1
            move_line = {
                'name': write_off_name or name,
                'account_id': account_id,
                'move_id': move_id,
                'partner_id': voucher.partner_id.id,
                'date': voucher.date,
                'credit': diff > 0 and diff or 0.0,
                'debit': diff < 0 and -diff or 0.0,
                'amount_currency': company_currency <> current_currency and (sign * -1 * voucher.writeoff_amount) or False,
                'currency_id': company_currency <> current_currency and current_currency or False,
                'analytic_account_id': voucher.analytic_id and voucher.analytic_id.id or False,
            }
        return move_line
    
    
    def proforma_voucher(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        for voucher in self.browse(cr, uid, ids, context=context):
            if voucher.bill_deposit or voucher.automatic_recon:
                self.create_account_move(cr, uid, ids, context=context)
                self.write(cr, uid, ids, {'state': 'posted'}, context=context)
            else:
                res = super(account_voucher, self).proforma_voucher(cr, uid, ids, context=context)
        return True
    
    
    def create_account_move(self, cr, uid, ids, context=None):
        move_pool  = self.pool.get('account.move')
        move_line_pool =  self.pool.get('account.move.line')
        seq_obj = self.pool.get('ir.sequence')
        move_vals = {}
        line_vals = {}
        recon_line_ids = []
        move_id = False
        for voucher in self.browse(cr, uid, ids, context=context):
            company_currency = self._get_company_currency(cr, uid, voucher.id, context)
            c = dict(context)
            c.update({'fiscalyear_id': voucher.period_id.fiscalyear_id.id})
            name = seq_obj.next_by_id(cr, uid, voucher.journal_id.sequence_id.id, context=c)
            ref = ''
            if not voucher.reference:
                ref = name.replace('/','')
            else:
                ref = voucher.reference
            
            move_vals = {
                'name': name,
                'journal_id': voucher.journal_id.id,
                'narration': voucher.narration,
                'date': voucher.date,
                'ref': ref,
                'period_id': voucher.period_id.id,
                }
            move_id = move_pool.create(cr, uid, move_vals, context=context)
            account_id = False
            if voucher.journal_id.type in ('sale','sale_refund'):
                account_id = partner.property_account_receivable.id
            elif voucher.journal_id.type in ('purchase', 'purchase_refund','expense'):
                account_id = partner.property_account_payable.id
            else:
                account_id = voucher.journal_id.default_credit_account_id.id or voucher.journal_id.default_debit_account_id.id
            for line_obj in voucher.line_cr_ids:
                if line_obj.reconcile:
                    line_vals = {
                        'journal_id': voucher.journal_id.id,
                        'period_id': voucher.period_id.id,
                        'name': line_obj.name or '/',
                        'account_id': line_obj.account_id.id,
                        'move_id': move_id,
                        'currency_id': line_obj.move_line_id and (company_currency <> line_obj.move_line_id.currency_id.id and line_obj.move_line_id.currency_id.id) or False,
                        'analytic_account_id': line_obj.account_analytic_id and line_obj.account_analytic_id.id or False,
                        'quantity': 1,
                        'credit': line_obj.amount,
                        'debit': 0.0,
                        'date': voucher.date,
                        'bill_deposit': voucher.bill_deposit,
                        'automatic_recon': voucher.automatic_recon,
                        'invoice_id': line_obj.invoice_id and line_obj.invoice_id.id,
                        'date_maturity': voucher.due_date,
                        
                        }
                    line_id = move_line_pool.create(cr, uid, line_vals, context=context)
                    line_vals.update({
                          'account_id': account_id, 
                          'credit': 0.0, 
                          'debit': line_obj.amount, 
                          'due_date': line_obj.move_line_id.move_id.due_date, 
                          'clearance_delay': line_obj.move_line_id.move_id.clearance_delay,
                          'name': '/',
                          })
                    recon_line_ids.append([line_id, line_obj.move_line_id.id])
                    line_id = move_line_pool.create(cr, uid, line_vals, context=context)
            self.write(cr, uid, ids, {'move_id': move_id, 'number': name}, context=context)
        if move_id:
            if recon_line_ids:
                for line_ids in recon_line_ids:
                    move_line_pool.reconcile_partial(cr, uid, line_ids, writeoff_acc_id=voucher.writeoff_acc_id.id, writeoff_period_id=voucher.period_id.id, writeoff_journal_id=voucher.journal_id.id)
        
        return True 
    
    def get_lines(self, cr, uid, voucher_lines, line, total):
        for l in voucher_lines:
            l.update({'reconcile': True, 'amount': l['amount_original']})
            line.append([0, False, l])
            total += l['amount_original']
        del voucher_lines
        return total
    
#    def cron_automatic_reconcilation(self, cr, uid,  ids=False, context=None):
#        
#        if context is None:
#            context = {}
#        move_pool = self.pool.get('account.move')
#        move_line_pool = self.pool.get('account.move.line')
#        journal_pool = self.pool.get('account.journal')
#        user_pool = self.pool.get('res.users')
#        today_date = time.strftime('%Y-%m-%d')
#        wf_service = netsvc.LocalService("workflow")
#        ids = move_line_pool.search(cr, uid, [('due_date','=', today_date), ('state','=','valid'), ('account_id.type', '=', 'receivable'), ('bill_deposit','=',True) ,('reconcile_id', '=', False)], context=context)
#        voucher_vals = {}
#        if ids:
#            context.update({'automatic_recon': True})
#            company_obj = user_pool.browse(cr, uid, uid, context=context).company_id
#            journal_id2 = company_obj.bill_journal_id.id
#            journal_id = company_obj.bill_journal_id.rec_journal_id.id
#            currency_id = company_obj.currency_id.id
#            account_id = company_obj.bill_journal_id.rec_journal_id.default_credit_account_id.id or company_obj.bill_journal_id.rec_journal_id.default_debit_account_id.id
#            res = {'value': {}}
#        
#            ctx = {'automatic_recon': True}
#            # not passing the payment_rate currency and the payment_rate in the context but it's ok because they are reset in recompute_payment_rate
#            vals = self.recomputes_voucher_lines(cr, uid, [], journal_id2, journal_id, 0.0, currency_id, 'reciept', today_date, context=ctx)
#            vals2 = self.recompute_payment_rate(cr, uid, ids, vals, currency_id, today_date, 'reciept', journal_id, 0.0, context=ctx)
#            for key in vals.keys():
#                res[key].update(vals[key])
#            for key in vals2.keys():
#                res[key].update(vals2[key])
#            voucher_vals = {
#                'journal_id2': journal_id2, 
#                'journal_id': journal_id,
#                'currency_id': currency_id,
#                'type': 'receipt',
#                'date': today_date,
#                'account_id': account_id,
#                'automatic_recon': True,
#                'bill_deposit': False,
#                }
#            line_cr = []
#            line_dr = []
#            total_cr = 0.0
#            total_dr = 0.0
#            if res['value'].get('line_cr_ids', False):
#                total_cr = self.get_lines(cr, uid, res['value']['line_cr_ids'], line_cr, total_cr)
#            if vals['value'].get('line_dr_ids', False):
#                total_dr = self.get_lines(cr, uid, res['value']['line_dr_ids'], line_dr, total_dr)
#            
#            voucher_vals.update(res['value'])
#            voucher_vals.update({'line_dr_ids': line_dr, 'line_cr_ids': line_cr, 'amount': total_cr-total_dr})
#            voucher_id = self.create(cr, uid, voucher_vals, context=context)
#            wf_service.trg_validate(uid, 'account.voucher', voucher_id, 'proforma_voucher', cr)
#            print 'AUTOMATIC RECONCILATION'
#        return True
#        
#    
    def cron_invoice_payment(self, cr, uid, ids=False, context=None):
        invoice_pool = self.pool.get('account.invoice')
        move_line_pool = self.pool.get('account.move.line')
        move_pool = self.pool.get('account.move')
        cr.execute('select id from account_voucher where state=%s and bill=True and cleared=False and bill_receivable=False and bill_deposit=False', ('posted',))
        bill_ids = [x[0] for x in cr.fetchall()]
        for voucher_obj in self.browse(cr, uid, bill_ids, context=context):
            self.write(cr, uid, [voucher_obj.id], {'cleared': True}, context=context)
            today_date = time.strftime('%Y-%m-%d')
            date = datetime.datetime.strptime(voucher_obj.due_date, '%Y-%m-%d') + datetime.timedelta(days=voucher_obj.clearance_delay)
            enddate = date.strftime("%Y-%m-%d")
            print date, enddate
            if today_date == enddate:
                for line_cr in voucher_obj.line_cr_ids:
                    if line_cr.move_line_id.invoice and line_cr.move_line_id.invoice.state == 'paid':
                        cr.execute('select sum(amount) from account_voucher_line where voucher_id in (select id from  account_voucher where state=%s and bill=True and cleared=True and bill_receivable=False and bill_deposit=False) and move_line_id=%s', ('posted', line_cr.move_line_id.id))
                        amount = [x[0] for x in cr.fetchall()]
                        if amount[0] == line_cr.move_line_id.invoice.amount_total:
                            invoice_pool.write(cr, uid, [line_cr.move_line_id.invoice.id], {'state': 'fully_paid'}, context=context)
                            print 'INVOICE FULLY PAID'
        return True
            
            
            
    def recompute_voucher_lines(self, cr, uid, ids, partner_id, journal_id, price, currency_id, ttype, date, context=None):
        """
        Returns a dict that contains new values and context

        @param partner_id: latest value from user input for field partner_id
        @param args: other arguments
        @param context: context arguments, like lang, time zone

        @return: Returns a dict which contains new values, and context
        """
        
        
        """
        #    DON'T MODIFIE THIS FUNCTION WITHOUT SUPPORT
        """
        
        def _remove_noise_in_o2m():
            """if the line is partially reconciled, then we must pay attention to display it only once and
                in the good o2m.
                This function returns True if the line is considered as noise and should not be displayed
            """
            if line.reconcile_partial_id:
                if currency_id == line.currency_id.id:
                    if line.amount_residual_currency <= 0:
                        return True
                else:
                    if line.amount_residual <= 0:
                        return True
            return False

        if context is None:
            context = {}
        context_multi_currency = context.copy()

        currency_pool = self.pool.get('res.currency')
        move_line_pool = self.pool.get('account.move.line')
        partner_pool = self.pool.get('res.partner')
        journal_pool = self.pool.get('account.journal')
        line_pool = self.pool.get('account.voucher.line')

        #set default values
        default = {
            'value': {'line_dr_ids': [] ,'line_cr_ids': [] ,'pre_line': False,},
        }

        #drop existing lines
        line_ids = ids and line_pool.search(cr, uid, [('voucher_id', '=', ids[0])]) or False
        if line_ids:
            line_pool.unlink(cr, uid, line_ids)

        if not partner_id or not journal_id:
            return default

        journal = journal_pool.browse(cr, uid, journal_id, context=context)
        partner = partner_pool.browse(cr, uid, partner_id, context=context)
        currency_id = currency_id or journal.company_id.currency_id.id

        total_credit = 0.0
        total_debit = 0.0
        account_type = 'receivable'
        if ttype == 'payment':
            account_type = 'payable'
            total_debit = price or 0.0
        else:
            total_credit = price or 0.0
            account_type = 'receivable'

        if not context.get('move_line_ids', False):
            ids = move_line_pool.search(cr, uid, [('state','=','valid'),('journal_id.bill_receivable', '=', False), ('account_id.type', '=', account_type), ('reconcile_id', '=', False), ('partner_id', '=', partner_id)], context=context)
        else:
            ids = context['move_line_ids']
        invoice_id = context.get('invoice_id', False)
        company_currency = journal.company_id.currency_id.id
        move_line_found = False

        #order the lines by most old first
        ids.reverse()
        account_move_lines = move_line_pool.browse(cr, uid, ids, context=context)

        #compute the total debit/credit and look for a matching open amount or invoice
        for line in account_move_lines:
            if _remove_noise_in_o2m():
                continue

            if invoice_id:
                if line.invoice.id == invoice_id:
                    #if the invoice linked to the voucher line is equal to the invoice_id in context
                    #then we assign the amount on that line, whatever the other voucher lines
                    move_line_found = line.id
                    break
            elif currency_id == company_currency:
                #otherwise treatments is the same but with other field names
                if line.amount_residual == price:
                    #if the amount residual is equal the amount voucher, we assign it to that voucher
                    #line, whatever the other voucher lines
                    move_line_found = line.id
                    break
                #otherwise we will split the voucher amount on each line (by most old first)
                total_credit += line.credit or 0.0
                total_debit += line.debit or 0.0
            elif currency_id == line.currency_id.id:
                if line.amount_residual_currency == price:
                    move_line_found = line.id
                    break
                total_credit += line.credit and line.amount_currency or 0.0
                total_debit += line.debit and line.amount_currency or 0.0

        #voucher line creation
        for line in account_move_lines:

            if _remove_noise_in_o2m():
                continue

            if line.currency_id and currency_id == line.currency_id.id:
                amount_original = abs(line.amount_currency)
                amount_unreconciled = abs(line.amount_residual_currency)
            else:
                #always use the amount booked in the company currency as the basis of the conversion into the voucher currency
                amount_original = currency_pool.compute(cr, uid, company_currency, currency_id, line.credit or line.debit or 0.0, context=context_multi_currency)
                amount_unreconciled = currency_pool.compute(cr, uid, company_currency, currency_id, abs(line.amount_residual), context=context_multi_currency)
            line_currency_id = line.currency_id and line.currency_id.id or company_currency
            rs = {
                'name':line.move_id.name,
                'type': line.credit and 'dr' or 'cr',
                'move_line_id':line.id,
                'account_id':line.account_id.id,
                'amount_original': amount_original,
                'amount': (move_line_found == line.id) and min(abs(price), amount_unreconciled) or 0.0,
                'date_original':line.date,
                'date_due':line.date_maturity,
                'amount_unreconciled': amount_unreconciled,
                'currency_id': line_currency_id,
            }
            #in case a corresponding move_line hasn't been found, we now try to assign the voucher amount
            #on existing invoices: we split voucher amount by most old first, but only for lines in the same currency
            if not move_line_found:
                if currency_id == line_currency_id:
                    if line.credit:
                        amount = min(amount_unreconciled, abs(total_debit))
                        rs['amount'] = amount
                        total_debit -= amount
                    else:
                        amount = min(amount_unreconciled, abs(total_credit))
                        rs['amount'] = amount
                        total_credit -= amount

            if rs['amount_unreconciled'] == rs['amount']:
                rs['reconcile'] = True

            if rs['type'] == 'cr':
                default['value']['line_cr_ids'].append(rs)
            else:
                default['value']['line_dr_ids'].append(rs)

            if ttype == 'payment' and len(default['value']['line_cr_ids']) > 0:
                default['value']['pre_line'] = 1
            elif ttype == 'receipt' and len(default['value']['line_dr_ids']) > 0:
                default['value']['pre_line'] = 1
            default['value']['writeoff_amount'] = self._compute_writeoff_amount(cr, uid, default['value']['line_dr_ids'], default['value']['line_cr_ids'], price, ttype)
        return default
account_voucher()


class account_voucher_line(osv.osv):
    _inherit = 'account.voucher.line'
    _columns = {
        'invoice_id': fields.many2one('account.invoice', 'Invoice'),
        }
    
account_voucher_line()


class account_move_reconcile(osv.osv):
    _inherit = "account.move.reconcile"
    _description = "Account Reconciliation"

    def _check_same_partner(self, cr, uid, ids, context=None):
        for reconcile in self.browse(cr, uid, ids, context=context):
            move_lines = []
            if not reconcile.opening_reconciliation:
                if reconcile.line_id:
                    first_partner = reconcile.line_id[0].partner_id.id
                    move_lines = reconcile.line_id
                elif reconcile.line_partial_ids:
                    first_partner = reconcile.line_partial_ids[0].partner_id.id
                    move_lines = reconcile.line_partial_ids
                if any([(line.journal_id.bill_receivable == True) for line in move_lines]):
                    continue
                if any([(line.account_id.type in ('receivable', 'payable') and line.partner_id.id != first_partner) for line in move_lines]):
                    return False
        return True

    _constraints = [
        (_check_same_partner, 'You can only reconcile journal items with the same partner.', ['line_id']),
    ]
    

account_move_reconcile()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: