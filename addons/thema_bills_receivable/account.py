
from openerp.osv import fields, osv

class account_journal(osv.osv):
    _inherit = 'account.journal'
    _columns = {
        'bill_receivable': fields.boolean('Sezionale effetti'),
        'clearance_delay':  fields.integer('Giorni esposizione'),
        'clearance_type': fields.selection([
            ('due', 'Da data scadenza'),
            ('deposit', 'Da data versamento'),
         ], string='Decorrenza esposizione'),
        'bill_deposit': fields.boolean('Sezionale versamento effetti'),
        'rec_journal_id': fields.many2one('account.journal', 'Sezionale incasso effetti'),
        'bill_cancel_journal_id': fields.many2one('account.journal', 'Sezionale richiami'),
        'bill_unpaid_journal_id': fields.many2one('account.journal', 'Sezionale insoluti'),
    }
"""
class account_invoice(osv.osv):
    _inherit = 'account.invoice'
    _columns = {
        'state': fields.selection([
            ('draft','Draft'),
            ('proforma','Pro-forma'),
            ('proforma2','Pro-forma'),
            ('open','Open'),
            ('paid','Paid'),
            ('fully_paid','Fully Paid'),
            ('cancel','Cancelled'),
            ],'Status', select=True, readonly=True, track_visibility='onchange',
            help=' * The \'Draft\' status is used when a user is encoding a new and unconfirmed Invoice. \
            \n* The \'Pro-forma\' when invoice is in Pro-forma status,invoice does not have an invoice number. \
            \n* The \'Open\' status is used when user create invoice,a invoice number is generated.Its in open status till user does not pay invoice. \
            \n* The \'Paid\' status is set automatically when the invoice is paid. Its related journal entries may or may not be reconciled. \
            \n* The \'Cancelled\' status is used when user cancel invoice.'),
        }

    def confirm_paid(self, cr, uid, ids, context=None):
        res =  super(account_invoice, self).confirm_paid(cr, uid, ids, context=context)
        via_bill =  False
        count = pay_count = 0
        for invoice_obj in self.browse(cr, uid, ids, context=context):
            pay_count = len(invoice_obj.payment_ids)
            for journal_line in invoice_obj.payment_ids:
                if journal_line.journal_id.bill_receivable:
                    via_bill = True
                    count += 1
        if not via_bill:
            self.write(cr, uid, ids, {'state': 'fully_paid'}, context=context)

        return True
"""