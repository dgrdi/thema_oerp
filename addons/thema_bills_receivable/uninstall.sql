DROP TRIGGER IF EXISTS trg_bill_state_delete ON account_move;
DROP TRIGGER IF EXISTS trg_bill_state_insert ON account_move;
DROP TRIGGER IF EXISTS trg_bill_state_update ON account_move;
DROP TRIGGER IF EXISTS trg_bill_update_state ON account_move;
