from openerp.osv import orm, fields
from osv.osv import except_osv


class payment_term(orm.Model):
    _inherit = 'account.payment.term'


    _columns = {
        'bills_rec_journal_code': fields.selection(lambda s, *a, **k: s._get_journal_codes(*a, **k),
                                                   string='Effetti da generare'),
        'bills_rec_journal_id': fields.function(lambda s, *a, **k: s._bills_rec_journal_id(*a, **k),
                                                string='Sezionale effetti da generare')
    }

    def _get_journal_codes(self, cr, uid, context=None):
        companies = self.pool.get('res.users').read(cr, uid, uid, fields=['company_ids'], context=context)['company_ids']
        j = self.pool.get('account.journal')
        journals = j.search(cr, uid, [('company_id', 'in', companies), ('bill_receivable', '=', True)], context=context)
        journals = j.read(cr, uid, journals, fields=['code', 'name'], context=context)
        res = {}
        for j in journals:
            if j['code'] not in res:
                res[j['code']] = j['name']
        return res.items()

    def _bills_rec_journal_id(self, cr, uid, ids, field, arg, context=None):
        if not context or 'company_id' not in context:
            return dict.fromkeys(ids, False)
        company_id = context['company_id']
        res = {}
        j = self.pool.get('account.journal')
        for pay in self.browse(cr, uid, ids):
            jid = j.search([('code', '=', pay.bills_rec_journal_code), ('company_id', '=', company_id)], context=context)
            if not jid:
                raise except_osv('Errore!', u'Non esiste il sezionale "%s" per l\'azienda %s. Impossibile '
                                            u'generare effetti come previsto '
                                            u'dal termine di pagamento.' % pay.bills_rec_journal_code)
            res[pay.id] = jid[0]
        return res

class invoice(orm.Model):
    _inherit = 'account.invoice'

    def generate_bills(self, cr, uid, ids, context=None):
        context = context or {}
        ids = self.search(cr, uid, [
            ('id', 'in', ids),
            ('move_id', '!=', False),
            ('payment_term.bills_rec_journal_code', '!=', False)
        ])
        move = self.pool.get('account.move')
        line = self.pool.get('account.move.line')
        for inv in self.browse(cr, uid, ids, context=context):
            line_ids = line.search(cr, uid, [
                ('move_id', '=', inv.move_id.id),
                ('account_id', '=', inv.account_id.id),
            ])
            oldcid = context.pop('company_id', None)
            context['company_id'] = inv.company_id.id
            for l in line.browse(cr, uid, line_ids, context=context):
                self.generate_bill_payment_move(cr, uid, inv, l)
            if oldcid:
                context['company_id'] = oldcid
            else:
                context.pop('company_id')
        return True

    def bill_payment_move_get(self, cr, uid, inv, line):
        bj = inv.payment_term.bills_rec_journal_id
        move = {
            'journal_id': bj.id,
            'period_id': inv.move_id.period_id.id,
            'date': inv.move_id.date,
            'bill_receivable': True,
            'clearance_delay': bj.clearance_delay,
            'clearance_type': bj.clearance_type,
            'line_id': [
                (0, 0, {
                    'name': u'Emissione effetti',
                    'debit': line.credit,
                    'credit': line.debit,
                    'partner_id': line.partner_id.id,
                    'account_id': line.account_id.id,
                    'date_maturity': line.date_maturity,
                }), (0, 0, {
                    'name': u'Emissione effetti',
                    'debit': line.debit,
                    'credit': line.credit,
                    'partner_id': line.partner_id.id,
                    'account_id': bj.default_debit_account_id.id,
                    'date_maturity': line.date_maturity,
                    'bill_receivable': True,
                })
            ]
        }

    def generate_bill_payment_move(self, cr, uid, inv, line):
        m = self.pool.get('account.move')
        ml = self.pool.get('account.move.line')
        move = self.bill_payment_move_get(cr, uid, inv, line)
        mid = m.create(cr, uid, move)
        for l in m.browse(cr, uid, mid, context=None).line_id:
            if l.account_id.id == inv.account_id.id:
                ml.reconcile_partial(cr, uid, [line.id, l.id])
        return mid


    def action_move_create(self, cr, uid, ids, context=None):
        res = super(invoice, self).action_move_create(cr, uid, ids, context=context)
        ids = self.search(cr, uid, [
            ('id', 'in', ids),
            ('type', '=', ['out_invoice'])
        ])
        self.generate_bills(cr, uid, ids, context=context)