from openerp.osv import orm, fields


class account_voucher(orm.Model):
    _inherit = 'account.voucher'
    _columns = {
        'clearance_delay': fields.integer('Giorni di esposizione', readonly=True, states={'draft':[('readonly',False)]}),
        'bill' : fields.boolean('Bill'),
        }

    _constraints = [
        (
            lambda s, *a, **k: s._check_bill_date_due(*a, **k),
            'L\'emissione di effetti richiede una data di scadenza!',
            ['journal_id', 'date_due'],
        )
    ]

    def _check_bill_date_due(self, cr, uid, ids, context=None):
        r = self.search(cr, uid, [
            ('id', 'in', ids),
            ('journal_id.bill_receivable', '=', True),
            ('date_due','=', False),
            ], limit=1, context=context)
        return not bool(r)

    def onchange_journal(self, cr, uid, ids, journal_id, line_ids, tax_id, partner_id, date, amount, ttype, company_id, context=None):
        res = super(account_voucher, self).onchange_journal(cr, uid, ids, journal_id, line_ids, tax_id, partner_id, date, amount, ttype, company_id, context=context)
        journal = self.pool.get('account.journal').browse(cr, uid, journal_id, context=context)
        if res:
            res['value'].update({
                'clearance_delay': journal.clearance_delay,
                'bill': journal.bill_receivable,
                })
        return res

    def account_move_get(self, cr, uid, voucher_id, context=None):
        res = super(account_voucher, self).account_move_get(cr, uid, voucher_id, context=context)
        voucher_record = self.browse(cr, uid, voucher_id, context=context)
        res.update({
            'bill_receivable': voucher_record.journal_id.bill_receivable,
            'clearance_delay': voucher_record.clearance_delay,
            'clearance_type': voucher_record.journal_id.clearance_type,
            'bill_date_due': voucher_record.date_due,
        })
        return res


    def first_move_line_get(self, cr, uid, voucher_id, move_id, company_currency, current_currency, context=None):
        res = super(account_voucher, self).first_move_line_get(cr, uid, voucher_id, move_id,
                                                               company_currency, current_currency, context=context)
        voucher = self.browse(cr, uid, voucher_id, context=context)
        res.update({
            'bill_receivable': voucher.journal_id.bill_receivable,
        })
        return res


    def _get_company_currency(self, cr, uid, voucher_id, context=None):
        return self.browse(cr,uid,voucher_id,context).journal_id.company_id.currency_id.id


    def cancel_voucher(self, cr, uid, ids, context=None):
        """
        Fix a couple bugs with unreconciliation.
        """
        reconcile_pool = self.pool.get('account.move.reconcile')
        move_pool = self.pool.get('account.move')
        move_line_pool = self.pool.get('account.move.line')
        for voucher in self.browse(cr, uid, ids, context=context):
            # refresh to make sure you don't unlink an already removed move
            voucher.refresh()
            for line in voucher.move_ids:
                line.refresh() # bug no.1: if the line is reconciled with another one in the same move,
                               # we might try to unlink a reconciliation that is already unlinked
                if line.reconcile_id:
                    move_lines = [move_line.id for move_line in line.reconcile_id.line_id
                                  if move_line.move_id != voucher.move_id] # bug no. 2:  we don't want to re-reconcile
                                                                           # lines that are part of the voucher still
                    reconcile_pool.unlink(cr, uid, [line.reconcile_id.id])
                    if len(move_lines) >= 2:
                        move_line_pool.reconcile_partial(cr, uid, move_lines, 'auto',context=context)
            if voucher.move_id:
                move_pool.button_cancel(cr, uid, [voucher.move_id.id])
                move_pool.unlink(cr, uid, [voucher.move_id.id])
        res = {
            'state':'cancel',
            'move_id':False,
        }
        self.write(cr, uid, ids, res)
        return True
