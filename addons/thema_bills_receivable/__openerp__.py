# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-2014 ZestyBeanz Technologies Pvt Ltd(<http://www.zbeanztech.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Bills Receivable',
    'version': '0.5',
    'sequence': 1,
    'category': 'Custom',
    'complexity': "easy",
    'description': """ This module handled the concept of bills receivable in OpenERP""",
    'author': 'Zesty Beanz Technologies PVT Ltd',
    'website': 'http://www.zbeanztech.com',
    'depends': [
        'account',
        'account_voucher',
        'sale',
        'account_cancel',
        'thema_bank',
        ],
    'data': [
        'ir.model.access.csv',
        'account_view.xml',
        'deposit_view.xml',
        'payment_view.xml',
        'cron.xml',
        'bank.xml',
        'invoice.xml',
        ],
    'demo_xml': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: