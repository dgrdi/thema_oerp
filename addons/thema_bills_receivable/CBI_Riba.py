import unidecode
import datetime

CAMPI_DISTINTA = {
    'cod_sia':      'Codice SIA nostro',
    'cod_abi':      'Codice ABI nostro',
    'cod_cab':      'Codice CAB nostro',
    'cod_conto':    'Codice del nostro Conto (incassi)', #facoltativo
    'data_creaz':   'Data di creazione del flusso',
    'id_distinta':  'Codice identificativo della distinta',
    'riferimento':  'Riferimento nostro',    #facoltativo
    'rag_soc':      'Ragione sociale nostra',
    'indirizzo':    'Indirizzo nostra sede',
    'localita':     'Localita` nostra sede',
    'cap_prov':     'Cap e Provincia nostra sede',
    'piva':         'Codice Fiscale/Partita IVA nostra'
}
CAMPI_EFFETTO = {
    'data_scad':    'Data scadenza pagamento',
    'importo':      'Importo della fattura',
    'cod_abi_cli':  'Codice ABI cliente',
    'cod_cab_cli':  'Codice CAB cliente',
    'denom_banca':  'Denominazione Banca del cliente',
    'id_cliente':   'Codice interno del cliente',
    'rag_soc_cli':  'Ragione sociale del cliente',
    'piva_cli':     'Partita IVA del cliente',
    'indirizzo_cli':'Indirizzo nostra del cliente',
    'localita_prov':'Localita` e provincia del cliente',
    'cap_cli':      'Cap e Provincia  del cliente',
    'is_banca_cli': 'Vero se il cliente e` una banca',
    'rif_fattura':  'Codice identificativo della fattura di riferimento',
    'rif_effetto':  'Codice di riferimento all''effetto',
}


def n(string, length, align=1, char=""):
    if align==0:
        #allinea a destra
        char=str(char)+'>'
    else:
        #allinea a sinistra
        char=str(char)+'<'

    template='{:'+'{0}{1}'.format(char,length)+'}'
    return template.format(string)[:length]


def get_record_string(ir, dati, fr):
    """
    Metodo per calcolare la stringa dei record a partire da
     @ir  record iniziale
     @dati lista dei record
     @fr record finale
    """

    #stringa di result
    res=''
    #stringa di blank
    blank=' '
    #stringa di nuova linea
    newline='\n'


    #numero dei record
    num=1
    #record iniziale
    res=res+blank+n(ir['tipo_record'],2)+n(ir['sia_mittente'],5)+n(ir['abi_ricevente'],5) \
        +'{:%d%m%y}'.format(ir['data_creazione'])+n(format(ir['nome_supporto']),20) \
        +n(ir['campo_disposizione'],6)+ 59*blank+ n(ir['tipo_flusso'],1) \
        +n(ir['qualificatore_flusso'], 1)+ n(ir['soggetto_veicolatore'],5) \
        + 2*blank +n(ir['codice_divisa'],1) + blank + 5*blank + newline

    for i in range(len(dati)):
        num=num+7
        j=0
        res=res+blank+n(dati[i][j]['tipo_record'], 2) +n(dati[i][j]['num_progressivo'],7,0,0) + 12*blank \
        +'{:%d%m%y}'.format(dati[i][j]['data_pagamento'])+n(dati[i][j]['causale'],5) +n(dati[i][j]['importo'],13,0,0) \
        +n(dati[i][j]['segno'],1) +n(dati[i][j]['abi_assuntrice'],5)+n(dati[i][j]['cab_assuntrice'],5)\
        +n(dati[i][j]['conto'],12)+n(dati[i][j]['abi_domiciliataria'],5)+n(dati[i][j]['cab_domiciliataria'],5)\
        +12*blank+n(dati[i][j]['sia_azienda'],5)+n(dati[i][j]['tipo_codice'],1)  +n(dati[i][j]['codice_cliente'],16)\
        +n(dati[i][j]['flag_debitore'],1) + 5*blank +n(dati[i][j]['codice_divisa'],1) + newline
        j=j+1
        res=res+blank+n(dati[i][j]['tipo_record'], 2) +n(dati[i][j]['num_progressivo'],7,0,0) \
        +n(dati[i][j]['creditore_s1'],24) +n(dati[i][j]['creditore_s2'],24) +n(dati[i][j]['creditore_s3'],24) \
        +n(dati[i][j]['creditore_s4'],24) + 14*blank + newline
        j=j+1 #
        res=res+blank+n(dati[i][j]['tipo_record'], 2) +n(dati[i][j]['num_progressivo'],7,0,0) \
        +n(dati[i][j]['debitore_s12'],60) +n(dati[i][j]['debitore_cf'],16) + 34*blank + newline
        j=j+1
        res=res+blank+n(dati[i][j]['tipo_record'], 2) +n(dati[i][j]['num_progressivo'],7,0,0) \
        +n(dati[i][j]['debitore_indirizzo'],30) +n(dati[i][j]['debitore_cap'],5) +n(dati[i][j]['debitore_comprov'],25) \
        +n(dati[i][j]['banca_domiciliataria'],50) + newline
        j=j+1
        res=res+blank+n(dati[i][j]['tipo_record'], 2) +n(dati[i][j]['num_progressivo'],7,0,0) \
        +n(dati[i][j]['debito_s12'],80) + 10*blank +n(dati[i][j]['creditore_cf'],16) + 4*blank + newline
        j=j+1
        res=res+blank+n(dati[i][j]['tipo_record'], 2) +n(dati[i][j]['num_progressivo'],7,0,0) \
        +n(dati[i][j]['numero_ricevuta'],10) +n(dati[i][j]['creditore_denom'],20) \
        +n(dati[i][j]['bv_provincia'],15) +n(dati[i][j]['bv_num_autorizzazione'],10,0,0)\
        +'{:%d%m%y}'.format(dati[i][j]['bv_data_autorizzazione'])+49*blank + newline
        j=j+1
        res=res+blank+n(dati[i][j]['tipo_record'], 2) +n(dati[i][j]['num_progressivo'],7,0,0) +78*blank \
        +n(dati[i][j]['indicatori_circuito'],12) +n(dati[i][j]['debitore_tipo_doc'],1) \
        +n(dati[i][j]['flag_richiesta_esito'],1) +n(dati[i][j]['flag_stampa_avviso'],1)\
        +n(dati[i][j]['chiavi_controllo'],17)+ newline

    num=num+1
    fr['num_record']=num
    #record finale
    res=res+blank+n(fr['tipo_record'],2)+n(fr['sia_mittente'],5)+n(fr['abi_ricevente'],5) \
        +'{:%d%m%y}'.format(fr['data_creazione'])+n(format(fr['nome_supporto']),20) \
        +n(fr['campo_disposizione'],6)+ n(fr['num_disposizioni'],7,0,0) \
        +n(fr['tot_negativo'],15,0,0)+ n(fr['tot_positivo'],15,0,0) + n(fr['num_record'],7,0,0) \
        + 24*blank +n(fr['codice_divisa'],1) + 6*blank + newline

    return res

def get_efdistinta_string(distinta, effetti):

    cod_divisa='E'  #codice divisa fisso
    # record iniziale
    recI ={'tipo_record': 'IB', 'sia_mittente': distinta['cod_sia'], 'abi_ricevente': distinta['cod_abi'],
           'data_creazione': distinta['data_creaz'], 'nome_supporto':distinta['id_distinta'],
           'campo_disposizione':distinta['riferimento'], 'tipo_flusso':'', 'qualificatore_flusso':'',
           'soggetto_veicolatore':'', 'codice_divisa':cod_divisa}

    # lista contenente i dati dei singoli record
    data=[]
    num_riba=len(effetti)
    importi=0
    for i in range(num_riba):
        prog=i+1 #numero progressivo
        importi=importi+effetti[i]['importo']
        # record codice fisso 14
        rec14={'tipo_record': '14', 'num_progressivo': prog, 'data_pagamento': effetti[i]['data_scad'], 'causale':30000,
               'importo':effetti[i]['importo'], 'segno':'-', 'abi_assuntrice':distinta['cod_abi'],
               'cab_assuntrice':distinta['cod_cab'], 'conto': distinta['cod_conto'],
               'abi_domiciliataria':effetti[i]['cod_abi_cli'], 'cab_domiciliataria':effetti[i]['cod_cab_cli'],
               'sia_azienda': distinta['cod_sia'], 'tipo_codice':'4','codice_cliente':effetti[i]['id_cliente'],
               'flag_debitore':'B' if effetti[i]['is_banca_cli'] else '', 'codice_divisa':cod_divisa}

        # record codice fisso 20
        rec20={'tipo_record': '20', 'num_progressivo':  prog, 'creditore_s1': distinta['rag_soc'],
               'creditore_s2':  distinta['indirizzo'], 'creditore_s3':  distinta['localita'],
               'creditore_s4':  distinta['cap_prov']}

        # record codice fisso 30
        rec30={'tipo_record': '30', 'num_progressivo': prog, 'debitore_s12':effetti[i]['rag_soc_cli'],
               'debitore_s1': effetti[i]['rag_soc_cli'],'debitore_s2': '', #non usati: viene usato s12
               'debitore_cf':effetti[i]['piva_cli']}

        # record codice fisso 40
        rec40={'tipo_record': '40', 'num_progressivo': prog, 'debitore_indirizzo': effetti[i]['indirizzo_cli'],
               'debitore_cap': effetti[i]['cap_cli'],'debitore_comprov':effetti[i]['localita_prov'],
               'banca_domiciliataria':effetti[i]['denom_banca']}

        # record codice fisso 50
        desc_fatt_ins='F'+n(effetti[i]['rif_fattura'],7)+'{:%d/%m/%y}'.format(effetti[i]['data_scad'])+' IMPORTO TOTALE FATTURA '+str(effetti[i]['importo'])
        rec50={'tipo_record': '50', 'num_progressivo': prog, 'debito_s12':  desc_fatt_ins,
               'debito_s1': desc_fatt_ins, 'debito_s2': '',  #non usati: viene usato s12
               'creditore_cf': distinta['piva']}

        # record codice fisso 51
        rec51={'tipo_record': '51', 'num_progressivo':prog, 'numero_ricevuta': effetti[i]['rif_effetto'],
               'creditore_denom': distinta['rag_soc'],
               'bv_provincia': '','bv_num_autorizzazione':0, 'bv_data_autorizzazione':datetime.date(1999,12,30)}

        # record codice fisso 70
        rec70={'tipo_record': '70', 'num_progressivo': prog, 'indicatori_circuito': '',
               'debitore_tipo_doc':'', 'flag_richiesta_esito':'', 'flag_stampa_avviso':'', 'chiavi_controllo': ''}
        data.append([rec14,rec20, rec30, rec40, rec50, rec51, rec70])

    #record di coda
    recF ={'tipo_record': 'EF', 'sia_mittente': distinta['cod_sia'], 'abi_ricevente': distinta['cod_abi'],
       'data_creazione': distinta['data_creaz'], 'nome_supporto':distinta['id_distinta'],
       'campo_disposizione':distinta['riferimento'], 'codice_divisa':cod_divisa,
       #campi dinamici
       'num_disposizioni':num_riba, 'tot_negativo':importi, 'tot_positivo': 0, 'num_record':num_riba*7+2}


    return get_record_string(recI, data, recF)


