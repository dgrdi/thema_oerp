from openerp.osv import orm, fields
from openerp.addons.account.account_invoice import account_invoice as aiv
from openerp import netsvc

class invoice(orm.Model):
    _inherit = 'account.invoice'

    _columns = {
        'paid': fields.function(lambda s, *a, **k: s._paid(*a, **k),
                                type='boolean',
                                string='Pagata',
                                multi='pay',
                                ),
        'fully_paid': fields.function(lambda s, *a, **k: s._paid(*a, **k),
                                      type='boolean',
                                      string='Incassata',
                                      multi='pay'),
        'reconciled': fields.function(lambda s, *a, **k: s._reconciled(*a, **k),
                                      string='Paid/Reconciled', type='boolean',
            store={
                'account.invoice': (lambda self, cr, uid, ids, c={}: ids, None, 50), # Check if we can remove ?
                'account.move.line': (aiv._get_invoice_from_line.__func__, None, 50),
                'account.move.reconcile': (aiv._get_invoice_from_reconcile.__func__, None, 50),
            }, help="It indicates that the invoice has been paid and the journal entry of the invoice has been "
                    "reconciled with one or several journal entries of payment."),

        'state_ext': fields.function(lambda s, *a, **k: s._state_ext(*a, **k),
                                     string='Stato',
                                     type='selection',
                                     selection = [
                                        ('draft','Bozza'),
                                        ('proforma','Pro-forma'),
                                        ('proforma2','Pro-forma'),
                                        ('open','Aperta'),
                                        ('paid','Pagata'),
                                        ('cancel','Annullata'),
                                        ('fully_paid', 'Incassata')
                                     ],
                                     store={
                                         'account.invoice': (
                                             lambda s, c, u, ids, ctx=None: ids,
                                             ['state'],
                                             10
                                         )
                                     })

    }

    def _paid(self, cr, uid, ids, fields, arg, context=None):
        cr.execute("""
            SELECT
              i.id,
              CASE WHEN exists(
                  SELECT
                    1
                  FROM account_invoice i2
                    LEFT JOIN account_move m ON m.id = i2.move_id
                    LEFT JOIN account_move_line ml ON ml.move_id = m.id AND ml.account_id = i2.account_id
                  WHERE ml.reconcile_id IS NULL
                        AND i2.id = i.id
                        AND i2.id IN %(ids)s

              ) THEN FALSE
              ELSE TRUE END paid,
              CASE WHEN exists(
                  SELECT
                    1
                  FROM account_invoice i2
                    LEFT JOIN account_move m ON m.id = i2.move_id
                    LEFT JOIN account_move_line ml ON ml.move_id = m.id AND ml.account_id = i2.account_id
                    LEFT JOIN account_move_line ml2 ON ml2.move_group_id = ml.id
                    LEFT JOIN account_move m2 ON m2.id = ml2.move_id
                  WHERE
                    i2.id = i.id
                    AND (
                      ml2.reconcile_id IS NULL
                      OR (m2.bill_receivable
                          AND (m2.bill_state_current IN ('emitted', 'deposited') OR m2.bill_state_current IS NULL)
                      )
                    )
                    AND i2.id IN %(ids)s

              ) THEN FALSE
              ELSE TRUE END fully_paid
            FROM account_invoice i
            WHERE i.id IN %(ids)s
        """, {'ids': tuple(ids)})
        return {el.pop('id'): el for el in cr.dictfetchall()}


    def _reconciled(self, cr, uid, ids, field, arg, context=None):
        data = self.read(cr, uid, ids, fields=['paid', 'state'], context=context)
        res = {}
        wf_service = netsvc.LocalService("workflow")
        for el in data:
            res[el['id']] = el['paid']
            if not el['paid'] and el['state'] == 'paid':
                wf_service.trg_validate(uid, 'account.invoice', el['id'], 'open_test', cr)
        return res

    def _state_ext(self, cr, uid, ids, field, arg, context=None):
        data = self.read(cr, uid, ids, fields=['fully_paid', 'state'], context=context)
        res = {}
        for el in data:
            if el['fully_paid'] and el['state'] == 'paid':
                res[el['id']] = 'fully_paid'
            else:
                res[el['id']] = el['state']
        return res

    def recompute_state_ext(self, cr, uid, context=None):
        ids = self.search(cr, uid, [('state_ext', '=', 'paid')], context=context)
        self._store_set_values(cr, uid, ids, ['state_ext'], context=context)

    def recompute_state(self, cr, uid, ids=None, context=None):
        if ids is None:
            ids = self.search(cr, uid, [], context=context)
        data = self.read(cr, uid, ids, fields=['paid'], context=context)
        paid = [el['id'] for el in data if el['paid']]
        self.confirm_paid(cr, uid, paid, context=context)
