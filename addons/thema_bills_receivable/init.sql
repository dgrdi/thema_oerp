DO LANGUAGE plpgsql $b$
BEGIN
  IF NOT exists(SELECT
                  1
                FROM information_schema.schemata
                WHERE schema_name = 'bills_receivable')
  THEN
    CREATE SCHEMA bills_receivable;
  END IF;
END
$b$;

CREATE OR REPLACE FUNCTION bills_receivable.fn_date_paid(date_due       DATE, date_deposit DATE, clearance_delay INT,
                                                         clearance_type VARCHAR)
  RETURNS DATE AS $b$
BEGIN
  RETURN CASE WHEN clearance_type IS NULL OR clearance_type = 'due' THEN date_due + clearance_delay
         ELSE date_deposit + clearance_delay
         END;
END
$b$ LANGUAGE plpgsql IMMUTABLE;


CREATE OR REPLACE FUNCTION bills_receivable.fn_state(IN ids INT [], IN date_ref DATE, IN move_states VARCHAR [])
  RETURNS TABLE(
  id INT,
  state VARCHAR
  )
SET SEARCH_PATH TO bills_receivable, PUBLIC
AS
  $BODY$
  BEGIN
    RETURN QUERY
    SELECT
      m.id,
      CASE
      WHEN NOT m.bill_receivable OR NOT m.state = ANY (move_states) THEN NULL
      WHEN can.id IS NOT NULL THEN 'canceled' :: VARCHAR
      WHEN unp.id IS NOT NULL THEN 'unpaid' :: VARCHAR
      WHEN (m.bill_force_deposit OR dep.id IS NOT NULL)
           AND
           fn_date_paid(m.bill_date_due, m.bill_date_deposit, m.clearance_delay, m.clearance_type)
           <= date_ref THEN 'paid' :: VARCHAR
      WHEN m.bill_force_deposit OR dep.id IS NOT NULL THEN 'deposited' :: VARCHAR
      ELSE 'emitted' :: VARCHAR
      END
    FROM account_move m
      LEFT JOIN account_move dep
        ON dep.id = m.bill_deposit_id AND dep.date <= date_ref AND dep.state = ANY (move_states)
      LEFT JOIN account_move can ON can.id = m.bill_cancel_id AND can.date <= date_ref AND dep.state = ANY (move_states)
      LEFT JOIN account_move unp ON unp.id = m.bill_unpaid_id AND unp.date <= date_ref AND unp.state = ANY (move_states)
    WHERE m.id = ANY (ids);
  END
  $BODY$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION bills_receivable.fn_state(bill_id INT, date_ref DATE, move_states VARCHAR [])
  RETURNS VARCHAR SET search_path TO bills_receivable, PUBLIC AS $b$
DECLARE
  res VARCHAR;
BEGIN
  SELECT
    CASE
    WHEN NOT m.bill_receivable OR NOT m.state = ANY (move_states) THEN NULL
    WHEN can.id IS NOT NULL THEN 'canceled' :: VARCHAR
    WHEN unp.id IS NOT NULL THEN 'unpaid' :: VARCHAR
    WHEN (m.bill_force_deposit OR dep.id IS NOT NULL)
         AND
         fn_date_paid(m.bill_date_due, m.bill_date_deposit, m.clearance_delay, m.clearance_type)
         <= date_ref THEN 'paid' :: VARCHAR
    WHEN m.bill_force_deposit OR dep.id IS NOT NULL THEN 'deposited' :: VARCHAR
    ELSE 'emitted' :: VARCHAR
    END
  FROM account_move m
    LEFT JOIN account_move dep
      ON dep.id = m.bill_deposit_id AND dep.date <= date_ref AND dep.state = ANY (move_states)
    LEFT JOIN account_move can ON can.id = m.bill_cancel_id AND can.date <= date_ref AND dep.state = ANY (move_states)
    LEFT JOIN account_move unp ON unp.id = m.bill_unpaid_id AND unp.date <= date_ref AND unp.state = ANY (move_states)
  WHERE m.id = bill_id
  INTO res;
  RETURN res;
END
$b$ LANGUAGE plpgsql STABLE;


CREATE UNLOGGED TABLE IF NOT EXISTS bills_receivable.track_ids (
  txid    BIGINT,
  move_id INT
);


CREATE OR REPLACE FUNCTION bills_receivable.trg_track_move_pointers()
  RETURNS TRIGGER
SET SEARCH_PATH TO bills_receivable, PUBLIC
AS $b$
BEGIN
  INSERT INTO track_ids
    SELECT
      txid_current(),
      m.id
    FROM account_move m
    WHERE m.bill_deposit_id = old.id OR m.bill_cancel_id = old.id OR m.bill_unpaid_id = old.id OR m.id = old.id;
  IF TG_OP = 'DELETE'
  THEN RETURN OLD; END IF;

  RETURN NEW;
END
$b$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION bills_receivable.trg_track_move()
  RETURNS TRIGGER SET search_path TO bills_receivable, PUBLIC
AS $b$
BEGIN
  INSERT INTO track_ids (txid, move_id)
  VALUES (txid_current(), NEW.id);
  RETURN NULL;
END
$b$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION bills_receivable.trg_update_state()
  RETURNS TRIGGER SET search_path TO bills_receivable, PUBLIC AS $b$
BEGIN
  UPDATE account_move
  SET bill_state_current = n.state
  FROM fn_state(
           array(SELECT
                   t.move_id
                 FROM track_ids t
                 WHERE t.txid = txid_current()),
           current_date,
           ARRAY ['posted']
       ) n
  WHERE n.id = account_move.id;
  DELETE FROM track_ids
  WHERE txid = txid_current();
  RETURN NULL;
END
$b$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_bill_state_delete ON account_move;
CREATE TRIGGER trg_bill_state_delete
BEFORE DELETE ON account_move FOR EACH ROW EXECUTE PROCEDURE bills_receivable.trg_track_move_pointers();

DROP TRIGGER IF EXISTS trg_bill_state_insert ON account_move;
CREATE TRIGGER trg_bill_state_insert
AFTER INSERT ON account_move FOR EACH ROW EXECUTE PROCEDURE bills_receivable.trg_track_move();

DROP TRIGGER IF EXISTS trg_bill_state_update_pointers ON account_move;
CREATE TRIGGER trg_bill_state_update_pointers
BEFORE UPDATE OF state, "date"
ON account_move
FOR EACH ROW
WHEN (
  (old.state IS DISTINCT FROM new.state)
  OR (old.date IS DISTINCT FROM new.date)
)
EXECUTE PROCEDURE bills_receivable.trg_track_move_pointers();

DROP TRIGGER IF EXISTS trg_bill_state_update ON account_move;
CREATE TRIGGER trg_bill_state_update
AFTER UPDATE OF bill_receivable, bill_deposit_id, bill_cancel_id, bill_unpaid_id
ON account_move
FOR EACH ROW
WHEN (
  (old.bill_receivable IS DISTINCT FROM new.bill_receivable)
  OR (old.bill_deposit_id IS DISTINCT FROM new.bill_deposit_id)
  OR (old.bill_cancel_id IS DISTINCT FROM new.bill_cancel_id)
  OR (old.bill_unpaid_id IS DISTINCT FROM new.bill_unpaid_id)
)
EXECUTE PROCEDURE bills_receivable.trg_track_move();


DROP TRIGGER IF EXISTS trg_bill_update_state ON account_move;
CREATE TRIGGER trg_bill_update_state
AFTER INSERT
OR DELETE
OR UPDATE OF bill_receivable, bill_deposit_id, bill_cancel_id, bill_unpaid_id, state, "date"
ON account_move
FOR EACH STATEMENT
EXECUTE PROCEDURE bills_receivable.trg_update_state();

CREATE OR REPLACE FUNCTION bills_receivable.fn_refresh_state()
  RETURNS VOID SET search_path TO bills_receivable, PUBLIC AS $b$
BEGIN
  UPDATE account_move
  SET bill_state_current = n.state
  FROM fn_state(array(
                    SELECT
                      id
                    FROM account_move
                    WHERE bill_state_current = 'deposited'
                          AND fn_date_paid(bill_date_due, bill_date_deposit, clearance_delay, clearance_type) <=
                              current_date
                ), current_date, ARRAY ['posted']) n
  WHERE n.id = account_move.id;
END
$b$ LANGUAGE plpgsql;

