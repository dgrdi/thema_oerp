from openerp.osv import orm, fields

class sale(orm.Model):
    _inherit = 'sale.order'
    _columns = {
        'debit_bank_id': fields.many2one('res.partner.bank', 'Conto bancario di addebito'),
    }

    def _prepare_invoice(self, cr, uid, order, lines, context=None):
        res = super(sale, self)._prepare_invoice(cr, uid, order, lines, context=context)
        res.update({
            'debit_bank_id': order.debit_bank_id and order.debit_bank_id.id or False,
        })
        return res

class stock_picking(orm.Model):
    _inherit = 'stock.picking'

    def _prepare_invoice(self, cr, uid, picking, partner, inv_type, journal_id, context=None):
        res = super(stock_picking, self)._prepare_invoice(cr, uid, picking, partner, inv_type, journal_id,
                                                          context=context)
        res.update({
            'debit_bank_id': picking.sale_id.debit_bank_id and picking.sale_id.debit_bank_id.id,
        })
        return res

class invoice(orm.Model):
    _inherit = 'account.invoice'
    _columns = {
        'debit_bank_id': fields.many2one('res.partner.bank', 'Conto bancario di addebito'),
    }


class move(orm.Model):
    _inherit = 'account.move'
    _columns = {
        'debit_bank_id': fields.many2one('res.partner.bank', 'Banca versamento'),
    }

class invoice(orm.Model):
    _inherit = 'account.invoice'

    def bill_payment_move_get(self, cr, uid, inv, line):
        res = super(invoice, self).bill_payment_move_get(cr, uid, inv, line)
        res.update({
            'debit_bank_id': inv.debit_bank_id and inv.debit_bank_id.id,
        })
        return res
