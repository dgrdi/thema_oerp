# coding=utf-8
from openerp.osv import orm, fields
from itertools import imap
import time, datetime
from openerp.tools.float_utils import float_is_zero
from dateutil.parser import parse
from osv.osv import except_osv

BILL_STATE_SEL = [
    ('na', 'Non applicabile'),
    ('emitted', 'Emesso'),
    ('deposited', 'Depositato'),
    ('canceled', 'Richiamato'),
    ('unpaid', 'Insoluto'),
    ('paid', 'Incassato'),
]

class account_move(orm.Model):
    _inherit = 'account.move'

    _columns = {
        'bill_receivable': fields.boolean('Bills Receivable'),
        'bill_deposit': fields.boolean('Bills Deposit'),
        'clearance_type': fields.selection([
            ('emit', 'Da data emissione'),
            ('deposit', 'Da data versamento'),
        ], string='Decorrenza esposizione'),
        'clearance_delay': fields.integer('Giorni esposizione'),
        'bill_date_due': fields.date('Scadenza effetto'),
        'bill_force_deposit': fields.boolean('Forza deposito effetto'),
        'bill_date_deposit': fields.related('bill_deposit_id', 'date', string='Data versamento',
                                            type='date',
                                            store=True),
        'bill_deposit_id': fields.many2one('account.move', 'Reg. deposito effetto', select=1),
        'bill_cancel_id': fields.many2one('account.move', 'Reg. richiamo effetto', select=1),
        'bill_unpaid_id': fields.many2one('account.move', 'Reg. insoluto effetto', select=1),
        'bill_state': fields.function(lambda s, *a, **k: s._bill_state(*a, **k),
                                      type='selection',
                                      string='Stato effetto',
                                      selection=BILL_STATE_SEL,
                                      ),
        'bill_state_current': fields.selection(BILL_STATE_SEL, 'Stato attuale', readonly=True),
        'bill_line_id': fields.function(lambda s, *a, **k: s._bill_line_id(*a, **k),
                                        type='many2one',
                                        relation='account.move.line',
                                        string='Voce effetto'),
        'partner_line_ids': fields.one2many('account.move.line', 'move_id', domain=[('bill_receivable', '=', False)],
                                           string='Voci cliente'),
    }

    _constraints = [
        (
            lambda s, *a, **k: s._check_one_bill_line(*a, **k),
            u'E` consentito solo un effetto per registrazione',
            ['line_id']
        ),
    ]

    def init(self, cr):
        sp = super(account_move, self)
        if hasattr(sp, 'init'):
            sp.init(cr)
        import os
        with open(os.path.join(os.path.dirname(__file__), 'init.sql')) as fo:
            cr.execute(fo.read())

    def _uninstall(self, cr):
        import os
        with open(os.path.join(os.path.dirname(__file__), 'uninstall.sql')) as fo:
            cr.execute(fo.read())

    def _check_one_bill_line(self, cr, uid, ids, context=None):
        cr.execute("""
            SELECT 1
            FROM account_move m
            INNER JOIN account_move_line ml ON ml.move_id = m.id
            WHERE m.bill_receivable
            AND ml.bill_receivable
            AND m.id IN %s
            GROUP BY m.id
            HAVING count(*) > 1;
        """, [tuple(ids)])
        return not bool(cr.fetchall())

    def _bill_state(self, cr, uid, ids, field, arg, context=None):
        context = context or {}
        date = context.get('date', time.strftime('%Y-%m-%d'))
        states = context.get('states', ['posted'])
        cr.execute("""
            SELECT id, state FROM
            bills_receivable.fn_state(%s, %s, %s);
        """, [list(ids), date, list(states)])
        return dict(cr.fetchall())

    def cron_recompute_bill_state(self, cr, uid, ids=None, context=None):
        cr.execute("SELECT bills_receivable.fn_refresh_state();")
        return False

    def _bill_line_id(self, cr, uid, ids, field, arg, context=None):
        cr.execute("""
            SELECT
                m.id,
                ml.id
            FROM account_move m
            LEFT JOIN account_move_line ml ON ml.move_id = m.id AND ml.bill_receivable
            WHERE m.id IN %s
        """, [tuple(ids)])
        return dict((m, ml if ml else False) for m, ml in cr.fetchall())

    def bill_action_cancel(self, cr, uid, ids, context=None):
        ids = self.search(cr, uid, [
            ('id', 'in', ids),
            ('bill_state_current', 'in', ['deposited', 'paid']),
        ], context=context)
        m = self.pool.get('account.move')
        created = []
        for bill in self.browse(cr, uid, ids, context=context):
            journal = bill.journal_id.bill_cancel_journal_id
            if not journal:
                raise except_osv(u'Errore!', u"Il sezionale %s non è configurato "
                                             u"per la registrazione dei richiami." % bill.journal_id.name)
            cancel_acct = bill.bill_line_id.bill_deposit_id.cancel_account_id
            cancel_acct = cancel_acct and cancel_acct.id or \
                bill.bill_line_id.bill_deposit_id.journal_id.default_credit_account_id.id
            move = self.bill_get_reopen_move(cr, uid, bill, u'Richiamo', journal, cancel_acct)
            cid = m.create(cr, uid, move, context=context)
            self.write(cr, uid, bill.id, {'bill_cancel_id': cid}, context=context)
            created.append(cid)
        return self._open_ids(cr, uid, created, context=context)

    def bill_action_unpaid(self, cr, uid, ids, context=None):
        ids = self.search(cr, uid, [
            ('id', 'in', ids),
            ('bill_state_current', 'in', ['deposited', 'paid'])
        ], context=context)
        m = self.pool.get('account.move')
        created = []
        for bill in self.browse(cr, uid, ids, context=context):
            journal = bill.journal_id.bill_unpaid_journal_id
            if not journal:
                raise except_osv(u'Errore!', u"Il sezionale %s non è configurato "
                                             u"per la registrazione degli insoluti." % bill.journal_id.name)
            unpaid_acct = bill.bill_line_id.bill_deposit_id.unpaid_account_id
            unpaid_acct = unpaid_acct and unpaid_acct.id or \
                bill.bill_line_id.bill_deposit_id.journal_id.default_credit_account_id.id
            if not unpaid_acct:
                raise except_osv(u'Errore!', u"Il sezionale %s non è configurato "
                                             u"con un conto predefinito." % journal.name)
            move = self.bill_get_reopen_move(cr, uid, bill, u'Insoluto', journal, unpaid_acct)
            cid = m.create(cr, uid, move, context=context)
            self.write(cr, uid, bill.id, {'bill_unpaid_id': cid}, context=context)
            created.append(cid)
        return self._open_ids(cr, uid, created, context=context)

    def _open_ids(self, cr, uid, ids, context=None):
        act =  {
            'type': 'ir.actions.act_window',
            'res_model': 'account.move',
            'domain': "[('id', 'in', %s)]" % repr(ids),
            'view_mode': 'tree,form',
        }
        if len(ids) == 1:
            act['res_id'] = ids[0]
            act['view_mode'] = 'form,tree'
        return act


    def bill_get_reopen_lines(self, cr, uid, bill):
        res = []
        for line in bill.line_id:
            if line.id == bill.bill_line_id.id:
                continue
            res.append({
                'bill_orig_id': line.id,
                'debit': line.credit,
                'credit': line.debit,
                'account_id': line.account_id.id,
                'name': bill.name,
                'partner_id': line.partner_id.id,
            })
        return res

    def bill_get_reopen_move(self, cr, uid, bill, reason, journal, acct):
        name = u'%s %s scad. %s' % (reason, bill.name, parse(bill.bill_date_due).strftime('%d/%m/%Y'))
        move = {
            'narration': name,
            'journal_id': journal.id,
            'ref': bill.name,
        }
        deposit = bill.bill_line_id.bill_deposit_id
        lines = self.bill_get_reopen_lines(cr, uid, bill)
        lines.append({
            'account_id': acct,
            'credit': deposit.credit,
            'debit': 0,
            'name': name,
            'partner_id': bill.bill_line_id.partner_id.id,
        })
        tot = sum(el['debit'] - el['credit'] for el in lines)
        assert float_is_zero(tot, precision_digits=4), lines
        lines = [(0, 0, l) for l in lines]
        move['line_id'] = lines
        return move

    def action_generate_deposit(self, cr, uid, ids, context=None):
        v = self.pool.get('account.voucher.deposit')
        ids = self.search(cr, uid, [
            ('id', 'in', ids),
            ('bill_state_current', '=', 'emitted'),
        ], context=context)
        if not ids:
            raise except_osv(u'Errore!', u'Nessuna delle registrazioni selezionate è un effetto non ancora versato.')
        data = self.read(cr, uid, ids, fields=['journal_id'], context=context)
        journals = {el['journal_id'][0] for el in data}
        if len(journals) > 1:
            raise except_osv(u'Errore!', u'Per generare una distinta, tutti gli effetti devono essere dello stesso tipo.')
        lines = []
        for l in self.browse(cr, uid, ids, context=context):
            lines.append(v._get_line(cr, uid, l.bill_line_id))
        vals = {
            'default_bill_journal_id': iter(journals).next(),
            'default_line_cr_ids': lines
        }
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'account.voucher.deposit',
            'context': vals,
            'view_mode': 'form',
            'target': 'current',
        }
