# coding=utf-8
from openerp.osv import orm, fields
from osv.osv import except_osv
from .CBI_Riba import get_efdistinta_string
import datetime
import base64
from dateutil.parser import parse

class deposit_cbi(orm.TransientModel):
    _name = 'account.voucher.deposit.export.cbi'

    _columns = {
        'deposit_id': fields.many2one('account.voucher.deposit', 'Distinta', required=True, readonly=True),
        'export_file': fields.binary('Distinta elettronica', readonly=True),
    }

    def validate(self, cr, uid, ids, context=None):
        for dci in self.browse(cr, uid, ids, context=None):
            self.validate_deposit(cr, uid, dci.deposit_id)
            for l in dci.deposit_id.line_ids:
                self.validate_deposit_line(cr, uid, l)

    def validate_deposit(self, cr, uid, deposit):
        if not deposit.company_id.sia_code:
            raise except_osv('Errore!', u'Non è stato definito un codice SIA per l\'azienda.')
        if not deposit.bank_id:
            raise except_osv('Errore!', u'Non è stata definita un a banca per il versamento della distinta.')
        if not deposit.bank_id.abi or not deposit.bank_id.cab:
            raise except_osv('Errore!', u"Mancano l'ABI e/o il CAB per la banca di versamento "
                                        u"della distinta %s" % deposit.bank_id.bank_name)
        if not deposit.bank_id.acc_number:
            raise except_osv('Errore!', u'Il numero di conto bancario non e` definito per '
                                        u'sla banca %s' % deposit.bank_id.bank_name)
        cp =  self.check_partner(deposit.company_id.partner_id)
        if cp:
            raise except_osv(u'Errore!',
                             u'Il campo "%s" non è definito per l\'azienda %s' % (cp, deposit.company_id.name))

    def validate_deposit_line(self, cr, uid, line):
        ml = line.move_line_id
        msg = None
        if not ml.date_maturity and not ml.move_id.bill_date_due:
            msg = 'Data scadenza'
        print ml.move_id.partner_line_ids
        print ml.move_id.bill_line_id
        if not ml.partner_id and (not ml.move_id.partner_line_ids or not ml.move_id.partner_line_ids[0].partner_id):
            msg = 'Partner'
        if not msg:
            for el in [
                ('amount', 'Importo'),
                ('debit_bank_id', 'Banca di addebito'),
            ]:
                if not line[el[0]]:
                    msg = el[1]
        if not msg:
            msg = self.check_partner(ml.partner_id or ml.move_id.partner_line_ids[0].partner_id)
        if msg:
            raise except_osv('Errore!', u'Il campo "%s" non è definito per l\'effetto %s' % (msg, ml.move_id.name))
        bank = line.debit_bank_id
        if not bank.abi or not bank.cab:
            raise except_osv('Errore!', u'Mancano l\'ABI e/o il CAB per la banca di addebito '
                             u'dell\'effetto %s'  % ml.move_id.name)
        if not bank.bank_name:
            raise except_osv('Errore!', u'Manca un nome per la banca di addebito dell\'effetto %s' % ml.move_id.name)


    def check_partner(self, partner):
        for el in [
            ('name', 'Ragione sociale'),
            ('street', 'Indirizzo'),
            ('city', u'Città'),
            ('zip', 'CAP'),
            ('vat', 'Partita IVA')]:
            if not partner[el[0]]:
                return el[1]

    def generate_export_file(self, cr, uid, ids, context=None):
        self.validate(cr, uid, ids, context=context)
        for dci in self.browse(cr, uid, ids, context=None):
            ds = self.deposit_data_get(cr, uid, dci.deposit_id)
            lines = []
            for l in dci.deposit_id.line_ids:
                lines.append(self.line_data_get(cr, uid, l))
            exp = get_efdistinta_string(ds, lines)
            exp = exp.encode('utf-8')
            exp = base64.b64encode(exp)
            self.write(cr, uid, dci.id, {'export_file': exp})

    def deposit_data_get(self, cr, uid, deposit):
        p = deposit.company_id.partner_id
        return {
            'cod_sia': deposit.company_id.sia_code,
            'cod_abi': deposit.bank_id.abi,
            'cod_cab': deposit.bank_id.cab,
            'cod_conto': deposit.bank_id.acc_number,
            'data_creaz': datetime.date.today(),
            'id_distinta': deposit.id,
            'riferimento': deposit.name,
            'rag_soc': p.name,
            'indirizzo': p.street,
            'localita': p.city,
            'cap_prov': p.zip + ' ' + p.state_id.code if p.state_id else '',
            'piva': p.vat,
        }

    def line_data_get(self, cr, uid, line):
        ml = line.move_line_id
        bk = line.debit_bank_id
        p = ml.partner_id or ml.move_id.partner_line_ids[0].partner_id
        for l in ml.move_id.partner_line_ids[0].reconcile_id.line_id:
            if l.id != ml.move_id.partner_line_ids[0].id:
                break
        else:
            assert False, 'Impossibile trovare la partita originaria!'
        inv = l.move_id
        return {
            'data_scad': parse(ml.date_maturity or ml.move_id.bill_date_due),
            'importo': line.amount,
            'cod_abi_cli': bk.abi,
            'cod_cab_cli': bk.cab,
            'denom_banca': bk.bank_name,
            'id_cliente': str(p.id),
            'rag_soc_cli': p.name,
            'piva_cli': p.vat,
            'indirizzo_cli': p.street,
            'localita_prov': p.city + ' ' + p.state_id.code if p.state_id else '',
            'cap_cli': p.zip,
            'is_banca_cli': False,
            'rif_fattura': l.name,
            'rif_effetto': ml.move_id.name,
        }

    def action_generate(self, cr, uid, deposit_id, context=None):
        cid = self.create(cr, uid, {'deposit_id': deposit_id}, context=context)
        self.generate_export_file(cr, uid, [cid], context=context)
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'account.voucher.deposit.export.cbi',
            'res_id': cid,
            'view_mode': 'form',
            'target': 'new',
            'name': 'Export CBI',
        }

