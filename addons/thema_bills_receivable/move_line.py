from openerp.osv import orm, fields
import time

class move_line(orm.Model):
    _inherit = 'account.move.line'

    _columns = {
        # bill receivable should be True for the line that debits the bill receivable account.
        'bill_receivable': fields.boolean('Effetto'),
        # bill deposit should be True for the line that credits the bill receivable account for a deposit.
        'bill_deposit': fields.boolean('Deposito effetto'),
        'cancel_account_id': fields.many2one('account.account', u'Conto per richiamo effetto'),
        'unpaid_account_id': fields.many2one('account.account', u'Conto per insoluto effetto'),
        'bill_orig_id': fields.many2one('account.move.line', 'Riapertura credito'),
        'bill_deposit_id': fields.function(lambda s, *a, **k: s._bill_deposit_id(*a, **k),
                                           type='many2one',
                                           relation='account.move.line',
                                           string='Voce versamento effetto'),
    }

    _constraints = [
        (
            lambda s, *a, **k: s._check_deposit_reconcile(*a, **k),
            u'La riconciliazione per versamento effetti deve essere fatta con una singola voce',
            ['reconcile_id']
        )
    ]

    def _check_deposit_reconcile(self, cr, uid, ids, context=None):
        cr.execute("""
            SELECT 1
            FROM account_move_line ml
            INNER JOIN account_move_reconcile r ON ml.reconcile_id = r.id
            INNER JOIN account_move_line ml2 ON ml2.reconcile_id = r.id AND ml2.id <> ml.id AND ml2.bill_deposit
            WHERE ml.bill_receivable
            AND ml2.id IN %s
            GROUP BY ml.id
            HAVING count(*) <> 1
        """, [tuple(ids)])
        return not bool(cr.fetchall())

    def _bill_deposit_id(self, cr, uid, ids, field, arg, context=None):
        cr.execute("""
            SELECT
              ml.id,
              ml2.id
            FROM account_move_line ml
            LEFT JOIN account_move_reconcile r ON r.id = ml.reconcile_id
            LEFT JOIN account_move_line ml2 ON ml2.reconcile_id = r.id
            WHERE ml.bill_receivable
            AND ml2.bill_deposit
            AND ml.id IN %s
        """, [tuple(ids)])
        return dict((id1, id2 if id2 else False) for id1, id2 in cr.fetchall())
