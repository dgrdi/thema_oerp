from openerp.osv import orm, fields
from openerp.addons import decimal_precision as dp
from devutils import knapsack


class deposit(orm.Model):
    _inherit = 'account.voucher'
    _name = 'account.voucher.deposit'
    _table = 'account_voucher_deposit'

    def __init__(self, *a, **kwargs):
        """
        Parent calls pool.get to get a reference to itself, breaking inheritance.
        """
        super(deposit, self).__init__(*a, **kwargs)

        class PoolWrapper(object):
            def __init__(self, obj):
                self._obj = obj

            def get(self, name, *a, **kw):
                if name == 'account.voucher':
                    name = 'account.voucher.deposit'
                return self._obj.get(name, *a, **kw)

            def __getattr__(self, item):
                return getattr(self._obj, item)

            def __getitem__(self, item):
                return self._obj[item]

        self.pool = PoolWrapper(self.pool)


    def _get_lines(obj, cr, uid, ids, context=None):
        cr.execute("""
            SELECT DISTINCT voucher_id FROM account_voucher_deposit_line WHERE id IN %s;
        """, [tuple(ids)])
        return [el[0] for el in cr.fetchall()]

    _columns = {
        'line_ids': fields.one2many('account.voucher.deposit.line', 'voucher_id', 'Voucher Lines',
                                    readonly=True, states={'draft': [('readonly', False)]}),
        'line_cr_ids': fields.one2many('account.voucher.deposit.line', 'voucher_id', 'Credits',
                                       domain=[('type', '=', 'cr')], context={'default_type': 'cr'}, readonly=True,
                                       states={'draft': [('readonly', False)]}),
        'line_dr_ids': fields.one2many('account.voucher.deposit.line', 'voucher_id', 'Debits',
                                       domain=[('type', '=', 'dr')], context={'default_type': 'dr'}, readonly=True,
                                       states={'draft': [('readonly', False)]}),
        'deposit_journal_id': fields.many2one('account.journal', 'Sezionale deposito',
                                              readonly=True,
                                              states={'draft': [('readonly', False)]},
        ),
        'bill_journal_id': fields.many2one('account.journal', string='Tipo effetti',
                                           domain=[('bill_receivable', '=', True)],
                                           readonly=True,
                                           states={'draft': [('readonly', False)]}),
        'bill_account_id': fields.many2one('account.account', string='Conto effetti'),
        'amount_target': fields.float('Total', digits_compute=dp.get_precision('Account'), required=False,
                                      readonly=True,
                                      states={'draft': [('readonly', False)]}),
        'amount': fields.function(lambda s, *a, **k: s._amount(*a, **k),
                                  string='Totale',
                                  type='float',
                                  digits_compute=dp.get_precision('Account'),
                                  store={
                                      'account.voucher.deposit.line': (_get_lines, ['amount'], 10)
                                  }, readonly=True),
        'account_id': fields.related('journal_id', 'default_debit_account_id', type='many2one',
                                     relation='account.account'),
        'bank_id': fields.many2one('res.partner.bank', 'Conto bancario per versamento', domain=[
            ('company_id', '!=', False), ('state', '=', 'ic')
        ]),
    }

    _defaults = {
        'type': 'receipt',
    }

    def _amount(self, cr, uid, ids, field, arg, context=None):
        cr.execute("""
            SELECT v.id, COALESCE(SUM(l.amount), 0::float)
            FROM account_voucher_deposit v
            LEFT JOIN account_voucher_deposit_line l ON v.id = l.voucher_id
            WHERE v.id IN %s
            GROUP BY v.id
        """, [tuple(ids)])
        return dict(cr.fetchall())

    def on_change_deposit(self, cr, uid, ids, bill_journal_id, amount_target, context=None):
        bill_journal = None
        if bill_journal_id:
            bill_journal = self.pool.get('account.journal').browse(cr, uid, bill_journal_id, context=context)
        val = {
            'bill_account_id': bill_journal and bill_journal.default_debit_account_id.id,
            'account_id': bill_journal and bill_journal.default_debit_account_id.id,
        }
        if bill_journal_id and amount_target:
            val['line_cr_ids'] = self.get_voucher_lines(cr, uid, bill_journal_id, amount_target, context=context)
        return {
            'value': val
        }

    def on_change_journal_id(self, cr, uid, ids, journal_id, context=None):
        val = {}
        if journal_id:
            bk = self.pool.get('res.partner.bank').search(cr, uid, [
                ('journal_id', '=', journal_id),
                ('state', '=', 'ic'),
            ])
            val['bank_id'] = bk[0] if len(bk) else False
        return {'value': val}

    def _get_line(self, cr, uid, move_line):
        part = move_line.partner_id or move_line.move_id.partner_line_ids[0].partner_id
        if move_line.move_id.debit_bank_id:
            bank = move_line.move_id.debit_bank_id.id
        else:
            if part.bank_ids:
                bank = part.bank_ids[0].id
            else:
                bank = False
        return {
            'name': move_line.move_id.name,
            'type': 'cr',
            'move_line_id': move_line.id,
            'account_id': move_line.account_id.id,
            'amount': move_line.debit,
            'date_original': move_line.date,
            'date_due': move_line.move_id.bill_date_due,
            'reconcile': True,
            'currency_id': move_line.currency_id.id,
            'debit_bank_id': bank,
            'partner_id': part.id,
        }

    def get_voucher_lines(self, cr, uid, bill_journal_id, amount_target, context=None):
        ml = self.pool.get('account.move.line')
        j = self.pool.get('account.journal').browse(cr, uid, bill_journal_id, context=context)
        res = []
        lines = ml.search(cr, uid, [
            ('journal_id', '=', bill_journal_id),
            ('debit', '>', 0),
            ('reconcile_id', '=', False),
            ('bill_receivable', '=', True),
            ('move_id.bill_state_current', '=', 'emitted'),
            ('debit', '<=', amount_target),
            ('account_id', '=', j.default_debit_account_id.id)
        ], context=context)
        lines = ml.read(cr, uid, lines, fields=['debit'], context=context)
        lines = [(l['id'], l['debit']) for l in lines]
        lines = knapsack.knapsack(lines, amount_target)
        if not lines:
            return []
        res = []
        for line in ml.browse(cr, uid, lines, context=context):
            res.append(self._get_line(cr, uid, line))
        return res

    def writeoff_move_line_get(self, *a, **kw):
        return {}

    def action_cancel_draft(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'draft'})

    def account_move_get(self, cr, uid, voucher_id, context=None):
        res = super(deposit, self).account_move_get(cr, uid, voucher_id, context=context)
        res.update({
            'bill_deposit': True,
        })
        return res

    def voucher_move_line_create(self, cr, uid, voucher_id, line_total,
                                 move_id, company_currency, current_currency, context=None):
        tot, rec_ids = super(deposit, self).voucher_move_line_create(cr, uid, voucher_id, line_total,
                                                                     move_id, company_currency, current_currency,
                                                                     context=context)
        ml = self.pool.get('account.move.line')
        m = self.pool.get('account.move')
        voucher = self.browse(cr, uid, voucher_id, context=context)
        for lines in rec_ids:
            old = ml.browse(cr, uid, lines[1], context=context)  # the original line is the second
            new = lines[:1] + lines[2:]
            ml.write(cr, uid, new, {
                'partner_id': old.partner_id.id,
            })
            cacc = voucher.journal_id.default_credit_account_id
            uacc = voucher.journal_id.rec_journal_id.default_credit_account_id
            ml.write(cr, uid, new[0], {
                'date_maturity': old.date_maturity or old.move_id.bill_date_due,
                'bill_deposit': True,
                'cancel_account_id': cacc and cacc.id or False,
                'unpaid_account_id': uacc and uacc.id or False,
            })
            # save a reference to the deposit in the original bill move
            m.write(cr, uid, old.move_id.id, {'bill_deposit_id': move_id}, context=context)
        return tot, rec_ids

    def action_export_cbi(self, cr, uid, ids, context=None):
        assert len(ids) == 1
        return self.pool.get('account.voucher.deposit.export.cbi').action_generate(cr, uid, ids[0], context=context)



class deposit_line(orm.Model):
    _inherit = 'account.voucher.line'
    _name = 'account.voucher.deposit.line'
    _table = 'account_voucher_deposit_line'

    _columns = {
        'voucher_id': fields.many2one('account.voucher.deposit', 'Voucher', required=1, ondelete='cascade'),
        'move_line_id': fields.many2one('account.move.line', 'Voce contabile', domain=[('bill_receivable', '=', True)]),
        'debit_bank_id': fields.many2one('res.partner.bank', 'Banca di addebito'),
        'partner_id': fields.many2one('res.partner', 'Partner'),
    }

    def onchange_move_line_id(self, cr, uid, ids, move_line_id, context=None):
        val = super(deposit_line, self).onchange_move_line_id(cr, uid, ids, move_line_id, context=context)
        if move_line_id:
            ml = self.pool.get('account.move.line').browse(cr, uid, move_line_id, context=context)
            val['value'].update(self.pool.get('account.voucher.deposit')._get_line(cr, uid, ml))
        return val

    def _add_missing_default_values(self, cr, uid, values, context=None):
        # when adding a voucher line manually, only the move line id is sent back on save -
        # don't know why, it works fine via get_voucher_lines
        line_id = values.get('move_line_id')
        if line_id:
            line = self.pool.get('account.move.line').browse(cr, uid, line_id, context=context)
            values.update(self.pool.get('account.voucher.deposit')._get_line(cr, uid, line))
        return super(deposit_line, self)._add_missing_default_values(cr, uid, values, context=None)
