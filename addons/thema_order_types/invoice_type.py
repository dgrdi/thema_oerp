from openerp.osv import orm, fields

class invoice_type(orm.Model):
    _inherit = 'sale_journal.invoice.type'
    _columns = {
        'type': fields.selection([('immediate', 'Immediata'), ('delayed', 'Differita')],
                                 string='Fatturazione', required=True)
    }

