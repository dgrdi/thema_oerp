__author__ = 'dgirardi'
from openerp.osv import orm, fields
from openerp import netsvc


class sale_order_origin(orm.Model):
    _name = 'sale.order.origin'

    _columns = {
        'name': fields.char('Nome'),
    }


class sale_order_type(orm.Model):
    _name = 'sale.order.type'

    _columns = {
        'name': fields.char('Nome'),
        'sequence_id': fields.many2one('ir.sequence', 'Numeratore'),
    }


class sale_order(orm.Model):
    _inherit = 'sale.order'

    _columns = {
        'order_type_id': fields.many2one('sale.order.type', 'Tipo ordine', required=False),
        'origin_id': fields.many2one('sale.order.origin', 'Origine'),
        'state': fields.selection([
                                      ('draft', 'Preventivo'),
                                      ('sent', 'Inviato'),
                                      ('blocked', 'Bloccato'),
                                      ('cancel', 'Annullato'),
                                      ('waiting_date', 'In attesa data'),
                                      ('progress', 'In evasione'),
                                      ('manual', 'In fatturazione'),
                                      ('shipping_except', 'Eccezione consegna'),
                                      ('invoice_except', 'Eccezione fattura'),
                                      ('sub_order', 'Sotto-ordine'),
                                      ('done', 'Evaso'),
                                  ], 'Stato', select=True)
    }


    # TODO: fix order state when delivery order is canceled

    def action_block(self, cr, uid, ids, context=None):
        wf = netsvc.LocalService('workflow')
        for id in ids:
            wf.trg_validate(uid, 'sale.order', id, 'order_block', cr)
        return True

    def action_unblock(self, cr, uid, ids, context=None):
        wf = netsvc.LocalService('workflow')
        for id in ids:
            wf.trg_validate(uid, 'sale.order', id, 'order_unblock', cr)
        return True

    def create(self, cr, user, vals, context=None):
        if vals.get('name', '/') == '/':
            ot_id = vals.get('order_type_id',
                             self.default_get(cr, user, fields_list=['order_type_id'], context=context)[
                                 'order_type_id'])
            ot = self.pool.get('sale.order.type').read(cr, user, ot_id, fields=['sequence_id'], context=context)[
                'sequence_id'][0]
            ctx = (context or {}).copy()
            if 'fiscalyear_id' not in ctx:
                ctx['fiscalyear_id'] = self.pool.get('account.fiscalyear').find(cr, user)
            if 'company_id' not in ctx:
                cid = ctx.get('force_company', vals.get('company_id', vals.get('order_company_id')))
                if not cid:
                    cid = self.pool.get('res.users').read(cr, user, user, fields=['company_id'])['company_id'][0]
                ctx['company_id'] = cid
            vals['name'] = self.pool.get('ir.sequence').next_by_id(cr, user, ot, context=ctx)
        return super(sale_order, self).create(cr, user, vals, context=context)

