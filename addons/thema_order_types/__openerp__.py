# -*- encoding: utf-8 -*-

{
    'name': 'Order types',
    'version': '0.1',
    'sequence': 1,
    'category': 'Custom',
    'complexity': "easy",
    'description': """ Tipi ordine """,
    'author': 'Thema Optical SRL',
    'website': 'http://www.thema-optical.com',
    'depends': [
        'sale',
        'account',
        'sale_journal',
        'thema_delivery_type',
        'sale_stock',
    ],
    'data': [
        'data/ir.model.access.csv',
        'data/order_types.xml',
        'data/origin.xml',
        'data/invoice_type.xml',
        'data/order_workflow.xml',
        'views/invoice_type.xml',
        'views/sale_order.xml',
    ],
    'demo_xml': [],
    'installable': True,
    'auto_install': True,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: