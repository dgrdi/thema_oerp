# -*- encoding: utf-8 -*-
##############################################################################
#
#    Copyright (c) 2013 ZestyBeanz Technologies Pvt. Ltd.
#    (http://wwww.zbeanztech.com)
#    contact@zbeanztech.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
import datetime

from jasper_product_report import JasperDataParser
from jasper_reports import jasper_report

from osv import fields, osv


class jasper_product_report(JasperDataParser.JasperDataParser):
        
    def __init__(self, cr, uid, ids, data, context):
        super(jasper_product_report, self).__init__(cr, uid, ids, data, context)
        self.sheet_names=[]
        
    def generate_data_source(self, cr, uid, ids, data, context):
        return 'records'

    
    def generate_parameters(self, cr, uid, ids, data, context):
        if data['report_type']=='xls':
            return {'IS_IGNORE_PAGINATION':True}
        return {}

    def generate_properties(self, cr, uid, ids, data, context):
        return {
            'net.sf.jasperreports.export.xls.one.page.per.sheet':'true',
            'net.sf.jasperreports.export.xls.sheet.names.all': '/'.join(self.sheet_names),
            'net.sf.jasperreports.export.ignore.page.margins':'true',
            'net.sf.jasperreports.export.xls.remove.empty.space.between.rows':'true'
            }
        
    def generate_records(self, cr, uid, ids, data, context):
        result = []
        internal_no = 0
        product_pool = self.pool.get('product.product')
        translation_pool = self.pool.get('ir.translation')
        prod_category_pool = self.pool.get('product.category')
        pricelist_pool = self.pool.get('product.pricelist')
        pricelist_ids = pricelist_pool.search(cr, uid, [('type', '=', 'sale')], context=context)
        if 'context' in data and data['context'] == 'wizard':
            product_ids = product_pool.search(cr, uid, [], context=context)
        else:
            product_ids = ids
        if pricelist_ids:
            for product_obj in product_pool.browse(cr, uid, product_ids, context=context):
                category_name = ''
                category = prod_category_pool.name_get(cr, uid, product_obj.categ_id.id, context=context)
                if category:
                    category_name = category[0][1]
                active = False
                if product_obj.active:
                    active = True
                
                internal_no += 1
                data = {
                    'internal_no':  internal_no,
                    'name': product_obj.name,
                    'category': category_name,
                    'product_code': product_obj.default_code,
                    'active': active,
                    'sale_ok': product_obj.sale_ok,
                    'price': pricelist_pool.price_get(cr, uid, [pricelist_ids[0]] ,
                    product_obj.id,  1.0, None, {
                        'uom': product_obj.uom_id.id,
                        'date': time.strftime('%Y-%m-%d'),
                        })[pricelist_ids[0]] or '0.00'
                    }
                result.append(data)
             
        self.sheet_names.append('Products')
        return result
    

jasper_report.report_jasper('report.jasper_product_report_test1', 'jasper.product.report.wizard', parser=jasper_product_report)
jasper_report.report_jasper('report.jasper_product_report_test2', 'product.product', parser=jasper_product_report)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
