# -*- encoding: utf-8 -*-
##############################################################################
#
#    Copyright (c) 2013 ZestyBeanz Technologies Pvt. Ltd.
#    (http://wwww.zbeanztech.com)
#    contact@zbeanztech.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields

import time

class product_report(osv.osv_memory):
    _name = 'jasper.product.report.wizard'
    _descrption = 'Wizard for Product report'
    
   
    def print_report(self, cr, uid, ids, context=None):
        data = self.read(cr, uid, ids,)[-1]
        report = {
            'type': 'ir.actions.report.xml',
            'report_name': 'jasper_product_report_test1',
            'datas': {
                'model':'jasper.product.report.wizard',
                'id': context.get('active_ids') and context.get('active_ids')[0] or False,
                'ids': context.get('active_ids') and context.get('active_ids') or [],
                'report_type': 'xls',
                'form': data,
                'context': 'wizard',
                },
            'nodestroy': False
            }
        return report
    
product_report()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: