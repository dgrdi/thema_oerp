# -*- encoding: utf-8 -*-
##############################################################################
#
#    Copyright (c) 2013 ZestyBeanz Technologies Pvt. Ltd.
#    (http://wwww.zbeanztech.com)
#    contact@zbeanztech.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Japer Products Report',
    'version': '1.2',
    'category': 'Custom',
    'sequence': 2,
    'description': """
    This module for Analyse the Jasper Report
    """,
    'author': 'ZestyBeanz Technologies Pvt Ltd',
    'website': 'http://www.openerp.com',
    'depends': ['jasper_reports','purchase'],
    'data': [
         'report/report.xml',
         'wizard/product_report_wizard_view.xml',
         ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': True,
    'application': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
