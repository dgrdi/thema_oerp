from openerp.tests.common import TransactionCase


class test_cc(TransactionCase):
    def setUp(self):
        super(test_cc, self).setUp()
        self.cc = self.registry('thema.cc')
        self.version = self.registry('thema.cc.version')
        self.item = self.registry('thema.cc.item')

    def test_date_fine(self):
        cc = self.cc.create(self.cr, self.uid, {
            'name': 'test',
            'version_ids': [
                (0, 0, {
                    'name': 'test1',
                    'date_start': '2000-01-01',
                    'date_stop': '2000-01-20',
                }),
                (0, 0, {
                    'name': 'test2',
                    'date_start': '2000-01-21',
                    'date_stop': False,
                })
            ]
        })

    def test_date_overlap(self):
        create = lambda: self.cc.create(self.cr, self.uid, {
            'name': 'test',
            'version_ids': [
                (0, 0, {
                    'name': 'test1',
                    'date_start': '2000-01-01',
                    'date_stop': False,
                }),
                (0, 0, {
                    'name': 'test2',
                    'date_start': '2000-01-21',
                    'date_stop': False,
                })
            ]
        })
        self.assertRaises(Exception, create)
