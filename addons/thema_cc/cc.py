#coding: utf-8
from collections import OrderedDict
from datetime import date
from openerp.osv import orm, fields, osv
from openerp.tools.misc import attrgetter
import json

class cc(orm.Model):
    _name = 'thema.cc'
    _description = 'Copia commissione'

    _columns = {
        'name': fields.char('Nome', required=True),
        'version_ids': fields.one2many('thema.cc.version', 'cc_id', 'Versioni'),
    }

    def get_definition(self, cr, uid, id, context=None):
        ver = self.pool.get('thema.cc.version').find(cr, uid, id, context=context)
        df = self.pool.get('thema.cc.version').read(cr, uid, ver, fields=('definition',), context=context)
        return json.loads(df['definition'])


class cc_version(orm.Model):
    _name = 'thema.cc.version'
    _description = 'Versione copia commissione'

    def _calc_def(self, cr, uid, ids, field_name, arg, context=None):
        return {i: json.dumps(self.get_definition(cr, uid, i, context)) for i in ids}

    def _recalc_def(obj, cr, uid, ids, context=None):
        context = context or {}
        if context.get('cc_no_recompute'):
            return []
        if not ids:
            return []
        cr.execute("SELECT DISTINCT version_id FROM thema_cc_item WHERE id IN %s", [tuple(ids)])
        res = cr.fetchall()
        if res:
            return zip(*res)[0]
        else:
            return []

    _columns = {
        'cc_id': fields.many2one('thema.cc', 'Copia commissione', required=True),
        'name': fields.char('Nome versione, come sul cartaceo', required=True),
        'date_start': fields.date('Data inizio validità'),
        'date_stop': fields.date('Data fine validità'),
        'item_ids': fields.one2many('thema.cc.item', 'version_id', 'Elementi'),
        'pricelist_id': fields.many2one('product.pricelist', 'Listino'),
        'reload_pricelist': fields.dummy(type='boolean'),
        'definition': fields.function(_calc_def,
                                      type='serialized',
                                      store={
                                          'thema.cc.item': (
                                              _recalc_def,
                                              ('product_id', 'product_tmpl', 'collection',
                                               'price', 'row_label', 'col_label'),
                                              10,
                                          )
                                      })

    }

    def _check_date_overlap(self, cr, uid, ids, context=None):
        if not ids: return True
        cr.execute("""
            SELECT 1
            FROM thema_cc_version v1
            INNER JOIN thema_cc_version v2 ON v1.cc_id = v2.cc_id AND v1.id <> v2.id
            WHERE
              v1.id IN %s
              AND (
                    COALESCE(v1.date_start, '0001-01-01') BETWEEN
                      COALESCE(v2.date_start, '0001-01-01') AND COALESCE(v2.date_stop, '9999-01-01')
                OR  COALESCE(v1.date_stop, '9999-01-01') BETWEEN
                      COALESCE(v2.date_start, '0001-01-01') AND COALESCE(v2.date_stop, '9999-01-01')
              )
            LIMIT 1;
        """, [tuple(ids)])
        return not cr.fetchall()

    _constraints = [
        (
            _check_date_overlap,
            'Esiste già una versione che copre queste date!',
            ('cc_id', 'date_start', 'date_stop')
        )
    ]


    def button_generate(self, cr, uid, ids, context=None):
        """
        Genera copia commissione usando tutti i prodotti non obsoleti
        che hanno un prezzo nel listino pricelist_id
        """
        context = context or {}
        pricelist_id = (context or {}).get('pricelist_id')
        if not pricelist_id:
            raise osv.except_osv('Errore!', 'Devi prima selezionare un listino.')
        product_obj = self.pool.get('product.product')
        products = product_obj.search(cr, uid, [('state', '=', 'sellable'), ('is_frame', '=', True)], context=context)
        items = self.pool.get('thema.cc.item').compute_items(cr, uid, products, pricelist_id, context=context)
        current = self.read(cr, uid, ids, fields=('item_ids', ), context=context)
        for el in current:
            self.pool.get('thema.cc.item').unlink(cr, uid, el['item_ids'], context=context)
        self.write(cr, uid, ids, {
            'item_ids': [(0, 0, i) for i in items]
        })
        return True

    def find(self, cr, uid, cc_id, context=None):
        context = context or {}
        dt = context.get('date', date.today())
        res = self.search(cr, uid, [
            ('cc_id', '=', cc_id),
            '|',
            ('date_start', '<', dt),
            ('date_start', '=', False),
            '|',
            ('date_stop', '>', dt),
            ('date_stop', '=', False)
        ], context=context)
        if not res:
            raise osv.except_osv('Errore!', u'La copia commissione non ha nessuna versione attiva '
                                            u'alla data specificata!')
        assert len(res) == 1, 'Più versioni che coprono le stesse date!'
        return res[0]

    def view_elements_tree(self, cr, uid, ids, context=None):
        if not ids:
            raise osv.except_osv('Errore!', 'Devi prima salvare.')
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'thema.cc.item',
            'view_mode': 'tree',
            'domain': [('version_id', '=', ids[0])],
            'name': 'Elementi copia commissione',
        }

    def get_definition(self, cr, uid, id, context=None):
        context = context or {}
        context['lang'] = 'en_US'
        ver = self.browse(cr, uid, id, context=context)
        res = {
            'id': ver.cc_id.id,
            'name': ver.cc_id.name,
            'version': ver.name,
            'version_id': ver.id,
            'collections': [],
        }
        items = sorted(ver.item_ids, key=attrgetter('product_tmpl'))
        items.sort(key=attrgetter('collection'))
        cur_col = {}
        cur_mod = {}
        models = []
        colls = res['collections']
        for itm in ver.item_ids:
            if cur_col.get('name') != itm.collection:
                cur_col = {
                    'name': itm.collection,
                    'models': []
                }
                models = cur_col['models']
                colls.append(cur_col)
            if cur_mod.get('name') != itm.product_tmpl:
                if cur_mod:
                    cur_mod['rows'] = sorted(list(cur_mod['rows']))
                    cur_mod['cols'] = sorted(list(cur_mod['cols']))
                    cur_mod['prices'] = sorted(list(cur_mod['prices']))
                cur_mod = {
                    'name': itm.product_tmpl,
                    'material': itm.product_id.material_id.name.lower().replace(' ', '') if itm.product_id.material_id else 'none',
                    'prices': set(),
                    'rows': set(),
                    'cols': set(),
                    'products': {}
                }
                models.append(cur_mod)
            cur_mod['rows'].add(itm.row_label)
            cur_mod['cols'].add(itm.col_label)
            cur_mod['prices'].add(itm.price)
            cur_mod['products'].setdefault(itm.row_label, {})[itm.col_label] = {
                'name': itm.product_id.name,
                'code': itm.product_id.default_code,
                'id': itm.product_id.id,
                'price': itm.price
            }
        if cur_mod:
            cur_mod['rows'] = sorted(list(cur_mod['rows']))
            cur_mod['cols'] = sorted(list(cur_mod['cols']))
            cur_mod['prices'] = sorted(list(cur_mod['prices']))
        return res


class cc_version_item(orm.Model):
    _name = 'thema.cc.item'

    _columns = {
        'version_id': fields.many2one('thema.cc.version', 'Versione copia commissione', required=True),
        'product_id': fields.many2one('product.product', 'Prodotto', required=True),
        'product_tmpl': fields.related('product_id', 'product_tmpl_id', 'name',
                                       string='Modello',
                                       type='char', readonly=True),
        'collection': fields.related('product_id', 'collection_id', 'name',
                                     string='Linea',
                                     type='char', readonly=True),
        'row_label': fields.char('Legenda riga', required=True),
        'col_label': fields.char('Legenda colonna', required=True),
        'price': fields.float('Prezzo', digits=(16,2)),
    }
    _sql_constraints = [
        (
            'uniq_prod',
            "UNIQUE (version_id, product_id)",
            'Lo stesso prodotto appare più di una volta!',
        ),
    ]
    # todo check uniqueness of template - row - column

    def action_recompute(self, cr, uid, context=None):
        context = context or {}
        ids = context.get('active_ids', [])
        self.recompute(cr, uid, ids, context)

    def recompute(self, cr, uid, ids, context=None):
        items = self.read(cr, uid, ids, fields=('product_id', 'version_id'), context=context)
        if not items:
            return True
        prod_by_version = {}
        for i in items:
            prod_by_version.setdefault(i['version_id'][0], []).append(i['product_id'][0])
        pricelists = self.pool.get('thema.cc.version').read(cr, uid, prod_by_version.keys(), fields=('pricelist_id', ))
        pricelists = {p['id']: p['pricelist_id'][0] if p['pricelist_id'] else None for p in pricelists}
        if None in pricelists.values():
            raise osv.except_osv(u'Errore!', u'Impossibile ricalcolare: per alcune versioni di copia commissione '
                                             u'non è definito un listino')
        self.unlink(cr, uid, ids, context=context)
        for version_id, product_ids in prod_by_version.items():
            items = self.compute_items(cr, uid, product_ids, pricelists[version_id], context=context)
            self.pool.get('thema.cc.version').write(cr, uid, version_id, {
                'item_ids': [(0,0,i) for i in items],
            })
        return True


    def compute_items(self, cr, uid, product_ids, pricelist_id, context=None):
        """
        :return: a list of dicts to as for create() for product_ids, using prices of pricelist_id.
                 the version_id must still be set.
        """
        pricelist_obj = self.pool.get('product.pricelist')
        product_obj = self.pool.get('product.product')

        prices = pricelist_obj.price_get_multi(cr, uid, [pricelist_id],
                                               [(p, 1, None) for p in product_ids],
                                               context=context)
        items = []
        def get_labels(p):
            row = p.color_id and p.color_id.ext_code or ''
            if p.is_reading:
                col = '+' + str(p.power)
            else:
                col = str(p.eye_size)
            return {
                'row_label': row,
                'col_label': col,
            }

        for p in product_obj.browse(cr, uid, product_ids, context=context):
            price = prices[p.id][pricelist_id]
            if price:
                itm = get_labels(p)
                itm.update({
                    'product_id': p.id,
                    'price': price,
                })
                items.append(itm)
        return items
