thcc_ccview = function(instance) {
    var _t = instance.web._t,
       _lt = instance.web._lt;
    var QWeb = instance.web.qweb;

    instance.thema_cc.CCView = instance.web.View.extend({
        template: 'thcc.CCView',
        view_type: 'cc',
        display_name: 'Copia commissione',
        start: function(){
            var self = this;
            this.cc = new instance.web.Model('thema.cc');
            return this._super().then(function(){
                return self.cc.call('name_search', [],
                                    {context: new instance.web.CompoundContext()}).then(function(res){
                    self.cclist = res;
                });
            });
        },
        do_show: function(){
            this._super();
            this.options.$sidebar.append(QWeb.render('thcc.Sidebar', {'cclist': this.cclist}));
            this.options.$sidebar.on('change', 'select.thcc_selectcc', this.do_select_cc);
            this.do_select_cc();
        },
        do_hide: function(){
            this._super();
            this.options.$sidebar.off('change', this.do_select_cc);
            this.options.$sidebar.html('');
        },
        do_select_cc: function(e){
            var cc_id = parseInt(this.options.$sidebar.find('select.thcc_selectcc').val());
            var self = this;
            this.cc.call('get_definition', [cc_id], {context: new instance.web.CompoundContext()}).then(function(def){
                self.ccwidget = new instance.thema_cc.CCWidget(self, def);
                self.$el.html('');
                self.ccwidget.appendTo(self.$el);
            });
        },
        do_search: function(domain, context, group_by){
            console.log(arguments);
            console.log(this.is_action_enabled('create'));
            console.log(this.is_action_enabled('edit'));
            console.log(this.is_action_enabled('delete'));
        }


    });
    instance.web.views.add('cc', 'instance.thema_cc.CCView');
};