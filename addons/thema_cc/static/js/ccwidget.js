thcc_ccwidget = function(instance){
    instance.thema_cc.CCWidget = instance.web.Widget.extend({
        template: 'thcc.CCWidget',

        init: function(parent, definition, options){
            this.definition = definition;
            this.options = options || {};
            this._super(parent);
        },
        start: function(){
            this.$el.find('.thcc_cc_position').each(function(){
                /**
                $(this).append('<div class="thcc_cc_value thcc_cc_value_1">1</div>');
                $(this).append('<div class="thcc_cc_value thcc_cc_value_2">2</div>');
                $(this).append('<div class="thcc_cc_value thcc_cc_value_3">3</div>');
                $(this).append('<div class="thcc_cc_value thcc_cc_value_4">4</div>');
                 **/
                $(this).append('<input type="text" class="thcc_cc_value thcc_cc_value_1" value="1" />');
                $(this).append('<input type="text" class="thcc_cc_value thcc_cc_value_2" value="2" />');
                $(this).append('<input type="text" class="thcc_cc_value thcc_cc_value_3" value="3" />');
                $(this).append('<input type="text" class="thcc_cc_value thcc_cc_value_4" value="4" />');
            });
            this._super();
        }
    })
};