from lxml import etree
from openerp.osv import orm, fields

class ir_ui_view(orm.Model):
    _inherit = 'ir.ui.view'

    def _type_field(self, cr, uid, ids, name, args, context=None):
        return super(ir_ui_view, self)._type_field(cr, uid, ids, name, args, context)

    _columns = {
        'type': fields.function(_type_field, type='selection', selection=[
            ('tree','Tree'),
            ('form','Form'),
            ('mdx','mdx'),
            ('graph', 'Graph'),
            ('calendar', 'Calendar'),
            ('diagram','Diagram'),
            ('gantt', 'Gantt'),
            ('kanban', 'Kanban'),
            ('cc', 'Order copy'),
            ('search','Search')], string='View Type', required=True, select=True, store=True),

    }