{
    'name': 'Thema - lingue',
    'version': '0.1',
    'category': '',
    'description': """Traduzione da lingua non inglese""",
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': ['base'],
    'init_xml': [],
    'data': [
    ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': True,
}
