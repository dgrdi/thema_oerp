from openerp.osv import orm, osv
from openerp.tools.translate import _

class ir_translation(orm.Model):
    _inherit = 'ir.translation'

    def translate_fields(self, cr, uid, model, id, field=None, context=None):
        trans_model = self.pool.get(model)
        domain = ['&', ('res_id', '=', id), ('name', '=like', model + ',%')]
        langs_ids = self.pool.get('res.lang').search(cr, uid, [], context=context)
        if not langs_ids:
            raise osv.except_osv(_('Error'), _("Translation features are unavailable until you install an extra OpenERP translation."))
        langs = [lg.code for lg in self.pool.get('res.lang').browse(cr, uid, langs_ids, context=context)]
        main_lang = 'en_US'
        translatable_fields = []
        for f, info in trans_model._all_columns.items():
            if info.column.translate:
                if info.parent_model:
                    parent_id = trans_model.read(cr, uid, [id], [info.parent_column], context=context)[0][info.parent_column][0]
                    translatable_fields.append({ 'name': f, 'id': parent_id, 'model': info.parent_model })
                    domain.insert(0, '|')
                    domain.extend(['&', ('res_id', '=', parent_id), ('name', '=', "%s,%s" % (info.parent_model, f))])
                else:
                    translatable_fields.append({ 'name': f, 'id': id, 'model': model })
        if len(langs):
            fields = [f.get('name') for f in translatable_fields]
            record = trans_model.read(cr, uid, [id], fields, context={ 'lang': main_lang })[0]
            for lg in langs:
                for f in translatable_fields:
                    # Check if record exists, else create it (at once)
                    sql = """INSERT INTO ir_translation (lang, src, name, type, res_id, value)
                        SELECT %s, %s, %s, 'model', %s, %s WHERE NOT EXISTS
                        (SELECT 1 FROM ir_translation WHERE lang=%s AND name=%s AND res_id=%s AND type='model');
                        UPDATE ir_translation SET src = %s WHERE lang=%s AND name=%s AND res_id=%s AND type='model';
                        """
                    src = record[f['name']] or None
                    name = "%s,%s" % (f['model'], f['name'])
                    cr.execute(sql, (lg, src , name, f['id'], src, lg, name, f['id'], src, lg, name, id))

        action = {
            'name': 'Translate',
            'res_model': 'ir.translation',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'domain': domain,
        }
        if field:
            info = trans_model._all_columns[field]
            action['context'] = {
                'search_default_name': "%s,%s" % (info.parent_model or model, field)
            }
        return action
