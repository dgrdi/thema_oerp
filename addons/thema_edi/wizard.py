from openerp.osv import orm, fields
import base64

class edi_wizard(orm.TransientModel):
    _name = 'thema.edi.wizard'

    _columns = {
        'name': fields.char('Nome'),
        'file_ids': fields.one2many('thema.edi.wizard.file', 'wizard_id', 'File', readonly=True),
    }

    def encode(self, cr, uid, encoder_name, record_ids, context=None):
        res = self.pool.get('thema.edi.encoder').encode(cr, uid, encoder_name, record_ids, context=context)
        files = [(0, 0, {'name': k, 'file': base64.b64encode(v)}) for k, v in res.items()]
        cid = self.create(cr, uid, {'name': encoder_name, 'file_ids': files}, context=context)
        return {
            'type': 'ir.actions.act_window',
            'target': 'new',
            'res_model': 'thema.edi.wizard',
            'res_id': cid,
            'view_mode': 'form',
            'name': 'Esporta dati',
        }

class edi_wizard_file(orm.TransientModel):
    _name = 'thema.edi.wizard.file'

    _columns = {
        'wizard_id':fields.many2one('thema.edi.wizard', 'Wizard'),
        'name': fields.char('Nome'),
        'file': fields.binary('File'),
    }