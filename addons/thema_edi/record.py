
class Record(object):
    def __init__(self, records):
        self._records = records

    def __getattr__(self, item):
        return self[item]

    def __getitem__(self, item):
        if item in self._records:
            return self._records[item]
        return self._records[None][item]

class RecordIterator(object):

    def __init__(self, records, join):
        self.records = records
        self.join = join

    def __iter__(self):
        for r in self.records:
            if not self.join:
                yield r
            else:
                for rnest in RecordIterator(r[self.join[0]], self.join[1:]):
                    yield Record({None: r, self.join[0]: rnest})
