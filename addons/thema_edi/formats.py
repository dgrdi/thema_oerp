from openerp.osv import orm, fields
import unidecode
from openerp.tools import ormcache
from devutils.tools import flatten_browse
from .formatter import Spec, Formatter
from openerp.tools.safe_eval import safe_eval
import csv
from StringIO import StringIO
from .record import RecordIterator
from csv import excel
from _csv import Error, __version__, writer, reader, register_dialect, \
                 unregister_dialect, get_dialect, list_dialects, \
                 field_size_limit, \
                 QUOTE_MINIMAL, QUOTE_ALL, QUOTE_NONNUMERIC, QUOTE_NONE

class edi_cleaner(orm.Model):
    """
    A 'cleaner' is a function taking one argument whose responsibility is to "clean up" that argument before formatting.
    For example, the 'unidecode' cleaner strips all non-ascii characters if the value is a string.
    If a cleaner has a parent, it will be composed with it (the parent will be called first, and the cleaner will be
    called with the parent's result as argument).
    """
    _name = 'thema.edi.cleaner'

    _columns = {
        'name': fields.char('Nome', required=True),
        'parent_id': fields.many2one('thema.edi.cleaner', 'Genitore'),
        'code': fields.text('Codice', required=True),
        'cleaner': fields.function(lambda s, *a, **k: s._cleaner(*a, **k), type='other', string='Cleaner'),
    }

    @ormcache(skiparg=2)
    def get(self, cr, uid, id):
        obj = self.browse(cr, uid, id)
        glob, loc = {'unidecode': unidecode}, {}
        exec obj.code in glob, loc
        res = cleaner = loc['cleaner']
        if obj.parent_id:
            res = lambda x: cleaner(obj.parent_id.cleaner(x))
        return res

    def _cleaner(self, cr, uid, ids, field, arg, context=None):
        res = {}
        for id in ids:
            res[id] = self.get(cr, uid, id)
        return res

    def write(self, cr, user, ids, vals, context=None):
        self.get.clear_cache(self)
        return super(edi_cleaner, self).write(cr, user, ids, vals, context=context)

    def unlink(self, cr, uid, ids, context=None):
        self.get.clear_cache(self)
        return super(edi_cleaner, self).write(cr, uid, ids, context=context)


class edi_formatter(orm.Model):
    """
    A formatter is an object that knows how to format one record. The record's fields are formatted using specs,
    and they are then joined together with the field and line separators defined in the formatter, if the file
    is not a csv file. (If it is, the separators are ignored).
    """

    _name = 'thema.edi.formatter'

    _columns = {
        'field_sep': fields.char('Separatore campi'),
        'line_sep': fields.char('Separatore righe'),
        'spec_ids': fields.one2many('thema.edi.spec', 'formatter_id', 'Specifiche'),
    }

    _defaults = {
        'field_sep': r'',
        'line_sep': r'\r\n',
    }

    def get(self, cr, uid, id, params, csv, context=None):
        obj = self.browse(cr, uid, id, context=context)
        specs = [(s.field_path, s.spec) for s in obj.spec_ids]
        return Formatter(specs,
                         obj.field_sep and obj.field_sep.decode('string_escape') or '',
                         obj.line_sep and obj.line_sep.decode('string_escape') or '',
                         parameters=params, csv=csv)


class edi_spec(orm.Model):
    """
    A spec is an object that knows how to format one field. Given a record (an orm.browse_record instance), the spec
    will:
     - find the field value, by following 'field_path' - a dot-separated list of fields names;
     - if the value is missing (because the field itself is null, or one of the many2ones leading to it is), take
        'default' (evaluated as a python expression) as the value
     - clean up the value using the cleaner defined in 'cleaner_id'
     - wrap the value in a container class that understands some additional format specs
     - format the value by calling .format(value, **params) on the format string defined in 'format'. `params` is
       defined in the encoder.

     The additional format specs are:
      - for all values, 'l(number)(l|r)' at the end of the format spec will truncate the output to 'number' characters,
      keeping the ones on the (l)eft or the (r)ight.
      - for numbers, a ',' at the end of the spec will replace periods with commas.

    """
    _name = 'thema.edi.spec'
    _order = 'sequence, id'

    _columns = {
        'formatter_id': fields.many2one('thema.edi.formatter', 'Formatter', required=True, ondelete="cascade"),
        'sequence': fields.integer('Sequence'),
        'name': fields.char('Nome', required=True),
        'description': fields.char('Descrizione'),
        'field_path': fields.char('Nome campo'),
        'cleaner_id': fields.many2one('thema.edi.cleaner', 'Cleaner', required=True),
        'format': fields.char('Formato', required=True),
        'spec': fields.function(lambda s, *a, **k: s._spec(*a, **k), type='other', string='Spec'),
        'default': fields.char('Valore di default'),
    }

    def _spec(self, cr, uid, ids, field, arg, context=None):
        res = {}
        for s in self.browse(cr, uid, ids, context=context):
            df = safe_eval(s.default) if s.default is not False else None
            res[s.id] = Spec(s.cleaner_id.cleaner, s.format, df)
        return res

    _defaults = {
        'sequence': 10,
    }


class edi_encoder(orm.Model):
    _name = 'thema.edi.encoder'

    _columns = {
        'name': fields.char('Nome', required=True),
        'code': fields.char('Codice', required=True),
        'model': fields.char('Modello', required=True),
        'method': fields.char('Metodo', required=True),
        'file_ids': fields.one2many('thema.edi.file', 'encoder_id', 'File'),
        'param_ids': fields.one2many('thema.edi.parameter', 'encoder_id', 'Parametri'),
    }
    _sql_constraints = [('uniq_code', "UNIQUE(code)", 'Il codice del codificatore deve essere univoco!')]

    def get_files(self, files):
        groups = {}
        for f in files:
            groups.setdefault(f.group or None, []).append(f)
        res = [(f, f.sequence) for f in groups.pop(None, [])]
        res += [(g, min(f.sequence for f in g)) for g in groups.values()]
        res.sort(key=lambda x: x[1])
        return [el[0] for el in res]

    def tally(self, file, values, data):
        if file.csv:
            o = StringIO()
            wr = csv.writer(o, dialect=file.csv)
            if file.name not in data and file.csv_headers:
                header = [el.name for el in file.formatter_id.spec_ids]
                wr.writerow(header)
            wr.writerows(values)
            data[file.name] = data.get(file.name, '') + o.getvalue()
        else:
            values = ''.join([''.join(row) for row in values])
            data[file.name] = data.get(file.name, '') + values

    def encode(self, cr, uid, encoder_name, record_ids, context=None):
        if not record_ids:
            return {}
        enc = self.search(cr, uid, [('code', '=', encoder_name)], context=context)
        if not enc:
            raise RuntimeError('Encoder not found: %s' % encoder_name)
        obj = self.browse(cr, uid, enc[0], context=context)
        if obj.method:
            records = getattr(self.pool.get(obj.model), obj.method)(cr, uid, record_ids, context=context)
        else:
            records = self.pool.get(obj.model).b_browse(cr, uid, record_ids, context=context)
        records = list(records)
        files = {}
        edi_file = self.pool.get('thema.edi.file')
        formatters = {}
        for file_or_group in self.get_files(obj.file_ids):
            if isinstance(file_or_group, list):
                for r in records:
                    for f in file_or_group:
                        values = edi_file.encode(cr, uid, f, obj, [r], formatter_cache=formatters, context=context)
                        self.tally(f, values, files)
            else:
                values = edi_file.encode(cr, uid, file_or_group, obj, records, formatter_cache=formatters,
                                         context=context)
                self.tally(file_or_group, values, files)
        return files


class edi_parameter(orm.Model):
    _name = 'thema.edi.parameter'
    _columns = {
        'encoder_id': fields.many2one('thema.edi.encoder', ondelete='cascade', string='Encoder'),
        'name': fields.char('Nome'),
        'value': fields.char('Valore'),
    }


class edi_file(orm.Model):
    _name = 'thema.edi.file'
    _order = 'sequence'

    _columns = {
        'encoder_id': fields.many2one('thema.edi.encoder', 'Codificatore', required=True),
        'sequence': fields.integer('Progressivo', required=True),
        'name': fields.char('Nome', required=True),
        'datasource': fields.char('Sorgente dati'),
        'datasource_type': fields.selection([('field', 'Campo oggetto'), ('method', 'Metodo oggetto')],
                                            'Tipo sorgente dati', required=True),
        'join': fields.char('Join expr'),
        'group': fields.char('Group expr'),
        'formatter_id': fields.many2one('thema.edi.formatter', 'Formatter'),
        'csv': fields.selection(lambda s, *a, **k: s._csv_dialects(*a, **k), string='Formato CSV'),
        'csv_headers': fields.boolean('Riga di header in csv'),
    }

    _defaults = {
        'sequence': 10,
        'datasource_type': 'field',
    }

    def _csv_dialects(self, *a, **k):
        return [(name, name) for name in csv.list_dialects()]

    class fixedcsv(excel):
        quoting = QUOTE_ALL
    register_dialect("fixedcsv", fixedcsv)


    def get_records(self, cr, uid, file, model, records, context=None):
        if file.datasource and file.datasource_type == 'field':
            dsource = file.datasource.split('.')
            dsource.reverse()
            while dsource:
                f = dsource.pop()
                records = flatten_browse(r[f] for r in records)
        elif file.datasource and file.datasource_type == 'method':
            meth = getattr(self.pool.get(model), file.datasource)
            records = meth(cr, uid, records, context=context)
        return records

    def encode(self, cr, uid, file, encoder, records, formatter_cache=None, context=None):
        params = {p.name: p.value for p in encoder.param_ids}
        records = self.get_records(cr, uid, file, encoder.model, records, context=context)
        records = RecordIterator(records, file.join.split('.') if file.join else None)
        if formatter_cache and file.formatter_id.id in formatter_cache:
            formatter = formatter_cache.get[file.formatter_id.id]
        else:
            formatter = self.pool.get('thema.edi.formatter').get(cr, uid, file.formatter_id.id, params, file.csv,
                                                                 context=context)
            if formatter_cache:
                formatter_cache[file.formatter_id.id] = formatter

        return map(formatter.format, records)


