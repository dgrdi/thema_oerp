{
    'name': 'Thema - EDI',
    'version': '0.1',
    'category': '',
    'description': """Electronic data interchange""",
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': ['base'],
    'init_xml': [],
    'data': [
        'data/ir.model.access.csv',
        'data/cleaners.xml',
        'wizard.xml',
    ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': True,
}
