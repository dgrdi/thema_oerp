import re
from decimal import Decimal
from logging import getLogger

logger = getLogger(__name__)


class Number(object):
    @property
    def int(self):
        return Frapper(nint(self))

    @property
    def dec(self):
        p = str(self).split('.')
        if len(p) == 1:
            return Frapper(nint(0))
        else:
            return Frapper(nint(p[1]))

    def __format__(self, format_spec):
        if format_spec.endswith(','):
            val = super(Number, self).__format__(format_spec[:-1])
            val = val.replace('.', ',')
        else:
            val = super(Number, self).__format__(format_spec)
        return val


class String(unicode):
    slice_rx = re.compile('^slice_(\d+)$')

    def __getattr__(self, item):
        m = self.slice_rx.match(item)
        if m:
            n = int(m.group(1))
            return Frapper(String(self[n:]))
        else:
            return super(String, self).__getattribute__(item)


nint = type('nint', (Number, int), {})
nfloat = type('nfloat', (Number, float), {})
nDecimal = type('nDecimal', (Number, Decimal), {})


class Spec(object):
    NUMBERS = {
        int: nint,
        float: nfloat,
        Decimal: nDecimal,
    }

    def __init__(self, cleaner, fmt, default):
        self.cleaner = cleaner
        self.fmt = fmt
        self.default = default

    def format(self, value, params=None):
        params = params or {}
        value = self.cleaner(value)
        if value is None or value is False:
            value = self.default if self.default is not None else u''
        value = self.NUMBERS.get(type(value))(value) if type(value) in self.NUMBERS else value
        value = String(value) if isinstance(value, (str, unicode)) else value
        return self.fmt.format(Frapper(value), **params)


exf = re.compile("""
   ^
   (.*)     # the usual format spec, that will be passed to the object
   l        # the 'l' stands for length
   (\d+)    # the maximum length
   (l|r)    # whether to keep the (l)eft or (r)ight characters if we have to truncate
   $
""", re.VERBOSE)


class Frapper(object):
    __slots__ = ['value']

    def __init__(self, value):
        self.value = value

    def __format__(self, format_spec):
        m = exf.match(format_spec)
        val = object.__getattribute__(self, 'value')
        if m:
            spec, length, dir = m.groups()
            length = int(length)
            try:
                v = val.__format__(spec)
            except ValueError:
                logger.exception('Error formatting value %s with spec %s (value type: %s)', val, spec, type(val))
                raise
            v = v[:length] if dir == 'l' else v[-length:]
        else:
            try:
                v = val.__format__(format_spec)
            except ValueError:
                logger.exception('Error formatting value %s with spec %s (value type: %s)', val, format_spec, type(val))
                raise
        return v

    def __getattribute__(self, item):
        if item == '__format__':
            return object.__getattribute__(self, '__format__')
        else:
            return getattr(object.__getattribute__(self, 'value'), item)

    def __getitem__(self, item):
        return object.__getattribute__(self, 'value')[item]


class Formatter(object):
    def __init__(self, specs, separator='', line_sep='\r\n', parameters=None, csv=False):
        self.specs = specs
        self.separator = separator
        self.line_sep = line_sep
        self.parameters = parameters
        self.csv = csv

    def getter(self, obj):
        def getter(name):
            if not name:
                return None
            name = name.split('.')
            name.reverse()
            val = obj
            while name:
                n = name.pop()
                try:
                    val = val[n]
                except (IndexError, AttributeError) as e:
                    logger.debug('Cannot fetch %s, returning None (original exception: %s)', n, e)
                    return None
                if not val:
                    break
            return val

        return getter


    def format(self, record):
        """
        :param record: a dictionary or object whose keys or attribute map to the field value for the record
        :return: a formatted line
        """
        getter = self.getter(record)

        def formatter(name, spec):
            try:
                if name  and "{" in name:
                    #considero prima l'espressione
                    name=name.replace("{", "getter(\"")
                    name=name.replace("}", "\")")
                    res=eval(name)
                    return spec.format(res, self.parameters)
                else:
                    return spec.format(getter(name), self.parameters)
            except ValueError:
                logger.error('Error during formatting of field path %s, spec index %s', name,
                             [el[1] for el in self.specs].index(spec))
                raise

        line = [formatter(name, spec) for name, spec in self.specs]
        line = map(lambda x: x.encode('utf-8'), line)
        if self.csv:
            return line
        else:
            return self.separator.join(line) + self.line_sep

