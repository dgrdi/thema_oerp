{
    'name': 'CORS',
    'version': '0.2',
    'category': 'Development',
    'description': """Set Access-Control-Allow-Origin on http responses.""",
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': [],
    'init_xml': [],
    'data':[
        ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': True,
}
