from openerp.addons.web.http import Root
import functools

orig_dispatch = Root.__call__

@functools.wraps(Root.dispatch)
def cors_dispatch(self, environ, start_response):
    def wrap_start_response(status, headers):
        headers.extend([
            ('Access-Control-Allow-Origin', environ.get('HTTP_ORIGIN')),
            ('Access-Control-Allow-Methods', 'POST, GET, OPTIONS'),
            ('Access-Control-Allow-Credentials', 'true'),
        ])
        return start_response(status, headers)
    return orig_dispatch(self, environ, wrap_start_response)

Root.__call__ = cors_dispatch
