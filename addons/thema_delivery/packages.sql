DO LANGUAGE plpgsql $$
BEGIN
  IF NOT exists(SELECT
                  1
                FROM information_schema.schemata
                WHERE schema_name = 'delivery')
  THEN
    CREATE SCHEMA delivery;
  END IF;
END
$$;


CREATE OR REPLACE FUNCTION delivery.coerce_package(package_number INT)
  RETURNS INT SET search_path TO delivery, PUBLIC
AS $b$
BEGIN
  RETURN coalesce(CASE package_number
                  WHEN 0 THEN 1
                  ELSE package_number END, 1) % 1000;
END
$b$ LANGUAGE plpgsql IMMUTABLE;


CREATE OR REPLACE FUNCTION delivery.package_id(delivery_id INT, package_number INT)
  RETURNS INT8 SET search_path TO delivery, PUBLIC AS $b$
BEGIN
  RETURN delivery_id :: INT8 * 1000 + coerce_package(package_number);
END
$b$ LANGUAGE plpgsql IMMUTABLE;


CREATE OR REPLACE FUNCTION delivery.box_id(delivery_id INT, package_number INT, box_number INT)
  RETURNS INT8 SET search_path TO delivery, PUBLIC AS $b$
BEGIN
  RETURN package_id(delivery_id, package_number) * 1000 + coalesce(box_number, 0);
END
$b$ LANGUAGE plpgsql IMMUTABLE;

DO LANGUAGE plpgsql $$
BEGIN
  PERFORM 'public.idx_package' :: REGCLASS;
  EXCEPTION
  WHEN OTHERS THEN
    CREATE INDEX idx_package ON stock_move (delivery.package_id(delivery_id, package))
      WHERE delivery_id IS NOT NULL;

END
$$;

DO LANGUAGE plpgsql $$
BEGIN
  PERFORM 'public.idx_box' :: REGCLASS;
  EXCEPTION
  WHEN OTHERS THEN
    CREATE INDEX idx_box ON stock_move (delivery.box_id(delivery_id, package, box))
      WHERE delivery_id IS NOT NULL;

END
$$;

CREATE TABLE IF NOT EXISTS delivery_package (
  id          INT8 PRIMARY KEY,
  delivery_id INT4 REFERENCES delivery_delivery (id) ON DELETE CASCADE,
  number      INT
);

CREATE TABLE IF NOT EXISTS delivery_box (
  id          INT8 PRIMARY KEY,
  package_id  INT8 REFERENCES delivery_package (id) ON DELETE CASCADE ,
  delivery_id INT4 REFERENCES delivery_delivery (id) ON DELETE CASCADE,
  number      INT
);


DO LANGUAGE plpgsql $$
BEGIN
  PERFORM 'public.idx_box_package' :: REGCLASS;
  EXCEPTION
  WHEN OTHERS THEN
    CREATE INDEX idx_box_package ON delivery_box (package_id);
END
$$;

DO LANGUAGE plpgsql $$
BEGIN
  PERFORM 'public.idx_box_delivery' :: REGCLASS;
  EXCEPTION
  WHEN OTHERS THEN
    CREATE INDEX idx_box_delivery ON delivery_box (delivery_id);
END
$$;

/**
  Packages
 */
CREATE OR REPLACE FUNCTION delivery.fn_update_packages(p_delivery_id INT, p_number_of_packages INT)
  RETURNS VOID SET search_path TO delivery, PUBLIC AS $b$
BEGIN
  DELETE FROM delivery_package
  WHERE delivery_id = p_delivery_id AND number > coerce_package(p_number_of_packages);
  INSERT INTO delivery_package (id, delivery_id, number)
    SELECT
      package_id(p_delivery_id, n),
      p_delivery_id,
      n
    FROM generate_series(1, least(coerce_package(p_number_of_packages), 999)) n
      LEFT JOIN delivery_package pk ON package_id(p_delivery_id, n) = pk.id
    WHERE pk.id IS NULL;
END
$b$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION delivery.fn_update_all_packages()
  RETURNS VOID SET search_path TO delivery, PUBLIC AS $b$
BEGIN
  DELETE FROM delivery_package
  WHERE delivery_id NOT IN (SELECT
                              id
                            FROM delivery_delivery);
  DELETE FROM delivery_package
  USING delivery_delivery d
  WHERE d.id = delivery_package.delivery_id
        AND delivery_package.number > coerce_package(d.number_of_packages);
  INSERT INTO delivery_package (id, delivery_id, number)
    SELECT
      package_id(d.id, n),
      d.id,
      n
    FROM delivery_delivery d
      CROSS JOIN generate_series(1, least(coerce_package(d.number_of_packages), 999)) n
      LEFT JOIN delivery_package p ON p.id = package_id(d.id, n)
    WHERE p.id IS NULL;

END
$b$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION delivery.trg_fn_post()
  RETURNS TRIGGER SET search_path TO delivery, PUBLIC AS $b$
BEGIN
  PERFORM fn_update_packages(NEW.id, NEW.number_of_packages);
  RETURN NULL;
END
$b$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION delivery.trg_fn_before()
  RETURNS TRIGGER SET search_path TO delivery, PUBLIC AS $b$
BEGIN
  DELETE FROM delivery_package
  WHERE delivery_id = OLD.id;
  IF TG_OP = 'DELETE'
  THEN
    RETURN OLD;
  END IF;
  RETURN NEW;
END
$b$ LANGUAGE plpgsql VOLATILE;


/**
  Boxes
 */


CREATE OR REPLACE FUNCTION delivery.fn_update_all_boxes()
  RETURNS VOID AS $b$
BEGIN

END
$b$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION delivery.fn_update_all_boxes()
  RETURNS VOID SET search_path TO delivery, PUBLIC AS $b$
BEGIN
  DELETE FROM delivery_box
  WHERE package_id NOT IN (SELECT
                             delivery.package_id(delivery_id, package)
                           FROM stock_move
                           WHERE delivery_id IS NOT NULL);
  WITH mx AS (
      SELECT
        package_id(s.delivery_id, s.package) pid,
        COALESCE(max(box), 0)                mx
      FROM stock_move s
      WHERE delivery_id IS NOT NULL
      GROUP BY package_id(s.delivery_id, s.package)
  ) DELETE FROM delivery_box
  USING mx
  WHERE mx.pid = delivery_box.id AND delivery_box.number > mx.mx;

  WITH mx AS (
      SELECT
        s.delivery_id,
        s.package,
        package_id(s.delivery_id, s.package) pid,
        COALESCE(max(box), 0)                mx
      FROM stock_move s
      WHERE delivery_id IS NOT NULL
      GROUP BY s.delivery_id, s.package, package_id(s.delivery_id, s.package)
  ) INSERT INTO delivery_box (id, package_id, delivery_id, number)
    SELECT
      box_id(mx.delivery_id, mx.package, n),
      mx.pid,
      mx.delivery_id,
      n
    FROM mx
      CROSS JOIN generate_series(0, mx.mx) n
      LEFT JOIN delivery_box bx ON bx.delivery_id = mx.delivery_id
                                   and mx.pid = bx.package_id and bx.number = n
    WHERE bx.id IS NULL;


END
$b$ LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION delivery.fn_update_boxes(t_delivery_id INT, t_package INT)
  RETURNS VOID SET search_path TO delivery, PUBLIC AS $b$
DECLARE
  mx INT;
BEGIN
  SELECT
    COALESCE(max(box), 0)
  FROM stock_move
  WHERE delivery_id = t_delivery_id
        AND coerce_package(package) = coerce_package(t_package)
  INTO mx;

  DELETE FROM delivery_box
  WHERE package_id = package_id(t_delivery_id, t_package) AND number > mx;

  INSERT INTO delivery_box (id, package_id, delivery_id, number)
    SELECT
      box_id(t_delivery_id, t_package, n),
      package_id(t_delivery_id, t_package),
      t_delivery_id,
      n
    FROM generate_series(0, mx) n
      LEFT JOIN delivery_box bx ON bx.id = box_id(t_delivery_id, t_package, n)
    WHERE bx.id IS NULL;
END
$b$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION delivery.fn_trg_before_box()
  RETURNS TRIGGER SET search_path TO delivery, PUBLIC AS $b$
BEGIN
  DELETE FROM delivery_box
  WHERE package_id = delivery.package_id(OLD.delivery_id, OLD.package);
  IF TG_OP = 'DELETE'
  THEN
    RETURN OLD;
  END IF;
  RETURN NEW;
END
$b$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION delivery.fn_trg_after_box()
  RETURNS TRIGGER SET search_path TO delivery, PUBLIC AS $b$
BEGIN
  PERFORM delivery.fn_update_boxes(NEW.delivery_id, NEW.package);
  RETURN NULL;
END
$b$ LANGUAGE plpgsql VOLATILE;

/**
  Setup
 */
CREATE OR REPLACE FUNCTION delivery.fn_enable_triggers()
  RETURNS VOID SET search_path TO delivery, PUBLIC AS $b$
BEGIN
  CREATE TRIGGER trg_post AFTER INSERT OR UPDATE OF number_of_packages
  ON delivery_delivery
  FOR EACH ROW
  EXECUTE PROCEDURE delivery.trg_fn_post();

  CREATE TRIGGER trg_before_del BEFORE DELETE ON delivery_delivery
  FOR EACH ROW EXECUTE PROCEDURE delivery.trg_fn_before();

  CREATE TRIGGER trg_post_box AFTER INSERT OR UPDATE OF package, delivery_id, box
  ON stock_move
  FOR EACH ROW
  WHEN (NEW.delivery_id IS NOT NULL)
  EXECUTE PROCEDURE delivery.fn_trg_after_box();

  CREATE TRIGGER trg_pre_box BEFORE DELETE OR UPDATE OF package, delivery_id
  ON stock_move
  FOR EACH ROW
  WHEN (OLD.delivery_id IS NOT NULL)
  EXECUTE PROCEDURE delivery.fn_trg_before_box();

END
$b$ LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION delivery.fn_disable_triggers()
  RETURNS VOID SET search_path TO delivery, PUBLIC AS $b$
BEGIN
  DROP TRIGGER IF EXISTS trg_post ON delivery_delivery;
  DROP TRIGGER IF EXISTS trg_before_del ON delivery_delivery;
  DROP TRIGGER IF EXISTS trg_post_box ON stock_move;
  DROP TRIGGER IF EXISTS trg_pre_box ON stock_move;
END
$b$ LANGUAGE plpgsql;

/*
SELECT
  delivery.fn_update_all_packages();
SELECT
  delivery.fn_update_all_boxes();
  */
SELECT
  delivery.fn_disable_triggers();
SELECT
  delivery.fn_enable_triggers();

