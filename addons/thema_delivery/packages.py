# coding=utf-8
from openerp.osv import orm, fields


class stock_move(orm.Model):
    _inherit = 'stock.move'

    _columns = {
        'package': fields.integer('Collo', default=1),
        'box': fields.integer('Scatola'),
        'package_id': fields.function(lambda s, *a, **k: s._package_and_box(*a, **k),
                                      type='many2one', relation='delivery.package',
                                      multi='package_and_box', string='Dettaglio collo'),
        'box_id': fields.function(lambda s, *a, **k: s._package_and_box(*a, **k),
                                  type='many2one', relation='delivery.box',
                                  multi='package_and_box', string='Dettaglio scatola'),
    }

    _defaults = {
        'package': 1,
    }

    _sql_constraints = [
        ('package_box_nonnegative', "CHECK( package >= 0 AND box >= 0 )", 'Collo e scatola non possono esser negativi!')
    ]

    _constraints = [
        (lambda s, *a, **k: s._check_packages,
         u'I colli specificati nelle righe sono in conflitto '
         u'con il numero di colli specificato nella testata.',
         ['package']),
    ]

    def _check_packages(self, cr, uid, ids, context=None):
        if not ids:
            return True
        cr.execute("""
            SELECT 1
            FROM stock_move m
            INNER JOIN stock_picking p ON p.id = m.picking_id
            INNER JOIN delivery_delivery d ON d.id = p.delivery_id
            WHERE m.package > d.number_of_packages
            AND m.id IN %s
            LIMIT 1
        """, [tuple(ids)])
        return not cr.fetchall()

    def _package_and_box(self, cr, uid, ids, field, arg, context=None):
        cr.execute("""
            SELECT
              m.id,
              delivery.package_id(m.delivery_id, m.package) package_id,
              delivery.box_id(m.delivery_id, m.package, m.box) box_id
            FROM stock_move m
            WHERE m.id IN %s AND delivery_id IS NOT NULL
        """, [tuple(ids)])
        res = {el.pop('id'): el for el in cr.dictfetchall()}
        res.update(dict.fromkeys(set(ids) - set(res), {'package_id': False, 'box_id': False}))
        return res



from devutils.sql_func import sql_function


class package_field(sql_function):
    field_map = {
        'move_ids': 'm.id move_ids',
        'box_ids': 'delivery.box_id(m.delivery_id, m.package, m.box) box_ids',
    }

    def id_expr(self):
        return "delivery.package_id(m.delivery_id, m.package)"

    def query(self, fields):
        fields = ', '.join(self.field_map[f] for f in fields)
        q = """
            SELECT {0} id,
                {1}
            FROM stock_move m
            WHERE delivery_id IS NOT NULL
        """.format(self.id_expr(), fields)
        return q

    def query_read(self, fields, ids):
        return self.query(fields) + " AND {0} IN %s".format(self.id_expr()), [tuple(ids)]


class box_field(package_field):
    def id_expr(self):
        return "delivery.box_id(m.delivery_id, m.package, m.box)"


class delivery_package_data(orm.Model):
    _inherit = 'devutils.copyonwrite'
    _name = 'delivery.package.data'

    _columns = {
        'weight': fields.float('Peso (KG)'),
        'weight_net': fields.float('Peso netto (KG)'),
        'height': fields.float('Altezza (cm)'),
        'width': fields.float('Larghezza (cm)'),
        'depth': fields.float(u'Profondità (cm)'),
    }

    _defaults = {
        'weight': 0.0,
        'weight_net': 0.0,
        'height': 0.0,
        'width': 0.0,
        'depth': 0.0,
    }

class delivery_package(orm.Model):
    _name = 'delivery.package'
    _auto = False
    _order = 'id'
    _inherits = {
        'delivery.package.data': 'id',
    }

    _columns = {
        'delivery_id': fields.many2one('delivery.delivery', 'Spedizione', readonly=True),
        'number': fields.integer('Numero', readonly=True),
        'move_ids': package_field('Righe', type='one2many', multi='move_box', relation='stock.move', readonly=True),
        'box_ids': package_field('Dettaglio scatole', multi='move_box', relation='delivery.box', type='one2many', readonly=True)
    }

    def init(self, cr):
        from os import path as p

        with open(p.join(p.dirname(__file__), 'packages.sql')) as fo:
            cr.execute(fo.read())

    def _numbers(self, cr, uid, ids, field, arg, context=None):
        number = lambda x: x % 1000
        delivery = lambda x: int(x / 1000)
        return {id: {'delivery_id': delivery(id), 'number': number(id)} for id in ids}


class delivery_box(orm.Model):
    _name = 'delivery.box'
    _auto = False
    _order = None

    _columns = {
        'package_id': fields.function(lambda s, *a, **k: s._numbers(*a, **k), type='many2one',
                                      relation='delivery.package', string='Collo', multi='numbers'),
        'delivery_id': fields.function(lambda s, *a, **k: s._numbers(*a, **k), type='many2one',
                                       relation='delivery.delivery', string='Spedizione', multi='numbers'),
        'number': fields.function(lambda s, *a, **k: s._numbers(*a, **k), type='integer', string='Numero scatola',
                                  multi='numbers'),
        'package_number': fields.function(lambda s, *a, **k: s._numbers(*a, **k), type='integer',
                                          string='Numero collo', multi='numbers'),
        'move_ids': box_field('Righe', type='one2many', relation='stock.move'),
    }

    def _numbers(self, cr, uid, ids, field, arg, context=None):
        left = lambda x: long(x/1000)
        right = lambda x: x % 1000
        res = {}
        for id in ids:
            package_id, box_no = left(id), right(id)
            delivery_id, package_no = left(package_id), right(package_id)
            res[id] = {
                'package_id': package_id,
                'delivery_id': delivery_id,
                'package_number': package_no,
                'number': box_no,
            }
        return res


class delivery(orm.Model):
    _inherit = 'delivery.delivery'
    _columns = {
        'package_ids': fields.one2many('delivery.package', 'delivery_id', 'Dettaglio colli', readonly=True,
                                       states={'open': [('readonly', False)]}),

    }

