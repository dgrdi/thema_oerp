from openerp.osv import orm, fields, osv
from openerp.netsvc import LocalService
from openerp.tools.translate import _
from openerp.addons import decimal_precision as dp
import time

class stock_picking(orm.Model):
    _inherit = 'stock.picking'

    _columns = {
        'return_of_id': fields.many2one('delivery.delivery', 'Bolla di ritorno per'),
        'related_deliveries_ids': fields.function(lambda s, *a, **k: s._related_deliveries_ids(*a, **k),
                                                  type='one2many', relation='delivery.delivery',
                                                  string='Spedizioni correlate')
    }

    def _related_deliveries_ids(self, cr, uid, ids, field, arg, context=None):
        if not ids:
            return {}
        cr.execute("""
            SELECT DISTINCT mv.picking_id id, mv2.delivery_id
            FROM stock_move mv
            INNER JOIN stock_move_related_rel rl ON rl.move_id = mv.id
            INNER JOIN stock_move mv2 ON mv2.id = rl.related_move_id
            WHERE mv.picking_id IN %s
            AND mv2.delivery_id IS NOT NULL
        """, [tuple(ids)])
        res = {}
        for pick_id, delivery_id in cr.fetchall():
            res.setdefault(pick_id, []).append(delivery_id)
        res.update(dict.fromkeys(set(ids) - set(res), []))
        return res


    def generate_returns(self, cr, uid, line_data, delivery_type_id=None, invoice_state='none', context=None):
        """
        genera bolle di reso per un insieme di movimenti di magazzino. Sostituisce il codice in
        stock.stock_return_picking, per permettere la generazione programmatica di resi senza dover passare per un
        oggetto transitorio.
        line data: dict da ID movimento a quantita`.
        return: list degli ID dei picking generati.
        """
        res = {}
        pickings = {}
        now = time.strftime('%Y-%m-%d')
        def get_new_picking(pick):
            if pick.id not in pickings:
                if pick.type == 'in':
                    new_type = 'out'
                    assert delivery_type_id is not None
                elif pick.type == 'out':
                    new_type = 'in'
                else:
                    new_type = pick.type
                new_name = self.get_name(cr, uid, new_type, delivery_type_id, pick.company_id.id, context=context)
                new_pick_id = self.copy(cr, uid, pick.id, {
                    'name': _('%s-%s-return') % (new_name, pick.name),
                    'move_lines': [],
                    'state': 'draft',
                    'type': new_type,
                    'date': now,
                    'invoice_state': invoice_state,
                }, context=context)
                pickings[pick.id] = new_pick_id
        move_osv = self.pool.get('stock.move')
        uom_osv = self.pool.get('product.uom')
        set_invoice_state_to_none = set()
        returned = 0
        for move in move_osv.browse(cr, uid, line_data.keys(), context=context):
            pick_id = get_new_picking(move.picking_id)
            quantity = line_data[move.id]
            remaning_qty = move.product_qty - sum(h.product_qty for h in move.move_history_ids2)
            if remaning_qty != quantity:
                set_invoice_state_to_none.add(pick_id)
            if quantity:
                returned += 1
                new_move = move_osv.copy(cr, uid, move.id, {
                    'product_qty': quantity,
                    'product_uos_qty': uom_osv._compute_qty(cr, uid, move.product_uom.id, quantity, move.product_uos.id),
                    'picking_id': pick_id,
                    'state': 'draft',
                    'location_id': move.location_dest_id.id,
                    'location_dest_id': move.location_id.id,
                    'date': now,
                })
                move_osv.write(cr, uid, move.id, {'move_history_ids2': [(4, new_move)]}, context=context)

        if not returned:
            raise osv.except_osv(_('Warning!'), _("Please specify at least one non-zero quantity."))
        if set_invoice_state_to_none:
            self.write(cr, uid, list(set_invoice_state_to_none), {'invoice_state': 'none'}, context=context)
        wf = LocalService('workflow')
        for pick_id in pickings.values():
            wf.trg_validate(uid, 'stock.picking', pick_id, 'button_confirm', cr)
        self.force_assign(cr,  uid, pickings.values())
        return pickings.values()







class stock_picking_in(orm.Model):
    _inherit = 'stock.picking.in'
    _columns = {
        'return_of_id': fields.many2one('delivery.delivery', 'Bolla di ritorno per')
    }

class stock_picking_out(orm.Model):
    _inherit = 'stock.picking.out'
    _columns = {
        'related_deliveries_ids': fields.function(lambda s, *a, **k: s._related_deliveries_ids(*a, **k),
                                                  type='one2many', relation='delivery.delivery',
                                                  string='Spedizioni correlate')

    }

    def action_related(self, cr, uid, ids, context=None):
        context = context or {}
        context.update({
            'active_model': self._name,
            'active_ids': ids,
            'active_id': len(ids) and ids[0] or False,
            'generate_related': True
        })
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'stock.partial.picking',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': context,
            'nodestroy': True,
        }


class delivery_type(orm.Model):
    _inherit = 'thema.delivery.type'
    _columns = {
        'expects_return': fields.boolean('Bolla con giro di ritorno'),

    }

class delivery_delivery(orm.Model):
    _inherit = 'delivery.delivery'

    _columns = {
        'return_ids': fields.one2many('stock.picking.in', 'return_of_id', string='Bolle di rientro', readonly=True),
        'related_deliveries_ids': fields.function(lambda s, *a, **k: s._related_deliveries_ids(*a, **k),
                                               type='one2many', relation='delivery.delivery',
                                               string='Picking correlati'),
        'related_type_id': fields.many2one('stock.related.type', 'Tipo lavorazione'),
        'expects_return': fields.related('delivery_type_id', 'expects_return', type='boolean',
                                         string='Bolla con giro di ritorno')
    }


    def _related_deliveries_ids(self, cr, uid, ids, field, arg, context=None):
        if not ids: return {}
        cr.execute("""
            SELECT DISTINCT dv.id, mv2.delivery_id
            FROM delivery_delivery dv
            INNER JOIN stock_move mv ON mv.delivery_id = dv.id
            INNER JOIN stock_move_related_rel rl ON rl.move_id = mv.id
            INNER JOIN stock_move mv2 ON rl.related_move_id = mv2.id
            WHERE dv.id IN %s
            AND mv2.delivery_id IS NOT NULL
        """, [tuple(ids)])
        res = {}
        for delivery_id, picking_id in cr.fetchall():
            res.setdefault(delivery_id, []).append(picking_id)
        res.update(dict.fromkeys(set(ids) - set(res), []))
        return res

    def action_related_return(self, cr, uid, ids, context=None):
        assert len(ids) == 1



class stock_move(orm.Model):
    _inherit = 'stock.move'

    _columns = {
        'related_move_ids': fields.many2many('stock.move', 'stock_move_related_rel', 'move_id', 'related_move_id',
                                          string='Movimenti correlati (lavoro c/terzi)'),
        'move_for_ids': fields.many2many('stock.move', 'stock_move_related_rel', 'related_move_id', 'move_id',
                                         string='Movimento originato da'),
        'related_qty': fields.function(lambda s, *a, **k: s._related_qty(*a, **k), string='Qta in lavorazione esterna',
                                       type='float', digits_compute=dp.get_precision('Product UoS')),

    }

    def _related_qty(self, cr, uid, ids, field, arg, context=None):
        if not ids: return {}
        cr.execute("""
            SELECT mv.id, COALESCE(SUM(mv2.product_qty), 0)
            FROM stock_move mv
            LEFT JOIN stock_move_related_rel rl ON rl.move_id = mv.id
            LEFT JOIN stock_move mv2 ON mv2.id = rl.related_move_id
            WHERE mv.id IN %s
             GROUP BY mv.id
        """, [tuple(ids)])
        return dict(cr.fetchall())

    def generate_related_delivery(self, cr, uid, line_data, type_id, delivery_type_id, partner_id, context=None):
        """
        Genera una spedizione di lavorazione (una bolla generalmente in c/lavoro con giro di rientro)
        a partire da un gruppo di movimenti di magazzino.

        line_data: dict da ID movimento (stock.move) a quantita` da inserire nella bolla di c/lavoro).
        """
        pick_osv = self.pool.get('stock.picking')
        deltype_osv = self.pool.get('thema.delivery.type')
        delivery_type = deltype_osv.browse(cr, uid, delivery_type_id, context=context)
        pick_id = pick_osv.create(cr, uid, {
            'type': 'out',
            'partner_id': partner_id,
            'delivery_type_id': delivery_type_id,
            'stock_journal_id': delivery_type.stock_journal_id and delivery_type.stock_journal_id.id,
        }, context=context)
        for move_id, qty in line_data.items():
            vals = {
                'product_qty': qty,
                'picking_id': pick_id,
                'move_for_ids': [(6, 0, [move_id])],
           }
            if delivery_type.location_dest_id:
                vals['location_dest_id'] = delivery_type.location_dest_id.id
            self.copy(cr, uid, move_id, vals, context=context)
        wf = LocalService('workflow')
        wf.trg_validate(uid, 'stock.picking', pick_id, 'button_confirm', cr)
        pick_osv.force_assign(cr, uid, [pick_id])
        wf.trg_validate(uid, 'stock.picking', pick_id, 'button_done', cr)
        delivery_id = pick_osv.action_delivery_create(cr, uid, [pick_id], context=context)[pick_id]
        dv_osv = self.pool.get('delivery.delivery')
        dv_osv.write(cr, uid, delivery_id, {
            'related_type_id': type_id
        }, context=context)
        return delivery_id




class stock_picking_template(orm.Model):
    _name = 'stock.picking.template'
    _columns = {
        'name': fields.char('Nome', required=True),
        'type_id': fields.many2one('stock.related.type', 'Tipo lavorazione', required=True),
        'delivery_type_id': fields.many2one('thema.delivery.type', required=True, string='Tipo spedizione'),
        'partner_id': fields.many2one('res.partner', 'Partner', required=True)
    }



class stock_partial_picking(orm.TransientModel):
    _inherit = 'stock.partial.picking'
    _columns = {
        'template_id': fields.many2one('stock.picking.template', 'Modello'),
        'type_id': fields.many2one('stock.related.type', 'Tipo lavorazione'),
        'delivery_type_id': fields.many2one('thema.delivery.type', 'Tipo spedizione'),
        'partner_id': fields.many2one('res.partner', 'Partner'),
    }

    def onchange_template_id(self, cr, uid, ids, template_id, context=None):
        value = {}
        if template_id:
            tid = self.pool.get('stock.picking.template').browse(cr, uid, template_id, context=context)
            value.update({
                'type_id': tid.type_id.id,
                'delivery_type_id': tid.delivery_type_id.id,
                'partner_id': tid.partner_id.id,
            })
        return {
            'value': value
        }

    def do_related(self, cr, uid, ids, context=None):
        assert len(ids) == 1
        wz = self.browse(cr, uid, ids[0], context=context)
        line_data = {}
        for line in wz.move_ids:
            if line.quantity:
                line_data[line.move_id.id] = line.quantity
        if not line_data:
            raise osv.except_osv('Attenzione', 'Nessun movimento da generare.')
        dv_id = self.pool.get('stock.move').generate_related_delivery(cr, uid,
                                                                      line_data, wz.type_id.id, wz.delivery_type_id.id,
                                                                      wz.partner_id.id, context=context)
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'delivery.delivery',
            'view_mode': 'form',
            'res_id': dv_id,
            'name': 'Spedizione c/lavoro'
        }



class stock_related_type(orm.Model):
    _name = 'stock.related.type'
    _columns = {
        'name': fields.char('Nome', required=True),
        'product_id': fields.many2one('product.product', 'Prodotto'),
    }


class stock_return_picking(orm.TransientModel):
    _inherit = 'stock.return.picking'

    def create_returns(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        record_id = context and context.get('active_id', False) or False
        data = self.browse(cr, uid, ids[0], context=context)
        line_data = {}
        for line in data.product_return_moves:
            if not line.move_id:
                raise osv.except_osv(_('Warning !'),
                                     _("You have manually created product lines, please delete them to proceed"))
            if line.quantity > 0:
                line_data[line.move_id.id] = line.quantity
        invoice_state = data.invoice_state
        gen_picks = self.pool.get('stock.picking').generate_returns(cr, uid, line_data, invoice_state=invoice_state,
                                                                    context=context)
        assert len(gen_picks) == 1

        new_picking = self.pool.get('stock.picking').browse(cr, uid, gen_picks[0], context=context)
        model_list = {
                'out': 'stock.picking.out',
                'in': 'stock.picking.in',
                'internal': 'stock.picking',
        }
        return {
            'domain': "[('id', 'in', ["+str(new_picking.id)+"])]",
            'name': _('Returned Picking'),
            'view_type':'form',
            'view_mode':'tree,form',
            'res_model': model_list.get(new_picking.type, 'stock.picking'),
            'type':'ir.actions.act_window',
            'context':context,
        }



