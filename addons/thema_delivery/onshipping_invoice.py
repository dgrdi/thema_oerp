from openerp.osv import orm, fields


class onshipping_delivery_invoice(orm.TransientModel):
    _name = 'delivery.onshipping.invoice'
    _columns = {
        'journal_id': fields.many2one('account.journal', 'Sezionale', domain=[('type', '=', 'sale')], required=True),
        'group': fields.boolean('Raggruppa fatture'),
    }

    _defaults = {
        'group': True,
    }

    def action_generate_invoice(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
        inv_res = self.pool.get('delivery.delivery').action_invoice(cr, uid, context.get('active_ids'),
                                                                    journal_id=obj.journal_id.id, group=obj.group,
                                                                    type='out_invoice', context=context)
        return self.action_des(cr, uid, list(set(inv_res.values())))

    def action_des(self, cr, uid, invoice_ids):
        imd = self.pool.get('ir.model.data')
        ads = {
            'type': 'ir.actions.act_window',
            'res_model': 'account.invoice',
            'target': 'current',
            'name': 'Fatture generate',
            'domain': repr([('id', 'in', invoice_ids)]),
            'view_mode': 'tree,form',
            'view_id': imd.get_object_reference(cr, uid, 'account', 'invoice_form')[1],
        }
        if len(invoice_ids) == 1:
            ads['view_mode'] = 'form'
            ads['res_id'] = invoice_ids[0]
        return ads
