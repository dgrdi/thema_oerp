from openerp.osv import orm, fields
# the stock.picking wizard should create deliveries, not invoices; invoices should be created from deliveries.
from lxml import etree
from openerp.osv import osv

class onshipping(orm.TransientModel):
    _name = 'delivery.onshipping.create'

    _columns = {
        'delivery_id': fields.many2one('delivery.delivery', 'Accoda a spedizione'),
    }

    def fields_view_get(self, cr, user, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):
        # dynamic domain on delivery_id field
        context = context or  {}
        res = super(onshipping, self).fields_view_get(cr, user, view_id, view_type, context, toolbar, submenu)
        pick_id = context.get('picking_id')
        if not pick_id:
            return res
        stock_picking = self.pool.get('stock.picking')
        appendable = stock_picking.find_delivery_append(cr, user, pick_id, context=context)
        doc = etree.fromstring(res['arch'])
        doc.xpath("//field[@name='delivery_id']")[0].set('domain', repr([('id', 'in', appendable)]))
        res['arch'] = etree.tostring(doc)
        return res

    def action_append(self, cr, uid, ids, context=None):
        context = context or {}
        obj = self.browse(cr, uid, ids[0], context=context)
        if not obj.delivery_id:
            raise osv.except_osv('Attenzione!', 'Selezionare una spedizione per l\'accodamento')
        pick_id = context.get('active_id')
        if not pick_id:
            return False
        self.pool.get('stock.picking').action_delivery_append(cr, uid, pick_id, obj.delivery_id.id, context=context)
        return self.return_action([obj.delivery_id.id])

    def action_create(self, cr, uid, ids, context=None):
        context = context or {}
        pick_id = context.get('active_id')
        if not pick_id:
            return False
        dv = self.pool.get('stock.picking').action_delivery_create(cr, uid, [pick_id], context=context)
        return self.return_action(dv.values())

    def return_action(self, delivery_ids):
        act_descr= {
            'type': 'ir.actions.act_window',
            'target': 'current',
            'res_model': 'delivery.delivery',
            'domain': repr([('id', 'in', delivery_ids)]),
            'view_mode': 'tree,form',
        }
        if len(delivery_ids) == 1:
            act_descr['view_mode'] = 'form'
            act_descr['res_id'] = delivery_ids[0]
        return act_descr

