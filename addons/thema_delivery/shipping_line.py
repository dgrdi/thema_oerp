from openerp.osv import orm, fields, osv
from openerp.tools.translate import _
from openerp.addons.delivery import stock
from openerp.addons import decimal_precision as dp

def action_invoice_create(self, cr, uid, ids, journal_id=False, group=False, type='out_invoice', context=None):
    context = context or {}
    invoice_obj = self.pool.get('account.invoice')
    picking_obj = self.pool.get('stock.picking')
    invoice_line_obj = self.pool.get('account.invoice.line')
    del_obj = self.pool.get('delivery.delivery')
    result = super(stock.stock_picking, self).action_invoice_create(cr, uid,
                                                                    ids, journal_id=journal_id, group=group, type=type,
                                                                    context=context)
    done = set()
    for picking in picking_obj.browse(cr, uid, result.keys(), context=context):
        if not picking.delivery_id or picking.delivery_id.id in done:
            continue
        done.add(picking.delivery_id.id)
        ctx = context.copy()
        ctx['force_company'] = picking.delivery_id.company_id.id
        dv = del_obj.browse(cr, uid, picking.delivery_id.id, context=ctx)
        invoice = invoice_obj.browse(cr, uid, result[picking.id], context=ctx)
        invoice_line = del_obj._prepare_shipping_invoice_line(cr, uid, dv, invoice, context=context)
        if invoice_line:
            invoice_line_obj.create(cr, uid, invoice_line)
            invoice_obj.button_compute(cr, uid, [invoice.id], context=context)
    return result


stock.stock_picking.action_invoice_create = action_invoice_create


class delivery(orm.Model):
    _inherit = 'delivery.delivery'

    _columns = {
        'shipping_charge': fields.float('Spese di spedizione', digits_compute=dp.get_precision('Account')),
    }

    def _prepare_shipping_invoice_line(self, cr, uid, delivery, invoice, context=None):
        """Prepare the invoice line to add to the shipping costs to the shipping's
           invoice.

            :param browse_record delivery: the stock picking being invoiced
            :param browse_record invoice: the stock picking's invoice
            :return: dict containing the values to create the invoice line,
                     or None to create nothing
        """
        carrier_obj = self.pool.get('delivery.carrier')
        grid_obj = self.pool.get('delivery.grid')
        if not delivery.carrier_id:
            return None
        if delivery.carrier_id.free_backorder and delivery.picking_ids and all(
                p.backorder_id for p in delivery.picking_ids):
            return None
        if delivery.shipping_charge:
            # if a shipping charge was manually specified for the delivery, use that one
            price = delivery.shipping_charge
        else:
            grid_id = carrier_obj.grid_get(cr, uid, [delivery.carrier_id.id],
                                           delivery.partner_id.id, context=context)
            if not grid_id:
                raise osv.except_osv(_('Warning!'),
                                     _('The carrier %s (id: %d) has no delivery grid!') \
                                     % (delivery.carrier_id.name,
                                        delivery.carrier_id.id))
            price = grid_obj.get_price_from_picking(cr, uid, grid_id,
                                                    invoice.amount_untaxed, delivery.weight, delivery.volume,
                                                    context=context)
        account_id = delivery.carrier_id.product_id.property_account_income.id
        if not account_id:
            account_id = delivery.carrier_id.product_id.categ_id \
                .property_account_income_categ.id

        taxes = delivery.carrier_id.product_id.taxes_id
        partner = delivery.partner_id or False
        if partner:
            account_id = self.pool.get('account.fiscal.position').map_account(cr, uid,
                                                                              partner.property_account_position,
                                                                              account_id)
            taxes_ids = self.pool.get('account.fiscal.position').map_tax(cr, uid, partner.property_account_position,
                                                                         taxes)
        else:
            taxes_ids = [x.id for x in taxes]

        return {
            'name': delivery.carrier_id.name,
            'invoice_id': invoice.id,
            'uos_id': delivery.carrier_id.product_id.uos_id.id,
            'product_id': delivery.carrier_id.product_id.id,
            'account_id': account_id,
            'price_unit': price,
            'quantity': 1,
            'invoice_line_tax_id': [(6, 0, taxes_ids)],
            'line_type_id': self.pool.get('ir.model.data').get_object_reference(cr, uid, 'thema_line_type', 'shipping')[
                -1]
        }



