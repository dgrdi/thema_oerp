from openerp import netsvc
from openerp.osv import orm, fields
from openerp.addons import decimal_precision as dp
from openerp import netsvc


class Picking(orm.Model):
    _inherit = 'stock.picking'

    def _get_picking_line(self, cr, uid, ids, context=None):
        cr.execute("SELECT DISTINCT picking_id FROM stock_move WHERE id IN %s", [tuple(ids)])
        return [el[0] for el in cr.fetchall()]

    # we redefine the weight fields and their evaluation functions because of performance problems
    # in the delivery module (unacceptable slowdowns on sale order confirmation)

    _columns = {
        'delivery_id': fields.many2one('delivery.delivery', 'Spedizione', readonly=True, select=True),
        'weight': fields.function(lambda s, *a, **k: s._cal_weight(*a, **k), type='float', string='Weight', digits_compute= dp.get_precision('Stock Weight'), multi='_cal_weight',
                  store={
                 'stock.picking': (lambda self, cr, uid, ids, c={}: ids, ['move_lines'], 20),
                 'stock.move': (_get_picking_line, ['product_id','product_qty','product_uom','product_uos_qty'], 20),
                 }),
        'weight_net': fields.function(lambda s, *a, **k: s._cal_weight(*a, **k), type='float', string='Net Weight', digits_compute= dp.get_precision('Stock Weight'), multi='_cal_weight',
                  store={
                 'stock.picking': (lambda self, cr, uid, ids, c={}: ids, ['move_lines'], 20),
                 'stock.move': (_get_picking_line, ['product_id','product_qty','product_uom','product_uos_qty'], 20),
                 }),
        'invoice_group_hashcode': fields.function(lambda s, *a, **k: s._invoice_group_hashcode(*a, **k),
                                                  type='char', string='Hash raggruppamento fatture'),


    }

    def _invoice_group_hashcode(self, cr, uid, ids, field, arg, context=None):
        res = {}
        pp = self.pool.get('res.partner')
        for p in self.browse(cr, uid, ids, context=context):
            partner = self._get_partner_to_invoice(cr, uid, p, context=context)
            if isinstance(partner, (int, long)):
                partner = pp.browse(cr, uid, partner, context=context)
            res[p.id] = repr(self._get_invoice_group_hashcode(cr, uid, p, partner, context=context))
        return res

    def _cal_weight(self, cr, uid, ids, field, arg, context=None):
        cr.execute("SELECT picking_id id,"
                   " sum(weight) weight, "
                   " SUM(weight_net) weight_net "
                   "FROM stock_move "
                   "WHERE picking_id IN %s"
                   "GROUP BY picking_id ",
                   [tuple(ids)])
        res = {el.pop('id'): el for el in cr.dictfetchall()}
        res.update(dict.fromkeys(set(res) - set(ids), {'weight': 0.0, 'weight_net': 0.0}))
        return res


    def _get_invoice_group_hashcode(self, cr, uid, picking, partner, context=None):
        """
        Picking with the same hashcode will be invoiced together if the group invoice option was chosen.
        """
        return (
            partner.id,  # cliente
            picking.sale_id.agent_id and picking.sale_id.agent_id.id or False,  # agente
            picking.sale_id.area_manager_id and picking.sale_id.area_manager_id.id or False,  # capoarea
            picking.sale_id and picking.sale_id.payment_discount or 0.0,  #sconto pagamento,
            picking.sale_id.payment_term and picking.sale_id.payment_term.id or False,  # condizioni pagamento
        )

    def _prepare_invoice_delivery(self, cr, uid, picking, partner, invoice, context=None):
        """
        Called when a picking is to be invoiced toghether with another picking on the same delivery.
        Works like _prepare_invoice_group.
        """
        return {}

    def copy_data(self, cr, uid, id, default=None, context=None):
        vals = super(Picking, self).copy_data(cr, uid, id, default=default, context=context)
        vals.pop('delivery_id', None)
        return vals

    def find_delivery_append(self, cr, uid, id, context=None):
        # we can append a picking to a delivery if they have the same invoice group hashcode\.
        dv = self.pool.get('delivery.delivery')
        pick = self.browse(cr, uid, id, context=context)
        if not pick.partner_id:
            return []
        # restrict the search by partner; we cannot append to a delivery made to someone else
        delvs = dv.search(cr, uid, [
            ('partner_id', '=', pick.partner_id.id),
            ('state', '=', 'open'),
        ], context=context)
        appendable = []
        this_partner = self._get_partner_to_invoice(cr, uid, pick, context=context)
        if isinstance(this_partner, (int, long)):
            this_partner = self.pool.get('res.partner').browse(cr, uid, this_partner, context=context)
        this_hash = self._get_invoice_group_hashcode(cr, uid, pick, this_partner, context=context)
        # find deliveries with the same invoice hashcode - if they can be invoiced together, they can be
        # delivered together
        for delivery in dv.browse(cr, uid, delvs, context=context):
            if not delivery.picking_ids:
                continue
            partner = self._get_partner_to_invoice(cr, uid, delivery.picking_ids[0], context=context)
            if not partner:
                continue
            if isinstance(partner, (int, long)):
                partner = self.pool.get('res.partner').browse(cr, uid, partner, context=context)
            hashcode = self._get_invoice_group_hashcode(cr, uid, delivery.picking_ids[0], partner, context=context)
            if hashcode == this_hash:
                appendable.append(delivery.id)
        return appendable

    def action_delivery_create(self, cr, uid, ids, context=None):
        deliveries = {}
        dv = self.pool.get('delivery.delivery')
        for pick in self.browse(cr, uid, ids, context=context):
            if pick.type != 'out' or not pick.partner_id or pick.delivery_id:
                continue
            vals = self._prepare_delivery(cr, uid, pick, context=context)
            deliveries[pick.id] = dv.create(cr, uid, vals, context=context)
            self.write(cr, uid, pick.id, {'delivery_id': deliveries[pick.id]})
        return deliveries

    def action_delivery_append(self, cr, uid, pick_id, delivery_id, context=None):
        pick = self.browse(cr, uid, pick_id, context=context)
        if pick.delivery_id:
            return False
        delivery = self.pool.get('delivery.delivery').browse(cr, uid, delivery_id, context=context)
        vals = self._update_delivery(cr, uid, pick, delivery, context=context)
        self.pool.get('delivery.delivery').write(cr, uid, delivery_id, vals)
        self.write(cr, uid, pick_id, {'delivery_id': delivery_id}, context=context)
        return True

    def _prepare_delivery(self, cr, uid, pick, context=None):
        return {
            'state': 'open',
            'partner_id': pick.partner_id.id,
            'company_id': pick.company_id.id,
            'delivery_type_id': pick.delivery_type_id.id,
            'carrier_id': pick.carrier_id and pick.carrier_id.id or False,
            'date': pick.date_done,
            'weight': pick.weight,
            'weight_net': pick.weight_net,
            'volume': pick.volume,
            'number_of_packages': pick.number_of_packages,
            'incoterm_id': pick.sale_id.incoterm and pick.sale_id.incoterm.id or False,
            'invoice_type_id': pick.invoice_type_id and pick.invoice_type_id.id or False,
            'invoice_partner_sale': pick.sale_id.partner_invoice_id and pick.sale_id.partner_invoice_id.id or False,
            'comm_user_id': pick.sale_id.user_id and pick.sale_id.user_id.id or False,
            'cod': pick.sale_id and pick.sale_id.cod or False,
        }

    def _update_delivery(self, cr, uid, pick, delivery, context=None):
        return {
            'date': pick.date_done,
            'weight': delivery.weight + pick.weight,
            'weight_net': delivery.weight_net + pick.weight_net,
            'volume': delivery.volume + pick.volume,
            'number_of_packages': delivery.number_of_packages + pick.number_of_packages,
        }


class PickingOut(orm.Model):
    _inherit = 'stock.picking.out'

    _columns = {
        'delivery_id': fields.many2one('delivery.delivery', 'Spedizione'),
        'purchase_id': fields.many2one('purchase.order', 'Ordine di acquisto', ondelete='set null', select=True),
    }

    def _prepare_shipping_invoice_line(self, cr, uid, picking, invoice, context=None):
        raise NotImplementedError('Should pass by delivery!')


    def action_delivery_create_or_append(self, cr, uid, ids, context=None):
        assert len(ids) == 1
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'delivery.onshipping.create',
            'target': 'new',
            'view_mode': 'form',
            'name': 'Crea / accoda a spedizione',
            'context': repr({'picking_id': ids[0]}),
        }



def _get_store_delivery(obj, cr, uid, ids, context=None):
    if not ids:
        return []
    cr.execute("""
        SELECT id FROM stock_move WHERE picking_id IN %s
    """, [tuple(ids)])
    return [el[0] for el in cr.fetchall()]

class stock_move(orm.Model):
    _inherit = 'stock.move'
    _order = 'id'

    _columns = {
        'delivery_id': fields.related('picking_id', 'delivery_id', type='many2one',
                                      relation='delivery.delivery', select=True, string='Spedizione',
                                      store = {
                                          'stock.move': (
                                              lambda o, c, u, ids, context=None: ids,
                                              ['picking_id'],
                                              10,
                                          ),

                                          'stock.picking': (
                                              _get_store_delivery,
                                              ['delivery_id'],
                                              10,

                                          )
                                      }),
        'sequence': fields.function(lambda s, *a, **k: s._fsequence(*a, **k), type='integer',
                                    string='Numero riga', multi='sequence'),
        'sequence_delivery': fields.function(lambda s, *a, **k: s._sequence(*a, **k), type='integer',
                                             string='Numero riga bolla', multi='sequence'),

    }

    def _fsequence(self, cr, uid, ids, field, arg, context=None):
        cr.execute("""
            SELECT m.id,
                   row_number() over (partition by picking_id order by id) "sequence",
                   row_number() over (partition by delivery_id order by picking_id, id) sequence_delivery
                   from stock_move m
                   where picking_id in (select picking_id from stock_move where id in %s)
        """, [tuple(ids)])
        res = {el.pop('id'): el for el in cr.dictfetchall()}
        return res

