# coding=utf-8
from openerp.osv import orm, fields, osv
from openerp.addons import decimal_precision as dp
import re


class carrier(orm.Model):
    _inherit = 'delivery.carrier'

    _columns = {
        'free_backorder': fields.boolean('Spedizione saldi gratuita'),
        'carrier_register_number': fields.char('Numero Albo Autotrasportatori'),
    }

    _defaults = {
        'free_backorder': True,
        'carrier_register_number': None,
    }


class delivery(orm.Model):
    _name = 'delivery.delivery'
    _order = 'date desc'

    _NUM_RX = re.compile('^(?:[^/]*/)+.*(\d+).*$')

    _columns = {
        'name': fields.char('Numero', readonly=True, required=True),
        'number': fields.char('Numero DDT', readonly=True),
        'state': fields.selection([('open', 'Aperta'), ('done', 'Confermata')], string='Stato', readonly=True),
        'company_id': fields.many2one('res.company', 'Azienda', states={'open': [('readonly', False)]}, readonly=True),
        'partner_id': fields.many2one('res.partner', 'Partner', required=True, states={'open': [('readonly', False)]},
                                      readonly=True),
        'delivery_type_id': fields.many2one('thema.delivery.type', 'Tipo consegna', required=True, readonly=True),
        'invoice_type_id': fields.many2one('sale_journal.invoice.type', 'Tipo fatturazione',
                                           states={'open': [('readonly', False)]}, readonly=True),
        'invoice_id': fields.many2one('account.invoice', 'Fattura immediata', readonly=True),
        'invoice_delayed_id': fields.many2one('account.invoice', 'Fattura differita', readonly=True),
        'invoice_partner_sale': fields.many2one('res.partner', 'Fatturazione', readonly=True),
        'comm_user_id': fields.many2one('res.users', 'Utente commerciale', readonly=True),
        'picking_ids': fields.one2many('stock.picking.out', 'delivery_id', ondelete='restrict',
                                       string='Ordini di consegna', readonly=True),
        'move_ids': fields.function(lambda s, *a, **k: s._move_ids(*a, **k),
                                    string='Righe',
                                    type='one2many',
                                    relation='stock.move'),
        'carrier_id': fields.many2one('delivery.carrier', string='Vettore', states={'open': [('readonly', False)]},
                                      readonly=True),
        'date': fields.date('Data', required=True, states={'open': [('readonly', False)]}, readonly=True),
        'weight': fields.float('Peso', digits_compute=dp.get_precision('Stock Weight'),
                               states={'open': [('readonly', False)]}, readonly=True),
        'weight_net': fields.float('Peso netto', digits_compute=dp.get_precision('Stock Weight'),
                                   states={'open': [('readonly', False)]}, readonly=True),
        'volume': fields.float('Volume'),
        'number_of_packages': fields.integer('Colli', states={'open': [('readonly', False)]}, readonly=True),
        'incoterm_id': fields.many2one('stock.incoterms', 'Incoterm', states={'open': [('readonly', False)]},
                                       readonly=True),
        'cod': fields.boolean('Contrassegno'),
        'cod_amount': fields.function(lambda s, *a, **k: s._cod_amount(*a, **k), type='float',
                                      digits_compute=dp.get_precision('Account'),
                                      string='Importo contrassegno'),
        'int_number': fields.function(lambda s, *a, **k: s._int_number(*a, **k),
                                      type='integer',
                                      string='Numero (numerico)'),
    }

    _defaults = {
        'name': '/',
        'state': 'open',
        'number_of_packages': 1,
        # 'invoice_partner_sale': ['partner_id'],
    }

    _sql_constraints = [
        ('no_packages_positive', "CHECK(number_of_packages >= 0)", 'La spedizione deve avere almeno un collo!')
    ]

    _constraints = [
        (lambda s, *a, **k: s._check_packages,
         u'I colli specificati nelle righe sono in conflitto '
         u'con il numero di colli specificato nella testata.',
         ['number_of_packages']),
    ]


    def _check_packages(self, cr, uid, ids, context=None):
        if not ids:
            return True
        cr.execute("""
            SELECT 1
            FROM delivery_delivery d
            INNER JOIN stock_picking p ON p.delivery_id = d.id
            INNER JOIN stock_move m ON m.picking_id = p.id
            WHERE m.package > d.number_of_packages
            AND d.id IN %s
            LIMIT 1
        """, [tuple(ids)])
        return not cr.fetchall()

    def _int_number(self, cr, uid, ids, field, arg, context=None):
        res = {}
        irs = self.pool.get('ir.sequence')
        for dv in self.browse(cr, uid, ids, context=context):
            if dv.state != 'done':
                res[dv.id] = False
            elif dv.invoice_id:
                res[dv.id] = dv.invoice_id.int_number
            elif dv.number and dv.delivery_type_id.public_id.code:
                res[dv.id] = irs.number_from_name(cr, uid, dv.delivery_type_id.public_id.code, dv.number,
                                                  context=context) or False
                if not res[dv.id]:
                    m = self._NUM_RX.match(dv.number)
                    if m:
                        res[dv.id] = int(m.group(1))
                if not res[dv.id]:
                    res[dv.id] = False
            else:
                res[dv.id] = False
        return res


    def _move_ids(self, cr, uid, ids, field, arg, context=None):
        if not ids:
            return {}
        cr.execute("""
            SELECT d.id, m.id
            FROM delivery_delivery d
            INNER JOIN stock_picking p ON p.delivery_id = d.id
            INNER JOIN stock_move m ON m.picking_id = p.id
            WHERE d.id IN %s
            ORDER BY p.date, p.id, m.id
        """, [tuple(ids)])
        res = {}
        for did, mid in cr.fetchall():
            res.setdefault(did, []).append(mid)
        res.update(dict.fromkeys(set(ids) - set(res), []))
        return res

    def _cod_amount(self, cr, uid, ids, field, arg, context=None):
        res = {}
        for ob in self.browse(cr, uid, ids, context=context):
            res[ob.id] = (ob.invoice_id and ob.invoice_id.amount_total or 0.0) if ob.cod else 0.0
        return res

    def name_get(self, cr, user, ids, context=None):
        delvs = self.browse(cr, user, ids, context=context)
        res = []
        for d in delvs:
            if not d.invoice_id and not d.number:
                nam = 'Aperta: %s' % d.partner_id.name
            else:
                nam = '%s (%s)' % (d.name, d.partner_id.name)
            res.append((d.id, nam))
        return res

    def _confirm_hook(self, cr, uid, delivery, context=None):
        pass

    def _cancel_hook(self, cr, uid, delivery, context=None):
        pass

    def action_assign(self, cr, uid, ids, context=None):
        context = context or {}
        fy_obj = self.pool.get('account.fiscalyear')
        seq = self.pool.get('ir.sequence')
        res = {}
        for delv in self.browse(cr, uid, ids, context=context):
            if delv.number or delv.invoice_id or delv.state != 'open':
                continue
            ctx = context.copy()
            ctx.update({
                'fiscalyear_id': fy_obj.find(cr, uid),
                'company_id': delv.company_id.id,
            })
            name = seq.next_by_id(cr, uid, delv.delivery_type_id.public_id.id, context=ctx)
            self.write(cr, uid, delv.id, {'number': name, 'state': 'done', 'name': name}, context=context)
            self._confirm_hook(cr, uid, delv, context=context)
            res[delv.id] = name
        res.update(dict.fromkeys(set(ids) - set(res), False))
        return res

    def action_invoice(self, cr, uid, ids, journal_id=False, group=False, type='out_invoice', context=None):
        context = context or {}
        ctx = context.copy()
        ctx['delivery_invoicing'] = True
        pick_ids = self.read(cr, uid, ids, fields=['picking_ids'], context=context)
        pick_ids = {pick_id: el['id'] for el in pick_ids for pick_id in el['picking_ids']}
        pickres = self.pool.get('stock.picking').action_invoice_create(cr, uid, list(pick_ids), journal_id=journal_id,
                                                                       group=group, type=type, context=ctx)
        res = {}
        for k, v in pickres.items():
            res[pick_ids[k]] = v
        for k, v in res.items():
            self.write(cr, uid, k, {'state': 'done', 'invoice_id': v}, context=context)
            self._confirm_hook(cr, uid, self.browse(cr, uid, k, context=context), context=context)
        res.update(dict.fromkeys(set(ids) - set(res), False))
        return res

    def action_cancel(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'open'}, context=context)
        for delv in self.browse(cr, uid, ids, context=context):
            self._cancel_hook(cr, uid, delv, context=context)
        return True

    def action_reconfirm(self, cr, uid, ids, context=None):
        for delv in self.browse(cr, uid, ids, context=context):
            if delv.invoice_id or delv.number:
                self.write(cr, uid, delv.id, {'state': 'done'})
                self._confirm_hook(cr, uid, delv, context=context)
        return True

    def unlink(self, cr, uid, ids, context=None):
        if self.search(cr, uid, [('id', 'in', ids), ('state', '=', 'done')]):
            raise osv.except_osv('Errore!', u'Non si può eliminare una spedizione confermata.')
        return super(delivery, self).unlink(cr, uid, ids, context=context)

