"""
Modify action_invoice_create at the top level of the inheritance chain, to group invoice based
 deliveries.
"""

from openerp.addons.stock.stock import stock_picking
from openerp.osv import osv
from openerp.tools.translate import _
from openerp.osv import orm, fields


def action_invoice_create(self, cr, uid, ids, journal_id=False,
                          group=False, type='out_invoice', context=None):
    if context is None:
        context = {}

    invoice_obj = self.pool.get('account.invoice')
    invoice_line_obj = self.pool.get('account.invoice.line')
    partner_obj = self.pool.get('res.partner')
    invoices_group = {}
    deliveries = {}
    res = {}
    inv_type = type
    for picking in self.browse(cr, uid, ids, context=context):
        if picking.invoice_state != '2binvoiced':
            continue
        # diff: do not invoice pickings that have not been delivered
        if picking.type == 'out' and (not picking.delivery_id or picking.delivery_id.state != 'done') \
                and not context.get('delivery_invoicing'):
            # delivery_invoicing is true when the invoice is being generated from the delivery.
            # The delivery is not yet done, but the invoice still has to be generated.
            continue
        partner = self._get_partner_to_invoice(cr, uid, picking, context=context)
        if isinstance(partner, int):
            partner = partner_obj.browse(cr, uid, [partner], context=context)[0]
        if not partner:
            raise osv.except_osv(_('Error, no partner!'),
                                 _('Please put a partner on the picking list if you want to generate invoice.'))

        if not inv_type:
            inv_type = self._get_invoice_type(picking)
        # diff: invoices must always be grouped based on delivery. we may want to further group them
        # by some other criteria, expressed in _get_invoice_group_hashcode
        group_hash = self._get_invoice_group_hashcode(cr, uid, picking, partner, context=context)
        if picking.type == 'out' and picking.delivery_id in deliveries:
            # if there is an invoice for the same delivery, append to that invoice
            invoice_id = deliveries[picking.delivery_id.id]
            invoice = invoice_obj.browse(cr, uid, invoice_id.id, context=context)
            invoice_vals_group = self._prepare_invoice_delivery(cr, uid, picking, partner, invoice, context=context)
            invoice_obj.write(cr, uid, [invoice_id], invoice_vals_group, context=context)
        elif group and group_hash in invoices_group:
            # otherwise, append to the group invoice if there is one
            invoice_id = invoices_group[group_hash]
            invoice = invoice_obj.browse(cr, uid, invoice_id)
            invoice_vals_group = self._prepare_invoice_group(cr, uid, picking, partner, invoice, context=context)
            invoice_obj.write(cr, uid, [invoice_id], invoice_vals_group, context=context)
        else:
            # or create the invoice
            invoice_vals = self._prepare_invoice(cr, uid, picking, partner, inv_type, journal_id, context=context)
            invoice_id = invoice_obj.create(cr, uid, invoice_vals, context=context)
            invoices_group[group_hash] = invoice_id
            if picking.type == 'out':
                deliveries[picking.delivery_id.id] = invoice_id
        res[picking.id] = invoice_id
        for move_line in picking.move_lines:
            if move_line.state == 'cancel':
                continue
            if move_line.scrapped:
                # do no invoice scrapped products
                continue
            vals = self._prepare_invoice_line(cr, uid, group, picking, move_line,
                                              invoice_id, invoice_vals, context=context)
            if vals:
                invoice_line_id = invoice_line_obj.create(cr, uid, vals, context=context)
                self._invoice_line_hook(cr, uid, move_line, invoice_line_id)

        invoice_obj.button_compute(cr, uid, [invoice_id], context=context,
                                   set_total=(inv_type in ('in_invoice', 'in_refund')))
        self.write(cr, uid, [picking.id], {
            'invoice_state': 'invoiced',
        }, context=context)
        self._invoice_hook(cr, uid, picking, invoice_id)
    self.write(cr, uid, res.keys(), {
        'invoice_state': 'invoiced',
    }, context=context)
    return res

stock_picking.action_invoice_create = action_invoice_create

class account_invoice(orm.Model):
    _inherit = 'account.invoice'

    _columns = {
        'delivery_id': fields.function(lambda s, *a, **k: s._delivery_id(*a, **k),
                                       type='many2one', relation='delivery.delivery',
                                       string='Spedizione'),
   }

    def _delivery_id(self, cr, uid, ids, field, arg, context=None):
        cr.execute("""
            SELECT invoice_id, id
            FROM delivery_delivery
            WHERE invoice_id IN %s
        """, [tuple(ids)])
        res = dict(cr.fetchall())
        res.update(dict.fromkeys(set(ids) - set(res), False))
        return res

    def action_number(self, cr, uid, ids, context=None):
        # when assigning a number to the invoice, we change the name of the delivery as well
        res = super(account_invoice, self).action_number(cr, uid, ids, context=context)
        dlv = self.pool.get('delivery.delivery')
        for inv in self.browse(cr, uid, ids, context=context):
            if inv.delivery_id:
                dlv.write(cr, uid, inv.delivery_id.id, {'name': inv.number}, context=context)
        return res

    def unlink(self, cr, uid, ids, context=None):
        # when deleting an invoice, we need to reopen the delivery
        dlv = self.pool.get('delivery.delivery')
        pk = self.pool.get('stock.picking')
        for inv in self.browse(cr, uid, ids, context=context):
            if inv.delivery_id:
                dlv.write(cr, uid, inv.delivery_id.id, {'state': 'open', 'name': '/'})
        return super(account_invoice, self).unlink(cr, uid, ids, context=context)
