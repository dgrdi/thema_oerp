import locale
from dateutil.parser import parse
import datetime

def date_locale(date, context):
    loc = locale.getlocale(locale.LC_TIME)
    lang = context.get('lang') if context is not None else None
    if lang:
        locale.setlocale(locale.LC_TIME, lang)
    df = locale.nl_langinfo(locale.D_FMT)
    if lang:
        locale.setlocale(locale.LC_TIME, loc)
    if not isinstance(date, datetime.date):
        date = datetime.datetime.strptime(date, '%Y-%m-%d')
    return date.strftime(df)

