from functools import wraps
from openerp.osv.orm import BaseModel


def check_flag_validate(val_func):
    if not hasattr(val_func, 'ctx_flag'):
        @wraps(val_func)
        def wrap_val(self, cr, uid, ids, context=None):
            if context and context.get('novalidate', None):
                return
            return val_func.__get__(self)(cr, uid, ids, context=context)
        wrap_val.ctx_flag = True
        return wrap_val
    else:
        return val_func

BaseModel._validate = check_flag_validate(BaseModel._validate)

def check_flag_store(store_func):
    if not hasattr(store_func, 'ctx_flag'):
        @wraps(store_func)
        def wrap_store(self, cr, uid, ids, fields, context):
            if context and context.get('no_store_compute', None):
                return True
            return store_func.__get__(self)(cr, uid, ids, fields, context)
        wrap_store.ctx_flag = True
        return wrap_store
    else:
        return store_func

BaseModel._store_set_values = check_flag_store(BaseModel._store_set_values)
