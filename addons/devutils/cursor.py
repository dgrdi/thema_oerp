from openerp import sql_db
import sqlparse
from sqlparse.tokens import Token
import re
import logging
logger = logging.getLogger(__name__)

plmatch = re.compile(r'^%\((.*)\)s$')

class SplitError(Exception):
    pass

def process_query(query, params):
    if not params:
        return query, params
    if hasattr(params, 'items'):
        list = False
        pm = params.items()
    else:
        list = True
        pm = enumerate(params)
    keys = []
    for key, val in pm:
        if isinstance(val, tuple) and len(val) >= 100 and isinstance(val[0], (int, long)):
            keys.append(key)
    if keys:
        pquery = sqlparse.parse(query)
        tokens = [tk for smt in pquery for tk in smt.flatten() if tk.ttype == Token.Name.Placeholder]
        try:
            if list:
                return split_tuples_list(pquery, tokens, params, keys)
            else:
                return split_tuples_dict(pquery, tokens, params, keys)
        except:
            logger.exception('Error during query splitting')
    return query, params



def split_tuples_list(pquery, tokens, params, indexes):
    repl_par = []
    for i, el in enumerate(params):
        if i not in indexes:
            repl_par.append(el)
        else:
            try:
                pack, orphans, data = group_data(params[i])
                repl_tok = split_tuple(tokens[i], pack, orphans)
            except SplitError:
                repl_par.append(el)
            else:
                replace_token(tokens[i], repl_tok)
                repl_par.extend(data)
    return ';\n'.join(unicode(s) for s in pquery), repl_par

def split_tuples_dict(pquery, tokens, params, keys):
    params = params.copy()
    tk = {}
    for t in tokens:
        m = plmatch.match(t.value)
        if not m:
            raise ValueError('Cannot parse placeholder: %s', t.value)
        tk.setdefault(m.group(1),[]).append(t)
    for key in keys:
        try:
            pack, orphans, data = group_data(params[key])
        except SplitError:
            pass
        else:
            params.pop(key)
            params.update({'{name}_{ni}__'.format(name=key, ni=i+1): el for i, el in enumerate(data)})
            for token in tk[key]:
                repl_tok  = split_tuple(token, pack, orphans, name=key)
                replace_token(token, repl_tok)
    return ';\n'.join(unicode(s) for s in pquery), params

def replace_token(token, repl):
    idx_iden, idx, iden = find_iden(token)
    tks = token.parent.tokens
    tks = tks[:idx_iden-2] + repl + tks[idx+1:]
    token.parent.tokens = tks


def find_iden(token):
    parent = token.parent
    idx = parent.token_index(token)
    prev = parent.token_prev(idx)
    if not prev.is_keyword or prev.value.lower() != 'in':
        raise SplitError()
    idx_iden = parent.token_index(prev)
    iden = parent.token_prev(idx_iden)
    return idx_iden, idx, iden



def split_tuple(token, pack, orphans, name=None):
    idx_iden, idx_tk, iden = find_iden(token)
    iden = unicode(iden)
    cond = []
    ni = 0
    if name is None:
        pl = '%s'
    if pack:
        for el in pack:
            if name:
                ni += 1
                pl = '%({name}_{ni}__)s'.format(name=name, ni=ni)
            cond.append('({i} >= {pl} AND {i} <= {pl})'.format(i=iden, pl=pl))
    if orphans:
        if name:
            ni += 1
            pl = '%({name}_{ni}__)s'.format(name=name, ni=ni)
        cond.append('({i} IN {pl})'.format(i=iden, pl=pl))
    smt = '(%s)' % ' OR '.join(cond)
    smt = sqlparse.parse(smt)[0]
    return smt.tokens


def group_data(data):
    data = sorted(data)
    pack = []
    orphans = []
    data = iter(data + [object()])
    lower = next(data)
    upper = lower
    i = lower
    for el in data:
        i += 1
        if i != el:
            if lower == upper:
                orphans.append(lower)
            else:
                pack.append((lower, upper))
            i = el
            lower = el; upper = el
        else:
            upper = el
    data = [e for els in pack for e in els]
    if orphans:
        data += [tuple(orphans)]
    return pack, orphans, data

class SplitCursor(sql_db.Cursor):
    def execute(self, query, params=None, log_exceptions=None):
        query, params = process_query(query, params)
        return super(SplitCursor, self).execute(query, params=params, log_exceptions=log_exceptions)


sql_db.Cursor = SplitCursor

