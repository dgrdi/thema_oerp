from openerp.osv import orm, fields
from osv.orm import MetaModel

class ir_module(orm.Model):
    _inherit = 'ir.module.module'

    def module_uninstall(self, cr, uid, ids, context=None):
        for mod in self.browse(cr, uid, ids, context=context):
            for obj in MetaModel.module_to_models[mod.name]:
                if hasattr(obj, '_uninstall'):
                    obj._uninstall(cr)
        return super(ir_module, self).module_uninstall(cr, uid, ids, context=None)
