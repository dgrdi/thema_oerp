from openerp.osv import orm, fields
from openerp import netsvc
from functools import wraps
from openerp.pooler import get_db_and_pool

import time

USERS = {}
READ_DATA = {}


class profiling_profile(orm.TransientModel):
    _name = 'profiling.profile'
    _order = 'create_date desc'

    _columns = {
        'name': fields.char('Name'),
        'create_uid': fields.many2one('res.users', 'User'),
        'create_date': fields.datetime('Timestamp'),
        'service_name': fields.char('Service name'),
        'method': fields.char('Method'),
        'params': fields.char('Params'),
        'ttime': fields.float(digits=(16, 4), string='Total time'),
        'profile': fields.html('Profile'),
        'fnct_profile': fields.html('Functional read profile'),
    }

    def enable(self, cr, uid, context=None):
        USERS[uid] = cr.dbname
        return True

    def disable(self, cr, uid, context=None):
        USERS.pop(uid, None)
        return True

    def clear(self, cr, uid, context=None):
        return self.unlink(cr, uid, self.search(cr, uid, [('create_uid', '=', uid)]))

    def results(self, cr, uid, context=None):
        return {
            'name': 'Profiling results',
            'type': 'ir.actions.act_window',
            'res_model': 'profiling.profile',
            'domain': "[('create_uid', '=', %s)]" % uid,
            'view_mode': "tree,form"
        }


def profile_dispath(dispatch_rpc):
    @wraps(dispatch_rpc)
    def wrapper(service_name, method, params):
        uid_index = None
        name = None
        if service_name == 'object' and params[3] != 'profiling.profile':
            uid_index = 1
            name = '%s: %s' % (params[3], params[4])
        if uid_index and params[uid_index] in USERS:
            uid = params[uid_index]
            import yappi
            stime = time.time()
            yappi.start()
            try:
                return dispatch_rpc(service_name, method, params)
            finally:
                yappi.stop()
                if uid in USERS:
                    ttime = time.time() - stime
                    fnct_data = fnct_read_profile(uid)
                    db, pool = get_db_and_pool(USERS[uid])
                    cr = db.cursor()
                    pool.get('profiling.profile').create(cr, uid, {
                        'name': name,
                        'service_name': service_name,
                        'method': method,
                        'params': params,
                        'ttime': ttime,
                        'profile': format(yappi.get_func_stats()),
                        'fnct_profile': format_fnct(fnct_data) if fnct_data else False,
                    })
                    cr.commit()
                    cr.close()
        else:
            return dispatch_rpc(service_name, method, params)

    return wrapper


def profile_fnct_get(get):
    @wraps(get)
    def wrapper(self, cr, obj, ids, name, uid=False, context=None, values=None):
        if uid not in USERS:
            return get(self, cr, obj, ids, name, uid, context, values)
        stime = time.time()
        try:
            return get(self, cr, obj, ids, name, uid, context, values)
        finally:
            stime = time.time() - stime
            name = '%s: %s' % (obj._name, name)
            vals = READ_DATA.setdefault(uid, {}).setdefault(name, {'ncall': 0, 'ttot': 0,
                                                                   'name': name,
                                                                   'ids': 0})
            vals.update({
                'ncall': vals['ncall'] + 1,
                'ttot': vals['ttot'] + stime,
                'ids': vals['ids'] + len(ids)
            })

    return wrapper


fields.function.get = profile_fnct_get(fields.function.get)


def format(data):
    skeleton = """
        <table style="border-collapse: collapse; border: 1px solid black" padding="5">
            <tr>
                <th>name</th>
                <th>ncall</th>
                <th>ttot</th>
                <th>tsub</th>
                <th>tavg</th>
            </tr>
            {0}
        </table>
    """
    line = """
        <tr>
        <td>{name}</td>
            <td>{ncall}</td>
            <td>{ttot}</td>
            <td>{tsub}</td>
            <td>{tavg}</td>
        </tr>
    """
    lines = map(lambda l: line.format(name=l.full_name,
                                      ncall=l.ncall,
                                      ttot=l.ttot,
                                      tsub=l.tsub,
                                      tavg=l.tavg), data)
    return skeleton.format('\n'.join(lines))


def format_fnct(data):
    skeleton = """
        <table style="border-collapse: collapse; border: 1px solid black" padding="5">
            <tr>
                <th>name</th>
                <th>ncall</th>
                <th>ttot</th>
                <th>tid</th>
            </tr>
            {0}
        </table>
    """
    line = """
        <tr>
        <td>{name}</td>
            <td>{ncall}</td>
            <td>{ttot}</td>
            <td>{tid}</td>
        </tr>
    """
    lines = map(lambda x: line.format(**x), data)
    return skeleton.format('\n'.join(lines))


def fnct_read_profile(uid):
    data = READ_DATA.pop(uid, None)
    if not data:
        return None
    data = data.values()
    data.sort(key=lambda d: d['ttot'], reversed=True)
    for d in data:
        d['tid'] = d['ttot'] / d['ids']
    return data

netsvc.dispatch_rpc = profile_dispath(netsvc.dispatch_rpc)


