from openerp.tests.common import SingleTransactionCase
from testutils.factories.partner import PartnerFactory
import datetime

class TestOrm(SingleTransactionCase):

    fpartner = PartnerFactory({
        'date': '2014-01-01',
    })

    @classmethod
    def setUpClass(cls):
        super(TestOrm, cls).setUpClass()
        cls.partners = [cls.fpartner.create(cls.cr, cls.uid) for _ in range(3)]
        cls.p = cls.partners[0]

    def test_browse_single(self):
        res = self.p.wobj.b_browse(self.partners[0])
        self.assertIsInstance(res.date, datetime.date)

    def test_browse_multiple(self):
        res = self.p.wobj.b_browse(self.partners)
        self.assertIsInstance(res[0].date, datetime.date)

    def test_browse_across_one2many(self):
        self.fpartner.create({
            'parent_id': self.p
        })
        res = self.p.wobj.b_browse(self.p)
        self.assertIsInstance(res.child_ids[0].date, datetime.date)

    def test_read(self):
        res = self.p.wobj.b_read(self.p, fields=('date',))
        self.assertIsInstance(res['date'], datetime.date)

    def test_read_multiple(self):
        res = self.p.wobj.b_read(self.partners, fields=('date',))
        self.assertIsInstance(res[0]['date'], datetime.date)

    def test_write(self):
        self.p.wobj.b_write(self.p, {'date': datetime.date(2014,1,10)})
        self.assertEqual(self.p.browse.date, '2014-01-10')

    def test_create(self):
        p = self.p.wobj.b_create({'name': 'test', 'date': datetime.date(2014, 1, 1)})
        p = self.p.wobj.browse(p)
        self.assertEqual(p.date, '2014-01-01')


    def test_read_scalar(self):
        res = self.p.wobj.read_field(self.p, 'name')
        self.assertEqual(res, self.p.browse.name)

    def test_read_scalar_multiple(self):
        res = self.p.wobj.read_field(self.partners, 'name')
        self.assertDictEqual(res, {p: p.browse.name for p in self.partners})

    def test_read_map(self):
        res = self.p.wobj.read_map(self.partners, fields=('name',))
        red = self.p.wobj.read(self.partners, fields=('name',))
        self.assertDictEqual(res, {o['id']: o for o in red})

    def test_search_read(self):
        res = self.p.wobj.search_read([('id', '=', self.p)], fields=('name',))
        self.assertListEqual(res, [{'name': self.p.browse.name, 'id': self.p}])

    def test_search_browse(self):
        res = self.p.wobj.search_browse([('id', '=', self.p)])
        self.assertEqual(res[0].name, self.p.browse.name)


