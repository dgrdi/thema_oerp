import unittest2
from devutils.dtypemap import mapper
from openerp.osv import fields
import datetime
from decimal import Decimal

class TestMapper(unittest2.TestCase):

    def test_date_from(self):
        field = fields.date()
        date = '2014-01-01'
        res = mapper.from_openerp(field, date)
        self.assertEqual(res, datetime.date(2014, 1, 1))

    def test_date_to(self):
        field = fields.date()
        date = datetime.date(2014, 1, 1)
        res = mapper.to_openerp(field, date)
        self.assertEqual(res, '2014-01-01')

    def test_datetime_from(self):
        field = fields.datetime()
        dt = datetime.datetime(2014, 1, 1)
        res = mapper.from_openerp(field, dt.isoformat())
        self.assertEqual(res, dt)

    def test_datetime_to(self):
        field = fields.datetime()
        dt = datetime.datetime(2014, 1,1)
        res = mapper.to_openerp(field, dt)
        self.assertEqual(res, dt.isoformat())

    def test_function(self):
        field = fields.function(lambda *a: None, type='datetime')
        dt = datetime.datetime(2014, 1, 1)
        res = mapper.to_openerp(field, dt)
        self.assertEqual(res, dt.isoformat())

    def test_float_float(self):
        field = fields.float()
        f = 1.0
        res = mapper.from_openerp(field, f)
        self.assertIsInstance(res, float)

    def test_decimal_to(self):
        field = fields.float(digits=(10, 2))
        f = Decimal('1.0')
        res = mapper.to_openerp(field, f)
        self.assertIsInstance(res, float)

    def test_decimal_from(self):
        field = fields.float(digits=(10, 2))
        f = 1.0
        res = mapper.from_openerp(field, f)
        self.assertIsInstance(res, Decimal)

    def test_decimal_compute(self):
        field = fields.float(digits_compute = lambda *a: (10, 2))
        field.digits_change(None)
        f = Decimal('1.0')
        res = mapper.to_openerp(field, f)
        self.assertIsInstance(res, float)
