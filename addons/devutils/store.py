from logging import getLogger

logger = getLogger(__name__)
import datetime, time
from openerp import SUPERUSER_ID
from functools import wraps
from openerp.osv.orm import BaseModel

@wraps(BaseModel._store_set_values)
def store_set_values(self, cr, uid, ids, fields, context):
    """Calls the fields.function's "implementation function" for all ``fields``, on records with ``ids`` (taking care of
       respecting ``multi`` attributes), and stores the resulting values in the database directly."""
    if not ids:
        return True
    #logger.debug('Store recompute: performing magic...')
    ## BEGIN MAGIC
    field_flag = False
    field_dict = {}
    if self._log_access:
        cr.execute('select id,write_date from '+self._table+' where id IN %s', (tuple(ids),))
        res = cr.fetchall()
        for r in res:
            if r[1]:
                field_dict.setdefault(r[0], [])
                res_date = time.strptime((r[1])[:19], '%Y-%m-%d %H:%M:%S')
                write_date = datetime.datetime.fromtimestamp(time.mktime(res_date))
                for i in self.pool._store_function.get(self._name, []):
                    if i[5]:
                        up_write_date = write_date + datetime.timedelta(hours=i[5])
                        if datetime.datetime.now() < up_write_date:
                            if i[1] in fields:
                                field_dict[r[0]].append(i[1])
                                if not field_flag:
                                    field_flag = True
        del res
    #logger.debug('Store recompute: magic complete; calculating values...')
    todo = {}
    keys = []
    for f in fields:
        if self._columns[f]._multi not in keys:
            keys.append(self._columns[f]._multi)
        todo.setdefault(self._columns[f]._multi, [])
        todo[self._columns[f]._multi].append(f)
    # keys = a list of field 'multi' keys, and False (for fields with no 'multi')
    result = {}
    m2ofields = set(c for c, v in self._columns.items() if v._type == 'many2one')
    def fix_m2o(v):
        for f in [f2 for f2 in m2ofields if f2 in v]:
            if not isinstance(v[f], (int, long)):
                v[f] = v[f][0]
        return v

    for key in keys:
        val = todo[key]
        if key:
            #logger.debug('Calculating values for fields: %s', val)
            # use admin user for accessing objects having rules defined on store fields
            r = self._columns[val[0]].get(cr, self, ids, val, SUPERUSER_ID, context=context)
            # r is a map from ids to maps from field names to values
            for id, values in r.iteritems():
                result.setdefault(id, {}).update(fix_m2o(values))
        else:
            for f in val:
                #logger.debug('Calculating values for field: %s', f)
                r = self._columns[f].get(cr, self, ids, f, SUPERUSER_ID, context=context)
                # r is a map from ids to values
                for id, val in r.iteritems():
                    if f in m2ofields and not isinstance(val, (int, long)):
                        val = val[0]
                    result.setdefault(id, {})[f] = val
    # remove fields marked by MAGIC
    #logger.debug('Values calculated.')
    special = {}
    special_ids = set()

    if field_flag:
        for id, values in result.iteritems():
            for f in field_dict[id]:
                values.pop(f, None)
            special.setdefault(tuple(sorted(field_dict[id])), []).append(id)
            special_ids.add(id)

    def split_by_col():
        ids = set(result.iterkeys()) - special_ids
        if ids:
            cols = result[iter(ids).next()].keys()
            yield cols, ids
        for ids in special.itervalues():
            cols = result[iter(ids).next()].keys()
            yield cols, ids

    #logger.debug('Writing values: %s queries', len(special) + 1)
    for cols, ids in split_by_col():
        #logger.debug('Building query...')
        fdef = ', '.join(['"%s"' % c for c in cols])
        val_placeh = '(%s)' % ', '.join([self._columns[c]._symbol_set[0] for c in cols] + ['%s'])
        get_values = lambda v: [self._columns[c]._symbol_set[1](v[c]) for c in cols]

        values = []
        for id in ids:
            values.append(get_values(result[id]) + [id])

        table = self._table
        upst = ', '.join(['"%s"="p_updata"."%s"' % (c, c) for c in cols])

        def chunkify(lst):
            ln = len(lst)
            for i in range(0, ln, 200000):
                upper = min(i+200000, ln)
                yield upper-i, (lst[j] for j in range(i, upper))


        cr.execute("""
            CREATE TEMP TABLE p_updata AS
            SELECT {fdef}, 1 t_imp_id
            FROM {table}
            LIMIT 0;
        """.format(**locals()))

        for ln, chunk in chunkify(values):

            placeholders = ',\n'.join([val_placeh for _ in range(ln)])
            cr.execute("""
                INSERT INTO p_updata
                VALUES
                {placeholders};
            """.format(**locals()), [v for vs in chunk for v in vs])
        #logger.debug('Executing...')
        cr.execute("""
            UPDATE {table}
            SET {upst}
            FROM p_updata WHERE {table}.id = p_updata.t_imp_id;

            DROP TABLE p_updata;
        """.format(**locals()))
    #logger.debug('Done!')
    return True

BaseModel._store_set_values = store_set_values