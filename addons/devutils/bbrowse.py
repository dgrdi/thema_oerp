from openerp.osv.orm import browse_record, browse_record_list, browse_null
from openerp.osv import fields
from itertools import imap
from .dtypemap import mapper

def browse_list_subclass(superclass):
    class BList(superclass):
        def __init__(self, lst, context=None):
            lst = imap(BetterBrowseRecord._from_browse_record, lst)
            super(BList, self).__init__(lst, context)
    return BList

BetterBrowseRecordList = browse_list_subclass(browse_record_list)

class BetterBrowseRecord(browse_record):
    _convert = True

    @classmethod
    def _from_browse_record(cls, br, convert=True):
        if isinstance(br, browse_null):
            return br
        return cls(br._cr, br._uid, br._id, br._model, br._cache, context=br._context,
                   list_class=br._list_class, fields_process=br._fields_process)

    def __getitem__(self, item):
        itm = super(BetterBrowseRecord, self).__getitem__(item)
        if not self._convert:
            return itm
        if item == 'id':
            return itm
        else:
            col = self._model._all_columns.get(item).column
        if col._type == 'many2one':
            return self._from_browse_record(itm)
        if col._type in ('one2many', 'many2many'):
            return BetterBrowseRecordList(itm)
        return mapper.from_openerp(col, itm)

