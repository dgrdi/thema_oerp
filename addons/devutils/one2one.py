from openerp.osv import orm, fields

class One2One(orm.AbstractModel):
    _name = 'devutils.one2one'
    LINK_FIELD = []

    def create(self, cr, user, vals, context=None):
        self.check_access_rights(cr, user, 'create')
        cr.execute("INSERT INTO ")