from openerp.osv import fields
from openerp.pooler import get_pool

from .tools import call_once


class sql_function(fields.function):
    def __init__(self, string, **kwargs):
        self.init = call_once(self.init)
        args = {
            'fnct_search': self._fnct_search,
            'string': string,
        }
        args.update(kwargs)
        super(sql_function, self).__init__(self._fnct, **args)

    def init(self, pool, cr):
        pass

    def query(self, fields):
        """
        Should return a SQL query string where the first column is named id,
        and the others are named after the fields. The query should have no WHERE clause.
        :param fields: list of field names.
        """
        pass

    def _get_query(self, fields):
        q = self.query(fields)
        if isinstance(q, tuple):
            q, p = q
        else:
            q, p = q, []
        return q, p

    def query_read(self, fields, ids):
        query, params = self._get_query(fields)
        return "SELECT * FROM ({0}) "" a WHERE id IN %s".format(query), params + [tuple(ids)]

    def query_search(self, field, op, val):
        where, args = self.where_calc(field, op, val)
        query, params = self._get_query([field])
        return "SELECT DISTINCT id FROM ({0}) a ".format(query) + where, params + args

    def where_calc(self, field, op, val):
        argmap = self.op_arg_map.get(op)
        if not argmap:
            assert op in self.simple_ops
        else:
            val = argmap(val)
        op = self.op_map.get(op, op)
        return "WHERE {0} {1} %s".format(field, op), [val]

    simple_ops = ['<', '>', '<=', '>=', '<>', '!=', '=']

    op_map = {
        '!=': '<>'
    }

    op_arg_map = {
        'like': str,
        'ilike': str,
        'not like': str,
        'not ilike': str,
        'in': tuple,
        'not in': tuple
    }

    def _fnct(self, obj, cr, uid, ids, fields, arg, context=None):
        self.init(obj, cr)
        if not ids:
            return {}
        if not self._multi:
            fields = [fields]
        cr.execute(*self.query_read(fields, ids))
        return self.process_result(cr.dictfetchall(), obj, fields, ids)

    def process_result(self, data, obj, fields, ids):
        tomany = self.tomany_fields(obj, fields)
        res = {}
        for row in data:
            id = row.pop('id')
            if id not in res:
                res[id] = val = {}
            for k, v in row.items():
                if k in tomany:
                    val.setdefault(k, set()).add(v)
                else:
                    val[k] = v
        for field in tomany:
            for val in res.values():
                val[field] = list(val[field])
        default = self.get_default(fields, tomany)
        for id in set(ids) - set(res):
            res[id] = default.copy()
        if not self._multi:
            res = {k: v.pop(fields[0]) for k, v in res.iteritems()}
        return res

    def get_default(self, fields, tomany, tomany_class=list):
        return {f: tomany_class() if f in tomany else False for f in fields}


    def _fnct_search(self, obj, cr, uid, obj2, field, domain, context=None):
        self.init(obj, cr)
        assert len(domain) == 1
        field, op, val = domain[0]
        cr.execute(*self.query_search(field, op, val))
        return [('id', 'in', [el[0] for el in cr.fetchall()])]

    def tomany_fields(self, obj, fields):
        return [f for f in fields if obj._all_columns[f].column._type in ('many2many', 'one2many')]

