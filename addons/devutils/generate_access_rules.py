import argparse
import sys
import csv

FIELDS = ['id', 'name', 'model_id:id', 'group_id:id', 'perm_read', 'perm_write', 'perm_create', 'perm_unlink', ]

parser = argparse.ArgumentParser()
parser.add_argument('-r', help='Output headers', action='store_true')
parser.add_argument('-g', help='Groups', action='append')
parser.add_argument('model', nargs='+')

writer = csv.writer(sys.stdout, quoting=csv.QUOTE_NONNUMERIC)

def output_headers():
    writer.writerow(FIELDS)

def model_stub(model, group):
    uname = model.replace('.', '_')
    ugroup = group.replace('.', '_')
    out = [
        'access_%s_%s' % (uname, ugroup),
        'access.%s.%s' % (model, group),
        'model_%s' % uname,
        group,
        1,
        0,
        0,
        0
    ]
    writer.writerow(out)

if __name__ == '__main__':
    v = parser.parse_args(sys.argv[1:])
    if v.r:
        output_headers()
    for mod in v.model:
        for g in v.g:
            model_stub(mod, g)
        if not v.g:
            model_stub(mod, 'base.group_user')



