from openerp import SUPERUSER_ID
from openerp.osv import orm, fields
from functools import wraps


class CopyOnWrite(orm.AbstractModel):
    """
    Records of this abstract model cannot be created. Instead, it will appear as though every id already exists,
    with the default values. When write() is called, an actual database row is created.

    This can be used in another model's _inherits map, to attach additional data to that model without polluting
    its table or duplicating a lot of data. (For example, if we want a view showing all sales orders, and the view needs
    additional fields, this avoids the need to add those fields to the sales order model or creating a temporary record
    for every sales order).

    """

    _name = 'devutils.copyonwrite'

    def init(self, cr):
        if self._auto:
            self._create_default(cr, 0)

    def _create_default(self, cr, id):
        if not self.search(cr, SUPERUSER_ID, [('id', '=', id)]):
            cid = super(CopyOnWrite, self).create(cr, SUPERUSER_ID, {})
            cr.execute("UPDATE \"{0}\" set id=%s WHERE id=%s".format(self._table), [id, cid])
        else:
            super(CopyOnWrite, self).write(cr, SUPERUSER_ID, id,
                                           self.default_get(cr, SUPERUSER_ID, fields_list=self._all_columns.keys()))

    def missing_ids(self, cr, ids):
        if not ids:
            return []
        cr.execute("SELECT id FROM \"{0}\" WHERE id IN %s".format(self._table), [tuple(ids)])
        missing = set(ids) - {el[0] for el in cr.fetchall()}
        return list(missing)

    def read(self, cr, user, ids, fields=None, context=None, load='_classic_read'):
        if isinstance(ids, (int, long)):
            ids = [ids]
            unpak = True
        else:
            unpak = False
        missing = self.missing_ids(cr, ids)
        to_read = list(set(ids) - set(missing))
        if to_read:
            values = super(CopyOnWrite, self).read(cr, user, list(set(ids) - set(missing)),
                                                   fields=fields, context=context,
                                                   load=load)
        else:
            values = []
        if missing:
            mval = super(CopyOnWrite, self).read(cr, user, 0, fields=fields, context=context, load=load)
            for id in missing:
                v = mval.copy()
                v['id'] = id
                values.append(v)
        if unpak:
            values = values[0]
        return values

    def create(self, cr, user, vals, context=None):
        raise NotImplementedError("Cannot create a copyonwrite model record. Use write() instead.")

    def write(self, cr, user, ids, vals, context=None):
        if isinstance(ids, (int, long)):
            ids = [ids]
        for id in self.missing_ids(cr, ids):
            self._create_default(cr, id)
        return super(CopyOnWrite, self).write(cr, user, ids, vals, context=context)


def many2one_id(get):
    """When using id as a link to a parent object (one in _inherits), id is returned formatted as
    a many2one. This should fix that."""

    @wraps(get)
    def wrapper(self, cr, obj, ids, name, user=None, context=None, values=None):
        if name == 'id':
            return {id: id for id in ids}
        return get(self, cr, obj, ids, name, user=user, context=context, values=values)

    return wrapper


fields.many2one.get = many2one_id(fields.many2one.get)
