from openerp.osv import fields
import datetime
from dateutil import parser
from decimal import Decimal
from openerp.tools.float_utils import float_repr

class Mapper(object):
    instance = None

    type_map = {}

    def __new__(cls):
        if not cls.instance:
            cls.instance = super(Mapper, cls).__new__(cls)
        return cls.instance


    def to_openerp(self, field, value):
        typ = field._type
        if typ in self.type_map:
            return self.type_map[typ].to_openerp(field, value)
        return value

    def from_openerp(self, field, value):
        typ = field._type
        if typ in self.type_map:
            return self.type_map[typ].from_openerp(field, value)
        return value

    def dict_to_openerp(self, obj, dct):
        return {k: self.to_openerp(obj._columns.get(k), v) for k, v in dct.items()}

    def dict_from_openerp(self, obj, dct):
        return {k: self.from_openerp(obj._columns[k], v) if k in obj._columns else v
                for k, v in dct.items()}

    def register(self, type):
        def dec(obj):
            self.type_map[type] = obj()
        return dec


mapper = Mapper()

@mapper.register('date')
class DateMapper(object):

    def to_openerp(self, field, value):
        if isinstance(value, datetime.date):
            return value.strftime('%Y-%m-%d')
        return value

    def from_openerp(self, field, value):
        if value:
            return datetime.datetime.strptime(value, '%Y-%m-%d').date()
        return value

@mapper.register('datetime')
class DateTimeMapper(object):

    def to_openerp(self, field, value):
        if isinstance(value, datetime.datetime):
            return value.isoformat()
        return value

    def from_openerp(self, field, value):
        if value:
            return parser.parse(value)
        return value

@mapper.register('float')
class FloatMapper(object):

    def to_openerp(self, field, value):
        assert field.digits or not field.digits_compute
        if isinstance(value, Decimal):
            return float(value)
        return value

    def from_openerp(self, field, value):
        assert field.digits or not field.digits_compute
        if isinstance(value, float) and field.digits:
            return Decimal(float_repr(value, precision_digits=field.digits[1]))
        return value