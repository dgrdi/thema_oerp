import time

class Node(object):

    def __init__(self, nodes):
        self.nodes = nodes
        self.path = []
        self.dead = []
        self.weight = 0

    def forwards(self):
        node = self.nodes.pop()
        self.path.append(node)
        self.weight += node[1]

    def backwards(self):
        node = self.path.pop()
        self.weight -= node[1]
        self.dead.append(node)

    def sideways(self):
        self.nodes = self.dead
        node = self.nodes.pop()
        self.dead = []

    def kill(self):
        self.dead = self.nodes
        self.nodes = []


def knapsack(nodes, target, tolerance=0.05, limit=10):
    tolerance = float(target) * tolerance
    n = Node(nodes[:])
    minerr = float('inf')
    solution = None
    startt = time.time()
    while True:
        if time.time() - startt > limit:
            break
        error = n.weight - target
        if abs(error) < minerr:
            minerr = abs(error)
            solution = [el[0] for el in n.path]
            if minerr < tolerance:
                break
        if error > minerr:
            n.kill()
        if n.nodes:
            n.forwards()
        elif n.path:
            n.backwards()
        elif n.dead:
            n.sideways()
        else:
            break
    return solution

