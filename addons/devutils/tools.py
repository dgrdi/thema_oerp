from functools import wraps
from openerp.osv.orm import browse_record

class Container(object):
    __slots__ = ['value']

    def __init__(self, value=None):
        self.value = value

def call_once(func):
    null = object()
    res = Container(null)

    @wraps(func)
    def wrapper(*a, **k):
        if res.value is null:
            res.value = func(*a, **k)
        return res.value
    return wrapper


def flatten_browse(list):
    def isiterable(x):
        return hasattr(x, "__iter__") and not isinstance(x, browse_record) and not isinstance(x, dict)

    r = []
    for e in list:
        if isiterable(e):
            map(r.append, flatten_browse(e))
        else:
            r.append(e)
    return r
