from openerp.osv import orm, fields
from openerp.addons.jasper_reports import jasper_report
import xml.dom.minidom
from xmlrpclib import Binary

class report_xml(orm.Model):
    _inherit = 'ir.actions.report.xml'

    def generate_data_xml(self, cr, uid, action, report_ids, context=None):
        report_data = {}
        if 'report_type' in action:
            report_data['report_type'] = action['report_type']
        if 'datas' in action:
            if 'ids' in action['datas']:
                report_ids = action['datas'].pop('ids')
            report_data.update(action['datas'])
        rp = jasper_report.Report('report.' + action['report_name'], cr, uid, report_ids, report_data, context=context)
        x = xml.dom.minidom.parseString(rp.get_data())
        return x.toprettyxml()

    def generate_data_buffer(self, cr, uid, action, report_ids, context=None):
        report_data = {}
        if 'report_type' in action:
            report_data['report_type'] = action['report_type']
        if 'datas' in action:
            if 'ids' in action['datas']:
                report_ids = action['datas'].pop('ids')
            report_data.update(action['datas'])

        rp = jasper_report.Report('report.' + action['report_name'], cr, uid, report_ids, report_data, context=context)
        return Binary(rp.get_data(format='protoBuffer'))
