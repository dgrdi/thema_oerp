from openerp.osv import orm
from openerp.osv.orm import except_orm
from .bbrowse import BetterBrowseRecordList, BetterBrowseRecord
from .dtypemap import mapper
import functools
from openerp.addons.bulk_import import fnct_write

class BaseModelExt:

    def b_browse(self, cr, uid, select, context=None, fields_process=None):
        res = self.browse(cr, uid, select, context=context, list_class=BetterBrowseRecordList, fields_process=fields_process)
        if not isinstance(res, BetterBrowseRecordList):
            res =  BetterBrowseRecord._from_browse_record(res)
        return res

    def b_read(self, cr, user, ids, fields=None, context=None, load='_classic_read'):
        res = self.read(cr, user, ids, fields=fields, context=context, load=load)
        if isinstance(res, dict):
            return mapper.dict_from_openerp(self, res)
        else:
            return map(functools.partial(mapper.dict_from_openerp, self), res)

    def b_write(self, cr, user, ids, vals, context=None):
        vals = mapper.dict_to_openerp(self, vals)
        return self.write(cr, user, ids, vals, context=context)

    def b_create(self, cr, user, vals, context=None):
        vals = mapper.dict_to_openerp(self, vals)
        return self.create(cr, user, vals, context=context)

    def read_field(self, cr, uid, ids, field, context=None):
        res = self.read(cr, uid, ids, fields=(field, ), context=context)
        if isinstance(res, dict):
            return res[field]
        else:
            return {o['id']: o[field] for o in res}

    def b_read_field(self, cr, uid, ids, field, context=None):
        res = self.b_read(cr, uid, ids, fields=(field, ), context=context)
        if isinstance(res, dict):
            return res[field]
        else:
            return {o['id']: o[field] for o in res}

    def read_map(self, cr, uid, ids, fields=None, context=None, key='id'):
        if isinstance(ids, (int, long)):
            ids = ids
        res = self.read(cr, uid, ids, fields=fields, context=None)
        return {o[key]: o for o in res}

    def b_read_map(self, cr, uid, ids, fields=None, context=None, key='id'):
        if isinstance(ids, (int, long)):
            ids = ids
        res = self.b_read(cr, uid, ids, fields=fields, context=None)
        return {o[key]: o for o in res}

    def search_read(self, cr, uid, domain, fields=None, context=None, limit=None):
        sres = self.search(cr, uid, domain, context=context, limit=limit)
        return self.read(cr, uid, sres, fields=fields, context=context)

    def b_search_read(self, cr, uid, domain, fields=None, context=None, limit=None):
        sres = self.search(cr, uid, domain, context=context, limit=limit)
        return self.b_read(cr, uid, sres, fields=fields, context=context)

    def search_browse(self, cr, uid, domain, context=None, fields_process=None, limit=None):
        sres = self.search(cr, uid, domain, context=context, limit=None)
        return self.browse(cr, uid, sres, context=context, fields_process=fields_process)

    def b_search_browse(self, cr, uid, domain, context=None, fields_process=None):
        sres = self.search(cr, uid, domain, context=context)
        return self.b_browse(cr, uid, sres, context=context, fields_process=fields_process)

    def upsert(self, cr, uid, predicate, vals, context=None):
        sres = self.search(cr, uid, predicate, context=context)
        if len(sres) > 1:
            raise except_orm('Error', 'More than one row to UPSERT')
        if sres:
            self.write(cr, uid, sres[0], vals, context=context)
        else:
            return self.create(cr, uid, vals, context=context)

    def b_upsert(self, cr, uid, predicate, vals, context=None):
        vals = mapper.dict_to_openerp(self, vals)
        self.upsert(cr, uid, predicate, vals, context=context)

    def store_trigger_recompute(self, cr, uid, ids, fields, context=None):
        context = context or {}
        if not fields:
            fields = [v.name for v in self._all_columns.values() if hasattr(v.column, 'store') and v.column.store]
        context['store_ignore_magic'] = True
        res = fnct_write.store_set_values(self, cr, uid, ids, fields, context)
        context.pop('store_ignore_magic')
        return res

    def store_set_null(self, cr, uid, fields, val=None, context=None):
        upst = ['"{f}"=%s'.format(f=f) for f in fields]
        upst = ', '.join(upst)
        cr.execute("""
            UPDATE {tbl}
            SET {upst};
        """.format(tbl=self._table, upst=upst), [val] * len(fields))
        return True

    def store_get_null(self, cr, uid, fields, val=None, context=None):
        if val is None:
            wst = ['"{f}" IS NULL'.format(f=f) for f in fields]
        else:
            wst = ['"{f}" = %s'.format(f=f) for f in fields]
        wst = ' OR '.join(wst)
        cr.execute("""
            SELECT id
            FROM {tbl}
            WHERE {wst}
        """.format(tbl=self._table, wst=wst), [val] * len(fields) if val is not None else [])
        return [el[0] for el in cr.fetchall()]

def monkey():
    for c in (orm.Model, orm.TransientModel):
        c.__bases__ = (BaseModelExt, ) + c.__bases__

monkey()