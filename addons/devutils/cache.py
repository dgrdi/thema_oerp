from openerp.osv.orm import BaseModel
from functools import wraps
from collections import deque

_empty = []

def crud_and_signal(func):
    @wraps(func)
    def wrap(self, *a, **kw):
        for cache in Cache._resetmap.get(self._name, _empty):
            cache.reset()
        return func.__get__(self)(*a, **kw)
    return wrap

BaseModel.create = crud_and_signal(BaseModel.create)
BaseModel.write = crud_and_signal(BaseModel.write)
BaseModel.unlink = crud_and_signal(BaseModel.unlink)


class Cache(object):
    _resetmap = {}

    def __init__(self, reset=None, size=100):
        for obj in reset:
            self._resetmap.setdefault(obj, []).append(self)
        self.cache = {}
        self.size = size
        self.hit = deque(maxlen=self.size)

    def memoize(self, func=None, start=3, stop=None):
        if func is None:
            return self.memoize(start=start, stop=stop)
        @wraps(func)
        def wrap(*a):
            key = a[start:stop]
            res = self.cache.get(key)
            self.hit.append(key)
            if res is None:
                res = func(*a)
                self.cache[key] = res
                if len(self.cache) > self.size:
                    del self.cache[self.hit.popleft()]
            return res
        return wrap

    def reset(self):
        self.cache.clear()
