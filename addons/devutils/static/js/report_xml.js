if (typeof devutils === 'undefined'){
    devutils = {};
}

devutils.report_xml = function (instance) {
    var _t = instance.web._t;
    instance.web.Sidebar = instance.web.Sidebar.extend({
        add_toolbar: function (toolbar) {
            // add a menu entry for each jasper report entry for easy XML generation
            var print = [];
            _.each(toolbar.print, function (act) {
                print.push(act);
                if (act.jasper_report) {
                    var jr_xml = _.clone(act);
                    jr_xml.devutils_xml = true;
                    jr_xml.name = jr_xml.name + ' (XML)';
                    print.push(jr_xml);
                }
            });
            var tool = _.clone(toolbar);
            tool.print = print;
            return this._super(tool);
        },
        on_item_action_clicked: function (item) {
            if (!item.action.devutils_xml) {
                return this._super(item);
            } else {
                this.generate_data_xml(item);
            }
        },
        generate_data_xml: function (item) {
            var self = this;
            self.getParent().sidebar_eval_context().done(function (sidebar_eval_context) {
                var ids = self.getParent().get_selected_ids();
                if (ids.length == 0) {
                    instance.web.dialog($("<div />").text(_t("You must choose at least one record.")), { title: _t("Warning"), modal: true });
                    return false;
                }
                var active_ids_context = {
                    active_id: ids[0],
                    active_ids: ids,
                    active_model: self.getParent().dataset.model
                };
                var c = instance.web.pyeval.eval('context',
                    new instance.web.CompoundContext(
                        sidebar_eval_context, active_ids_context));

                var rxml = new instance.web.Model('ir.actions.report.xml');
                rxml.call('generate_data_xml', [item.action, ids],
                    {context: c}).then(function (res) {
                        var b = new Blob([res], {type: 'octet/stream'});
                        var url = window.URL.createObjectURL(b);
                        var a = document.createElement('a');
                        document.body.appendChild(a);
                        a.style = 'display: none;';
                        a.href = url;
                        a.download = 'data.xml';
                        a.click();
                        window.URL.revokeObjectURL(url);
                    });
            });
        }

    });
};