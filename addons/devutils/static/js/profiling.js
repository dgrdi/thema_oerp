if (typeof devutils === 'undefined'){
    devutils = {};
}

devutils.profiling = function (instance) {

    var menu = function (self) {
        var mn =  $('<div class="dev-profiling oe_form_dropdown_section">' +
            '<button class="oe_dropdown_toggle oe_dropdown_arrow">Profiling</button>' +
            '<ul class="oe_dropdown_menu">' +
            '   <li id="dev-prof-enable">Enable</li>' +
            '   <li id="dev-prof-disable">Disable</li>' +
            '   <li id="dev-prof-clear">Clear</li>' +
            '   <li id="dev-prof-results">Results</li>' +
            '</ul>' +
            '</div>');
        var prof = new instance.web.Model('profiling.profile');
        self.$buttons.append(mn);
        mn.find('#dev-prof-enable').click(function(){
            prof.call('enable');
        });
        mn.find('#dev-prof-disable').click(function(){
            prof.call('disable');
        });
        mn.find('#dev-prof-clear').click(function(){
            prof.call('clear');
        });
        mn.find('#dev-prof-results').click(function(){
            prof.call_button('results', []).then(function (r) {
                self.do_action(r)
            });
        });
        return mn;
    };


    instance.web.FormView = instance.web.FormView.extend({
        load_form: function () {
            var self = this;
            return this._super.apply(this, arguments).then(function (res) {
                menu(self);
                return res;
            });
        }
    });

};