openerp.devutils = function(instance){
    var queryDict = {};
    location.search.substr(1)
        .split("&")
        .forEach(function(item) {queryDict[item.split("=")[0]] = item.split("=")[1]});
    if ( !_(_(queryDict).keys()).contains('debug') )
        return;

    devutils.view_xid(instance);
    devutils.report_xml(instance);
    devutils.profiling(instance);

};