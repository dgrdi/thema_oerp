if (typeof devutils === 'undefined'){
    devutils = {};
}

devutils.view_xid = function(instance){
    /**
     * Add a button to view the current record's XML id when in debug mode.
     */

    var btn = function(){ return $('<button>XML id</button>') };

    instance.web.FormView = instance.web.FormView.extend({
        load_form: function() {
            var self = this;
            var f = this._super.apply(this, arguments);
            return f.then(function(res){
                var b = btn();
                self.$buttons.append(b);
                b.click(function(e){
                    self.show_xid.call(self);
                });
            });
        },
        show_xid: function(){
            var self = this;
            var md = new instance.web.Model('ir.model.data');
            md.query(['module', 'name'])
                .filter([['model', '=', this.model], ['res_id', '=', this.datarecord.id]])
                .all()
                .then(function(res){
                    var el = instance.web.qweb.render('xid-info', {
                        dbid: self.datarecord.id,
                        xid: res.length > 0 ? res[0].module + '.' + res[0].name : 'None'
                    });
                    instance.web.dialog($(el), {
                        title: "Object instance info",
                        buttons: [
                            {text: "Ok", click: function() { $(this).dialog("close"); }}
                        ]
                    })
                });
        }
    });
};

