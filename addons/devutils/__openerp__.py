{
    'name': 'Miscellaneous development utilities',
    'version': '0.2',
    'category': 'Development',
    'description': """Utilities that make openerp development easier.""",
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': ['base', 'bulk_import'],
    'init_xml': [],
    'data':[
        'profiling.xml',
        ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': True,
    'js': [
        'static/js/*.js',
    ],
    'qweb': [
        'static/qweb/*.xml',
    ],
    'css': [
        'static/css/*.css',
    ]

}
