{
    'name': 'Journal number check',
    'version': '0.1',
    'category': '',
    'description': """Optionally force journal entries numbering to agree with their dates""",
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': ['base', 'devutils', 'account'],
    'init_xml': [],
    'data':[
        'ir.model.access.csv',
        'journal_view.xml',
        ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': True,
}


