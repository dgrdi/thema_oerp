from openerp.osv.orm import except_orm
from openerp.osv.osv import except_osv
from testutils.object_wrapper import ModelWrapper
from testutils.transaction import SavepointCase
from testutils.factories.account import MoveFactory, JournalFactory
from testutils.factories.field import Many2One

class TestMoveOrder(SavepointCase):

    journalf = JournalFactory({
        'force_date_number': True,
    })

    movef = MoveFactory({
        'journal_id': Many2One(journalf),
        'name': '/'
    })


    def setUp(self):
        super(TestMoveOrder, self).setUp()
        self.journal = self.journalf.create()

    def test_move_date_before_number(self):
        m1 = self.movef.create({
            'date': '2014-01-10',
            'journal_id': self.journal
        })
        m1.wobj.post([m1])
        m2 = self.movef.create({
            'date': '2014-01-01',
            'journal_id': self.journal,
        })
        with self.assertRaises(except_orm):
            m2.wobj.post([m2])

    def test_move_date_after_number(self):

        m1 = self.movef.create({
            'date': '2014-01-10',
            'journal_id': self.journal
        })
        m2 = self.movef.create({
            'date': '2014-01-11',
            'journal_id': self.journal,
        })
        m2.wobj.post([m1, m2])
        with self.assertRaises(except_orm):
            m2.wobj.write(m2, {
                'date': '2014-01-01'
            })


    def test_move_date_exception_is_meaningful(self):
        m1 = self.movef.create({
            'date': '2014-01-10',
            'journal_id': self.journal
        })
        m2 = self.movef.create({
            'date': '2014-01-01',
            'journal_id': self.journal,
        })
        try:
            m2.wobj.post([m1, m2])
        except except_orm as e:
            self.assertIn(m1.browse.name, unicode(e))
            self.assertIn(m2.browse.name, unicode(e))

