from openerp.osv.osv import except_osv
from testutils.transaction import SavepointCase
from testutils.factories.ir import SequenceFactory
import datetime

class TestNumbering(SavepointCase):
    fsequence = SequenceFactory()

    def test_deinterpolate(self):
        prefix = 'FV/%(year)s/'
        suffix = ''
        res = self.fsequence.obj.deinterpolate(prefix, suffix, 'FV/2014/000123')
        self.assertEqual(res, 123)


    def create_sequence(self):
        seq = self.fsequence.create({
            'prefix': 'FV/%(year)s/',
            'code': {
                'name': 't0',
                'code': 't0'
            }
        })
        return seq

    def test_number_from_name(self):
        seq = self.create_sequence()
        name = seq.wobj.next_by_id(seq)
        cur_year = datetime.date.today().year
        self.assertEqual(name, 'FV/%s/1' % cur_year)
        num = seq.wobj.number_from_name('t0', name)
        self.assertEqual(num, 1)

    def test_number_after_format_change(self):
        seq = self.create_sequence()
        name = seq.wobj.next_by_id(seq)
        seq.wobj.write(seq, {'prefix': 't'})
        num = seq.wobj.number_from_name('t0', name)
        self.assertEqual(num, 1)

    def test_number_with_same_name(self):
        seq = self.create_sequence()
        name = seq.wobj.next_by_id(seq)
        seq.wobj.write(seq, {'number_next': 2})
        seq.wobj.write(seq, {'number_next': 1})
        name2 = seq.wobj.next_by_id(seq)
        self.assertEqual(name, name2)
        num = seq.wobj.number_from_name('t0', name)
        self.assertEqual(num, 1)

    def test_non_existing_number(self):
        seq = self.create_sequence()
        res = seq.wobj.number_from_name('t0', 'n')
        self.assertIsNone(res)

    def test_non_existing_code(self):
        with self.assertRaises(except_osv):
            self.fsequence.wobj.number_from_name('t0', 'n')


