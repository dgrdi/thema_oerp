# coding=utf-8
from openerp.osv import orm, fields, osv
from openerp.osv.osv import except_osv
from openerp.tools.translate import _
import re

num_re = re.compile(r'[^\d]*(\d+)')

class ir_sequence_number(orm.Model):
    _name = 'ir.sequence.number'

    _columns = {
        'code': fields.char('Sequence code'),
        'name': fields.char('Full number'),
        'number': fields.integer('Number'),
    }
    _sql_constraints = (
        (
            'unique_code_name',
            "UNIQUE (code, name)",
            u'Il numeratore è già stato usato per generare questo numero!',
        ),
    )

class ir_sequence(orm.Model):
    _inherit = 'ir.sequence'

    def deinterpolate(self, prefix, suffix, name):
        prefix = prefix or ''
        suffix = suffix or ''
        d = self._interpolation_dict()
        len_dict = {'%({0})'.format(k): len(v) for k, v in d.items()}
        prefix_len = len(prefix)
        suffix_len = len(suffix)
        for s, l in len_dict.items():
            while prefix.find(s) != -1:
                prefix = prefix.replace(s, '')
                prefix_len -= l
            while suffix.find(s) != -1:
                suffix = suffix.replace(s, '')
                suffix_len -= l
        nums = name[prefix_len:]
        if suffix_len:
            nums = nums[:-suffix_len]
        return int(nums)

    def _get_preferred_seq(self, cr, uid, seq_ids, context=None):
        if context is None:
            context = {}
        force_company = context.get('force_company')
        if not force_company:
            force_company = self.pool.get('res.users').browse(cr, uid, uid).company_id.id
        sequences = self.read(cr, uid, seq_ids, ['company_id'])
        preferred_sequences = [s for s in sequences if s['company_id'] and s['company_id'][0] == force_company ]
        seq = preferred_sequences[0] if preferred_sequences else sequences[0]
        return seq['id']

    def _next(self, cr, uid, seq_ids, context=None):
        name = super(ir_sequence, self)._next(cr, uid, seq_ids, context)
        if name:
            seq = self._get_preferred_seq(cr, uid, seq_ids, context)
            seq = self.browse(cr, uid, seq, context=context)
            number = self.deinterpolate(seq.prefix, seq.suffix, name)
            self.pool.get('ir.sequence.number').upsert(cr, uid, [('code', '=', seq.code), ('name', '=', name)], {
                    'code': seq.code,
                    'name': name,
                    'number': number
                })
        return name

    def number_from_name(self, cr, uid, code, name, context=None):
        seqnum = self.pool.get('ir.sequence.number')
        res = seqnum.search_read(cr, uid, [('code', '=', code), ('name', '=', name)], fields=('number',))
        if res:
            return res[0]['number']
        else:
            if not self.pool.get('ir.sequence.type').search(cr, uid, [('code', '=', code)], context=context):
                raise except_osv(u'Errore!', u'Il codice sequenza %s non esiste!' % code)
            return None
