# coding=utf-8
from openerp.osv import orm, fields, osv
from devutils.dates import date_locale
import re

class account_journal(orm.Model):
    _inherit = 'account.journal'

    _columns = {
        'force_date_number': fields.boolean('Force numbering to agree with date')
    }


class account_move(orm.Model):
    _inherit = 'account.move'

    _columns = {
        'seq_number': fields.integer('Sequence number'),
        'fiscalyear_id': fields.related('period_id', 'fiscalyear_id',
                                        string='Fiscal year',
                                        relation='account.fiscalyear',
                                        type='many2one',
                                        store=True)
    }


    def _check_date_number(self, cr, uid, ids, context=None):
        cf = self.pool.get('account.move.number.conflict')
        conflicts = cf.search(cr, uid, [('move_id', 'in', ids)], limit=1, context=context)
        return not conflicts

    def _check_date_number_msg(self, cr, uid, ids, context=None):
        cf = self.pool.get('account.move.number.conflict')
        conflict = cf.search_read(cr, uid, [('move_id', 'in', ids)], context=context, limit=1)[0]
        return u"La scrittura {m} del {d} è in conflitto con la scrittura {cm} del {dm}: " \
               u"la numerazione non è consistente con le date.".format(m=conflict['move_id'][1],
                                                                       d=date_locale(conflict['date'], context),
                                                                       cm=conflict['conflict_move_id'][1],
                                                                       dm=date_locale(conflict['conflict_date'], context))


    _constraints = (
        (
            _check_date_number,
            _check_date_number_msg,
            ('date', 'seq_number', 'journal_id', 'fiscalyear_id', 'state'),
        ),
    )

    def init(self, cr):
        sp = super(account_move, self)
        if hasattr(sp, 'init'):
            sp.init(cr)
        cr.execute("""
            DROP INDEX IF EXISTS ix_account_move_unique_number;
            CREATE UNIQUE INDEX ix_account_move_unique_number
              ON account_move(journal_id, fiscalyear_id, seq_number)
            WHERE
                  state = 'posted'
              AND seq_number IS NOT NULL;
        """)

    def post(self, cr, uid, ids, context=None):
        res = super(account_move, self).post(cr, uid, ids, context=context)
        ir_sequence = self.pool.get('ir.sequence')
        do_check = []
        for move in self.browse(cr, uid, ids, context=context):
            if move.state == 'posted' and move.journal_id.force_date_number:
                self.write(cr, uid, move.id, {
                    'seq_number': ir_sequence.number_from_name(cr, uid, move.journal_id.sequence_id.code,
                                                               move.name, context=context)
                })
                do_check.append(move.id)
        if do_check:
            self._validate(cr, uid, do_check, context=context)
        return res

    def write(self, cr, user, ids, vals, context=None):
        # just mae ids a list, to fix an instance where parents would call
        # validate() with an int when it expects a list
        if isinstance(ids, (int, long)):
            ids = [ids]
        return super(account_move, self).write(cr, user, ids, vals, context=context)



class account_move_number_conflict(orm.Model):
    _name = 'account.move.number.conflict'
    _auto = False

    _columns = {
        'move_id': fields.many2one('account.move', 'Move', readonly=True),
        'date': fields.date('Move date', readonly=True),
        'name': fields.char('Move name', readonly=True),
        'conflict_move_id': fields.many2one('account.move', 'Conflicting move', readonly=True),
        'conflict_date': fields.date('Conflicting date'),
        'conflict_name': fields.char('Conflicting name'),
    }

    def init(self, cr):
        cr.execute("""
            CREATE OR REPLACE VIEW account_move_number_conflict AS
            SELECT
                m.id id,
                m.id move_id,
                m.date "date",
                m.name "name",
                m2.id conflict_move_id,
                m2.date conflict_date,
                m2.name conflict_name

            FROM account_move m
            INNER JOIN account_journal j ON m.journal_id = j.id
            INNER JOIN account_move m2 ON
                    m2.id <> m.id
                AND m2.journal_id = m.journal_id
                AND m2.fiscalyear_id = m.fiscalyear_id
                AND (
                       (m2.date > m.date AND m2.seq_number < m.seq_number)
                    OR (m2.date < m.date AND m2.seq_number > m.seq_number)
                )
            WHERE
                   j.force_date_number
               AND m.state = 'posted';
        """)


class account_invoice(orm.Model):
    _inherit = 'account.invoice'
    _NUM_RX = re.compile('^(?:[^/]*/)+.*(\d+).*$')

    _columns = {
        'int_number': fields.function(lambda s, *a, **k: s._int_number(*a, **k),
                                      type='integer', string='Numero (numerico)')
    }

    def _int_number(self, cr, uid, ids, field, arg, context=None):
        res = {}
        for inv in self.browse(cr, uid, ids, context=context):
            if not inv.move_id:
                res[inv.id] = False
            elif inv.move_id.seq_number:
                res[inv.id] = inv.move_id.seq_number
            else:
                m = self._NUM_RX.match(inv.move_id.name)
                if m:
                    res[inv.id] = int(m.group(1))
                else:
                    res[inv.id] = False
        return res
