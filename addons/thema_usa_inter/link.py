from openerp.osv import orm, fields

class res_partner(orm.Model):
    _inherit = 'res.partner'
    _columns = {
        'isite_id': fields.char('Site ID of the customer', select=True)
    }

class sale_order(orm.Model):
    _inherit = 'sale.order'
    _columns = {
        'isite_id': fields.char('Site ID of the order', select=True)
    }

    def _add_missing_default_values(self, cr, uid, values, context=None):
        values = super(sale_order, self)._add_missing_default_values(cr, uid, values, context=context)
        if context and 'interimport' in context and 'partner_id' in values:
            val = self.onchange_partner_id(cr, uid, [], values['partner_id'], context)
            values.update(val['value'])
        return values

class res_user(orm.Model):
    _inherit = 'res.users'
    _columns = {
        'isite_id': fields.char('Site ID of the user'),
    }