from collections import OrderedDict
from openerp.osv import orm, fields
import csv
import base64
from StringIO import StringIO
from openerp.osv.osv import except_osv
from .command import command, get_fn
from logging import getLogger
from openerp.tools.translate import _
logger = getLogger(__name__)

NETERR_SUBJ = _('Error during import')
NETERR_BODY = _('An error occurred during import. '
                'This is most likely due to network problems. \n'
                'Error details: \n%s')


class tui_import(orm.Model):
    _name = 'thema.usa.inter.import'
    _inherit = 'mail.thread'
    _table = 'thema_usa_inter_import'

    PRIORITY_MAP = {
        'res.users': 10,
        'res.partner': 20,
        'sale.order': 30,
    }

    _order = 'priority'

    def _field_res_inst(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for i in self.browse(cr, uid, ids, context=context):
            if i.res_id:
                res[i.id] = i.model_id.model + ',' + str(i.res_id)
            else:
                res[i.id] = False
        return res

    def _field_res_inst_set(self, cr, uid, ids, fields_name, val, arg, context):
        if not val:
            self.write(cr, uid, ids, {'res_id': False, 'model_id': False}, context=context)
            return True
        if isinstance(ids, (int, long)):
            ids = [ids]
        mod, res_id = val.split(',')
        res_id = int(res_id)
        model_id = self.pool.get('ir.model').search(cr, uid, [('model', '=', mod)], context=context)[0]
        for imp in self.browse(cr, uid, ids, context=context):
            self.write(cr, uid, imp.id, {'res_id': res_id, 'model_id': model_id, 'state': 'done'})
            self.pool.get(mod).write(cr, uid, res_id, {'isite_id': imp.isite_id}, context=context)
        return True

    def _field_resource(self, cr, uid, ids, field_name, arg, context):
        res = {}
        bin_size = context.pop('bin_size', False)
        for imp in self.browse(cr, uid, ids, context=context):
            fc = self.map_isite_ids(cr, uid, imp.model_id.model,
                                    base64.b64decode(imp.orig_resource),
                                    context=context)
            if bin_size:
                res[imp.id] = len(fc)
            else:
                res[imp.id] = base64.b64encode(fc)
        if bin_size:
            context['bin_size'] = bin_size
        return res

    _columns = {
        'state': fields.selection([('start', 'Not imported'),
                                   ('done', 'Done'),
                                   ('error', 'Error')], 'State'),
        'priority': fields.integer('Priority'),
        'model_id': fields.many2one('ir.model', 'Model', readonly=True),
        'name': fields.char('Name', readonly=True),
        'orig_resource': fields.binary('Original file', readonly=True),
        'resource': fields.function(_field_resource,
                                    type='binary',
                                    string='Import file',
                                    readonly=True),
        'isite_id': fields.char('Site ID'),
        'res_id': fields.integer('Resource ID'),
        'res_name': fields.char('Resource name'),
        'res_inst': fields.function(_field_res_inst,
                                    fnct_inv=_field_res_inst_set,
                                    type='reference',
                                    string='Resource instance',
                                    selection=[('res.partner', 'Customer'),
                                               ('res.users', 'Agent'),
                                               ('sale.order', 'Order')]),
        'err_msg': fields.text('Error message'),
    }

    _defaults = {
        'state': 'start',
    }


    def __init__(self, pool, cr):
        super(tui_import, self).__init__(pool, cr)
        try:
            self._load_tui_group_ref(cr, 1)
        except:
            pass # on module install, the object has not been yet loaded

    def _load_tui_group_ref(self, cr, uid):
        md = self.pool.get('ir.model.data')
        mod, res_id = md.get_object_reference(cr, 1, 'thema_usa_inter', 'group_inter_log')
        self.tui_mail_group = res_id

    def map_isite_ids(self, cr, uid, model, data, context=None):
        model = self.pool.get(model)
        reader = csv.DictReader(StringIO(data))
        fields = []
        for field in reader.fieldnames:
            if '/isite_id' in field:
                fields.append(field.replace('/isite_id', '/.id'))
            else:
                fields.append(field)
        out = StringIO()
        writer = csv.DictWriter(out, fieldnames=fields)
        writer.writeheader()
        for d in reader:
            wrd = {}
            for k, v in d.items():
                if '/isite_id' in k:
                    fr = k.replace('/isite_id', '/.id')
                    if not v:
                        wrd[fr] = v
                        continue

                    path = k.split('/')
                    mod = model
                    for p in path[:-1]:
                        mod = self.pool.get(mod._columns[p]._obj)
                    id = mod.search(cr, uid, [('isite_id', '=', v)], context=context)

                    if len(id) == 1:
                        wrd[fr] = id[0]
                    else:
                        # id is missing, return the original file
                        # this will fail on import, but is more useful
                        # than the raw database ID for humans
                        return data
                else:
                    wrd[k] = v
            writer.writerow(wrd)
        return out.getvalue()

    def generate_from_data(self, cr, uid, model, data, context=None):
        model_id = self.pool.get('ir.model').search(cr, uid, [('model', '=', model)], context=context)[0]
        model = self.pool.get('ir.model').browse(cr, uid, model_id, context=context)
        data = filter(lambda d: d.strip(), data.split('##~csvsplit~##\n'))
        res = []
        context = context or {}
        for d in data:
            reader = csv.DictReader(StringIO(d))
            elem = reader.next()
            elem = {k: v.decode('utf-8') for k, v in elem.items()}
            isite_id = elem.get('isite_id')
            vals = {
                'model_id': model_id,
                'orig_resource': base64.b64encode(d),
                'isite_id': isite_id,
                'name': model.name + ': ' + elem.get('name', elem.get('isite_id', '')),
                'res_name': elem.get('login', elem.get('name')),
                'priority': self.PRIORITY_MAP.get(model._name),
            }
            ex = self.search(cr, uid,
                             [('isite_id', '=', isite_id), ('model_id', '=', model_id)],
                             context=context)
            if not ex:
                cid = self.create(cr, uid, vals, context=context)
                res.append(cid)
            else:
                self.write(cr, uid, ex[0], vals, context=context)
                res.append(ex[0])
        return res

    def merge(self, cr, uid, ids, context=None):
        ids = self.search(cr, uid, [('id', 'in', ids), ('res_id', '=', False)], context=context)
        for imp in self.browse(cr, uid, ids, context=context):
            model = self.pool.get(imp.model_id.model)
            ids = model.search(cr, uid, [('isite_id', '=', imp.isite_id)], context=context)
            if ids:
                self.write(cr, uid, imp.id, {'res_id': ids[0], 'state': 'done'})
            else:
                if model._name == 'res.users':
                    ns = model.search(cr, uid, [('login', 'ilike', imp.res_name)])
                else:
                    ns = model.name_search(cr, uid, imp.res_name, context=context)
                    ns = map(lambda x: x[0], ns)
                if len(ns) > 1 and model._name == 'res.partner':
                    # if there is only one that has no parent, use that one
                    ns = model.search(cr, uid, [('id', 'in', ns), ('parent_id', '=', False)], context=context)
                if len(ns) == 1:
                    model.write(cr, uid, ns[0], {'isite_id': imp.isite_id})
                    self.write(cr, uid, imp.id, {'res_id': ns[0], 'state': 'done'})

    def import_(self, cr, uid, ids, context=None):
        context = context or {}
        context['interimport'] = True
        ids = self.search(cr, uid, [('id', 'in', ids), ('state', '!=', 'done')], context=context)
        ret = True
        for imp in self.browse(cr, uid, ids, context=context):
            model = self.pool.get(imp.model_id.model)
            reader = csv.reader(StringIO(base64.b64decode(imp.resource)))
            fields = reader.next()
            matrix = [[el.decode('utf-8') for el in row] for row in reader]
            res = model.load(cr, uid, fields, matrix, context=context)
            if res['ids']:
                self.write(cr, uid, imp.id, {'res_id': res['ids'][0], 'state': 'done'}, context=context)
            else:
                errs = [m['message'] for m in res['messages'] if m['type'] == 'error']
                self.write(cr, uid, imp.id, {'state': 'error', 'err_msg': '\n'.join(errs)}, context=context)
                subj = _('Error during record import')
                body = _('An error occurred during import of a record. '
                         'Please fix it by either linking it to '
                         'a record already in the system, or by running the import manually.')
                if not context.get('nomessage'):
                    mg = self.pool.get('mail.group').browse(cr, uid, self.tui_mail_group, context=context)
                    self.message_subscribe(cr, uid, [imp.id], [p.id for p in mg.message_follower_ids], context=context)
                    msgid= self.message_post(cr, uid, imp.id, subject=subj,
                                      body=body,
                                      context=context)
                    self.pool.get('mail.message').set_message_read(cr, uid, [msgid], False, context=context)
                elif len(ids) == 1:
                    cr.commit()
                    raise except_osv(subj, body)
        context.pop('interimport')
        return ret

    def fetch(self, cr, uid, models=None, context=None):
        cmdmap = [
            ('res.users', 'agents'),
            ('res.partner', 'customers'),
            ('sale.order', 'orders'),
        ]
        if models is not None:
            cmdmap = [c for c in cmdmap if c[0] in models]
        datas = {}
        res = []
        context = context or {}
        cr.execute('SAVEPOINT inter_download;')
        for mod, cmd in cmdmap:
            cr.execute("SELECT DISTINCT isite_id "
                       "FROM thema_usa_inter_import ui "
                       "INNER JOIN ir_model m ON m.id = ui.model_id "
                       "WHERE m.model = %s ", [mod])
            existing = cr.fetchall()
            inputfn = get_fn(self.pool, cr, uid, 'ex_' + cmd)
            inputdata = '\n'.join(map(str, [iid for row in existing for iid in row]))
            try:
                datas[mod] = command(self.pool, cr, uid, cmd, inputfn, inputfn=inputfn, inputdata=inputdata)
            except Exception as e:
                cr.execute('ROLLBACK TO SAVEPOINT inter_download;')
                logger.exception('Error during intercompany import')
                if not context.get('nomessage'):
                    mg = self.pool.get('mail.group')
                    msg = mg.message_post(cr, uid, self.tui_mail_group,
                                    subject=NETERR_SUBJ,
                                    body=NETERR_BODY % e)
                    self.pool.get('mail.message').set_message_read(cr, uid, [msg], False, context=context)
                raise except_osv(NETERR_SUBJ, NETERR_BODY % e)
        cr.execute('RELEASE SAVEPOINT inter_download;')
        for mod, data in datas.items():
            cids = self.generate_from_data(cr, uid, mod, data, context=context)
            res.extend(cids)
        return res

    def download(self, cr, uid, context=None):
        context = context or {}
        try:
            res = self.fetch(cr, uid, context=context)
        except:
            # trap the exception if we want messages to be saved to db
            if context.get('nomessage'):
                raise
            else:
                return False
        context.pop('nomessage', None)
        self.merge(cr, uid, res, context=context)
        self.import_(cr, uid, res, context=context)

    def mark_resolved(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'done'})
        return True

    def create(self, cr, user, vals, context=None):
        context = context or {}
        context['mail_create_nolog'] = True
        id = super(tui_import, self).create(cr, user, vals, context)
        context.pop('mail_create_nolog')
        return id

    def write(self, cr, uid, ids, vals, context=None):
        res = super(tui_import, self).write(cr, uid, ids, vals, context)
        if vals.get('state') == 'done':
            # mark notifications as read
            if isinstance(ids, (int, long)):
                ids = []
            notobj = self.pool.get('mail.notification')
            nots = notobj.search(cr, uid,
                                 [('message_id.model', '=', 'thema.usa.inter.import'),
                                  ('message_id.res_id', 'in', ids)],
                                 context=context)
            notobj.write(cr, uid, nots, {'read': True}, context=context)
        return res

    def download_merge(self, cr, uid, models=None, context=None):
        context=context or {}
        try:
            res = self.fetch(cr, uid, models=['res.users', 'res.partner'],
                             context=context)
        except:
            if not context.get('nomessage'):
                raise
            else:
                return False
        context.pop('nomessage', None)
        self.merge(cr, uid, res, context=context)
        unmerged = self.search(cr, uid, [('state', '!=', 'done'), ('id', 'in', res)], context=context)
        subj = _('Could not link record')
        body = _('Could not link the record to one in the system. Please '
                 'specify the corresponding record, or run the import manually.')
        self.write(cr, uid, unmerged, {
            'state': 'error',
            'err_msg': body,
        }, context=context)
        if not context.get('nomessage'):
            messages = []
            for imp in unmerged:
                mg = self.pool.get('mail.group').browse(cr, uid, self.tui_mail_group, context=context)
                self.message_subscribe(cr, uid, [imp], [p.id for p in mg.message_follower_ids], context=context)
                mid = self.message_post(cr, uid, imp, subject=subj, body=body, context=context)
                messages.append(mid)
            self.pool.get('mail.message').set_message_read(cr, uid, messages, False, context=context)
        return True
