from openerp.osv import orm, fields
from openerp.osv.osv import except_osv
from .command import PMAP, get_params, command
from .imprt import NETERR_BODY, NETERR_SUBJ

PMAP_NAMES = {
    'home': 'Home',
    'user': 'Username',
    'passwd': 'Password',
    'host': 'Hostname',
    'script_path': 'Script path',
}

class tui_settings(orm.TransientModel):
    _name = 'thema.usa.inter.config'
    _inherit = 'res.config.settings'

    _columns = {
        'cron_active': fields.boolean('Enable automatic download'),
        'cron_interval': fields.integer('Download interval (minutes)', required=True),
    }
    for p in PMAP:
        _columns[p] = fields.char(PMAP_NAMES[p], required=True)

    def get_default_params(self, cr, uid, fields, context=None):
        return get_params(self.pool, cr, uid)

    def set_params(self, cr, uid, ids, context=None):
        params = self.read(cr, uid, ids, fields=PMAP.keys(), context=context)[0]
        params.pop('id')
        pobj = self.pool.get('ir.config_parameter')
        for k, v in params.items():
            pobj.set_param(cr, uid, PMAP[k], v, context=context)

    def get_default_cron(self, cr, uid, fields, context=None):
        md = self.pool.get('ir.model.data')
        mod, res_id = md.get_object_reference(cr, uid, 'thema_usa_inter', 'cron_download')
        cron = self.pool.get('ir.cron').browse(cr, uid, res_id, context=context)
        return {
            'cron_active': cron.active,
            'cron_interval': cron.interval_number,
        }

    def set_cron(self, cr, uid, ids, context=None):
        md = self.pool.get('ir.model.data')
        mod, res_id = md.get_object_reference(cr, uid, 'thema_usa_inter', 'cron_download')
        crn = self.read(cr, uid, ids, fields=('cron_active', 'cron_interval'), context=context)[0]
        self.pool.get('ir.cron').write(cr, uid, [res_id], {
            'active': crn['cron_active'],
            'interval_number': crn['cron_interval'],
        }, context=context)

    def orders_initial(self, cr, uid, ids=None, context=None):
        iobj = self.pool.get('thema.usa.inter.import')
        cr.execute('SAVEPOINT pre_orders;')
        try:
            orders = command(self.pool, cr, uid, 'existing_orders')
        except Exception as e:
            cr.execute('ROLLBACK TO SAVEPOINT pre_orders;')
            if not context.get('nomessage'):
                mg = self.pool.get('mail.group').browse(cr, uid, iobj.tui_mail_group, context=context)
                mg.message_post(cr, uid, subject=NETERR_SUBJ, body=NETERR_BODY % e, context=context)
                return False
            else:
                raise except_osv(NETERR_SUBJ, NETERR_BODY % e)
        cr.execute('RELEASE SAVEPOINT pre_orders;')
        order_mod_id = self.pool.get('ir.model').search(cr, uid, [('model', '=', 'sale.order')], context=context)[0]
        orders = filter(lambda x: x. strip(), orders.split('\n'))
        exist = iobj.search(cr, uid, [('model_id', '=', order_mod_id)], context=context)
        exist = iobj.read(cr, uid, exist, fields=('isite_id', ), context=context)
        exist = [el['isite_id'] for el in exist]
        for o in set(orders) - set(exist):
            iobj.create(cr, uid, {
                'model_id': order_mod_id,
                'isite_id': o,
                'state': 'done',
                'name': 'Initial: order %s' % o,
            }, context=context)
        return True

    def merge_initial(self, cr, uid, ids=None, context=None):
        iobj = self.pool.get('thema.usa.inter.import')
        return iobj.download_merge(cr, uid, context=context)
