from openerp.netsvc import LocalService
from openerp.osv import orm, fields
import base64

class stock_warehouse(orm.Model):
    _inherit = 'stock.warehouse'

    _columns = {
        'iexport': fields.boolean('Export orders to Thema ITA'),
    }

class stock_location(orm.Model):
    _inherit = 'stock.location'

    def _field_iexport(self, cr, uid, ids, field_name, arg, context):
        whobj = self.pool.get('stock.warehouse')
        exp_w = whobj.search(cr, uid, [('iexport', '=', True), ('lot_stock_id', 'in', ids)],
                             context=context)
        exp_loc = whobj.read(cr, uid, exp_w, fields=('lot_stock_id', ), context=context)
        exp_loc = set([el['lot_stock_id'][0] for el in exp_loc])
        res = {id: True for id in exp_loc}
        res.update({id: False for id in set(ids) - exp_loc})
        return res

    def _trg_whouse(obj, cr, uid, ids, context=None):
        return obj

    _columns = {
        'iexport': fields.function(_field_iexport,
                                   type='boolean',
                                   string='Export to Thema ITA',
                                   store={
                                       'stock.warehouse': (
                                           lambda o, c, u, i, ctx: o.pool.get('stock.location').search(c, u, [],
                                                                                                       context=ctx),
                                           ('iexport', 'lot_stock_id'),
                                           10,
                                       )
                                   })
    }

class stock_picking(orm.Model):
    _inherit = 'stock.picking'

    _columns = {
        'iexported': fields.boolean('Exported to Thema ITA'),
    }

    def ideliver(self, cr, uid, data, context=None):
        """
        Data is a list of 2-tuples (move_id, qty) to deliver
        """
        partial_data = {}
        for move_id, qty in data:
            partial_data[move_id] = {
                'product_qty': qty
            }
        product_obj = self.pool.get('product.product')
        move_obj = self.pool.get('stock.move')
        uoms = move_obj.read(cr, uid, partial_data.keys(), fields=('product_uom', 'picking_id'), context=context)
        pickings = list(set([el['picking_id'][0] for el in uoms]))
        uoms = {el['id']: el['product_uom'][0] for el in uoms}
        for k, v in partial_data.items():
            v['product_uom'] = uoms[k]
        partial_data = {'move%s' % k: v for k, v in partial_data.items()}
        res = self.do_partial(cr, uid, pickings, partial_data, context=context)
        delivered = []
        for pick in self.browse(cr, uid, pickings, context=context):
            if pick.backorder_id:
                delivered.append(pick.backorder_id.id)
            elif pick.state == 'done':
                delivered.append(pick.id)
        moves = move_obj.search(cr, uid, [('picking_id', 'in', delivered)], context=context)
        moves = move_obj.read(cr, uid, moves, fields=('product_qty', 'picking_id'), context=context)
        res = {}
        for m in moves:
            res.setdefault(str(m['picking_id'][0]), []).append((m['id'], m['product_qty']))
        return res

class account_invoice(orm.Model):
    _inherit = 'account.invoice'

    def ireport_invoice(self, cr, uid, ids, context=None):
        self._workflow_signal(cr, uid, ids, 'invoice_open', context=context)
        rpt = LocalService('report.account.invoice')
        res = {}
        for id in ids:
            payload, type = rpt.create(cr, uid, ids, {'model': self._name}, context=context)
            res[id] = base64.b64encode(payload)
        return res
