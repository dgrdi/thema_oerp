from openerp.tests.common import TransactionCase
from testutils.object_wrapper import ModelWrapper
import os


class TestImport(TransactionCase):
    imprt = ModelWrapper('thema.usa.inter.import')
    agent_data = os.path.join(os.path.dirname(__file__), 'agents.data')
    customer_data = os.path.join(os.path.dirname(__file__), 'customers.data')
    single_customer_data = os.path.join(os.path.dirname(__file__), 'single_customer.data')
    single_order_data = os.path.join(os.path.dirname(__file__), 'single_order.data')

    def test_merge(self):
        #this will pass only if there is a user with login 'themausa'
        with open(self.agent_data) as f:
            res = self.imprt.generate_from_data('res.users', f.read())
            self.imprt.merge(res)
            i = self.imprt.search([('res_name', '=', 'themausa')])
            i = self.imprt.browse(i[0])
            self.assertEqual(i.res_inst.isite_id, i.isite_id)

    def test_import(self):
        with open(self.agent_data) as f:
            res = self.imprt.generate_from_data('res.users', f.read())
        self.imprt.merge(res)
        with open(self.single_customer_data) as f:
            res = self.imprt.generate_from_data('res.partner', f.read())
        self.imprt.merge(res)
        with open(self.single_order_data) as f:
            res = self.imprt.generate_from_data('sale.order', f.read())
        self.imprt.import_(res)
