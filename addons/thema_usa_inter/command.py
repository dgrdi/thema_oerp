from paramiko import client as pclient
import paramiko

PMAP = {
    'host': 'thema.usa.inter.ssh_host',
    'user': 'thema.usa.inter.ssh_user',
    'passwd': 'thema.usa.inter.ssh_passwd',
    'home': 'thema.usa.inter.ssh_home',
    'script_path': 'thema.usa.inter.script_path',
}

def get_params(pool, cr, uid, params=None):
    probj = pool.get('ir.config_parameter')
    if params is None:
        params = PMAP.keys()
    return {p: probj.get_param(cr, uid, PMAP[p]) for p in params}

def execute_command(params, *args, **kwargs):
    cl = pclient.SSHClient()
    cl.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    cl.connect(params['host'], username=params['user'], password=params['passwd'])
    try:
        inputfn = kwargs.get('inputfn')
        inputdata = kwargs.get('inputdata')
        if inputfn is not None and inputdata is not None:
            fo_in, fo_out, fo_err = cl.exec_command('cat > %s' % inputfn)
            fo_in.write(inputdata)
            fo_in.close()
        cmd = params['script_path'] + ' ' + ' '.join(map(unicode, args))
        fo_in, fo_out, fo_err = cl.exec_command(cmd)
        res = fo_out.read()
        if isinstance(res, unicode):
            return res.encode('utf-8')
        else:
            return res
    finally:
        cl.close()

def command(pool, cr, uid, *args, **kwargs):
    params = get_params(pool, cr, uid)
    return execute_command(params, *args, **kwargs)

def get_fn(pool, cr, uid, fn):
    params = get_params(pool, cr, uid, params=['home'])
    return params['home'] + fn
