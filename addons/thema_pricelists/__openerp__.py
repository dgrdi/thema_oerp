{
    'name': 'Thema - pricelists',
    'version': '0.2',
    'category': 'Product management',
    'description': """Allows pricelists with fixed prices, disconnected from product fields""",
    'author': 'Thema Optical srl',
    'website': 'http://www.thema-optical.com',
    'depends': ['thema_product',],
    'init_xml': [],
    'data':[
            'init.xml',
            'views/pricelist.xml',
        ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
