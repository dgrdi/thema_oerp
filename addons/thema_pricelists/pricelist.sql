
DROP FUNCTION IF EXISTS thema_find_pricelist_items();

CREATE OR REPLACE FUNCTION thema_find_pricelist_items() RETURNS TABLE (
  product_id int,
  qty numeric,
  partner_id int,
  pricelist_id int,
  item_id int,
  suppinfo_id int
) AS
  $$
    DECLARE
      curn int;
      n int;
    BEGIN

      FOR n, product_id, qty, partner_id, pricelist_id, item_id, suppinfo_id IN
        WITH temp_plp AS (
          SELECT
            row_number() OVER (ORDER BY pqp.product_id) n,
            pqp.*,
            p.product_tmpl_id,
            tpl.categ_id,
            tpl.collection_id,
            tpl.model_id,
            si.id suppinfo_id,
            v.pricelist_id,
            vv.price_version_id
          FROM product_by_qty_by_partner pqp
          CROSS JOIN tmp_pl_versions vv
          INNER JOIN product_product p on p.id = pqp.product_id
          INNER JOIN product_template tpl on tpl.id = p.product_tmpl_id
          INNER JOIN product_pricelist_version v ON v.id = vv.price_version_id
          LEFT JOIN product_model m ON m.id = tpl.model_id
          LEFT JOIN product_supplierinfo si ON pqp.partner_id = si.name AND p.product_tmpl_id = si.product_id
        ),
        temp_categ_cache  AS (
            SELECT p.id parent_id, c.id child_id
                FROM product_category p, product_category c
                WHERE
                    p.parent_left <= c.parent_left
                AND p.parent_right >= c.parent_right
                AND p.parent_left <= c.parent_right
                AND p.parent_right >= c.parent_left
        ),
        temp_collection_cache AS (
            SELECT p.id parent_id, c.id child_id
                FROM product_collection p, product_collection c
                WHERE
                    p.parent_left <= c.parent_left
                AND p.parent_right >= c.parent_right
                AND p.parent_left <= c.parent_right
                AND p.parent_right >= c.parent_left
        ),
        temp_pl_items AS (
            SELECT plp.*, i.id item_id, i.sequence
            FROM temp_plp plp
            CROSS JOIN product_pricelist_item i
            LEFT JOIN temp_categ_cache catc ON catc.child_id = plp.categ_id AND catc.parent_id = i.categ_id
            LEFT JOIN temp_collection_cache colc ON colc.child_id = plp.collection_id AND colc.parent_id = i.collection_id
            WHERE
                  (i.price_version_id = plp.price_version_id)
              AND (i.product_id IS NULL OR i.product_id = plp.product_id)
              AND (i.product_tmpl_id IS NULL OR i.product_tmpl_id = plp.product_tmpl_id)
              AND (i.model_id IS NULL OR i.model_id = plp.model_id)
              AND (i.min_quantity IS NULL OR i.min_quantity <= plp.qty)
              AND (i.base <> -2 OR plp.suppinfo_id IS NOT NULL)
              AND (i.categ_id IS NULL OR catc.parent_id IS NOT NULL)
              AND (i.collection_id IS NULL OR colc.parent_id IS NOT NULL)
        )
          SELECT
            plp.n,
            plp.product_id,
            plp.qty,
            plp.partner_id,
            plp.pricelist_id,
            i.item_id,
            i.suppinfo_id
          FROM
            temp_plp plp
            LEFT JOIN temp_pl_items i ON plp.n = i.n
          ORDER BY plp.n, i.sequence
        LOOP
          IF COALESCE(curn, -1) <> n THEN
            curn := n;
            RETURN NEXT;
          END IF;
        END LOOP
        ;

      RETURN;
    END;
  $$ language plpgsql;