
import time
from openerp.osv import osv
from openerp.osv import fields, orm
from openerp.tools.translate import _
from openerp.tools.float_utils import  float_round
from logging import getLogger
logger = getLogger(__name__)

class product_pricelist_item(orm.Model):
    _inherit = 'product.pricelist.item'

    def _price_field_get(self, cr, uid, context=None):
        res = super(product_pricelist_item, self)._price_field_get(cr, uid, context)
        assert -3 not in zip(*res)[0], 'Unexpected: -3 is already taken'
        res.append((-3, _('Fixed price')))
        return res

    _columns = {
        'base': fields.selection(_price_field_get, 'Based on', required=True, size=-1, help="Base price for computation."),
        'model_id': fields.many2one('product.model', 'Modello'),
        'collection_id': fields.many2one('product.collection', 'Collection',
                                         help="Specify a collection if this rule only applies to product of one "
                                              "collection. Keep empty otherwise."),
        'fixed_price': fields.float('Fixed price', digits=(16,4)),
    }


class product_pricelist(orm.Model):
    _inherit = 'product.pricelist'

    def _init_sql(self, cr, uid, context=None):
        import os
        with open(os.path.join(os.path.dirname(__file__), 'pricelist.sql')) as sqlfile:
            cr.execute(sqlfile.read())

    def get_versions(self, cr, uid, pricelist_ids, context=None):
        """
        :return: a list of pricelist versions active at the date in context['date'] (or today, if undefined),
                 one for each pricelist in pricelist_ids.
        """

        if context is None:
            context = {}

        date = context.get('date') or time.strftime('%Y-%m-%d')

        pricelist_version_ids = self.pool.get('product.pricelist.version').search(cr, uid, [
                                                        ('pricelist_id', 'in', pricelist_ids),
                                                        '|',
                                                        ('date_start', '=', False),
                                                        ('date_start', '<=', date),
                                                        '|',
                                                        ('date_end', '=', False),
                                                        ('date_end', '>=', date),
                                                    ])

        if len(pricelist_ids) != len(pricelist_version_ids):
            raise osv.except_osv(_('Warning!'), _("At least one pricelist has no active version !\nPlease create or activate one."))

        return pricelist_version_ids

    def find_items(self, cr, uid, version_ids, products_by_qty_by_partner, context=None):
        """
        :param version_ids: list of pricelist version ids
        :param products_by_qty_by_partner: list of 3-tuples (product_id, qty, partner_id)
        :return: a list of 6-tuples (product_id, qty, partner_id, pricelist_id, item_id, suppinfo_id),
                 of size len(version_ids) * len(products_products_by_qty_by_partner).
                 For each triple in products_by_qty_by_partner, and for each version in version_ids,
                 pricelist_id is the pricelist of version_id, and item_id is the applicable pricelist item,
                 or None if there is none. suppinfo_id is the ID of the applicable product_supplierinfo
                 object for items based on supplier prices.

        This operation is expensive, so it is done entirely at the database level. It should be fine to circumvent
        the ORM because there is no write, and the only read access rules that are ignored are the ones on
        product category and collection, which should be visible to everyone.
        """
        if context is None:
            context = {}

        vals = ', '.join(['%s' for el in products_by_qty_by_partner])
        products_by_qty_by_partner = map(tuple, products_by_qty_by_partner)

        cr.execute("""
            DROP TABLE IF EXISTS product_by_qty_by_partner;
            CREATE TEMPORARY TABLE product_by_qty_by_partner (
                product_id int,
                qty numeric,
                partner_id int
            ) ON COMMIT DROP;

            INSERT INTO product_by_qty_by_partner VALUES {vals};

            DROP TABLE IF EXISTS tmp_pl_versions;
            CREATE TEMPORARY TABLE tmp_pl_versions (price_version_id int PRIMARY KEY) ON COMMIT DROP;
            INSERT INTO tmp_pl_versions
            SELECT v FROM unnest(%s) g(v);

            SELECT * FROM thema_find_pricelist_items();

        """.format(vals=vals), products_by_qty_by_partner + [list(version_ids)])

        return cr.fetchall()




    def compute_prices(self, cr, uid, items, prices, pricelist_currencies, context):
        """
        :param items: a list of 6-tuples in the format returned by find_items().
        :param prices: a dict of prices in the format returned by price_get_multi(). These will be used
                       to calculate items based on other pricelists.
        :param pricelist_currencies: a dict {pricelist_id: currency_id}, that must contain
                                     a value for every pricelist_id that appears in `items`.
        :return: a 3-tuple (prices, other_pricelists, unresolved_items)
                 `prices` is a dict in the format returned by price_get_multi()
                 `other_pricelists` is a list of ids to pricelists on which some of the items depend.
                 `unresolved_items` is a subset of `items` that could not be computed because they depend
                                    on one of the pricelists in `other_pricelists`, and the price could not
                                    be found in `prices`.
        """
        if not items:
            return {}
        if context is None:
            context = {}

        price_type_obj = self.pool.get('product.price.type')
        product_obj = self.pool.get('product.product')
        item_obj = self.pool.get('product.pricelist.item')
        product_uom_obj = self.pool.get('product.uom')
        currency_obj = self.pool.get('res.currency')
        partnerinfo_obj = self.pool.get('pricelist.partnerinfo')
        supplierinfo_obj = self.pool.get('product.supplierinfo')

        results = {}
        uom = context.get('uom')

        # read the pricelist items
        item_fields = ('id', 'base', 'base_pricelist_id', 'currency_id',
                       'price_discount', 'price_round', 'price_surcharge',
                       'price_min_margin', 'price_max_margin', 'fixed_price', )
        item_read = item_obj.read(cr, uid, filter(lambda x: x is not None, zip(*items)[4]),
                                  fields=item_fields, context=context)
        item_read = {i['id']: i for i in item_read}

        # read the products' units of measure
        products = product_obj.read(cr, uid, zip(*items)[0], fields=('uos_id', 'uom_id'))
        products = {p['id']: p for p in products}

        # read supplier info
        suppinfo_ids = filter(lambda x: x, zip(*items)[5])
        supplier_info = supplierinfo_obj.read(cr, uid, suppinfo_ids, fields=('product_uom', ), context=context)
        supplier_info = {p['id']: p for p in supplier_info}
        partnerinfo_ids = partnerinfo_obj.search(cr, uid, [('suppinfo_id', 'in', suppinfo_ids)],
                                                 order='suppinfo_id, min_quantity DESC',  context=context, )
        pinfo = partnerinfo_obj.read(cr, uid, partnerinfo_ids, fields=('price', 'min_quantity', 'suppinfo_id'))
        pinfo = {p['id']: p for p in pinfo}
        partner_info = {}
        for p in partnerinfo_ids:
            suppinfo_id = pinfo[p]['suppinfo_id'][0]
            if suppinfo_id in partner_info:
                partner_info[suppinfo_id].append(pinfo[p])
            else:
                partner_info[suppinfo_id] = [pinfo[p]]

        def update_prices(pricelist_id, product_id, price, item, calc_uom=False):
            """
            Updates the `results` dict, for a certain `product_id` and `pricelist_id`,
             with the price `price`, adjusted with the rules (surcharge, discount, etc..)
             contained in `item`. If calc_uom is True and a unit of measure is defined in
             context['uom'], then the price is adjusted for the unit of measure as well.
            """
            if price is not False:
                price_limit = price
                price = price * (1.0 + (item['price_discount'] or 0.0))
                if item['price_round']:
                    price = float_round(price, precision_rounding=item['price_round'])
                price += (item['price_surcharge'] or 0.0)
                if item['price_min_margin']:
                    price = max(price, price_limit+item['price_min_margin'])
                if item['price_max_margin']:
                    price = min(price, price_limit+item['price_max_margin'])
            if price:
                results['item_id'] = item['id']
                if uom and calc_uom:
                    u = products[product_id]['uos_id'] or products[product_id]['uom_id']
                    price = product_uom_obj._compute_price(cr, uid, u[0], price, uom)
            if product_id in results:
                results[product_id][pricelist_id] = price
            else:
                results[product_id] = {pricelist_id: price}

        other_pricelists = set()
        unresolved_items = set()
        product_items = {}

        # first pass: loop through each item, keep track of which items need to call product.price_get(),
        # which items need another pricelist to be calculated,
        # and calculate all the rest.

        for product_id, qty, partner_id, pricelist_id, item_id, suppinfo_id in items:
            if item_id is None:
                # there is no applicable rule - price is set to False
                update_prices(pricelist_id, product_id, False, None)
                continue
            item = item_read[item_id]
            base = item['base']
            if base == -1:
                # price is based on another pricelist. See if we can  find the other
                # pricelist's price for this product in `prices`, and if so calculate
                # the price; otherwise mark the item as unresolved, and add the referenced
                # pricelist to `other_pricelists`.
                base_pl = item['base_pricelist_id']
                if not base_pl:
                    update_prices(pricelist_id, product_id, 0.0, item, calc_uom=True)
                else:
                    base_pl = base_pl[0]
                    if product_id in prices and base_pl in prices[product_id]:
                        price = prices[product_id][base_pl]
                        price = currency_obj.compute(cr, uid,
                                                     pricelist_currencies[base_pl], pricelist_currencies[pricelist_id],
                                                     price, round=False, context=context)
                        update_prices(pricelist_id, product_id, price, item)
                    else:
                        other_pricelists.add(base_pl)
                        unresolved_items.add((product_id, qty, partner_id, pricelist_id, item_id, suppinfo_id))
            elif base == -2:
                # price based on supplier prices.
                # we calculate the quantity in uom, and look for the first supplier price whose
                # minimum quantity is below the uom quantity.
                qty_in_product_uom = qty
                price = 0.0
                if suppinfo_id:
                    suppinfo = supplier_info[suppinfo_id]
                    from_uom = uom or products[product_id]['uom_id'][0]
                    seller_uom = suppinfo['product_uom'] and suppinfo['product_uom'][0]
                    if seller_uom and from_uom and from_uom != seller_uom:
                        qty_in_product_uom = product_uom_obj._compute_qty(cr, uid,
                                                                          from_uom,
                                                                          qty, to_uom_id=seller_uom,
                                                                          context=context)
                    for pinfo in partner_info[suppinfo_id]:
                        if not pinfo['min_quantity'] or pinfo['min_quantity'] <= qty_in_product_uom:
                            price = pinfo['price']
                            break
                update_prices(pricelist_id, product_id, price, item, calc_uom=(qty!=qty_in_product_uom))
            elif base == -3:
                # the price is fixed, and contained in the pricelist item
                price = item['fixed_price']
                update_prices(pricelist_id, product_id, price, item)
            else:
                # the price is based on some product field. Keep a reference
                # to this item, so that later we can fetch all the data at once
                # from the product.
                if base in product_items:
                    product_items[base].append((product_id, pricelist_id, item))
                else:
                    product_items[base] = [(product_id, pricelist_id, item)]

        # (begin) prices based on product fields

        # after the first pass, the prices based on product fields remain to be calculated.
        # the following calls product_product.price_get() once for each different
        # price type, and updates the results.
        price_types = price_type_obj.read(cr, uid, product_items.keys(), fields=('field', 'currency_id'))
        price_types = {t['id']: t for t in price_types}

        product_prices = {}
        for base, proditem in product_items.items():
            price_type = price_types[base]
            products = list(zip(*proditem)[0])
            product_prices[base] = product_obj.price_get(cr, uid, products, price_type['field'], context)

        for base, proditems in product_items.items():
            type = price_types[base]
            for product_id, pricelist_id, item in proditems:
                price = product_prices[base][product_id]
                price = currency_obj.compute(cr, uid,
                                             type['currency_id'][0], pricelist_currencies[pricelist_id],
                                             price, round=False, context=context)
                update_prices(pricelist_id, product_id, price, item)
        # (end) prices based on product fields

        return results, other_pricelists, unresolved_items

    def price_get_multi(self, cr, uid, pricelist_ids, products_by_qty_by_partner, context=None):
        if context is None:
            context = {}

        if not pricelist_ids:
            pricelist_ids = self.pool.get('product.pricelist').search(cr, uid, [], context=context)


        visited_pricelists = set(pricelist_ids)

        versions = self.get_versions(cr, uid, pricelist_ids, context)
        items = self.find_items(cr, uid, versions, products_by_qty_by_partner, context)
        product_ids = set(zip(*products_by_qty_by_partner)[0])
        results = {}
        prices = {}

        def update_results(data):
            for product_id, prc in data.items():
                if product_id in prices:
                    prices[product_id].update(prc)
                    if product_id in product_ids:
                        results[product_id].update(prc)
                else:
                    prices[product_id] = prc
                    if product_id in product_ids:
                        results[product_id] = prc

        pricelist_currencies = {}
        item_id = None
        other_pricelists = pricelist_ids

        while items:
            pcur = self.read(cr, uid, other_pricelists, fields=('currency_id', ))
            pricelist_currencies.update({pl['id']: pl['currency_id'][0] for pl in pcur})
            res, other_pricelists, unresolved_items = self.compute_prices(cr, uid, items, prices,
                                                                          pricelist_currencies, context=context)
            if item_id is None:
                item_id = prices.pop('item_id', None)
            else:
                prices.pop('item_id', None)

            update_results(res)
            items = unresolved_items
            other_pricelists -= visited_pricelists
            if other_pricelists:
                other_prices = self.price_get_multi(cr, uid, list(other_pricelists),
                                                    map(lambda x: x[:3], unresolved_items),
                                                    context)
                update_results(other_prices)
                visited_pricelists = visited_pricelists.union(other_pricelists)


        results['item_id'] = item_id
        return results






