from openerp.tests.common import SingleTransactionCase, TransactionCase
from testutils.transaction import SavepointCase
products = [
    #product name, template name, collection name, category name, standard_price
    ('test1', 'ttest1', 'coll1', 'cat1', 10),
    ('test2', 'ttest2', 'coll2', 'cat2', 20),
    ('test3', 'ttest3', 'coll3', 'cat3', 30),
]

class test_pricelist(SavepointCase):

    products = []
    noise_objects = {}

    @classmethod
    def setUpClass(cls):
        super(test_pricelist, cls).setUpClass()
        cls.products = []

        cls.product = cls.registry('product.product')
        cls.template = cls.registry('product.template')
        cls.collection  = cls.registry('product.collection')
        cls.category = cls.registry('product.category')
        cls.pricetype = cls.registry('product.price.type')
        cls.pricelist = cls.registry('product.pricelist')
        cls.pricever = cls.registry('product.pricelist.version')
        cls.priceitem = cls.registry('product.pricelist.item')
        cls.suppinfo = cls.registry('product.supplierinfo')
        cls.partinfo = cls.registry('pricelist.partnerinfo')
        cls.partner = cls.registry('res.partner')


        spid = cls.registry('ir.model.fields').search(cls.cr, cls.uid, [('model', '=', 'product.template'),
                                                                        ('name', '=', 'standard_price')])
        assert len(spid) == 1, spid
        spid = cls.registry('ir.model.fields').read(cls.cr, cls.uid, spid[0], fields=('name', ))['name']
        cls.cost_type = cls.pricetype.create(cls.cr, cls.uid, {
            'field': spid,
            'name': 'Standard price',
        })

        for pname, tname, colname, catname, cost in products:
            cat = cls.category.create(cls.cr, cls.uid, {'name': catname})
            coll = cls.collection.create(cls.cr, cls.uid, {'name': colname})
            tpl = cls.template.create(cls.cr, cls.uid, {
                'name': tname,
                'categ_id': cat,
                'collection_id': coll,
                'standard_price': cost,
            })
            cls.products.append(cls.product.create(cls.cr, cls.uid, {
                'name': pname,
                'product_tmpl_id': tpl,
            }))

        cls.products = cls.product.browse(cls.cr, cls.uid, cls.products)

        tpl = cls.template.create(cls.cr, cls.uid, {'name': 'null'})

        cls.noise_objects.update({
            'product_tmpl_id': tpl,
            'product_id': cls.product.create(cls.cr, cls.uid, {'name': 'null', 'product_tmpl_id': tpl}),
            'categ_id':  cls.category.create(cls.cr, cls.uid, {'name': 'null'}),
            'collection_id': cls.collection.create(cls.cr, cls.uid, {'name': 'null'}),
        })


    def create_pricelist(self):
        pl = self.pricelist.create(self.cr, self.uid, {
            'name': 'test',
            'type': 'sale',

        })
        plv = self.pricever.create(self.cr, self.uid, {
            'name': 'test',
            'pricelist_id': pl,
        })
        return pl, plv

    def setUp(self):
        super(test_pricelist, self).setUp()
        self.pl, self.plv = self.create_pricelist()


    def tearDown(self):
        self.pricelist.unlink(self.cr, self.uid, self.pl)
        super(test_pricelist, self).tearDown()


    def create_rule(self, params):
        defaults = {
            'price_version_id': self.plv,
        }
        defaults.update(params)
        return self.priceitem.create(self.cr, self.uid, defaults)

    def create_noise_rule(self, type):
        obj = None
        params = {
            'sequence': 0,
            'base': self.cost_type,
            'price_surcharge': 99,
            type: self.noise_objects[type],
            'price_version_id': self.plv,
        }
        self.priceitem.create(self.cr, self.uid, params)

    def expect_prices(self, price_per_product):
        pqp = [(p, 1, None) for p in price_per_product]
        ret = self.pricelist.price_get_multi(self.cr, self.uid, [self.pl], pqp)
        ret = {pid: pr[self.pl] for pid, pr in ret.items() if pid != 'item_id'}
        self.assertDictEqual(ret, price_per_product)

    def test_standard_price(self):
        self.create_rule({
            'base': self.cost_type,
        })
        expect = {p.id: p.standard_price for p in self.products}
        self.expect_prices(expect)

    def test_standard_price_per_product(self):
        for p in self.products:
            self.create_rule({
                'base': self.cost_type,
                'product_id': p.id
            })
        self.create_noise_rule('product_id')
        expect = {p.id: p.standard_price for p in self.products}
        self.expect_prices(expect)

    def test_standard_price_per_template(self):
        for p in self.products:
            self.create_rule({
                'base': self.cost_type,
                'product_tmpl_id': p.product_tmpl_id.id,
            })
        self.create_noise_rule('product_tmpl_id')
        expect = {p.id: p.standard_price for p in self.products}
        self.expect_prices(expect)

    def test_standard_price_per_categ_id(self):
        for p in self.products:
            self.create_rule({
                'base': self.cost_type,
                'categ_id': p.categ_id.id,
            })
        self.category._parent_store_compute(self.cr)
        self.create_noise_rule('categ_id')
        expect = {p.id: p.standard_price for p in self.products}
        self.expect_prices(expect)

    def test_standard_price_per_collection(self):
        for p in self.products:
            self.create_rule({
                'base': self.cost_type,
                'collection_id': p.collection_id.id,
            })
        self.create_noise_rule('collection_id')
        self.collection._parent_store_compute(self.cr)
        expect = {p.id: p.standard_price for p in self.products}
        self.expect_prices(expect)

    def test_price_discount(self):
        self.create_rule({
            'base': self.cost_type,
            'price_discount': 1,
        })
        self.expect_prices({self.products[0].id: self.products[0].standard_price * 2})

    def test_price_surcharge(self):
        self.create_rule({
            'base': self.cost_type,
            'price_surcharge': 1,
        })
        self.expect_prices({self.products[0].id: self.products[0].standard_price + 1})

    def test_price_min_margin(self):
        self.create_rule({
            'base': self.cost_type,
            'price_surcharge': 1,
            'price_min_margin': 100
        })
        self.expect_prices({self.products[0].id: self.products[0].standard_price + 100})

    def test_price_max_margin(self):
        self.create_rule({
            'base': self.cost_type,
            'price_max_margin': 1,
            'price_surcharge': 2,
        })
        self.expect_prices({self.products[0].id: self.products[0].standard_price + 1})

    def test_pricelist_base(self):
        pl, plv = self.create_pricelist()
        self.priceitem.create(self.cr, self.uid, {
            'price_version_id': plv,
            'base': self.cost_type,
        })
        self.create_rule({
            'base': -1,
            'base_pricelist_id': pl,
        })
        self.expect_prices({p.id: p.standard_price for p in self.products})

    def test_pricelist_suppinfo(self):
        sup = self.partner.create(self.cr, self.uid, {
            'supplier': True,
            'name': 'test',
        })
        si = self.suppinfo.create(self.cr, self.uid, {
            'name': sup,
            'product_id': self.products[0].product_tmpl_id.id,
            'min_qty': 1,
        })

        self.partinfo.create(self.cr, self.uid, {
            'suppinfo_id': si,
            'min_quantity': 10,
            'price': 123,
        })
        self.create_rule({
            'base': -2,
        })
        pr = self.pricelist.price_get_multi(self.cr, self.uid, [self.pl], [(self.products[0].id, 10, sup)])
        pr.pop('item_id', None)
        self.assertDictEqual(pr, {self.products[0].id: {self.pl: 123}})

    def test_min_qty(self):
        self.create_rule({
            'min_quantity': 10,
            'sequence': 1,
            'base': self.cost_type,
            'price_discount': 1,
        })
        self.create_rule({
            'sequence': 2,
            'price_discount': -0.5,
            'base': self.cost_type,
        })
        pr1 = self.pricelist.price_get_multi(self.cr, self.uid, [self.pl], [(self.products[0].id, 1, None)])
        pr2 = self.pricelist.price_get_multi(self.cr, self.uid, [self.pl], [(self.products[0].id, 10, None)])
        pr1.pop('item_id', None)
        pr2.pop('item_id', None)
        self.assertDictEqual(pr1, {self.products[0].id: {self.pl: self.products[0].standard_price / 2}})
        self.assertDictEqual(pr2, {self.products[0].id: {self.pl: self.products[0].standard_price * 2}})


    def test_fixed_price(self):
        self.create_rule({
            'base': -3,
            'fixed_price': 10
        })
        self.expect_prices({p['id']: 10 for p in self.products})

    def test_missing_rule(self):
        self.expect_prices({p['id']: False for p in self.products})
