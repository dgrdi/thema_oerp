from .oereq import OERequestManager
from .service import service_registry, OEService
from .model import model_registry, OEModel

class OEClient(object):
    def __init__(self, url):
        self.request_manager = OERequestManager(url)
        self.url = url
        self.dataset_service = self.get_service('dataset')

    def authenticate(self, db, login, password):
        self.get_service('session').authenticate(db=db, login=login, password=password)

    def get_service(self, name):
        if name in service_registry:
            return service_registry[name](self.request_manager, name)
        else:
            return OEService(self.request_manager, name)

    def get_model(self, name):
        if name in model_registry:
            return model_registry['name'](self.dataset_service, name)
        else:
            return OEModel(self.dataset_service, name)

