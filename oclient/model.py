import functools

model_registry = {}

def model_register(klass=None, name=None):
    if klass is None:
        return functools.partial(model_register, name=name)
    model_registry[name] = klass
    return klass

class OEModel(object):

    def __init__(self, dataset_service, name):
        self.name = name
        self.dataset_service = dataset_service

    def _call(self, method, args, kwargs):
        return self.dataset_service.call_kw(model=self.name,
                                            method=method,
                                            args=args,
                                            kwargs=kwargs)

    def __getattr__(self, item):
        def mproxy(*args, **kwargs):
            return self._call(item, args, kwargs)
        return mproxy

    def search_read(self, fields=None, offset=0, limit=None, domain=None, sort=None, context=None):
        context = context or {}
        domain = domain or []
        res =  self.dataset_service.search_read(model=self.name,
                                                fields=fields,
                                                offset=offset,
                                                limit=limit,
                                                domain=domain,
                                                sort=sort,
                                                context=context)
        return res['records']

