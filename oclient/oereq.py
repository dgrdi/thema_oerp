import requests
import json

class OException(Exception):
    def __init__(self, error_data):
        self.code = error_data['code']
        self.debug = error_data['data']['debug']
        self.type = error_data['data']['type']
        self.message = error_data['message']

    def __unicode__(self):
        return self.debug
    def __str__(self):
        return self.debug


class OERequestManager(object):
    SERVICE_URL = '/web/'
    SESSION_INFO_URL = 'session/get_session_info'

    def __init__(self, base_url):
        self._rid = 0
        self.base_url = base_url
        self.cookies = {}
        res = self.request_json(self.SESSION_INFO_URL, fill_session=False)
        self.cookies = res.cookies
        self.session_id = res.json()['result']['session_id']

    @property
    def next_id(self):
        self._rid += 1
        return 'r%s' % self._rid

    def request_json(self, url, params=None, method='call', fill_session=True):
        params = params or {}
        if fill_session:
            params['session_id'] = self.session_id
        data = {
            'id': self.next_id,
            'jsonrpc': '2.0',
            'method': method,
            'params': params
        }
        return requests.post(self.base_url + self.SERVICE_URL +  url, json.dumps(data), cookies=self.cookies)

    def parse_json(self, res):
        res.raise_for_status()
        rj = res.json()
        error = rj.get('error', None)
        if error is None:
            return rj['result']
        else:
            raise OException(rj['error'])

    def invoke_json(self, url, params=None, method='call', fill_session=True):
        res = self.request_json(url, params, method, fill_session)
        return self.parse_json(res)

    def invoke_uencode(self, url, params=None, fill_session=True):
        params = params or {}
        if fill_session:
            params['session_id'] = self.session_id
        res = requests.post(self.base_url + self.SERVICE_URL + url, params, cookies=self.cookies)
        res.raise_for_status()
        return res.content

