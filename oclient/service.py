import functools
service_registry = {}
import json

def service_register(klass=None, name=None):
    if klass is None:
        return functools.partial(service_register, name=name)
    service_registry[name] = klass
    return klass

class OEService(object):
    _uencode_methods = []

    def __init__(self, request_manager, name):
        self.request_manager = request_manager
        self.name = name

    def _call(self, method, kwargs):
        if method in self._uencode_methods:
            return self.request_manager.invoke_uencode(self._url(method), kwargs)
        else:
            return self.request_manager.invoke_json(self._url(method), kwargs)

    def _url(self, method):
        return self.name + '/' + method

    def __call__(self, **kwargs):
        return self._call('', kwargs)

    def __getattr__(self, item):
        def mproxy(**kwargs):
            return self._call(item, kwargs)
        return mproxy

@service_register(name='report')
class OEReport(OEService):
    _uencode_methods = ['']

    def report(self, report_name, ids=None, datas=None, report_type=None, context=None):
        datas = datas or {}
        if 'ids' not in datas and ids:
            datas['ids'] = ids
        context = context or {}
        context['active_ids'] = ids or []
        action = {
            'report_name': report_name,
            'report_type': report_type,
            'datas': datas,
            'context': context or {},
        }

        if not report_type:
            del action['report_type']

        return self(action=json.dumps(action), token='')

