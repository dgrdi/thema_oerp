DROP PROCEDURE thema.pr_import_offerta;
go;
CREATE procedure [thema].[pr_import_offerta] (
  @CAMPO_RICERCA tyclifor,
  @DESTINAZIONE tydestinaz,
  @PROVENIENZA varchar(10),
  @NUM_OFFCLI varchar(10),
  @NOTE varchar(max),
  @RIGHE thema.ty_righeimport READONLY
) as begin

  if exists (
      select  * from tempdb.dbo.sysobjects o
      where o.xtype in ('U')

     and o.id = object_id(N'tempdb..#import_offerta')
  )
  DROP TABLE #import_offerta;

  declare @offerte table (ID_OFFERTA int);
  declare @idoff int = NULL;


  select *
  into #import_offerta
  from @RIGHE ;

  begin try

    set @idoff = NULL;
    exec @idoff = cdepgv.pr_import_consuma_gv @CAMPO_RICERCA,
                                              @DESTINAZIONE, @PROVENIENZA, @NUM_OFFCLI, @NOTE;
    if isnull(@idoff, 0) <> 0 insert into @offerte values(@idoff);

    exec @idoff = cdepgv.pr_import_consuma_coi @CAMPO_RICERCA,
                                              @DESTINAZIONE, @PROVENIENZA, @NUM_OFFCLI, @NOTE;
    if isnull(@idoff, 0) <> 0 insert into @offerte values(@idoff);

    set @idoff = NULL;
    exec @idoff = thema.pr_import_consuma_stock @CAMPO_RICERCA,
                                              @DESTINAZIONE, @PROVENIENZA, @NUM_OFFCLI, @NOTE;
    if isnull(@idoff, 0) <> 0 insert into @offerte values(@idoff);


    set @idoff = NULL;
    exec @idoff = thema.pr_import_consuma_standard @CAMPO_RICERCA,
                                              @DESTINAZIONE, @PROVENIENZA, @NUM_OFFCLI, @NOTE;
    if isnull(@idoff, 0) <> 0 insert into @offerte values(@idoff);


    -- import spedizione per ordini americani
    update T_OFFERTE_CLI set CAMPO_RICERCA_DDT = '012708'
    where ID_OFFERTA in (select ID_OFFERTA from @offerte)
    and isnull(COD_NAZIONE, '') = 'US';

    -- sblooca offerte COI bloccate
    update T_OFFERTE_CLI set F_BLOCCO = NULL
    where isnull(F_BLOCCO, '') <> '' and COD_DOC_OFFCLI = 'VG'
    and ID_OFFERTA in (select ID_OFFERTA from @offerte);

  end try
  begin catch
    declare @err varchar(max) = ERROR_MESSAGE();
    declare ff cursor for select ID_OFFERTA from @offerte;
    open ff;
    while 1=1 begin
      fetch ff into @idoff;
      if @@fetch_status <> 0 break;
      exec PR_Delete_T_OFFERTE_CLI @idoff;
    end;
    deallocate ff;
    raiserror (@err, 11, -1);
  end catch

  select * from @offerte;

  end;